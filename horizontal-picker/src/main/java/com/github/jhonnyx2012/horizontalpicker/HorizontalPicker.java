package com.github.jhonnyx2012.horizontalpicker;

import android.app.AlarmManager;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Jhonny Barrios on 22/02/2017.
 */

public class HorizontalPicker extends LinearLayout implements HorizontalPickerListener {

    private static final int NO_SETTED = -1;
    private TextView tvMonth;
    private DatePickerListener listener;
    private HorizontalPickerRecyclerView rvDays;
    private List<Day> daysList = new ArrayList<>();
    private int days;
    private int offset;

    public HorizontalPicker(Context context) {
        super(context);
        internInit();
    }

    public HorizontalPicker(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        internInit();

    }

    public HorizontalPicker(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        internInit();
    }

    private void internInit() {
        this.days = NO_SETTED;
        this.offset = NO_SETTED;
    }

    public HorizontalPicker setListener(DatePickerListener listener) {
        this.listener = listener;
        return this;
    }

    public void setDate(final DateTime date) {
        rvDays.post(new Runnable() {
            @Override
            public void run() {
                rvDays.setDate(date);
            }
        });
    }


    public void init() {
        inflate(getContext(), R.layout.horizontal_picker, this);
        rvDays = (HorizontalPickerRecyclerView) findViewById(R.id.rvDays);
        rvDays.setDaysList(daysList);
        int DEFAULT_DAYS_TO_PLUS = 120;
        int finalDays = days == NO_SETTED ? DEFAULT_DAYS_TO_PLUS : days;
        int DEFAULT_INITIAL_OFFSET = 7;
        int finalOffset = offset == NO_SETTED ? DEFAULT_INITIAL_OFFSET : offset;
        tvMonth = (TextView) findViewById(R.id.tvMonth);
        Typeface typeface1=Typeface.createFromAsset(getContext().getAssets(), "OpenSans-Regular.ttf");
        tvMonth.setTypeface(typeface1);

        rvDays.setListener(this);
        rvDays.init(getContext(), finalDays, finalOffset);
    }

    @Override
    public boolean post(Runnable action) {
        return rvDays.post(action);
    }

    @Override
    public void onStopDraggingPicker() {
    }

    @Override
    public void onDraggingPicker() {
    }

    @Override
    public void onDateSelected(Day item) {
        if (listener != null) {
            listener.onDateSelected(item.getRealDate());
        }
    }

    @Override
    public void onWeekChange(Day firstDay, Day lastDay) {

        String month = firstDay.getMonth();
        String monthLast = lastDay.getMonth();
        String day = firstDay.getDay();
        tvMonth.setText(month.substring(0, 3).toUpperCase() + " " + day + " - " + monthLast.substring(0, 3).toUpperCase() + " " + lastDay.getDay());
    }

    public HorizontalPicker setDays(int days) {
        this.days = days;
        return this;
    }

    public int getDays() {
        return days;
    }

    public HorizontalPicker setOffset(int offset) {
        this.offset = offset;
        return this;
    }

    public List<Day> generateDays(long toDate, long initialDate, List<Long> selectedList) {
        List<Day> days = new ArrayList<>();
        int i = 0;
        long n = getDaysSize(toDate, initialDate);
        while (i < n) {
            DateTime actualDate = new DateTime(initialDate + (AlarmManager.INTERVAL_DAY * i++));
            Day day = new Day(actualDate);
            Date date = new Date(day.getDate().getMillis());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
            for (long selectedDay : selectedList) {
                Date dateEvent = new Date(selectedDay);
                if (simpleDateFormat.format(date).equals(simpleDateFormat.format(dateEvent))) {
                    day.setEvent(true);
                }
            }
            days.add(day);
        }
        return days;
    }


    public long getDaysSize(long toDate, long initialDate) {
        return (toDate - initialDate) / AlarmManager.INTERVAL_DAY;
    }

    public HorizontalPicker setDaysList(List<Day> daysList) {
        this.daysList.addAll(daysList);
        return this;
    }

    public void setSelect(Day day) {
        rvDays.setSelect(day);
    }
}
