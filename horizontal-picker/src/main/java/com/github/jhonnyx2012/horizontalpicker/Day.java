package com.github.jhonnyx2012.horizontalpicker;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by jhonn on 28/02/2017.
 */
public class Day {
    private DateTime date;
    private boolean selected;
    private boolean isEvent;

    public Day(DateTime date) {
        this.date = date;
    }

    public String getDay() {
        return String.valueOf(date.getDayOfMonth());
    }

    public String getWeekDay() {
        return date.toString("EEE", Locale.getDefault()).toUpperCase();
    }

    public String getMonth() {
        return date.toString("MMMM YYYY", Locale.getDefault());
    }

    public DateTime getDate() {
        return date.withTime(0,0,0,0);
    }
    public DateTime getRealDate() {
        return date;
    }

    public boolean isEvent() {
        return isEvent;
    }

    public void setEvent(boolean event) {
        isEvent = event;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }
}
