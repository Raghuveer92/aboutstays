package com.aboutstays.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aboutstays.R;
import com.aboutstays.holder.BaseHolder;
import com.aboutstays.holder.CheckInInformationHolder;
import com.aboutstays.holder.CheckInServiceHolder;
import com.aboutstays.holder.MessageHolder;
import com.aboutstays.holder.StaysHolder;
import com.aboutstays.model.BaseAdapterModel;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.List;

import simplifii.framework.utility.AppConstants;

/**
 * Created by Neeraj on 12/29/2016.
 */

public class BaseRecycleAdapter<T extends BaseAdapterModel> extends RecyclerSwipeAdapter<BaseHolder> {

    protected List<T> list;
    protected Context context;
    protected LayoutInflater inflater;
    protected AlertDialog dialog;
    private RecyclerClickInterface clickInterface;
    private RecyclerClickListener listener;

    public RecyclerClickInterface getClickInterface() {
        return clickInterface;
    }

    public void setClickInterface(RecyclerClickInterface clickInterface) {
        this.clickInterface = clickInterface;
    }

    public BaseRecycleAdapter(Context context, List<T> list) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }


    public RecyclerClickListener getListener() {
        return listener;
    }

    public void setListener(RecyclerClickListener listener) {
        this.listener = listener;
    }

    @Override
    public BaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        BaseHolder holder = null;
        switch (viewType) {
            case AppConstants.VIEW_TYPE.CHECKIN_SERVICE_DATA:
                //View inflate = inflater.inflate(R.layout.row_services, null);
                holder = new CheckInServiceHolder(getInflatedView(R.layout.row_services, null));
                //checkInServiceHolder.setClickListener(clickInterface);
                //return checkInServiceHolder;
                break;
            case AppConstants.VIEW_TYPE.INFORMATION_DATA:
//                View view = inflater.inflate(R.layout.row_services, null);
//                CheckInInformationHolder informationHolder = new CheckInInformationHolder(view);
                holder = new CheckInInformationHolder(getInflatedView(R.layout.row_services, null));
                break;
            //informationHolder.setClickListener(clickInterface);
//                return informationHolder;
//            case AppConstants.VIEW_TYPE.STAY_SERVICE_DATA:
//                View view1 = inflater.inflate(R.layout.row_services, null);
//                StayServiceHolder stayServiceHolder = new StayServiceHolder(view1);
//                stayServiceHolder.setClickListener(clickInterface);
//                return stayServiceHolder;
            case AppConstants.VIEW_TYPE.STAYS:
                holder = new StaysHolder(getInflatedView(R.layout.view_upcoming_row, null));
                break;

            case AppConstants.VIEW_TYPE.CHAT_INCOMMING_MSG:
                holder = new MessageHolder(getInflatedView(R.layout.row_incomming_message, parent));
                break;

            case AppConstants.VIEW_TYPE.CHAT_OUTGOING_MSG:
                holder = new MessageHolder(getInflatedView(R.layout.row_outgoing_msg, parent));
                break;

        }
        if (listener != null) {
            holder.setClickListener(listener);
        }
        return holder;
    }

    private View getInflatedView(int layoutId, ViewGroup parent) {
        return inflater.inflate(layoutId, parent, false);
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        holder.onBind(position, this.list.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return 0;
    }

    public static interface RecyclerClickInterface {
        public void onItemClick(View itemView, int position, Object obj, int actionType);
    }

    public interface RecyclerClickListener {
        void onItemClicked(int position, Object model, int actionType);
    }
}
