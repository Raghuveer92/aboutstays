package com.aboutstays.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.activity.InternetActivity;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.internet.InternetPackList;

import java.util.ArrayList;

import simplifii.framework.utility.AppConstants;

/**
 * Created by neeraj on 18/3/17.
 */

public class InternetListAdapter extends BaseAdapter implements Filterable {

    private InternetActivity activity;
    private DataFilter dataFilter;
    private String staysId;
    private ArrayList<InternetPackList> internetList;
    private GeneralServiceModel generalServiceModel;
    private ArrayList<InternetPackList> filteredList = new ArrayList<>();

    /**
     * Initialize context variables
     * @param activity friend list activity
     * @param itemList friend list
     * @param model
     */
    public InternetListAdapter(InternetActivity activity, ArrayList<InternetPackList> itemList, GeneralServiceModel model) {
        this.activity = activity;
        this.internetList = itemList;
        this.filteredList = itemList;
        this.generalServiceModel = model;
        getFilter();
    }

    /**
     * Get size of user list
     * @return userList size
     */
    @Override
    public int getCount() {
        return filteredList.size();
    }

    /**
     * Get specific item from user list
     * @param i item index
     * @return list item
     */
    @Override
    public Object getItem(int i) {
        return filteredList.get(i);
    }

    /**
     * Get user list item id
     * @param i item index
     * @return current item id
     */
    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;

        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.row_internet, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final InternetPackList ob = filteredList.get(position);
        if (ob != null) {


            holder.textTitle.setText(ob.getTitle());
            holder.textOffer.setText(ob.getDescription());

            Boolean complementry = ob.getComplementry();
            if (complementry) {
                holder.textCost.setText("Free");
                holder.textButton.setText("GET");
            } else {
                holder.textCost.setText("\u20B9 " + ob.getPrice());
                holder.textButton.setText("BUY");
            }
        }

        holder.textButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.INTERNET_PACK_LIST, ob);
                FragmentContainerActivity.startActivity(activity, AppConstants.FRAGMENT_TYPE.RESERVATION_ORDER_FRAGMENT, bundle);
            }
        });
            return view;

    }
    /**
     * Get custom filter
     * @return filter
     */
    @Override
    public Filter getFilter() {
        if (dataFilter == null) {
            dataFilter = new DataFilter();
        }

        return dataFilter;
    }

    static class ViewHolder {
        TextView textTitle, textOffer, textCost, textButton;

        public ViewHolder(View view){
            textTitle = (TextView) view.findViewById(R.id.tv_title_internet);
            textOffer = (TextView) view.findViewById(R.id.tv_internet_speed);
            textCost = (TextView) view.findViewById(R.id.tv_internet_cost);
            textButton = (TextView) view.findViewById(R.id.tv_add);
        }
    }

    private class DataFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint!=null && constraint.length()>0) {
                ArrayList<InternetPackList> tempList = new ArrayList<InternetPackList>();

                for (InternetPackList item : internetList) {
                    if (item.getTitle().toLowerCase().contains(constraint.toString().toLowerCase())
                            || item.getDescription().toLowerCase().contains(constraint.toString().toLowerCase())){
                        tempList.add(item);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = internetList.size();
                filterResults.values = internetList;
            }

            return filterResults;
        }


        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredList = (ArrayList<InternetPackList>) results.values;
            notifyDataSetChanged();
        }
    }
}
