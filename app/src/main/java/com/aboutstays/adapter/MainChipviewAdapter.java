package com.aboutstays.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.GenralHotelInfoActivity;
import com.aboutstays.model.ActionTag;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajay on 24/12/16.
 */

public class MainChipviewAdapter extends ChipViewAdapter {


    private List<Chip> chipList;
    private Context context;
    private int listType;

    public MainChipviewAdapter(Context context, List<Chip> chipList, int listType) {
        super(context);

        this.context = context;
        this.chipList = new ArrayList<>(chipList);
        this.listType = listType;
    }

    @Override
    public int getLayoutRes(int position) {
        switch (listType) {
            case 0:
                return R.layout.view_chip;
            case 1:
                return R.layout.view_aminities;
            case 2:
                return R.layout.view_award_chip;
        }
        return 0;
    }

    @Override
    public int getBackgroundRes(int position) {
        switch (listType) {
            case 0:
                return R.drawable.shape_bg_chip;
            case 1:
                return R.color.white;
            case 2:
                return R.color.white;
            default:
                return 0;
        }
        // return R.drawable.shape_bg_chip;
    }

    @Override
    public int getBackgroundColor(int position) {

        switch (listType) {
            case 0:
            case 1:
                return R.color.white;
            case 2:
                return R.color.white;
            default:
                return R.color.white;
        }

    }

    @Override
    public int getBackgroundColorSelected(int position) {
        switch (listType) {
            case 0:
            case 1:
                return R.color.white;
            case 2:
                return R.color.white;
            default:
                return R.color.et_gray;
        }

    }

    @Override
    public void onLayout(View view, int position) {

        ImageView imageView;
        ActionTag tag = (ActionTag) chipList.get(position);
        switch (listType) {
            case 0:
                imageView = (ImageView) view.findViewById(R.id.iv_chip);
                Picasso.with(context).load(tag.getImageUrl())
                        .placeholder(R.drawable.progress_animation)
                        .error(R.mipmap.place_holder)
                        .into(imageView);
                break;
            case 1:
                imageView = (ImageView) view.findViewById(R.id.iv_aminitie);
                TextView tvAmenity = (TextView) view.findViewById(R.id.tv_aminitie);
                Picasso.with(context).load(tag.getImageUrl())
                        .placeholder(R.drawable.progress_animation)
                        .error(R.mipmap.place_holder)
                        .into(imageView);
                tvAmenity.setText(tag.getTitle());
                break;
            case 2:
                TextView tvTitle = (TextView) view.findViewById(R.id.tv_award);
                imageView = (ImageView) view.findViewById(R.id.iv_award);
                Picasso.with(context).load(tag.getImageUrl())
                        .placeholder(R.drawable.progress_animation)
                        .error(R.mipmap.place_holder)
                        .into(imageView);
                tvTitle.setText(tag.getTitle());
                break;
        }
    }
}
