package com.aboutstays.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.rest.response.ChildCategoryItem;
import com.aboutstays.rest.response.ParentCategoryItems;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by aman on 27/03/17.
 */

public class HouseRulesExpandableAdapter extends BaseExpandableListAdapter {

    private LinkedHashMap<String, List<ChildCategoryItem>> mapCategoryVsDescp;
    private Context context;
    private LayoutInflater layoutInflater;
    List<String> listKeys = new ArrayList<>();

    public HouseRulesExpandableAdapter(LinkedHashMap<String, List<ChildCategoryItem>> mapCategoryVsDescp, Context context) {
        this.mapCategoryVsDescp = mapCategoryVsDescp;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getGroupCount() {
        return mapCategoryVsDescp.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mapCategoryVsDescp.get(listKeys.get(groupPosition)).size();
    }

    @Override
    public String getGroup(int groupPosition) {
        listKeys.addAll(mapCategoryVsDescp.keySet());
        return listKeys.get(groupPosition);
    }

    @Override
    public ChildCategoryItem getChild(int groupPosition, int childPosition) {
        List<ChildCategoryItem> listCategoryItems = mapCategoryVsDescp.get(listKeys.get(groupPosition));
        if(listCategoryItems != null){
            return listCategoryItems.get(childPosition);
        }
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String title = getGroup(groupPosition);
        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.row_house_rules_group, null);
        }

        TextView groupTitle = (TextView) convertView.findViewById(R.id.tv_title);
        ImageView ivIndicator = (ImageView) convertView.findViewById(R.id.iv_indicator);
        groupTitle.setText(title);

        if(isExpanded){
            ivIndicator.setImageResource(R.drawable.up_arrow);
        } else{
            ivIndicator.setImageResource(R.drawable.down_arrow);

        }
        ivIndicator.setColorFilter(R.color.dark_red);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildCategoryItem cgItem =  getChild(groupPosition, childPosition);
        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.row_child_description, null);
        }
        TextView tvChildTitle = (TextView) convertView.findViewById(R.id.tv_child_title);
        if(cgItem != null){
            tvChildTitle.setText(cgItem.getTitle());
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
