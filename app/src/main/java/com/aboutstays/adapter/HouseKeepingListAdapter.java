package com.aboutstays.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.HouseKeepingItemActivity;
import com.aboutstays.model.housekeeping.HouseKeepingItemData;

import java.util.ArrayList;

/**
 * Created by neeraj on 17/3/17.
 */

public class HouseKeepingListAdapter extends BaseAdapter implements Filterable {

    private HouseKeepingItemActivity activity;
    private DataFilter dataFilter;
    private String staysId;
    private ArrayList<HouseKeepingItemData> houseKeepingList;
    private ArrayList<HouseKeepingItemData> filteredList = new ArrayList<>();

    /**
     * Initialize context variables
     * @param activity friend list activity
     * @param itemList friend list
     * @param staysId
     */
    public HouseKeepingListAdapter(HouseKeepingItemActivity activity, ArrayList<HouseKeepingItemData> itemList, String staysId) {
        this.activity = activity;
        this.houseKeepingList = itemList;
        this.filteredList = itemList;
        this.staysId = staysId;
        getFilter();
    }

    /**
     * Get size of user list
     * @return userList size
     */
    @Override
    public int getCount() {
        return filteredList.size();
    }

    /**
     * Get specific item from user list
     * @param i item index
     * @return list item
     */
    @Override
    public Object getItem(int i) {
        return filteredList.get(i);
    }

    /**
     * Get user list item id
     * @param i item index
     * @return current item id
     */
    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;

        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.row_laundry_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final HouseKeepingItemData ob = filteredList.get(position);
        if (ob != null) {


            holder.tvCount.setText(ob.getItemCount()+"");
            holder.tvTitle.setText(ob.getName());
            holder.tvTime.setVisibility(View.GONE);

            holder.tvPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int itemCount = ob.getItemCount();
                    ob.setItemCount(itemCount+1);
                    notifyDataSetChanged();
                    activity.addItemToCart();
                }
            });

            holder.tvMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int itemCount = ob.getItemCount();
                    if (itemCount >= 1) {
                        ob.setItemCount(itemCount - 1);
                    }
                    notifyDataSetChanged();
                    activity.addItemToCart();
                }
            });
        }

        holder.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ob.setItemCount(1);
                notifyDataSetChanged();
                activity.addItemToCart();
            }
        });

        if (ob.getItemCount() >= 1){
            holder.tvAdd.setVisibility(View.GONE);
            holder.rlCount.setVisibility(View.VISIBLE);
        } else {
            holder.tvAdd.setVisibility(View.VISIBLE);
            holder.rlCount.setVisibility(View.GONE);
        }

        holder.rlTitle.setVisibility(View.GONE);


        return view;
    }

    /**
     * Get custom filter
     * @return filter
     */
    @Override
    public Filter getFilter() {
        if (dataFilter == null) {
            dataFilter = new DataFilter();
        }

        return dataFilter;
    }

    static class ViewHolder {

        TextView tvTitle,tvTime,serviceDescription, tvAdd;
        TextView tvMinus, tvCount, tvPlus;
        ImageView imgVegNonveg, imgChilli, imgLike;
        RelativeLayout rlCount, rlTitle;

        public ViewHolder(View view){

            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            tvTime= (TextView) view.findViewById(R.id.tv_price);
            serviceDescription= (TextView) view.findViewById(R.id.tv_service_description);
            imgVegNonveg = (ImageView) view.findViewById(R.id.iv_veg_nonveg);
            imgChilli = (ImageView) view.findViewById(R.id.iv_chilli);
            imgLike = (ImageView) view.findViewById(R.id.iv_like);
            tvAdd = (TextView) view.findViewById(R.id.tv_add);
            tvMinus = (TextView) view.findViewById(R.id.tv_minus);
            tvCount = (TextView) view.findViewById(R.id.tv_count);
            tvPlus = (TextView) view.findViewById(R.id.tv_plus);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_add_count);
            rlTitle = (RelativeLayout) view.findViewById(R.id.rl_title);
        }

    }


    private class DataFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint!=null && constraint.length()>0) {
                ArrayList<HouseKeepingItemData> tempList = new ArrayList<HouseKeepingItemData>();

                for (HouseKeepingItemData item : houseKeepingList) {
                    if (item.getName().toLowerCase().contains(constraint.toString().toLowerCase())){
                        tempList.add(item);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = houseKeepingList.size();
                filterResults.values = houseKeepingList;
            }

            return filterResults;
        }


        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredList = (ArrayList<HouseKeepingItemData>) results.values;
            notifyDataSetChanged();
        }
    }

}
