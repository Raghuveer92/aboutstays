package com.aboutstays.adapter;

import android.content.Context;
import android.media.Image;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.rest.response.DateBasedExpenseItem;
import com.aboutstays.rest.response.DateWiseExpenseData;
import com.aboutstays.rest.response.TimeBasedExpenseItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeoutException;

import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

/**
 * Created by aman on 22/03/17.
 */

public class ExpendableListAdapter extends BaseExpandableListAdapter {

    private LinkedHashMap<String, DateBasedExpenseItem> dateVsExpenseItemMap;
    private Context context;
    private List<String> listDates = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private static final String EXPENDABLE_EXPENSE_GROUP_DATE_FORMAT = "dd MMM yyyy";
    private static final String EXPENDABLE_EXPENSE_API_DATE_FORMAT = "dd-MM-yyyy";

    public ExpendableListAdapter(LinkedHashMap<String, DateBasedExpenseItem> dateVsExpenseItemList, Context context) {
        this.dateVsExpenseItemMap = dateVsExpenseItemList;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        makeDateList();
    }


    public void makeDateList() {
        listDates.addAll(dateVsExpenseItemMap.keySet());
    }

    @Override
    public int getGroupCount() {
        return listDates.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        DateBasedExpenseItem dateBasedExpenseItem = dateVsExpenseItemMap.get(listDates.get(groupPosition));
        if(dateBasedExpenseItem != null && dateBasedExpenseItem.getListTimeBasedExpenseItem() != null){
            return dateBasedExpenseItem.getListTimeBasedExpenseItem().size();
        }
        return 0;
    }


    @Override
    public String getGroup(int groupPosition) {
        String key = listDates.get(groupPosition);
        String showDate = Util.convertDateFormat(key, EXPENDABLE_EXPENSE_API_DATE_FORMAT, EXPENDABLE_EXPENSE_GROUP_DATE_FORMAT);
        DateBasedExpenseItem dateBasedExpenseItem = dateVsExpenseItemMap.get(key);
//        String total = dateBasedExpenseItem.getDateWiseTotal() + "";
        String total = Util.formatDecimal(dateBasedExpenseItem.getDateWiseTotal(), "0.00#");
        return showDate + " - " + " \u20B9 " + total;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        DateBasedExpenseItem dateBasedExpenseItem = dateVsExpenseItemMap.get(listDates.get(groupPosition));
        List<TimeBasedExpenseItem> listTimeBasedExpenseItem = dateBasedExpenseItem.getListTimeBasedExpenseItem();
        if(listTimeBasedExpenseItem != null){
            return listTimeBasedExpenseItem.get(childPosition);
        }
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = getGroup(groupPosition);
        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.view_date_wise_expendable_list_group, null);
        }

        TextView tvGroupHeader = (TextView) convertView.findViewById(R.id.tv_title);
        ImageView ivIndicator = (ImageView) convertView.findViewById(R.id.iv_indicator);
        tvGroupHeader.setText(headerTitle);

        if(isExpanded){
            ivIndicator.setImageResource(R.drawable.up_arrow);
        } else{
            ivIndicator.setImageResource(R.drawable.down_arrow);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        TimeBasedExpenseItem timeBasedExpenseItem = (TimeBasedExpenseItem) getChild(groupPosition, childPosition);
        if(convertView  == null){
            convertView = layoutInflater.inflate(R.layout.row_date_wise, null);
        }
        ImageView ivIcon = (ImageView) convertView.findViewById(R.id.iv_title_image1);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_title);
        TextView tvAmount = (TextView) convertView.findViewById(R.id.tv_amount);
        TextView tvTime = (TextView) convertView.findViewById(R.id.tv_time);

        if(timeBasedExpenseItem != null){
            tvName.setText(timeBasedExpenseItem.getName());
            String decimal = Util.formatDecimal(timeBasedExpenseItem.getTotal(), "0.00#");
            tvAmount.setText("\u20B9 "+decimal);
            if(!TextUtils.isEmpty(timeBasedExpenseItem.getIconUrl())) {
                Picasso.with(context).load(timeBasedExpenseItem.getIconUrl())
                        .into(ivIcon);
            }
            String createdDate = Util.format(new Date(timeBasedExpenseItem.getCreated()), Util.CHECK_IN_TIME_UI_FORMAT);
            tvTime.setText(createdDate);
        } else{

        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
