package com.aboutstays.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FoodItemActivity;
import com.aboutstays.activity.RoomServiceFoodItemActivity;
import com.aboutstays.enums.FoodLabels;
import com.aboutstays.enums.SpicyType;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.cart.BaseAddToCart;
import com.aboutstays.model.cart.FoodItemOrderDetailList;
import com.aboutstays.model.cart.GetCartData;
import com.aboutstays.model.food.FoodDataByType;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by neeraj on 9/3/17.
 */

public class RoomServiceListAdapter extends BaseAdapter implements Filterable{

    private RoomServiceFoodItemActivity activity;
    private DataFilter dataFilter;
    private BaseAddToCart addToCartFoodData = new BaseAddToCart();
    private String staysId;
    //    private Typeface typeface;
    private ArrayList<FoodDataByType> foodList;
    private ArrayList<FoodDataByType> filteredList = new ArrayList<>();
    private GetCartData cartData = new GetCartData();
    /**
     * Initialize context variables
     * @param activity friend list activity
     * @param foodItemList friend list
     * @param staysId
     * @param cartData
     */
    public RoomServiceListAdapter(RoomServiceFoodItemActivity activity, ArrayList<FoodDataByType> foodItemList, String staysId, GetCartData cartData) {
        this.activity = activity;
        this.foodList = foodItemList;
        this.filteredList = foodItemList;
        //this.staysId = staysId;
        //this.cartData = cartData;
        getFilter();

    }

//    private void cartItemMapList() {
//        if (cartData!= null){
//            List<FoodItemOrderDetailList> foodItemOrderDetailList = cartData.getFoodItemOrderDetailList();
//            if (CollectionUtils.isNotEmpty(foodItemOrderDetailList)){
//                for (FoodItemOrderDetailList detailList : foodItemOrderDetailList){
//                    for (FoodDataByType dataByType : filteredList){
//                        if (detailList.getFoodItemId().equals(dataByType.getFooditemId())){
//                            detailList.setQuantity(dataByType.getItemCount());
//                        }
//                    }
//                    foodItemOrderDetailList.add(detailList);
//                    cartData.setFoodItemOrderDetailList(foodItemOrderDetailList);
//                }
//
//            }
//        }
//    }

    /**
     * Get size of user list
     * @return userList size
     */
    @Override
    public int getCount() {
        return filteredList.size();
    }

    /**
     * Get specific item from user list
     * @param i item index
     * @return list item
     */
    @Override
    public Object getItem(int i) {
        return filteredList.get(i);
    }

    /**
     * Get user list item id
     * @param i item index
     * @return current item id
     */
    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.row_search_services, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            // get view holder back
            holder = (ViewHolder) view.getTag();
        }

        final FoodDataByType ob = filteredList.get(position);

            Boolean recommended = ob.getRecommended();

            holder.tvCount.setText(ob.getItemCount()+"");
            holder.serviceTitle.setText(ob.getName());
            holder.serviceDescription.setText(ob.getDescription());
            Double price = ob.getPrice();
            if (price != null){
                holder.serviceCost.setText("\u20B9 " +price);
            }

            Integer foodLabel = ob.getFoodLabel();
            if (foodLabel != 0 && foodLabel < 9) {
                FoodLabels statusByCode = FoodLabels.findStatusByCode(foodLabel);
                if (statusByCode != null) {
                    int vegUrl = statusByCode.getImageUrl();
                    holder.imgVegNonveg.setImageResource(vegUrl);
                    holder.imgVegNonveg.setVisibility(View.VISIBLE);
                }
            } else {
                holder.imgVegNonveg.setVisibility(View.GONE);
            }

            holder.tvPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int itemCount = ob.getItemCount();
                    ob.setItemCount(itemCount+1);
                    notifyDataSetChanged();
                    activity.addItemToCart();
                }
            });

            holder.tvMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int itemCount = ob.getItemCount();
                    if (itemCount >= 1) {
                        ob.setItemCount(itemCount - 1);
                    }
                    notifyDataSetChanged();
                    activity.addItemToCart();
                }
            });

            Integer spicyType = ob.getSpicyType();
            if (spicyType != 0 && spicyType < 9) {
                SpicyType statusByCode1 = SpicyType.findStatusByCode(spicyType);
                if (statusByCode1 != null) {
                    int spicyUrl = statusByCode1.getImageUrl();
                    holder.imgChilli.setImageResource(spicyUrl);
                    holder.imgChilli.setVisibility(View.VISIBLE);
                }
            } else {
                holder.imgChilli.setVisibility(View.GONE);
            }

            if (recommended == true){
                holder.imgLike.setVisibility(View.VISIBLE);
            } else {
                holder.imgLike.setVisibility(View.GONE);
            }

        holder.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ob.setItemCount(1);
                notifyDataSetChanged();
                activity.addItemToCart();
            }
        });

        if (ob.getItemCount()>0){
            holder.tvAdd.setVisibility(View.GONE);
            holder.rlCount.setVisibility(View.VISIBLE);
        } else {
            holder.tvAdd.setVisibility(View.VISIBLE);
            holder.rlCount.setVisibility(View.GONE);
        }

        /*view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                cartItemMapList();
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.FOOD_ITEM_DETAILS, ob);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.CART_DATA, cartData);
                bundle.putString(AppConstants.BUNDLE_KEYS.STAYS_ID, staysId);
                Intent intent = new Intent(activity, FoodItemActivity.class);
                intent.putExtras(bundle);
                activity.startActivityForResult(intent, AppConstants.REQUEST_CODE.SHOW_FOOD_DETAIL);
//                activity.startActivity(intent);

            }
        });*/

        return view;
    }

    /**
     * Get custom filter
     * @return filter
     */
    @Override
    public Filter getFilter() {
        if (dataFilter == null) {
            dataFilter = new DataFilter();
        }

        return dataFilter;
    }

    /**
     * Keep reference to children view to avoid unnecessary calls
     */
    static class ViewHolder {

        TextView serviceTitle,serviceCost,serviceDescription, tvAdd;
        TextView tvMinus, tvCount, tvPlus;
        ImageView imgVegNonveg, imgChilli, imgLike;
        RelativeLayout rlCount;

        public ViewHolder(View view){

            serviceTitle = (TextView) view.findViewById(R.id.tv_service_title);
            serviceCost= (TextView) view.findViewById(R.id.tv_service_cost);
            serviceDescription= (TextView) view.findViewById(R.id.tv_service_description);
            imgVegNonveg = (ImageView) view.findViewById(R.id.iv_veg_nonveg);
            imgChilli = (ImageView) view.findViewById(R.id.iv_chilli);
            imgLike = (ImageView) view.findViewById(R.id.iv_like);
            tvAdd = (TextView) view.findViewById(R.id.tv_add);
            tvMinus = (TextView) view.findViewById(R.id.tv_minus);
            tvCount = (TextView) view.findViewById(R.id.tv_count);
            tvPlus = (TextView) view.findViewById(R.id.tv_plus);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_add_count);
        }

    }

    /**
     * Custom filter for friend list
     * Filter content in friend list according to the search text
     */
    private class DataFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint!=null && constraint.length()>0) {
                ArrayList<FoodDataByType> tempList = new ArrayList<FoodDataByType>();

                for (FoodDataByType user : foodList) {
                    if (user.getName().toLowerCase().contains(constraint.toString().toLowerCase())
                            || user.getCuisineType().toLowerCase().contains(constraint.toString().toLowerCase())
                            || user.getDescription().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(user);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = foodList.size();
                filterResults.values = foodList;
            }

            return filterResults;
        }


        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredList = (ArrayList<FoodDataByType>) results.values;
            notifyDataSetChanged();
        }
    }

}
