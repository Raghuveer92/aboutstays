package com.aboutstays.utitlity;

import android.os.Bundle;
import android.text.TextUtils;

import com.aboutstays.model.DeleteIdPojo;
import com.aboutstays.model.booking.GetBookingApi;
import com.aboutstays.model.cart.RoomServiceCartResponse;
import com.aboutstays.model.cart.HouseKeepingCartResponse;
import com.aboutstays.model.cart.LaundryCartResponse;
import com.aboutstays.model.cart.HouseKeepingCart;
import com.aboutstays.model.cart.LaundryCart;
import com.aboutstays.model.cart.RoomServiceCart;
import com.aboutstays.model.common.FetchCommonData;
import com.aboutstays.model.common.FetchIdentityDocsType;
import com.aboutstays.model.common.FetchProfileTitles;
import com.aboutstays.model.commute.CommuteResponceApi;
import com.aboutstays.model.contactUpdate.ContactUpdate;
import com.aboutstays.model.earlycheckIn.EarlyCheckInResponse;
import com.aboutstays.model.entertainment.AllEntertainmentResponseApi;
import com.aboutstays.model.entertainment.EntertainmentResponseApi;
import com.aboutstays.model.feedback.GetServiceFeedbackResponse;
import com.aboutstays.model.food.GetFoodItemsApi;
import com.aboutstays.model.food.GetFoodItemsbyRoomServiceApi;
import com.aboutstays.model.hotels.GetAllHotelsApi;
import com.aboutstays.model.hotels.GetHotelById;
import com.aboutstays.model.housekeeping.HouseKeepingItemDataResponce;
import com.aboutstays.model.housekeeping.HouseKeepingServiceItem;
import com.aboutstays.model.insiate_checkout.IniciateCheckoutResponse;
import com.aboutstays.model.initiate_checkIn.CheckInData;
import com.aboutstays.model.initiate_checkIn.InitiateCheckInResponse;
import com.aboutstays.model.internet.InternetServiceApi;
import com.aboutstays.model.laundry.LaundryApiResponse;
import com.aboutstays.model.laundry.LaundryItemResponceApi;
import com.aboutstays.model.maintenance.MaintenanceDataResponce;
import com.aboutstays.model.maintenance.MaintenanceItemResponse;
import com.aboutstays.model.message.AddMessage;
import com.aboutstays.model.message.MessageApiResponse;
import com.aboutstays.model.notification.NotificationResponseApi;
import com.aboutstays.model.reservation.ReservationResponseApi;
import com.aboutstays.model.reservation.ReservationTypeResponseApi;
import com.aboutstays.model.roomService.RoomServiceResponse;
import com.aboutstays.model.rooms_category.GetAllRoomCategories;
import com.aboutstays.model.search.SearchResponseApi;
import com.aboutstays.model.stayline.AddtoStaylineApi;
import com.aboutstays.model.stayline.GetStayApi;
import com.aboutstays.model.stayline.SearchedHotelData;
import com.aboutstays.rest.request.BaseServiceRequest;
import com.aboutstays.rest.request.FeedbackAndSuggestionsRequest;
import com.aboutstays.rest.request.FetchReservationRequest;
import com.aboutstays.rest.request.InternetOrderRequest;
import com.aboutstays.rest.request.LateCheckoutRequest;
import com.aboutstays.rest.request.MaintenanceRequest;
import com.aboutstays.rest.request.CommuteRequest;
import com.aboutstays.rest.request.EditOrderRequest;
import com.aboutstays.rest.request.GenerateExpenseDashboardRequest;
import com.aboutstays.rest.request.GetAirportBookingData;
import com.aboutstays.rest.request.GetHotelBookingRequest;
import com.aboutstays.rest.request.ReservationRequestOrder;
import com.aboutstays.rest.request.InitiateAirportPickupRequest;
import com.aboutstays.rest.request.RequestDashboardRequest;
import com.aboutstays.rest.request.SubmittFeedbackRequest;
import com.aboutstays.rest.request.SuggestHotelRequest;
import com.aboutstays.rest.response.DashboardOrderDetailsApiResponce;
import com.aboutstays.rest.response.GenerateOtpApi;
import com.aboutstays.rest.response.GetAirportServiceResponse;
import com.aboutstays.rest.response.GetAllServicesResponse;
import com.aboutstays.rest.response.GetCategoryBaseExpenseResponse;
import com.aboutstays.rest.response.GetCityGuideItemResponse;
import com.aboutstays.rest.response.GetCityGuideResponse;
import com.aboutstays.rest.response.GetComplementaryServiceResponse;
import com.aboutstays.rest.response.GetDateWiseExpenseResponse;
import com.aboutstays.rest.response.GetHotelGeneralInfoResponse;
import com.aboutstays.rest.response.GetHotelGeneralServicesResponse;
import com.aboutstays.rest.response.GetHouseRulesResponse;
import com.aboutstays.rest.response.GetLateCheckoutServiceResponse;
import com.aboutstays.rest.response.GetRequestDashboardResponse;
import com.aboutstays.rest.response.GetStaysFeedbackResponse;
import com.aboutstays.rest.response.GetUserOptedServicesResponse;
import com.aboutstays.rest.response.InitiateAirportPickupResponse;
import com.aboutstays.rest.response.RetreiveLateCheckoutResponse;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.requests.ForgotPasswordRequest;
import simplifii.framework.rest.response.requests.LoginRequest;
import simplifii.framework.rest.response.requests.SignupRequest;
import simplifii.framework.rest.response.requests.UpdatePasswordRequest;
import simplifii.framework.rest.response.requests.UpdatePersonalDetailsRequest;
import simplifii.framework.rest.response.requests.UpdateStayPreferences;
import simplifii.framework.rest.response.requests.ValidateOtpRequest;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.response.GetOtpResponse;
import simplifii.framework.rest.response.response.GetUserResponse;
import simplifii.framework.rest.response.response.LoginResponse;
import simplifii.framework.rest.response.response.PreferanceResponceApi;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.JsonUtil;

/**
 * Created by aman on 14/01/17.
 */

public class ApiRequestGenerator {



    public interface API_PARAMS {

        String TYPE = "type";

        String VALUE = "value";

        String USER_ID = "userId";
        String HOTEL_ID = "hotelId";
        String STAY_ID = "stayId";
        String STAY_STATUS = "stayStatus";
        String ID = "id";
        String RS_ID = "roomServicesId";
        String QUERY = "query";
        String LSI = "laundryServicesId";
        String STAYS_ID = "staysId";
        String CART_TYPE = "cartType";
        String HK_SERVICE_ID = "hkServicesId";
        String IS_GI = "isGI";
        String CGID = "cgId";
        String RsId = "rsId";
        String CG_TYPE = "type";
        String UOSID = "uosId";
        String MT_SERVICE_ID = "mtServicesId";
        String RESERVATION_ID = "rsId";
        String CATEGORY = "category";
        String PARENT_ID = "parentId";
        String ROOM_CATEGORY_ID = "roomCategoryId";
        String GENERID = "genereId";
        String ENT_SERVICE_ID = "entServiceId";
    }
    public static HttpParamObject validateSignup(String email, String firstName, String lastName, String number, String password, String referralCode, Integer signupType) {
        SignupRequest request = new SignupRequest(firstName, lastName, number, password, email, referralCode, signupType);
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.VALIDATE_SIGNUP);
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setPostMethod();
        return httpParamObject;
    }

    public static HttpParamObject signup(Bundle bundle, boolean otpValidated) {
        SignupRequest request = new SignupRequest();
        request.setEmailId(bundle.getString(AppConstants.BUNDLE_KEYS.EMAIL));
        request.setFirstName(bundle.getString(AppConstants.BUNDLE_KEYS.FIRST_NAME));
        request.setLastName(bundle.getString(AppConstants.BUNDLE_KEYS.LAST_NAME));
        request.setNumber(bundle.getString(AppConstants.BUNDLE_KEYS.NUMBER));
        request.setPassword(bundle.getString(AppConstants.BUNDLE_KEYS.PASSWORD));
        request.setReferralCode(bundle.getString(AppConstants.BUNDLE_KEYS.REFERRAL_CODE));
        request.setSignupType(bundle.getInt(AppConstants.BUNDLE_KEYS.SIGNUP_TYPE));
        request.setOtpValidated(otpValidated);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SIGNUP);
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setJSONContentType();
        return httpParamObject;
    }

    public static HttpParamObject getOtp(Integer type, String value) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_OTP);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(GetOtpResponse.class);
        httpParamObject.addParameter(API_PARAMS.TYPE, type.toString());
        httpParamObject.addParameter(API_PARAMS.VALUE, value);
        return httpParamObject;
    }

    public static HttpParamObject updateOtpField(String type, String value, boolean validated) {
        ValidateOtpRequest request = new ValidateOtpRequest(type, value, validated);
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.VALIDATE_OTP);
        httpParamObject.setPatchMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(request));
        return httpParamObject;
    }

    public static HttpParamObject login(Integer type, String value, String password, Integer loginType, SignupRequest request) {
        LoginRequest loginRequest = new LoginRequest(type, value, password, loginType, request);
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.LOGIN);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(LoginResponse.class);
        httpParamObject.setJson(JsonUtil.toJson(loginRequest));
        return httpParamObject;
    }

    public static HttpParamObject updatePassword(Integer type, String value, String password) {
        UpdatePasswordRequest request = new UpdatePasswordRequest(type, value, password);
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.UPDATE_PASS);
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setPostMethod();
        return httpParamObject;
    }

    public static HttpParamObject forgotPassword(Integer type, String value) {
        ForgotPasswordRequest request = new ForgotPasswordRequest();
        request.setType(type);
        request.setValue(value);

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.FORGOT_PASS);
        httpParamObject.setJSONContentType();
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        return httpParamObject;
    }

    public static HttpParamObject getAllHotels() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_ALL_HOTELS);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(GetAllHotelsApi.class);
        return httpParamObject;
    }

    public static HttpParamObject getRoomsbyId(String hotelId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_ROOM_CATEGORIES_BY_HOTEL_ID);
        httpParamObject.addParameter("hotelId",hotelId);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(GetAllRoomCategories.class);
        return httpParamObject;
    }


    public static HttpParamObject updatePersonalDetails(String title ,String firstName, String lastName, String dob, Integer maritalStatus, String anniversaryDate, String userId) {
        UpdatePersonalDetailsRequest request = new UpdatePersonalDetailsRequest(title, firstName, lastName, dob, maritalStatus, anniversaryDate, userId);
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.UPDATE_PERSONAL_DETAILS);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(request));
        return httpParamObject;
    }


    public static HttpParamObject getBookingHoteldata(String cityName, String hotelName) {

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_HOTELS_BY_NAME_CITY);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("city", cityName);
            jsonObject.put("name", hotelName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        httpParamObject.setPostMethod();
        httpParamObject.setClassType(SearchedHotelData.class);
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(jsonObject.toString());
        return httpParamObject;
    }

    public static HttpParamObject getBookingData(String revNum, String checkindate, String hotelId, String name, String userId) {
        GetHotelBookingRequest request = new GetHotelBookingRequest();
        request.setHotelId(hotelId);
        request.setCheckInDate(checkindate);
        request.setReservationNumber(revNum);
        request.setName(name);
        request.setUserId(userId);
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_BOOKING_DATA);
        httpParamObject.setPostMethod();
        httpParamObject.setClassType(GetBookingApi.class);
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(request));
        return httpParamObject;
    }

    public static HttpParamObject getInitiateChoutData(String userId, String staysId, String hotelId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_INICIATE_CHECKOUT_DATA);
        httpParamObject.setPatchMethod();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("hotelId", hotelId);
            jsonObject.put("userId", userId);
            jsonObject.put("staysId", staysId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(IniciateCheckoutResponse.class);
        return httpParamObject;
    }
    public static HttpParamObject postIniciateCheckout(String userId, String staysId, String hotelId,int integer) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.POST_INICIATE_DATA);
        httpParamObject.setPatchMethod();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("hotelId", hotelId);
            jsonObject.put("userId", userId);
            jsonObject.put("staysId", staysId);
            jsonObject.put("paymentMethod", integer);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject addToStayline(String hotelId, String roomId, String userId, String bookingId,
                                                String checkinDate, String checkoutDate, String checkInTime, String checkOutTime, boolean checked) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.ADD_TO_STAYLINE);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("bookingId", bookingId);
            jsonObject.put("hotelId", hotelId);
            jsonObject.put("roomId", roomId);
            jsonObject.put("userId", userId);
            jsonObject.put("checkInDate", checkinDate);
            jsonObject.put("checkOutDate", checkoutDate);
            jsonObject.put("checkInTime", checkInTime);
            jsonObject.put("checkOutTime", checkOutTime);
            jsonObject.put("official", checked);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setPostMethod();
        httpParamObject.setClassType(AddtoStaylineApi.class);
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(jsonObject.toString());
        return httpParamObject;
    }

    public static HttpParamObject getUSerStayline(String userId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_USER_ALL_STAYS);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.USER_ID, userId);
        httpParamObject.setClassType(GetStayApi.class);
        return httpParamObject;
    }

    public static HttpParamObject updateContact(ContactUpdate contactUpdate) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.UPDATE_CONTACT);
        httpParamObject.setPostMethod();
        httpParamObject.setClassType(BaseApiResponse.class);
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(contactUpdate));
        return httpParamObject;
    }

    public static HttpParamObject updateStayPreference(UpdateStayPreferences updateStayPreferences) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.UPDATE_PREFERENCE);
        httpParamObject.setPostMethod();
        httpParamObject.setClassType(BaseApiResponse.class);
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(updateStayPreferences));
        return httpParamObject;
    }

    public static HttpParamObject getUserByUserId(String userId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_USER);
        httpParamObject.addParameter(API_PARAMS.USER_ID, userId);
        httpParamObject.setClassType(GetUserResponse.class);
        httpParamObject.setJSONContentType();
        return httpParamObject;
    }

    public static HttpParamObject getAllService(String hotelId, String status, String isGI) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_SERVICE);
        httpParamObject.addParameter(API_PARAMS.HOTEL_ID, hotelId);
        httpParamObject.addParameter(API_PARAMS.STAY_STATUS, status);
        httpParamObject.addParameter(API_PARAMS.IS_GI, isGI);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(GetHotelGeneralServicesResponse.class);
        return httpParamObject;
    }

//    public static HttpParamObject getUserOptedServices(String userId, String stayId) {
//        GetUserOptedServicesRequest request = new GetUserOptedServicesRequest();
//        request.setUserId(userId);
//        request.setStayId(stayId);
//        HttpParamObject httpParamObject = new HttpParamObject();
//        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_USER_OPTED_SERVICES);
//        httpParamObject.setJSONContentType();
//        httpParamObject.setPostMethod();
//        httpParamObject.setJson(JsonUtil.toJson(request));
//        httpParamObject.setClassType(GetUserOptedServicesResponse.class);
//        return httpParamObject;
//    }

        public static HttpParamObject getUserOptedServices(BaseServiceRequest request) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_USER_OPTED_SERVICES);
        httpParamObject.setJSONContentType();
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(GetUserOptedServicesResponse.class);
        return httpParamObject;
    }


    public static HttpParamObject getHotelGeneralInfoById(String hotelId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_HOTEL_GENERAL_INFO);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.HOTEL_ID, hotelId);
        httpParamObject.setClassType(GetHotelGeneralInfoResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getSupportedAirportList(String serviceId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_AIRPORT_SERVICE);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, serviceId);
        httpParamObject.setClassType(GetAirportServiceResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getAirportBookingData(String refId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_AIRPORT_BOOKING);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, refId);
        httpParamObject.setClassType(GetAirportBookingData.class);
        return httpParamObject;
    }

    public static HttpParamObject requestAirportPickup(InitiateAirportPickupRequest request) {
        if (request == null) {
            return null;
        }
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.INITIATE_PICKUP);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(InitiateAirportPickupResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject updateAirportPickup(String airportPickupId, InitiateAirportPickupRequest request, Integer pickupStatus) {
        if (request == null) {
            return null;
        }
        request.setGsId(null);
        request.setId(airportPickupId);
        request.setPickupStatus(pickupStatus);
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.UPDATE_PICKUP);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getSupportedRoomServiceList(String serviceId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_ROOM_SERVICE);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, serviceId);
        httpParamObject.setClassType(RoomServiceResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getAllFoodItemmsbyRoomService(String serviceId, Integer menuItemType) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_FOOD_ITEM);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.TYPE, menuItemType.toString());
        httpParamObject.addParameter(API_PARAMS.RS_ID, serviceId);
        httpParamObject.setClassType(GetFoodItemsbyRoomServiceApi.class);
        return httpParamObject;
    }

    public static HttpParamObject searchFoodItemmsbyRoomService(String serviceId, String query) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SEARCH_FOOD_ITEM);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, serviceId);
        httpParamObject.addParameter(API_PARAMS.QUERY, query);
        httpParamObject.setClassType(SearchResponseApi.class);
        return httpParamObject;
    }

    public static HttpParamObject getFoodItemmsById(String foodItemId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_FOOD_BYID);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, foodItemId);
        httpParamObject.setClassType(GetFoodItemsApi.class);
        return httpParamObject;
    }

    public static HttpParamObject getLaundryById(String serviceId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_LAUNDRY_BYID);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, serviceId);
        httpParamObject.setClassType(LaundryApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getLaundryItemById(String serviceId, String laundryType) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_LAUNDRY_ITEM_BYID);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter("laundryType", laundryType);
        httpParamObject.addParameter(API_PARAMS.LSI, serviceId);
        httpParamObject.setClassType(LaundryItemResponceApi.class);
        return httpParamObject;
    }

    public static HttpParamObject getLaundryItemBySearch(String serviceId, String query) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_LAUNDRY_ITEM_BY_SEARCH);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.QUERY, query);
        httpParamObject.addParameter(API_PARAMS.LSI, serviceId);
        httpParamObject.setClassType(LaundryItemResponceApi.class);
        return httpParamObject;
    }

    public static HttpParamObject addOrRemoveFoodItem(RoomServiceCart cartFoodData) {
        if (cartFoodData == null) {
            return null;
        }
//        JsonArray result = (JsonArray) new Gson().toJsonTree(cartFoodDataList,
//                new TypeToken<List<BaseAddToCart>>() {
//                }.getType());

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.ADD_DELETE_TO_CART);
        httpParamObject.setPatchMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(cartFoodData));
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }


    public static HttpParamObject getAllRoomServiceCartItem(String userId, String staysId, String hotelId, String cartType) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_CART_DETAILS);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.USER_ID, userId);
        httpParamObject.addParameter(API_PARAMS.STAYS_ID, staysId);
        httpParamObject.addParameter(API_PARAMS.HOTEL_ID, hotelId);
        httpParamObject.addParameter(API_PARAMS.CART_TYPE, cartType);
        httpParamObject.setClassType(RoomServiceCartResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getAllCartItem(String userId, String staysId, String hotelId, String cartType) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_CART_DETAILS);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.USER_ID, userId);
        httpParamObject.addParameter(API_PARAMS.STAYS_ID, staysId);
        httpParamObject.addParameter(API_PARAMS.HOTEL_ID, hotelId);
        httpParamObject.addParameter(API_PARAMS.CART_TYPE, cartType);
        if (cartType != null) {
            switch (cartType) {
                case AppConstants.STAYS_TYPE.HOUSE_KEEPING:
                    httpParamObject.setClassType(HouseKeepingCartResponse.class);
                    break;
                case AppConstants.STAYS_TYPE.LAUNDRY:
                    httpParamObject.setClassType(LaundryCartResponse.class);
                    break;
                case AppConstants.STAYS_TYPE.ROOM_SERVICE:
                    httpParamObject.setClassType(RoomServiceCartResponse.class);
                    break;
            }
        }
        return httpParamObject;
    }


    public static HttpParamObject getHouseKeepingList(String serviceId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_HOUSEKEEPING_SERVICE);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, serviceId);
        httpParamObject.setClassType(HouseKeepingServiceItem.class);
        return httpParamObject;
    }

    public static HttpParamObject getAllFoodItemmsbyHousekeeping(String hotelServiceId, Integer itemType) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_HOUSEKEEPING_ITEM);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.TYPE, itemType.toString());
        httpParamObject.addParameter(API_PARAMS.HK_SERVICE_ID, hotelServiceId);
        httpParamObject.setClassType(HouseKeepingItemDataResponce.class);
        return httpParamObject;
    }

    public static HttpParamObject searchHouseKeepingItem(String serviceId, String query) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SEARCH_HOUSEKEEPING_ITEM);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, serviceId);
        httpParamObject.addParameter(API_PARAMS.QUERY, query);
        httpParamObject.setClassType(HouseKeepingItemDataResponce.class);
        return httpParamObject;
    }

    public static HttpParamObject getInternetService(String serviceId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_INTERNET_SERVICE);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, serviceId);
        httpParamObject.setClassType(InternetServiceApi.class);
        return httpParamObject;
    }

    public static HttpParamObject placeOrder(String gsId, String staysId, String hotelId, String userId, Integer cartType, long pickupTime, boolean reqNow, String comment, int laundryPrefs, String deliveryDate) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.PLACE_CART_ORDER);
        httpParamObject.setPostMethod();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", userId);
            jsonObject.put("staysId", staysId);
            jsonObject.put("hotelId", hotelId);
            jsonObject.put("cartType", cartType);
            jsonObject.put("gsId", gsId);
            jsonObject.put("deliveryTime", pickupTime);
            jsonObject.put("comment", comment);
            jsonObject.put("requestedNow", reqNow);
            jsonObject.put("deliverTimeString", deliveryDate);
            if (laundryPrefs != 0) {
                jsonObject.put("laundryPrefs", laundryPrefs);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject addOrRemoveLaundryItem(LaundryCart laundryCart) {
        if (laundryCart == null) {
            return null;
        }
//        JsonArray result = (JsonArray) new Gson().toJsonTree(cartFoodDataList,
//                new TypeToken<List<BaseAddToCart>>() {
//                }.getType());

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.ADD_DELETE_TO_LAUNDRY_CART);
        httpParamObject.setPatchMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(laundryCart));
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }


    public static HttpParamObject getAllLaundryCartItem(String userId, String staysId, String hotelId, String cartType) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_CART_DETAILS);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.USER_ID, userId);
        httpParamObject.addParameter(API_PARAMS.STAYS_ID, staysId);
        httpParamObject.addParameter(API_PARAMS.HOTEL_ID, hotelId);
        httpParamObject.addParameter(API_PARAMS.CART_TYPE, cartType);
        httpParamObject.setClassType(LaundryCartResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getCategoryBasedExpenses(GenerateExpenseDashboardRequest request) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_CATEGORY_BASED_EXPENSE);
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(GetCategoryBaseExpenseResponse.class);
        httpParamObject.setPostMethod();
        return httpParamObject;
    }

    public static HttpParamObject getDateWiseExpense(GenerateExpenseDashboardRequest request) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_DATE_WISE_EXPENSE);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(GetDateWiseExpenseResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject addOrRemoveHouseKeepingItem(HouseKeepingCart houseKeepingCart) {
        if (houseKeepingCart == null) {
            return null;
        }
//        JsonArray result = (JsonArray) new Gson().toJsonTree(cartFoodDataList,
//                new TypeToken<List<BaseAddToCart>>() {
//                }.getType());

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.ADD_DELETE_TO_HOUSEKEEPING_CART);
        httpParamObject.setPatchMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(houseKeepingCart));
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }


    public static HttpParamObject getAllHouseKeepingCartItem(String userId, String staysId, String hotelId, String cartType) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_CART_DETAILS);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.USER_ID, userId);
        httpParamObject.addParameter(API_PARAMS.STAYS_ID, staysId);
        httpParamObject.addParameter(API_PARAMS.HOTEL_ID, hotelId);
        httpParamObject.addParameter(API_PARAMS.CART_TYPE, cartType);
        httpParamObject.setClassType(HouseKeepingCartResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getHotelsById(String hotelId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_HOTELS_BY_ID);
        httpParamObject.addParameter(API_PARAMS.HOTEL_ID, hotelId);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(GetHotelById.class);
        return httpParamObject;
    }

    public static HttpParamObject getCityGuide(String cgId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_CITY_GUIDE);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, cgId);
        httpParamObject.setClassType(GetCityGuideResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject fetchCityGuideItemByType(String cgId, int cgType) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.FETCH_CITY_GUIDE_ITEM);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.CGID, cgId);
        httpParamObject.addParameter(API_PARAMS.CG_TYPE, cgType + "");
        httpParamObject.setClassType(GetCityGuideItemResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getHouseRules(String houseRuleServiceId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_HOUSE_RULES);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, houseRuleServiceId);
        httpParamObject.setClassType(GetHouseRulesResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getRequestDashboard(RequestDashboardRequest request) {
        if (request == null) {
            return null;
        }
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_REQUEST_DASHBOARD);
        httpParamObject.setJSONContentType();
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(GetRequestDashboardResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getOrderRequestData(String uosId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_REQUEST_DASHBOARD_DETAIL);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.UOSID, uosId);
        httpParamObject.setClassType(DashboardOrderDetailsApiResponce.class);
        return httpParamObject;
    }

    public static HttpParamObject cancelOrder(String uosId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.CANCEL_ORDER);
        httpParamObject.setPatchMethod();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(API_PARAMS.UOSID, uosId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject cancelAirportPickup(String uosId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.CANCEL_AIRPORT_PICKUP);
        httpParamObject.setPatchMethod();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(API_PARAMS.UOSID, uosId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject editOrderDetails(EditOrderRequest request) {
        if (request == null) {
            return null;
        }
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.EDIT_ORDER_DETAILS);
        httpParamObject.setJSONContentType();
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getCommuteData(String hsID) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_COMMUTE);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, hsID);
        httpParamObject.setClassType(CommuteResponceApi.class);
        return httpParamObject;
    }

    public static HttpParamObject requestBellBoy(MaintenanceRequest request) {
        if (request == null) {
            return null;
        }
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.REQUEST_BELLBOY);
        httpParamObject.setJSONContentType();
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getMaintenance(String hsID) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_MAINTENANCE);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, hsID);
        httpParamObject.setClassType(MaintenanceDataResponce.class);
        return httpParamObject;
    }

    public static HttpParamObject orderCommute(CommuteRequest request) {
        if (request == null) {
            return null;
        }
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.ORDER_COMMUTE);
        httpParamObject.setJSONContentType();
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getMaintenanceItem(String hsID, int type) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_MAINTENANCE_ITEM);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.MT_SERVICE_ID, hsID);
        httpParamObject.addParameter(API_PARAMS.TYPE, type + "");
        httpParamObject.setClassType(MaintenanceItemResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject requestMaintenance(MaintenanceRequest request) {
        if (request == null) {
            return null;
        }
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.REQUEST_MAINTENANCE);
        httpParamObject.setJSONContentType();
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getReservation(String hsID) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_RESERVATION_CATEGORY);
//        httpParamObject.setPostMethod();
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put(API_PARAMS.ID, hsID);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        httpParamObject.addParameter(API_PARAMS.ID, hsID);
        httpParamObject.setJSONContentType();
//        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setClassType(ReservationResponseApi.class);
        return httpParamObject;
    }


    public static HttpParamObject getReservationItemListByType(FetchReservationRequest fetchReservationRequest) {
        if (fetchReservationRequest == null) {
            return null;
        }
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_RESERVATION_ITEM_BY_TYPE);
        httpParamObject.setJSONContentType();
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(fetchReservationRequest));
        httpParamObject.setClassType(ReservationResponseApi.class);
        return httpParamObject;
    }


    public static HttpParamObject getReservationSubItems(String parentId, String hsId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_RESERVATION_SUB_ITEMS);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter("hotelId", hsId);
        httpParamObject.addParameter("categoryId", parentId);
        httpParamObject.setClassType(ReservationTypeResponseApi.class);
        return httpParamObject;
    }

    public static HttpParamObject requestHealthBeautyReserve(ReservationRequestOrder request, boolean b) {
        if (request == null) {
            return null;
        }
        HttpParamObject httpParamObject = new HttpParamObject();
        if (b == true){
            httpParamObject.setUrl(AppConstants.PAGE_URL.REQUEST_INTERNET_ORDER);
        } else {
            httpParamObject.setUrl(AppConstants.PAGE_URL.REQUEST_HEALTH_BEAUTY_RESERVE);
        }
        httpParamObject.setJSONContentType();
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject requestInternetOrder(InternetOrderRequest request) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.REQUEST_INTERNET_ORDER);
        httpParamObject.setJSONContentType();
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getServiceFeedback(String uosId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_SERVICE_FEEDBACK);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.UOSID, uosId);
        httpParamObject.setClassType(GetServiceFeedbackResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject submitFeedback(SubmittFeedbackRequest request, String feedbackId) {
        if (request == null) {
            return null;
        }
        HttpParamObject httpParamObject = new HttpParamObject();
        if (!TextUtils.isEmpty(feedbackId)) {
            httpParamObject.setUrl(AppConstants.PAGE_URL.UPDATE_FEEDBACK);
        } else {
            httpParamObject.setUrl(AppConstants.PAGE_URL.SUBMIT_FEEDBACK);
        }
        httpParamObject.setJSONContentType();
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getRoomLateCheckoutList(String roomId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_LATE_CHECKOUT_INFO);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ROOM_CATEGORY_ID, roomId);
        httpParamObject.setClassType(GetLateCheckoutServiceResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getRoomEarlyCheckInList(String roomId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_EARLY_CHECKIN_INFO);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ROOM_CATEGORY_ID, roomId);
        httpParamObject.setClassType(EarlyCheckInResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject requestLateCheckout(LateCheckoutRequest lateCheckoutRequest) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.REQUEST_LATE_CHECKOUT);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(lateCheckoutRequest));
        return httpParamObject;
    }

    public static HttpParamObject getEntertainMentService(String id) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_ENTERTAINMENT_SERVICE);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, id);
        httpParamObject.setClassType(EntertainmentResponseApi.class);
        return httpParamObject;
    }

    public static HttpParamObject getAllEntertainMentService() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_ALL_ENTERTAINMENT_SERVICE);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(AllEntertainmentResponseApi.class);
        return httpParamObject;
    }


    public static HttpParamObject getAllEntertainMentServicebyCategory(String serviceId, String genereId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_ALL_ENTERTAINMENT_SERVICE_CATEGORY);
        httpParamObject.addParameter(API_PARAMS.GENERID, genereId);
        httpParamObject.addParameter(API_PARAMS.ENT_SERVICE_ID, serviceId);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(AllEntertainmentResponseApi.class);
        return httpParamObject;
    }

    public static HttpParamObject getFeedbacksAndSuggestions(BaseServiceRequest baseServiceRequest) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_FEEDBACK_AND_SUGGESTIONS);
        httpParamObject.setJSONContentType();
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(baseServiceRequest));
        httpParamObject.setClassType(GetStaysFeedbackResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject submitStaysFeedback(FeedbackAndSuggestionsRequest request) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SUBMIT_STAYS_FEEDBACK);
        httpParamObject.setPostMethod();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setJSONContentType();
        return httpParamObject;
    }

    public static HttpParamObject initiateCheckin(CheckInData initiateCheckInResponse) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.INITIATE_CHECKIN_RESPONSE);
        httpParamObject.setJSONContentType();
        httpParamObject.setPatchMethod();
        httpParamObject.setJson(JsonUtil.toJson(initiateCheckInResponse));
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getInitiateCheckin(String hotelId, String stayId, String userId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_INITIATE_CHECKIN);
        httpParamObject.setPatchMethod();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(API_PARAMS.HOTEL_ID, hotelId);
            jsonObject.put(API_PARAMS.STAYS_ID, stayId);
            jsonObject.put(API_PARAMS.USER_ID, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setClassType(InitiateCheckInResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getAllStayPref(){
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_PREF);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(PreferanceResponceApi.class);
        return httpParamObject;
    }

    public static HttpParamObject retreiveLateCheckout(BaseServiceRequest request) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.RETREIVE_LATE_CHECKOUT);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(RetreiveLateCheckoutResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject suggestHotel(SuggestHotelRequest request) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SUGGEST_HOTEL);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(request));
        return httpParamObject;
    }

    public static HttpParamObject getComplementaryService(String hsId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_COMP_SERVICE);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter(API_PARAMS.ID, hsId);
        httpParamObject.setClassType(GetComplementaryServiceResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getNotifications(BaseServiceRequest request) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_NOTIFICATION_BY_ID);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(NotificationResponseApi.class);
        return httpParamObject;
    }

    public static HttpParamObject getAllServices(BaseServiceRequest request) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_ALL_SERVICES);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(request));
        httpParamObject.setClassType(GetAllServicesResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject getMessage(BaseServiceRequest baseServiceRequest) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_MESSAGE);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(baseServiceRequest));
        httpParamObject.setClassType(MessageApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject addMessage(AddMessage baseServiceRequest) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.ADD_MESSAGE);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(baseServiceRequest));
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject fetchProfileTitles() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.FETCH_PROFILE_TITLES);
        httpParamObject.setClassType(FetchProfileTitles.class);
        return httpParamObject;
    }

    public static HttpParamObject fetchUserCommonData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.FETCH_COMMON_DATA);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(FetchCommonData.class);
        return httpParamObject;
    }

    public static HttpParamObject fetchIdentityDocTypes() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.FETCH_IDENTITY_DOCS);
        httpParamObject.setClassType(FetchIdentityDocsType.class);
        return httpParamObject;
    }

    public static HttpParamObject deleteDoc(DeleteIdPojo deleteReq) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.DELETE_DOC);
        httpParamObject.setPatchMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(JsonUtil.toJson(deleteReq));
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject DeleteStay(String stayId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.DELETE_STAY + "?" + API_PARAMS.ID+"="+stayId);
        httpParamObject.setDeleteMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(BaseApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject generateOtp(String type, String value) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GENERATE_OTP);
        httpParamObject.setJSONContentType();
        httpParamObject.addParameter("type", type);
        httpParamObject.addParameter("value", value);
        httpParamObject.setClassType(GenerateOtpApi.class);
        return httpParamObject;
    }

}
