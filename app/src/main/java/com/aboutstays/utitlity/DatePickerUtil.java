package com.aboutstays.utitlity;


import android.app.FragmentManager;
import android.content.DialogInterface;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Rajnikant on 11-04-2017.
 */

public class DatePickerUtil {
    private String format = "dd-MM-yyyy";
    private Calendar calendar = Calendar.getInstance();
    private OnDateSelected onDateSelected;
    FragmentManager fragmentManger;
    private String dailogName = "datePickerDialog";
    private boolean isMinDayToday, isMaxDayToday;

    public DatePickerUtil(String format, Calendar calendar, FragmentManager fragmentManger, String dailogName, OnDateSelected onDateSelected) {
        this.format = format;
        this.calendar = calendar;
        this.onDateSelected = onDateSelected;
        this.fragmentManger = fragmentManger;
        this.dailogName = dailogName;
    }

    public DatePickerUtil(String format, Calendar calendar, FragmentManager fragmentManger, String dailogName, boolean isMinDayToday, boolean isMaxDayToday, OnDateSelected onDateSelected) {
        this.format = format;
        this.calendar = calendar;
        this.onDateSelected = onDateSelected;
        this.fragmentManger = fragmentManger;
        this.dailogName = dailogName;
        this.isMinDayToday = isMinDayToday;
        this.isMaxDayToday = isMaxDayToday;
    }

    public void show() {
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        SimpleDateFormat sdf = new SimpleDateFormat(format);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.YEAR, year);
                        String selectedDate = sdf.format(calendar.getTime());
                        onDateSelected.onDateSelected(calendar, selectedDate);
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        if (isMinDayToday) {
            dpd.setMinDate(Calendar.getInstance());
        }
        if (isMaxDayToday) {
            dpd.setMaxDate(Calendar.getInstance());
        }
        dpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                onDateSelected.onCanceled();
            }
        });
        dpd.show(fragmentManger, dailogName);
    }

    public void show(Calendar calendarMinimum) {
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        SimpleDateFormat sdf = new SimpleDateFormat(format);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.YEAR, year);
                        String selectedDate = sdf.format(calendar.getTime());
                        onDateSelected.onDateSelected(calendar, selectedDate);
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(calendarMinimum);
        if (isMaxDayToday) {
            dpd.setMaxDate(Calendar.getInstance());
        }
        dpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                onDateSelected.onCanceled();
            }
        });
        dpd.show(fragmentManger, dailogName);
    }

    public interface OnDateSelected {
        void onDateSelected(Calendar calendar, String date);

        void onCanceled();
    }


}
