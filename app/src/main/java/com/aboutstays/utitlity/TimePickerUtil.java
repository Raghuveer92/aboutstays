package com.aboutstays.utitlity;


import android.app.FragmentManager;

import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by neeraj on 13/4/17.
 */

public class TimePickerUtil {

    private String format="hh:mm a";
    private Calendar calendar=Calendar.getInstance();
    private OnTimeSelected onTimeSelected;
    FragmentManager fragmentManger;
    private String dailogName= "timePickerDialog";

    public TimePickerUtil(String format, Calendar calendar, FragmentManager fragmentManger, String dailogName,OnTimeSelected onTimeSelected) {
        this.format = format;
        this.calendar = calendar;
        this.onTimeSelected = onTimeSelected;
        this.fragmentManger = fragmentManger;
        this.dailogName = dailogName;
    }

    public void show(){
        com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(com.wdullaer.materialdatetimepicker.time.TimePickerDialog view,  int hourOfDay, int minute, int second) {
                        SimpleDateFormat sdf = new SimpleDateFormat(format);
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        String selectedDate = sdf.format(calendar.getTime());
                        onTimeSelected.onTimeSelected(calendar,selectedDate);
                    }
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                false
        );
        tpd.show(fragmentManger, dailogName);
    }
    public void show(Timepoint minimumTime, Timepoint maximumTime){
        com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(com.wdullaer.materialdatetimepicker.time.TimePickerDialog view,  int hourOfDay, int minute, int second) {
                        SimpleDateFormat sdf = new SimpleDateFormat(format);
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        String selectedDate = sdf.format(calendar.getTime());
                        onTimeSelected.onTimeSelected(calendar,selectedDate);
                    }
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                false
        );
        tpd.setMinTime(minimumTime);
        tpd.setMaxTime(maximumTime);
        tpd.show(fragmentManger, dailogName);
    }

    public void show(Timepoint minimumTime){
        com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(com.wdullaer.materialdatetimepicker.time.TimePickerDialog view,  int hourOfDay, int minute, int second) {
                        SimpleDateFormat sdf = new SimpleDateFormat(format);
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        String selectedDate = sdf.format(calendar.getTime());
                        onTimeSelected.onTimeSelected(calendar,selectedDate);
                    }
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                false
        );
        tpd.setMinTime(minimumTime);
        tpd.show(fragmentManger, dailogName);
    }

    public interface OnTimeSelected{
        void onTimeSelected(Calendar calendar,String date);
    }

}
