package com.aboutstays.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.GetDirectionModel;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;


public class GetDirectionFragment extends BaseFragment implements CustomListAdapterInterface {
    private ListView lvGetDirection;
    private CustomListAdapter listAdapter;
    private TextView tvEmptyView;
    private List<GetDirectionModel> directionModelList = new ArrayList<>();

    @Override
    public void initViews() {
        initToolBar("Get Direction");
        lvGetDirection = (ListView) findView(R.id.lv_get_direction);
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_stay_preferences, directionModelList , this);
        lvGetDirection.setAdapter(listAdapter);
        lvGetDirection.setEmptyView(tvEmptyView);
        setData();
        listAdapter.notifyDataSetChanged();
    }

    private void setData() {
        for (int i=0; i<4; i++){
            GetDirectionModel getDirectionModel = new GetDirectionModel();
            getDirectionModel.setDirectionName("Left "+i);
            directionModelList.add(getDirectionModel);
        }
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_get_direction;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_stay_preferences, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        GetDirectionModel getDirectionModel = directionModelList.get(position);
        holder.tvName.setText(getDirectionModel.getDirectionName());
        holder.ivSelect.setVisibility(View.GONE);

        return convertView;
    }

    class Holder {
        TextView tvName;
        ImageView tvIcon, ivSelect;

        public Holder(View view) {
            tvName = (TextView) view.findViewById(R.id.tv_preference);
            tvIcon = (ImageView) view.findViewById(R.id.iv_preferences);
            ivSelect = (ImageView) view.findViewById(R.id.iv_select);
        }
    }
}
