package com.aboutstays.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.aboutstays.R;
import com.aboutstays.fragment.dialog.BookingConfirmDialogFragment;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.commute.ListCommutePackage;
import com.aboutstays.rest.request.CommuteRequest;
import com.aboutstays.rest.response.CarDetails;
import com.aboutstays.rest.response.CarDetailsAndPrice;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.aboutstays.utitlity.DatePickerUtil;
import com.aboutstays.utitlity.TimePickerUtil;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.receivers.CartUpdateReceiver;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by neeraj on 31/3/17.
 */

public class CommuteItemFragment extends BaseFragment implements CustomListAdapterInterface {

    private ListView lvCommute;
    private Button btnConfirmBooking;
    private TextView emptyTextView;
    private TextView tvScheduledDate;
    List<CarDetailsAndPrice> commuteList = new ArrayList<>();
    private CustomListAdapter listAdapter;
    private TextView tvRequestNow;
    private TextView tvDeliveryDate;
    private boolean reqNow = false;
    private long pickupTime;
    private GeneralServiceModel generalServiceModel = new GeneralServiceModel();
    private CarDetails carDetails;
    private String gsId, hotelId, staysId, userId, packageName;
    private Double price;
    public static final String PICKUP_DATE_AND_TIME_FORMAT = "MMM dd, hh:mm a";
    public static final String COMMUTE_DATE_AND_TIME_FORMAT = "dd MMM yyyy, hh:mm a";
    public static final String PICKUP_DATE = "MMM dd";
    public String datePickerDate;
    private Calendar calendarInstance;
    private String deliveryDate;
    private long checkOutDate;


    public static CommuteItemFragment getInstance(List<CarDetailsAndPrice> subList, GeneralServiceModel generalServiceModel, String string) {
        CommuteItemFragment fragment = new CommuteItemFragment();
        fragment.commuteList = subList;
        fragment.packageName = string;
        fragment.generalServiceModel = generalServiceModel;
        return fragment;
    }

    private EditText etComment;

    @Override
    public void initViews() {
        calendarInstance = Calendar.getInstance();

        if (generalServiceModel != null){
            gsId = generalServiceModel.getGeneralServiceData().getId();
            if (generalServiceModel.getHotelStaysList().getStaysPojo() != null) {
                hotelId = generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId();
                staysId = generalServiceModel.getHotelStaysList().getStaysPojo().getStaysId();
                userId = generalServiceModel.getHotelStaysList().getStaysPojo().getUserId();
            }
        }

        checkOutDate = Preferences.getData(AppConstants.PREF_KEYS.CHECKOUT_DATE, 0l);
        lvCommute = (ListView) findView(R.id.lv_commute);
        emptyTextView = (TextView) findView(R.id.tv_empty_view);
        btnConfirmBooking = (Button) findView(R.id.btn_confirm_booking);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_commute, commuteList, this);
        lvCommute.setAdapter(listAdapter);
        lvCommute.setEmptyView(emptyTextView);
        setFooter();
        listAdapter.notifyDataSetChanged();

        setOnClickListener(R.id.btn_confirm_booking);
    }

    private void setFooter() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View inflate = inflater.inflate(R.layout.view_commute_page_footer, lvCommute, false);
        tvRequestNow = (TextView) inflate.findViewById(R.id.tv_add);
        etComment = (EditText) inflate.findViewById(R.id.et_comment);
        tvDeliveryDate = (TextView) inflate.findViewById(R.id.tv_date_time);
        tvScheduledDate = (TextView) inflate.findViewById(R.id.tv_shcedule_date);

        View view = inflate.findViewById(R.id.view_line);
        RelativeLayout rlHeader = (RelativeLayout) inflate.findViewById(R.id.rl_total);
        RelativeLayout rlline = (RelativeLayout) inflate.findViewById(R.id.rl_row_view0);
        LinearLayout llDatePick = (LinearLayout) inflate.findViewById(R.id.rl_row_data);
        LinearLayout llMsg = (LinearLayout) inflate.findViewById(R.id.ll_msg);
        TextView tvMsg = (TextView) inflate.findViewById(R.id.tv_msg);
        RelativeLayout rlOr = (RelativeLayout) inflate.findViewById(R.id.lay_or);
        TextView tvTitle = (TextView) inflate.findViewById(R.id.tv_title);

        rlHeader.setVisibility(View.GONE);
        view.setVisibility(View.GONE);
        llMsg.setVisibility(View.VISIBLE);
        tvMsg.setText(getString(R.string.reservation_bottom_msg));
        rlline.setVisibility(View.GONE);
        //rlOr.setVisibility(View.GONE);
        llDatePick.setOnClickListener(this);

        tvDeliveryDate.setHint("");
        tvTitle.setText(R.string.preferred_dateTime);
        tvScheduledDate.setOnClickListener(this);
        tvRequestNow.setOnClickListener(this);
        //setRequestedDateAndTime();
        lvCommute.addFooterView(inflate);


//        LayoutInflater inflater = getActivity().getLayoutInflater();
//        View inflate = inflater.inflate(R.layout.view_order_details_footer, lvCommute, false);
//        tvRequestNow = (TextView) inflate.findViewById(R.id.tv_add);
//        etComment = (EditText) inflate.findViewById(R.id.et_comment);
//        tvDeliveryDate = (TextView) inflate.findViewById(R.id.tv_date_time);
//        View view = inflate.findViewById(R.id.view_line);
//        RelativeLayout rlHeader = (RelativeLayout) inflate.findViewById(R.id.rl_total);
//        RelativeLayout rlline = (RelativeLayout) inflate.findViewById(R.id.rl_row_view0);
//        LinearLayout llDatePick = (LinearLayout) inflate.findViewById(R.id.rl_row_data);
//        LinearLayout llMsg = (LinearLayout) inflate.findViewById(R.id.ll_msg);
//        rlHeader.setVisibility(View.GONE);
//        view.setVisibility(View.GONE);
//        llMsg.setVisibility(View.GONE);
//        rlline.setVisibility(View.GONE);
//        llDatePick.setOnClickListener(this);
//        tvRequestNow.setOnClickListener(this);
//        lvCommute.addFooterView(inflate);
    }

    private void setRequestedDateAndTime() {
        Calendar c = Calendar.getInstance();
        pickupTime = c.getTimeInMillis() - 1000;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_shcedule_date:
//                setReqNowFalse();
//                setDateandTime();
                break;
            case R.id.rl_row_data:
                setReqNowFalse();
                setDateandTime();
                break;
            case R.id.tv_add:
                if (reqNow == false) {
                    reqNow = true;
                    tvRequestNow.setBackgroundResource(R.drawable.shape_button_selected);
                    tvRequestNow.setTextColor(Color.WHITE);
                    tvDeliveryDate.setText("");
//                    tvRequestNow.setText(R.string.requested);
//                    selectCurrentDateandTime();
                } else {
                    setReqNowFalse();
                }
                break;
            case R.id.btn_confirm_booking:
                boolean ifBeforeCheckout = Util.ifGreaterThnCheckOut(pickupTime, checkOutDate);
                if (ifBeforeCheckout) {
                    validateDateandTime();
                } else {
                    errorDialog(getString(R.string.select_date_error));
                    return;
                }
                break;
        }
    }

    private void errorDialog(String string) {
        SuccessDialogFragment.show(getChildFragmentManager(),
                "MAINTENANCE",
                "REQUEST FAILED",
                string,
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                    }
                }
        );
    }

    private void validateDateandTime() {
        CommuteRequest commuteRequest = new CommuteRequest();
        commuteRequest.setHotelId(hotelId);
        commuteRequest.setRequestedNow(reqNow);
        commuteRequest.setStaysId(staysId);
        commuteRequest.setUserId(userId);
        commuteRequest.setGsId(gsId);
        commuteRequest.setPrice(price);
        commuteRequest.setPackageName(packageName);

        if (carDetails != null){
            commuteRequest.setCarDetails(carDetails);
        } else {
            showToast("Please select car details");
            return;
        }

        if (reqNow == true) {
            commuteRequest.setRequestedNow(true);
        } else if (pickupTime > 0){
            commuteRequest.setRequestedNow(false);
            commuteRequest.setDeliveryTime(pickupTime);
        } else {
            showToast(getString(R.string.please_select_time_first));
            return;
        }
        if (reqNow == false && TextUtils.isEmpty(deliveryDate)){
            showToast(getString(R.string.please_select_time_first));
            return;
        } else {
            commuteRequest.setDeliverTimeString(deliveryDate);
        }
        
        if (!TextUtils.isEmpty(etComment.getText().toString().trim())){
            commuteRequest.setComment(etComment.getText().toString().trim());
        }

        confirmBooking(commuteRequest);
    }

    private void confirmBooking(CommuteRequest commuteRequest) {
        HttpParamObject httpParamObject = ApiRequestGenerator.orderCommute(commuteRequest);
        executeTask(AppConstants.TASK_CODES.COMMUTE_ORDER, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.COMMUTE_ORDER:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() ==false){
                    successDialog();
                    CartUpdateReceiver.sendBroadcast(getActivity(),CartUpdateReceiver.ACTION_UPDATE_REQUEST);
                }
                break;
        }
    }

    private void successDialog() {
        if (reqNow == true){
            setRequestedDateAndTime();
        }
        BookingConfirmDialogFragment.show(getChildFragmentManager(),
                "COMMUTE",
                "REQUEST INITIATED",
                packageName + " commute",
                carDetails.getName() + " | "+getString(R.string.rupeesSymbol) + price,
                Util.getDate(pickupTime, COMMUTE_DATE_AND_TIME_FORMAT),
                "Your request has been initiated successfully.Booking confirmation will be sent shortly.",
                new BookingConfirmDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                        getActivity().finish();
                    }
                }
        );
    }


    private void selectCurrentDateandTime() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time =&gt; " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat(PICKUP_DATE_AND_TIME_FORMAT);
        String formattedDate = df.format(c.getTime());
        tvDeliveryDate.setText(formattedDate);
        //tvScheduledDate.setText(formattedDate);
        pickupTime = c.getTimeInMillis() - 1000;
    }

    private void setReqNowFalse() {
        reqNow = false;
        tvRequestNow.setBackgroundResource(R.drawable.shape_button_email);
        tvRequestNow.setTextColor(getResourceColor(R.color.color_red_bg));
        tvRequestNow.setText(R.string.request_now);
        tvDeliveryDate.setText("");
        pickupTime = 0;
        deliveryDate = "";
    }

    private void setDateandTime() {

        final AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        DatePickerUtil datePickerUtil = new DatePickerUtil(PICKUP_DATE, calendarInstance, appCompatActivity.getFragmentManager(), getString(R.string.select_date), true, false, new DatePickerUtil.OnDateSelected() {
            @Override
            public void onDateSelected(Calendar calendar, String date) {
                TimePickerUtil timePickerUtil = new TimePickerUtil(PICKUP_DATE_AND_TIME_FORMAT, calendar, appCompatActivity.getFragmentManager(), getString(R.string.select_time), new TimePickerUtil.OnTimeSelected() {
                    @Override
                    public void onTimeSelected(Calendar calendar, String date) {
                        calendarInstance = calendar;
                        pickupTime = calendar.getTimeInMillis() - 1000;
                        tvDeliveryDate.setText(date);
                        Date time = calendar.getTime();
                        deliveryDate = Util.format(time, Util.DELIVERY_TIME_DATE_PATTERN);
                    }
                });
                timePickerUtil.show();
            }

            @Override
            public void onCanceled() {
//                tvDeliveryDate.setText("");
            }
        });
        datePickerUtil.show();

       /* final Calendar c = Calendar.getInstance();
        long timeToShow = 0;
         if(!TextUtils.isEmpty(datePickerDate)) {
            timeToShow = convertStringToLong(Util.API_DATE_FORMAT, datePickerDate);
        }
        c.setTimeInMillis(timeToShow);

        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    //  @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Util.API_DATE_FORMAT);
                        datePickerDate = simpleDateFormat.format(c.getTime());

//                        etPickupDateAndTime.setText(fromDatePick.toString());
//                        pickupTime = c.getTimeInMillis();
                    }
                }, mYear, mMonth, mDay);
        openTimePicker(c);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();*/
    }

/*    private void openTimePicker(final Calendar c) {
        long timeToShow = 0;
        if(TextUtils.isEmpty(tvDeliveryDate.getText().toString())){
            timeToShow = Calendar.getInstance().getTimeInMillis();
        } else {
            timeToShow = convertStringToLong(PICKUP_DATE_AND_TIME_FORMAT, tvDeliveryDate.getText().toString());
        }
        c.setTimeInMillis(timeToShow);
        final int hour = c.get(Calendar.HOUR_OF_DAY);
        final int min = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        c.set(Calendar.MINUTE, minute);
                        SimpleDateFormat format = new SimpleDateFormat(PICKUP_DATE_AND_TIME_FORMAT);
                        String time = format.format(c.getTime());
                        tvDeliveryDate.setText(time);
                        //tvScheduledDate.setText(time);
                        pickupTime = c.getTimeInMillis() - 1000;
                    }
                }, hour, min, false);
        timePickerDialog.show();
    }*/


    public long convertStringToLong(String dateFormat, String dateValue){
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        try{
            Date date = formatter.parse(dateValue);
            return date.getTime();
        } catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_commute_item;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {

        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final CarDetailsAndPrice detailsAndPrice = commuteList.get(position);
        if (detailsAndPrice.isSelect() == true){
            holder.ivSelect.setVisibility(View.VISIBLE);
        } else {
            holder.ivSelect.setVisibility(View.INVISIBLE);
        }

        if (detailsAndPrice.getCarDetails() != null) {
            holder.tvCarName.setText(detailsAndPrice.getCarDetails().getName());
            String rupeesSymbol = getString(R.string.rupeesSymbolonly);
            holder.tvCarPrice.setText(rupeesSymbol + detailsAndPrice.getPrice());
            holder.tvMaxPerson.setText("upto " + detailsAndPrice.getCarDetails().getPax() + " people");
            holder.tvDistance.setText("upto " + detailsAndPrice.getCarDetails().getBaseKm() + "Km/1 Hr");
            holder.tvAdditionalPrice.setText("Additional : " + rupeesSymbol + detailsAndPrice.getCarDetails().getPricePerKm() + " / km");

            String imageUrl = detailsAndPrice.getCarDetails().getImageUrl();
            if (!TextUtils.isEmpty(imageUrl)) {
                Picasso.with(getActivity()).load(imageUrl).placeholder(R.drawable.progress_animation).into(holder.ivCar);
            }
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carDetails = new CarDetails();
                carDetails = detailsAndPrice.getCarDetails();
                price = detailsAndPrice.getPrice();
                for (CarDetailsAndPrice detailsAndPrice1 : commuteList){
                    detailsAndPrice1.setSelect(false);
                }
                if (detailsAndPrice.isSelect() == true){
                    detailsAndPrice.setSelect(false);
                } else {
                    detailsAndPrice.setSelect(true);
                }
                listAdapter.notifyDataSetChanged();
            }
        });

        return convertView;
    }

    class Holder {
        ImageView ivCar, ivSelect;
        TextView tvCarName, tvCarPrice, tvMaxPerson, tvDistance, tvAdditionalPrice;

        public Holder(View view) {
            ivCar = (ImageView) view.findViewById(R.id.iv_car);
            ivSelect = (ImageView) view.findViewById(R.id.iv_select_icon);
            tvCarName = (TextView) view.findViewById(R.id.tv_car_name);
            tvMaxPerson = (TextView) view.findViewById(R.id.tv_max_people);
            tvCarPrice = (TextView) view.findViewById(R.id.tv_price);
            tvDistance = (TextView) view.findViewById(R.id.tv_km);
            tvAdditionalPrice = (TextView) view.findViewById(R.id.tv_additional_price);
            ivSelect.setColorFilter(getResourceColor(R.color.et_gray));
        }
    }
}
