package com.aboutstays.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.activity.SereneSpaActivity;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.reservation.ReservationData;
import com.aboutstays.model.reservation.TypeInfoList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by neeraj on 3/4/17.
 */

public class ReservatonSubItemFragment extends BaseFragment implements CustomListAdapterInterface {

    private List<String> subType = new ArrayList<>();
    private ListView lvReservation;
    private CustomListAdapter listAdapter;
    private TextView tvEmptyView;
    private String toolbarTitle;
    private Boolean imageListing;
    private GeneralServiceModel generalServiceModel;
    private TypeInfoList typeInfoList;

    @Override
    public void initViews() {

        Bundle bundle = getArguments();
        if (bundle != null){
            typeInfoList = (TypeInfoList) bundle.getSerializable(AppConstants.BUNDLE_KEYS.RESERVATION_SUB_TYPES);
            if (typeInfoList != null){
                toolbarTitle  = typeInfoList.getTypeName();
                subType = typeInfoList.getSubTypes();
                imageListing = typeInfoList.getImageListing();
            }

            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        }

        if (!TextUtils.isEmpty(toolbarTitle)){
            initToolBar(toolbarTitle);
        } else {
            initToolBar("");
        }

        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        lvReservation = (ListView) findView(R.id.lv_maintenance);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_room_service, subType, this);
        lvReservation.setAdapter(listAdapter);
        lvReservation.setEmptyView(tvEmptyView);
        listAdapter.notifyDataSetChanged();

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_maintenance;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final String subItem = subType.get(position);
        if (!TextUtils.isEmpty(subItem)){
            holder.title.setText(subItem);
        }
        holder.tvTime.setVisibility(View.GONE);
        holder.titleImage.setVisibility(View.GONE);

        if (imageListing == true){
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.RESERVATION_SUB_TYPES, typeInfoList);
                    bundle.putString(AppConstants.BUNDLE_KEYS.TITLE_NAME, subItem);
                    FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.RESTAURANTS_LIST_FRAGMENT, bundle);
                }
            });
        } else {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.RESERVATION_SUB_TYPES, typeInfoList);
                    bundle.putString(AppConstants.BUNDLE_KEYS.TITLE_NAME, subItem);
                    startNextActivity(bundle, SereneSpaActivity.class);
                }
            });
        }

        return convertView;
    }

    class Holder {
        TextView title, tvTime, tvItemCount;
        ImageView titleImage, arrowImage;
        RelativeLayout rlCount;

        public Holder(View view) {
            title = (TextView) view.findViewById(R.id.tv_title);
            titleImage = (ImageView) view.findViewById(R.id.iv_title_image1);
            arrowImage = (ImageView) view.findViewById(R.id.iv_right_angle);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_count);
            tvItemCount = (TextView) view.findViewById(R.id.tv_item_count);
        }
    }

}
