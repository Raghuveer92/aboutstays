package com.aboutstays.fragment;

import android.app.AlarmManager;
import android.icu.util.Calendar;

import com.aboutstays.R;
import com.github.jhonnyx2012.horizontalpicker.DatePickerListener;
import com.github.jhonnyx2012.horizontalpicker.Day;
import com.github.jhonnyx2012.horizontalpicker.HorizontalPicker;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.Util;

/**
 * Created by admin on 3/30/17.
 */

public class WeekViewFragment extends BaseFragment {
    private List<Long> eventList = new ArrayList<>();
    private OnDateSelectListener onDateSelectListener;

    public static WeekViewFragment getInstance(List<Long> eventList,OnDateSelectListener onDateSelectListener) {
        WeekViewFragment weekViewFragment = new WeekViewFragment();
        weekViewFragment.eventList = eventList;
        weekViewFragment.onDateSelectListener=onDateSelectListener;
        return weekViewFragment;
    }

    @Override
    public void initViews() {
        if (eventList.size() > 0) {
            Collections.sort(eventList);
            HorizontalPicker horizontalPicker = (HorizontalPicker) findView(R.id.horizontal_picker);
            Long toDate = eventList.get(eventList.size() - 1);
            long daysSize = horizontalPicker.getDaysSize(toDate, System.currentTimeMillis());
            long checkSize = 7 - daysSize;
            if (checkSize > 0) {
                toDate = toDate + (AlarmManager.INTERVAL_DAY * checkSize);
            }else {
                toDate = toDate + (AlarmManager.INTERVAL_DAY * 3);
            }
            List<Day> dayList = horizontalPicker.generateDays(toDate, System.currentTimeMillis(),eventList);
            horizontalPicker
                    .setDaysList(dayList)
                    .init();

            horizontalPicker.setListener(new DatePickerListener() {
                @Override
                public void onDateSelected(DateTime dateSelected) {
                    onDateSelectListener.onDaySelected(dateSelected.getMillis());
                }
            });
            for(Day day:dayList){
                if(day.isEvent()){
                    horizontalPicker.setSelect(day);
                    break;
                }
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_week_view;
    }

    public interface OnDateSelectListener {
        void onDaySelected(long selctedDate);
    }
}
