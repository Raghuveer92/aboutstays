package com.aboutstays.fragment;

import com.aboutstays.R;
import com.aboutstays.model.hotels.Hotel;

import java.util.List;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by ajay on 8/12/16.
 */

public class PremiumFragment extends BaseFragment {

    public static PremiumFragment getInstance(){
        PremiumFragment premiumFragment = new PremiumFragment();
        return  premiumFragment;
    }

    @Override
    public void initViews() {

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_premium;
    }

    public void setData(List<Hotel> premiumList) {

    }
}
