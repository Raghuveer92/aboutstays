package com.aboutstays.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.rest.response.ChildCategoryItem;
import com.aboutstays.rest.response.GetHouseRulesResponse;
import com.aboutstays.rest.response.HouseRulesData;
import com.aboutstays.rest.response.ParentCategoryItems;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import simplifii.framework.ListAdapters.CustomExpandableListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by aman on 27/03/17.
 */

public class HouseRulesFragment extends BaseFragment {

    private ExpandableListView exLv;
    private TextView tvEmptyView;
    private LinkedHashMap<String, List<ChildCategoryItem>> mapCategoryVsDescp;
    private CustomExpandableListAdapter customExplandableListAdapter;

    @Override
    public void initViews() {
        initToolBar("House Rules");

        exLv = (ExpandableListView) findView(R.id.exlv_house_rules);
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        mapCategoryVsDescp = new LinkedHashMap<>();
        exLv.setEmptyView(tvEmptyView);

        final Bundle bundle = getArguments();
        if(bundle != null){
            GeneralServiceModel generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if(generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null){
                String houseRuleServiceId = generalServiceModel.getGeneralServiceData().getHsId();
                getHouseRules(houseRuleServiceId);
            }
        }
    }

    private void getHouseRules(String houseRuleServiceId) {
        HttpParamObject httpParamObject = ApiRequestGenerator.getHouseRules(houseRuleServiceId);
        executeTask(AppConstants.TASK_CODES.GET_HOUSE_RULES, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode){
            case AppConstants.TASK_CODES.GET_HOUSE_RULES:
                GetHouseRulesResponse houseRulesResponse = (GetHouseRulesResponse) response;
                if(houseRulesResponse != null && !houseRulesResponse.getError()){
                    HouseRulesData hrData = houseRulesResponse.getData();
                    if(hrData != null && hrData.getListPGItems() != null){
                        List<ParentCategoryItems> listPGItem = hrData.getListPGItems();
                        if(listPGItem != null){
                            for(ParentCategoryItems pgItem : listPGItem){
                                mapCategoryVsDescp.put(pgItem.getTitle(), pgItem.getListChildCategory());
                            }
                            setExpandableList(mapCategoryVsDescp);
                            customExplandableListAdapter.notifyDataSetChanged();
                        }
                    }
                }
                break;
        }
    }

    private void setExpandableList(final LinkedHashMap<String, List<ChildCategoryItem>> listMap) {
        Iterator<String> iterator = listMap.keySet().iterator();
        final List<String> keys = new ArrayList<>();
        while (iterator.hasNext()) {
            keys.add(iterator.next());
        }
        customExplandableListAdapter = new CustomExpandableListAdapter(getActivity(), R.layout.row_header_expandable, R.layout.row_house_rules, listMap, new CustomExpandableListAdapter.ExpandableListener() {
            @Override
            public int getChildSize(int parentPosition) {
                return listMap.get(keys.get(parentPosition)).size();
            }

            @Override
            public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
                convertView = inflater.inflate(resourceId, parent, false);
                setText(R.id.tv_men_women,keys.get(groupPosition),convertView);
                ImageView imageView= (ImageView) convertView.findViewById(R.id.iv_indicator);
                View view = convertView.findViewById(R.id.viewLine);
                view.setVisibility(View.VISIBLE);
                if(isExpanded){
                    imageView.setImageResource(R.mipmap.up_arrow_white);
                }else {
                    imageView.setImageResource(R.mipmap.arrow_down_white);
                }
                return convertView;
            }

            @Override
            public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
                Holder holder = null;
                if (convertView == null) {
                    convertView = inflater.inflate(resourceId, parent, false);
                    holder = new Holder(convertView);
                    convertView.setTag(holder);
                } else {
                    holder = (Holder) convertView.getTag();
                }

                View viewDescription = (View) convertView.findViewById(R.id.view_house_rules);
                if(isLastChild) {
                    viewDescription.setVisibility(View.GONE);
                } else {
                    viewDescription.setVisibility(View.VISIBLE);
                }

                final ChildCategoryItem childCategoryItem = listMap.get(keys.get(groupPosition)).get(childPosition);
                holder.title.setText(childCategoryItem.getTitle());
                holder.tvTime.setVisibility(View.GONE);

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle childBundle = new Bundle();
                        childBundle.putSerializable(AppConstants.BUNDLE_KEYS.CHILD_CATEGORY_ITEM, childCategoryItem);
                        FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.HOUSE_RULES_DESCRIPTION, childBundle);
                    }
                });
                return convertView;
            }
        });
        exLv.setAdapter(customExplandableListAdapter);
        for(int x=0;x<keys.size();x++){
            exLv.expandGroup(x);
        }
    }


    class Holder {
        TextView title, tvItemCount, tvTime;
        ImageView arrowImage;
        RelativeLayout rlCount;

        public Holder(View view) {
            title = (TextView) view.findViewById(R.id.tv_title);
            arrowImage = (ImageView) view.findViewById(R.id.iv_right_angle);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_count);
            tvItemCount = (TextView) view.findViewById(R.id.tv_item_count);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_house_rules;
    }
}
