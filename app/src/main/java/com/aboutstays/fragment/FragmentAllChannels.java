package com.aboutstays.fragment;

import android.graphics.LinearGradient;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.model.entertainment.AllEntertainmentData;
import com.aboutstays.model.entertainment.AllEntertainmentResponseApi;
import com.aboutstays.model.entertainment.EntertainmentData;
import com.aboutstays.model.entertainment.EntertainmentItemDetails;
import com.aboutstays.model.entertainment.EntertainmentResponseApi;
import com.aboutstays.model.reservation.TypeInfoList;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by neeraj on 6/4/17.
 */

public class FragmentAllChannels extends BaseFragment implements CustomListAdapterInterface {

    private ListView lvChannels;
    private CustomListAdapter listAdapter;
    private TextView tvEmptyView;
    private List<AllEntertainmentData> entertainmentList = new ArrayList<>();
    private String genereId;
    private String serviceId;
    private String typeName;

    @Override
    public void initViews() {

        lvChannels = (ListView) findView(R.id.lv_city_guide);
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_all_channels, entertainmentList, this);
        lvChannels.setAdapter(listAdapter);
        lvChannels.setEmptyView(tvEmptyView);

        Bundle bundle = getArguments();
        if (bundle != null){
            serviceId = bundle.getString(AppConstants.BUNDLE_KEYS.SERVICE_ID);
            TypeInfoList typeInfoList = (TypeInfoList) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (typeInfoList != null){
                genereId = typeInfoList.getGenereId();
                typeName = typeInfoList.getTypeName();

            }
        }

        if (!TextUtils.isEmpty(typeName)){
            initToolBar(typeName + " Channels");
        } else {
            initToolBar("All Channel");
        }

        if (!TextUtils.isEmpty(serviceId) && !TextUtils.isEmpty(genereId)){
            getEntertainmentItembyId(serviceId, genereId);
        } else {
            getAllEntertainmentsrvice();
        }
    }

    private void getEntertainmentItembyId(String serviceId, String genereId) {
        HttpParamObject httpParamObject = ApiRequestGenerator.getAllEntertainMentServicebyCategory(serviceId, genereId);
        executeTask(AppConstants.TASK_CODES.ALL_ENTERTAINMENT_SERVICE, httpParamObject);
    }

    private void getAllEntertainmentsrvice() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getAllEntertainMentService();
        executeTask(AppConstants.TASK_CODES.ALL_ENTERTAINMENT_SERVICE, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.ALL_ENTERTAINMENT_SERVICE:
                AllEntertainmentResponseApi responseApi = (AllEntertainmentResponseApi) response;
                if (responseApi != null && responseApi.getError() == false){
                    List<AllEntertainmentData> entertainmentData = responseApi.getData();
                    if (CollectionUtils.isNotEmpty(entertainmentData)){
                        entertainmentList.addAll(entertainmentData);
                        listAdapter.notifyDataSetChanged();
                    }
                }
                break;
        }
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_city_guide;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if(convertView == null){
            convertView = inflater.inflate(resourceID, parent,false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }

        if (!TextUtils.isEmpty(serviceId)){
            holder.llGenere.setVisibility(View.GONE);
        } else {
            holder.llGenere.setVisibility(View.VISIBLE);
        }

        final AllEntertainmentData entertainmentResponseApi = entertainmentList.get(position);
        EntertainmentItemDetails itemDetails = entertainmentResponseApi.getItemDetails();
        if (itemDetails != null && itemDetails.getGenereDetails() != null){
            holder.tvName.setText(itemDetails.getName());
            if (!TextUtils.isEmpty(serviceId) && !TextUtils.isEmpty(genereId)){
               // holder.tvChennelGen.setVisibility(View.GONE);
                holder.tvChennelGen.setText(typeName);
            } else {
                holder.tvChennelGen.setVisibility(View.VISIBLE);
                holder.tvChennelGen.setText(itemDetails.getGenereDetails().getName());
            }
            holder.tvChannelNum.setText(entertainmentResponseApi.getNumber());
            String imageUrl = itemDetails.getImageUrl();
            if (!TextUtils.isEmpty(imageUrl)){
                Picasso.with(getActivity()).load(imageUrl).into(holder.ivChannelPic);
            }
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, entertainmentResponseApi);
                FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.FRAGMENT_ENTERTAINMENT_DETAILS, bundle);
            }
        });

        return convertView;
    }

    class Holder{
        TextView tvName, tvChannelNum, tvChennelGen;
        ImageView ivChannelPic;
        LinearLayout llGenere;

        public Holder (View view){
            tvName = (TextView) view.findViewById(R.id.tv_channel_name);
            tvChennelGen = (TextView) view.findViewById(R.id.tv_channel_genere);
            tvChannelNum = (TextView) view.findViewById(R.id.tv_channel_no);
            ivChannelPic = (ImageView) view.findViewById(R.id.iv_channel_pic);
            llGenere = (LinearLayout) view.findViewById(R.id.ll_genere);
        }
    }

}
