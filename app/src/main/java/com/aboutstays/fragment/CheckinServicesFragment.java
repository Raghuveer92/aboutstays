package com.aboutstays.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.CheckInDetailsActivity;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.activity.checkout.InitiateCheckoutActivity;
import com.aboutstays.adapter.BaseRecycleAdapter;
import com.aboutstays.enums.ServiceType;
import com.aboutstays.model.BaseAdapterModel;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.currentStays.GetAllServicesData;
import com.aboutstays.model.currentStays.StaysService;
import com.aboutstays.model.initiate_checkIn.CheckInData;
import com.aboutstays.model.stayline.HotelStaysList;
import com.aboutstays.rest.request.BaseServiceRequest;
import com.aboutstays.rest.response.GeneralServiceData;
import com.aboutstays.rest.response.GetAllServicesResponse;
import com.aboutstays.rest.response.UserOptedService;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by ajay on 23/12/16.
 */

public class CheckinServicesFragment extends BaseFragment implements BaseRecycleAdapter.RecyclerClickListener {

    private RecyclerView rvService;
    private BaseRecycleAdapter baseRecyclerAdapter;
    private List<BaseAdapterModel> serviceModelList = new ArrayList<>();
    private Integer[] checkInLogo, stayLogo, checkoutLogo;
    private String[] checkinService, stayService, checkOutService;
    private Button btnCheckIn;
    private TextView tvBottomMsg;
    private String selectId;
    private boolean checkIn, stay, checkOut;
    private String isGI = "false";
    private String stayStatus = "1";

    private String hotelId;
    private String stayId;
    private String userId;
    private HotelStaysList hotelStaysList;
    private LinearLayout ll_btn, ll_msg;
    private boolean isGetUserOptedCallFinished = false;
    private boolean isGetGeneralServiceCallFinished = false;
    private boolean isActive = false;
    private List<UserOptedService> listUserOptedServices = new ArrayList<>();
    private Map<String, GeneralServiceModel> mapGSIdVSModel = new HashMap<>();
    private GeneralServiceModel generalServiceModel;
    private CheckInData checkinData;

    private List<StaysService> listStaysService = new ArrayList<>();
    private boolean showCheckInButton;
    private boolean showCheckoutButton;
    private String checkinButtonText;

    private void refreshServiceModelList() {
        serviceModelList.clear();
        for(StaysService staysService : listStaysService) {
            if(staysService == null) {
                continue;
            }
            GeneralServiceModel model = new GeneralServiceModel();
            model.setHotelStaysList(hotelStaysList);

            GeneralServiceData serviceData = new GeneralServiceData();
            serviceData.setActivatedImageUrl(staysService.getServiceImageUrl());
            serviceData.setName(staysService.getServiceName());
            serviceData.setHsId(staysService.getServiceId());
            serviceData.setId(staysService.getGsId());
            serviceData.setServiceType(staysService.getServiceType());
            serviceData.setUiOrder(staysService.getUiOrder());
            serviceData.setStatus(staysService.getStaysStatus());
            model.setGeneralServiceData(serviceData);
            model.setShowTick(staysService.isShowTick());
            model.setShowNumber(staysService.isShowNumber());
            model.setHeadCount(staysService.getCount());
            model.setClickable(staysService.isClickable());
            model.setUosId(staysService.getRelevantUosId());
            model.setReferenceId(staysService.getReferenceId());
            serviceModelList.add(model);
        }
        if (baseRecyclerAdapter !=null) {
            baseRecyclerAdapter.notifyDataSetChanged();
        }
    }



    public static CheckinServicesFragment getInstance(String selectId) {
        CheckinServicesFragment fragment = new CheckinServicesFragment();
        fragment.selectId = selectId;
        return fragment;
    }

    @Override
    public void initViews() {

        UserSession userSession = UserSession.getSessionInstance();
        if (userSession != null) {
            userId = userSession.getUserId();
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            hotelStaysList = (HotelStaysList) bundle.getSerializable(AppConstants.BUNDLE_KEYS.HOTEL_CHECK_IN);
            if (hotelStaysList != null && hotelStaysList.getStaysPojo() != null) {
                stayId = hotelStaysList.getStaysPojo().getStaysId();
                hotelId = hotelStaysList.getStaysPojo().getHotelId();
            }
        }

        ll_btn = (LinearLayout) findView(R.id.ll_btn);
        ll_msg = (LinearLayout) findView(R.id.ll_msg);
        btnCheckIn = (Button) findView(R.id.btn_check_in);
        tvBottomMsg = (TextView) findView(R.id.tv_msg);
        rvService = (RecyclerView) findView(R.id.rv_service);
        baseRecyclerAdapter = new BaseRecycleAdapter(getActivity(), serviceModelList);
        rvService.setLayoutManager(new GridLayoutManager(getActivity(), 3));
//        rvService.addItemDecoration(new SpacesItemDecoration(16));
        rvService.setAdapter(baseRecyclerAdapter);
        //baseRecyclerAdapter.setClickInterface(this);
        baseRecyclerAdapter.setListener(this);
        stayStatus = "1";
        //loadServiceData();
        //setAllFalse();
        //checkIn = true;
        getAllServices();
        //setBottomButtonAndMsg();
        setOnClickListener(R.id.btn_check_in);
        //refreshCheckinData();
        ll_btn.setVisibility(View.GONE);
        ll_msg.setVisibility(View.GONE);
    }


    private void getAllServices() {
        BaseServiceRequest baseServiceRequest = new BaseServiceRequest();
        baseServiceRequest.setHotelId(hotelId);
        baseServiceRequest.setStaysId(stayId);
        baseServiceRequest.setUserId(userId);
        HttpParamObject httpParamObject = ApiRequestGenerator.getAllServices(baseServiceRequest);
        executeTask(AppConstants.TASK_CODES.GET_ALL_SERVICES, httpParamObject);
    }

    private void setBottomButtonAndMsg() {
        if(showCheckInButton ) {
            if(!TextUtils.isEmpty(checkinButtonText)){
                btnCheckIn.setText(checkinButtonText);
            }
            ll_btn.setVisibility(View.VISIBLE);
            ll_msg.setVisibility(View.GONE);
        } else if(showCheckoutButton) {
            ll_btn.setVisibility(View.VISIBLE);
            ll_msg.setVisibility(View.GONE);
            if(!TextUtils.isEmpty(checkinButtonText)){
                btnCheckIn.setText(checkinButtonText);
            }
        } else {
//            ll_btn.setVisibility(View.GONE);
//            ll_msg.setVisibility(View.GONE);
        }

//        isActive = isServiceActive();
//        if (checkIn) {
//            if (isActive == true) {
//                btnCheckIn.setText("Check-IN");
//                ll_btn.setVisibility(View.VISIBLE);
//                ll_msg.setVisibility(View.GONE);
//            } else {
//                ll_btn.setVisibility(View.GONE);
//                ll_msg.setVisibility(View.VISIBLE);
//                tvBottomMsg.setText(getString(R.string.check_in_btm_msg));
//            }
//        } else if (stay) {
//            ll_btn.setVisibility(View.GONE);
//            ll_msg.setVisibility(View.GONE);
//        } else if (checkOut) {
//            ll_btn.setVisibility(View.VISIBLE);
//            ll_msg.setVisibility(View.GONE);
//            btnCheckIn.setText("Check-Out");
//        }
    }

//    private void loadServiceData() {
//        serviceModelList.clear();
//        mapGSIdVSModel.clear();
//        isGetGeneralServiceCallFinished=false;
//        isGetUserOptedCallFinished=false;
//
//        if (getActivity() == null) {
//            return;
//        }
//        HttpParamObject httpParamObject = ApiRequestGenerator.getAllService(hotelId, stayStatus, isGI);
//        executeTask(AppConstants.TASK_CODES.GET_HOTEL_GENERAL_SERVICES, httpParamObject);
//
//    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        if(null == getActivity())return;
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
//            case AppConstants.TASK_CODES.GET_HOTEL_GENERAL_SERVICES:
//                serviceModelList.clear();
//                baseRecyclerAdapter.notifyDataSetChanged();
//                GetHotelGeneralServicesResponse generalServicesResponse = (GetHotelGeneralServicesResponse) response;
//                if (generalServicesResponse != null && generalServicesResponse.getError() == false) {
//                    GeneralServiceList serviceList = generalServicesResponse.getData();
//                    if (serviceList != null) {
//                        List<GeneralServiceData> listGS = serviceList.getListGS();
//                        if (listGS != null ) {
//                            Collections.sort(listGS);
//                            for (GeneralServiceData service : listGS) {
//                                if(hotelStaysList.isPast()){
//                                    if(fileterListForPastStays(service)) {
//                                        continue;
//                                    }
//                                }
//                                GeneralServiceModel model = new GeneralServiceModel();
//                                model.setGeneralServiceData(service);
//                                model.setShowNumerable(service.getNumerable());
//                                model.setShowTickable(service.getTickable());
//                                model.setHotelStaysList(hotelStaysList);
//                                model.getGeneralServiceData().setGeneralInfo(service.isGeneralInfo());
//                                model.setActive(isActive);
//                                serviceModelList.add(model);
//                                mapGSIdVSModel.put(service.getId(), model);
//                            }
//                            isGetGeneralServiceCallFinished = true;
//                            //setUserOptedServices();
//                        } else {
//                            serviceModelList.clear();
//                            //setUserOptedServices();
//                        }
//                    }
//                } else {
//
//                    showToast(generalServicesResponse.getMessage());
//                }
//                break;

            case AppConstants.TASK_CODES.GET_ALL_SERVICES:
                GetAllServicesResponse serviceResponse = (GetAllServicesResponse) response;
                if(serviceResponse != null && !serviceResponse.getError()) {
                    GetAllServicesData serviceData = serviceResponse.getData();
                    if (serviceData != null) {
                        showCheckoutButton = serviceData.isShowCheckoutButton();
                        showCheckInButton = serviceData.isShowCheckInButton();
                        checkinButtonText = serviceData.getCheckinButtonText();
                        List<StaysService> tempList = serviceData.getServicesList();
                        if (CollectionUtils.isNotEmpty(tempList)) {
                            listStaysService = new ArrayList<>();
                            listStaysService.clear();
                            for (StaysService staysService : tempList) {
                                if (staysService == null) {
                                    continue;
                                }
                                if (!staysService.isGeneralInfo()) {
                                    listStaysService.add(staysService);
                                }
                            }
                            setBottomButtonAndMsg();
                            sortServicesList();
                            refreshServiceModelList();
                        }
                    }
                }
                break;
        }
    }

    private void sortServicesList() {
        if(CollectionUtils.isNotEmpty(listStaysService)) {
            Collections.sort(listStaysService, new Comparator<StaysService>() {
                @Override
                public int compare(StaysService o1, StaysService o2) {
                    Integer uiOrder1 = o1.getUiOrder();
                    Integer uiOrder2 = o2.getUiOrder();
                    if(uiOrder1 == null && uiOrder2 == null) {
                        return 0;
                    }
                    return uiOrder1.compareTo(uiOrder2);
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_check_in:
                if(showCheckInButton){
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, hotelStaysList);
                    startNextActivityForResult(bundle, CheckInDetailsActivity.class,AppConstants.REQUEST_CODES.CHECKIN);
                }else if(showCheckoutButton){
                    InitiateCheckoutActivity.startActivity(getActivity(),hotelId,stayId);
                }
                break;
        }
    }

//    private void openDialog() {
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_success);
//
//        TextView tvTitle = (TextView) dialog.findViewById(R.id.dialog_title);
//        TextView tvMsg = (TextView) dialog.findViewById(R.id.dialog_msg);
//        Button btnOk = (Button) dialog.findViewById(R.id.btn_ok);
//
//        tvTitle.setText(getString(R.string.request_initiated));
//        tvMsg.setText(R.string.resend_msg);
//        btnOk.setText(getString(R.string.ok));
//        btnOk.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
//    }

    @Override
    public int getViewID() {
        return R.layout.fragment_services_checkin;
    }

//    public void setCheckIn(String checkIn) {
//        this.selectId = checkIn;
//        setAllFalse();
//        stayStatus = "1";
//        loadServiceData();
//        this.checkIn = true;
//        setBottomButtonAndMsg();
//    }
//
//    public void setStay(String stay) {
//        this.selectId = stay;
//        setAllFalse();
//        stayStatus = "2";
//        loadServiceData();
//        this.stay = true;
//        setBottomButtonAndMsg();
//    }
//
//    public void setCheckOut(String checkOut) {
//        this.selectId = checkOut;
//        setAllFalse();
//        stayStatus = "3";
//        loadServiceData();
//        this.checkOut = true;
//        setBottomButtonAndMsg();
//    }
//
//    private void setAllFalse() {
//        stay = false;
//        checkIn = false;
//        checkOut = false;
//    }

    @Override
    public void onItemClicked(int position, Object model, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.NEXT_ACTIVITY:
                generalServiceModel = (GeneralServiceModel) model;
                if (generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null) {
                    openServices(generalServiceModel);
                }
                break;
        }
    }

    private void openServices(GeneralServiceModel generalServiceModel) {
        int serviceType = generalServiceModel.getGeneralServiceData().getServiceType();
        ServiceType typeEnum = ServiceType.getServiceTypeByCode(serviceType);

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);

        if (typeEnum == null) {
            showToast("Under Development");
            return;
        }

        switch (typeEnum) {
//            case EXPENSE_DASHBOARD:
//
//                break;
            case COMMUTE:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.COMMUTE_FRAGMENT, bundle, AppConstants.REQUEST_CODE.ORDER_PLACED);
                return;
            case BELL_BOY:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.BELL_BOY_FRAGMENT, bundle, AppConstants.REQUEST_CODE.ORDER_PLACED);
                return;
            case MAINTENANCE:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.MAINTENANCE_FRAGMENT, bundle, AppConstants.REQUEST_CODE.ORDER_PLACED);
                return;
            case RESERVATION:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.RESERVATION_FRAGMENT, bundle, AppConstants.REQUEST_CODE.ORDER_PLACED);
                return;
            case LATE_CHECKOUT:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.LATE_CHECKOUT, bundle, AppConstants.REQUEST_CODE.ORDER_PLACED);
                return;
            case FEEDBACK_AND_SUGGESTION:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.FEEDBACK_AND_SUGGESTIONS, bundle, AppConstants.REQUEST_CODE.ORDER_PLACED);
                return;
            case MESSAGE:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.MESSAGE, bundle, AppConstants.REQUEST_CODE.ORDER_PLACED);
                return;
            case MESSAGE_CURRENT_STAY:
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.MESSAGE, bundle, AppConstants.REQUEST_CODE.ORDER_PLACED);
                return;
            default:
                if (TextUtils.isEmpty(generalServiceModel.getGeneralServiceData().getHsId())) {
                    showToast("Under Development");
                    return;
                }
        }
        Class targetClass = ServiceType.getTargetClassByServiceType(serviceType);
        if (targetClass != null) {
            Intent intent = new Intent(getActivity(), targetClass);
            intent.putExtras(bundle);
            if (!generalServiceModel.isClickable()  /*!generalServiceModel.isActive() && serviceType != ServiceType.EXPENSE_DASHBOARD.getCode()*/) {
                return;
            } else if (generalServiceModel.isUserOpted()) {
                for (UserOptedService userOptedService : listUserOptedServices) {
                    String gsId = userOptedService.getGsId();
                    String id = generalServiceModel.getGeneralServiceData().getId();
                    if (gsId.equals(id)) {
                        String refId = userOptedService.getRefId();
                        bundle.putString(AppConstants.BUNDLE_KEYS.REF_ID, refId);
                        intent.putExtras(bundle);
//                                    startNextActivity(bundle, targetClass);
                        startActivityForResult(intent, AppConstants.REQUEST_CODE.SERVICE_REQ);
                        break;
                    }
                }
            } else {
                startActivityForResult(intent, AppConstants.REQUEST_CODE.SERVICE_REQ);
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppConstants.RESULT_CODE.RESULT_DONE){
            getActivity().setResult(AppConstants.RESULT_CODE.RESULT_DONE);
            getActivity().finish();
        }
//        if (resultCode == getActivity().RESULT_OK){
//            getActivity().setResult(Activity.RESULT_OK);
//            getActivity().finish();
//        }
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK) {
//            switch (requestCode) {
//                case AppConstants.REQUEST_CODE.SERVICE_REQ:
//                    if (data != null) {
//                        int intExtra = data.getIntExtra(AppConstants.BUNDLE_KEYS.FROM_CHECKOUT, 0);
//                        if (intExtra != 0) {
//                            stayStatus = intExtra + "";
//                        } else {
//                            stayStatus = "1";
//                        }
//                        break;
//                    }
//                    //getUserOptedServices();
//                   // loadServiceData();
//                    getAllServices();
//
//            }
//        }
//    }
//

    public void setCheckinData(CheckInData checkinData) {
        //this.checkinData = checkinData;
        //refreshCheckinData();
    }

    private void refreshCheckinData() {
        if(v==null){
            return;
        }
        if(!stayStatus.equalsIgnoreCase("1")){
            return;
        }
        if(checkinData!=null){
            btnCheckIn.setText(R.string.modify_checkin);
        }else {
            btnCheckIn.setText(R.string.chech_in);
        }
    }
}


