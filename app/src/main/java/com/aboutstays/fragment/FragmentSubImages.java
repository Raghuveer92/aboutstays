package com.aboutstays.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FragmentContainerActivity;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

public class FragmentSubImages extends BaseFragment implements CustomListAdapterInterface {

    private List<String> imageList = new ArrayList<>();
    private ListView lvSubImages;
    private CustomListAdapter listAdapter;
    private TextView tvEmptyView;

    @Override
    public void initViews() {

        initToolBar("Images");

        Bundle bundle = getArguments();
        if (bundle != null){
            imageList = (List<String>) bundle.getSerializable(AppConstants.BUNDLE_KEYS.SUB_IMAGES_LIST);
        }

        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        lvSubImages = (ListView) findView(R.id.lv_sub_images);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_3_star, imageList, this);
        lvSubImages.setAdapter(listAdapter);
        lvSubImages.setEmptyView(tvEmptyView);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_sub_images;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {

        Holder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final String image = imageList.get(position);
        if (!TextUtils.isEmpty(image)){
            Picasso.with(getActivity()).load(image).placeholder(R.drawable.progress_animation).into(holder.ivImage);
        }
        holder.frameShadow.setVisibility(View.GONE);
        holder.rlBottom.setVisibility(View.GONE);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putString(AppConstants.BUNDLE_KEYS.IMAGEURL, image);
                FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.IMAGE_FRAGMENT, b);
            }
        });

        return convertView;
    }

    private class Holder {

        RelativeLayout rlBottom;
        ImageView ivImage;
        FrameLayout frameShadow;

        public Holder(View view) {
            ivImage = (ImageView) view.findViewById(R.id.iv_hotel_room);
            frameShadow = (FrameLayout) view.findViewById(R.id.frame_shadow);
            rlBottom = (RelativeLayout) view.findViewById(R.id.rl_bottom_text);
        }
    }
}
