package com.aboutstays.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.NotificationDetailsActivity;
import com.aboutstays.model.notification.NotificationsList;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by Rahul Aarya on 23-12-2016.
 */

public class NotificationFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    private List<NotificationsList> notificationsLists = new ArrayList<>();
    private CustomListAdapter listAdapter;
    private ListView notificationsListView;
    private TextView tvEmptyView;
    public static final String REQUEST_DASHBOARD_DELIVERY_TIME_UI_FORMAT = "hh:mm a, dd MMM";

    public static NotificationFragment getInstance(List<NotificationsList> notificationsLists) {
        NotificationFragment notificationFragment = new NotificationFragment();
        notificationFragment.notificationsLists = notificationsLists;
        return notificationFragment;
    }

    @Override
    public void initViews() {
        if (CollectionUtils.isNotEmpty(notificationsLists)){
            setDatatoView(notificationsLists);
        }
    }

    private void setDatatoView(List<NotificationsList> notificationsLists) {
        notificationsListView = (ListView) findView(R.id.listview_notification);
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_request_related, notificationsLists, this);
        notificationsListView.setAdapter(listAdapter);
        notificationsListView.setEmptyView(tvEmptyView);
        listAdapter.notifyDataSetChanged();
        notificationsListView.setOnItemClickListener(this);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_notification;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.row_request_related,parent,false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }

        NotificationsList ob = notificationsLists.get(position);
        Integer maxUiLines = ob.getMaxUiLines();
        if (maxUiLines != null && maxUiLines > 0) {
            holder.tvNotificationsDummyText.setMaxLines(maxUiLines);
        }
        holder.tvNotificationsTitle.setText(ob.getTitle());
        holder.tvNotificationsDummyText.setText(ob.getDescription());
        String iconUrl = ob.getIconUrl();
        if (!TextUtils.isEmpty(iconUrl)){
            Picasso.with(getActivity()).load(iconUrl).placeholder(R.drawable.progress_animation).into(holder.ivSettings);
        }
        long created = ob.getCreated();
        String dateString = new SimpleDateFormat(REQUEST_DASHBOARD_DELIVERY_TIME_UI_FORMAT).format(new Date(created));
        holder.tvNotificationsDateTime.setText(dateString.toUpperCase());
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        NotificationsList list = notificationsLists.get(i);
        Bundle b=new Bundle();
        b.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,list);
        startNextActivity(b,NotificationDetailsActivity.class);
    }

    class Holder{
        ImageView ivSettings;
        TextView tvNotificationsTitle,tvNotificationsDateTime,tvNotificationsDummyText;

        public Holder(View view){
            ivSettings = (ImageView) view.findViewById(R.id.iv_dummyImage_Row_RequestRelated);
            tvNotificationsTitle = (TextView) view.findViewById(R.id.tv_title);
            tvNotificationsDateTime = (TextView) view.findViewById(R.id.tv_date);
            tvNotificationsDummyText = (TextView) view.findViewById(R.id.tv_dummyText_Row_RequestRelated);
        }
    }


}
