package com.aboutstays.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.DrawerActivity;
import com.aboutstays.activity.LoginActivity;
import com.aboutstays.listeners.GetHotelDataListener;
import com.aboutstays.model.cart.HouseKeepingCartResponse;
import com.aboutstays.model.hotels.Amenity;
import com.aboutstays.model.hotels.Hotel;
import com.aboutstays.model.hotels.HotelData;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

import static android.app.Activity.RESULT_OK;

/**
 * Created by ajay on 9/12/16.
 */

public class DrawerFragment extends BaseFragment implements CustomListAdapterInterface,AdapterView.OnItemClickListener {

    private static DrawerLayout drawerLayout;
    DrawerActivity drawerActivity;
    List<String> values = new ArrayList<>();
    ArrayList<String> listItem = new ArrayList<>();

    private ImageView ivLogo, ivCross;
    private EditText etSearch;
    private ListView listView;
    private boolean fromStay;
    private boolean fromGuest;
    private boolean add;
    private HotelData hotelData = new HotelData();
    private CustomListAdapter listAdapter;
    ToolbarTitleListner toolbarTitleListner;
    private Boolean isLoggedIn;
    private GetHotelDataListener getHotelDataListener;
    int itemPosition;


    public static DrawerFragment getInstance(List<String> arrayList, DrawerActivity drawerActivity, DrawerLayout drawerLayout, ToolbarTitleListner toolbarTitleListner, boolean fromStay, boolean fromGuest, boolean stay) {
        DrawerFragment drawerFragment = new DrawerFragment();
        drawerFragment.values = arrayList;
        drawerFragment.drawerActivity = drawerActivity;
        drawerFragment.drawerLayout = drawerLayout;
        drawerFragment.fromStay = fromStay;
        drawerFragment.fromGuest = fromGuest;
        drawerFragment.add = stay;
        drawerFragment.toolbarTitleListner = toolbarTitleListner;
        return drawerFragment;
    }

    @Override
    public void initViews() {
        ivLogo = (ImageView) findView(R.id.iv_logo_drawer);
        ivCross = (ImageView) findView(R.id.iv_cross_drawer);
        listView = (ListView) findView(R.id.listview_drawer);

        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(Gravity.LEFT);
            }
        });

        refreshDrawerData();

    }
    private void refreshDrawerData() {
        isLoggedIn = Preferences.getData(Preferences.LOGIN_KEY, false);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_drawer, listItem, this);
        listView.setAdapter(listAdapter);
        putDataIntoList();
        listAdapter.notifyDataSetChanged();
        listView.setOnItemClickListener(this);
        if (fromStay == true){
            StayLineFragment stayLineFragment = StayLineFragment.getInstance();
            drawerActivity.addFragment(stayLineFragment,true);
            return;
        } else {
            listView.setOnItemClickListener(this);
        }
/*        if (add){
            FindHotelRetrieveFragment findHotelRetrieveFragment = FindHotelRetrieveFragment.getInstance(getHotelDataListener);
            drawerActivity.addFragment(findHotelRetrieveFragment, true);
            return;
        } else {
            listView.setOnItemClickListener(this);
        }*/
    }

    private void putDataIntoList() {
       String[] temp = getResources().getStringArray(R.array.drawer_list_item);
        int length = temp.length;
        if (fromGuest == true){
            length = length-1;
        } else {
            length = length;
        }
        listItem.clear();
        for (int i = 0; i < length; i++) {
            listItem.add(temp[i]);
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_drawer_layout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {

        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_drawer, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.tvItem.setText(listItem.get(position));

        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        listAdapter.notifyDataSetChanged();
        itemPosition = position;
        switch (itemPosition){
            case 0:
                HotelsFragment hotelsFragment = HotelsFragment.getInstance();
                getHotelDataListener=hotelsFragment;
                drawerActivity.addFragment(hotelsFragment,true);
                toolbarTitleListner.sendTitle(listItem.get(itemPosition));
                break;
            case 1:
                if(isLoggedIn) {
                    FindHotelRetrieveFragment findHotelRetrieveFragment = FindHotelRetrieveFragment.getInstance(getHotelDataListener);
                    drawerActivity.addFragment(findHotelRetrieveFragment, true);
                    toolbarTitleListner.sendTitle(listItem.get(itemPosition));
                }
                else{
                    showToast(getString(R.string.login_to_continue));
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Bundle b = new Bundle();
                    b.putBoolean(AppConstants.BUNDLE_KEYS.IS_STAY_LINE_FRAGMENT, true);
                    intent.putExtras(b);
                    startActivityForResult(intent, AppConstants.REQUEST_CODE.LOGIN_RETRIVE_STAY);
                }
                break;
            case 2:
                if(isLoggedIn){
                    StayLineFragment stayLineFragment = StayLineFragment.getInstance();
                    drawerActivity.addFragment(stayLineFragment,true);
                    toolbarTitleListner.sendTitle(listItem.get(itemPosition));
                } else{
                    showToast(getString(R.string.login_to_continue));
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Bundle b = new Bundle();
                    b.putBoolean(AppConstants.BUNDLE_KEYS.IS_STAY_LINE_FRAGMENT, true);
                    intent.putExtras(b);
//                    getActivity().finish();
                    startActivityForResult(intent, AppConstants.REQUEST_CODE.LOGIN_STAY_LINE);
                }

                break;
            case 3:
                if(isLoggedIn){
                    ProfileFragment profileFragment = ProfileFragment.getInstance();
                    drawerActivity.addFragment(profileFragment,true);
                    toolbarTitleListner.sendTitle(listItem.get(itemPosition));
                } else{
                    showToast(getString(R.string.login_to_continue));
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Bundle b = new Bundle();
                    b.putBoolean(AppConstants.BUNDLE_KEYS.IS_STAY_LINE_FRAGMENT, true);
                    intent.putExtras(b);
//                    getActivity().finish();
                    startActivityForResult(intent, AppConstants.REQUEST_CODE.LOGIN_MY_PROFILE);
                }

                break;
            case 4:
                FaqFragment faqFragment = FaqFragment.getInstance();
                drawerActivity.addFragment(faqFragment,true);
                toolbarTitleListner.sendTitle(listItem.get(itemPosition));
                break;
            case 5:
                UserSession.removeCurrentSession();
                getActivity().finish();
                startNextActivity(LoginActivity.class);
                break;
        }
        drawerLayout.closeDrawer(Gravity.LEFT);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case AppConstants.REQUEST_CODE.LOGIN_RETRIVE_STAY:
                    fromGuest = false;
                    refreshDrawerData();
                    FindHotelRetrieveFragment findHotelRetrieveFragment = FindHotelRetrieveFragment.getInstance(getHotelDataListener);
                    drawerActivity.addFragment(findHotelRetrieveFragment, true);
                    toolbarTitleListner.sendTitle(listItem.get(itemPosition));
                    break;
                case AppConstants.REQUEST_CODE.LOGIN_STAY_LINE:
                    fromGuest = false;
                    refreshDrawerData();
                    StayLineFragment stayLineFragment = StayLineFragment.getInstance();
                    drawerActivity.addFragment(stayLineFragment,true);
                    toolbarTitleListner.sendTitle(listItem.get(itemPosition));
                    break;
                case AppConstants.REQUEST_CODE.LOGIN_MY_PROFILE:
                    fromGuest = false;
                    refreshDrawerData();
                    ProfileFragment profileFragment = ProfileFragment.getInstance();
                    drawerActivity.addFragment(profileFragment,true);
                    toolbarTitleListner.sendTitle(listItem.get(itemPosition));
                    break;
            }
        }
    }

    public void setGetHotelDataListener(HotelsFragment getHotelDataListener) {
        this.getHotelDataListener = getHotelDataListener;
    }

    class Holder {
        TextView tvItem;
        public Holder(View view) {
            tvItem = (TextView) view.findViewById(R.id.tv_drawer_item);
        }
    }

    public interface ToolbarTitleListner{
        public void sendTitle(String s);
    }
}
