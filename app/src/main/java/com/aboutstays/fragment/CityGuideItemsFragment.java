package com.aboutstays.fragment;

import android.Manifest;
import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aboutstays.AboutstaysApplication;
import com.aboutstays.R;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.rest.response.CityGuideItemData;
import com.aboutstays.rest.response.GeneralItemType;
import com.aboutstays.rest.response.GetCityGuideItemResponse;
import com.aboutstays.utils.FusedLocationService;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.google.android.gms.common.ConnectionResult;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.rest.response.pojo.Address;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.GPSTracker;
import simplifii.framework.utility.Util;

import static com.facebook.FacebookSdk.getApplicationContext;

public class CityGuideItemsFragment extends BaseFragment implements CustomListAdapterInterface {

    private ListView lvCityGuideItem;
    private CustomListAdapter listAdapter;
    private List<CityGuideItemData> listCityGuideItemsDetails;
    private TextView tvEmptyView;
    private int cgType;
    private String cgId;
    private FusedLocationService.MyLocation location;
    private FusedLocationService fusedLocationService;
    private String titleName;
    private Double myLatitude = 0.0, myLongitude = 0.0, locationLat, locationLon;


    @Override
    public void initViews() {
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        lvCityGuideItem = (ListView) findView(R.id.lv_city_guide_item);
        listCityGuideItemsDetails = new ArrayList<>();
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_3_star, listCityGuideItemsDetails, this);
        lvCityGuideItem.setAdapter(listAdapter);
        lvCityGuideItem.setEmptyView(tvEmptyView);

        checkLocationPermission();

        Bundle bundle = getArguments();
        if (bundle != null) {
            cgId = bundle.getString(AppConstants.BUNDLE_KEYS.CITY_GUIDE_ID);
            GeneralItemType generalItemType = (GeneralItemType) bundle.getSerializable(AppConstants.BUNDLE_KEYS.CITY_GUIDE_ITEM_TYPE);
            if (generalItemType != null) {
                cgType = generalItemType.getType();
                titleName = generalItemType.getTypeName();
            }
        }

        if (!TextUtils.isEmpty(titleName)) {
            initToolBar(titleName);
        }
        fetchCityGuideItem();

//        lvCityGuideItem.setOnItemClickListener(this);
    }

    private void checkLocationPermission() {
        if (AboutstaysApplication.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            getLocation();
        } else {
            new TedPermission(getActivity()).setPermissionListener(new PermissionListener() {
                @Override
                public void onPermissionGranted() {
                    getLocation();
                }

                @Override
                public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                }
            }).setPermissions(Manifest.permission.ACCESS_FINE_LOCATION).check();
        }
    }

    private void getLocation() {
        fusedLocationService = new FusedLocationListener(getActivity());
        fusedLocationService.connect();
    }

/*    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        CityGuideItemData cityGuideItemData = listCityGuideItemsDetails.get(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.CITY_GUIDE_ITEM_OBJECT, cityGuideItemData);
        FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.CITY_GUIDE_ITEM_DETAIL_FRAGMENT, bundle);
    }*/

    class FusedLocationListener extends FusedLocationService {
        public FusedLocationListener(Activity mContext) {
            super(mContext);
        }

        @Override
        protected void onSuccess(MyLocation mLastKnownLocation) {
            CityGuideItemsFragment.this.location = mLastKnownLocation;
        }

        @Override
        protected void onFailed(ConnectionResult connectionResult) {

        }
    }

    private void fetchCityGuideItem() {
        if (!TextUtils.isEmpty(cgId) && cgType > 0) {
            HttpParamObject httpParamObject = ApiRequestGenerator.fetchCityGuideItemByType(cgId, cgType);
            executeTask(AppConstants.TASK_CODES.FETCH_CITY_GUIDE_ITEM_BY_TYPE, httpParamObject);
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.FETCH_CITY_GUIDE_ITEM_BY_TYPE:
                GetCityGuideItemResponse cgItemResponse = (GetCityGuideItemResponse) response;
                if (cgItemResponse != null && !cgItemResponse.getError()) {
                    if (cgItemResponse.getData() != null) {
                        listCityGuideItemsDetails.addAll(cgItemResponse.getData());
                        listAdapter.notifyDataSetChanged();
                    }
                }
                break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_city_guide_item;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        CityGuideItemHolder cityGuideItemHolder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            cityGuideItemHolder = new CityGuideItemHolder(convertView);
            convertView.setTag(cityGuideItemHolder);
        } else {
            cityGuideItemHolder = (CityGuideItemHolder) convertView.getTag();
        }

        final CityGuideItemData cityGuideItemData = listCityGuideItemsDetails.get(position);
        if (cityGuideItemData != null) {
            cityGuideItemHolder.onBindView(cityGuideItemData);
        }
        cityGuideItemHolder.ivStar.setVisibility(View.GONE);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.CITY_GUIDE_ITEM_OBJECT, cityGuideItemData);
                FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.CITY_GUIDE_ITEM_DETAIL_FRAGMENT, bundle);
            }
        });
        return convertView;
    }


    class CityGuideItemHolder {

        ImageView ivImageUrl, ivLike, ivStar;
        TextView tvName, tvSightAddress, tvDistance, tvPerson;

        public CityGuideItemHolder(View view) {
            ivImageUrl = (ImageView) view.findViewById(R.id.iv_hotel_room);
            ivLike = (ImageView) view.findViewById(R.id.iv_image_option1);
            ivStar = (ImageView) view.findViewById(R.id.iv_parttnership);
            tvName = (TextView) view.findViewById(R.id.tv_hotel_name);
            tvDistance = (TextView) view.findViewById(R.id.tv_price);
            tvSightAddress = (TextView) view.findViewById(R.id.tv_hotel_address);
            tvPerson = (TextView) view.findViewById(R.id.tv_person);
        }

        void onBindView(CityGuideItemData cityGuideItemData) {
            if (cityGuideItemData.getGeneralInfo() != null) {
                tvName.setText(cityGuideItemData.getGeneralInfo().getName());
                if (!TextUtils.isEmpty(cityGuideItemData.getGeneralInfo().getImageUrl())) {
                    Picasso.with(getActivity()).load(cityGuideItemData.getGeneralInfo().getImageUrl())
                            .error(R.mipmap.ic_general)
                            .into(ivImageUrl);
                }
//                if (cityGuideItemData.getGeneralInfo().getAddress() != null) {
//                    String sightAddress = cityGuideItemData.getGeneralInfo().getAddress().getCity()
//                            + ", " + cityGuideItemData.getGeneralInfo().getAddress().getCountry();
//                    tvSightAddress.setText(sightAddress);
//                }
                tvSightAddress.setText(cityGuideItemData.getDestinationType());
                tvPerson.setVisibility(View.VISIBLE);

                String openTime = Util.convertDateFormat(cityGuideItemData.getOpeningTime(),
                        Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
                String closingTime = Util.convertDateFormat(cityGuideItemData.getClosingTime(),
                        Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
                if (!TextUtils.isEmpty(openTime) && !TextUtils.isEmpty(closingTime)) {
                    tvPerson.setText(openTime + " - " + closingTime);
                }

                //tvPerson.setText(cityGuideItemData.getOpeningTime() + " - " + cityGuideItemData.getClosingTime());

                Address address = cityGuideItemData.getGeneralInfo().getAddress();
                if (address != null) {
                    String latitude = address.getLatitude();
                    String longitude = address.getLongitude();

                    try {
                        locationLat = Double.parseDouble(latitude.toString());
                        locationLon = Double.parseDouble(longitude.toString());
                    } catch (NumberFormatException e){
                        locationLat = 0.0;
                        locationLon = 0.0;
                    }
                }

                if (location != null) {
                    myLatitude = location.getLatitude();
                    myLongitude = location.getLongitude();
                }
            }
            if (myLatitude != 0 && myLongitude != 0 && locationLat != 0 && locationLon != 0) {
                tvDistance.setVisibility(View.VISIBLE);
                double distance = cityGuideItemData.distance(myLatitude, myLongitude, locationLat, locationLon);
                if (distance > 0){
                    double round = cityGuideItemData.round(distance, 2);
                    tvDistance.setText(round + " KM");
                }
            } else {
                tvDistance.setVisibility(View.INVISIBLE);
            }
        }
    }

}
