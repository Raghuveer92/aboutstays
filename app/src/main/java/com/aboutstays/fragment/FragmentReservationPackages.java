package com.aboutstays.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.reservation.FetchReservationByTypeResponse;
import com.aboutstays.model.reservation.ReservationByTypeData;
import com.aboutstays.model.reservation.ReservationResponseApi;
import com.aboutstays.model.reservation.ReservationTypeData;
import com.aboutstays.model.reservation.ReservationTypeResponseApi;
import com.aboutstays.rest.request.FetchReservationRequest;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import simplifii.framework.ListAdapters.CustomExpandableListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

import static android.R.attr.type;

/**
 * Created by neeraj on 4/4/17.
 */

public class FragmentReservationPackages extends BaseFragment {

    private ExpandableListView expandableListView;
    private TextView tvEmptyView;
    private String parentId;
    private GeneralServiceModel generalServiceModel;
    private CustomExpandableListAdapter customExplandableListAdapter;
    private String category;
    private String hsId;
    private Integer reservationType;
    private String reservationItemId;
    private String reservationName;

    @Override
    public void initViews() {
        Bundle bundle = getArguments();
        if (bundle != null){
            ReservationByTypeData byTypeData = (ReservationByTypeData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.RESERVATION_TYPES);
            if (byTypeData != null){
//                category = byTypeData.getCategory();
                hsId = byTypeData.getHotelId();
                reservationItemId = byTypeData.getCategoryId();
                reservationType = byTypeData.getReservationType();
                reservationName = byTypeData.getName();
            }
//            parentId = bundle.getString(AppConstants.BUNDLE_KEYS.RESERVATION_ITEM_ID);
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        }
        initToolBar(reservationName);
        expandableListView = (ExpandableListView) findView(R.id.lv_package_details);
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        expandableListView.setEmptyView(tvEmptyView);
        getAllReservationSubItems();
    }

    private void getAllReservationSubItems() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getReservationSubItems(reservationItemId, hsId);
        executeTask(AppConstants.TASK_CODES.GET_RESERVATION_SUB_ITEMS, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.GET_RESERVATION_SUB_ITEMS:
                ReservationTypeResponseApi byTypeResponse = (ReservationTypeResponseApi) response;
                if (byTypeResponse != null && byTypeResponse.getError() == false){
                    List<ReservationTypeData> reservationSubItem = byTypeResponse.getData();
                    if (CollectionUtils.isNotEmpty(reservationSubItem)){

                        LinkedHashMap<String, List<ReservationTypeData>> listMap = new LinkedHashMap<>();
                        for (ReservationTypeData typeData : reservationSubItem) {
                            String type = typeData.getSubCategory();
                            if (listMap.containsKey(type)) {
                                List<ReservationTypeData> list = listMap.get(type);
                                list.add(typeData);
                            } else {
                                List<ReservationTypeData> list = new ArrayList<>();
                                list.add(typeData);
                                listMap.put(type, list);
                            }
                        }
                        setExpandableList(listMap);
                    }
                } else {
                    tvEmptyView.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    private void setExpandableList(final LinkedHashMap<String, List<ReservationTypeData>> listMap) {
        Iterator<String> iterator = listMap.keySet().iterator();
        final List<String> keys = new ArrayList<>();
        while (iterator.hasNext()) {
            keys.add(iterator.next());
        }

        customExplandableListAdapter = new CustomExpandableListAdapter(getActivity(), R.layout.row_header_expandable, R.layout.row_house_rules, listMap, new CustomExpandableListAdapter.ExpandableListener() {
            @Override
            public int getChildSize(int parentPosition) {
                return listMap.get(keys.get(parentPosition)).size();
            }

            @Override
            public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
                convertView = inflater.inflate(resourceId, parent, false);
                setText(R.id.tv_men_women,keys.get(groupPosition),convertView);
                ImageView imageView= (ImageView) convertView.findViewById(R.id.iv_indicator);
                View view = convertView.findViewById(R.id.viewLine);
                view.setVisibility(View.VISIBLE);
                if(isExpanded){
                    imageView.setImageResource(R.mipmap.up_arrow_white);
                }else {
                    imageView.setImageResource(R.drawable.down_arrow);
                }
                imageView.setColorFilter(Color.WHITE);
                return convertView;
            }

            @Override
            public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
                Holder holder = null;
                if (convertView == null) {
                    convertView = inflater.inflate(resourceId, parent, false);
                    holder = new Holder(convertView);
                    convertView.setTag(holder);
                } else {
                    holder = (Holder) convertView.getTag();
                }
                final ReservationTypeData typeData = listMap.get(keys.get(groupPosition)).get(childPosition);
                View viewDescription = (View) convertView.findViewById(R.id.view_house_rules);
                if(isLastChild) {
                    viewDescription.setVisibility(View.GONE);
                } else {
                    viewDescription.setVisibility(View.VISIBLE);
                }

                holder.title.setText(typeData.getName());
                if(typeData.getAmountApplicable() == true) {
                    holder.tvTime.setText("\u20B9 " + typeData.getPrice());
                } else if (typeData.getDurationBased() == true){
                    holder.tvTime.setText("Duration: " + typeData.getDuration()+"");
                } else if (typeData.getAmountApplicable() == true && typeData.getDurationBased() == true) {
                    holder.tvTime.setText("Duration: " + typeData.getDuration() + " | " + "\u20B9 " + typeData.getPrice());
                } else {
                    holder.tvTime.setText("");
                }
//                holder.tvItemPrice.setText("\u20B9 " + typeData.getPrice());
//                long duration = typeData.getDuration();
//                long minutes = TimeUnit.MILLISECONDS.toMinutes(duration);
//                holder.tvDuration.setText("Duration: " + minutes + " Minutes");
//
//                String openingTime = typeData.getOpeningTime();
//                String closingTime = typeData.getClosingTime();
//                String startTime = Util.convertDateFormat(openingTime,
//                        Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
//                String endTime = Util.convertDateFormat(closingTime,
//                        Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
//                holder.tvTime.setText(startTime + " - " + endTime);

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.RESERVATION_TYPES, typeData);
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                        bundle.putInt(AppConstants.BUNDLE_KEYS.RESERVATION_TYPE_CODE, reservationType);
                        FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.FRAGMENT_RESERVATION_PACKAGE_DETAIL, bundle, AppConstants.REQUEST_CODE.ORDER_PLACED);
                    }
                });

                return convertView;
            }
        });
        expandableListView.setAdapter(customExplandableListAdapter);
        for(int x=0;x<keys.size();x++){
            expandableListView.expandGroup(x);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppConstants.RESULT_CODE.RESULT_DONE){
            getActivity().setResult(AppConstants.RESULT_CODE.RESULT_DONE);
            getActivity().finish();
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_reservation_package;
    }

    class Holder {
        TextView title, tvItemCount, tvTime;
        ImageView arrowImage;
        RelativeLayout rlCount;

        public Holder(View view) {
            title = (TextView) view.findViewById(R.id.tv_title);
            arrowImage = (ImageView) view.findViewById(R.id.iv_right_angle);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_count);
            tvItemCount = (TextView) view.findViewById(R.id.tv_item_count);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
        }
    }

}
