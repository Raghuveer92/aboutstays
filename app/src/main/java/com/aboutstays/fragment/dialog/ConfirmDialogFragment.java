package com.aboutstays.fragment.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.aboutstays.R;

/**
 * Created by admin on 4/24/17.
 */

public class ConfirmDialogFragment extends DialogFragment {
    private String title;
    private String message;
    private OnConfirmListener onConfirmListener;
    private String topButtonText;
    private String bottomButtonText;

    public static void show(FragmentManager fragmentManager,String title, String message, String topButtonText, String bottomButtonText, OnConfirmListener onConfirmListener) {
        ConfirmDialogFragment confirmDialogFragment=new ConfirmDialogFragment();
        confirmDialogFragment.title = title;
        confirmDialogFragment.message = message;
        confirmDialogFragment.topButtonText = topButtonText;
        confirmDialogFragment.bottomButtonText = bottomButtonText;
        confirmDialogFragment.onConfirmListener = onConfirmListener;
        confirmDialogFragment.show(fragmentManager,"ConfirmDialog");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.dialog_confirm,null);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);

        TextView tvTitle= (TextView) rootView.findViewById(R.id.tv_title);
        TextView tvMessage= (TextView) rootView.findViewById(R.id.tv_message);
        Button btnConfirm = (Button) rootView.findViewById(R.id.tv_confirm);
        Button btnCancel = (Button) rootView.findViewById(R.id.tv_cancel);

        tvTitle.setText(title);
        tvMessage.setText(message);

        if(!TextUtils.isEmpty(topButtonText)) {
            btnConfirm.setText(topButtonText);
        }

        if(!TextUtils.isEmpty(bottomButtonText)) {
            btnCancel.setText(bottomButtonText);
        }

        rootView.findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                onConfirmListener.confirm();
            }
        });
        rootView.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return rootView;
    }

    public interface OnConfirmListener{
        void confirm();
    }
}
