package com.aboutstays.fragment.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.reservation.ReservationByTypeData;
import com.aboutstays.model.reservation.ReservationTypeData;

/**
 * Created by admin on 4/24/17.
 */

public class ReservationDialogFragment extends DialogFragment {
    private String title;
    private String subTitle;
    private ReservationTypeData reservationByTypeData;
    private OnConfirmListener onConfirmListener;
    private int count;

    public static void show(FragmentManager fragmentManager,String title,String subTitle,int count,ReservationTypeData reservationByTypeData, OnConfirmListener onConfirmListener) {
        ReservationDialogFragment confirmDialogFragment=new ReservationDialogFragment();
        confirmDialogFragment.title = title;
        confirmDialogFragment.subTitle = subTitle;
        confirmDialogFragment.count = count;
        confirmDialogFragment.reservationByTypeData = reservationByTypeData;
        confirmDialogFragment.onConfirmListener = onConfirmListener;
        confirmDialogFragment.show(fragmentManager,"ConfirmDialog");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.dialog_reservation,null);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setCancelable(false);

        TextView tvTitle= (TextView) rootView.findViewById(R.id.tv_title);
        tvTitle.setText(title);

        TextView tvSubTitle= (TextView) rootView.findViewById(R.id.tv_subtitle);
        tvSubTitle.setText(subTitle);

        TextView tvItemName= (TextView) rootView.findViewById(R.id.tv_item_name);
        tvItemName.setText(reservationByTypeData.getName());

        TextView tvItemDes= (TextView) rootView.findViewById(R.id.tv_item_description);

        if(reservationByTypeData.getAmountApplicable() == true) {
            tvItemDes.setText("\u20B9 " + reservationByTypeData.getPrice()*count);
        } else if (reservationByTypeData.getDurationBased() == true){
            tvItemDes.setText("Duration: " + reservationByTypeData.getDuration()+"");
        } else if (reservationByTypeData.getAmountApplicable() == true && reservationByTypeData.getDurationBased() == true) {
            tvItemDes.setText(reservationByTypeData.getDuration() + " Minutes" +  " | " + "\u20B9 " + reservationByTypeData.getPrice()*count);
        } else {
            tvItemDes.setText("");
        }

        TextView tvItemCont= (TextView) rootView.findViewById(R.id.tv_item_count);
        tvItemCont.setText(count+"");

        rootView.findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                onConfirmListener.confirm();
            }
        });
        return rootView;
    }

    public interface OnConfirmListener{
        void confirm();
    }
}
