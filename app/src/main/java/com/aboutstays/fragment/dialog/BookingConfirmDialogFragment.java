package com.aboutstays.fragment.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.aboutstays.R;

/**
 * Created by admin on 4/24/17.
 */

public class BookingConfirmDialogFragment extends DialogFragment {
    private String title;
    private String subtitle;
    private String itemName;
    private String itemDescription;
    private String itemTime;
    private String message;
    private OnSuccessListener onConfirmListener;

    public static void show(FragmentManager fragmentManager,String title,String subtitle,String itemName,String itemDescription,String itemTime, String message, OnSuccessListener onConfirmListener) {
        BookingConfirmDialogFragment confirmDialogFragment=new BookingConfirmDialogFragment();
        confirmDialogFragment.title = title;
        confirmDialogFragment.subtitle = subtitle;
        confirmDialogFragment.itemName = itemName;
        confirmDialogFragment.itemDescription = itemDescription;
        confirmDialogFragment.itemTime = itemTime;
        confirmDialogFragment.message = message;
        confirmDialogFragment.onConfirmListener = onConfirmListener;
        confirmDialogFragment.show(fragmentManager,"SuccessDialog");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.dialog_slot_confirmed,null);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().onBackPressed();


        TextView tvTitle= (TextView) rootView.findViewById(R.id.dialog_title);
        tvTitle.setText(title);

        TextView tvMessage= (TextView) rootView.findViewById(R.id.tv_message);
        tvMessage.setText(message);

        TextView tvSubTitle= (TextView) rootView.findViewById(R.id.tv_subtitle);
        tvSubTitle.setText(subtitle);

        TextView tvName= (TextView) rootView.findViewById(R.id.tv_name_title);
        tvName.setText(itemName);

        TextView tvPrice= (TextView) rootView.findViewById(R.id.tv_time_price);
        tvPrice.setText(itemDescription);

        TextView tvTime= (TextView) rootView.findViewById(R.id.tv_time);
        tvTime.setText(itemTime);


        Button btnOk = (Button) rootView.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                onConfirmListener.done();
            }
        });
        return rootView;
    }

    public interface OnSuccessListener {
        void done();
    }
}
