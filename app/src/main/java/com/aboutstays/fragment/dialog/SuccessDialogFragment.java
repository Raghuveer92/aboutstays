package com.aboutstays.fragment.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.aboutstays.R;

/**
 * Created by admin on 4/24/17.
 */

public class SuccessDialogFragment extends DialogFragment {
    private String title;
    private String subTitle;
    private String message;
    private String btnText;
    private OnSuccessListener onConfirmListener;

    public static void show(FragmentManager fragmentManager,String title,String subTitle, String message,String btnText, OnSuccessListener onConfirmListener) {
        SuccessDialogFragment confirmDialogFragment=new SuccessDialogFragment();
        confirmDialogFragment.title = title;
        confirmDialogFragment.subTitle = subTitle;
        confirmDialogFragment.btnText = btnText;
        confirmDialogFragment.message = message;
        confirmDialogFragment.onConfirmListener = onConfirmListener;
        confirmDialogFragment.show(fragmentManager,"SuccessDialog");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.dialog_success,null);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCancelable(false);
        getDialog().onBackPressed();
        getDialog().setCanceledOnTouchOutside(false);

        TextView tvTitle= (TextView) rootView.findViewById(R.id.dialog_title);
        TextView tvSubTitle= (TextView) rootView.findViewById(R.id.dialog_sub_title);
        TextView tvMessage= (TextView) rootView.findViewById(R.id.dialog_msg);

        tvTitle.setText(title);
        if(!TextUtils.isEmpty(subTitle)){
            tvSubTitle.setText(subTitle);
        }else {
            tvSubTitle.setVisibility(View.GONE);
        }
        tvMessage.setText(message);

        Button btnOk = (Button) rootView.findViewById(R.id.btn_ok);
        btnOk.setText(btnText);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                onConfirmListener.done();
            }
        });
        return rootView;
    }

    public interface OnSuccessListener {
        void done();
    }
}
