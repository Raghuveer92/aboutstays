package com.aboutstays.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.cart.ItemOrderDetails;
import com.aboutstays.model.cart.LaundryCart;
import com.aboutstays.model.cart.LaundryCartItemData;
import com.aboutstays.model.cart.LaundryCartResponse;
import com.aboutstays.model.cart.LaundryServiceCartData;
import com.aboutstays.model.cart.ListLaundryItemOrderDetail;
import com.aboutstays.model.internet.InternetPackList;
import com.aboutstays.model.laundry.LaundryItemData;
import com.aboutstays.model.laundry.LaundryItemResponceApi;
import com.aboutstays.receiver.StayCartUpdateReceiver;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import simplifii.framework.ListAdapters.CustomExpandableListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by neeraj on 15/3/17.
 */

public class LaundryItemFragment extends BaseFragment implements CustomExpandableListAdapter.ExpandableListener {

    private String serviceId;
    private List<LaundryItemData> itemDataList = new ArrayList<>();
    private TextView tvEmptyView;
    private String query;
    private ProgressBar progressBar;
    private String userId, staysId;
    private LaundryCart laundryCart;
    private String laundryType;
    private AddToCartListener addToCartListener;
    private boolean fromSameDay = false;


    private LinkedHashMap<String, List<LaundryItemData>> mapExpandable = new LinkedHashMap<>();
    private LinkedHashMap<String, List<LaundryItemData>> mapExpandableAll = new LinkedHashMap<>();
    private List<String> listHeader = new ArrayList<>();
    private ExpandableListView expandableListView;
    private CustomExpandableListAdapter customExpandableListAdapter;

    public static LaundryItemFragment getInstance(AddToCartListener addToCartListener) {
        LaundryItemFragment laundryItemFragment = new LaundryItemFragment();
        laundryItemFragment.addToCartListener = addToCartListener;
        return laundryItemFragment;
    }

    @Override
    public void initViews() {

        progressBar = (ProgressBar) findView(R.id.progressbar);
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);

        expandableListView = (ExpandableListView) findView(R.id.list_expandable_laundry);
        customExpandableListAdapter = new CustomExpandableListAdapter(getActivity(), R.layout.row_header_expandable, R.layout.row_epadable_child_laundry, mapExpandable, this);
        expandableListView.setAdapter(customExpandableListAdapter);
        expandableListView.setEmptyView(tvEmptyView);


        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            userId = sessionInstance.getUserId();
        }

        if (!TextUtils.isEmpty(query) && query.length() >= 3) {
            getSearchedData();
        } else {
            getLaundryItemData();
        }
    }

    @Override
    public void onPreExecute(int taskCode) {
        if (taskCode == AppConstants.TASK_CODES.ADD_REMOVE_LAUNDRY_TO_CART) {
            return;
        }
        super.onPreExecute(taskCode);
    }

    public void addItemToCart() {
        List<LaundryServiceCartData> serviceCartDataList = new ArrayList<>();
        LaundryCart laundryCartFill = new LaundryCart();
        for (LaundryItemData itemData : itemDataList) {
            if (fromSameDay) {
                if (!itemData.isSameDayAvailable()) {
                    continue;
                }
            }
            laundryCartFill.setUserId(userId);
            laundryCartFill.setHotelId(itemData.getHotelId());
            laundryCartFill.setStaysId(staysId);

            LaundryServiceCartData cartData = new LaundryServiceCartData();
            cartData.setCartType(3);
            cartData.setCartOperation(1);

            ItemOrderDetails itemOrderDetails = new ItemOrderDetails();
            itemOrderDetails.setLaundryItemId(itemData.getItemId());
            itemOrderDetails.setType(itemData.getType());
            if (fromSameDay && itemData.getSameDayPrice() > 0) {
                itemOrderDetails.setPrice(itemData.getSameDayPrice());
                itemOrderDetails.setSameDaySelected(true);
            } else {
                itemOrderDetails.setPrice(itemData.getPrice());
                itemOrderDetails.setSameDaySelected(false);
            }
            itemOrderDetails.setQuantity(itemData.getQuqntity());

            cartData.setItemOrderDetails(itemOrderDetails);
            serviceCartDataList.add(cartData);
            laundryCartFill.setData(serviceCartDataList);
        }

        if (laundryCartFill != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.addOrRemoveLaundryItem(laundryCartFill);
            executeTask(AppConstants.TASK_CODES.ADD_REMOVE_LAUNDRY_TO_CART, httpParamObject);
        }
    }

    private List<LaundryItemData> getListFromMap() {
        List<LaundryItemData> itemDatas = new ArrayList<>();
        for (Map.Entry<String, List<LaundryItemData>> listEntry : mapExpandable.entrySet()) {
            itemDatas.addAll(listEntry.getValue());
        }
        return itemDatas;
    }

    public void getLaundryItemData() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getLaundryItemById(serviceId, laundryType);
        executeTask(AppConstants.TASK_CODES.GET_LAUNDRY_ITEM, httpParamObject);
    }

    public void getSearchedData() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getLaundryItemBySearch(serviceId, query);
        executeTask(AppConstants.TASK_CODES.GET_LAUNDRY_ITEM, httpParamObject);
        hideProgressBar();
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
//        addToCartListener.searchCallback();
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        progressBar.setVisibility(View.GONE);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_LAUNDRY_ITEM:
                LaundryItemResponceApi laundryItemResponceApi = (LaundryItemResponceApi) response;
                if (laundryItemResponceApi != null && laundryItemResponceApi.getError() == false) {
                    List<LaundryItemData> dataList = laundryItemResponceApi.getData();
                    if (!CollectionUtils.isEmpty(dataList)) {
//                        boolean ifShown = checkShowOnApp(dataList);
//                        if (ifShown == true) {
                        itemDataList.clear();
                        for (LaundryItemData itemData : dataList) {
                            if (itemData.getShowOnApp() == true) {
                                itemDataList.add(itemData);
                            }
                        }
//                            itemDataList.addAll(dataList);
                        checkCartItenQuantity(itemDataList);
                        filterList(itemDataList);
                    }
                } else {
                    tvEmptyView.setVisibility(View.VISIBLE);
                }
                break;
            case AppConstants.TASK_CODES.ADD_REMOVE_LAUNDRY_TO_CART:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false) {
                    getActivity().setResult(AppConstants.RESULT_CODE.RESULT_NULL);
                }
//                StayCartUpdateReceiver.callOnClear(getActivity());
                break;
        }
    }

    private boolean checkShowOnApp(List<LaundryItemData> dataList) {
        for (LaundryItemData itemData : dataList) {
            if (itemData.getShowOnApp() == true) {
                return true;
            }
        }
        return false;
    }

    private void checkCartItenQuantity(List<LaundryItemData> dataList) {
        List<ListLaundryItemOrderDetail> listLaundryItemOrderDetails = LaundryCartResponse.getInstance().getListLaundryItemOrderDetails();
        for (LaundryItemData itemData : dataList) {
            for (ListLaundryItemOrderDetail itemOrderDetail : listLaundryItemOrderDetails) {
                if (itemData.getItemId().equals(itemOrderDetail.getLaundryItemId())) {
                    if ((itemOrderDetail.isSameDaySelected() && fromSameDay) || (!itemOrderDetail.isSameDaySelected() && !fromSameDay)) {
                        itemData.setQuqntity(itemOrderDetail.getQuantity());
                    }
                }
            }
        }
    }

    private void filterList(List<LaundryItemData> dataList) {
        listHeader.clear();
        mapExpandableAll.clear();
        mapExpandable.clear();

        for (LaundryItemData laundryList : dataList) {
            String category = laundryList.getCategory();
            if (!TextUtils.isEmpty(category)) {
                if (mapExpandable.containsKey(category)) {
                    List<LaundryItemData> laundryItemDatas = mapExpandable.get(category);
                    laundryItemDatas.add(laundryList);
                } else {
                    List<LaundryItemData> laundryItemDatas = new ArrayList<>();
                    laundryItemDatas.add(laundryList);
                    mapExpandable.put(category, laundryItemDatas);
                }
            }
        }
        refreshParentData();
        mapExpandableAll.putAll(mapExpandable);
        customExpandableListAdapter.notifyDataSetChanged();
    }

    private void refreshParentData() {
        listHeader.clear();
        Iterator<String> iterator = mapExpandable.keySet().iterator();
        while (iterator.hasNext()) {
            listHeader.add(iterator.next());
        }
        customExpandableListAdapter = new CustomExpandableListAdapter(getActivity(), R.layout.row_header_expandable, R.layout.row_epadable_child_laundry, mapExpandable, this);
        expandableListView.setAdapter(customExpandableListAdapter);
        for (int x = 0; x < listHeader.size(); x++) {
            expandableListView.expandGroup(x);
        }
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_laundry_item;
    }

    public void sendQueryData(String serviceId, String query, String staysId, LaundryCartItemData laundryCartData) {
        this.serviceId = serviceId;
        this.query = query;
        this.staysId = staysId;
        if (query.length() >= 3) {
            getSearchedData();
        }
    }

    public void sendData(String serviceId, String staysId, LaundryCartItemData cartData, String laundryType, boolean fromSameDay) {
        this.serviceId = serviceId;
        this.staysId = staysId;
        this.fromSameDay = fromSameDay;
        this.laundryType = laundryType;
    }

    public void callBack() {
        addToCartListener.searchCallback();
    }

    @Override
    public int getChildSize(int parentPosition) {
        return mapExpandable.get(listHeader.get(parentPosition)).size();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        HolderParent holderParent;
//        if (convertView == null) {
//            convertView = inflater.inflate(resourceId, null);
//            holderParent = new HolderParent(convertView);
//            convertView.setTag(holderParent);
//        } else {
//            holderParent = (HolderParent) convertView.getTag();
//        }
        convertView = inflater.inflate(resourceId, null);
        holderParent = new HolderParent(convertView);
        String title = listHeader.get(groupPosition);
        holderParent.bindData(title, isExpanded);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        HolderChild holderChild;
//        if (convertView == null) {
//            convertView = inflater.inflate(resourceId, parent, false);
//            holderChild = new HolderChild(convertView);
//            convertView.setTag(holderChild);
//        } else {
//            holderChild = (HolderChild) convertView.getTag();
//        }
        convertView = inflater.inflate(resourceId, parent, false);
        holderChild = new HolderChild(convertView);

        if (isLastChild) {
            holderChild.viewBottomLine.setVisibility(View.INVISIBLE);
        } else {
            holderChild.viewBottomLine.setVisibility(View.VISIBLE);
        }

        final LaundryItemData laundryItemData = mapExpandable.get(listHeader.get(groupPosition)).get(childPosition);

//        Boolean showOnApp = laundryItemData.getShowOnApp();
//        if (!showOnApp) {
//            holderChild.rl_row_data.setVisibility(View.GONE);
//            holderChild.viewBottomLine.setVisibility(View.GONE);
//        } else {
            setChildView(laundryItemData, holderChild);
//        }

        return convertView;
    }

    private void setChildView(final LaundryItemData laundryItemData, HolderChild holderChild) {
        holderChild.laundryTitle.setText(laundryItemData.getName());
        if (fromSameDay && laundryItemData.getSameDayPrice() != null && laundryItemData.getSameDayPrice() != 0) {
            holderChild.tvPrice.setText("\u20B9 " + laundryItemData.getSameDayPrice());
        } else {
            holderChild.tvPrice.setText("\u20B9 " + laundryItemData.getPrice());
        }

        holderChild.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                laundryItemData.setQuqntity(1);
                addItemToCart();
                refreshCartData(laundryItemData);
                customExpandableListAdapter.notifyDataSetChanged();
            }
        });

        holderChild.tvCount.setText(laundryItemData.getQuqntity() + "");

        holderChild.tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quqntity = laundryItemData.getQuqntity();
                laundryItemData.setQuqntity(quqntity + 1);
                addItemToCart();
                refreshCartData(laundryItemData);
                customExpandableListAdapter.notifyDataSetChanged();
            }
        });

        holderChild.tvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quqntity = laundryItemData.getQuqntity();
                if (quqntity >= 1) {
                    laundryItemData.setQuqntity(quqntity - 1);
                    addItemToCart();
                    refreshCartData(laundryItemData);
                    customExpandableListAdapter.notifyDataSetChanged();
                }
            }
        });

        if (laundryItemData.getQuqntity() >= 1) {
            holderChild.tvAdd.setVisibility(View.GONE);
            holderChild.rl_add_count.setVisibility(View.VISIBLE);
        } else {
            holderChild.tvAdd.setVisibility(View.VISIBLE);
            holderChild.rl_add_count.setVisibility(View.GONE);
        }

    }

    private void refreshCartData(LaundryItemData laundryItemData) {
        LaundryCartItemData.refreshItem(laundryItemData, fromSameDay, getActivity());
        addToCartListener.updateCart();
    }

    public void searchQuery(String query) {
        mapExpandable.clear();
        for (Map.Entry<String, List<LaundryItemData>> entry : mapExpandableAll.entrySet()) {
            List<LaundryItemData> packLists = entry.getValue();
            List<LaundryItemData> packListsSelected = new ArrayList<>();
            for (LaundryItemData internetPackList : packLists) {
                if (internetPackList.getName().toLowerCase().contains(query.toLowerCase())) {
                    packListsSelected.add(internetPackList);
                }
            }
            if (packListsSelected.size() > 0) {
                mapExpandable.put(entry.getKey(), packListsSelected);
            }
        }
        refreshParentData();
    }


    public void refreshData() {
        if (!TextUtils.isEmpty(query) && query.length() >= 3) {
            getSearchedData();
        } else {
            getLaundryItemData();
        }
    }


    class HolderChild {
        TextView laundryTitle, tvPrice, tvAdd, tvPlus, tvMinus, tvCount;
        private RelativeLayout rl_add_count, rl_row_data;
        private View viewBottomLine;

        public HolderChild(View view) {
            laundryTitle = (TextView) view.findViewById(R.id.tv_title);
            tvPrice = (TextView) view.findViewById(R.id.tv_price);
            tvAdd = (TextView) view.findViewById(R.id.tv_add);
            tvPlus = (TextView) view.findViewById(R.id.tv_plus);
            tvMinus = (TextView) view.findViewById(R.id.tv_minus);
            tvCount = (TextView) view.findViewById(R.id.tv_count);
            rl_add_count = (RelativeLayout) view.findViewById(R.id.rl_add_count);
            rl_row_data = (RelativeLayout) view.findViewById(R.id.rl_row_data);
            viewBottomLine = (View) view.findViewById(R.id.view_bottom_line);
        }
    }

    class HolderParent {
        TextView textView;
        ImageView ivIndicator;

        public HolderParent(View view) {
            textView = (TextView) view.findViewById(R.id.tv_men_women);
            ivIndicator = (ImageView) view.findViewById(R.id.iv_indicator);
        }

        public void bindData(String title, boolean isExpand) {
            textView.setText(title);
            if (isExpand) {
                ivIndicator.setImageResource(R.mipmap.up_arrow_white);
            } else {
                ivIndicator.setImageResource(R.mipmap.arrow_down_white);
            }
        }
    }

    public interface AddToCartListener {
        void searchCallback();

        void updateCart();
    }

}
