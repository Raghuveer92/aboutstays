package com.aboutstays.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.HotelNameActivity;
import com.aboutstays.model.hotels.GeneralInformation;
import com.aboutstays.model.hotels.Hotel;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by ajay on 8/12/16.
 */

public class FourStarFragment  extends BaseFragment implements CustomListAdapterInterface {
    private CustomListAdapter listAdapter;
    private List<Hotel> fourStarList = new ArrayList<>();
    private ListView fourStarListView;
    private TextView tvError;


    public static FourStarFragment getInstance(){
        FourStarFragment fourStarFragment = new FourStarFragment();
        return  fourStarFragment;
    }
    @Override
    public void initViews() {
       if (fourStarList!= null){
           setDatatoView(fourStarList);
       }
    }

    private void putDataIntoList() {
//        for(int i=0; i < 5; i++){
//            HotelModel ob = new HotelModel();
//
//            ob.setHotelName("The Taj");
//            ob.setHotelAddrass("Banglore, Country");
//            ob.setImgUrl(R.mipmap.cambriasuites);
//            ob.setRightImgUrl(R.mipmap.google_plus);
//
//            fourStarList.add(ob);
//        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_four_star;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {

        Holder holder = null;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.row_3_star,parent,false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }

        Hotel ob = fourStarList.get(position);
        if (ob != null){
            GeneralInformation generalInformation = ob.getGeneralInformation();
            holder.tvHotelName.setText(generalInformation.getName());
            holder.tvHotelAddress.setText(generalInformation.getAddress().getAddressLine1());
//            holder.ivHotelRoom.setImageResource(generalInformation.);
//            holder.ivRightSide.setImageResource(ob.getRightImgUrl());

        }

        return convertView;
    }

    public void setData(List<Hotel> fourStarList) {
        this.fourStarList = fourStarList;
        if (isVisible()){
            setDatatoView(fourStarList);
        }

    }

    private void setDatatoView(List<Hotel> fourStarList) {
        tvError = (TextView) findView(R.id.tv_error);
        fourStarListView = (ListView) findView(R.id.listview_4_star);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_3_star, fourStarList, this);
        fourStarListView.setAdapter(listAdapter);
        putDataIntoList();
        listAdapter.notifyDataSetChanged();

        fourStarListView.setEmptyView(tvError);

        fourStarListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startNextActivity(HotelNameActivity.class);
            }
        });
    }

    class Holder{
        RatingBar ratingbar;
        ImageView ivHotelRoom,ivRightSide;
        TextView tvHotelName,tvHotelAddress;

        public Holder(View view){
            //ratingbar = (RatingBar) view.findViewById(R.id.ratindbar);
            ivHotelRoom = (ImageView) view.findViewById(R.id.iv_hotel_room);
            ivRightSide = (ImageView) view.findViewById(R.id.iv_image_option1);

            tvHotelName = (TextView) view.findViewById(R.id.tv_hotel_name);
            tvHotelAddress = (TextView) view.findViewById(R.id.tv_hotel_address);
        }
    }

}
