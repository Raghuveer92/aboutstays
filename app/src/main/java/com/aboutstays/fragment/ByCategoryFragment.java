package com.aboutstays.fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.rest.response.ExpenseItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.Util;

/**
 * Created by Rahul Aarya on 29-12-2016.
 */

public class ByCategoryFragment extends BaseFragment implements CustomListAdapterInterface {

    private CustomListAdapter listAdapter;
    private List<ExpenseItem> listExpenseItem;
    private ListView expenceCategoryListView;
    private TextView tvEmptyView;

    public static ByCategoryFragment getInstance() {
        ByCategoryFragment byCategoryFragment = new ByCategoryFragment();
        return byCategoryFragment;
    }

    public void setListExpenseItem(List<ExpenseItem> listExpenseItem) {
        this.listExpenseItem.clear();
        if(listExpenseItem!=null)
            this.listExpenseItem.addAll(listExpenseItem);
    }

    @Override
    public void refreshData() {
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void initViews() {
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        expenceCategoryListView = (ListView) findView(R.id.listView_expence_category);
        listExpenseItem = new ArrayList<>();
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_by_category, listExpenseItem, this);
        expenceCategoryListView.setAdapter(listAdapter);
        expenceCategoryListView.setEmptyView(tvEmptyView);
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_by_category;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ExpenseItemHolder expenseItemHolder = null;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.row_by_category, parent,false);
            expenseItemHolder = new ExpenseItemHolder(convertView);
            convertView.setTag(expenseItemHolder);
        }else{
            expenseItemHolder = (ExpenseItemHolder) convertView.getTag();
        }

        ExpenseItem expenseItem = listExpenseItem.get(position);
        expenseItemHolder.onBindView(expenseItem);
        return convertView;
    }

    private class ExpenseItemHolder{

        ImageView ivIcon;
        TextView tvName,tvTotalAmount;

        public ExpenseItemHolder(View view){
            ivIcon = (ImageView) view.findViewById(R.id.iv_title_image);
            tvName = (TextView) view.findViewById(R.id.tv_title);
            tvTotalAmount= (TextView) view.findViewById(R.id.tv_amount);
        }

        void onBindView(ExpenseItem expenseItem){
            tvName.setText(expenseItem.getName());
            String decimal = Util.formatDecimal(expenseItem.getTotal(), "0.00#");
            tvTotalAmount.setText("\u20B9 "+decimal);
            if(!TextUtils.isEmpty(expenseItem.getIconUrl())){
                Picasso.with(getActivity()).load(expenseItem.getIconUrl())
                        .into(ivIcon);
            }
        }
    }


}
