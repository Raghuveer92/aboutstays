package com.aboutstays.fragment;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.enums.HotelType;
import com.aboutstays.listeners.GetHotelDataListener;
import com.aboutstays.model.hotels.GetAllHotelsApi;
import com.aboutstays.model.hotels.HotelData;
import com.aboutstays.model.hotels.Hotel;
import com.aboutstays.services.CommonDataServices;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Preferences;

/**
 * Created by ajay on 8/12/16.
 */

public class HotelsFragment extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface,GetHotelDataListener {
    private List<StarFragment> starFragments = new ArrayList<>();
    private List<String> tabs = new ArrayList<>();
    private List<Hotel> hotelList=new ArrayList<>();

    private CustomPagerAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private DrawerLayout drawerLayout;
    private DrawerFragment drawerFragment;
    private RelativeLayout relativeLayout;
    private TextView textView, tvEmptyView;


    public static HotelsFragment getInstance() {
        HotelsFragment hotelsFragment = new HotelsFragment();
//        hotelsFragment.textView=textView;
        return hotelsFragment;

    }

    @Override
    public void initViews() {
        tabLayout = (TabLayout) findView(R.id.tablayout_hotles);
        viewPager = (ViewPager) findView(R.id.viewpager_hotles);
        adapter = new CustomPagerAdapter(getChildFragmentManager(), tabs, this);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        getAllHotels();
    }

    private void getAllHotels() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getAllHotels();
        executeTask(AppConstants.TASK_CODES.GET_HOTELS_DATA, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_HOTELS_DATA:


                GetAllHotelsApi getAllHotelsApi = (GetAllHotelsApi) response;
                if (getAllHotelsApi != null && !getAllHotelsApi.getError()) {
                    tvEmptyView.setVisibility(View.GONE);
                    HotelData data = getAllHotelsApi.getData();

                    if (data != null && CollectionUtils.isNotEmpty(data.getListHotels())) {
                        hotelList.clear();
                        hotelList.addAll(data.getListHotels());

                        Gson gson=new Gson();
                        Preferences.saveData(AppConstants.PREF_KEYS.HOTEL_DATA,gson.toJson(data));

                        List<Hotel> listHotels = data.getListHotels();

                        HashMap<Integer, List<Hotel>> map = new HashMap<>();
                        for (Hotel hotel : listHotels) {
                            if (hotel != null) {
                                int hotelType = hotel.getHotelType();
                                if (map.containsKey(hotelType)) {
                                    List<Hotel> subHotelList = map.get(hotelType);
                                    subHotelList.add(hotel);
                                } else {
                                    List<Hotel> subHotelList = new ArrayList<>();
                                    subHotelList.add(hotel);
                                    map.put(hotelType, subHotelList);
                                }
                            }
                        }

                        tabs.clear();
                        starFragments.clear();
                        Set<Integer> integers = map.keySet();
                        List<Integer> integerList = new ArrayList<>();
                        for (Integer integer : integers) {
                            integerList.add(integer);
                        }
                        Collections.sort(integerList);
                        for (Integer integer : integerList) {
                            List<Hotel> subHotelList = map.get(integer);
                            HotelType hotelByType = HotelType.findHotelByType(integer);
                            String displayName = hotelByType.getDisplayName();
                            tabs.add(displayName.toUpperCase());
                            starFragments.add(StarFragment.getInstance(subHotelList));
                            adapter.notifyDataSetChanged();
                        }
                    }
                } else {
                    tvEmptyView.setVisibility(View.VISIBLE);
                }
                break;
        }

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_hotles;
    }

    private void addDrawerFragment(DrawerFragment drawerFragment, int lay_drawer) {
        getChildFragmentManager().beginTransaction().add(lay_drawer, drawerFragment).commit();
    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.ic_drawer_menu;
    }

//    @Override
//    protected void onHomePressed() {
//        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_home);
//        drawerFragment = DrawerFragment.getInstance(drawerLayout);
//        addDrawerFragment(drawerFragment, R.id.lay_drawer);
//        drawerLayout.openDrawer(Gravity.LEFT);
//    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {

        return starFragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return tabs.get(position);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public List<Hotel> getHotelList() {
        return hotelList;
    }
}
