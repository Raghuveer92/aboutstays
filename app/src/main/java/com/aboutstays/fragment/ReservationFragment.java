package com.aboutstays.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.activity.SereneSpaActivity;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.hotels.Hotel;
import com.aboutstays.model.reservation.ReservationByTypeData;
import com.aboutstays.model.reservation.ReservationData;
import com.aboutstays.model.reservation.ReservationDataResponse;
import com.aboutstays.model.reservation.ReservationResponseApi;
import com.aboutstays.model.reservation.ReservationServicePojo;
import com.aboutstays.model.reservation.TypeInfoList;
import com.aboutstays.rest.request.FetchReservationRequest;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

import static android.R.attr.data;
import static android.R.attr.type;

/**
 * Created by neeraj on 3/4/17.
 */

public class ReservationFragment extends BaseFragment implements CustomListAdapterInterface {

    private ListView lvReservation;
    private CustomListAdapter listAdapter;
    private String hsId;
    private TextView tvEmptyView;
    List<ReservationByTypeData> reservationList = new ArrayList<>();
    private GeneralServiceModel generalServiceModel;
    private boolean fromHotel;
    private int reservationType;
    private Hotel hotelData;
    private String typeName;

    @Override
    public void initViews() {

        initToolBar(getString(R.string.reservation));

        Bundle bundle = getArguments();
        if (bundle != null) {
            fromHotel = bundle.getBoolean(AppConstants.BUNDLE_KEYS.FROM_HOTEL);
            reservationType = bundle.getInt(AppConstants.BUNDLE_KEYS.RESERVATION_TYPE);
            hotelData = (Hotel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.GENERAL_INFO);
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null) {
                hsId = generalServiceModel.getGeneralServiceData().getHsId();
            }
        }
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        lvReservation = (ListView) findView(R.id.lv_maintenance);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_3_star, reservationList, this);
        lvReservation.setAdapter(listAdapter);
        lvReservation.setEmptyView(tvEmptyView);
        listAdapter.notifyDataSetChanged();
//        lvReservation.setOnItemClickListener(this);
        if (fromHotel){
            if(hotelData != null && hotelData.getReservationServicePojo()!= null) {
               List<TypeInfoList> listTypeInfo =  hotelData.getReservationServicePojo().getTypeInfoList();
               if(CollectionUtils.isNotEmpty(listTypeInfo)) {
                   for(TypeInfoList type : listTypeInfo) {
                       if(type == null) {
                           continue;
                       }
                       if(type.getType() == reservationType) {
                           typeName = type.getTypeName();
                       }
                   }
               }
            }
            initToolBar(typeName);
            getReservationByType();
        } else {
            initToolBar(getString(R.string.reservation));
            getreservationItem();
        }
        tvEmptyView.setVisibility(View.VISIBLE);
    }

    private void getReservationByType() {
        if (hotelData != null){
            ReservationServicePojo reservationServicePojo = hotelData.getReservationServicePojo();
            if (reservationServicePojo != null){
                String id = reservationServicePojo.getId();
                FetchReservationRequest fetchReservationRequest = new FetchReservationRequest();
                fetchReservationRequest.setType(reservationType);
                fetchReservationRequest.setRsId(id);
                HttpParamObject httpParamObject = ApiRequestGenerator.getReservationItemListByType(fetchReservationRequest);
                executeTask(AppConstants.TASK_CODES.GET_RESERVATION, httpParamObject);
            }
        }
    }


    private void getreservationItem() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getReservation(hsId);
        executeTask(AppConstants.TASK_CODES.GET_RESERVATION, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_RESERVATION:
                ReservationResponseApi responseApi = (ReservationResponseApi) response;
                if (responseApi != null && responseApi.getError() == false) {
                    ReservationDataResponse data = responseApi.getData();
                    if (data != null){
                    List<ReservationByTypeData> reservationData = data.getCategories();
//                    ReservationData reservationData = responseApi.getData();
//                    if (reservationData != null) {
//                        List<TypeInfoList> typeInfoList = reservationData.getTypeInfoList();
                        if (CollectionUtils.isNotEmpty(reservationData)){
                            reservationList.addAll(reservationData);
                            listAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                    tvEmptyView.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_maintenance;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final ReservationByTypeData reservationItem = reservationList.get(position);
        holder.tvName.setText(reservationItem.getName());
        String imageUrl = reservationItem.getImageUrl();
        if (!TextUtils.isEmpty(imageUrl)){
            Picasso.with(getActivity()).load(imageUrl).placeholder(R.drawable.progress_animation).into(holder.ivImageUrl);
        }
        if (!TextUtils.isEmpty(reservationItem.getAmenityType())) {
            holder.tvType.setText(reservationItem.getAmenityType());
        } else {
            holder.tvType.setText("");
        }
        String shortDescription = reservationItem.getPrimaryCuisine();
        if (!TextUtils.isEmpty(shortDescription)){
            holder.tvCusionType.setVisibility(View.VISIBLE);
            holder.tvCusionType.setText(shortDescription);
            holder.tvCusionType.setTextSize(14);
        } else {
            holder.tvCusionType.setVisibility(View.GONE);
        }
        holder.tvTiming.setVisibility(View.VISIBLE);
        String checkInTime = Util.convertDateFormat(reservationItem.getStartTime(), Util.CHECK_IN_TIME_UI_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
        String checkoutTime = Util.convertDateFormat(reservationItem.getEndTime(), Util.CHECK_IN_TIME_UI_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
        if (!TextUtils.isEmpty(checkInTime) && !TextUtils.isEmpty(checkoutTime)) {
            holder.tvTiming.setText(checkInTime + " - " + checkoutTime);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SereneSpaActivity.class);
                Bundle bundle = new Bundle();
                if (fromHotel){
                    bundle.putBoolean(AppConstants.BUNDLE_KEYS.FROM_HOTEL, true);
                }
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.RESERVATION_SUB_TYPES, reservationItem);
                intent.putExtras(bundle);
                startActivityForResult(intent, AppConstants.REQUEST_CODE.ORDER_PLACED);
            }
        });
        return convertView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppConstants.RESULT_CODE.RESULT_DONE){
            getActivity().setResult(AppConstants.RESULT_CODE.RESULT_DONE);
            getActivity().finish();
        }
    }

    /*@Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ReservationByTypeData reservationItem = reservationList.get(position);
        Bundle bundle = new Bundle();
        if (fromHotel){
            bundle.putBoolean(AppConstants.BUNDLE_KEYS.FROM_HOTEL, true);
        }
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.RESERVATION_SUB_TYPES, reservationItem);
        startNextActivity(bundle, SereneSpaActivity.class);
//        FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.RESERVATION_SUBITEM_FRAGMENT, bundle);
    }*/


    class Holder {
        ImageView ivImageUrl, ivLike, ivStar;
        TextView tvName, tvCusionType, tvTiming, tvType;

        public Holder(View view) {
            ivImageUrl = (ImageView) view.findViewById(R.id.iv_hotel_room);
            ivLike = (ImageView) view.findViewById(R.id.iv_image_option1);
            ivStar = (ImageView) view.findViewById(R.id.iv_parttnership);
            tvName = (TextView) view.findViewById(R.id.tv_hotel_name);
            tvCusionType = (TextView) view.findViewById(R.id.tv_price);
            tvType = (TextView) view.findViewById(R.id.tv_hotel_address);
            tvTiming = (TextView) view.findViewById(R.id.tv_person);
        }
    }
}
