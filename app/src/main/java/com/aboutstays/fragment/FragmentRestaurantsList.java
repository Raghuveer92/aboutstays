package com.aboutstays.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.SereneSpaActivity;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.reservation.FetchReservationByTypeResponse;
import com.aboutstays.model.reservation.ReservationByTypeData;
import com.aboutstays.model.reservation.TypeInfoList;
import com.aboutstays.rest.request.FetchReservationRequest;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by neeraj on 4/4/17.
 */

public class FragmentRestaurantsList extends BaseFragment implements CustomListAdapterInterface {
    private ListView lvCityGuideItem;
    private CustomListAdapter listAdapter;
    private TextView tvEmptyView;
    private List<ReservationByTypeData> reservationByTypeDataList = new ArrayList<>();
    private int cgType;
    private String cgId;
    private String titleName;
    private String title;
    private Integer type;
    private String hsId;
    private GeneralServiceModel generalServiceModel;
    private String typeName;
    private String name;

    @Override
    public void initViews() {

        Bundle bundle = getArguments();
        if (bundle != null) {
            title = bundle.getString(AppConstants.BUNDLE_KEYS.TITLE_NAME);
            TypeInfoList typeInfoList = (TypeInfoList) bundle.getSerializable(AppConstants.BUNDLE_KEYS.RESERVATION_SUB_TYPES);
            if (typeInfoList != null) {
                type = typeInfoList.getType();
            }
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null) {
                hsId = generalServiceModel.getGeneralServiceData().getHsId();
            }
        }

        if (!TextUtils.isEmpty(title)){
            initToolBar(title);
        } else {
            initToolBar("");
        }

        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        lvCityGuideItem = (ListView) findView(R.id.lv_city_guide_item);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_3_star, reservationByTypeDataList, this);
        lvCityGuideItem.setAdapter(listAdapter);
        lvCityGuideItem.setEmptyView(tvEmptyView);

        getDataFromServer();
    }

    private void getDataFromServer() {
        FetchReservationRequest fetchReservationRequest = new FetchReservationRequest();
        fetchReservationRequest.setCategory(title);
        fetchReservationRequest.setType(type);
        fetchReservationRequest.setRsId(hsId);
        HttpParamObject httpParamObject = ApiRequestGenerator.getReservationItemListByType(fetchReservationRequest);
        executeTask(AppConstants.TASK_CODES.GET_RESERVATION_BY_TYPE, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_RESERVATION_BY_TYPE:
                FetchReservationByTypeResponse byTypeResponse = (FetchReservationByTypeResponse) response;
                if (byTypeResponse != null && byTypeResponse.getError() == false) {
                    List<ReservationByTypeData> typeData = byTypeResponse.getData();
                    if (CollectionUtils.isNotEmpty(typeData)){
                        reservationByTypeDataList.addAll(typeData);
                        listAdapter.notifyDataSetChanged();
                    }
                } else {
                    showToast(byTypeResponse.getMessage());
                }
                break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_city_guide_item;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final ReservationByTypeData byTypeData = reservationByTypeDataList.get(position);
        name = byTypeData.getName();
        holder.tvName.setText(byTypeData.getName());
        holder.tvshortDesc.setText(byTypeData.getDescription());
//        holder.tvType.setText(byTypeData.getType());
        holder.tvType.setVisibility(View.VISIBLE);

        String imageUrl = byTypeData.getImageUrl();
        if (!TextUtils.isEmpty(imageUrl)){
            Picasso.with(getActivity()).load(imageUrl).placeholder(R.drawable.progress_animation).into(holder.ivImageUrl);
        } else {

        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.RESERVATION_TYPES, byTypeData);
                bundle.putString(AppConstants.BUNDLE_KEYS.TITLE_NAME, name);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                startNextActivity(bundle, SereneSpaActivity.class);
            }
        });

        return convertView;
    }

    class Holder {
        ImageView ivImageUrl, ivLike, ivStar;
        TextView tvName, tvshortDesc, tvDistance, tvType;

        public Holder(View view) {
            ivImageUrl = (ImageView) view.findViewById(R.id.iv_hotel_room);
            ivLike = (ImageView) view.findViewById(R.id.iv_image_option1);
            ivStar = (ImageView) view.findViewById(R.id.iv_parttnership);
            tvName = (TextView) view.findViewById(R.id.tv_hotel_name);
            tvDistance = (TextView) view.findViewById(R.id.tv_price);
            tvType = (TextView) view.findViewById(R.id.tv_person);
            tvshortDesc = (TextView) view.findViewById(R.id.tv_hotel_address);
        }
    }
}
