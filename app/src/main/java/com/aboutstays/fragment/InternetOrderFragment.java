package com.aboutstays.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.dialog.ReservationDialogFragment;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.internet.InternetPackList;
import com.aboutstays.rest.request.InternetOrderRequest;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import simplifii.framework.ListAdapters.CustomExpandableListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.receivers.CartUpdateReceiver;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;

/**
 * Created by aman on 24/04/17.
 */

public class InternetOrderFragment extends BaseFragment implements CustomExpandableListAdapter.ExpandableListener{

    private ExpandableListView expandableListView;
    private String gsId, hotelId, staysId, userId;
    private InternetPackList internetPackList;
    private CustomExpandableListAdapter customExpandableListAdapter;
    private List<InternetPackList> packList = new ArrayList<>();

    @Override
    public void initViews() {
        showVisibility(R.id.btn_place_order);
        initToolBar("Order Details");

        Bundle bundle = getArguments();
        if (bundle != null) {
            GeneralServiceModel generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null) {
                gsId = generalServiceModel.getGeneralServiceData().getId();
                if (generalServiceModel.getHotelStaysList().getStaysPojo() != null) {
                    hotelId = generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId();
                    staysId = generalServiceModel.getHotelStaysList().getStaysPojo().getStaysId();
                    userId = generalServiceModel.getHotelStaysList().getStaysPojo().getUserId();
                }
            }
            internetPackList = (InternetPackList) bundle.getSerializable(AppConstants.BUNDLE_KEYS.INTERNET_PACK_LIST);
            packList.add(internetPackList);
        }

        setOnClickListener(R.id.btn_place_order);

        expandableListView = (ExpandableListView) findView(R.id.lv_internet_reservation);

        expandableListView.setEmptyView(findView(R.id.tv_empty_view));
        customExpandableListAdapter = new CustomExpandableListAdapter(getActivity(), R.layout.row_header_expandable, R.layout.row_order_detail, packList, this);
        setHeader();
        expandableListView.setAdapter(customExpandableListAdapter);
        expandableListView.expandGroup(0);
    }

    private void setHeader() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View inflate = inflater.inflate(R.layout.view_order_details_header, expandableListView, false);
        TextView tvQuantity = (TextView) inflate.findViewById(R.id.tv_quantity);
        TextView tvRate = (TextView) inflate.findViewById(R.id.tv_rate);

        tvQuantity.setVisibility(View.INVISIBLE);
        expandableListView.addHeaderView(inflate);
    }



    @Override
    public int getViewID() {
        return R.layout.fragment_internet_reservation_order;
    }

    @Override
    public int getChildSize(int parentPosition) {
        return packList.size();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        convertView = inflater.inflate(resourceId, parent, false);
        setText(R.id.tv_men_women, packList.get(groupPosition).getTitle(),  convertView);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.iv_indicator);
        if (isExpanded) {
            imageView.setImageResource(R.mipmap.up_arrow_white);
        } else {
            imageView.setImageResource(R.mipmap.arrow_down_white);
        }
        imageView.setColorFilter(Color.WHITE);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        convertView = inflater.inflate(resourceId, parent, false);

        TextView tvItemCount = (TextView) convertView.findViewById(R.id.tv_item_count);
        TextView tvItemPrice = (TextView) convertView.findViewById(R.id.tv_item_price);

        InternetPackList internetPackList = packList.get(groupPosition);
        if(internetPackList != null) {
            setText(R.id.tv_item, internetPackList.getDescription(), convertView);

            if(!internetPackList.getComplementry())
                tvItemPrice.setText("\u20B9 " + internetPackList.getPrice() + "");
            else
                tvItemPrice.setText("Free");
        }
        tvItemCount.setVisibility(View.INVISIBLE);
        return convertView;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch(v.getId()) {
            case R.id.btn_place_order :
                InternetOrderValidateAndSubmit();
                break;
        }
    }

    private void InternetOrderValidateAndSubmit() {
        if(internetPackList == null) {
            showToast("Please select an internet pack.");
            return;
        }

        InternetOrderRequest request = new InternetOrderRequest();
        request.setStaysId(staysId);
        request.setUserId(userId);
        request.setHotelId(hotelId);
        request.setGsId(gsId);
        request.setRequestedNow(true);
        request.setInternetPack(internetPackList);

        HttpParamObject httpParamObject = ApiRequestGenerator.requestInternetOrder(request);
        executeTask(AppConstants.TASK_CODES.INTERNET_ORDER, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.INTERNET_ORDER:
                BaseApiResponse baseResponse = (BaseApiResponse) response;
                if(baseResponse != null && !baseResponse.getError()) {
                    showSuccessDialog();
                    CartUpdateReceiver.sendBroadcast(getActivity(),CartUpdateReceiver.ACTION_UPDATE_REQUEST);
                }
                break;
        }
    }

    private void showSuccessDialog() {
        SuccessDialogFragment.show(getChildFragmentManager(), "INTERNET","ORDER PLACED", getString(R.string.internet_msg), "OK", new SuccessDialogFragment.OnSuccessListener() {
            @Override
            public void done() {
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        });
    }
}