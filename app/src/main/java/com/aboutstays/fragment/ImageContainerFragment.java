package com.aboutstays.fragment;

import android.text.TextUtils;
import android.widget.ImageView;

import com.aboutstays.R;
import com.squareup.picasso.Picasso;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by aman on 26/04/17.
 */

public class ImageContainerFragment extends BaseFragment {

    private String imageUrl;
    private ImageView ivAmenityImage;

    public static ImageContainerFragment getInstance(String imageUrl) {
        ImageContainerFragment fragment = new ImageContainerFragment();
        fragment.imageUrl = imageUrl;
        return fragment;
    }

    @Override
    public void initViews() {

        ivAmenityImage = (ImageView) findView(R.id.iv_hotel_room);

        if(!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(getActivity())
                    .load(imageUrl)
                    .error(R.mipmap.cambriasuites)
                    .into(ivAmenityImage);
        }

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_reservation_image_swipe;
    }
}
