package com.aboutstays.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.rest.request.EditOrderRequest;
import com.aboutstays.rest.response.DashBoardDetailData;
import com.aboutstays.rest.response.ListGeneralItemDetail;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.receivers.CartUpdateReceiver;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by neeraj on 30/3/17.
 */

public class FragmentEditOrder extends BaseFragment implements CustomListAdapterInterface {

    private CustomListAdapter listAdapter;
    private TextView emptyTextView;
    private Button btnUpdateOrder;
    private DashBoardDetailData orderData = new DashBoardDetailData();
    private List<ListGeneralItemDetail> generalItemDetailsList = new ArrayList<>();
    private String uosId;
    EditOrderRequest editOrderRequest = new EditOrderRequest();
    Map<String, Integer> mapItemIdVSQuantity = new HashMap<>();
    private List<ListGeneralItemDetail> updatedList = new ArrayList<>();

    @Override
    public void initViews() {
        initToolBar("Edit Order");

        btnUpdateOrder = (Button) findView(R.id.btn_update);
        emptyTextView = (TextView) findView(R.id.tv_empty_view);
        ListView editOrderListView = (ListView) findView(R.id.lv_edit_order);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_laundry_item, generalItemDetailsList , this);
        editOrderListView.setAdapter(listAdapter);
        editOrderListView.setEmptyView(emptyTextView);

        Bundle bundle = getArguments();
        if (bundle != null){
            uosId = bundle.getString(AppConstants.BUNDLE_KEYS.UOSID);
            DashBoardDetailData orderData = (DashBoardDetailData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (orderData != null){
                List<ListGeneralItemDetail> listGeneralItemDetails = orderData.getListGeneralItemDetails();
                generalItemDetailsList.clear();
                if (!CollectionUtils.isEmpty(listGeneralItemDetails)){
                    generalItemDetailsList.addAll(listGeneralItemDetails);
                }
                listAdapter.notifyDataSetChanged();
            }
        }
        btnUpdateOrder.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_update:
                HttpParamObject httpParamObject = ApiRequestGenerator.editOrderDetails(editOrderRequest);
                executeTask(AppConstants.TASK_CODES.EDIT_ORDER_DETAIL, httpParamObject);
                break;
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.EDIT_ORDER_DETAIL:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false){
                    Bundle bundle = new Bundle();
                    orderData.setListGeneralItemDetails(generalItemDetailsList);
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.EDIT_ORDER_ITEM, orderData);
                    Intent intent = new Intent();
                    intent.putExtras(bundle);
                    getActivity().setResult(Activity.RESULT_OK,intent);
                    getActivity().finish();
                }
                break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_edit_order;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        editOrderRequest.setMapItemIdVSQuantity(mapItemIdVSQuantity);

        final ListGeneralItemDetail itemDetail = generalItemDetailsList.get(position);
        holder.rl_title.setVisibility(View.GONE);
        holder.tvAdd.setVisibility(View.GONE);
        holder.rl_add_count.setVisibility(View.VISIBLE);
        holder.title.setText(itemDetail.getName());
        if (itemDetail.getPrice()>0) {
            holder.tvPrice.setVisibility(View.VISIBLE);
            holder.tvPrice.setText("\u20B9 " + itemDetail.getPrice() + "");
        } else {
            holder.tvPrice.setVisibility(View.GONE);
        }
        holder.tvCount.setText(itemDetail.getQuantity()+"");

        holder.tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemDetail.setQuantity(itemDetail.getQuantity()+1);
                listAdapter.notifyDataSetChanged();
            }
        });

        holder.tvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemDetail.getQuantity()>0) {
                    itemDetail.setQuantity(itemDetail.getQuantity() - 1);
                    listAdapter.notifyDataSetChanged();
                }
            }
        });

        editOrderRequest.setUosId(uosId);
        mapItemIdVSQuantity.put(itemDetail.getId(), itemDetail.getQuantity());
        return convertView;
    }

    class Holder {
        TextView title, tvPrice, tvLaundryFor, tvAdd, tvPlus, tvMinus, tvCount;
        private RelativeLayout rl_title, rl_add_count;

        public Holder(View view) {
            title = (TextView) view.findViewById(R.id.tv_title);
            tvPrice = (TextView) view.findViewById(R.id.tv_price);
            tvLaundryFor = (TextView) view.findViewById(R.id.tv_men_women);
            tvAdd = (TextView) view.findViewById(R.id.tv_add);
            tvPlus = (TextView) view.findViewById(R.id.tv_plus);
            tvMinus = (TextView) view.findViewById(R.id.tv_minus);
            tvCount = (TextView) view.findViewById(R.id.tv_count);
            rl_title = (RelativeLayout) view.findViewById(R.id.rl_title);
            rl_add_count = (RelativeLayout) view.findViewById(R.id.rl_add_count);
        }
    }

}
