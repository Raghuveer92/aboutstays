package com.aboutstays.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.complementaryServices.ComplementaryServiceData;
import com.aboutstays.model.complementaryServices.Service;
import com.aboutstays.model.complementaryServices.TabWiseComplementaryService;
import com.aboutstays.rest.response.GetComplementaryServiceResponse;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

/**
 * Created by aman on 22/04/17.
 */

public class ComplementaryServicesFragment extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface{

    private String hsId;
    private TextView tvEmptyView;
    private ViewPager viewPagerCompService;
    private TabLayout tabCompService;
    private CustomPagerAdapter pagerAdapter;
    private List<String> tabs = new ArrayList<>();
    private List<ComplementaryServiceItemFragment> compServiceItemListFragment = new ArrayList<>();

    @Override
    public void initViews() {
        initToolBar(getString(R.string.comp_service));

        Bundle bundle = getArguments();
        if (bundle != null) {
            GeneralServiceModel generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null) {
                if (generalServiceModel.getGeneralServiceData() != null) {
                    hsId = generalServiceModel.getGeneralServiceData().getHsId();
                }
            }
        }

        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        tvEmptyView.setVisibility(View.GONE);
        viewPagerCompService = (ViewPager) findView(R.id.viewPager_comp_service);
        tabCompService = (TabLayout) findView(R.id.tablayout_comp_service);
        setAdapter();
        pagerAdapter.notifyDataSetChanged();
        Util.changeTabsFont(tabCompService);
    }

    private void setAdapter() {
        pagerAdapter = new CustomPagerAdapter(getChildFragmentManager(), tabs, this);
        viewPagerCompService.setAdapter(pagerAdapter);
        tabCompService.setupWithViewPager(viewPagerCompService);
        getComplementaryService();
    }

    private void getComplementaryService() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getComplementaryService(hsId);
        executeTask(AppConstants.TASK_CODES.GET_COMPLEMENTARY_SERVICE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_COMPLEMENTARY_SERVICE :
                GetComplementaryServiceResponse compResponse = (GetComplementaryServiceResponse) response;
                if(response != null && !compResponse.getError()) {
                    ComplementaryServiceData compServiceData = compResponse.getData();
                    if(compServiceData != null) {
                        List<TabWiseComplementaryService> listTabWiseCompServices = compServiceData.getTabWiseComplementaryServices();
                        if(CollectionUtils.isNotEmpty(listTabWiseCompServices)) {
                            Map<String, List<Service>> mapServices = new HashMap<>();
                            for(TabWiseComplementaryService service : listTabWiseCompServices) {
                                if(service == null) {
                                    continue;
                                }
                                String tabName = service.getTabName();
                                List<Service> listServices = new ArrayList<>();
                                listServices.addAll(service.getServices());
                                mapServices.put(tabName, listServices);

                            }

                            tabs.clear();
                            compServiceItemListFragment.clear();
                            Set<String> setTabs = mapServices.keySet();
                            List<String> tabKeys = new ArrayList<>();
                            for(String key : setTabs) {
                                tabKeys.add(key);
                            }
                            Collections.sort(tabKeys);
                            for(String key : tabKeys) {
                                List<Service> subList = mapServices.get(key);
                                if(CollectionUtils.isNotEmpty(subList)) {
                                    tabs.add(key);
                                    compServiceItemListFragment.add(ComplementaryServiceItemFragment.getInstance(subList));
                                    pagerAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    } else {
                        tvEmptyView.setVisibility(View.VISIBLE);
                    }

                }
                break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.complementry_service_fragment;
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        return compServiceItemListFragment.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return tabs.get(position);
    }
}
