package com.aboutstays.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.maintenance.ListMaintenanceItemType;
import com.aboutstays.rest.request.MaintenanceRequest;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.aboutstays.utitlity.DatePickerUtil;
import com.aboutstays.utitlity.TimePickerUtil;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by neeraj on 1/4/17.
 */

public class MaintenanceDetailFragment extends BaseFragment {

    private Integer type;
    private String typeName;
    private String gsId;
    private RelativeLayout rlDateTime;
    private TextView tvReqNow, tvDateTime, tvDateTitle;
    private EditText etComment;
    private RadioGroup rgProblemType;
    private RadioButton rbNotWork, rbPartiallyWork, rbDamadge;
    private Button btnConfirm;
    private long pickupTime;
    private boolean reqNow;
    private String hotelId, staysId, userId;
    private int selectedValueId;
    private String problemType;
    public String datePickerDate;
    private Calendar calendarInstance;
    public static final String MAINTENANCE_DATE = "MMM dd";
    public static final String MAINTENANCE_DATE_TIME_FORMAT = "MMM dd, hh:mm a";
    public String maintenanceItemName;
    private String deliveryDate;
    private long checkOutDate;

    @Override
    public void initViews() {
        calendarInstance = Calendar.getInstance();
        findViews();

        tvDateTitle.setText("Preferred date & Time");

        Bundle bundle = getArguments();
        if (bundle != null){
            GeneralServiceModel generalServiceModel = (GeneralServiceModel)bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if(generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null){
                gsId = generalServiceModel.getGeneralServiceData().getId();
                if (generalServiceModel.getHotelStaysList().getStaysPojo() != null) {
                    hotelId = generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId();
                    staysId = generalServiceModel.getHotelStaysList().getStaysPojo().getStaysId();
                    userId = generalServiceModel.getHotelStaysList().getStaysPojo().getUserId();
                }
            }
            ListMaintenanceItemType maintenanceItemType = (ListMaintenanceItemType) bundle.getSerializable(AppConstants.BUNDLE_KEYS.MAINTENANCE_ITEM);
            if (maintenanceItemType != null){
                type = maintenanceItemType.getType();
                typeName = maintenanceItemType.getTypeName();
            }
            maintenanceItemName = bundle.getString(AppConstants.BUNDLE_KEYS.MAINTENANCE_ITEM_NAME);
        }

        checkOutDate = Preferences.getData(AppConstants.PREF_KEYS.CHECKOUT_DATE_TIME, 0l);

        if (!TextUtils.isEmpty(typeName)){
            initToolBar(typeName);
        } else {
            initToolBar("");
        }

        setOnClickListener(R.id.rl_datetime, R.id.tv_add, R.id.btn_confirm);

    }

    private void findViews() {
        tvDateTime = (TextView) findView(R.id.tv_date_time);
        tvDateTitle = (TextView) findView(R.id.tv_title);
        rlDateTime = (RelativeLayout) findView(R.id.rl_datetime);
        tvReqNow = (TextView) findView(R.id.tv_add);
        etComment = (EditText) findView(R.id.et_comment);
        rgProblemType = (RadioGroup) findView(R.id.rg_problemType);
        rbNotWork = (RadioButton) findView(R.id.rb_not_working);
        rbPartiallyWork = (RadioButton) findView(R.id.rb_partially_working);
        rbDamadge = (RadioButton) findView(R.id.rb_damaged);
        btnConfirm = (Button) findView(R.id.btn_confirm);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_datetime:
                setReqNowFalse();
                setDateandTime();
                break;
            case R.id.tv_add:
                if (reqNow == false) {
                    reqNow = true;
                    tvReqNow.setBackgroundResource(R.drawable.shape_button_selected);
                    tvReqNow.setTextColor(Color.WHITE);
                    tvDateTime.setText("");
//                    tvReqNow.setText(R.string.requested);
//                    selectCurrentDateandTime();
                } else {
                    setReqNowFalse();
                }
                break;
            case R.id.btn_confirm:
                boolean ifBeforeCheckout = Util.ifGreaterThnCheckOut(pickupTime, checkOutDate);
                if (ifBeforeCheckout) {
                    validateDateandTime();
                } else {
                    errorDialoge(getString(R.string.select_date_error));
                    return;
                }

                break;
        }
    }

    private void errorDialoge(String string) {
        SuccessDialogFragment.show(getChildFragmentManager(),
                "MAINTENANCE",
                "REQUEST FAILED",
                string,
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                    }
                }
        );
    }

    private void validateDateandTime() {

        selectedValueId = rgProblemType.getCheckedRadioButtonId();
        if(selectedValueId == rbNotWork.getId())
        {
          problemType = rbNotWork.getText().toString();
        }
        else if (selectedValueId == rbPartiallyWork.getId())
        {
            problemType = rbPartiallyWork.getText().toString();
        }
        else if (selectedValueId == rbDamadge.getId())
        {
            problemType = rbDamadge.getText().toString();
        }
        else {
            problemType = "";
        }

        MaintenanceRequest maintenanceReq = new MaintenanceRequest();
        maintenanceReq.setHotelId(hotelId);
        maintenanceReq.setGsId(gsId);
        maintenanceReq.setStaysId(staysId);
        maintenanceReq.setUserId(userId);
        maintenanceReq.setType(type);
        maintenanceReq.setItemName(maintenanceItemName);
        if (TextUtils.isEmpty(problemType)){
            showToast("Please select problem type first");
            return;
        } else {
            maintenanceReq.setProblemType(problemType);
        }
        String comment = etComment.getText().toString().trim();
        if (!TextUtils.isEmpty(comment)){
            maintenanceReq.setComment(comment);
        }

        if (reqNow == true) {
            maintenanceReq.setRequestedNow(true);
        } else if (pickupTime > 0){
            maintenanceReq.setRequestedNow(false);
            maintenanceReq.setDeliveryTime(pickupTime);
        } else {
            showToast(getString(R.string.please_select_time_first));
            return;
        }
        if (reqNow == false && TextUtils.isEmpty(deliveryDate)){
            showToast(getString(R.string.please_select_time_first));
            return;
        } else {
            maintenanceReq.setDeliverTimeString(deliveryDate);
        }

        maintenanceRequest(maintenanceReq);
    }

    private void maintenanceRequest(MaintenanceRequest maintenanceReq) {
        HttpParamObject httpParamObject = ApiRequestGenerator.requestMaintenance(maintenanceReq);
        executeTask(AppConstants.TASK_CODES.REQUEST_MAINTENANCE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.REQUEST_MAINTENANCE:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError()== false){
                    showSuccessDialog();
                }
                break;
        }
    }

    private void showSuccessDialog() {
        SuccessDialogFragment.show(getChildFragmentManager(),
                "MAINTENANCE",
                "REQUEST INITIATED",
                getString(R.string.request_initiated_successfully),
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                        getActivity().setResult(AppConstants.RESULT_CODE.RESULT_DONE);
                        getActivity().finish();
                    }
                }
        );

    }

    private void setReqNowFalse() {
        reqNow = false;
        tvReqNow.setBackgroundResource(R.drawable.shape_button_email);
        tvReqNow.setTextColor(getResourceColor(R.color.color_red_bg));
        tvReqNow.setText(R.string.request_now);
        tvDateTime.setText("");
        pickupTime = 0;
        deliveryDate = "";
    }

    private void selectCurrentDateandTime() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time =&gt; " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat(MAINTENANCE_DATE_TIME_FORMAT);
        String formattedDate = df.format(c.getTime());
        tvDateTime.setText(formattedDate);
        pickupTime = c.getTimeInMillis() - 1000;
    }

    private void setDateandTime() {
        final AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        DatePickerUtil datePickerUtil = new DatePickerUtil(MAINTENANCE_DATE, calendarInstance, appCompatActivity.getFragmentManager(), getString(R.string.select_date), true, false, new DatePickerUtil.OnDateSelected() {
            @Override
            public void onDateSelected(Calendar calendar, String date) {
                TimePickerUtil timePickerUtil = new TimePickerUtil(MAINTENANCE_DATE_TIME_FORMAT, calendar, appCompatActivity.getFragmentManager(), getString(R.string.select_time), new TimePickerUtil.OnTimeSelected() {
                    @Override
                    public void onTimeSelected(Calendar calendar, String date) {
                        calendarInstance = calendar;
                        pickupTime = calendar.getTimeInMillis() - 1000;
                        tvDateTime.setText(date);
                        Date time = calendar.getTime();
                        deliveryDate = Util.format(time, Util.DELIVERY_TIME_DATE_PATTERN);
                    }
                });
                timePickerUtil.show();
            }

            @Override
            public void onCanceled() {
//                tvDateTime.setText("");
            }
        });
        datePickerUtil.show();

/*        final Calendar c = Calendar.getInstance();
        long timeToShow = 0;
        if(!TextUtils.isEmpty(datePickerDate)) {
            timeToShow = Util.convertStringToLong(Util.API_DATE_FORMAT, datePickerDate);
        }
        c.setTimeInMillis(timeToShow);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    //  @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Util.API_DATE_FORMAT);
                        datePickerDate = simpleDateFormat.format(c.getTime());

                    }
                }, mYear, mMonth, mDay);
        openTimePicker(c);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();*/
    }

    /*private void openTimePicker(final Calendar c) {
        long timeToShow = 0;
        if(TextUtils.isEmpty(tvDateTime.getText().toString())){
            timeToShow = Calendar.getInstance().getTimeInMillis();
        } else {
            timeToShow = Util.convertStringToLong(MAINTENANCE_DATE_TIME_FORMAT, tvDateTime.getText().toString());
        }
        c.setTimeInMillis(timeToShow);
        final int hour = c.get(Calendar.HOUR_OF_DAY);
        final int min = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        c.set(Calendar.MINUTE, minute);
                        SimpleDateFormat format = new SimpleDateFormat(MAINTENANCE_DATE_TIME_FORMAT);
                        String time = format.format(c.getTime());
                        tvDateTime.setText(time);
                        pickupTime = c.getTimeInMillis() - 1000;
                    }
                }, hour, min, false);
        timePickerDialog.show();
    }*/

    @Override
    public int getViewID() {
        return R.layout.fragment_maintenance_details;
    }
}
