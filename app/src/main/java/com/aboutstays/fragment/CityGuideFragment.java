package com.aboutstays.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.rest.response.CityGuideData;
import com.aboutstays.rest.response.CityGuideGeneralInfo;
import com.aboutstays.rest.response.ExpenseItem;
import com.aboutstays.rest.response.GeneralItemType;
import com.aboutstays.rest.response.GetCityGuideResponse;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by Rahul Aarya on 29-12-2016.
 */

public class CityGuideFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    private CustomListAdapter listAdapter;
    private List<GeneralItemType> listCityGuideItems;
    private CityGuideData cityGuideData;
    private ListView cityGuideListView;
    private TextView tvEmptyView;
    private String cgId;

    @Override
    public void initViews() {
        initToolBar("City Guide");
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        cityGuideListView = (ListView) findView(R.id.lv_city_guide);
        listCityGuideItems = new ArrayList<>();
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_stay_preferences, listCityGuideItems, this);
        cityGuideListView.setAdapter(listAdapter);
        cityGuideListView.setEmptyView(tvEmptyView);

        Bundle bundle = getArguments();
        if(bundle != null){
            GeneralServiceModel generalServiceModel = (GeneralServiceModel)bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if(generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null){
                cgId = generalServiceModel.getGeneralServiceData().getHsId();
            }
        }
        getCityGuideData();

        cityGuideListView.setOnItemClickListener(this);
    }

    private void getCityGuideData() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getCityGuide(cgId);
        executeTask(AppConstants.TASK_CODES.GET_CITY_GUIDE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode){
            case AppConstants.TASK_CODES.GET_CITY_GUIDE:
            GetCityGuideResponse cityGuideResponse = (GetCityGuideResponse) response;
            if(cityGuideResponse != null && !cityGuideResponse.getError()){
                cityGuideData = cityGuideResponse.getData();
                if(cityGuideData != null){
                    CityGuideGeneralInfo generalInfo = cityGuideData.getGeneralInfo();
                    if(generalInfo != null){
                        setCityGuideListHeader(generalInfo);
                    }
                    if(cityGuideData.getCityGuideItemTypeList() != null){
                        listCityGuideItems.addAll(cityGuideData.getCityGuideItemTypeList());
                    }
                    listAdapter.notifyDataSetChanged();
                }
            }
            break;
        }
    }

    private void setCityGuideListHeader(CityGuideGeneralInfo generalInfo) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View inflate = inflater.inflate(R.layout.view_city_guide_header, cityGuideListView, false);

        ImageView ivCityImage = (ImageView) inflate.findViewById(R.id.iv_hotel_room);
        TextView tvCityName = (TextView) inflate.findViewById(R.id.tv_city_name);

        if(!TextUtils.isEmpty(generalInfo.getImageUrl())){
            Picasso.with(getActivity()).load(generalInfo.getImageUrl())
                    .placeholder(R.drawable.progress_animation)
//                    .error(R.mipmap.cambriasuites)
                        .into(ivCityImage);
            }
            if (cityGuideData.getGeneralInfo().getAddress() != null) {
                tvCityName.setText(cityGuideData.getGeneralInfo().getAddress().getCity());
            }
        cityGuideListView.addHeaderView(inflate);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_city_guide;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        CityGuideHolder cityGuideItemHolder = null;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.row_stay_preferences, parent,false);
            cityGuideItemHolder = new CityGuideHolder(convertView);
            convertView.setTag(cityGuideItemHolder);
        }else{
            cityGuideItemHolder = (CityGuideHolder) convertView.getTag();
        }

        final GeneralItemType cityGuideItem = listCityGuideItems.get(position);
        cityGuideItemHolder.onBindView(cityGuideItem);

/*        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.CITY_GUIDE_ITEM_TYPE, cityGuideItem);
                bundle.putString(AppConstants.BUNDLE_KEYS.CITY_GUIDE_ID, cgId);
                FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.CITY_GUIDE_ITEM_FRAGMENT, bundle);
            }
        });*/

        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0){

        } else {
            GeneralItemType cityGuideItem = listCityGuideItems.get(position - 1);
            Bundle bundle = new Bundle();
            bundle.putSerializable(AppConstants.BUNDLE_KEYS.CITY_GUIDE_ITEM_TYPE, cityGuideItem);
            bundle.putString(AppConstants.BUNDLE_KEYS.CITY_GUIDE_ID, cgId);
            FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.CITY_GUIDE_ITEM_FRAGMENT, bundle);
        }
    }

    private class CityGuideHolder{

        ImageView ivCGItemIcon, ivRightIcon;
        TextView tvName;

        public CityGuideHolder(View view){
            ivCGItemIcon = (ImageView) view.findViewById(R.id.iv_preferences);
            tvName = (TextView) view.findViewById(R.id.tv_preference);
            ivRightIcon = (ImageView) view.findViewById(R.id.iv_select);
        }

        void onBindView(GeneralItemType cityGuideItem){
            tvName.setText(cityGuideItem.getTypeName());
            if(!TextUtils.isEmpty(cityGuideItem.getImageUrl())){
                Picasso.with(getActivity()).load(cityGuideItem.getImageUrl())
                        .fit()
                        .placeholder(R.drawable.progress_animation)
//                        .error(R.mipmap.ic_general)
                        .into(ivCGItemIcon);
            }
            ivRightIcon.setImageResource(R.mipmap.rightangle);
        }
    }


}
