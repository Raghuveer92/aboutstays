package com.aboutstays.fragment;

import android.os.Bundle;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.rest.response.ChildCategoryItem;

import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by aman on 28/03/17.
 */

public class HouseRulesDescriptionFragment extends BaseFragment {

    private TextView tvDescription;
    private String toolBar = "";

    @Override
    public void initViews() {
        tvDescription = (TextView) findView(R.id.tv_description);

        Bundle bundle = getArguments();
        if(bundle != null){
            ChildCategoryItem childCategoryItem = (ChildCategoryItem) bundle.getSerializable(AppConstants.BUNDLE_KEYS.CHILD_CATEGORY_ITEM);
            if(childCategoryItem != null){
                tvDescription.setText(childCategoryItem.getDescription());
                toolBar = "House Rules - " + childCategoryItem.getTitle();
            }
        }

        initToolBar(toolBar);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_house_rules_descp;
    }
}
