package com.aboutstays.fragment;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.DrawerActivity;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.adapter.NonSwipeableViewPager;
import com.aboutstays.model.stayline.GetStayApi;
import com.aboutstays.model.stayline.GetStayResponse;
import com.aboutstays.model.stayline.HotelStaysList;
import com.aboutstays.model.stayline.StaysPojo;
import com.aboutstays.model.stayline.UserStayData;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

public class StayLineFragment extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface {

    List<String> tabs = new ArrayList<>();
    private List<UpcomingStaysFragment> stayListFragment =new ArrayList<>();

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private List<HotelStaysList> upcomingStayList = new ArrayList<>();
    private List<HotelStaysList> pastStayList = new ArrayList<>();
    private CustomPagerAdapter customPagerAdapter;
    private FloatingActionButton fab;
    private String userId;
    private TextView tv_titleName;
    private ImageView ivPropic;
    private TextView tvTotalNights;
    private TextView tvTotalHotels;
    private TextView tvEmptyView;
    private static final String BASE_DATE_FORMAT = "dd-MM-yyyy";

    public static StayLineFragment getInstance(){
        StayLineFragment stayLineFragment = new StayLineFragment();
        return stayLineFragment;
    }

    @Override
    public void initViews() {
//        fab = (FloatingActionButton) findView(R.id.action_fab);
        tabLayout = (TabLayout) findView(R.id.tablayout_stayline);
        viewPager = (ViewPager) findView(R.id.viewpager_stayline);
        tv_titleName = (TextView) findView(R.id.tv_title_name);
        ivPropic = (ImageView) findView(R.id.iv_stayline);
        tvTotalNights = (TextView) findView(R.id.tv_total_nights);
        tvTotalHotels = (TextView) findView(R.id.tv_total_hotels);
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        FloatingActionButton myFab = (FloatingActionButton) findView(R.id.fb_add);
        myFab.setBackgroundTintList(getResources().getColorStateList(R.color.button_login_color));
        myFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(AppConstants.BUNDLE_KEYS.ADD_STAY, true);
//                startNextActivity(bundle, DrawerActivity.class);
                FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.RETRIEVE_FRAGMENT, bundle);
            }
        });

        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null){
            userId = sessionInstance.getUserId();
            String imageUrl = sessionInstance.getImageUrl();
            String firstName = sessionInstance.getFirstName();
            String lastName = sessionInstance.getLastName();

            if (!TextUtils.isEmpty(imageUrl)) {
                Picasso.with(getActivity()).load(imageUrl).placeholder(R.drawable.progress_animation).into(ivPropic);
            }
            tv_titleName.setText(firstName + " " + lastName);
        }
        stayListFragment.add(UpcomingStaysFragment.getInstance(upcomingStayList));
        stayListFragment.add(UpcomingStaysFragment.getInstance(pastStayList));
        setAdapter();

        getStaylineData();
    }


    private void getStaylineData() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getUSerStayline(userId);
        executeTask(AppConstants.TASK_CODES.GET_STAYLINE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null){
            showToast(getString(R.string.serverr_error));
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.GET_STAYLINE:
                GetStayApi getStayApi = (GetStayApi) response;
                if (getStayApi != null && getStayApi.getError() == false){
//                    tvEmptyView.setVisibility(View.GONE);
                    upcomingStayList.clear();
                    pastStayList.clear();
                    upcomingStayList.addAll(getStayApi.getUpcomingStayList());
                    pastStayList.addAll(getStayApi.getPastStayList());

                    UserStayData data = getStayApi.getData();
                    if (data != null){
                        tvTotalHotels.setText(data.getHotels() + "");
                        tvTotalNights.setText(data.getNights() + "");
                    }
                } else {
                    tvEmptyView.setVisibility(View.VISIBLE);
                    showToast(getStayApi.getMessage());
                }
                refreshLists();
                break;
        }
    }

    private void refreshLists() {
        for(UpcomingStaysFragment upcomingStaysFragment:stayListFragment){
            upcomingStaysFragment.refreshList();
        }
    }

    private void setAdapter() {
        initTabs();
        customPagerAdapter = new CustomPagerAdapter(getChildFragmentManager(), tabs, this);
        viewPager.setAdapter(customPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_stayline;
    }

    private void initTabs() {
        tabs.add(getString(R.string.upcoming_stays));
        tabs.add(getString(R.string.past_stays));
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        return stayListFragment.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return tabs.get(position);
    }
}
