package com.aboutstays.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;

import com.aboutstays.R;
import com.squareup.picasso.Picasso;

import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

public class FragmentImageView extends BaseFragment {
    private ImageView iv;

    @Override
    public void initViews() {

        initToolBar("");

        iv = (ImageView) findView(R.id.iv);
        Bundle bundle = getArguments();
        if (bundle!= null){
            String imageUrl = bundle.getString(AppConstants.BUNDLE_KEYS.IMAGEURL);
            if (!TextUtils.isEmpty(imageUrl)){
                Picasso.with(getActivity()).load(imageUrl).placeholder(R.drawable.progress_animation).into(iv);
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_image_view;
    }
}
