package com.aboutstays.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.EditProfileActivity;
import com.squareup.picasso.Picasso;

import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends BaseFragment {
    private ImageView ivEdit, ivPropic, ivInfo;
    private TextView tvName;
    private FrameLayout frameEdit;
    private TextView tvMemberSince;
    public static final String MEMBER_SINCE_DATE_FORMAT = "dd MMM yyyy";


    public static ProfileFragment getInstance(){
        ProfileFragment myProfileActivity = new ProfileFragment();
        return myProfileActivity;
    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.fragment_my_profile);
//
//    }

    @Override
    public void initViews() {

        ivPropic = (ImageView) findView(R.id.iv_propic);
        ivInfo = (ImageView) findView(R.id.iv_info);
        ivEdit = (ImageView) findView(R.id.iv_edit_profile);
        tvName = (TextView) findView(R.id.tv_profile_title);
        tvMemberSince = (TextView) findView(R.id.tv_member_since);
        frameEdit = (FrameLayout) findView(R.id.frame_edit);
        RelativeLayout rlEdit = (RelativeLayout) findView(R.id.rl_edit);
        rlEdit.setVisibility(View.VISIBLE);
        setProfileProperties();
        setOnClickListener( R.id.iv_info, R.id.frame_edit);
    }

    private void setProfileProperties() {
        UserSession userSession = UserSession.getSessionInstance();
        if (userSession!=null){
            String userId = userSession.getUserId();
            String imageUrl = userSession.getImageUrl();
            String firstName = userSession.getFirstName();
            String lastName = userSession.getLastName();
            String signedUpTime = Util.getDate(userSession.getCreated(), MEMBER_SINCE_DATE_FORMAT);

            if (!TextUtils.isEmpty(imageUrl)) {
                Picasso.with(getActivity()).load(imageUrl).placeholder(R.drawable.progress_animation).into(ivPropic);
            }
            tvName.setText(firstName + " " + lastName);
            tvMemberSince.setText("Member Since " + signedUpTime);
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_my_profile;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.iv_edit_profile:
//                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
//                startActivityForResult(intent, AppConstants.REQUEST_CODE.PROFILE);
                break;
            case R.id.frame_edit:
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                startActivityForResult(intent, AppConstants.REQUEST_CODE.PROFILE);
                break;
/*            case R.id.iv_info:
                Intent int1 = new Intent(getActivity(), EditProfileActivity.class);
                Bundle bundle = new Bundle();
                bundle.putBoolean(AppConstants.BUNDLE_KEYS.FROM_PROFILE, true);
                int1.putExtras(bundle);
                startActivityForResult(int1, AppConstants.REQUEST_CODE.PROFILE);
                break;*/
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            setProfileProperties();
        }
    }
}
