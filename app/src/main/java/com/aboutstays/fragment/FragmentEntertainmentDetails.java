package com.aboutstays.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.SereneSpaActivity;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.entertainment.AllEntertainmentData;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by neeraj on 7/4/17.
 */

public class FragmentEntertainmentDetails extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface{

    private TextView tvTitle, tvopnHour, tvSubTitle, tvRestaurantType, tvDescription, tvEntryCriteriafix, tvEntryCriteria,
            tvTimeText, tvTime, tvOpeningTimeFix, tvMostPopular, tvAminityType, tvAddDirection;
    private View view1, view2;
    private String name;
    private RelativeLayout rlButton;
    private ViewPager viewPager;
    private CustomPagerAdapter pagerAdapter;
    private List<ImageContainerFragment> listImageContainerFragment = new ArrayList<>();

    @Override
    public void initViews() {

        findViews();
        rlButton.setVisibility(View.GONE);
        Bundle bundle = getArguments();
        if (bundle != null){
            AllEntertainmentData entertainmentData = (AllEntertainmentData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (entertainmentData != null){
                setData(entertainmentData);
                if (entertainmentData.getItemDetails() != null) {
                    name = entertainmentData.getItemDetails().getName();
                }
            }
        }

        if (!TextUtils.isEmpty(name)){
            initToolBar(name);
        } else {
            initToolBar("");
        }

        tvTimeText.setVisibility(View.VISIBLE);
        tvTime.setVisibility(View.VISIBLE);
        hideVisibility(R.id.tv_title, R.id.tv_opening_time, R.id.tv_opn_hour, R.id.tv_subTitle, R.id.tv_entry_criteria, R.id.tv_entry_criteria_fix,
                R.id.tv_most_popular, R.id.view1, R.id.view2,  R.id.btn_reserve);
    }

    private void setData(AllEntertainmentData entertainmentData) {
        if (entertainmentData.getItemDetails() != null) {
            List<String> headerImageList = new ArrayList<>();
            String imageUrl = entertainmentData.getItemDetails().getImageUrl();


            if (!TextUtils.isEmpty(imageUrl)){
                headerImageList.add(imageUrl);
                //Picasso.with(getActivity()).load(imageUrl).into(ivMainImage);
            }

            if(CollectionUtils.isNotEmpty(headerImageList)) {
                for(String amenityImage : headerImageList) {
                    ImageContainerFragment imageContainerFragment = ImageContainerFragment.getInstance(amenityImage);
                    listImageContainerFragment.add(imageContainerFragment);
                }
                pagerAdapter = new CustomPagerAdapter(getActivity().getSupportFragmentManager(), listImageContainerFragment, this);
                viewPager.setAdapter(pagerAdapter);
            }

            tvDescription.setText(entertainmentData.getItemDetails().getDescription());
            tvTimeText.setText("Channel No.");
            tvTime.setText("#"+entertainmentData.getNumber());
            tvAminityType.setText("Genere");
            if (entertainmentData.getItemDetails().getGenereDetails() != null) {
                tvRestaurantType.setText(entertainmentData.getItemDetails().getGenereDetails().getName());
            }
        }
    }

    private void findViews() {
//        ivMainImage = (ImageView) findView(R.id.iv_hotel_room);
        rlButton = (RelativeLayout) findView(R.id.rl_button);
        tvTitle = (TextView) findView(R.id.tv_title);
        tvSubTitle = (TextView) findView(R.id.tv_subTitle);
        tvopnHour = (TextView) findView(R.id.tv_opn_hour);
        tvAminityType = (TextView) findView(R.id.tv_aminity_type);
        tvRestaurantType = (TextView) findView(R.id.tv_restaurant_type);
        tvDescription = (TextView) findView(R.id.tv_description);
        tvEntryCriteria = (TextView) findView(R.id.tv_entry_criteria);
        tvEntryCriteriafix = (TextView) findView(R.id.tv_entry_criteria_fix);
        tvAddDirection = (TextView) findView(R.id.tv_add_direction);
        tvTimeText = (TextView) findView(R.id.tv_time_text);
        tvTime = (TextView) findView(R.id.tv_time);
        //llSubImages = (LinearLayout) findView(R.id.ll_subImages);
        //llGetDirection = (LinearLayout) findView(R.id.ll_get_direction);
        tvOpeningTimeFix = (TextView) findView(R.id.tv_opening_time);
        tvMostPopular = (TextView) findView(R.id.tv_most_popular);
        view1 = findView(R.id.view1);
        view2 = findView(R.id.view2);
        viewPager = (ViewPager) findView(R.id.vp_image);
        //TextView tvGetDirection = (TextView) findView(R.id.tv_get_direction);
        //tvGetDirection.setVisibility(View.INVISIBLE);
        //llGetDirection.setVisibility(View.INVISIBLE);
        tvAddDirection.setVisibility(View.GONE);
    }

    @Override
    public int getViewID() {
        return R.layout.activity_restaurant_name;
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        return listImageContainerFragment.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return null;
    }
}
