package com.aboutstays.fragment;

import android.support.v4.app.Fragment;

public interface NextFragmentListener {
    void startNext(Fragment fragment);
}