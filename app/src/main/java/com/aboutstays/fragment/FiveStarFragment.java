package com.aboutstays.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.HotelNameActivity;
import com.aboutstays.model.hotels.GeneralInformation;
import com.aboutstays.model.hotels.Hotel;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by ajay on 8/12/16.
 */

public class FiveStarFragment extends BaseFragment implements CustomListAdapterInterface {


    private CustomListAdapter listAdapter;
    private List<Hotel> fiveStarList = new ArrayList<>();
    private ListView fiveStarListView;
    private TextView tvError;


    public static FiveStarFragment getInstance() {
        FiveStarFragment fiveStarFragment = new FiveStarFragment();
        return fiveStarFragment;
    }

    @Override
    public void initViews() {
        if (fiveStarList != null) {
            setDatatoView(fiveStarList);
        }
    }

    private void putDataIntoList() {
//        for(int i=0; i < 5; i++){
//            HotelModel ob = new HotelModel();
//
//            ob.setHotelName("The Taj");
//            ob.setHotelAddrass("Banglore, Country");
//            ob.setImgUrl(R.mipmap.cambriasuites);
//            ob.setRightImgUrl(R.mipmap.google_plus);
//
//            fiveStarList.add(ob);
//        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_five_star;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {

        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_3_star, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }


        Hotel ob = fiveStarList.get(position);
        if (ob != null) {
            GeneralInformation generalInformation = ob.getGeneralInformation();
            holder.tvHotelName.setText(generalInformation.getName());
            holder.tvHotelAddress.setText(generalInformation.getAddress().getAddressLine1());
//            holder.ivHotelRoom.setImageResource(generalInformation.);
//            holder.ivRightSide.setImageResource(ob.getRightImgUrl());
        }

        return convertView;
    }

    public void setData(List<Hotel> fiveStarList) {
        this.fiveStarList = fiveStarList;
        if (isVisible()) {
            setDatatoView(fiveStarList);
        }
    }

    private void setDatatoView(List<Hotel> fiveStarList) {
        tvError = (TextView) findView(R.id.tv_error);
        fiveStarListView = (ListView) findView(R.id.listview_5_star);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_3_star, fiveStarList, this);
        fiveStarListView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();

        fiveStarListView.setEmptyView(tvError);

        fiveStarListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startNextActivity(HotelNameActivity.class);
            }
        });

    }

    class Holder {
        RatingBar ratingbar;
        ImageView ivHotelRoom, ivRightSide;
        TextView tvHotelName, tvHotelAddress;

        public Holder(View view) {
            ivHotelRoom = (ImageView) view.findViewById(R.id.iv_hotel_room);
            ivRightSide = (ImageView) view.findViewById(R.id.iv_image_option1);

            tvHotelName = (TextView) view.findViewById(R.id.tv_hotel_name);
            tvHotelAddress = (TextView) view.findViewById(R.id.tv_hotel_address);
        }
    }
}
