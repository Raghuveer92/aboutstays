package com.aboutstays.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FoodItemActivity;
import com.aboutstays.activity.OrderDetailActivity;
import com.aboutstays.activity.RoomServiceFoodItemActivity;
import com.aboutstays.enums.FoodLabels;
import com.aboutstays.enums.SpicyType;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.cart.RoomServiceCartData;
import com.aboutstays.model.cart.FoodItemOrderDetailList;
import com.aboutstays.model.cart.FoodItemOrderDetails;
import com.aboutstays.model.cart.GetCartData;
import com.aboutstays.model.cart.RoomServiceCart;
import com.aboutstays.model.cart.RoomServiceCartResponse;
import com.aboutstays.model.food.FoodDataByType;
import com.aboutstays.model.oder_detail.CartData;
import com.aboutstays.model.roomService.RoomServiceMenuItem;
import com.aboutstays.model.search.SearchResponseApi;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import simplifii.framework.ListAdapters.CustomExpandableListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

import static android.app.Activity.RESULT_OK;

/**
 * Created by neeraj on 8/3/17.
 */

public class SearchFoodItemsFragment extends BaseFragment implements CustomExpandableListAdapter.ExpandableListener {

//    private CustomListAdapter listAdapter;
    private List<FoodDataByType> searchServiceList = new ArrayList<>();
    private ExpandableListView searchServiceListView;
    private TextView tvEmptyView;
    private String roomServiceId, staysId;
    private RelativeLayout rlMain;
    private ProgressBar progressBar;
    private List<RoomServiceMenuItem> roomServiceList = new ArrayList<>();
    private String query;
    private String userId;
    private RoomServiceCart roomServiceCart;
    private AddToCartListener addToCartListener;
    private GeneralServiceModel generalServiceModel;
    private List<FoodItemOrderDetailList> dataList = new ArrayList<>();
    private CustomExpandableListAdapter customExpandableListAdapter;
    private LinkedHashMap<String, List<FoodDataByType>> mapExpandable = new LinkedHashMap<>();
    private LinkedHashMap<String, List<FoodDataByType>> mapExpandableAll = new LinkedHashMap<>();
    private List<String> listHeader = new ArrayList<>();
    private String gsId;

    public static SearchFoodItemsFragment getInstance(AddToCartListener addToCartListener, String roomServiceId, String gsId, GeneralServiceModel generalServiceModel) {
        SearchFoodItemsFragment searchFoodItemsFragment = new SearchFoodItemsFragment();
        searchFoodItemsFragment.addToCartListener = addToCartListener;
        searchFoodItemsFragment.roomServiceId = roomServiceId;
        searchFoodItemsFragment.gsId = gsId;
        searchFoodItemsFragment.generalServiceModel = generalServiceModel;
        return searchFoodItemsFragment;
    }

    @Override
    public void initViews() {

        Bundle b = getArguments();
        if (b != null) {
            roomServiceId = b.getString(AppConstants.BUNDLE_KEYS.ROOM_SERVICE_ID);
            gsId = b.getString(AppConstants.BUNDLE_KEYS.GS_ID);
        }

        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            userId = sessionInstance.getUserId();
        }

        searchServiceList.clear();
        query = "";

        progressBar = (ProgressBar) findView(R.id.progressbar);
        rlMain = (RelativeLayout) findView(R.id.rl_main);
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        searchServiceListView = (ExpandableListView) findView(R.id.lv_search_service);
        customExpandableListAdapter = new CustomExpandableListAdapter(getActivity(), R.layout.row_header_expandable, R.layout.row_search_services, mapExpandable,this);
        searchServiceListView.setAdapter(customExpandableListAdapter);
        searchServiceListView.setEmptyView(tvEmptyView);

        searchServiceListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                RoomServiceMenuItem menuItem = roomServiceList.get(childPosition);
                FoodDataByType foodDataByType = mapExpandable.get(listHeader.get(groupPosition)).get(childPosition);
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.FOOD_ITEM_DETAILS, foodDataByType);
                bundle.putString(AppConstants.BUNDLE_KEYS.STAYS_ID, staysId);
                bundle.putString(AppConstants.BUNDLE_KEYS.GS_ID, gsId);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.ROOM_SERVICE_TITLE, menuItem);
                Intent intent = new Intent(getActivity(), FoodItemActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, AppConstants.REQUEST_CODE.SHOW_FOOD_DETAIL);
                return true;
            }
        });

//        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_search_services, searchServiceList, this);
//        searchServiceListView.setAdapter(listAdapter);
//        searchServiceListView.setEmptyView(tvEmptyView);
        customExpandableListAdapter.notifyDataSetChanged();
    }


    private void getAllFoodItems() {
        if (!TextUtils.isEmpty(query) && !TextUtils.isEmpty(roomServiceId)) {
            HttpParamObject httpParamObject = ApiRequestGenerator.searchFoodItemmsbyRoomService(roomServiceId, query);
            executeTask(AppConstants.TASK_CODES.SEARCH_FOOD_ITEMS, httpParamObject);
            hideProgressBar();
        }
    }


    private void addItemToCart() {
        List<RoomServiceCartData> roomServiceCartDataList = new ArrayList<>();
        for (FoodDataByType foodDataByType : searchServiceList) {
            int itemCount = foodDataByType.getItemCount();

            roomServiceCart = new RoomServiceCart();
            roomServiceCart.setUserId(userId);
            roomServiceCart.setStaysId(staysId);
            roomServiceCart.setHotelId(foodDataByType.getHotelId());

            RoomServiceCartData roomServiceCartData = new RoomServiceCartData();
            roomServiceCartData.setCartType(1);
            roomServiceCartData.setCartOperation(1);

            FoodItemOrderDetails data = new FoodItemOrderDetails();
            data.setQuantity(itemCount);
            data.setType(foodDataByType.getType());
            data.setFoodItemId(foodDataByType.getFooditemId());
            data.setPrice(foodDataByType.getPrice());

            roomServiceCartData.setFoodItemOrderDetails(data);
            roomServiceCartDataList.add(roomServiceCartData);
            roomServiceCart.setData(roomServiceCartDataList);
        }

        if (roomServiceCart != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.addOrRemoveFoodItem(roomServiceCart);
            executeTask(AppConstants.TASK_CODES.ADD_REMOVE_FOOD_TO_CART, httpParamObject);
        }
    }

    @Override
    public void onPreExecute(int taskCode) {
        if (taskCode == AppConstants.TASK_CODES.ADD_REMOVE_FOOD_TO_CART) {
            return;
        }
        super.onPreExecute(taskCode);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_search_food_items;
    }

/*    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_search_services, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final FoodDataByType ob = searchServiceList.get(position);

        holder.tvCount.setText(ob.getItemCount() + "");
        holder.serviceTitle.setText(ob.getName());
        holder.serviceDescription.setText(ob.getDescription());
        Boolean recommended = ob.getRecommended();
        Double mrp = ob.getPrice();
        if (mrp != null) {
            holder.serviceCost.setText("\u20B9 " + mrp);
        } else {
            holder.serviceCost.setText("\u20B9 " + "");
        }

        holder.tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int itemCount = ob.getItemCount();
                ob.setItemCount(itemCount + 1);
                listAdapter.notifyDataSetChanged();
                addToCartListener.updateCartItem(ob);
                addItemToCart();
            }
        });

        holder.tvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int itemCount = ob.getItemCount();
                if (itemCount >= 1) {
                    ob.setItemCount(itemCount - 1);
                }
                listAdapter.notifyDataSetChanged();
                addToCartListener.updateCartItem(ob);
                addItemToCart();
            }
        });

        holder.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ob.setItemCount(1);
                listAdapter.notifyDataSetChanged();
                addToCartListener.updateCartItem(ob);
                addItemToCart();
            }
        });

        if (ob.getItemCount() >= 1) {
            holder.tvAdd.setVisibility(View.GONE);
            holder.rlCount.setVisibility(View.VISIBLE);
        } else {
            holder.tvAdd.setVisibility(View.VISIBLE);
            holder.rlCount.setVisibility(View.GONE);
        }

        Integer foodLabel = ob.getFoodLabel();
        if (foodLabel != 0) {
            FoodLabels statusByCode = FoodLabels.findStatusByCode(foodLabel);
            if (statusByCode != null) {
                int vegUrl = statusByCode.getImageUrl();
                holder.imgVegNonveg.setImageResource(vegUrl);
                holder.imgVegNonveg.setVisibility(View.VISIBLE);
            }
        } else {
            holder.imgVegNonveg.setVisibility(View.GONE);
        }

        Integer spicyType = ob.getSpicyType();
        if (spicyType != 0) {
            SpicyType statusByCode1 = SpicyType.findStatusByCode(spicyType);
            if (statusByCode1 != null) {
                int spicyUrl = statusByCode1.getImageUrl();
                holder.imgChilli.setImageResource(spicyUrl);
                holder.imgChilli.setVisibility(View.VISIBLE);
            }
        } else {
            holder.imgChilli.setVisibility(View.GONE);
        }

        if (recommended == true) {
            holder.imgLike.setVisibility(View.VISIBLE);
        } else {
            holder.imgLike.setVisibility(View.GONE);
        }


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.FOOD_ITEM_DETAILS, ob);
                bundle.putString(AppConstants.BUNDLE_KEYS.STAYS_ID, staysId);
                bundle.putString(AppConstants.BUNDLE_KEYS.GS_ID, gsId);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                Intent intent = new Intent(getActivity(), FoodItemActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, AppConstants.REQUEST_CODE.SHOW_FOOD_DETAIL);
            }
        });

        return convertView;
    }*/

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        hideProgressBar();
        progressBar.setVisibility(View.GONE);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.SEARCH_FOOD_ITEMS:
                SearchResponseApi searchResponseApi = (SearchResponseApi) response;
                if (searchResponseApi != null && searchResponseApi.getError() == false) {
                    searchServiceList.clear();
                    List<FoodDataByType> searchData = searchResponseApi.getData();
                    if (searchData != null && !searchData.isEmpty()) {
                        ckecIfAlreadyAddedInCart(searchData);
                        searchServiceList.clear();
                        for (FoodDataByType itemData : searchData){
                            if (itemData.getShowOnApp() == true){
                                searchServiceList.add(itemData);
                            }
                        }
//                        searchServiceList.addAll(searchData);
                    }
                    filterList();
                    customExpandableListAdapter.notifyDataSetChanged();
                }
                break;

            case AppConstants.TASK_CODES.ADD_REMOVE_FOOD_TO_CART:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false) {
//                    showToast(getString(R.string.cart_added));
//                    addToCartListener.searchCallback();
                }
                break;
        }
    }

    private void filterList() {
        listHeader.clear();
        mapExpandableAll.clear();
        mapExpandable.clear();

        for (FoodDataByType internetPackList : searchServiceList) {
            String category = internetPackList.getCategory();
            if (!TextUtils.isEmpty(category)) {
                if (mapExpandable.containsKey(category)) {
                    List<FoodDataByType> internetPackLists = mapExpandable.get(category);
                    internetPackLists.add(internetPackList);
                } else {
                    List<FoodDataByType> laundryItemDatas = new ArrayList<>();
                    laundryItemDatas.add(internetPackList);
                    mapExpandable.put(category, laundryItemDatas);
                }
            }
        }
        refreshHeader();
        mapExpandableAll.putAll(mapExpandable);
    }

    private void refreshHeader() {
        listHeader.clear();
        Iterator<String> iterator = mapExpandable.keySet().iterator();
        while (iterator.hasNext()) {
            listHeader.add(iterator.next());
        }
        customExpandableListAdapter = new CustomExpandableListAdapter(getActivity(), R.layout.row_header_expandable, R.layout.row_search_services, mapExpandable, this);
        searchServiceListView.setAdapter(customExpandableListAdapter);

        for (int x = 0; x < listHeader.size(); x++) {
            searchServiceListView.expandGroup(x);
        }
    }

    private void ckecIfAlreadyAddedInCart(List<FoodDataByType> searchData) {
        List<FoodItemOrderDetailList> foodItemOrderDetailList = RoomServiceCartResponse.getInstance().getFoodItemOrderDetailList();
        dataList.clear();
        dataList.addAll(foodItemOrderDetailList);
        if (!CollectionUtils.isEmpty(dataList)) {
            for (FoodDataByType dataByType : searchData) {
                for (FoodItemOrderDetailList detailList : dataList) {
                    if (dataByType.getFooditemId().equals(detailList.getFoodItemId())) {
                        dataByType.setItemCount(detailList.getQuantity());
                    }
                }
            }
        }
    }

    public void textUpdate(String newText, String serviceId, String staysId) {
        this.roomServiceId = serviceId;
        this.query = newText;
        this.staysId = staysId;
        getAllFoodItems();
        searchServiceList.clear();
        customExpandableListAdapter.notifyDataSetChanged();
        rlMain.setBackgroundColor(Color.WHITE);
        tvEmptyView.setTextColor(getResourceColor(R.color.gray_text));
    }

    public void callBack() {
        addToCartListener.searchCallback();
    }

    @Override
    public int getChildSize(int parentPosition) {
        return mapExpandable.get(listHeader.get(parentPosition)).size();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        HolderParent holderParent;
        if (convertView == null) {
            convertView = inflater.inflate(resourceId, null);
            holderParent = new HolderParent(convertView);
            convertView.setTag(holderParent);
        } else {
            holderParent = (HolderParent) convertView.getTag();
        }
        String title = listHeader.get(groupPosition);
        holderParent.bindData(title, isExpanded);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_search_services, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        View viewDescription = (View) convertView.findViewById(R.id.view_line_description);
        if(isLastChild) {
            viewDescription.setVisibility(View.INVISIBLE);
        } else {
            viewDescription.setVisibility(View.VISIBLE);
        }
        FoodDataByType foodDataByType = mapExpandable.get(listHeader.get(groupPosition)).get(childPosition);
        holder.onBindData(foodDataByType);
        return convertView;

    }

    public void sendRoomServiceMenu(List<RoomServiceMenuItem> roomServiceList) {
        this.roomServiceList = roomServiceList;
    }

    class HolderParent {
        TextView textView;
        ImageView ivIndicator;

        public HolderParent(View view) {
            textView = (TextView) view.findViewById(R.id.tv_men_women);
            ivIndicator = (ImageView) view.findViewById(R.id.iv_indicator);
        }

        public void bindData(String title, boolean isExpand) {
            textView.setText(title);
            if (isExpand) {
                ivIndicator.setImageResource(R.mipmap.up_arrow_white);
            } else {
                ivIndicator.setImageResource(R.mipmap.arrow_down_white);
            }
        }
    }

    class ViewHolder {

        TextView serviceTitle, serviceCost, serviceDescription, tvAdd;
        TextView tvMinus, tvCount, tvPlus;
        ImageView imgVegNonveg, imgChilli, imgLike;
        RelativeLayout rlCount;

        public ViewHolder(View view) {
            serviceTitle = (TextView) view.findViewById(R.id.tv_service_title);
            serviceCost = (TextView) view.findViewById(R.id.tv_service_cost);
            serviceDescription = (TextView) view.findViewById(R.id.tv_service_description);
            imgVegNonveg = (ImageView) view.findViewById(R.id.iv_veg_nonveg);
            imgChilli = (ImageView) view.findViewById(R.id.iv_chilli);
            imgLike = (ImageView) view.findViewById(R.id.iv_like);
            tvAdd = (TextView) view.findViewById(R.id.tv_add);
            tvMinus = (TextView) view.findViewById(R.id.tv_minus);
            tvCount = (TextView) view.findViewById(R.id.tv_count);
            tvPlus = (TextView) view.findViewById(R.id.tv_plus);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_add_count);
        }

        public void onBindData(final FoodDataByType foodDataByType) {
            Boolean recommended = foodDataByType.getRecommended();
            tvCount.setText(foodDataByType.getItemCount() + "");
            serviceTitle.setText(foodDataByType.getName());
            serviceDescription.setLineSpacing(16,1);
            serviceDescription.setText(foodDataByType.getDescription());
            Double price = foodDataByType.getPrice();
            if (price != null) {
                serviceCost.setText("\u20B9 " + price);
            }

            Integer foodLabel = foodDataByType.getFoodLabel();
            if (foodLabel != 0 && foodLabel < 9) {
                FoodLabels statusByCode = FoodLabels.findStatusByCode(foodLabel);
                if (statusByCode != null) {
                    int vegUrl = statusByCode.getImageUrl();
                    imgVegNonveg.setImageResource(vegUrl);
                    imgVegNonveg.setVisibility(View.VISIBLE);
                }
            } else {
                imgVegNonveg.setVisibility(View.GONE);
            }

            tvPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int itemCount = foodDataByType.getItemCount();
                    foodDataByType.setItemCount(itemCount + 1);
                    customExpandableListAdapter.notifyDataSetChanged();
                    addToCartListener.updateCartItem(foodDataByType);
                    addItemToCart();
                }
            });

            tvMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int itemCount = foodDataByType.getItemCount();
                    if (itemCount >= 1) {
                        foodDataByType.setItemCount(itemCount - 1);
                    }
                    customExpandableListAdapter.notifyDataSetChanged();
                    addToCartListener.updateCartItem(foodDataByType);
                    addItemToCart();
                }
            });

            Integer spicyType = foodDataByType.getSpicyType();
            if (spicyType != 0 && spicyType < 9) {
                SpicyType statusByCode1 = SpicyType.findStatusByCode(spicyType);
                if (statusByCode1 != null) {
                    int spicyUrl = statusByCode1.getImageUrl();
                    imgChilli.setImageResource(spicyUrl);
                    imgChilli.setVisibility(View.VISIBLE);
                }
            } else {
                imgChilli.setVisibility(View.GONE);
            }

            if (recommended == true) {
                imgLike.setVisibility(View.VISIBLE);
            } else {
                imgLike.setVisibility(View.GONE);
            }

            tvAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    foodDataByType.setItemCount(1);
                    customExpandableListAdapter.notifyDataSetChanged();
                    addToCartListener.updateCartItem(foodDataByType);
                    addItemToCart();
                }
            });

            if (foodDataByType.getItemCount() > 0) {
                tvAdd.setVisibility(View.GONE);
                rlCount.setVisibility(View.VISIBLE);
            } else {
                tvAdd.setVisibility(View.VISIBLE);
                rlCount.setVisibility(View.GONE);
            }
        }

    }

/*    class Holder {

        TextView serviceTitle, serviceCost, serviceDescription, tvAdd;
        TextView tvMinus, tvCount, tvPlus;
        ImageView imgVegNonveg, imgChilli, imgLike;
        RelativeLayout rlCount;

        public Holder(View view) {
            serviceTitle = (TextView) view.findViewById(R.id.tv_service_title);
            serviceCost = (TextView) view.findViewById(R.id.tv_service_cost);
            serviceDescription = (TextView) view.findViewById(R.id.tv_service_description);
            imgVegNonveg = (ImageView) view.findViewById(R.id.iv_veg_nonveg);
            imgChilli = (ImageView) view.findViewById(R.id.iv_chilli);
            imgLike = (ImageView) view.findViewById(R.id.iv_like);
            tvAdd = (TextView) view.findViewById(R.id.tv_add);
            tvMinus = (TextView) view.findViewById(R.id.tv_minus);
            tvCount = (TextView) view.findViewById(R.id.tv_count);
            tvPlus = (TextView) view.findViewById(R.id.tv_plus);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_add_count);
        }
    }*/

    public interface AddToCartListener {
        void searchCallback();

        void updateCartItem(FoodDataByType foodDataByType);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (data != null) {
                Bundle extras = data.getExtras();
                if (extras != null) {
                    FoodDataByType foodDataByType = (FoodDataByType) extras.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
                    for (FoodDataByType foodList : searchServiceList) {
                        if (foodList.getFooditemId().equalsIgnoreCase(foodDataByType.getFooditemId())) {
                            foodList.setItemCount(foodDataByType.getItemCount());
                            addToCartListener.updateCartItem(foodList);
                            break;
                        }
                    }
                    customExpandableListAdapter.notifyDataSetChanged();
                }
            }
        }
        if (resultCode == AppConstants.REQUEST_CODE.ORDER_PLACED) {
            addToCartListener.searchCallback();
            getActivity().onBackPressed();
        }
    }
}
