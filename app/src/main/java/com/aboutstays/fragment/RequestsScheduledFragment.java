package com.aboutstays.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.RequestDetailsActivity;
import com.aboutstays.rest.response.GeneralDashboardItem;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Rahul Aarya on 23-12-2016.
 */

public class RequestsScheduledFragment extends BaseFragment implements CustomListAdapterInterface {
    private List<GeneralDashboardItem> listScheduledItems = new ArrayList<>();
    private List<GeneralDashboardItem> listScheduledItemsAll = new ArrayList<>();
    private CustomListAdapter listAdapter;
    private ListView scheduleListView;
    private TextView tvEmptyView;

    public static RequestsScheduledFragment getInstance() {
        RequestsScheduledFragment requestsScheduledFragment = new RequestsScheduledFragment();
        return requestsScheduledFragment;
    }

    public void setListScheduledItems(List<GeneralDashboardItem> listScheduledItems) {
        List<Long> longList = new ArrayList<>();
        for (GeneralDashboardItem generalDashboardItem : listScheduledItems) {
            longList.add(generalDashboardItem.getOrderedTime());
        }
        if (longList.size() > 0) {
            setWeekHeader(longList);
        } else {
            hideVisibility(R.id.line);
        }
        this.listScheduledItems.clear();
        listScheduledItemsAll.clear();
        if (!CollectionUtils.isEmpty(listScheduledItems)) {
            listScheduledItemsAll.addAll(listScheduledItems);
            this.listScheduledItems.addAll(listScheduledItems);
            listAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void refreshData() {
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void initViews() {
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        scheduleListView = (ListView) findView(R.id.lv_schedule);
        scheduleListView.setEmptyView(findView(R.id.tv_empty_view));
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_request_open, listScheduledItems, this);
        scheduleListView.setAdapter(listAdapter);
        scheduleListView.setEmptyView(tvEmptyView);
    }

    private void setWeekHeader(List<Long> longList) {
        WeekViewFragment weekViewFragment = WeekViewFragment.getInstance(longList, new WeekViewFragment.OnDateSelectListener() {
            @Override
            public void onDaySelected(long selectedDate) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date date = new Date(selectedDate);
                listScheduledItems.clear();
                for (GeneralDashboardItem generalDashboardItem : listScheduledItemsAll) {
                    if (simpleDateFormat.format(date).equals(simpleDateFormat.format(new Date(generalDashboardItem.getOrderedTime())))) {
                        listScheduledItems.add(generalDashboardItem);
                    }
                }
                listAdapter.notifyDataSetChanged();
            }
        });
        getChildFragmentManager().beginTransaction().add(R.id.lay_week_container, weekViewFragment).commitAllowingStateLoss();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_scheduled_week;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ScheduleRequestHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_request_open, parent, false);
            holder = new ScheduleRequestHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ScheduleRequestHolder) convertView.getTag();
        }

        final GeneralDashboardItem dashboardItem = listScheduledItems.get(position);

        if (dashboardItem != null) {
            holder.textTitle.setText(dashboardItem.getTitle());
            holder.textItems.setText(dashboardItem.getDescription());
            holder.textStatus.setText(dashboardItem.getStatus());
            if (!TextUtils.isEmpty(dashboardItem.getImageUrl())) {
                Picasso.with(getActivity()).load(dashboardItem.getImageUrl())
                        .placeholder(R.drawable.progress_animation)
                        .error(R.mipmap.place_holder_circular)
                        .into(holder.imageTitle);
            } else {
                holder.imageTitle.setImageResource(R.drawable.image_placeholder);
            }
            if (!TextUtils.isEmpty(dashboardItem.getStatusImageUrl())) {
                Picasso.with(getActivity()).load(dashboardItem.getStatusImageUrl())
                        .placeholder(R.drawable.progress_animation)
                        .error(R.mipmap.processing_icon)
                        .into(holder.imageStatus);
            } else {
                holder.imageStatus.setImageResource(R.mipmap.processing_icon);
            }
            if (dashboardItem.getOrderedTime() != 0) {
                String deliveryTime = Util.format(new Date(dashboardItem.getOrderedTime()), Util.REQUEST_DASHBOARD_DELIVERY_TIME_UI_FORMAT);
                holder.textDateTime.setText(deliveryTime);
            }
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(dashboardItem.getUosId())) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.GENERAL_DASHBOARD_ITEM, dashboardItem);
                    bundle.putInt(AppConstants.BUNDLE_KEYS.REQUEST_FROM, 2);
                    Intent intent = new Intent(getActivity(), RequestDetailsActivity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, AppConstants.REQUEST_CODE.ORDER_UPDATE);
                }

//                startNextActivity(bundle, RequestDetailsActivity.class);
            }
        });

        return convertView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppConstants.REQUEST_CODE.ORDER_UPDATE:
                    refreshData();
                    break;
            }
        }
    }

    class ScheduleRequestHolder {

        ImageView imageTitle, imageStatus;
        TextView textTitle, textDateTime, textStatus, textItems;

        public ScheduleRequestHolder(View view) {

            imageTitle = (ImageView) view.findViewById(R.id.iv_row_requests_icon);
            imageStatus = (ImageView) view.findViewById(R.id.iv_status);
            textTitle = (TextView) view.findViewById(R.id.tv_row_title);
            textItems = (TextView) view.findViewById(R.id.tv_items_list);
            textDateTime = (TextView) view.findViewById(R.id.tv_date_time);
            textStatus = (TextView) view.findViewById(R.id.tv_status);
        }
    }
}
