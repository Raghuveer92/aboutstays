package com.aboutstays.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.aboutstays.R;
import com.aboutstays.activity.AirportPickupActivity;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.lateCheckout.DeviationAndPrice;
import com.aboutstays.model.lateCheckout.DeviationInformation;
import com.aboutstays.model.lateCheckout.LateCheckoutServiceData;
import com.aboutstays.model.lateCheckout.RetreiveLateCheckoutData;
import com.aboutstays.model.stayline.StaysPojo;
import com.aboutstays.rest.request.BaseServiceRequest;
import com.aboutstays.rest.request.LateCheckoutRequest;
import com.aboutstays.rest.response.GetLateCheckoutServiceResponse;
import com.aboutstays.rest.response.RetreiveLateCheckoutResponse;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.receivers.CartUpdateReceiver;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

/**
 * Created by aman on 06/04/17.
 */

public class LateCheckoutFragment extends BaseFragment {

    private String roomId;
    private StaysPojo staysPojo;
    private TextView tvStandardCheckoutTime;
    private TextView tvStandardCheckoutDate;
    private TextView tvSelectedCheckoutDate;
    private TextView tvSelectedCheckoutTime;
    private TextView tvApplicableCharges;
    private TextView tvBottomMsg;
    private Button btnLateCheckout;
    private LateCheckoutServiceData checkoutServiceData;
    private List<DeviationAndPrice> listDeviationAndPrice;
    public static final int MILISECONDS_IN_HOUR = 60 * 60 * 1000;
    public long pickupTime;
    public double requestPrice;
    public long requestCheckoutDate;
    public long requestCheckoutTime;
    private LateCheckoutRequest lateCheckoutRequest;
    private BaseServiceRequest baseServiceRequest;
    private String deliveryDate;


    @Override
    public void initViews() {
        initToolBar("Late Check-Out");
        tvStandardCheckoutDate = (TextView) findView(R.id.tv_std_checkout_value);
        tvStandardCheckoutTime = (TextView) findView(R.id.tv_std_checkout_time_value);
        tvSelectedCheckoutDate = (TextView) findView(R.id.tv_selected_checkout_date);
        tvSelectedCheckoutTime = (TextView) findView(R.id.tv_selected_checkout_time);
        tvApplicableCharges = (TextView) findView(R.id.tv_applicable_charges_selected);
        btnLateCheckout = (Button) findView(R.id.btn_req_late_checkout);
        tvBottomMsg = (TextView) findView(R.id.tv_msg);


        lateCheckoutRequest = new LateCheckoutRequest();
        baseServiceRequest = new BaseServiceRequest();
        Bundle bundle = getArguments();
        if(bundle != null){
            GeneralServiceModel generalServiceModel = (GeneralServiceModel)bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if(generalServiceModel != null ){
                if(generalServiceModel.getGeneralServiceData() != null){
                    roomId = generalServiceModel.getGeneralServiceData().getHsId();
                    lateCheckoutRequest.setGsId(generalServiceModel.getGeneralServiceData().getId());
                }
                if(generalServiceModel.getHotelStaysList() != null && generalServiceModel.getHotelStaysList() != null){
                    staysPojo = generalServiceModel.getHotelStaysList().getStaysPojo();
                }
            }
        }

        setUpStandardCheckoutProperties();

        UserSession session = UserSession.getSessionInstance();
        if(session != null){
            lateCheckoutRequest.setUserId(session.getUserId());
            baseServiceRequest.setUserId(session.getUserId());
        }

        if(!TextUtils.isEmpty(roomId)){
            getRoomLateCheckoutService();
        }
        retreiveLateCheckout();
        setOnClickListener(R.id.tv_selected_checkout_date, R.id.tv_selected_checkout_time, R.id.btn_req_late_checkout);
    }

    private void retreiveLateCheckout() {
        HttpParamObject httpParamObject = ApiRequestGenerator.retreiveLateCheckout(baseServiceRequest);
        executeTask(AppConstants.TASK_CODES.RETREIVE_LATE_CHECKOUT, httpParamObject);
    }



    private void setUpStandardCheckoutProperties() {
        if(staysPojo != null){
            String checkOutDate = Util.convertDateFormat(staysPojo.getCheckOutDate(), Util.API_DATE_FORMAT, Util.LATE_CHECKOUT_DATE_FORMAT);
            String checkOutTime = Util.convertDateFormat(staysPojo.getCheckOutTime(), Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);

            tvStandardCheckoutTime.setText(checkOutTime);
            tvStandardCheckoutDate.setText(checkOutDate);
            tvSelectedCheckoutDate.setText(checkOutDate);
            tvSelectedCheckoutTime.setText(checkOutTime);
            tvBottomMsg.setText(R.string.checkout_btm_msg);

            lateCheckoutRequest.setHotelId(staysPojo.getHotelId());
            lateCheckoutRequest.setStaysId(staysPojo.getStaysId());

            baseServiceRequest.setHotelId(staysPojo.getHotelId());
            baseServiceRequest.setStaysId(staysPojo.getStaysId());
        }
    }

    private void getRoomLateCheckoutService() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getRoomLateCheckoutList(roomId);
        executeTask(AppConstants.TASK_CODES.GET_LATE_CHECKOUT_SERVICE_RESPONSE, httpParamObject);
    }

    private void requestLateCheckout() {
        if(requestPrice <= 0 || requestCheckoutTime <= 0 || requestCheckoutDate <= 0){
            showToast("Please select a late valid check out time");
            return;
        }


        lateCheckoutRequest.setCheckoutDate(requestCheckoutDate);
        lateCheckoutRequest.setCheckoutTime(requestCheckoutTime);
        lateCheckoutRequest.setPrice(requestPrice);

        HttpParamObject httpParamObject = ApiRequestGenerator.requestLateCheckout(lateCheckoutRequest);
        executeTask(AppConstants.TASK_CODES.REQUEST_LATE_CHECKOUT, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode){
            case AppConstants.TASK_CODES.GET_LATE_CHECKOUT_SERVICE_RESPONSE:
                GetLateCheckoutServiceResponse checkoutResponse = (GetLateCheckoutServiceResponse) response;
                if(checkoutResponse != null && !checkoutResponse.getError()){
                    checkoutServiceData = checkoutResponse.getData();
                    if(checkoutServiceData != null){
                        setUpDeviationAndPriceList();
                    }
                }
                break;
            case AppConstants.TASK_CODES.REQUEST_LATE_CHECKOUT:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if(apiResponse != null && !apiResponse.getError()){
                    showToast("Success");
                    getActivity().finish();
                    CartUpdateReceiver.sendBroadcast(getActivity(),CartUpdateReceiver.ACTION_UPDATE_CHECKOUT);

                }
                break;
            case AppConstants.TASK_CODES.RETREIVE_LATE_CHECKOUT:
                RetreiveLateCheckoutResponse lateCheckoutResponse = (RetreiveLateCheckoutResponse) response;
                if(lateCheckoutResponse != null && !lateCheckoutResponse.getError()) {
                    RetreiveLateCheckoutData checkoutData = lateCheckoutResponse.getData();
                    if(checkoutData != null) {
                        lateCheckoutRequest.setId(checkoutData.getId());
                        long selectedTime = checkoutData.getCheckoutTime();
                        if(selectedTime != 0) {
                            String showTime = Util.format(new Date(selectedTime), Util.CHECK_IN_TIME_UI_FORMAT);
                            tvSelectedCheckoutTime.setText(showTime);
                            tvApplicableCharges.setText(checkoutData.getPrice() + "");
                        }
                    }
                }
                break;
        }
    }

    private void setUpDeviationAndPriceList() {
        if(checkoutServiceData.getCheckOutInfo() != null){
            List<DeviationInformation> listDeviationInfo = checkoutServiceData.getCheckOutInfo().getListDeviationInfo();
            if(CollectionUtils.isNotEmpty(listDeviationInfo)){
                listDeviationAndPrice = new ArrayList<>();
                for(DeviationInformation info : listDeviationInfo){
                    if(info == null||info.getDeviationInHours()==null){
                        continue;
                    }
                    DeviationAndPrice deviationAdnAndPrice = new DeviationAndPrice();
                    deviationAdnAndPrice.setPrice(info.getPrice());
                    deviationAdnAndPrice.setDevaition((long)(info.getDeviationInHours() * (double)MILISECONDS_IN_HOUR));
                    listDeviationAndPrice.add(deviationAdnAndPrice);
                }

                Collections.sort(listDeviationAndPrice, new Comparator<DeviationAndPrice>() {
                    @Override
                    public int compare(DeviationAndPrice o1, DeviationAndPrice o2) {
                        return o1.getDevaition().compareTo(o2.getDevaition());
                    }
                });
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_selected_checkout_date:
                setDatePicker();
                break;
            case R.id.tv_selected_checkout_time:
                setTimePicker();
                break;
            case R.id.btn_req_late_checkout:
                requestLateCheckout();
                break;
        }
    }



    private void setTimePicker() {
        final Calendar c = Calendar.getInstance();
        String selectedTime = tvSelectedCheckoutTime.getText().toString();
        long checkoutTime = convertStringToLong(Util.CHECK_IN_TIME_UI_FORMAT , selectedTime);
        c.setTimeInMillis(checkoutTime);
        final int hour = c.get(Calendar.HOUR_OF_DAY);
        final int min = c.get(Calendar.MINUTE);
        //final int hour = (int)checkoutTime / MILISECONDS_IN_HOUR;
        //final int min = c.MINUTE;
        final TimePickerDialog  timePickerDialog = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        c.set(Calendar.MINUTE, minute);

                        SimpleDateFormat format = new SimpleDateFormat(Util.CHECK_IN_TIME_UI_FORMAT);
                        String time = format.format(c.getTime());

                        pickupTime = c.getTimeInMillis() - 1000;
                        Date getTime = c.getTime();
                        deliveryDate = Util.format(getTime, Util.DELIVERY_TIME_DATE_PATTERN);

                        tvSelectedCheckoutTime.setText(time);
                        verifyValidCheckoutTime();
                    }
                }, hour, min, false);

        timePickerDialog.show();


    }

    private void verifyValidCheckoutTime() {
        String standardTime = tvStandardCheckoutTime.getText().toString();
        String selectedTime = tvSelectedCheckoutTime.getText().toString();
        long selectedTimeinLong = convertStringToLong(Util.CHECK_IN_TIME_UI_FORMAT, selectedTime);
        long standardTimeInLong = convertStringToLong(Util.CHECK_IN_TIME_UI_FORMAT, standardTime);
        if(selectedTimeinLong < standardTimeInLong){
            showToast("Can't checkout before standard check out time");
            tvSelectedCheckoutTime.setText(standardTime);
            tvApplicableCharges.setText("");
        } else if(selectedTimeinLong > (standardTimeInLong + (3 * MILISECONDS_IN_HOUR))){
            showToast("Deviation can only be maximum of 3 hours");
            tvSelectedCheckoutTime.setText(standardTime);
            tvApplicableCharges.setText("");
        } else {
            long selectedDeviation = selectedTimeinLong - standardTimeInLong;
            calculateApplicableCharges(selectedDeviation, selectedTimeinLong);
        }

    }

    private void calculateApplicableCharges(long selectedDeviation, long selectedTimeInlong) {
        if(CollectionUtils.isNotEmpty(listDeviationAndPrice)){

            for(DeviationAndPrice deviationAndPrice : listDeviationAndPrice){
                if(deviationAndPrice == null || deviationAndPrice.getDevaition() < selectedDeviation){
                    continue;
                } else {
                    requestCheckoutDate = convertStringToLong(Util.LATE_CHECKOUT_DATE_FORMAT, tvStandardCheckoutDate.getText().toString());
                    requestCheckoutTime = selectedTimeInlong;
                    requestPrice = deviationAndPrice.getPrice();
                    tvApplicableCharges.setText(deviationAndPrice.getPrice() + "");
                    break;
                }
            }
        }

    }


    private void setDatePicker() {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                    new DatePickerDialog.OnDateSetListener() {
                        //  @RequiresApi(api = Build.VERSION_CODES.N)
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            c.set(year, monthOfYear, dayOfMonth);
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Util.LATE_CHECKOUT_DATE_FORMAT);
                            String fromDatePick = simpleDateFormat.format(c.getTime());

                         tvSelectedCheckoutDate.setText(fromDatePick);
                        }
                    }, mYear, mMonth, mDay);

            long minCheckoutDate = convertStringToLong(Util.LATE_CHECKOUT_DATE_FORMAT, tvStandardCheckoutDate.getText().toString());
            datePickerDialog.getDatePicker().setMinDate(minCheckoutDate);
            datePickerDialog.getDatePicker().setMaxDate(minCheckoutDate);
            datePickerDialog.show();
    }

    public long convertStringToLong(String dateFormat, String dateValue){
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        try{
            Date date = formatter.parse(dateValue);
            return date.getTime();
        } catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_late_checkout;
    }
}
