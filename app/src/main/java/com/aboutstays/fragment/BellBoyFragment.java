package com.aboutstays.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.aboutstays.R;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.rest.request.MaintenanceRequest;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.aboutstays.utitlity.DatePickerUtil;
import com.aboutstays.utitlity.TimePickerUtil;
import com.wdullaer.materialdatetimepicker.date.DatePickerController;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.receivers.CartUpdateReceiver;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by neeraj on 1/4/17.
 */

public class BellBoyFragment extends BaseFragment {

    private EditText etComment;
    private TextView tvDeliveryDate,tvRequestNow;
    private Button btnRequest;
    private static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    private String hotelId, staysId, userId;
    private long pickupTime;
    private boolean reqNow = false;
    public static final String BELL_BOY_DATE_FORMAT = "dd MMM yyyy, hh:mm a";
    public static final String BELL_BOY_DATE = "dd MMM yyyy";
    private String format, datePickerDate, deliveryDate, gsId, cgId;
    private Calendar calendarInstance;
    private long checkOutDate;

    @Override
    public void initViews() {
        initToolBar("Bell Boy");
        calendarInstance = Calendar.getInstance();
        Bundle bundle = getArguments();
        if (bundle != null) {
            GeneralServiceModel generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null) {
                cgId = generalServiceModel.getGeneralServiceData().getHsId();
                gsId = generalServiceModel.getGeneralServiceData().getId();
                if (generalServiceModel.getHotelStaysList().getStaysPojo() != null) {
                    hotelId = generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId();
                    staysId = generalServiceModel.getHotelStaysList().getStaysPojo().getStaysId();
                    userId = generalServiceModel.getHotelStaysList().getStaysPojo().getUserId();
                }
            }
        }

        checkOutDate = Preferences.getData(AppConstants.PREF_KEYS.CHECKOUT_DATE_TIME, 0l);
        btnRequest = (Button) findView(R.id.btn_req_bellBoy);

        tvRequestNow = (TextView) findView(R.id.tv_add);
        etComment = (EditText) findView(R.id.et_comment);
        tvDeliveryDate = (TextView) findView(R.id.tv_date_time);
        View view = findView(R.id.view_line);
        RelativeLayout rlHeader = (RelativeLayout) findView(R.id.rl_total);
        RelativeLayout rlline = (RelativeLayout) findView(R.id.rl_row_view0);
        LinearLayout llDatePick = (LinearLayout) findView(R.id.rl_row_data);
        LinearLayout llMsg = (LinearLayout) findView(R.id.ll_msg);
        TextView tvTitle = (TextView) findView(R.id.tv_title);
        tvTitle.setText(getString(R.string.preferred_dateTime));
        rlHeader.setVisibility(View.GONE);
        view.setVisibility(View.GONE);
        llMsg.setVisibility(View.GONE);
        rlline.setVisibility(View.GONE);

        setOnClickListener(R.id.rl_row_data, R.id.tv_add, R.id.btn_req_bellBoy);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_row_data:
                setReqNowFalse();
                setDateandTime();
                break;
            case R.id.tv_add:
                if (reqNow == false) {
                    reqNow = true;
                    tvRequestNow.setBackgroundResource(R.drawable.shape_button_selected);
                    tvRequestNow.setTextColor(Color.WHITE);
//                    tvRequestNow.setText(R.string.requested);
//                    selectCurrentDateandTime();
                } else {
                    setReqNowFalse();
                }
                break;
            case R.id.btn_req_bellBoy:
                boolean ifBeforeCheckout = Util.ifGreaterThnCheckOut(pickupTime, checkOutDate);
                if (ifBeforeCheckout) {
                    validateAndFillData();
                } else {
                    errorDialoge(getString(R.string.select_date_error));
                    return;
                }
                break;
        }
    }

    private void errorDialoge(String string) {
        SuccessDialogFragment.show(getChildFragmentManager(),
                "BELL BOY",
                getString(R.string.order_error),
                string,
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                    }
                }
        );
    }

    private void validateAndFillData() {
        MaintenanceRequest maintenanceRequest = new MaintenanceRequest();
        maintenanceRequest.setHotelId(hotelId);
        maintenanceRequest.setGsId(gsId);
        maintenanceRequest.setStaysId(staysId);
        maintenanceRequest.setUserId(userId);
        String comment = etComment.getText().toString().trim();
        if (!TextUtils.isEmpty(comment)) {
            maintenanceRequest.setComment(comment);
        }
/*        if (pickupTime > 0) {
            maintenanceRequest.setDeliveryTime(pickupTime);
        } else {
            showToast("Please select a time first");
            return;
        }*/

        if (reqNow == true) {
            maintenanceRequest.setRequestedNow(true);
        } else if (pickupTime > 0) {
            maintenanceRequest.setRequestedNow(false);
            maintenanceRequest.setDeliveryTime(pickupTime);
        } else {
            showToast(getString(R.string.please_select_time_first));
            return;
        }
        if (reqNow == false && TextUtils.isEmpty(deliveryDate)) {
            showToast(getString(R.string.please_select_time_first));
            return;
        } else {
            maintenanceRequest.setDeliverTimeString(deliveryDate);
        }

        requestBellBoy(maintenanceRequest);
    }

    private void selectCurrentDateandTime() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time =&gt; " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat(BELL_BOY_DATE_FORMAT);
        String formattedDate = df.format(c.getTime());
        tvDeliveryDate.setText(formattedDate);
        pickupTime = c.getTimeInMillis() - 1000;
    }

    private void setReqNowFalse() {
        reqNow = false;
        tvRequestNow.setBackgroundResource(R.drawable.shape_button_email);
        tvRequestNow.setTextColor(getResourceColor(R.color.color_red_bg));
        tvRequestNow.setText(R.string.request_now);
        tvDeliveryDate.setText("");
        pickupTime = 0;
        deliveryDate = "";
    }

    private void requestBellBoy(MaintenanceRequest maintenanceRequest) {
        HttpParamObject httpParamObject = ApiRequestGenerator.requestBellBoy(maintenanceRequest);
        executeTask(AppConstants.TASK_CODES.GET_BELL_BOY, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_BELL_BOY:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false) {
                    successDialog();
                }
                break;
        }
    }

    private void successDialog() {

        SuccessDialogFragment.show(getChildFragmentManager(),
                "BELL BOY",
                getString(R.string.request_submitted),
                getString(R.string.request_bellboy_schedule),
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                        getActivity().finish();
                    }
                }
        );
    }

    private void setDateandTime() {

        final AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        DatePickerUtil datePickerUtil = new DatePickerUtil(BELL_BOY_DATE, calendarInstance, appCompatActivity.getFragmentManager(), getString(R.string.select_date), true, false, new DatePickerUtil.OnDateSelected() {
            @Override
            public void onDateSelected(Calendar calendar, String date) {
                TimePickerUtil timePickerUtil = new TimePickerUtil(BELL_BOY_DATE_FORMAT, calendar, appCompatActivity.getFragmentManager(), getString(R.string.select_time), new TimePickerUtil.OnTimeSelected() {
                    @Override
                    public void onTimeSelected(Calendar calendar, String date) {
                        calendarInstance = calendar;
                        pickupTime = calendar.getTimeInMillis() - 1000;
                        tvDeliveryDate.setText(date.toUpperCase());
                        Date time = calendar.getTime();
                        deliveryDate = Util.format(time, Util.DELIVERY_TIME_DATE_PATTERN);
                    }
                });
                timePickerUtil.show();
            }

            @Override
            public void onCanceled() {
//                tvDeliveryDate.setText("");
            }
        });
        datePickerUtil.show();

    }


    @Override
    public int getViewID() {
        return R.layout.fragment_bell_boy;
    }

}
