package com.aboutstays.fragment;

import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.ImageView;

import com.aboutstays.R;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by ajay on 14/12/16.
 */

public class LoyaltyLoginFragment extends BaseFragment {


    private TextInputLayout tilUserName, tilPassword;
    private Button btnFetchDetails;
    private ImageView ivLogo;


    public static LoyaltyLoginFragment getInstance() {
        LoyaltyLoginFragment fragment = new LoyaltyLoginFragment();
        return fragment;
    }

    @Override
    public void initViews() {

        initToolBar("Loyalty Login");
        tilPassword = (TextInputLayout) findView(R.id.til_password);
        tilUserName = (TextInputLayout) findView(R.id.til_username);
        btnFetchDetails = (Button) findView(R.id.btn_fetch_details);
        ivLogo = (ImageView) findView(R.id.iv_loyalty_login_logo);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_loyalty_login;
    }
}


