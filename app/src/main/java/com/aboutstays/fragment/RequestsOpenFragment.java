package com.aboutstays.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.RequestDetailsActivity;
import com.aboutstays.model.OpenModel;
import com.aboutstays.rest.response.GeneralDashboardItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

/**
 * Created by Rahul Aarya on 23-12-2016.
 */

public class RequestsOpenFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    private CustomListAdapter listAdapter;
    private List<GeneralDashboardItem> openList = new ArrayList<>();
    private ListView openListView;
    private TextView tvEmptyView;

    public static RequestsOpenFragment getInstance() {
        RequestsOpenFragment requestsOpenFragment = new RequestsOpenFragment();
        return requestsOpenFragment;
    }

    public void setOpenList(List<GeneralDashboardItem> openList) {
        this.openList.clear();
        if (!CollectionUtils.isEmpty(openList)) {
            this.openList.addAll(openList);
            //sortOpenList();
        }
        setAdapter();
    }

    private void setAdapter() {
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_request_open, openList, this);
        openListView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
        openListView.setEmptyView(tvEmptyView);
        openListView.setOnItemClickListener(this);
    }

    private void sortOpenList() {
        if (CollectionUtils.isNotEmpty(openList)) {
            Collections.sort(openList, new Comparator<GeneralDashboardItem>() {
                @Override
                public int compare(GeneralDashboardItem o1, GeneralDashboardItem o2) {
                    Long requestTime1 = o1.getOrderedTime();
                    Long requestTime2 = o2.getOrderedTime();
                    return requestTime2.compareTo(requestTime1);
                }
            });
        }
    }

    @Override
    public void initViews() {
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        openListView = (ListView) findView(R.id.lv_schedule);
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_scheduled;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        OpenRequestHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_request_open, parent, false);
            holder = new OpenRequestHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (OpenRequestHolder) convertView.getTag();
        }

        GeneralDashboardItem dashboardItem = openList.get(position);
        holder.textTitle.setText(dashboardItem.getTitle());
        holder.textItems.setText(dashboardItem.getDescription());
        holder.textStatus.setText(dashboardItem.getStatus());
        if (!TextUtils.isEmpty(dashboardItem.getImageUrl())) {
            Picasso.with(getActivity()).load(dashboardItem.getImageUrl())
                    .placeholder(R.drawable.progress_animation)
                    .error(R.mipmap.place_holder_circular)
                    .into(holder.imageTitle);
        } else {
            holder.imageTitle.setImageResource(R.drawable.image_placeholder);
        }
        if (!TextUtils.isEmpty(dashboardItem.getStatusImageUrl())) {
            Picasso.with(getActivity()).load(dashboardItem.getStatusImageUrl())
                    .placeholder(R.drawable.progress_animation)
                    .error(R.mipmap.processing_icon)
                    .into(holder.imageStatus);
        } else {
            holder.imageStatus.setImageResource(R.mipmap.processing_icon);
        }
        if (dashboardItem.getOrderedTime() != 0) {
            String deliveryTime = Util.format(new Date(dashboardItem.getOrderedTime()), Util.REQUEST_DASHBOARD_DELIVERY_TIME_UI_FORMAT);
            holder.textDateTime.setText(deliveryTime);
        } else {
            holder.textDateTime.setText("");
        }
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        GeneralDashboardItem dashboardItem = openList.get(position);
        if (!TextUtils.isEmpty(dashboardItem.getUosId())) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(AppConstants.BUNDLE_KEYS.GENERAL_DASHBOARD_ITEM, dashboardItem);
            bundle.putInt(AppConstants.BUNDLE_KEYS.REQUEST_FROM, 1);
            startNextActivity(bundle, RequestDetailsActivity.class);
        }
    }

    class OpenRequestHolder {
        ImageView imageTitle, imageStatus;
        TextView textTitle, textDateTime, textStatus, textItems;

        public OpenRequestHolder(View view) {
            imageTitle = (ImageView) view.findViewById(R.id.iv_row_requests_icon);
            imageStatus = (ImageView) view.findViewById(R.id.iv_status);
            textTitle = (TextView) view.findViewById(R.id.tv_row_title);
            textItems = (TextView) view.findViewById(R.id.tv_items_list);
            textDateTime = (TextView) view.findViewById(R.id.tv_date_time);
            textStatus = (TextView) view.findViewById(R.id.tv_status);
        }
    }
}
