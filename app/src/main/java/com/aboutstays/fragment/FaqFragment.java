package com.aboutstays.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FaqDetailsActivity;
import com.aboutstays.model.FaqModel;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;

public class FaqFragment extends BaseFragment implements CustomListAdapterInterface {
    private ListView lvfaq;
    private CustomListAdapter faqAdapter;
    private List<FaqModel> faqModelList = new ArrayList<>();

    public static FaqFragment getInstance(){
        FaqFragment faqFragment = new FaqFragment();
        return faqFragment;
    }


    @Override
    public void initViews() {

        lvfaq = (ListView) findView(R.id.list_faq);
        faqAdapter = new CustomListAdapter(getActivity(), R.layout.row_faq, faqModelList, this);
        lvfaq.setAdapter(faqAdapter);
        setFaqData();
        faqAdapter.notifyDataSetChanged();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_faq;
    }

    private void setFaqData() {
        for (int i=0; i<10; i++){
            FaqModel faqModel = new FaqModel();
            faqModel.setFaqText(getString(R.string.dummy_text));
            faqModelList.add(faqModel);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.row_faq,parent,false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }
        FaqModel faqModel = faqModelList.get(position);

        holder.text.setText(faqModel.getFaqText());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNextActivity(FaqDetailsActivity.class);
            }
        });

        return convertView;
    }

    class Holder{
        TextView text;

    public Holder(View view){
        text = (TextView) view.findViewById(R.id.tv_faq);
    }
    }

}
