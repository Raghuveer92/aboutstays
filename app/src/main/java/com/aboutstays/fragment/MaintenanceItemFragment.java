package com.aboutstays.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.maintenance.ListMaintenanceItemType;
import com.aboutstays.model.maintenance.MaintenanceData;
import com.aboutstays.model.maintenance.MaintenanceItemData;
import com.aboutstays.model.maintenance.MaintenanceItemResponse;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by neeraj on 1/4/17.
 */

public class MaintenanceItemFragment extends BaseFragment implements CustomListAdapterInterface {

    private String typeName;
    private Integer type;
    private String hsId;
    private ListView lvMaintenance;
    private CustomListAdapter listAdapter;
    List<MaintenanceItemData> listMaintenanceItem = new ArrayList<>();
    private TextView tvEmptyView;
    private GeneralServiceModel generalServiceModel;
    private ListMaintenanceItemType maintenanceItemType;

    @Override
    public void initViews() {

        Bundle bundle = getArguments();
        if (bundle != null){
            generalServiceModel = (GeneralServiceModel)bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if(generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null){
                hsId = generalServiceModel.getGeneralServiceData().getHsId();
            }
            maintenanceItemType = (ListMaintenanceItemType) bundle.getSerializable(AppConstants.BUNDLE_KEYS.MAINTENANCE_ITEM);
            if (maintenanceItemType != null){
                type = maintenanceItemType.getType();
                typeName = maintenanceItemType.getTypeName();
            }
        }

        if (!TextUtils.isEmpty(typeName)){
            initToolBar(typeName);
        } else {
            initToolBar("");
        }

        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        lvMaintenance = (ListView) findView(R.id.lv_maintenance);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_room_service, listMaintenanceItem, this);
        lvMaintenance.setAdapter(listAdapter);
        lvMaintenance.setEmptyView(tvEmptyView);
        listAdapter.notifyDataSetChanged();
        getMaintenanceItemData();

    }

    private void getMaintenanceItemData() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getMaintenanceItem(hsId, type);
        executeTask(AppConstants.TASK_CODES.GET_MAINTENANCE_ITEM, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.GET_MAINTENANCE_ITEM:
                MaintenanceItemResponse itemResponse = (MaintenanceItemResponse) response;
                if (itemResponse != null && itemResponse.getError() == false){
                    List<MaintenanceItemData> maintenamceItemData = itemResponse.getData();
                    if (CollectionUtils.isNotEmpty(maintenamceItemData)){
                        listMaintenanceItem.addAll(maintenamceItemData);
                        listAdapter.notifyDataSetChanged();
                    }

                }
                break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_maintenance;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final MaintenanceItemData maintenanceItemData = listMaintenanceItem.get(position);
        holder.title.setText(maintenanceItemData.getName());
        holder.titleImage.setVisibility(View.GONE);
        holder.tvTime.setVisibility(View.GONE);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.MAINTENANCE_ITEM, maintenanceItemType);
                bundle.putString(AppConstants.BUNDLE_KEYS.MAINTENANCE_ITEM_NAME, maintenanceItemData.getName());
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.MAINTENANCE_DETAIL_FRAGMENT, bundle, AppConstants.REQUEST_CODE.ORDER_PLACED);
            }
        });

        return convertView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppConstants.RESULT_CODE.RESULT_DONE){
            getActivity().setResult(AppConstants.RESULT_CODE.RESULT_DONE);
            getActivity().finish();
        }
    }

    class Holder {
        TextView title, tvTime, tvItemCount;
        ImageView titleImage, arrowImage;
        RelativeLayout rlCount;

        public Holder(View view) {
            title = (TextView) view.findViewById(R.id.tv_title);
            titleImage = (ImageView) view.findViewById(R.id.iv_title_image1);
            arrowImage = (ImageView) view.findViewById(R.id.iv_right_angle);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_count);
            tvItemCount = (TextView) view.findViewById(R.id.tv_item_count);
        }
    }
}
