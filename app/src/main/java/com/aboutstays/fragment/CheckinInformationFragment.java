package com.aboutstays.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.CheckInDetailsActivity;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.activity.HotelInfoActivity;
import com.aboutstays.activity.checkout.InitiateCheckoutActivity;
import com.aboutstays.adapter.BaseRecycleAdapter;
import com.aboutstays.enums.ServiceType;
import com.aboutstays.model.BaseAdapterModel;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.currentStays.GetAllServicesData;
import com.aboutstays.model.currentStays.StaysService;
import com.aboutstays.model.initiate_checkIn.CheckInData;
import com.aboutstays.model.stayline.HotelStaysList;
import com.aboutstays.rest.request.BaseServiceRequest;
import com.aboutstays.rest.response.GeneralServiceData;
import com.aboutstays.rest.response.GeneralServiceList;
import com.aboutstays.rest.response.GetAllServicesResponse;
import com.aboutstays.rest.response.GetHotelGeneralServicesResponse;
import com.aboutstays.rest.response.UserOptedService;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

import static android.app.Activity.RESULT_OK;

/**
 * Created by ajay on 23/12/16.
 */

public class CheckinInformationFragment extends BaseFragment implements BaseRecycleAdapter.RecyclerClickListener {

    private RecyclerView rvInfo;
    private BaseRecycleAdapter baseRecyclerAdapter;
    private List<BaseAdapterModel> informationModelList = new ArrayList<>();
    private Integer[] logo;
    private Button btnCheckIn;
    private TextView tvBottomMsg;
    private String selectId;
    private boolean checkIn, stay, checkOut;
    private HotelStaysList hotelStaysList;
    private String stayId;
    private String hotelId;
    private String userId;
    private LinearLayout ll_btn, ll_msg;
    private String stayStatus = "1";
    private String isGI = "true";
    private Map<String, GeneralServiceModel> mapGSIdVSModel = new HashMap<>();
    private boolean isActive = false;
    private CheckInData checkinData;

    private BaseServiceRequest baseServiceRequest;
    private List<StaysService> listStaysService = new ArrayList<>();
    private boolean showCheckInButton;
    private boolean showCheckoutButton;
    private String checkinButtonText;

//    public void setListStaysService(List<StaysService> listStaysService) {
//        if(CollectionUtils.isNotEmpty(listStaysService)) {
//            this.listStaysService.clear();
//            this.listStaysService = listStaysService;
//            refreshServiceModelList();
//        }
//    }
//
//    public void setShowCheckInButton(boolean showCheckInButton) {
//        this.showCheckInButton = showCheckInButton;
//        setBottomButtonAndMsg();
//    }

//    public void setShowCheckoutButton(boolean showCheckoutButton) {
//        this.showCheckoutButton = showCheckoutButton;
//        setBottomButtonAndMsg();
//    }

    private void refreshServiceModelList() {
        informationModelList.clear();
        for(StaysService staysService : listStaysService) {
            if(staysService == null) {
                continue;
            }
            GeneralServiceModel model = new GeneralServiceModel();
            model.setHotelStaysList(hotelStaysList);

            GeneralServiceData serviceData = new GeneralServiceData();
            serviceData.setActivatedImageUrl(staysService.getServiceImageUrl());
            serviceData.setName(staysService.getServiceName());
            serviceData.setHsId(staysService.getServiceId());
            serviceData.setId(staysService.getGsId());
            serviceData.setServiceType(staysService.getServiceType());
            serviceData.setUiOrder(staysService.getUiOrder());
            serviceData.setStatus(staysService.getStaysStatus());
            model.setGeneralServiceData(serviceData);
            model.setShowTick(staysService.isShowTick());
            model.setShowNumber(staysService.isShowNumber());
            model.setHeadCount(staysService.getCount());
            model.setClickable(staysService.isClickable());
            informationModelList.add(model);
        }
        if (baseRecyclerAdapter != null) {
            baseRecyclerAdapter.notifyDataSetChanged();
        }
    }


    public static CheckinInformationFragment getInstance(String selectId) {
        CheckinInformationFragment fragment = new CheckinInformationFragment();
        fragment.selectId = selectId;
        return fragment;
    }



    @Override
    public void initViews() {

        UserSession userSession = UserSession.getSessionInstance();
        if (userSession != null) {
            userId = userSession.getUserId();
        }

        ll_btn = (LinearLayout) findView(R.id.ll_btn_and_msg);
        ll_msg = (LinearLayout) findView(R.id.ll_msg);
        btnCheckIn = (Button) findView(R.id.btn_check_in);
        tvBottomMsg = (TextView) findView(R.id.tv_msg);
        rvInfo = (RecyclerView) findView(R.id.rv_information);

        logo = new Integer[]{
                R.mipmap.hotel_info,
                R.mipmap.airplane,
                R.mipmap.envelope
        };

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            hotelStaysList = (HotelStaysList) bundle.getSerializable(AppConstants.BUNDLE_KEYS.HOTEL_CHECK_IN);
            if (hotelStaysList != null && hotelStaysList.getStaysPojo() != null) {
                stayId = hotelStaysList.getStaysPojo().getStaysId();
                hotelId = hotelStaysList.getStaysPojo().getHotelId();
                Integer partnershipType = hotelStaysList.getPartnershipType();
                if (partnershipType == 0){
                    ll_msg.setVisibility(View.VISIBLE);
                    tvBottomMsg.setText(R.string.non_partner_hotel);
                    btnCheckIn.setVisibility(View.GONE);
                } else {
                    ll_msg.setVisibility(View.GONE);
                }
            }
        }

        baseRecyclerAdapter = new BaseRecycleAdapter(getActivity(), informationModelList);
        rvInfo.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        rvInfo.setAdapter(baseRecyclerAdapter);
        baseRecyclerAdapter.setListener(this);
       // stayStatus = "1";
        //loadInformationData();
       // setAllFalse();
      //  checkIn = true;

//        getAllServices();
        setOnClickListener(R.id.btn_check_in);
       // setBottomButtonAndMsg();
       // refreshCheckInData();
        ll_btn.setVisibility(View.GONE);
        ll_msg.setVisibility(View.GONE);
    }

    private void getAllServices() {
        baseServiceRequest = new BaseServiceRequest();
        baseServiceRequest.setHotelId(hotelId);
        baseServiceRequest.setStaysId(stayId);
        baseServiceRequest.setUserId(userId);
        HttpParamObject httpParamObject = ApiRequestGenerator.getAllServices(baseServiceRequest);
        executeTask(AppConstants.TASK_CODES.GET_ALL_SERVICES, httpParamObject);
    }




    private void loadInformationData() {
  //      informationModelList.clear();
  //      HttpParamObject httpParamObject = ApiRequestGenerator.getAllService(hotelId, stayStatus, isGI);
  //      executeTask(AppConstants.TASK_CODES.GET_HOTEL_GENERAL_INFORMATION, httpParamObject);
//        getUserOptedServices();
    }

//    private void loadCheckinData() {
//        informationModelList.clear();
//        for (int i=0; i<2; i++){
//            GeneralInformationModel generalInformationModel = new GeneralInformationModel();
//            generalInformationModel.setTitle("Hotel info");
//            generalInformationModel.setLogo(logo[i]);
//            informationModelList.add(generalInformationModel);
//            baseRecyclerAdapter.notifyDataSetChanged();
//        }
//    }
//
//    private void loadStayData() {
//        informationModelList.clear();
//        for (int i=0; i<1; i++){
//            GeneralInformationModel generalInformationModel = new GeneralInformationModel();
//            generalInformationModel.setTitle("Hotel");
//            generalInformationModel.setLogo(logo[i]);
//            informationModelList.add(generalInformationModel);
//            baseRecyclerAdapter.notifyDataSetChanged();
//        }
//    }
//
//    private void loadCheckOutData() {
//        informationModelList.clear();
//        for (int i=0; i<3; i++){
//            GeneralInformationModel generalInformationModel = new GeneralInformationModel();
//            generalInformationModel.setTitle("info");
//            generalInformationModel.setLogo(logo[i]);
//            informationModelList.add(generalInformationModel);
//            baseRecyclerAdapter.notifyDataSetChanged();
//        }
//    }


    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.btn_check_in:
                if(showCheckInButton){
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, hotelStaysList);
                    startNextActivityForResult(bundle, CheckInDetailsActivity.class,AppConstants.REQUEST_CODES.CHECKIN);
                }else if(showCheckoutButton){
                    InitiateCheckoutActivity.startActivity(getActivity(),hotelId,stayId);
                }
                break;
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        if(null == getActivity())return;
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
//            case AppConstants.TASK_CODES.GET_HOTEL_GENERAL_INFORMATION:
//                GetHotelGeneralServicesResponse generalServicesResponse = (GetHotelGeneralServicesResponse) response;
//                if (generalServicesResponse != null && generalServicesResponse.getError() == false) {
//                    GeneralServiceList serviceList = generalServicesResponse.getData();
//                    if (serviceList != null) {
//                        List<GeneralServiceData> listGS = serviceList.getListGS();
//                        if (listGS != null ) {
//                            Collections.sort(listGS);
//                            for (GeneralServiceData service : listGS) {
//                                GeneralServiceModel model = new GeneralServiceModel();
//                                model.setGeneralServiceData(service);
//                                model.setShowNumerable(service.getNumerable());
//                                model.setShowTickable(service.getTickable());
//                                model.setHotelStaysList(hotelStaysList);
//                                model.getGeneralServiceData().setGeneralInfo(service.isGeneralInfo());
//                                informationModelList.add(model);
////                                model.setActive(isActive);
//                                mapGSIdVSModel.put(service.getId(), model);
//                            }
//                        }
//                        baseRecyclerAdapter.notifyDataSetChanged();
//                    } else {
//
//                        showToast(generalServicesResponse.getMessage());
//                    }
//                    break;
            case AppConstants.TASK_CODES.GET_ALL_SERVICES:
                GetAllServicesResponse serviceResponse = (GetAllServicesResponse) response;
                if(serviceResponse != null && !serviceResponse.getError()) {
                    GetAllServicesData serviceData = serviceResponse.getData();
                    if (serviceData != null) {
                        showCheckoutButton = serviceData.isShowCheckoutButton();
                        showCheckInButton = serviceData.isShowCheckInButton();
                        checkinButtonText = serviceData.getCheckinButtonText();
                        List<StaysService> tempList = serviceData.getServicesList();
                        if (CollectionUtils.isNotEmpty(tempList)) {
                            listStaysService = new ArrayList<>();
                            listStaysService.clear();
                            for (StaysService staysService : tempList) {
                                if (staysService == null) {
                                    continue;
                                }
                                if (staysService.isGeneralInfo()) {
                                    listStaysService.add(staysService);
                                }
                            }
                            setBottomButtonAndMsg();
                            sortServiceList();
                            refreshServiceModelList();
                        }
                    }
                }
                break;
        }
    }


    private void sortServiceList() {
        if(CollectionUtils.isNotEmpty(listStaysService)) {
            Collections.sort(listStaysService, new Comparator<StaysService>() {
                @Override
                public int compare(StaysService o1, StaysService o2) {
                    Integer uiOrder1 = o1.getUiOrder();
                    Integer uiOrder2 = o2.getUiOrder();
                    if(uiOrder1 == null && uiOrder2 == null) {
                        return 0;
                    }
                    return uiOrder1.compareTo(uiOrder2);
                }
            });
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_information_chechin;
    }

    public void setStatus(String selectId) {

    }

    public void setCheckIn(String checkIn) {
        this.selectId = checkIn;
        setAllFalse();
        stayStatus = "1";
        loadInformationData();
//        loadCheckinData();
        this.checkIn = true;
        setBottomButtonAndMsg();
    }

    public void setStay(String stay) {
        this.selectId = stay;
        setAllFalse();
        stayStatus = "2";
        loadInformationData();
//        loadStayData();
        this.stay = true;
        setBottomButtonAndMsg();
    }

    public void setCheckOut(String checkOut) {
        this.selectId = checkOut;
        setAllFalse();
        stayStatus = "3";
        loadInformationData();
//        loadCheckOutData();
        this.checkOut = true;
        setBottomButtonAndMsg();
    }

    private void setBottomButtonAndMsg() {
        if(showCheckInButton) {
            if(checkinButtonText != null){
                btnCheckIn.setText(checkinButtonText);
            }
            ll_btn.setVisibility(View.VISIBLE);
            ll_msg.setVisibility(View.GONE);
        } else if(showCheckoutButton) {
            ll_btn.setVisibility(View.VISIBLE);
            ll_msg.setVisibility(View.GONE);
            if(checkinButtonText != null){
                btnCheckIn.setText(checkinButtonText);
            }
        } else {
//            ll_btn.setVisibility(View.GONE);
//            ll_msg.setVisibility(View.GONE);
        }

//        isActive = isServiceActive();
//        if (checkIn) {
//            if (isActive == true) {
//                btnCheckIn.setText("Check-IN");
//                ll_btn.setVisibility(View.VISIBLE);
//                ll_msg.setVisibility(View.GONE);
//            } else {
//                ll_btn.setVisibility(View.GONE);
//                ll_msg.setVisibility(View.VISIBLE);
//                tvBottomMsg.setText(getString(R.string.check_in_btm_msg));
//            }
//        } else if (stay) {
//            ll_btn.setVisibility(View.GONE);
//            ll_msg.setVisibility(View.GONE);
//
//        } else if (checkOut) {
//            ll_btn.setVisibility(View.VISIBLE);
//            ll_msg.setVisibility(View.GONE);
//            btnCheckIn.setText("Check-Out");
//        }
    }

//    public boolean isServiceActive() {
//        String DATE_FORMAT = "dd-MM-yyyy HH:mm";
//        if (hotelStaysList != null && hotelStaysList.getStaysPojo() != null) {
//            String checkInDate = hotelStaysList.getStaysPojo().getCheckInDate();
//            String checkInTime = hotelStaysList.getStaysPojo().getCheckInTime();
//            String checkOutDate = hotelStaysList.getStaysPojo().getCheckOutDate();
//            String checkOutTime = hotelStaysList.getStaysPojo().getCheckOutTime();
//            String checkIn = checkInDate + " " + checkInTime;
//            String checkOut = checkOutDate + " " + checkOutTime;
//            try {
//                Date bookingDate = Util.convertStringToDate(checkIn, DATE_FORMAT);
//                Date outDate = Util.convertStringToDate(checkOut, DATE_FORMAT);
//                Date date = new Date();
//                long difference = bookingDate.getTime() - date.getTime();
//                long hours = difference / (1000 * 60 * 60);
//                if (hours > 0 && hours < 48) {
//                    return true;
//                }
//                if (bookingDate.before(date) && date.before(outDate)) {
//                    return true;
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return false;
//    }


    private void setAllFalse() {
        stay = false;
        checkIn = false;
        checkOut = false;
    }

    @Override
    public void onItemClicked(int position, Object model, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.NEXT_ACTIVITY:
                GeneralServiceModel generalServiceModel = (GeneralServiceModel) model;
                if(generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null){

                    int serviceType = generalServiceModel.getGeneralServiceData().getServiceType();
                    ServiceType typeEnum = ServiceType.getServiceTypeByCode(serviceType);
                    if(typeEnum == null){
                        showToast("Under Development");
                        return;
                    }

                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.BUNDLE_KEYS.HOTEL_ID,hotelId);
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                    switch (typeEnum){
                        case CITY_GUIDE:
                            FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.CITY_GUIDE_FRAGMENT, bundle);
                            return;
                        case HOUSE_RULES:
                            FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.HOUSE_RULES, bundle);
                            return;
                        case TV_ENTERTAINMENT:
                            FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.TV_FRAGMENT, bundle);
                            return;
                        case COMPLEMENTRY_SERVICE:
                            FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.COMPLEMENTARY_SERVICE, bundle);
                            return;
                        default:
                            if(TextUtils.isEmpty(generalServiceModel.getGeneralServiceData().getHsId())){
                                showToast("Under Development");
                                return;
                            }
                    }


                    Class targetClass = ServiceType.getTargetClassByServiceType(serviceType);
                    if(targetClass != null ){
                        Intent intent = new Intent(getActivity(), targetClass);
                        intent.putExtras(bundle);
//                        if (!generalServiceModel.isActive()){
//                            return;
//                        } else if (generalServiceModel.isUserOpted()){
//                            for (UserOptedService userOptedService : listUserOptedServices){
//                                String gsId = userOptedService.getGsId();
//                                String id = generalServiceModel.getGeneralServiceData().getId();
//                                if (gsId.equals(id)){
//                                    String refId = userOptedService.getRefId();
//                                    bundle.putString(AppConstants.BUNDLE_KEYS.REF_ID, refId);
//                                    intent.putExtras(bundle);
////                                    startNextActivity(bundle, targetClass);
//                                    startActivityForResult(intent, AppConstants.REQUEST_CODE.SERVICE_REQ);
//                                    break;
//                                }
//
//                            }
//
//                        } else {
                            startActivityForResult(intent, AppConstants.REQUEST_CODE.SERVICE_REQ);
                    }

                }
                break;
        }
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK){
//            stayStatus = "1";
//            //loadInformationData();
//            getAllServices();
//        }
//    }

    public void setCheckinData(CheckInData checkinData) {
        this.checkinData = checkinData;
       // refreshCheckInData();
    }

    private void refreshCheckInData() {
        if(!stayStatus.equalsIgnoreCase("1")){
            return;
        }
        if(v!=null){
            if(checkinData!=null){
                btnCheckIn.setText(R.string.modify_checkin);
            }else {
                btnCheckIn.setText(R.string.chech_in);
            }
        }
    }

}
