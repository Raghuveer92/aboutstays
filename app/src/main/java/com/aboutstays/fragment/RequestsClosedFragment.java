package com.aboutstays.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.RequestDetailsActivity;
import com.aboutstays.model.RequestsModel;
import com.aboutstays.rest.response.GeneralDashboardItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

/**
 * Created by Rahul Aarya on 23-12-2016.
 */

public class RequestsClosedFragment extends BaseFragment implements CustomListAdapterInterface {

    private CustomListAdapter listAdapter;
    private List<GeneralDashboardItem> requestsList = new ArrayList<>();
    private ListView requestsListView;
    private TextView tvEmptyView;

    public static RequestsClosedFragment getInstance() {
        RequestsClosedFragment requestsClosedFragment = new RequestsClosedFragment();
        return requestsClosedFragment;
    }

    public void setRequestsList(List<GeneralDashboardItem> requestsList) {
        this.requestsList.clear();
        if (!CollectionUtils.isEmpty(requestsList)) {
            this.requestsList.addAll(requestsList);
            //sortClosedList();
            if (listAdapter != null) {
                listAdapter.notifyDataSetChanged();
            }
        }

    }

/*    private void sortClosedList() {
        if(CollectionUtils.isNotEmpty(requestsList)) {
            Collections.sort(requestsList, new Comparator<GeneralDashboardItem>() {
                @Override
                public int compare(GeneralDashboardItem o1, GeneralDashboardItem o2) {
                    Long requestTime1 = o1.getOrderedTime();
                    Long requestTime2 = o2.getOrderedTime();
                    return requestTime2.compareTo(requestTime1);
                }
            });
        }
    }*/

    @Override
    public void refreshData() {
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void initViews() {
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        requestsListView = (ListView) findView(R.id.lv_schedule);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_request_open, requestsList, this);
        requestsListView.setAdapter(listAdapter);
        requestsListView.setEmptyView(tvEmptyView);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_scheduled;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent,
                        int resourceID, LayoutInflater inflater) {
        ClosedRequestHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_request_open, parent, false);
            holder = new ClosedRequestHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ClosedRequestHolder) convertView.getTag();
        }

        final GeneralDashboardItem dashboardItem = requestsList.get(position);
        if (dashboardItem != null) {
            holder.textTitle.setText(dashboardItem.getTitle());
            holder.textItems.setText(dashboardItem.getDescription());
            holder.textStatus.setText(dashboardItem.getStatus());
            if (!TextUtils.isEmpty(dashboardItem.getImageUrl())) {
                Picasso.with(getActivity()).load(dashboardItem.getImageUrl())
                        .error(R.mipmap.place_holder_circular)
                        .into(holder.imageTitle);
            } else {
                holder.imageTitle.setImageResource(R.drawable.image_placeholder);
            }
            if (!TextUtils.isEmpty(dashboardItem.getStatusImageUrl())) {
                Picasso.with(getActivity()).load(dashboardItem.getStatusImageUrl())
                        .error(R.mipmap.processing_icon)
                        .into(holder.imageStatus);
            } else {
                holder.imageStatus.setImageResource(R.mipmap.processing_icon);
            }
            if (dashboardItem.getOrderedTime() != 0) {
                String deliveryTime = Util.format(new Date(dashboardItem.getOrderedTime()), Util.REQUEST_DASHBOARD_DELIVERY_TIME_UI_FORMAT);
                holder.textDateTime.setText(deliveryTime);
            }
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(dashboardItem.getUosId())) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.GENERAL_DASHBOARD_ITEM, dashboardItem);
                    bundle.putInt(AppConstants.BUNDLE_KEYS.REQUEST_FROM, 3);
                    startNextActivity(bundle, RequestDetailsActivity.class);
                }
            }
        });
        return convertView;
    }

    class ClosedRequestHolder {

        ImageView imageTitle, imageStatus;
        TextView textTitle, textDateTime, textStatus, textItems;

        public ClosedRequestHolder(View view) {

            imageTitle = (ImageView) view.findViewById(R.id.iv_row_requests_icon);
            imageStatus = (ImageView) view.findViewById(R.id.iv_status);
            textTitle = (TextView) view.findViewById(R.id.tv_row_title);
            textItems = (TextView) view.findViewById(R.id.tv_items_list);
            textDateTime = (TextView) view.findViewById(R.id.tv_date_time);
            textStatus = (TextView) view.findViewById(R.id.tv_status);
        }
    }


}
