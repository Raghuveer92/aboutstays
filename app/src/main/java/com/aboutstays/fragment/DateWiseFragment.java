package com.aboutstays.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.ExpenseDetailsActivity;
import com.aboutstays.adapter.ExpendableListAdapter;
import com.aboutstays.model.ExpencesModelCategory;
import com.aboutstays.model.ExpencesModelDateWise;
import com.aboutstays.rest.response.DateBasedExpenseItem;
import com.aboutstays.rest.response.ExpenseItem;
import com.aboutstays.rest.response.TimeBasedExpenseItem;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

public class DateWiseFragment extends BaseFragment {

    private ExpendableListAdapter expendableListAdapter;
    private List<ExpencesModelDateWise> expenceDateList = new ArrayList<>();
    private ExpandableListView dateWiseExpandableListView;
    private LinkedHashMap<String, DateBasedExpenseItem> dateVsExpenseItemMap = new LinkedHashMap<>();
    private TextView emptyView;

    public static DateWiseFragment getInstance() {
        DateWiseFragment dateWiseFragment = new DateWiseFragment();
        return dateWiseFragment;
    }

    public void setDateVsExpenseItemMap(LinkedHashMap<String, DateBasedExpenseItem> dateVsExpenseItemMap) {
        this.dateVsExpenseItemMap.clear();
        if(dateVsExpenseItemMap != null){
            this.dateVsExpenseItemMap.putAll(dateVsExpenseItemMap);
        }
    }

    @Override
    public void refreshData() {
        expendableListAdapter.makeDateList();
        expendableListAdapter.notifyDataSetChanged();
        dateWiseExpandableListView.expandGroup(0);
    }

    @Override
    public void initViews() {

        emptyView = (TextView) findView(R.id.tv_empty_view);
        dateWiseExpandableListView = (ExpandableListView) findView(R.id.exlv_date_wise);
        expendableListAdapter = new ExpendableListAdapter(dateVsExpenseItemMap, getActivity());
        dateWiseExpandableListView.setAdapter(expendableListAdapter);
        dateWiseExpandableListView.setEmptyView(emptyView);

        dateWiseExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                List<String> dateList = new ArrayList<>();
                dateList.addAll(dateVsExpenseItemMap.keySet());
                String key = dateList.get(groupPosition);
                DateBasedExpenseItem dateBasedExpenseItem = dateVsExpenseItemMap.get(key);
                List<TimeBasedExpenseItem> listTimeBasedExpenseItem = dateBasedExpenseItem.getListTimeBasedExpenseItem();
                TimeBasedExpenseItem timeBasedExpenseItem = listTimeBasedExpenseItem.get(childPosition);

                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.TIME_BASED_EXPENSE_ITEM, timeBasedExpenseItem);
                startNextActivity(bundle, ExpenseDetailsActivity.class);
                return false;
            }
        });
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_date_wise;
    }


}
