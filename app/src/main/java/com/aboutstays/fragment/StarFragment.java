package com.aboutstays.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.HotelNameActivity;
import com.aboutstays.model.hotels.GeneralInformation;
import com.aboutstays.model.hotels.HotelAddress;
import com.aboutstays.model.hotels.Hotel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.widgets.CustomRatingBar;


public class StarFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    private CustomListAdapter listAdapter;
    private List<Hotel> hotelList = new ArrayList<>();
    private ListView lvHolets;
    private TextView tvError;

    public static StarFragment getInstance(List<Hotel> listHotels) {
        StarFragment starFragment = new StarFragment();
        starFragment.hotelList = listHotels;
        return starFragment;
    }

    @Override
    public void initViews() {

        if (hotelList != null) {
            setDataToView(hotelList);
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_star;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {

        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final Hotel hoteldata = hotelList.get(position);
            int partnershipType = hoteldata.getPartnershipType();
            if (partnershipType == 1){
                holder.ivPartnership.setVisibility(View.VISIBLE);
            } else {
                holder.ivPartnership.setVisibility(View.GONE);
            }
            holder.ratingbar.setVisibility(View.VISIBLE);

            GeneralInformation generalInformation = hoteldata.getGeneralInformation();
            if (generalInformation != null) {
                holder.tvHotelName.setText(generalInformation.getName());
                String logoUrl = generalInformation.getImageUrl();
                if (!TextUtils.isEmpty(logoUrl)) {
                    Picasso.with(getActivity()).load(logoUrl).placeholder(R.drawable.progress_animation).into(holder.ivHotelRoom);
                } else {
                    holder.ivHotelRoom.setImageResource(R.mipmap.cambriasuites);
                }
                HotelAddress address = generalInformation.getAddress();
                if (address != null) {
                    holder.tvHotelAddress.setText(address.getCity());
                }
            }
        holder.ratingbar.setRating(hoteldata.getHotelType());

        return convertView;
    }


    private void setDataToView(List<Hotel> threeStarList) {
        tvError = (TextView) findView(R.id.tv_error);
        lvHolets = (ListView) findView(R.id.listview_3_star);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_3_star, threeStarList, this);
        lvHolets.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
        lvHolets.setEmptyView(tvError);
        lvHolets.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Hotel hotel = hotelList.get(position);
        Bundle b = new Bundle();
        b.putSerializable(AppConstants.BUNDLE_KEYS.HOTEL_DATA, hotel);
        startNextActivity(b, HotelNameActivity.class);
    }

    class Holder {
        CustomRatingBar ratingbar;
        ImageView ivHotelRoom, ivRightSide, ivPartnership;
        TextView tvHotelName, tvHotelAddress;

        public Holder(View view) {
            ratingbar = (CustomRatingBar) view.findViewById(R.id.custom_rating_bar);
            ivHotelRoom = (ImageView) view.findViewById(R.id.iv_hotel_room);
            ivRightSide = (ImageView) view.findViewById(R.id.iv_image_option1);
            ivPartnership = (ImageView) view.findViewById(R.id.iv_parttnership);

            tvHotelName = (TextView) view.findViewById(R.id.tv_hotel_name);
            tvHotelAddress = (TextView) view.findViewById(R.id.tv_hotel_address);
        }
    }
}
