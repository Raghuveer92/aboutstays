package com.aboutstays.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.holder.FeedbackHolder;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.feedbackAndSuggestions.FeedbackAndSuggestionsData;
import com.aboutstays.model.feedbackAndSuggestions.GeneralTimeTakenAndQuality;
import com.aboutstays.rest.request.FeedbackAndSuggestionsRequest;
import com.aboutstays.rest.response.GetStaysFeedbackResponse;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.cunoraz.gifview.library.GifView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by aman on 07/04/17.
 */

public class FeedbackAndSuggestionsFragment extends BaseFragment implements FeedbackHolder.FeedbackListener {

    private EditText etSpecialStaff;
    private EditText etComments;
    private Button btnSubmit;
    private FeedbackAndSuggestionsRequest request = new FeedbackAndSuggestionsRequest();
    private List<GeneralTimeTakenAndQuality> listFeedbacks;
    private boolean alreadySubmittedFeedback;
    private LinearLayout llFeedbacks;

    private List<FeedbackAndSuggestionsRowFragment> rowFragments = new ArrayList<>();
    private FeedbackAndSuggestionsRowFragment overallFeedbackRowFragment;

    private static final int QUALITY_AND_TIME_DEFAULT_VALUE = 3;
    private List<String> listTitleNames;
    private FrameLayout frameLayout;
    private GifView gifView;

    @Override
    public void initViews() {
        initToolBar("Feedback");

        frameLayout = (FrameLayout) findView(R.id.lay_frame_loader);
        gifView = (GifView) findView(R.id.gif);
        etSpecialStaff = (EditText) findView(R.id.et_special_staff);
        etComments = (EditText) findView(R.id.et_comment);
        btnSubmit = (Button) findView(R.id.btn_submit_feedback);
        llFeedbacks = (LinearLayout) findView(R.id.ll_feedbacks);

        listTitleNames = Arrays.asList(getResources().getStringArray(R.array.service_feedback_array));
        addRowFragments();

        Bundle bundle = getArguments();
        if (bundle != null) {
            GeneralServiceModel generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null) {
                if (generalServiceModel.getHotelStaysList() != null) {
                    if (generalServiceModel.getHotelStaysList().getStaysPojo() != null) {
                        request.setStaysId(generalServiceModel.getHotelStaysList().getStaysPojo().getStaysId());
                        request.setHotelId(generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId());
                    }
                    if (generalServiceModel.getGeneralServiceData() != null) {
                        request.setGsId(generalServiceModel.getGeneralServiceData().getId());
                    }
                }

            }
        }

        UserSession session = UserSession.getSessionInstance();
        if (session != null) {
            request.setUserId(session.getUserId());
        }

        getFeedbacksAndSuggestions();
        setOnClickListener(R.id.btn_submit_feedback);
    }

    @Override
    public void showProgressBar() {
        super.showProgressBar();
        frameLayout.setVisibility(View.VISIBLE);
        gifView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        super.hideProgressBar();
        frameLayout.setVisibility(View.GONE);
        gifView.setVisibility(View.GONE);
    }

    private void getFeedbacksAndSuggestions() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getFeedbacksAndSuggestions(request);
        executeTask(AppConstants.TASK_CODES.GET_STAYS_FEEDBACK, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_STAYS_FEEDBACK:
                GetStaysFeedbackResponse feedbackResponse = (GetStaysFeedbackResponse) response;
                if (feedbackResponse != null && !feedbackResponse.getError()) {
                    FeedbackAndSuggestionsData feedbackData = feedbackResponse.getData();

                    if (feedbackData != null) {
                        request.setId(feedbackData.getId());
                        etSpecialStaff.setText(feedbackData.getSpecialStaff());
                        etComments.setText(feedbackData.getComment());
                        if (CollectionUtils.isNotEmpty(feedbackData.getListFeedbacks())) {
                            listFeedbacks = new ArrayList<>();
                            listFeedbacks.addAll(feedbackData.getListFeedbacks());
                            alreadySubmittedFeedback = true;
                            setUserOptedFeedbacks();
                        }
                    }
                }
                break;

            case AppConstants.TASK_CODES.SUBMIT_STAYS_FEEDBACK:
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                if (baseApiResponse != null && !baseApiResponse.getError()) {
                    successDialog();
                } else {
                    showToast(baseApiResponse.getMessage());
                }
                break;
        }
    }

    private void successDialog() {

        SuccessDialogFragment.show(getChildFragmentManager(),
                "FEEDBACK",
                getString(R.string.thank_you),
                getString(R.string.thanks_for_feedback),
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                        getActivity().setResult(Activity.RESULT_OK);
                        getActivity().finish();
                    }
                }
        );

       /* final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_success);
        TextView tvTitle = (TextView) dialog.findViewById(R.id.dialog_title);
        TextView tvSubtitle = (TextView) dialog.findViewById(R.id.dialog_sub_title);
        TextView tvMsg = (TextView) dialog.findViewById(R.id.dialog_msg);
        Button btnOk = (Button) dialog.findViewById(R.id.btn_ok);
        tvTitle.setText(getString(R.string.thank_you));
        tvSubtitle.setVisibility(View.GONE);
        tvMsg.setText(R.string.thanks_for_feedback);
        btnOk.setText(getString(R.string.ok));
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getActivity().finish();
            }
        });
        dialog.show();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);*/
    }

    private void setUserOptedFeedbacks() {
        for (GeneralTimeTakenAndQuality feedback : listFeedbacks) {
            if (feedback == null) {
                continue;
            }
            for (FeedbackAndSuggestionsRowFragment rowFragment : rowFragments) {
                if (feedback.getName().equalsIgnoreCase(rowFragment.getCategoryName())) {
                    rowFragment.setUserSelectedTimeTaken(feedback.getTimeTaken());
                    rowFragment.setUserSelectedQuality(feedback.getQuality());
                    rowFragment.setFeedbackChecked(true);
                    //rowFragment.setQualitySubmitted(feedback.isQualitySubmitted());
                    //rowFragment.setTimeTakenSubmitted(feedback.isTimeTakenSubmitted());
                    //rowFragment.refresh();
                    rowFragment.setUserSelectedQualitySubmitted(feedback.isQualitySubmitted());
                    rowFragment.setUserSelectedTimeTakenSubmitted(feedback.isTimeTakenSubmitted());
                    rowFragment.setSmiley();
                }
            }
        }
    }

    private void addRowFragments() {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        overallFeedbackRowFragment = new FeedbackAndSuggestionsRowFragment();
        overallFeedbackRowFragment.setQuality(QUALITY_AND_TIME_DEFAULT_VALUE);
        overallFeedbackRowFragment.setTimeTaken(QUALITY_AND_TIME_DEFAULT_VALUE);
        overallFeedbackRowFragment.setCategoryName(getString(R.string.service_feedback_overall));
        overallFeedbackRowFragment.setFeedbackListener(this);
        overallFeedbackRowFragment.setEditable(false);
        if (CollectionUtils.isNotEmpty(listFeedbacks)){
            overallFeedbackRowFragment.setAlreadySubmitted(true);
        } else {
            overallFeedbackRowFragment.setAlreadySubmitted(false);
        }
        transaction.add(R.id.ll_feedbacks, overallFeedbackRowFragment);

        for (String item1 : listTitleNames) {
            FeedbackAndSuggestionsRowFragment feedbackAndSuggestionsRowFragment = new FeedbackAndSuggestionsRowFragment();
            feedbackAndSuggestionsRowFragment.setQuality(QUALITY_AND_TIME_DEFAULT_VALUE);
            feedbackAndSuggestionsRowFragment.setQualitySubmitted(false);
            feedbackAndSuggestionsRowFragment.setTimeTaken(QUALITY_AND_TIME_DEFAULT_VALUE);
            feedbackAndSuggestionsRowFragment.setTimeTakenSubmitted(false);
            feedbackAndSuggestionsRowFragment.setCategoryName(item1);
            feedbackAndSuggestionsRowFragment.setFeedbackListener(this);
            feedbackAndSuggestionsRowFragment.setEditable(true);
            if (CollectionUtils.isNotEmpty(listFeedbacks)){
                feedbackAndSuggestionsRowFragment.setAlreadySubmitted(true);
            } else {
                feedbackAndSuggestionsRowFragment.setAlreadySubmitted(false);
            }
            rowFragments.add(feedbackAndSuggestionsRowFragment);
            transaction.add(R.id.ll_feedbacks, feedbackAndSuggestionsRowFragment);
        }
        transaction.commit();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit_feedback:
                validateAndSubmitFeedback();
                break;
        }
    }

    private void validateAndSubmitFeedback() {
        List<GeneralTimeTakenAndQuality> listSubmitFeedback = new ArrayList<>();

        for (FeedbackAndSuggestionsRowFragment rowFragment : rowFragments) {
            if (!rowFragment.isChecked()) {
                GeneralTimeTakenAndQuality generalTimeTakenAndQuality = new GeneralTimeTakenAndQuality();
                generalTimeTakenAndQuality.setName(rowFragment.getCategoryName());
                generalTimeTakenAndQuality.setTimeTaken(rowFragment.getUserSelectedTimeTaken());
                generalTimeTakenAndQuality.setQuality(rowFragment.getUserSelectedQuality());
                generalTimeTakenAndQuality.setQualitySubmitted(true);
                generalTimeTakenAndQuality.setTimeTakenSubmitted(true);
                listSubmitFeedback.add(generalTimeTakenAndQuality);
            }
        }
        request.setListFeedbacks(listSubmitFeedback);
        if (!TextUtils.isEmpty(etSpecialStaff.getText().toString())) {
            request.setSpecialStaff(etSpecialStaff.getText().toString().trim());
        }
        if (!TextUtils.isEmpty(etComments.getText().toString())) {
            request.setComment(etComments.getText().toString().trim());
        }

//        if (CollectionUtils.isEmpty(request.getListFeedbacks())) {
//            showToast("Can not submit feedbacks");
//            return;
//        }
        submitFeedbacks();
    }

    private void submitFeedbacks() {
        HttpParamObject httpParamObject = ApiRequestGenerator.submitStaysFeedback(request);
        executeTask(AppConstants.TASK_CODES.SUBMIT_STAYS_FEEDBACK, httpParamObject);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_feedback_and_suggestions;
    }


    @Override
    public void onProgressChanged() {
        int qualitySum = 0, timeTakenSum = 0;
        for (FeedbackAndSuggestionsRowFragment rowFragment : rowFragments) {
            qualitySum += rowFragment.getUserSelectedQuality();
            timeTakenSum += rowFragment.getUserSelectedTimeTaken();
        }

        int overallQualityRating = qualitySum / (rowFragments.size());
        int overallTimeTakenRating = timeTakenSum / (rowFragments.size());

        float floatQuality = (float) qualitySum / ((float) rowFragments.size());
        float decimalValue = floatQuality % 1;

        if (decimalValue >= 0.5) {
            overallQualityRating++;
        }

        float floatTimeTaken = (float) timeTakenSum / (float) rowFragments.size();
        float decimalTimeTaken = floatTimeTaken % 1;

        if (decimalTimeTaken >= 0.5) {
            overallTimeTakenRating++;
        }

        overallFeedbackRowFragment.setUserSelectedQuality(overallQualityRating);
        overallFeedbackRowFragment.setUserSelectedTimeTaken(overallTimeTakenRating);
        overallFeedbackRowFragment.setUserSelectedQualitySubmitted(true);
        overallFeedbackRowFragment.setUserSelectedTimeTakenSubmitted(true);
        overallFeedbackRowFragment.setFeedbackChecked(true);
        overallFeedbackRowFragment.setSmiley();
    }

    @Override
    public void onChangeCheck() {
        overallFeedbackRowFragment.updateFeedback();
    }

}
