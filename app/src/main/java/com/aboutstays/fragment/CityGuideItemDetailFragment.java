package com.aboutstays.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.AboutstaysApplication;
import com.aboutstays.R;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.activity.SereneSpaActivity;
import com.aboutstays.rest.response.CityGuideGeneralInfo;
import com.aboutstays.rest.response.CityGuideItemData;
import com.aboutstays.utils.FusedLocationService;
import com.google.android.gms.common.ConnectionResult;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.rest.response.pojo.Address;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;


public class CityGuideItemDetailFragment extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface, ViewPager.OnPageChangeListener {

    private TextView tvOpnHours, tvDestinationType, tvDistance, tvAddress, tvDescription, tvGetDirection;
    private ImageView ivMainPic;
    private String titleName;
    private LinearLayout layAminity, laySubImages, llTravellOption;
    private ViewPager viewPager;
    private FusedLocationService.MyLocation location;
    private FusedLocationService fusedLocationService;
    private CustomPagerAdapter pagerAdapter;
    private List<ImageContainerFragment> listImageContainerFragment = new ArrayList<>();
    private ImageView[] dots;
    private LinearLayout pager_indicator;
    private int count;
    private Double myLatitude = 0.0, myLongitude = 0.0, locationLat, locationLon;
    private String latitude, longitude;

    @Override
    public void initViews() {
        findViews();

        checkLocationPermission();

        Bundle bundle = getArguments();
        if (bundle != null) {
            CityGuideItemData cityGuideItemData = (CityGuideItemData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.CITY_GUIDE_ITEM_OBJECT);
            if (cityGuideItemData != null) {
                titleName = cityGuideItemData.getGeneralInfo().getName();
                setData(cityGuideItemData);
            }
        }

        if (!TextUtils.isEmpty(titleName)) {
            initToolBar(titleName);
        }

        setOnClickListener(R.id.tv_get_direction);
    }

    private void checkLocationPermission() {
        if (AboutstaysApplication.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            getLocation();
        } else {
            new TedPermission(getActivity()).setPermissionListener(new PermissionListener() {
                @Override
                public void onPermissionGranted() {
                    getLocation();
                }

                @Override
                public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                }
            }).setPermissions(Manifest.permission.ACCESS_FINE_LOCATION).check();
        }
    }

    private void getLocation() {
        fusedLocationService = new FusedLocationListener(getActivity());
        fusedLocationService.connect();
    }

    class FusedLocationListener extends FusedLocationService {
        public FusedLocationListener(Activity mContext) {
            super(mContext);
        }

        @Override
        protected void onSuccess(MyLocation mLastKnownLocation) {
            CityGuideItemDetailFragment.this.location = mLastKnownLocation;
        }

        @Override
        protected void onFailed(ConnectionResult connectionResult) {

        }
    }

    private void setData(CityGuideItemData cityGuideItemData) {
        List<String> headerImageList = new ArrayList<>();

        CityGuideGeneralInfo generalInfo = cityGuideItemData.getGeneralInfo();
        if(generalInfo != null) {
            String imageUrl = cityGuideItemData.getGeneralInfo().getImageUrl();
            if(!TextUtils.isEmpty(imageUrl)) {
                headerImageList.add(imageUrl);
            }
            if(CollectionUtils.isNotEmpty(generalInfo.getAdditionalImageUrls())) {
                headerImageList.addAll(cityGuideItemData.getGeneralInfo().getAdditionalImageUrls());
            }
            if(CollectionUtils.isNotEmpty(headerImageList)) {
                for(String amenityImage : headerImageList) {
                    ImageContainerFragment imageContainerFragment = ImageContainerFragment.getInstance(amenityImage);
                    listImageContainerFragment.add(imageContainerFragment);
                }

                pagerAdapter = new CustomPagerAdapter(getActivity().getSupportFragmentManager(), listImageContainerFragment, this);
                viewPager.setAdapter(pagerAdapter);
                viewPager.setCurrentItem(0);
                viewPager.setOnPageChangeListener(this);
                setUiPageViewController();

                Address address = cityGuideItemData.getGeneralInfo().getAddress();
                if (address != null) {
                    latitude = address.getLatitude();
                    longitude = address.getLongitude();

                    try {
                        locationLat = Double.parseDouble(latitude.toString());
                        locationLon = Double.parseDouble(longitude.toString());
                    } catch (NumberFormatException e){
                        locationLat = 0.0;
                        locationLon = 0.0;
                    }

                    tvAddress.setText(address.getFullAddtess());
                    latitude = address.getLatitude();
                    longitude = address.getLongitude();
                }

                if (location != null) {
                    myLatitude = location.getLatitude();
                    myLongitude = location.getLongitude();
                }
                tvDistance.setVisibility(View.VISIBLE);
                if (myLatitude != 0 && myLongitude != 0 && locationLat != 0 && locationLon != 0) {
                    double distance = cityGuideItemData.distance(myLatitude, myLongitude, locationLat, locationLon);
                    if (distance > 0){
                        double round = cityGuideItemData.round(distance, 2);
                        tvDistance.setText(round + " KM");
                    }
                } else {
                    tvDistance.setText(R.string.n_a);
                }
            }
        }

        String openTime = Util.convertDateFormat(cityGuideItemData.getOpeningTime(),
                Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
        if (!TextUtils.isEmpty(openTime)) {
            tvOpnHours.setText(openTime);
        }
        tvDestinationType.setText(cityGuideItemData.getDestinationType());
//        tvDistance.setText(cityGuideItemData.getDistance() + " KM");
        tvDescription.setText(cityGuideItemData.getDescription());

        List<String> activityList = cityGuideItemData.getActivityList();
        if (!CollectionUtils.isEmpty(activityList)) {
            for (int i = 0; i < activityList.size(); i++) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View view = inflater.inflate(R.layout.row_aminites, layAminity, false);
                TextView tvAminity = (TextView) view.findViewById(R.id.tv_aminity);
                tvAminity.setText(activityList.get(i));
                layAminity.addView(view);
            }
        }

        List<String> travelOptionsList = cityGuideItemData.getTravelOptionsList();
        if (CollectionUtils.isNotEmpty(travelOptionsList)){
            for (int i = 0; i < travelOptionsList.size(); i++) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View view = inflater.inflate(R.layout.row_travel_icon, llTravellOption, false);
                ImageView imageView = (ImageView) view.findViewById(R.id.iv_option1);
                Picasso.with(getActivity()).load(travelOptionsList.get(i)).into(imageView);
                llTravellOption.addView(view);
            }
        }

//        CityGuideGeneralInfo generalInfo = cityGuideItemData.getGeneralInfo();
//        if (generalInfo != null) {
//            additionalImageUrls = generalInfo.getAdditionalImageUrls();
//            if (!CollectionUtils.isEmpty(additionalImageUrls)) {
//                int size = additionalImageUrls.size();
//                for (int i = 0; i < size; i++) {
//                    addImagesToLayout(additionalImageUrls.get(i), false, 0);
//                }
////                    addImagesToLayout(additionalImageUrls.get(3), true, size - 4);
////                } else {
////                    for (int i = 0; i < size; i++) {
////                        addImagesToLayout(additionalImageUrls.get(i), false, 0);
////                    }
////                }
//            }
//        }
    }

    private void setUiPageViewController() {
        count = pagerAdapter.getCount();
        if (count > 1) {
            dots = new ImageView[count];

            for (int i = 0; i < count; i++) {
                dots[i] = new ImageView(getActivity());
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot_not));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                params.setMargins(4, 0, 4, 0);

                pager_indicator.addView(dots[i], params);
            }

            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
        }
    }

//    private void addImagesToLayout(String s, boolean isMore, int moreSize) {
//        LayoutInflater inflater = LayoutInflater.from(getActivity());
//        View view = inflater.inflate(R.layout.view_serene_spa_images, laySubImages, false);
//        ImageView iv = (ImageView) view.findViewById(R.id.iv_hotel_room);
//        RelativeLayout rlMoreImage = (RelativeLayout) view.findViewById(R.id.rl_image_shade);
//        TextView tvMore = (TextView) view.findViewById(R.id.tv_more_number);
//        if (isMore == true) {
//            rlMoreImage.setVisibility(View.VISIBLE);
//            tvMore.setText("+" + moreSize + " More");
//        }
////        Picasso.with(getActivity()).load(s).placeholder(R.mipmap.aboutstays_icon).into(iv);
////        view.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                Bundle bundle = new Bundle();
////                bundle.putSerializable(AppConstants.BUNDLE_KEYS.SUB_IMAGES_LIST, (Serializable) additionalImageUrls);
////                FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.SUB_IMAGE_FRAGMENT, bundle);
////            }
////        });
//        laySubImages.addView(view);
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_get_direction:
                if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(longitude)) {
                    String strUri = "http://maps.google.com/maps?q=loc:" + latitude + "," + longitude;
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                }
                break;
        }
    }

    private void findViews() {
        tvOpnHours = (TextView) findView(R.id.tv_opening_hour);
        tvDestinationType = (TextView) findView(R.id.tv_destination_type);
        tvDistance = (TextView) findView(R.id.tv_distance);
        tvAddress = (TextView) findView(R.id.tv_address);
        tvDescription = (TextView) findView(R.id.tv_description);
        //ivMainPic = (ImageView) findView(R.id.iv_hotel_room);
        layAminity = (LinearLayout) findView(R.id.lay_aminites);
        tvGetDirection = (TextView) findView(R.id.tv_get_direction);
        pager_indicator = (LinearLayout) findView(R.id.viewPagerCountDots);

        //laySubImages = (LinearLayout) findView(R.id.lay_sub_images);
        llTravellOption = (LinearLayout) findView(R.id.ll_travel_option);
        viewPager = (ViewPager) findView(R.id.vp_image);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_city_guide_item_detail;
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        return listImageContainerFragment.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return null;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < count; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot_not));
        }
        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
