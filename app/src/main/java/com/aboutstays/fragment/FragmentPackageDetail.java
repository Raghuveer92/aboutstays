package com.aboutstays.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.reservation.ReservationByTypeData;
import com.aboutstays.model.reservation.ReservationTypeData;
import com.squareup.picasso.Picasso;

import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by neeraj on 4/4/17.
 */

public class FragmentPackageDetail extends BaseFragment {

    private ImageView ivMainImage;
    private TextView title, time, description;
    private Button btnSubmit;
    private String name;
    private FrameLayout shadowFrame;
    private GeneralServiceModel generalServiceModel;
    private ReservationTypeData reservationByTypeData;
    private int rType;

    @Override
    public void initViews() {

        ivMainImage = (ImageView) findView(R.id.iv_hotel_room);
        title = (TextView) findView(R.id.tv_title);
        time = (TextView) findView(R.id.tv_time);
        description = (TextView) findView(R.id.tv_description);
        btnSubmit = (Button) findView(R.id.btn_reserve);
        shadowFrame = (FrameLayout) findView(R.id.fl_image);
        shadowFrame.setVisibility(View.GONE);

        Bundle bundle = getArguments();
        if (bundle != null) {
            rType = bundle.getInt(AppConstants.BUNDLE_KEYS.RESERVATION_TYPE_CODE);
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            reservationByTypeData = (ReservationTypeData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.RESERVATION_TYPES);
            if (reservationByTypeData != null) {
                setData(reservationByTypeData);
                name = reservationByTypeData.getName();
            }
        }
        if (!TextUtils.isEmpty(reservationByTypeData.getName())) {
            initToolBar(reservationByTypeData.getName());
        } else {
            initToolBar("");
        }

        setOnClickListener(R.id.btn_reserve);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_reserve:
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.RESERVATION_TYPES, reservationByTypeData);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                bundle.putInt(AppConstants.BUNDLE_KEYS.RESERVATION_TYPE_CODE, rType);
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.RESERVATION_ORDER_FRAGMENT, bundle, AppConstants.REQUEST_CODE.ORDER_PLACED);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppConstants.RESULT_CODE.RESULT_DONE) {
            getActivity().setResult(AppConstants.RESULT_CODE.RESULT_DONE);
            getActivity().finish();
        }
    }

    private void setData(ReservationTypeData reservationByTypeData) {
        String imageUrl = reservationByTypeData.getImageUrl();
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(getActivity()).load(imageUrl).placeholder(R.drawable.progress_animation).into(ivMainImage);
        }
        title.setText(reservationByTypeData.getName());
        if (reservationByTypeData.getAmountApplicable() == true) {
            time.setText("\u20B9 " + reservationByTypeData.getPrice());
        } else if (reservationByTypeData.getDurationBased() == true) {
            time.setText("Duration: " + reservationByTypeData.getDuration() + "");
        } else if (reservationByTypeData.getAmountApplicable() == true && reservationByTypeData.getDurationBased() == true) {
            time.setText("Duration: " + reservationByTypeData.getDuration() + " | " + "\u20B9 " + reservationByTypeData.getPrice());
        } else {
            time.setText("");
        }
//        if(reservationByTypeData.isAmountApplicable()) {
//            time.setText(reservationByTypeData.getDurationAndPriceString());
//        } else {
//            time.setText(reservationByTypeData.getShortDescription());
//        }
        description.setText(reservationByTypeData.getDescripton());
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_package_detail;
    }
}
