package com.aboutstays.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.maintenance.ListMaintenanceItemType;
import com.aboutstays.model.maintenance.MaintenanceData;
import com.aboutstays.model.maintenance.MaintenanceDataResponce;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by neeraj on 1/4/17.
 */

public class MaintenanceFragment extends BaseFragment implements CustomListAdapterInterface {
    private ListView lvMaintenance;
    private CustomListAdapter listAdapter;
    private String hsId;
    private TextView tvEmptyView;
    List<ListMaintenanceItemType> listMaintenanceData = new ArrayList<>();
    private GeneralServiceModel generalServiceModel;

    @Override
    public void initViews() {
        initToolBar(getString(R.string.maintenance));

        Bundle bundle = getArguments();
        if(bundle != null){
            generalServiceModel = (GeneralServiceModel)bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if(generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null){
                hsId = generalServiceModel.getGeneralServiceData().getHsId();
            }
        }

        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        lvMaintenance = (ListView) findView(R.id.lv_maintenance);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_room_service, listMaintenanceData, this);
        lvMaintenance.setAdapter(listAdapter);
        lvMaintenance.setEmptyView(tvEmptyView);
        listAdapter.notifyDataSetChanged();
        getMaintenanceData();
    }

    private void getMaintenanceData() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getMaintenance(hsId);
        executeTask(AppConstants.TASK_CODES.MAINTENANCE_DATA, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.MAINTENANCE_DATA:
                MaintenanceDataResponce dataResponce = (MaintenanceDataResponce) response;
                if (dataResponce != null && dataResponce.getError() == false){
                    MaintenanceData maintenanceData = dataResponce.getData();
                    if (maintenanceData != null){
                        List<ListMaintenanceItemType> listMaintenanceItemType = maintenanceData.getListMaintenanceItemType();
                        if (CollectionUtils.isNotEmpty(listMaintenanceItemType)){
                            listMaintenanceData.addAll(listMaintenanceItemType);
                            listAdapter.notifyDataSetChanged();
                        }
                    }
                }
                break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_maintenance;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }


        final ListMaintenanceItemType itemType = listMaintenanceData.get(position);
        holder.title.setText(itemType.getTypeName());
        String imageUrl = itemType.getImageUrl();
        if (!TextUtils.isEmpty(imageUrl)){
            Picasso.with(getActivity()).load(imageUrl).placeholder(R.drawable.progress_animation).into(holder.titleImage);
        }
        holder.tvTime.setVisibility(View.GONE);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.MAINTENANCE_ITEM, itemType);
                FragmentContainerActivity.startActivityForResult(getActivity(), AppConstants.FRAGMENT_TYPE.MAINTENANCE_ITEM_FRAGMENT, bundle, AppConstants.REQUEST_CODE.ORDER_PLACED);
            }
        });

        return convertView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppConstants.RESULT_CODE.RESULT_DONE){
            getActivity().setResult(AppConstants.RESULT_CODE.RESULT_DONE);
            getActivity().finish();
        }
    }

    class Holder {
        TextView title, tvTime, tvItemCount;
        ImageView titleImage, arrowImage;
        RelativeLayout rlCount;

        public Holder(View view) {
            title = (TextView) view.findViewById(R.id.tv_title);
            titleImage = (ImageView) view.findViewById(R.id.iv_title_image1);
            arrowImage = (ImageView) view.findViewById(R.id.iv_right_angle);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_count);
            tvItemCount = (TextView) view.findViewById(R.id.tv_item_count);
        }
    }
}
