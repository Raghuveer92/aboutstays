
package com.aboutstays.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.dialog.ReservationDialogFragment;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.internet.InternetPackList;
import com.aboutstays.model.reservation.ReservationByTypeData;
import com.aboutstays.model.reservation.ReservationTypeData;
import com.aboutstays.rest.request.ReservationRequestOrder;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.aboutstays.utitlity.DatePickerUtil;
import com.aboutstays.utitlity.TimePickerUtil;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import simplifii.framework.ListAdapters.CustomExpandableListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.receivers.CartUpdateReceiver;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by neeraj on 4/4/17.
 */

public class ReservationOrderFragment extends BaseFragment implements CustomExpandableListAdapter.ExpandableListener {

    private TextView tvRequestNow;
    private TextView tvDeliveryDate;
    private Button initiateBooking;
    private String gsId;
    private String hotelId;
    private String staysId;
    private String userId;
    private long pickupTime;
    private boolean reqNow = false;
    private String name;
    private String id;
    private double price;
    private long duration;
    public static final String RESERVATION_DATE = "dd MMM";
    public static final String RESERVATION_DATE_AND_TIME_FORMAT = "MMM dd, hh:mm a";
    public String datePickerDate;
    private Calendar calendarInstance;
    private int itemCount = 1;

    private List<ReservationTypeData> listReservationItem = new ArrayList<>();
    private ExpandableListView expandableListView;
    private CustomExpandableListAdapter customExpandableListAdapter;
    private ReservationTypeData reservationByTypeData;
    private int reservationType;
    private String deliveryDate;
    private TextView tvTotalPrice;
    private TextView tvTotalCount;
    private long checkOutDate;


    @Override
    public void initViews() {
        calendarInstance = Calendar.getInstance();
        Bundle bundle = getArguments();
        if (bundle != null) {
            reservationType = bundle.getInt(AppConstants.BUNDLE_KEYS.RESERVATION_TYPE_CODE);
            GeneralServiceModel generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null) {
                gsId = generalServiceModel.getGeneralServiceData().getId();
                if (generalServiceModel.getHotelStaysList().getStaysPojo() != null) {
                    hotelId = generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId();
                    staysId = generalServiceModel.getHotelStaysList().getStaysPojo().getStaysId();
                    userId = generalServiceModel.getHotelStaysList().getStaysPojo().getUserId();
                }
            }

            reservationByTypeData = (ReservationTypeData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.RESERVATION_TYPES);
            if (reservationByTypeData != null) {
                name = reservationByTypeData.getName();
                id = reservationByTypeData.getId();
                price = reservationByTypeData.getPrice();
                duration = reservationByTypeData.getDuration();
                listReservationItem.add(reservationByTypeData);
//                reservationType = reservationByTypeData.getReservationType();
            }
        }

        checkOutDate = Preferences.getData(AppConstants.PREF_KEYS.CHECKOUT_DATE_TIME, 0l);

        if (!TextUtils.isEmpty(name)) {
            initToolBar(name);
        } else {
            initToolBar("");
        }


        initiateBooking = (Button) findView(R.id.btn_place_order);
        initiateBooking.setVisibility(View.VISIBLE);
        initiateBooking.setText(getString(R.string.initiate_booking));

        expandableListView = (ExpandableListView) findView(R.id.lv_internet_reservation);
        setOnClickListener(R.id.btn_place_order);
        expandableListView.setEmptyView(findView(R.id.tv_empty_view));
        customExpandableListAdapter = new CustomExpandableListAdapter(getActivity(), R.layout.row_header_expandable, R.layout.row_order_detail, listReservationItem, this);
        setHeader();
        setFooter();
        expandableListView.setAdapter(customExpandableListAdapter);
        expandableListView.expandGroup(0);
    }

    private void setHeader() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View inflate = inflater.inflate(R.layout.view_order_details_header, expandableListView, false);

        TextView tvQuantity = (TextView) inflate.findViewById(R.id.tv_quantity);
        TextView tvPrice = (TextView) inflate.findViewById(R.id.tv_rate);
        if (reservationType == 1) {
            tvQuantity.setVisibility(View.INVISIBLE);
            tvPrice.setText("Pax");
        } else {
            tvQuantity.setVisibility(View.VISIBLE);
            setText(R.id.tv_quantity, "Pax", inflate);
        }
        expandableListView.addHeaderView(inflate);
    }

    private void setFooter() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View inflate = inflater.inflate(R.layout.view_order_details_footer, expandableListView, false);
        tvTotalPrice = (TextView) inflate.findViewById(R.id.tv_total_price);
        tvTotalCount = (TextView) inflate.findViewById(R.id.tv_total_count);
        tvRequestNow = (TextView) inflate.findViewById(R.id.tv_add);
        TextView tvTitle = (TextView) inflate.findViewById(R.id.tv_title);
        RelativeLayout rlSpecialComment = (RelativeLayout) inflate.findViewById(R.id.rl_comment);

        tvDeliveryDate = (TextView) inflate.findViewById(R.id.tv_date_time);

        RelativeLayout rlDatePick = (RelativeLayout) inflate.findViewById(R.id.rl_datetime);
        TextView tvMsg = (TextView) inflate.findViewById(R.id.tv_msg);
        View view = (View) inflate.findViewById(R.id.view_speial_request_bottom);

        view.setVisibility(View.GONE);

        tvMsg.setText(R.string.reservation_bottom_msg);
        rlSpecialComment.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.preferred_dateTime));

        setCountAndPrice();

        rlDatePick.setOnClickListener(this);
        tvRequestNow.setOnClickListener(this);
        expandableListView.addFooterView(inflate);
    }

    private void setCountAndPrice() {
        if (reservationByTypeData != null) {
            if (reservationType == 1) {
                tvTotalCount.setVisibility(View.INVISIBLE);
                tvTotalPrice.setText(itemCount + "");
            } else {
                tvTotalCount.setVisibility(View.VISIBLE);
                if(reservationByTypeData.getAmountApplicable()== true) {
                    tvTotalPrice.setText("\u20B9 " + reservationByTypeData.getPrice()*itemCount);
                } else {
                    tvTotalPrice.setText("0");
                }
                tvTotalCount.setText(itemCount + "");
            }

        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_datetime:
                setReqNowFalse();
                setDateandTime();
                break;
            case R.id.tv_add:
                if (reqNow == false) {
                    reqNow = true;
                    tvRequestNow.setBackgroundResource(R.drawable.shape_button_selected);
                    tvRequestNow.setTextColor(Color.WHITE);
                    tvDeliveryDate.setText("");
//                    tvRequestNow.setText(R.string.requested);
                } else {
                    setReqNowFalse();
                }
                break;
            case R.id.btn_place_order:
                boolean ifBeforeCheckout = Util.ifGreaterThnCheckOut(pickupTime, checkOutDate);
                if (ifBeforeCheckout) {
                    validateAndFillData();
                } else {
                    errorDialoge(getString(R.string.select_date_error));
                    return;
                }

                break;
        }
    }

    private void errorDialoge(String string) {
        SuccessDialogFragment.show(getChildFragmentManager(),
                "AMENITY RESERVATION",
                getString(R.string.order_error),
                string,
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                    }
                }
        );
    }

    private void validateAndFillData() {
        ReservationRequestOrder reservationRequestOrder = new ReservationRequestOrder();
        reservationRequestOrder.setHotelId(hotelId);
        reservationRequestOrder.setGsId(gsId);
        reservationRequestOrder.setStaysId(staysId);
        reservationRequestOrder.setUserId(userId);
        reservationRequestOrder.setPax(itemCount);
        if (reservationByTypeData != null) {
            reservationRequestOrder.setRsItemId(id);
            reservationRequestOrder.setPrice(price * itemCount);
            reservationRequestOrder.setDuration(duration);
            reservationRequestOrder.setName(name);
        }
//        String comment = etComment.getText().toString().trim();
//        if (!TextUtils.isEmpty(comment)) {
//            reservationRequestOrder.setComment(comment);
//        }
//        if (pickupTime > 0 && reqNow == false) {
//            reservationRequestOrder.setDeliveryTime(pickupTime);
//        } else {
//            showToast("Please select a time first");
//            return;
//        }
        if (reqNow == true) {
            reservationRequestOrder.setRequestedNow(true);
        } else if (pickupTime > 0) {
            reservationRequestOrder.setRequestedNow(false);
            reservationRequestOrder.setDeliveryTime(pickupTime);
        } else {
            showToast(getString(R.string.please_select_time_first));
            return;
        }
        if (reqNow == false && TextUtils.isEmpty(deliveryDate)) {
            showToast(getString(R.string.please_select_time_first));
            return;
        } else {
            reservationRequestOrder.setDeliverTimeString(deliveryDate);
        }
        requestReserve(reservationRequestOrder);
    }


    private void requestReserve(ReservationRequestOrder reservationRequestOrder) {
        HttpParamObject httpParamObject = ApiRequestGenerator.requestHealthBeautyReserve(reservationRequestOrder, false);
        executeTask(AppConstants.TASK_CODES.RESERVATION_REQUEST, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.RESERVATION_REQUEST:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false) {
                    showSuccessDialog();
                    CartUpdateReceiver.sendBroadcast(getActivity(), CartUpdateReceiver.ACTION_UPDATE_REQUEST);
                }
                break;
        }
    }

    private void showSuccessDialog() {
        ReservationDialogFragment.show(getChildFragmentManager(), "AMENITY RESERVATION", "REQUEST INITIATED", itemCount, reservationByTypeData, new ReservationDialogFragment.OnConfirmListener() {
            @Override
            public void confirm() {
                getActivity().setResult(AppConstants.RESULT_CODE.RESULT_DONE);
                getActivity().finish();
            }
        });
    }

    private void selectCurrentDateandTime() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time =&gt; " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat(RESERVATION_DATE_AND_TIME_FORMAT);
        String formattedDate = df.format(c.getTime());
        tvDeliveryDate.setText(formattedDate);
        pickupTime = c.getTimeInMillis() - 1000;
        Date time = c.getTime();
        deliveryDate = Util.format(time, Util.DELIVERY_TIME_DATE_PATTERN);
    }

    private void setReqNowFalse() {
        reqNow = false;
        tvRequestNow.setBackgroundResource(R.drawable.shape_button_email);
        tvRequestNow.setTextColor(getResourceColor(R.color.color_red_bg));
        tvRequestNow.setText(R.string.request_now);
        tvDeliveryDate.setText("");
        pickupTime = 0;
        deliveryDate = "";
    }

    private void setDateandTime() {
        final AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        DatePickerUtil datePickerUtil = new DatePickerUtil(RESERVATION_DATE, calendarInstance, appCompatActivity.getFragmentManager(), getString(R.string.select_date), true, false, new DatePickerUtil.OnDateSelected() {
            @Override
            public void onDateSelected(Calendar calendar, String date) {
                TimePickerUtil timePickerUtil = new TimePickerUtil(RESERVATION_DATE_AND_TIME_FORMAT, calendar, appCompatActivity.getFragmentManager(), getString(R.string.select_time), new TimePickerUtil.OnTimeSelected() {
                    @Override
                    public void onTimeSelected(Calendar calendar, String date) {
                        calendarInstance = calendar;
                        pickupTime = calendar.getTimeInMillis() - 1000;
                        tvDeliveryDate.setText(date);
                        Date time = calendar.getTime();
                        deliveryDate = Util.format(time, Util.DELIVERY_TIME_DATE_PATTERN);
//                        deliveryDate = Util.convertDateFormat(calendar.getTime(), Util.DELIVERY_TIME_DATE_PATTERN);
                    }
                });
                timePickerUtil.show();
            }

            @Override
            public void onCanceled() {
//                tvDeliveryDate.setText("");
            }
        });
        datePickerUtil.show();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_internet_reservation_order;
    }

    @Override
    public int getChildSize(int parentPosition) {
        return listReservationItem.size();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        convertView = inflater.inflate(resourceId, parent, false);
        setText(R.id.tv_men_women, listReservationItem.get(groupPosition).getSubCategory(),  convertView);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.iv_indicator);
        if (isExpanded) {
            imageView.setImageResource(R.mipmap.up_arrow_white);
        } else {
            imageView.setImageResource(R.mipmap.arrow_down_white);
        }
        imageView.setColorFilter(Color.WHITE);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        convertView = inflater.inflate(resourceId, parent, false);
        ReservationTypeData reservationByTypeData = listReservationItem.get(groupPosition);

        TextView tvCount = (TextView) convertView.findViewById(R.id.tv_item_count);
        TextView tvMinus = (TextView) convertView.findViewById(R.id.tv_minus);
        TextView tvPlus = (TextView) convertView.findViewById(R.id.tv_plus);
        TextView tvPrice = (TextView) convertView.findViewById(R.id.tv_item_price);
        TextView tvDel = (TextView) convertView.findViewById(R.id.tv_del);
        TextView tvPaxNum = (TextView) convertView.findViewById(R.id.tv_count);
        TextView tvAdd = (TextView) convertView.findViewById(R.id.tv_add);
        RelativeLayout rlPaxCounnt = (RelativeLayout) convertView.findViewById(R.id.rl_pax_count);

        if (reservationByTypeData != null) {
            setText(R.id.tv_item, reservationByTypeData.getName(), convertView);
            if (reservationType == 1) {
                tvPrice.setVisibility(View.GONE);
                rlPaxCounnt.setVisibility(View.VISIBLE);
                tvAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        itemCount = itemCount + 1;
                        customExpandableListAdapter.notifyDataSetChanged();
                        setCountAndPrice();
                    }
                });

                tvDel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (itemCount >= 2) {
                            itemCount = itemCount - 1;
                            customExpandableListAdapter.notifyDataSetChanged();
                            setCountAndPrice();
                        }
                    }
                });
                tvPaxNum.setText(itemCount + "");
                tvCount.setVisibility(View.INVISIBLE);
            } else {
                tvMinus.setVisibility(View.VISIBLE);
                tvPlus.setVisibility(View.VISIBLE);
                tvPlus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        itemCount = itemCount + 1;
                        customExpandableListAdapter.notifyDataSetChanged();
                        setCountAndPrice();
                    }
                });

                tvMinus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (itemCount >= 2) {
                            itemCount = itemCount - 1;
                            customExpandableListAdapter.notifyDataSetChanged();
                            setCountAndPrice();
                        }
                    }
                });

                tvCount.setVisibility(View.VISIBLE);
                tvCount.setText(itemCount + "");
                if(reservationByTypeData.getAmountApplicable() == true)
                    setText(R.id.tv_item_price,"\u20B9 "+ reservationByTypeData.getPrice()*itemCount + "", convertView);
                else
                    setText(R.id.tv_item_price, "0", convertView);
//            }
/*            if(reservationByTypeData.isAmountApplicable())
                setText(R.id.tv_item_price, "\u20B9 "+reservationByTypeData.getPrice(), convertView);
            else
                setText(R.id.tv_item_price, "0", convertView);*/
            }
        }
            return convertView;
        }
}

