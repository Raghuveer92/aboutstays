package com.aboutstays.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.commute.CommuteData;
import com.aboutstays.model.commute.CommuteResponceApi;
import com.aboutstays.model.commute.ListCommutePackage;
import com.aboutstays.rest.response.CarDetailsAndPrice;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by neeraj on 31/3/17.
 */

public class CommuteFragment extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface {

    List<String> tabs = new ArrayList<>();
    private ViewPager viewPagerCommute;
    private TabLayout tabCommute;
    private TextView tvEmptyView;
    private CustomPagerAdapter pagerAdapter;
    private List<CommuteItemFragment> commuteListFragment = new ArrayList<>();
    private String hsId;
    private GeneralServiceModel generalServiceModel;

    @Override
    public void initViews() {

        initToolBar("Commute");
        Bundle bundle = getArguments();
        if (bundle != null) {
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null) {
                if (generalServiceModel.getGeneralServiceData() != null) {
                    hsId = generalServiceModel.getGeneralServiceData().getHsId();
                }
            }
        }

        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        tvEmptyView.setVisibility(View.GONE);
        viewPagerCommute = (ViewPager) findView(R.id.viewPager_commute);
        tabCommute = (TabLayout) findView(R.id.tablayout_commute);
        setAdapter();
        pagerAdapter.notifyDataSetChanged();
    }

    private void setAdapter() {
        pagerAdapter = new CustomPagerAdapter(getChildFragmentManager(), tabs, this);
        viewPagerCommute.setAdapter(pagerAdapter);
        tabCommute.setupWithViewPager(viewPagerCommute);
        getAllCommute();
    }


    private void getAllCommute() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getCommuteData(hsId);
        executeTask(AppConstants.TASK_CODES.GET_COMMUTE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_COMMUTE:
                CommuteResponceApi responceApi = (CommuteResponceApi) response;
                if (responceApi != null && responceApi.getError() == false) {
                    tvEmptyView.setVisibility(View.GONE);
                    CommuteData data = responceApi.getData();
                    if (data != null && CollectionUtils.isNotEmpty(data.getListCommutePackage())) {

                        List<ListCommutePackage> listCommutePackage = data.getListCommutePackage();
                        HashMap<String, List<CarDetailsAndPrice>> map = new HashMap<>();
                        for (ListCommutePackage commutePackage : listCommutePackage) {
                            List<CarDetailsAndPrice> carDetailsAndPriceList = commutePackage.getCarDetailsAndPriceList();

                            for (CarDetailsAndPrice detailsAndPrice : carDetailsAndPriceList) {
                                if (detailsAndPrice != null) {
                                    String packageName = commutePackage.getPackageName();
                                    if (map.containsKey(packageName)) {
                                        List<CarDetailsAndPrice> subList = map.get(packageName);
                                        subList.add(detailsAndPrice);
                                    } else {
                                        List<CarDetailsAndPrice> subList = new ArrayList<>();
                                        subList.add(detailsAndPrice);
                                        map.put(packageName, subList);
                                    }
                                }
                            }
                            tabs.clear();
                            commuteListFragment.clear();
                            Set<String> strings = map.keySet();
                            List<String> stringList = new ArrayList<>();
                            for (String s : strings) {
                                stringList.add(s);
                            }
                            Collections.sort(stringList);
                            for (String string : stringList) {
                                List<CarDetailsAndPrice> subList = map.get(string);
                                tabs.add(string);
                                commuteListFragment.add(CommuteItemFragment.getInstance(subList, generalServiceModel, string));
                                pagerAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                } else {
                    tvEmptyView.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_commute;
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        return commuteListFragment.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return tabs.get(position);
    }
}
