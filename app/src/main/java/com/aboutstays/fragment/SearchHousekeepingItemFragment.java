package com.aboutstays.fragment;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.cart.HkItemOrderDetails;
import com.aboutstays.model.cart.HkItemOrderDetailsList;
import com.aboutstays.model.cart.HouseKeepingCart;
import com.aboutstays.model.cart.HouseKeepingCartData;
import com.aboutstays.model.cart.HouseKeepingCartResponse;
import com.aboutstays.model.cart.HouseKeepingCartResponseData;
import com.aboutstays.model.housekeeping.HouseKeepingItemData;
import com.aboutstays.model.housekeeping.HouseKeepingItemDataResponce;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import simplifii.framework.ListAdapters.CustomExpandableListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;

/**
 * Created by neeraj on 17/3/17.
 */

public class SearchHousekeepingItemFragment extends BaseFragment implements CustomExpandableListAdapter.ExpandableListener {

    private List<HouseKeepingItemData> searchServiceList = new ArrayList<>();
    private TextView tvEmptyView;
    private String hkServiceId, staysId;
    private RelativeLayout rlMain;
    private ProgressBar progressBar;
    private String query;
    private String userId;
    private HouseKeepingCart houseKeepingCart;
    private CustomExpandableListAdapter customExpandableListAdapter;
    private LinkedHashMap<String, List<HouseKeepingItemData>> mapExpandable = new LinkedHashMap<>();
    private LinkedHashMap<String, List<HouseKeepingItemData>> mapExpandableAll = new LinkedHashMap<>();
    private List<String> listHeader = new ArrayList<>();

    private AddToCartListener addToCartListener;
    private ExpandableListView expandableListView;

    public static SearchHousekeepingItemFragment getInstance(AddToCartListener addToCartListener) {
        SearchHousekeepingItemFragment searchFoodItemsFragment = new SearchHousekeepingItemFragment();
        searchFoodItemsFragment.addToCartListener = addToCartListener;
        return searchFoodItemsFragment;
    }

    @Override
    public void initViews() {

        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            userId = sessionInstance.getUserId();
        }

        searchServiceList.clear();
        query = "";

        progressBar = (ProgressBar) findView(R.id.progressbar);
        rlMain = (RelativeLayout) findView(R.id.rl_main);
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);

        expandableListView = (ExpandableListView) findView(R.id.list_expandable_search);
        customExpandableListAdapter = new CustomExpandableListAdapter(getActivity(), R.layout.row_header_expandable, R.layout.row_laundry_item, mapExpandable, this);
        expandableListView.setAdapter(customExpandableListAdapter);
        expandableListView.setTextFilterEnabled(false);
        expandableListView.setEmptyView(tvEmptyView);
    }

    @Override
    public void onPreExecute(int taskCode) {
        if (taskCode == AppConstants.TASK_CODES.ADD_REMOVE_HOUSEKEEPING_TO_CART) {
            return;
        }
        super.onPreExecute(taskCode);
    }

    public void textUpdate(String newText, String serviceId, String staysId, HouseKeepingCartResponseData cartData) {
        this.hkServiceId = serviceId;
        this.query = newText;
        this.staysId = staysId;
        getAllHousekeepingItems();
        searchServiceList.clear();
        customExpandableListAdapter.notifyDataSetChanged();
        rlMain.setBackgroundColor(Color.WHITE);
        tvEmptyView.setTextColor(getResourceColor(R.color.gray_text));
    }

    private void getAllHousekeepingItems() {
        HttpParamObject httpParamObject = ApiRequestGenerator.searchHouseKeepingItem(hkServiceId, query);
        executeTask(AppConstants.TASK_CODES.SEARCH_HOUSEKEEPING, httpParamObject);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_search_house_keeping;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        progressBar.setVisibility(View.GONE);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.SEARCH_HOUSEKEEPING:
                HouseKeepingItemDataResponce searchResponse = (HouseKeepingItemDataResponce) response;
                if (searchResponse != null && searchResponse.getError() == false) {
                    List<HouseKeepingItemData> searchData = searchResponse.getData();
                    if (searchData != null && !searchData.isEmpty()) {
                        searchServiceList.clear();
                        for (HouseKeepingItemData itemData : searchData){
                            if (itemData.getShowOnApp() == true){
                                searchServiceList.add(itemData);
                            }
                        }
                        refreshCartData();
                        filterList(searchServiceList);
                    }
                }
                break;
            case AppConstants.TASK_CODES.ADD_REMOVE_HOUSEKEEPING_TO_CART:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false) {
//                    showToast(getString(R.string.cart_added));
//                    addToCartListener.searchCallback();
                }
                break;
        }
    }

    private void filterList(List<HouseKeepingItemData> itemDataList) {
        listHeader.clear();
        mapExpandable.clear();
        for (HouseKeepingItemData internetPackList : itemDataList) {
            String category = internetPackList.getCategory();
            if (!TextUtils.isEmpty(category)) {
                if (mapExpandable.containsKey(category)) {
                    List<HouseKeepingItemData> internetPackLists = mapExpandable.get(category);
                    internetPackLists.add(internetPackList);
                } else {
                    List<HouseKeepingItemData> laundryItemDatas = new ArrayList<>();
                    laundryItemDatas.add(internetPackList);
                    mapExpandable.put(category, laundryItemDatas);
                }
            }
        }
        refreshParentData();
        mapExpandableAll.putAll(mapExpandable);
    }

    private void refreshParentData() {
        listHeader.clear();
        Iterator<String> iterator = mapExpandable.keySet().iterator();
        while (iterator.hasNext()) {
            listHeader.add(iterator.next());
        }
        customExpandableListAdapter = new CustomExpandableListAdapter(getActivity(), R.layout.row_header_expandable, R.layout.row_internet, mapExpandable, this);
        expandableListView.setAdapter(customExpandableListAdapter);
        for (int x = 0; x < listHeader.size(); x++) {
            expandableListView.expandGroup(x);
        }
    }

    @Override
    public int getChildSize(int parentPosition) {
        return mapExpandable.get(listHeader.get(parentPosition)).size();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        HolderParent holderParent;
        if (convertView == null) {
            convertView = inflater.inflate(resourceId, null);
            holderParent = new HolderParent(convertView);
            convertView.setTag(holderParent);
        } else {
            holderParent = (HolderParent) convertView.getTag();
        }
        String title = listHeader.get(groupPosition);
        holderParent.bindData(title, isExpanded);
        return convertView;
    }


    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_laundry_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        HouseKeepingItemData houseKeepingItemData = mapExpandable.get(listHeader.get(groupPosition)).get(childPosition);
        holder.onBindData(houseKeepingItemData);
        return convertView;
    }

    class ViewHolder {

        TextView tvTitle, tvTime, serviceDescription, tvAdd;
        TextView tvMinus, tvCount, tvPlus;
        ImageView imgVegNonveg, imgChilli, imgLike;
        RelativeLayout rlCount, rlTitle;

        public ViewHolder(View view) {

            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            tvTime = (TextView) view.findViewById(R.id.tv_price);
            serviceDescription = (TextView) view.findViewById(R.id.tv_service_description);
            imgVegNonveg = (ImageView) view.findViewById(R.id.iv_veg_nonveg);
            imgChilli = (ImageView) view.findViewById(R.id.iv_chilli);
            imgLike = (ImageView) view.findViewById(R.id.iv_like);
            tvAdd = (TextView) view.findViewById(R.id.tv_add);
            tvMinus = (TextView) view.findViewById(R.id.tv_minus);
            tvCount = (TextView) view.findViewById(R.id.tv_count);
            tvPlus = (TextView) view.findViewById(R.id.tv_plus);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_add_count);
            rlTitle = (RelativeLayout) view.findViewById(R.id.rl_title);
        }

        public void onBindData(final HouseKeepingItemData ob) {

            tvCount.setText(ob.getItemCount() + "");
            tvTitle.setText(ob.getName());
            tvTime.setVisibility(View.GONE);

            tvPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int itemCount = ob.getItemCount();
                    ob.setItemCount(itemCount + 1);
                    HouseKeepingCartResponseData.refreshItem(ob, getActivity());
                    addToCartListener.updateCart();
                    addItemToCart();
                }
            });

            tvMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int itemCount = ob.getItemCount();
                    if (itemCount >= 1) {
                        ob.setItemCount(itemCount - 1);
                    }
                    if (ob.getItemCount() == 0) {
                        HouseKeepingCartResponseData.refreshItem(ob, getActivity());
                        addToCartListener.updateCart();
                    }
                    addItemToCart();
                }
            });

            tvAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ob.setItemCount(1);
                    HouseKeepingCartResponseData.refreshItem(ob, getActivity());
                    addToCartListener.updateCart();
                    addItemToCart();
                }
            });

            if (ob.getItemCount() >= 1) {
                tvAdd.setVisibility(View.GONE);
                rlCount.setVisibility(View.VISIBLE);
            } else {
                tvAdd.setVisibility(View.VISIBLE);
                rlCount.setVisibility(View.GONE);
            }

            rlTitle.setVisibility(View.GONE);
        }
    }

    public void addItemToCart() {
        customExpandableListAdapter.notifyDataSetChanged();
        List<HouseKeepingCartData> cartDataList = new ArrayList<>();
        for (HouseKeepingItemData itemData : searchServiceList) {
            houseKeepingCart = new HouseKeepingCart();
            houseKeepingCart.setUserId(userId);
            houseKeepingCart.setHotelId(itemData.getHotelId());
            houseKeepingCart.setStaysId(staysId);

            HouseKeepingCartData cartData = new HouseKeepingCartData();
            cartData.setCartType(2);
            cartData.setCartOperation(1);

            HkItemOrderDetails itemOrderDetails = new HkItemOrderDetails();
            itemOrderDetails.setHkItemId(itemData.getItemId());
            itemOrderDetails.setType(itemData.getType());
            itemOrderDetails.setPrice(itemData.getPrice());
            itemOrderDetails.setQuantity(itemData.getItemCount());

            cartData.setHkItemOrderDetails(itemOrderDetails);
            cartDataList.add(cartData);
            houseKeepingCart.setData(cartDataList);
        }

        if (houseKeepingCart != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.addOrRemoveHouseKeepingItem(houseKeepingCart);
            executeTask(AppConstants.TASK_CODES.ADD_REMOVE_HOUSEKEEPING_TO_CART, httpParamObject);
        }
    }

    public void refreshCartData() {
        List<HkItemOrderDetailsList> hkItemOrderDetailsList = HouseKeepingCartResponse.getInstance().getHkItemOrderDetailsList();
        for (HouseKeepingItemData dataByType : searchServiceList) {
                for (HkItemOrderDetailsList detailList : hkItemOrderDetailsList) {
                    if (dataByType.getItemId().equals(detailList.getHkItemId())) {
                        dataByType.setItemCount(detailList.getQuantity());
                    }
                }
            }
            customExpandableListAdapter.notifyDataSetChanged();
    }

    public void callBack() {
        addToCartListener.searchCallback();
    }

    class Holder {

        TextView tvTitle, tvTime, serviceDescription, tvAdd;
        TextView tvMinus, tvCount, tvPlus;
        ImageView imgVegNonveg, imgChilli, imgLike;
        RelativeLayout rlCount, rlTopLine;
        View viewBottomLine;

        public Holder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            tvTime = (TextView) view.findViewById(R.id.tv_price);
            serviceDescription = (TextView) view.findViewById(R.id.tv_service_description);
            imgVegNonveg = (ImageView) view.findViewById(R.id.iv_veg_nonveg);
            imgChilli = (ImageView) view.findViewById(R.id.iv_chilli);
            imgLike = (ImageView) view.findViewById(R.id.iv_like);
            tvAdd = (TextView) view.findViewById(R.id.tv_add);
            tvMinus = (TextView) view.findViewById(R.id.tv_minus);
            tvCount = (TextView) view.findViewById(R.id.tv_count);
            tvPlus = (TextView) view.findViewById(R.id.tv_plus);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_add_count);
            viewBottomLine = view.findViewById(R.id.view_bottom_line);
            rlTopLine = (RelativeLayout) view.findViewById(R.id.rl_row_view);
        }
    }

    class HolderParent {
        TextView textView;
        ImageView ivIndicator;

        public HolderParent(View view) {
            textView = (TextView) view.findViewById(R.id.tv_men_women);
            ivIndicator = (ImageView) view.findViewById(R.id.iv_indicator);
        }

        public void bindData(String title, boolean isExpand) {
            textView.setText(title);
            if (isExpand) {
                ivIndicator.setImageResource(R.mipmap.arrow_right_white);
            } else {
                ivIndicator.setImageResource(R.mipmap.arrow_down_white);
            }
        }
    }

    public interface AddToCartListener {
        void searchCallback();
        void updateCart();
    }

}
