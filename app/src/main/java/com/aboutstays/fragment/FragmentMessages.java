package com.aboutstays.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.adapter.BaseRecycleAdapter;
import com.aboutstays.model.BaseAdapterModel;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.message.AddMessage;
import com.aboutstays.model.message.MessageApiResponse;
import com.aboutstays.model.message.MessageData;
import com.aboutstays.model.message.MessageList;
import com.aboutstays.model.stayline.GetStayResponse;
import com.aboutstays.rest.request.BaseServiceRequest;
import com.aboutstays.utitlity.ApiRequestGenerator;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by mrnee on 4/28/2017.
 */

public class FragmentMessages extends BaseFragment {
    private RecyclerView rvMessages;
    private BaseRecycleAdapter baseRecyclerAdapter;
    private EditText etMsg;
    private ImageView imgSend;
    private TextView tvEmptyView;
    private List<BaseAdapterModel> messageModelList = new ArrayList<>();
    private String hotelId;
    private String userId;
    private String staysId;

    @Override
    public void initViews() {
        initToolBar(getString(R.string.message));
        Bundle bundle = getArguments();
        if (bundle != null) {
            GeneralServiceModel generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null) {
                if (generalServiceModel.getHotelStaysList().getStaysPojo() != null) {
                    hotelId = generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId();
                    staysId = generalServiceModel.getHotelStaysList().getStaysPojo().getStaysId();
                    userId = generalServiceModel.getHotelStaysList().getStaysPojo().getUserId();
                }
            }
        }

        rvMessages = (RecyclerView) findView(R.id.recycler_message);
        etMsg = (EditText) findView(R.id.et_message);
        tvEmptyView = (TextView) findView(R.id.tv_empty_view);
        imgSend = (ImageView) findView(R.id.iv_send_btn);
        baseRecyclerAdapter = new BaseRecycleAdapter(getActivity(), messageModelList);
        rvMessages.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMessages.setAdapter(baseRecyclerAdapter);

        getAllMessage();
        imgSend.setOnClickListener(this);
    }

    private void getAllMessage() {
        BaseServiceRequest baseServiceRequest = new BaseServiceRequest();
        baseServiceRequest.setHotelId(hotelId);
        baseServiceRequest.setStaysId(staysId);
        baseServiceRequest.setUserId(userId);
        HttpParamObject httpParamObject = ApiRequestGenerator.getMessage(baseServiceRequest);
        executeTask(AppConstants.TASK_CODES.GET_MESSAGE, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_send_btn:
                sendMessage();
                break;
        }

    }

    private void sendMessage() {
        String message = etMsg.getText().toString().trim();
        if (!TextUtils.isEmpty(message)) {
            AddMessage baseServiceRequest = new AddMessage();
            baseServiceRequest.setHotelId(hotelId);
            baseServiceRequest.setStaysId(staysId);
            baseServiceRequest.setUserId(userId);
            baseServiceRequest.setMessageText(message);
            HttpParamObject httpParamObject = ApiRequestGenerator.addMessage(baseServiceRequest);
            executeTask(AppConstants.TASK_CODES.SEND_MESSAGE, httpParamObject);
        } else {
            showToast("Please enter message first");
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_MESSAGE:
                MessageApiResponse messageApiResponse = (MessageApiResponse) response;
                if (messageApiResponse != null && !messageApiResponse.getError()) {
                    tvEmptyView.setVisibility(View.INVISIBLE);
                    MessageData messageData = messageApiResponse.getData();
                    if (messageData != null) {
                        List<MessageList> messages = messageData.getMessages();
                        if (CollectionUtils.isNotEmpty(messages)) {
                            messageModelList.clear();
                            messageModelList.addAll(messages);
                            filterLastMessage(messages);
                            baseRecyclerAdapter.notifyDataSetChanged();
                            rvMessages.scrollToPosition(messageModelList.size() - 1);
                        }
                    }
                } else {
                    tvEmptyView.setVisibility(View.VISIBLE);
                }
                break;

            case AppConstants.TASK_CODES.SEND_MESSAGE:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && !apiResponse.getError()) {
//                    showToast(apiResponse.getMessage());
                    getAllMessage();
                    etMsg.setText("");
                }
                break;
        }
    }

    private void filterLastMessage(List<MessageList> messages) {
        for (int i = 0; i < messages.size(); i++) {
            MessageList messageList = messages.get(i);
            if (i > 0) {
                MessageList messagePrevious = messages.get(i - 1);
                if (messagePrevious.getByUser() == messagePrevious.getByUser()) {
                    messagePrevious.setLastMessage(false);
                    messageList.setLastMessage(true);
                }
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_messages;
    }
}
