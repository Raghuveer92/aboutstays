package com.aboutstays.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.complementaryServices.Service;
import com.aboutstays.rest.response.ChildCategoryItem;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import simplifii.framework.ListAdapters.CustomExpandableListAdapter;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by aman on 22/04/17.
 */

public class ComplementaryServiceItemFragment extends BaseFragment implements CustomExpandableListAdapter.ExpandableListener{

    private ExpandableListView expandableListView;
    private CustomExpandableListAdapter customExpandableListAdapter;
    private List<Service> listCompService = new ArrayList<>();


    public static ComplementaryServiceItemFragment getInstance(List<Service> listCompService) {
        ComplementaryServiceItemFragment fragment = new ComplementaryServiceItemFragment();
        fragment.listCompService.clear();
        fragment.listCompService = listCompService;
        return fragment;
    }


    @Override
    public void initViews() {

        expandableListView = (ExpandableListView) findView(R.id.exlv_comp_service);
        customExpandableListAdapter = new CustomExpandableListAdapter(getActivity(), R.layout.row_comp_service, R.layout.row_child_description, listCompService, this);
        expandableListView.setAdapter(customExpandableListAdapter);

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_comp_service_item;
    }

    @Override
    public int getChildSize(int parentPosition) {
        return listCompService.size();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        convertView = inflater.inflate(resourceId, parent, false);
        setText(R.id.tv_title, listCompService.get(groupPosition).getTitle(), convertView);

        ImageView imageView= (ImageView) convertView.findViewById(R.id.iv_right_angle);

        View view = convertView.findViewById(R.id.rl_row_view);
        view.setVisibility(View.VISIBLE);

        if(isExpanded){
            imageView.setImageResource(R.mipmap.up_arrow_red);
        }else {
            imageView.setImageResource(R.mipmap.down_arrow_red);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        convertView = inflater.inflate(resourceId, parent, false);
        setText(R.id.tv_child_title, listCompService.get(groupPosition).getDescriptionList().get(childPosition), convertView);
        View viewLineDescription = (View) convertView.findViewById(R.id.view_line_description);
        if(isLastChild) {
            viewLineDescription.setVisibility(View.GONE);
        } else {
            viewLineDescription.setVisibility(View.VISIBLE);
        }
        return convertView;
    }
}
