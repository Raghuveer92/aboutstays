package com.aboutstays.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.CheckInActivity;
import com.aboutstays.activity.DrawerActivity;
import com.aboutstays.model.stayline.GeneralInfo;
import com.aboutstays.model.stayline.GetStayResponse;
import com.aboutstays.model.stayline.HotelStaysList;
import com.aboutstays.model.stayline.StaysAddress;
import com.daimajia.swipe.SwipeLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;
import simplifii.framework.widgets.CustomRatingBar;

import static com.aboutstays.R.id.tv_room_type;


public class UpcomingStaysFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    List<HotelStaysList> list;
    private ListView listView;
    private CustomListAdapter listAdapter;
    private TextView tvError;


    public static UpcomingStaysFragment getInstance(List<HotelStaysList> upcomingStayList) {
        UpcomingStaysFragment fragment = new UpcomingStaysFragment();
        fragment.list = upcomingStayList;
        return fragment;
    }

    @Override
    public void initViews() {
        tvError = (TextView) findView(R.id.tv_empty_view);

        if (list != null) {
            setdatatoView(list);
        }

        filterList(list);

    }

    private void filterList(List<HotelStaysList> list) {
        List<HotelStaysList> futureStayList = new ArrayList<>();
        List<HotelStaysList> currentStayList = new ArrayList<>();
        futureStayList.clear();
        currentStayList.clear();
        boolean isPastList = false;

        if (CollectionUtils.isNotEmpty(list)) {
            for (HotelStaysList hotelStaysList : list) {
                if (hotelStaysList == null) {
                    continue;
                }
                GetStayResponse staysPojo = hotelStaysList.getStaysPojo();
                if (staysPojo != null) {
                    if (staysPojo.getStaysOrdering() == AppConstants.STAYS_ORDERING.FUTURE) {
                        hotelStaysList.setColorId(R.color.future_stay_gray);
                        futureStayList.add(hotelStaysList);
                    } else if (staysPojo.getStaysOrdering() == AppConstants.STAYS_ORDERING.CURRENT) {
                        hotelStaysList.setColorId(R.color.current_stay_green);
                        currentStayList.add(hotelStaysList);
                    } else {
                        isPastList = true;
                        hotelStaysList.setColorId(R.color.login_activity_background);
                    }
                }

                if (futureStayList.size() > 0) {
                    futureStayList.get(0).setFirst(true);
                }

                if (currentStayList.size() > 0) {
                    currentStayList.get(0).setFirst(true);
                }

            }
        }
        if (!isPastList) {
            if (CollectionUtils.isNotEmpty(list)) {
                list.clear();
            }
            list.addAll(currentStayList);
            list.addAll(futureStayList);
        }

    }

    private void setdatatoView(List<HotelStaysList> list) {
        listView = (ListView) findView(R.id.listview_upcoming);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.view_check_in, list, this);
        listView.setAdapter(listAdapter);
//        listView.setMenuCreator(creator);
//        listView.setEmptyView(tvError);
//        listView.setOnItemClickListener(this);
//        putDataIntoList();
        listAdapter.notifyDataSetChanged();

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_upcoming_stays;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, final LayoutInflater inflater) {

        Holder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.view_check_in, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final HotelStaysList hotelStaysList = list.get(position);

        if (hotelStaysList.isEmptyStayPojo()){
            tvError.setVisibility(View.VISIBLE);
        } else {
            tvError.setVisibility(View.GONE);
        }
        if (hotelStaysList.isFirst()) {
            holder.tvStayStaus.setVisibility(View.VISIBLE);
        } else {
            holder.tvStayStaus.setVisibility(View.GONE);
        }

        if (hotelStaysList.getStaysPojo() != null){
            boolean official = hotelStaysList.getStaysPojo().isOfficial();
            if (official){
                holder.ivOfficial.setVisibility(View.VISIBLE);
            } else {
                holder.ivOfficial.setVisibility(View.GONE);
            }
        }

        if (hotelStaysList.getRoomPojo() != null) {
            holder.tvRoomType.setText(hotelStaysList.getRoomPojo().getName());
        }

        holder.tvStayStaus.setText(hotelStaysList.getStayTitle());
        holder.customRatingBar.setRating(hotelStaysList.getHotelType());
        if (hotelStaysList.getPartnershipType() == 0) {
            holder.ivPartnerShip.setVisibility(View.INVISIBLE);
        } else {
            holder.ivPartnerShip.setVisibility(View.VISIBLE);
        }

        if (hotelStaysList.getGeneralInfo() != null) {
            holder.tvTitle.setText(hotelStaysList.getGeneralInfo().getName());
            if (!TextUtils.isEmpty(hotelStaysList.getGeneralInfo().getLogoUrl())) {
                Picasso.with(getActivity()).load(hotelStaysList.getGeneralInfo().getLogoUrl())
                        .placeholder(R.drawable.progress_animation)
                        .into(holder.ivRowUpcoming);
            }
        } else {
            holder.tvTitle.setText("");
            holder.ivRowUpcoming.setImageResource(R.drawable.progress);
        }
        GetStayResponse staysPojo = hotelStaysList.getStaysPojo();
        if (staysPojo != null) {
            holder.tvCheckinDate.setText(staysPojo.getCheckInDate());
            holder.tvCheckOutDate.setText(staysPojo.getCheckOutDate());

        }

        if (!TextUtils.isEmpty(hotelStaysList.getActualCheckedIn())) {
            String checkin = Util.convertDateFormat(hotelStaysList.getActualCheckedIn(),
                    Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
            holder.tvCheckinTime.setText(checkin);
        } else {
            String checkInTime = Util.convertDateFormat(staysPojo.getCheckInTime(),
                    Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
            if (TextUtils.isEmpty(checkInTime)) {
                holder.tvCheckinTime.setText(staysPojo.getCheckInTime());
            } else {
                holder.tvCheckinTime.setText(checkInTime);
            }
        }

        if (!TextUtils.isEmpty(hotelStaysList.getActualCheckedOut())) {
            String checkout = Util.convertDateFormat(hotelStaysList.getActualCheckedOut(),
                    Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
            holder.tvCheckoutTime.setText(checkout);
        } else {
            String checkOutTime = Util.convertDateFormat(staysPojo.getCheckOutTime(),
                    Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
            if (TextUtils.isEmpty(checkOutTime)) {
                holder.tvCheckoutTime.setText(staysPojo.getCheckOutTime());
            } else {
                holder.tvCheckoutTime.setText(checkOutTime);
            }
        }

        holder.swipeLayout.setSwipeEnabled(false);
        holder.rl_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CheckInActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.HOTEL_CHECK_IN, hotelStaysList);
                intent.putExtras(bundle);
                startActivityForResult(intent, AppConstants.REQUEST_CODE.CHECK_IN);
            }
        });

        holder.rlMain.setBackgroundColor(getResourceColor(hotelStaysList.getColorId()));

        GeneralInfo generalInfo = hotelStaysList.getGeneralInfo();
        if (generalInfo != null) {
            StaysAddress address = generalInfo.getAddress();
            if (address != null) {
                holder.tvAddress.setText(address.getCity());
            } else {
                holder.tvAddress.setText("");
            }
        } else {
            holder.tvAddress.setText("");
        }
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        HotelStaysList hotelStaysList = list.get(position);
//        Bundle bundle = new Bundle();
//        bundle.putSerializable(AppConstants.BUNDLE_KEYS.HOTEL_CHECK_IN, hotelStaysList);
//        startNextActivity(bundle, CheckInActivity.class);
    }

    public void refreshList() {
        if (v != null) {
            filterList(list);
            listAdapter.notifyDataSetChanged();
        }
    }

    class Holder {
        TextView tvStayStaus, tvTitle, tvCheckinDate, tvCheckOutDate, tvCheckinTime, tvCheckoutTime, tvAddress, tvRoomType;
        ImageView ivRowUpcoming, ivPartnerShip, ivOfficial;
        RelativeLayout rlMain, rl_top;
        SwipeLayout swipeLayout;
        CustomRatingBar customRatingBar;

        public Holder(View view) {
            swipeLayout = (SwipeLayout) view.findViewById(R.id.swipe_layout);
            tvRoomType = (TextView) view.findViewById(R.id.tv_room_name);
            tvStayStaus = (TextView) view.findViewById(R.id.tv_stay);
            tvTitle = (TextView) view.findViewById(R.id.tv_title_upcoming);
            tvCheckinDate = (TextView) view.findViewById(R.id.tv_checkin_date);
            tvCheckOutDate = (TextView) view.findViewById(R.id.tv_checkout_date);
            tvCheckinTime = (TextView) view.findViewById(R.id.tv_checkin_time);
            tvCheckoutTime = (TextView) view.findViewById(R.id.tv_checkout_time);
            tvAddress = (TextView) view.findViewById(R.id.tv_hotel_address);
            ivRowUpcoming = (ImageView) view.findViewById(R.id.iv_row_upcoming);
            rlMain = (RelativeLayout) view.findViewById(R.id.lay_top);
            rl_top = (RelativeLayout) view.findViewById(R.id.rl_main);
            customRatingBar = (CustomRatingBar) view.findViewById(R.id.custom_rating_bar);
            ivPartnerShip = (ImageView) view.findViewById(R.id.iv_partnership);
            ivOfficial = (ImageView) view.findViewById(R.id.iv_official);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK){
            getActivity().setResult(getActivity().RESULT_OK);
            getActivity().finish();
            openFragment();
        }
        if (resultCode == AppConstants.RESULT_CODE.RESULT_DONE){
            getActivity().setResult(getActivity().RESULT_OK);
            getActivity().finish();
            openFragment();
        }
    }

    private void openFragment() {
            Bundle bundle = new Bundle();
            bundle.putBoolean(AppConstants.BUNDLE_KEYS.FROM_ADD_STAY, true);
            Intent intent = new Intent(getActivity(), DrawerActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtras(bundle);
            startActivity(intent);
    }
}
