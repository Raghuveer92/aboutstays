package com.aboutstays.fragment;

import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.holder.FeedbackHolder;

import simplifii.framework.fragments.BaseFragment;

/**
 * Created by aman on 07/04/17.
 */

public class FeedbackAndSuggestionsRowFragment extends BaseFragment implements FeedbackHolder.UpdateFeedBackListener, CompoundButton.OnCheckedChangeListener {

    private LinearLayout llFeedbackRow;
    private LinearLayout llQualityContainer;
    private LinearLayout llTimetakenCotainer;
    private TextView tvFeedbackCategory;
    private String categoryName;
    private CheckBox cbFeedback;
    private int quality;
    private int timeTaken;
    private boolean alreadySubmitted;
    private FeedbackHolder feedbackHolderQuality;
    private FeedbackHolder feedbackHolderTimeTaken;
    private boolean qualitySubmitted;
    private boolean timeTakenSubmitted;
    private boolean userSelectedQualitySubmitted;
    private boolean userSelectedTimeTakenSubmitted;


    private FeedbackHolder.FeedbackListener feedbackListener;
    private boolean isEditable;

    public FeedbackHolder.FeedbackListener getFeedbackListener() {
        return feedbackListener;
    }

    public void setFeedbackListener(FeedbackHolder.FeedbackListener feedbackListener) {
        this.feedbackListener = feedbackListener;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }


    public boolean isAlreadySubmitted() {
        return alreadySubmitted;
    }

    public void setAlreadySubmitted(boolean alreadySubmitted) {
        this.alreadySubmitted = alreadySubmitted;
    }

    public void setUserSelectedQuality(int value) {
        feedbackHolderQuality.setProgress(value);
    }

    public void setUserSelectedTimeTaken(int value) {
        feedbackHolderTimeTaken.setProgress(value);
    }

    public int getUserSelectedQuality() {
        return feedbackHolderQuality.getData();
    }

    public int getUserSelectedTimeTaken() {
        return feedbackHolderTimeTaken.getData();
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public int getQuality() {
        return quality;
    }

    public int getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(int timeTaken) {
        this.timeTaken = timeTaken;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public boolean isQualitySubmitted() {
        return feedbackHolderQuality.isSubmitted();
    }

    public void setQualitySubmitted(boolean qualitySubmitted) {
        this.qualitySubmitted = qualitySubmitted;
    }

    public boolean isTimeTakenSubmitted() {
        return feedbackHolderTimeTaken.isSubmitted();
    }

    public void setTimeTakenSubmitted(boolean timeTakenSubmitted) {
        this.timeTakenSubmitted = timeTakenSubmitted;
    }


    public void setUserSelectedQualitySubmitted(boolean userSelectedQualitySubmitted) {
        feedbackHolderQuality.setSubmitted(userSelectedQualitySubmitted);
    }


    public void setUserSelectedTimeTakenSubmitted(boolean userSelectedTimeTakenSubmitted) {
        feedbackHolderTimeTaken.setSubmitted(userSelectedTimeTakenSubmitted);
    }

    @Override
    public void initViews() {
        llFeedbackRow = (LinearLayout) findView(R.id.ll_container);
        tvFeedbackCategory = (TextView) findView(R.id.tv_category);
        llQualityContainer = (LinearLayout) findView(R.id.ll_container_left);
        llTimetakenCotainer = (LinearLayout) findView(R.id.ll_container_right);
        cbFeedback = (CheckBox) findView(R.id.check_box_feedback);
        cbFeedback.setOnCheckedChangeListener(this);

        feedbackHolderQuality = new FeedbackHolder(getActivity(), llQualityContainer, this.isEditable, this);
        feedbackHolderTimeTaken = new FeedbackHolder(getActivity(), llTimetakenCotainer, this.isEditable, this);
        refresh();
    }

    @Override
    public int getViewID() {
        return R.layout.row_feedback_and_suggestions;
    }

    public void refresh() {
        tvFeedbackCategory.setText(categoryName);
        feedbackHolderQuality.setData(getQuality(), "Quality");
        feedbackHolderQuality.setSubmitted(qualitySubmitted);
        feedbackHolderQuality.changeSmileys();
        feedbackHolderQuality.setFeedbackListener(this.feedbackListener);

        feedbackHolderTimeTaken.setData(getTimeTaken(), "Time taken");
        feedbackHolderTimeTaken.setSubmitted(timeTakenSubmitted);
        feedbackHolderTimeTaken.changeSmileys();
        feedbackHolderTimeTaken.setFeedbackListener(this.feedbackListener);
    }

    public void setSmiley() {
        feedbackHolderQuality.changeSmileys();
        feedbackHolderTimeTaken.changeSmileys();
    }

    @Override
    public void updateFeedback() {
        cbFeedback.setChecked(false);
    }

    private void refreshFeedBack(boolean isUpdate) {
        feedbackHolderQuality.setSubmitted(!isUpdate);
        feedbackHolderTimeTaken.setSubmitted(!isUpdate);
        feedbackHolderQuality.changeSmileys();
        feedbackHolderTimeTaken.changeSmileys();
    }

    public boolean isChecked() {
        return cbFeedback.isChecked();
    }

    public void setFeedbackChecked(boolean ifTrue) {
        cbFeedback.setChecked(!ifTrue);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        refreshFeedBack(b);
    }
}
