package com.aboutstays.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.model.entertainment.EntertainmentData;
import com.aboutstays.model.entertainment.EntertainmentResponseApi;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.reservation.TypeInfoList;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by neeraj on 6/4/17.
 */

public class FragmentTvEntertainment extends BaseFragment implements CustomListAdapterInterface {

    private GridView gvTvEntertainment;
    private CustomListAdapter listAdapter;
    private List<TypeInfoList> entertainmentList = new ArrayList<>();
    private TextView tvAllView;
    private GeneralServiceModel generalServiceModel;
    private String hsId;
    private String serviceId;


    @Override
    public void initViews() {

        initToolBar(getString(R.string.tv_entertainment));

        Bundle bundle = getArguments();
        if (bundle != null) {
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null) {
                hsId = generalServiceModel.getGeneralServiceData().getHsId();
            }
        }

        tvAllView = (TextView) findView(R.id.tv_view_all);
        gvTvEntertainment = (GridView) findView(R.id.grid_tv);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_tv_grid_entertainment, entertainmentList, this);
        gvTvEntertainment.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();

        setOnClickListener(R.id.tv_view_all);

        getEntertainmentService();
    }

    private void getEntertainmentService() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getEntertainMentService(hsId);
        executeTask(AppConstants.TASK_CODES.ENTERTAINMENT_SERVICE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.ENTERTAINMENT_SERVICE:
                EntertainmentResponseApi responseApi = (EntertainmentResponseApi) response;
                if (responseApi != null && responseApi.getError() == false){
                    EntertainmentData entertainmentData = responseApi.getData();
                    if (entertainmentData != null){
                        serviceId = entertainmentData.getServiceId();
                        List<TypeInfoList> typeInfoList = entertainmentData.getTypeInfoList();
                        if (CollectionUtils.isNotEmpty(typeInfoList)){
                            entertainmentList.addAll(typeInfoList);
                            listAdapter.notifyDataSetChanged();
                        }
                    }
                }
                break;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_view_all:
                FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.FRAGMENT_ALL_CHANNEL, null);
                break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_tv_entertainmennt;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if(convertView == null){
            convertView = inflater.inflate(resourceID, parent,false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }

        final TypeInfoList entertainmentResponseApi = entertainmentList.get(position);
        holder.tvName.setText(entertainmentResponseApi.getTypeName());
        String imageUrl = entertainmentResponseApi.getImageUrl();
        if (!TextUtils.isEmpty(imageUrl)){
            Picasso.with(getActivity()).load(imageUrl).into(holder.ivLogo);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.BUNDLE_KEYS.SERVICE_ID, serviceId);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, entertainmentResponseApi);
                FragmentContainerActivity.startActivity(getActivity(), AppConstants.FRAGMENT_TYPE.FRAGMENT_ALL_CHANNEL, bundle);
            }
        });
        return convertView;
    }

    class Holder{
        TextView tvName;
        ImageView ivLogo;

        public Holder (View view){
            tvName = (TextView) view.findViewById(R.id.tv_name);
            ivLogo = (ImageView) view.findViewById(R.id.iv_logo);
        }
    }
}
