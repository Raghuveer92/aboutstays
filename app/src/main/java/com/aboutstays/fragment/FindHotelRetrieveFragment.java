package com.aboutstays.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.aboutstays.R;
import com.aboutstays.activity.SearchHotelActivity;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.listeners.GetHotelDataListener;
import com.aboutstays.model.hotels.GeneralInformation;
import com.aboutstays.model.hotels.Hotel;
import com.aboutstays.model.hotels.HotelAddress;
import com.aboutstays.model.hotels.HotelData;
import com.aboutstays.model.stayline.SearchedData;
import com.aboutstays.model.stayline.SearchedHotelData;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Preferences;

public class FindHotelRetrieveFragment extends BaseFragment {
    private AutoCompleteTextView actvCity, actvHotel;
    Map<String, List<String>> filterMap = new HashMap<String, List<String>>();
    private List<String> cities = new ArrayList<>();
    private List<String> hotels = new ArrayList<>();
    private String hotelName;
    private String cityName;
    private GetHotelDataListener getHotelDataListener;
    private ArrayAdapter adapterCities;
    private ArrayAdapter adapterHotels;
    private Button btnFindHotel;
    private boolean citySelect, hotelSelect;
    private HotelData hotelData;
    private List<Hotel> listHotels;

    public static FindHotelRetrieveFragment getInstance(GetHotelDataListener getHotelDataListener) {
        FindHotelRetrieveFragment findHotelRetrieveFragment = new FindHotelRetrieveFragment();
        findHotelRetrieveFragment.getHotelDataListener = getHotelDataListener;
        return findHotelRetrieveFragment;
    }


    @Override
    public void initViews() {
        Toolbar toolbar = (Toolbar) findView(R.id.toolbar);
        actvCity = (AutoCompleteTextView) findView(R.id.et_city_name);
        actvHotel = (AutoCompleteTextView) findView(R.id.et_hotel_name);
        btnFindHotel = (Button) findView(R.id.btn_find_hotel);
        setOnClickListener(R.id.btn_find_hotel);

        setAdapters(hotels);

        Bundle bundle = getArguments();
        if (bundle != null){
            boolean addStay = bundle.getBoolean(AppConstants.BUNDLE_KEYS.ADD_STAY);
            if (addStay){
                toolbar.setVisibility(View.VISIBLE);
                initToolBar(getString(R.string.retrieve_stay).toUpperCase());
            } else {
                toolbar.setVisibility(View.GONE);
            }
        }

        actvCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String city = actvCity.getText().toString().trim();
                citySelect = true;
                if (filterMap.containsKey(city)) {
                    List<String> hotelList = filterMap.get(city);
                    hotels.clear();
                    actvHotel.clearListSelection();
                    actvHotel.setText("");
                    hotels.addAll(hotelList);
                    setAdapters(hotels);
                }
            }
        });

        actvHotel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                hotelSelect = true;
            }
        });

        Gson gson = new Gson();
        String json = Preferences.getData(AppConstants.PREF_KEYS.HOTEL_DATA, "");
        if (!TextUtils.isEmpty(json)) {
            hotelData = gson.fromJson(json, HotelData.class);
            listHotels = hotelData.getListHotels();
            if (CollectionUtils.isNotEmpty(listHotels)) {
//                setHotelAdapter();
                filterLists(listHotels);
            }
        }

        actvCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String s = charSequence.toString().toLowerCase();
                citySelect = false;
                if (filterMap.containsKey(s)) {
                    List<String> hotelList = filterMap.get(s);
                    hotels.clear();
                    actvHotel.clearListSelection();
                    actvHotel.setText("");
                    hotels.addAll(hotelList);
                    setAdapters(hotels);
                } else {
                    hotels.clear();
                    actvHotel.setText("");
                    setAdapters(hotels);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        actvHotel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                hotelSelect = false;
                if (TextUtils.isEmpty(actvCity.getText().toString().trim())) {
                    setHotelAdapter();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

//        if(getHotelDataListener!=null){
//            filterLists(getHotelDataListener.getHotelList());
//        }
    }

    private void setHotelAdapter() {
        int size = listHotels.size();
        hotels.clear();
        for (int i = 0; i < size; i++) {
            Hotel hotel = listHotels.get(i);
            if (hotel.getGeneralInformation() != null) {
                String name = hotel.getGeneralInformation().getName();
                hotels.add(name);
                setAdapters(hotels);
            }
        }
    }

    private void setAdapters(List<String> hotels) {
        adapterCities = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, cities);
        adapterHotels = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, hotels);
        actvCity.setAdapter(adapterCities);
        actvCity.setThreshold(1);
        actvHotel.setAdapter(adapterHotels);
        actvHotel.setThreshold(1);
        adapterCities.notifyDataSetChanged();
        adapterHotels.notifyDataSetChanged();
    }

    private void filterLists(List<Hotel> hotelList) {
        new AsyncTask() {

            @Override
            protected Object doInBackground(Object[] params) {
                List<Hotel> hotelList = new ArrayList<Hotel>();
                List<Hotel> param = (List<Hotel>) params[0];
                if (CollectionUtils.isNotEmpty(param)) {
                    hotelList.addAll(param);
                }
                if (CollectionUtils.isNotEmpty(hotelList)) {
                    for (Hotel hotel : hotelList) {
                        GeneralInformation generalInformation = hotel.getGeneralInformation();
                        if (generalInformation != null) {
                            HotelAddress address = generalInformation.getAddress();
                            if (address != null) {
                                String city = address.getCity();
                                if (city != null) {
                                    String cityName = city.toLowerCase();
                                    String hotelName = generalInformation.getName();
                                    if (filterMap.containsKey(cityName)) {
                                        List<String> tempHotelList = filterMap.get(cityName);
                                        tempHotelList.add(hotelName);
                                    } else {
                                        List<String> tempHotelList = new ArrayList();
                                        tempHotelList.add(hotelName);
                                        filterMap.put(cityName, tempHotelList);
                                    }
                                }
                            }
                        }
                    }
                }
                Set<String> strings = filterMap.keySet();
                Iterator<String> iterator = strings.iterator();
                while (iterator.hasNext()) {
                    cities.add(iterator.next());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                Collections.sort(cities);
                adapterCities.notifyDataSetChanged();
            }
        }.execute(hotelList);
    }

    public void validateCheck() {

        cityName = actvCity.getText().toString().trim();
        hotelName = actvHotel.getText().toString().trim();

        if (!citySelect && !hotelSelect){
            showToast("Please select a City name or Hotel name");
            return;
        }

        if (TextUtils.isEmpty(cityName) && TextUtils.isEmpty(hotelName)) {
            showToast(getString(R.string.enter_city_hotel_name));
            return;
        }

        getBookingHoteldata();

    }

    private void getBookingHoteldata() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getBookingHoteldata(cityName, hotelName);
        executeTask(AppConstants.TASK_CODES.GETBOOKHOTELDATA, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GETBOOKHOTELDATA:
                SearchedHotelData searchedHotelData = (SearchedHotelData) response;
                if (searchedHotelData != null && searchedHotelData.getError() == false) {
                    SearchedData searchData = searchedHotelData.getData();
                    if (searchData != null) {
//                        showToast(searchedHotelData.getMessage());
                        Bundle bundle = new Bundle();
                        bundle.putString(AppConstants.BUNDLE_KEYS.CITY_NAME, cityName);
                        bundle.putString(AppConstants.BUNDLE_KEYS.HOTEL_NAME, hotelName);
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.SEARCH_HOTEL_RESPONSE, searchData);
                        startNextActivity(bundle, SearchHotelActivity.class);
                    }
                } else {
                    successDialog();
                }
                break;
        }
    }

    private void successDialog() {

        SuccessDialogFragment.show(getChildFragmentManager(),
                "RETRIEVE STAY",
                "NO HOTEL FOUND",
                "Please modify search combination and retry.",
                getString(R.string.close),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {

                    }
                }
        );
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_find_hotel_retrieve;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_find_hotel:
                validateCheck();
//                startNextActivity(SearchHotelActivity.class);
                break;
        }
    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.left_arrow;
    }


}
