package com.aboutstays.enums;

import com.aboutstays.activity.AirportPickupActivity;
import com.aboutstays.activity.ExpenseDashboardActivity;
import com.aboutstays.activity.FragmentContainerActivity;
import com.aboutstays.activity.HotelInfoActivity;
import com.aboutstays.activity.HotelNameActivity;
import com.aboutstays.activity.HousekeepingActivity;
import com.aboutstays.activity.InternetActivity;
import com.aboutstays.activity.LaundryActivity;
import com.aboutstays.activity.NotificationsActivity;
import com.aboutstays.activity.RequestsActivity;
import com.aboutstays.activity.RoomServiceActivity;

/**
 * Created by aman on 03/03/17.
 */

public enum ServiceType {

    AIRPORT_SERVICE(1, AirportPickupActivity.class),
    NOTIFICATION(2, NotificationsActivity.class),
    REQUEST(3, RequestsActivity.class),
    EXPENSE_DASHBOARD(4, ExpenseDashboardActivity.class),
    ROOM_SERVICE(5, RoomServiceActivity.class),
    HOUSE_KEEPING(6, HousekeepingActivity.class),
    LAUNDRY(7, LaundryActivity.class),
    INTERNET(8, InternetActivity.class),
    RESERVATION(9, FragmentContainerActivity.class),
    COMMUTE(10, FragmentContainerActivity.class),
    MAINTENANCE(11, FragmentContainerActivity.class),
    MESSAGE(12, FragmentContainerActivity.class),
    AIRPORT_DROP(13, AirportPickupActivity.class),
    LATE_CHECKOUT(14, FragmentContainerActivity.class),
    BELL_BOY(15, FragmentContainerActivity.class),
    FEEDBACK_AND_SUGGESTION(16, FragmentContainerActivity.class),
    HOTEL_INFO(17, HotelNameActivity.class),
    HOTEL_INFO_STAYS(18, HotelNameActivity.class),
    HOUSE_RULES(19, FragmentContainerActivity.class),
    TV_ENTERTAINMENT(20, FragmentContainerActivity.class),
    CITY_GUIDE(21, FragmentContainerActivity.class),
    COMPLEMENTRY_SERVICE(22, FragmentContainerActivity.class),
    EXPENSE_CHECKOUT(24, ExpenseDashboardActivity.class),
    MESSAGE_CURRENT_STAY(26, FragmentContainerActivity.class),
    NOTIFICATIONS_CHECKIN(27, NotificationsActivity.class),
    REQUESTS_CHECKIN(28,RequestsActivity.class),
    EXPENSE_CHECKIN(29,ExpenseDashboardActivity.class);

    //    NOTIFICATION(2, "Notifications"),
//    REQUEST(3, "Requests"),
//    EXPENSE(4, "Expenses"),
//    ROOM_SERVICE(5, "Room Service"),
//    HOUSEKEEPING(6, "Housekeeping"),
//    LAUNDRY(7, "Laundry"),
//    INTERNET(8, "Internet"),
//    RESERVATION(9, "Reservations"),
//    COMMUTE(10, "Commute"),
//    MAINTENANCE(11, "Maintenance"),
//    MESSAGE(12, "Message"),
//    AIRPORT_DROP(13, "Airport Drop"),
//    LATE_CHECKOUT(14, "Late Check-out"),
//    BELL_BOY(15, "Bell Boy"),
//    FEEDBACK_AND_SUGGESTION(16, "Feedback & Suggestion"),
//    HOTEL_INFO(17, "Hotel Info"),
//    HOTEL_INFO_STAYS(18, "Hotel Info"),
//    HOUSE_RULES(19, "House Rules"),
//    TV_ENTERTAINMENT(20, "Tv/EntertainmentResponseApi"),
//    CITY_GUIDE(21, "City Guide"),
//    COMPLEMENTRY_SERVICE(22, "Complementry Service"),
//    IN_HOTEL_NAVIGATION(23, "In Hotel Navigation");
//    EXPENSE_CHECKOUT(24, "Expense");
//    EARLY_CHECK_IN(25,"Early Check-In"),
//
//    MESSAGE_CURRENT_STAY(26,"Message"),
//
//    NOTIFICATIONS_CHECKIN(27,"Notifications"),
//
//    REQUESTS_CHECKIN(28,"Requests"),
//
//    EXPENSE_CHECKIN(29,"Expenses")


    private int code;
    private Class classType;

    ServiceType(int code, Class classType) {
        this.code = code;
        this.classType = classType;
    }

    public int getCode() {
        return code;
    }

    public Class getClassType() {
        return classType;
    }

    public static Class getTargetClassByServiceType(int value) {
        for (ServiceType type : ServiceType.values()) {
            if (type.code == value) {
                return type.classType;
            }
        }
        return null;
    }

    public static ServiceType getServiceTypeByCode(int value) {
        for (ServiceType type : ServiceType.values()) {
            if (type.getCode() == value) {
                return type;
            }
        }
        return null;
    }
}
