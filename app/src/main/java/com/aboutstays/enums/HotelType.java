package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

public enum HotelType {

	ONE_STAR(1, "1 Star"), TWO_STAR(2, "2 Star"), THREE_STAR(3, "3 Star"), FOUR_STAR(4, "4 Star"), FIVE_STAR(5, "5 Star"), SIX_STAR(6, "6 Star"), SEVEN_STAR(7, "7 Star"), PREMIUM(11, "Premium");
	
	private int code;
	private String displayName;

	public String getDisplayName() {
		return displayName;
	}


	public int getCode() {
		return code;
	}

	private HotelType(int code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	public static boolean matchHotelType(int value){
		if(findHotelByType(value)!=null)
			return true;
		return false;
	}
	
	public static HotelType findHotelByType(int value){
		for(HotelType type : HotelType.values()){
			if(type.code == value){
				return type;
			}
		}
		return null;
	}

	public static List<Integer> getTypeList() {
		List<Integer> typeList = new ArrayList<>();
		for(HotelType type : HotelType.values()){
			typeList.add(type.code);
		}
		return typeList;
	}

	public static int getDefaultValue() {
		return THREE_STAR.code;
	}
	
}