package com.aboutstays.enums;

import com.aboutstays.R;

public enum LaundryItemType {


    WASH_IRON(1, "Wash & Iron", R.mipmap.wash),

    DRY_CLEANING(2, "Dry Cleaning", R.mipmap.dry),

    IRON(3, "Iron", R.mipmap.iron);


    private int code;
    private int imageUrl;
    private String displayName;

    public int getImageUrl() {
        return imageUrl;
    }

    public int getCode() {

        return code;

    }

    public String getDisplayName() {

        return displayName;

    }

    private LaundryItemType(int code, String displayName, int imageUrl) {

        this.code = code;
        this.imageUrl = imageUrl;
        this.displayName = displayName;

    }


    public static LaundryItemType findByCode(Integer code) {

        if (code == null)

            return null;

        for (LaundryItemType itemType : LaundryItemType.values()) {

            if (itemType.code == code) {

                return itemType;

            }

        }

        return null;

    }

}