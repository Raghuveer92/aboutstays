package com.aboutstays.enums;

import com.aboutstays.R;

/**
 * Created by neeraj on 13/4/17.
 */

public enum Preferences {

    QUIET_ROOM(1, "Quiet Room"), CLOSE_TO_ELEVATOR(2, "Closer to elevator"), NON_SMOKING(3, "Non Smoking"), SMOKING(4, "Smoking"), WITH_BALCONY(5, "With Balcony"), WITH_BATH(6, "With Bath"),
    HIGHER_FLOORS(7, "Higher Floors"), LOWER_FLOORS(8, "Lower Floors"), TWIN_BEDS(9, "Twin Beds"), EXTERNAL_VIEW(10, "External View");

    private Integer code;
    private String prefName;

    public String getPrefName() {
        return prefName;
    }

    private Preferences(Integer code, String prefName) {
        this.code = code;
        this.prefName = prefName;
    }

    public Integer getCode() {
        return code;
    }

    public static Preferences findStatusByCode(Integer value){
        for(Preferences status : Preferences.values()){
            if(status.code == value){
                return status;
            }
        }
        return null;
    }
}
