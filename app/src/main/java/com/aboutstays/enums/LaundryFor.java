package com.aboutstays.enums;

public enum LaundryFor {


    MEN(1, "Men"), WOMEN(2, "Women"), ALL(3, "All");


    private int code;

    private String displayName;


    public int getCode() {

        return code;

    }


    public String getDisplayName() {

        return displayName;

    }


    private LaundryFor(int code, String displayName) {

        this.code = code;

        this.displayName = displayName;

    }


    public static LaundryFor getByCode(Integer code) {

        if (code == null)

            return null;

        for (LaundryFor laundryFor : LaundryFor.values()) {

            if (laundryFor.code == code) {

                return laundryFor;

            }

        }

        return null;

    }

}