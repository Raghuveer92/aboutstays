package com.aboutstays.enums;

import com.aboutstays.R;

public enum SpicyType {


    NON_SPICY(1, R.mipmap.none_spicy, "Non-spicy"), LESS_SPICY(2, R.mipmap.less_spicy, "Less-spicy"), SPICY(3, R.mipmap.spicy, "Spicy"),
    MORE_SPLICY(4, R.mipmap.more_spicy, "More-spicy"), NONE(5, R.mipmap.none_spicy, "None");


    private int code;
    private int imageUrl;
    private String spicyType;


    public void setCode(int code) {
        this.code = code;
    }

    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSpicyType() {
        return spicyType;
    }

    private SpicyType(int code, int imageUrl, String spicyType) {

        this.code = code;
        this.imageUrl = imageUrl;
        this.spicyType = spicyType;

    }


    public int getCode() {

        return code;

    }


    public static SpicyType findStatusByCode(int value) {

        for (SpicyType status : SpicyType.values()) {

            if (status.code == value) {

                return status;

            }

        }

        return null;

    }


}