package com.aboutstays.enums;

import com.aboutstays.R;

public enum MenuItemType {


    MINI_BAR(1, "Mini Bar", R.mipmap.minibar),

    BREAKFAST(2, "Breakfast", R.mipmap.breakfast),

    LUNCH(3, "Lunch", R.mipmap.lunch_red),

    DINNER(4, "Dinner", R.mipmap.dinner),

    DESSERTS(5, "Desserts", R.mipmap.dessert),

    ALL_DAY(6, "All Day", R.mipmap.all_day),

    BAKERY_CAFE(7, "Bakery & Cafe", R.mipmap.bakery),

    BEVERAGES_DRINKS(8, "Beverages & Drinks", R.mipmap.beverage);


    private int code;

    private String displayName;

    private int imageUrl;


    public int getCode() {

        return code;

    }


    public String getDisplayName() {

        return displayName;

    }

    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }

    private MenuItemType(int code, String displayName, int imageUrl) {

        this.code = code;

        this.displayName = displayName;

        this.imageUrl = imageUrl;

    }


    public static MenuItemType getByCode(Integer code) {

        if (code == null)

            return null;

        for (MenuItemType itemType : MenuItemType.values()) {

            if (itemType.code == code)

                return itemType;

        }

        return null;

    }

}