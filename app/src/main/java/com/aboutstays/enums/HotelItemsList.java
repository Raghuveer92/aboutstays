package com.aboutstays.enums;

import android.content.Context;

import com.aboutstays.R;
import com.aboutstays.model.HotelNameModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neeraj on 14/4/17.
 */

public enum HotelItemsList {

    GENERAL_INFO(R.string.general_info, R.mipmap.information_icon, 0),
    LOCATION_CONTACT(R.string.location_n_contact, R.mipmap.location, 1),
    REVIEW_RATING(R.string.review_rating, R.mipmap.review_rate, 2),
    ROOMS(R.string.rooms, R.mipmap.rooms, 3),
    RESTAURANTS_CUISIONS(R.string.restaurants_cuisions, R.mipmap.restaurants, 4),
    HEALTH(R.string.health_beauty, R.mipmap.health_beauty, 5),
    SPORTS_ENTERTAINMENT(R.string.sports_entertainment, R.mipmap.sports, 6),
    BUSINESS_PARTIES(R.string.business_parties, R.mipmap.business, 7),
    SHOPES_STORE(R.string.shopes_store, R.mipmap.shopes, 8),
    ESSENTIALS(R.string.essential, R.mipmap.essential, 9);

    private int title, imageUrl, position;

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    HotelItemsList(int title, int imageUrl, int position) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.position = position;
    }

    public static HotelItemsList findHotelItembyType(int position) {
        for (HotelItemsList type : HotelItemsList.values()) {
            if (type.position == position) {
                return type;
            }
        }
        return null;
    }

    public static List<HotelNameModel> getHotelItemList(Context context) {
        List<HotelNameModel> hotelNameModels = new ArrayList<>();
        for (HotelItemsList type : HotelItemsList.values()) {
            HotelNameModel hotelNameModel = new HotelNameModel();
            hotelNameModel.setName(context.getString(type.getTitle()));
            hotelNameModel.setImgUrl(type.getImageUrl());
            hotelNameModels.add(hotelNameModel);
        }
        return hotelNameModels;
    }

}
