package com.aboutstays.enums;

public enum PickupStatus {


    INITIATED(1), APPROVED(2), CANCELLED(3);


    private Integer code;


    private PickupStatus(Integer code) {

        this.code = code;

    }


    public Integer getCode() {

        return code;

    }


    public static PickupStatus findValueByType(Integer value) {

        for (PickupStatus type : PickupStatus.values()) {

            if (type.code == value) {

                return type;

            }

        }

        return null;

    }

}