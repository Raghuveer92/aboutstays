package com.aboutstays.enums;

import com.aboutstays.R;

/**
 * Created by neeraj on 17/3/17.
 */

public enum HouseKeepingItemType {

    LENIN(1, "Lenin", R.mipmap.lenin),

    BEDROOM(2, "Bedroom", R.mipmap.bedroom_red),

    BATHROOM(3, "Bathroom", R.mipmap.bath_red),

    ROOM_CLEANING(4, "Room Cleaning", R.mipmap.room_cleaning),

    MINIBAR_REFILL(5, "Minibar Refill", R.mipmap.minibar);

    private int code;
    private int imageUrl;
    private String displayName;

    public int getImageUrl() {
        return imageUrl;
    }

    public int getCode() {

        return code;

    }

    public String getDisplayName() {

        return displayName;

    }

    private HouseKeepingItemType(int code, String displayName, int imageUrl) {

        this.code = code;
        this.imageUrl = imageUrl;
        this.displayName = displayName;

    }


    public static HouseKeepingItemType findByCode(Integer code) {

        if (code == null)

            return null;

        for (HouseKeepingItemType itemType : HouseKeepingItemType.values()) {

            if (itemType.code == code) {

                return itemType;

            }

        }

        return null;

    }

}
