package com.aboutstays.enums;

import com.aboutstays.R;

public enum FoodLabels {


    VEG(1, R.mipmap.veg_icon, "Veg"), NON_VEG(2, R.mipmap.nonveg_icon, "Non-Veg"), SEA_FOOD(3, R.mipmap.veg_icon, "Sea-Food"),
    EGG(4, R.mipmap.veg_icon, "Egg"), NONE(5, R.mipmap.veg_icon, "None");


    private int code;
    private int imageUrl;
    private String type;

    public void setCode(int code) {
        this.code = code;
    }

    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getType() {
        return type;
    }

    private FoodLabels(int code, int imageUrl, String type) {

        this.code = code;
        this.imageUrl = imageUrl;
        this.type = type;

    }


    public int getCode() {

        return code;

    }


    public static FoodLabels findStatusByCode(int value) {

        for (FoodLabels status : FoodLabels.values()) {

            if (status.code == value) {

                return status;

            }

        }

        return null;

    }


}