package com.aboutstays.enums;

/**
 * Created by neeraj on 16/3/17.
 */

public enum  CartType {
    FOOD_ITEM(1, "Food Item"), HOUSEKEEPING(2, "Housekeeping"), LAUNDRY(3, "Laundry"), INTERNET(4, "Internet");

    private int code;
    private String cartName;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getCartName() {
        return cartName;
    }

    public void setCartName(String carName) {
        this.cartName = carName;
    }

    private CartType(int code, String carName) {
        this.code = code;
        this.cartName = carName;
    }

    public static CartType findCarbyType(int value){
        for(CartType type : CartType.values()){
            if(type.code == value){
                return type;
            }
        }
        return null;
    }
}
