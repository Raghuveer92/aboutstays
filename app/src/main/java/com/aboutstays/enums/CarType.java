package com.aboutstays.enums;

/**
 * Created by neeraj on 6/3/17.
 */

public enum CarType {

    SEDAN(1, "Sedan"),
    SUV(2, "Suv"),
    HENCHBACK(3, "Henchback"),
    PREMIUM(4, "Premium");

    private int code;
    private String carName;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    private CarType(int code, String carName) {
        this.code = code;
        this.carName = carName;
    }

    public static CarType findCarbyType(int value){
        for(CarType type : CarType.values()){
            if(type.code == value){
                return type;
            }
        }
        return null;
    }

}
