package com.aboutstays.enums;

/**
 * Created by neeraj on 16/3/17.
 */

public enum  CartOperation {

    ADD(1, "Add"), DELETE(2, "Delete"), MODIFY(3, "Modify");

    private int code;
    private String cartOperationName;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getCartOperationName() {
        return cartOperationName;
    }

    public void setCartOperationName(String carName) {
        this.cartOperationName = carName;
    }

    private CartOperation(int code, String carName) {
        this.code = code;
        this.cartOperationName = carName;
    }

    public static CartOperation findCarbyType(int value){
        for(CartOperation type : CartOperation.values()){
            if(type.code == value){
                return type;
            }
        }
        return null;
    }

}
