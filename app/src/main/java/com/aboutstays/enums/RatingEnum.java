package com.aboutstays.enums;

import com.aboutstays.R;

/**
 * Created by ajay on 26/12/16.
 */

public enum RatingEnum {

    STAR_5(R.color.star_5),
    STAR_4(R.color.star_4),
    STAR_3(R.color.star_3),
    STAR_2(R.color.star_2),
    STAR_1(R.color.star_1);

    private int colorId;

    public int getColorId() {
        return colorId;
    }

    RatingEnum(int colorId){
        this.colorId = colorId;
    }

    public static int getWidthAccordingToValue(int maxWidth, int ratingCount, int maxValue) {
        return (ratingCount*maxWidth)/maxValue;
    }
}

