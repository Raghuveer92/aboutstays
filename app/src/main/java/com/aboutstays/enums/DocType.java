package com.aboutstays.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neeraj on 22/2/17.
 */

public enum DocType {

    AADHAR_CARD(1, "Aadhar Card"), DRIVING_LICENSE(2, "Driving License"), PAN_CARD(3, "Pan Card"), PASSPORT(4, "Passport"), VOTER_CARD(5, "Voter Card");

    private int code;
    private String docName;

    public String getDocName() {
        return docName;
    }

    public int getCode() {
        return code;
    }

    private DocType(int code, String docName){
        this.code = code;
        this.docName = docName;
    }

    public static DocType findDocByType(int value){
        for(DocType type : DocType.values()){
            if(type.code == value){
                return type;
            }
        }
        return null;
    }

    public static List<Integer> getTypeList() {
        List<Integer> typeList = new ArrayList<>();
        for(DocType type : DocType.values()){
            typeList.add(type.code);
        }
        return typeList;
    }
}
