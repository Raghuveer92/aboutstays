package com.aboutstays.enums;

import android.content.Context;

import com.aboutstays.R;
import com.aboutstays.model.initiate_checkIn.CheckinModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neeraj on 13/4/17.
 */

public enum CheckInDetails {

    PERSONAL_DETAIL(R.string.personal_info, R.mipmap.personal_detail, 0,true),
    IDENTITY_DOC(R.string.id_verification, R.mipmap.identity, 1,true),
    PREFERANSE(R.string.pref, R.mipmap.preference, 2,false),
    SPECIAL_REQUEST(R.string.spcl_req, R.mipmap.special_request, 3,false),
    EARLY_CHECKIN(R.string.early_checkIn, R.mipmap.early_checkin, 4,false);
//    LOYALTY_PROGRAM(R.string.loyal_program, R.mipmap.loyalty, 5,false),
//    COMPANY(R.string.company, R.mipmap.company, 6,false),


    private int title, imageUrl, position;
    private boolean isMendatry;

    CheckInDetails(int title, int imageUrl, int position,  boolean isMendatry) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.position = position;
        this.isMendatry = isMendatry;
    }

    public int getTitle() {
        return title;
    }

    public int getImageUrl() {
        return imageUrl;
    }

    public int getPosition() {
        return position;
    }



    public static CheckInDetails finCheckInDetailsbyType(int position){
        for(CheckInDetails type : CheckInDetails.values()){
            if(type.position == position){
                return type;
            }
        }
        return null;
    }

    public static List<CheckinModel> getCheckInDetailList(Context context){
        List<CheckinModel> checkinModelList = new ArrayList<>();
        for(CheckInDetails type : CheckInDetails.values()){
            CheckinModel checkinModel = new CheckinModel();
            checkinModel.setName(context.getString(type.getTitle()));
            checkinModel.setImageUrl(type.getImageUrl());
            checkinModel.setMendatry(type.isMendatry);
            checkinModelList.add(checkinModel);
        }
        return checkinModelList;
    }

}
