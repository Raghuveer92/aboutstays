package com.aboutstays.enums;

/**
 * Created by neeraj on 16/2/17.
 */

public enum  Partnership {
    PARTNER(1),
    NON_PARTNER(0);

    private int code;

    public int getCode() {
        return code;
    }

    private Partnership(int code) {
        this.code = code;
    }

    public static Partnership findPartnerByType(int value){
        for(Partnership type : Partnership.values()){
            if(type.code == value){
                return type;
            }
        }
        return null;
    }
}
