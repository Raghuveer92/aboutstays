package com.aboutstays.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by admin on 4/20/17.
 */

public class StayCartUpdateReceiver extends BroadcastReceiver {
    public static final String ACTION_UPDATE_CART = "StayCartUpdateReceiver.action_update_cart";
    public static final String ACTION_UPDATE_CLEAR = "StayCartUpdateReceiver.action_cart_clear";
    private OnCartUpdateListener onCartUpdateListener;

    public StayCartUpdateReceiver(OnCartUpdateListener onCartUpdateListener) {
        this.onCartUpdateListener = onCartUpdateListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        switch (action) {
            case ACTION_UPDATE_CART:
                onCartUpdateListener.onCartUpdate();
                break;
            case ACTION_UPDATE_CLEAR:
                onCartUpdateListener.onCartClear();
                break;
        }
    }
    public static void callOnUpdateCart(Context context){
        Intent intent=new Intent(ACTION_UPDATE_CART);
        context.sendBroadcast(intent);
    }
    public static void callOnClear(Context context){
        Intent intent=new Intent(ACTION_UPDATE_CLEAR);
        context.sendBroadcast(intent);
    }

    public interface OnCartUpdateListener {
        void onCartUpdate();
        void onCartClear();
    }
}
