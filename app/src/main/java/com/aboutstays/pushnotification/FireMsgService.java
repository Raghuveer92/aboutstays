package com.aboutstays.pushnotification;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.aboutstays.activity.ActivitySplash;
import com.aboutstays.model.push_notification.PushNotificationData;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;


public class FireMsgService extends FirebaseMessagingService {
    public static final String TAG = "FCM";
    private int id;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Map<String, String> data = remoteMessage.getData();
        if (data != null) {
            JSONObject jsonObject = new JSONObject();
            for (Map.Entry<String, String> stringStringEntry : data.entrySet()) {
                try {
                    jsonObject.put(stringStringEntry.getKey(), stringStringEntry.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            handleFCMData(jsonObject.toString());
        }

        RemoteMessage.Notification notification = remoteMessage.getNotification();
        if (notification != null) {
            Intent intent = new Intent(this, ActivitySplash.class);
            NotificationUtil.remoteNotification(this,intent, notification.getTitle(), notification.getBody(), id++);
        }
    }

    private void handleFCMData(String object) {
        int type = 0;
        try {
            PushNotificationData pushNotificationData = new Gson().fromJson(object, PushNotificationData.class);
            if (pushNotificationData != null) {
                String typeString = pushNotificationData.getType();
                if (typeString != null && TextUtils.isDigitsOnly(typeString)) {
                    type = Integer.parseInt(typeString);
                    Intent intent = new Intent(this, ActivitySplash.class);
                    NotificationUtil.remoteNotification(this,intent, pushNotificationData.getTitle(), pushNotificationData.getDescription(), type);
                }
            }
        } catch (Exception e) {
        }
    }
}