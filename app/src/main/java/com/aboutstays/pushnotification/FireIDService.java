
package com.aboutstays.pushnotification;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class FireIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String tkn = FirebaseInstanceId.getInstance().getToken();
        Log.d("FCM","Token : "+tkn);
        Preferences.saveData(AppConstants.PREF_KEYS.IS_UPDATE_TOKEN,true);
        Preferences.saveData(AppConstants.PREF_KEYS.FCM_TOKEN,tkn);
    }
}