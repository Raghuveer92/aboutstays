package com.aboutstays.rest.response;

import com.aboutstays.model.complementaryServices.ComplementaryServiceData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by aman on 22/04/17.
 */

public class GetComplementaryServiceResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private ComplementaryServiceData data;

    public ComplementaryServiceData getData() {
        return data;
    }

    public void setData(ComplementaryServiceData data) {
        this.data = data;
    }
}
