package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by aman on 22/03/17.
 */

public class TimeBasedExpenseItem extends ExpenseItem implements Serializable {

    @SerializedName("created")
    @Expose
    private Long created;
    @SerializedName("subExpenseItemList")
    @Expose
    private List<ExpenseItem> subExpenseItemList;

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public List<ExpenseItem> getSubExpenseItemList() {
        return subExpenseItemList;
    }

    public void setSubExpenseItemList(List<ExpenseItem> subExpenseItemList) {
        this.subExpenseItemList = subExpenseItemList;
    }
}
