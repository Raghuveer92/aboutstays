package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by aman on 27/03/17.
 */

public class CityGuideItemData implements Serializable{

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("hotelId")
        @Expose
        private String hotelId;
        @SerializedName("cityGuideId")
        @Expose
        private String cityGuideId;
        @SerializedName("cityGuideItemType")
        @Expose
        private Integer cityGuideItemType;
        @SerializedName("generalInfo")
        @Expose
        private CityGuideGeneralInfo generalInfo;
        @SerializedName("openingTime")
        @Expose
        private String openingTime;
        @SerializedName("closingTime")
        @Expose
        private String closingTime;
        @SerializedName("distance")
        @Expose
        private Double distance;
        @SerializedName("destinationType")
        @Expose
        private String destinationType;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("travelOptionsList")
        @Expose
        private List<String> travelOptionsList;
        @SerializedName("activityList")
        @Expose
        private List<String> activityList;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getHotelId() {
            return hotelId;
        }

        public void setHotelId(String hotelId) {
            this.hotelId = hotelId;
        }

        public String getCityGuideId() {
            return cityGuideId;
        }

        public void setCityGuideId(String cityGuideId) {
            this.cityGuideId = cityGuideId;
        }

        public Integer getCityGuideItemType() {
            return cityGuideItemType;
        }

        public void setCityGuideItemType(Integer cityGuideItemType) {
            this.cityGuideItemType = cityGuideItemType;
        }

        public CityGuideGeneralInfo getGeneralInfo() {
            return generalInfo;
        }

        public void setGeneralInfo(CityGuideGeneralInfo generalInfo) {
            this.generalInfo = generalInfo;
        }

        public String getOpeningTime() {
            return openingTime;
        }

        public void setOpeningTime(String openingTime) {
            this.openingTime = openingTime;
        }

        public String getClosingTime() {
            return closingTime;
        }

        public void setClosingTime(String closingTime) {
            this.closingTime = closingTime;
        }

        public Double getDistance() {
            return distance;
        }

        public void setDistance(Double distance) {
            this.distance = distance;
        }

        public String getDestinationType() {
            return destinationType;
        }

        public void setDestinationType(String destinationType) {
            this.destinationType = destinationType;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<String> getTravelOptionsList() {
            return travelOptionsList;
        }

        public void setTravelOptionsList(List<String> travelOptionsList) {
            this.travelOptionsList = travelOptionsList;
        }

        public List<String> getActivityList() {
            return activityList;
        }

        public void setActivityList(List<String> activityList) {
            this.activityList = activityList;
        }

    public double distance(Double lat1, Double lon1, Double lat2, Double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

}
