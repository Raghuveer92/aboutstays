package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by aman on 22/03/17.
 */

public class GetDateWiseExpenseResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private DateWiseExpenseData data;

    public DateWiseExpenseData getData() {
        return data;
    }

    public void setData(DateWiseExpenseData data) {
        this.data = data;
    }
}
