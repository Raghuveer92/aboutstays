package com.aboutstays.rest.response;

import com.aboutstays.model.currentStays.GetAllServicesData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by aman on 22/04/17.
 */

public class GetAllServicesResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private GetAllServicesData data;

    public GetAllServicesData getData() {
        return data;
    }

    public void setData(GetAllServicesData data) {
        this.data = data;
    }
}
