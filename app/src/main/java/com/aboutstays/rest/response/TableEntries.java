package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mrnee on 5/9/2017.
 */

public class TableEntries implements Serializable{

    @SerializedName("titles")
    @Expose
    private Titles titles;
    @SerializedName("rows")
    @Expose
    private List<RowDashboard> rows = null;

    public Titles getTitles() {
        return titles;
    }

    public void setTitles(Titles titles) {
        this.titles = titles;
    }

    public List<RowDashboard> getRows() {
        return rows;
    }

    public void setRows(List<RowDashboard> rows) {
        this.rows = rows;
    }


}
