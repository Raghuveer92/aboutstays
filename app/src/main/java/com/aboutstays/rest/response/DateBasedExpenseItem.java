package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aman on 22/03/17.
 */

public class DateBasedExpenseItem {

    @SerializedName("dateWiseTotal")
    @Expose
    private Double dateWiseTotal;
    @SerializedName("listTimeBasedExpenseItem")
    @Expose
    private List<TimeBasedExpenseItem> listTimeBasedExpenseItem = null;

    public Double getDateWiseTotal() {
        return dateWiseTotal;
    }

    public void setDateWiseTotal(Double dateWiseTotal) {
        this.dateWiseTotal = dateWiseTotal;
    }

    public List<TimeBasedExpenseItem> getListTimeBasedExpenseItem() {
        return listTimeBasedExpenseItem;
    }

    public void setListTimeBasedExpenseItem(List<TimeBasedExpenseItem> listTimeBasedExpenseItem) {
        this.listTimeBasedExpenseItem = listTimeBasedExpenseItem;
    }
}

