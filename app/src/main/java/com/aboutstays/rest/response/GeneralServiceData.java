package com.aboutstays.rest.response;

import android.support.annotation.NonNull;

import com.aboutstays.model.BaseAdapterModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by neeraj on 1/3/17.
 */

public class GeneralServiceData implements Serializable, Comparable{

    @SerializedName("activatedImageUrl")
    @Expose
    private String activatedImageUrl;
    @SerializedName("unactivatedImageUrl")
    @Expose
    private String unactivatedImageUrl;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("hsId")
    @Expose
    private String hsId;
    @SerializedName("numerable")
    @Expose
    private Boolean numerable;
    @SerializedName("tickable")
    @Expose
    private Boolean tickable;
    @SerializedName("serviceType")
    @Expose
    private Integer serviceType;
    @SerializedName("uiOrder")
    @Expose
    private int uiOrder;
    @SerializedName("generalInfo")
    @Expose
    private boolean generalInfo = false;

    public int getUiOrder() {
        return uiOrder;
    }

    public void setUiOrder(int uiOrder) {
        this.uiOrder = uiOrder;
    }

    public boolean isGeneralInfo() {
        return generalInfo;
    }

    public void setGeneralInfo(boolean generalInfo) {
        this.generalInfo = generalInfo;
    }

    public String getActivatedImageUrl() {
        return activatedImageUrl;
    }

    public void setActivatedImageUrl(String activatedImageUrl) {
        this.activatedImageUrl = activatedImageUrl;
    }

    public String getUnactivatedImageUrl() {
        return unactivatedImageUrl;
    }

    public void setUnactivatedImageUrl(String unactivatedImageUrl) {
        this.unactivatedImageUrl = unactivatedImageUrl;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHsId() {
        return hsId;
    }

    public void setHsId(String hsId) {
        this.hsId = hsId;
    }

    public Boolean getNumerable() {
        return numerable;
    }

    public void setNumerable(Boolean numerable) {
        this.numerable = numerable;
    }

    public Boolean getTickable() {
        return tickable;
    }

    public void setTickable(Boolean tickable) {
        this.tickable = tickable;
    }

    public Integer getServiceType() {
        return serviceType;
    }

    public void setServiceType(Integer serviceType) {
        this.serviceType = serviceType;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        GeneralServiceData serviceData = (GeneralServiceData) o;
        int uiOrder = serviceData.getUiOrder();
        int uiOrder1 = getUiOrder();
        return uiOrder1 - uiOrder;
    }
}
