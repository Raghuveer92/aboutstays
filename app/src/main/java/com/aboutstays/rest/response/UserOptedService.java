package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by aman on 01/03/17.
 */

public class UserOptedService {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("stayId")
    @Expose
    private String stayId;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("refId")
    @Expose
    private String refId;
    @SerializedName("refCollectionType")
    @Expose
    private Integer refCollectionType;
    @SerializedName("gsId")
    @Expose
    private String gsId;
    @SerializedName("showTick")
    @Expose
    private boolean showTick;
    @SerializedName("countToShow")
    @Expose
    private int countToShow;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStayId() {
        return stayId;
    }

    public void setStayId(String stayId) {
        this.stayId = stayId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public Integer getRefCollectionType() {
        return refCollectionType;
    }

    public void setRefCollectionType(Integer refCollectionType) {
        this.refCollectionType = refCollectionType;
    }

    public String getGsId() {
        return gsId;
    }

    public void setGsId(String gsId) {
        this.gsId = gsId;
    }

    public boolean isShowTick() {
        return showTick;
    }

    public void setShowTick(boolean showTick) {
        this.showTick = showTick;
    }

    public int getCountToShow() {
        return countToShow;
    }

    public void setCountToShow(int countToShow) {
        this.countToShow = countToShow;
    }
}
