package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aman on 28/03/17.
 */

public class RequestDashboardData {

    @SerializedName("openDashboardItems")
    @Expose
    private List<GeneralDashboardItem> openDashboardItems = null;
    @SerializedName("openCount")
    @Expose
    private int openCount;
    @SerializedName("scheduledDashboardItems")
    @Expose
    private List<GeneralDashboardItem> scheduledDashboardItems = null;
    @SerializedName("scheduleCount")
    @Expose
    private int scheduleCount;
    @SerializedName("closedDashboardItems")
    @Expose
    private List<GeneralDashboardItem> closedDashboardItems = null;
    @SerializedName("closedCount")
    @Expose
    private int closedCount;

    public List<GeneralDashboardItem> getOpenDashboardItems() {
        return openDashboardItems;
    }

    public void setOpenDashboardItems(List<GeneralDashboardItem> openDashboardItems) {
        this.openDashboardItems = openDashboardItems;
    }

    public List<GeneralDashboardItem> getScheduledDashboardItems() {
        return scheduledDashboardItems;
    }

    public void setScheduledDashboardItems(List<GeneralDashboardItem> scheduledDashboardItems) {
        this.scheduledDashboardItems = scheduledDashboardItems;
    }

    public List<GeneralDashboardItem> getClosedDashboardItems() {
        return closedDashboardItems;
    }

    public void setClosedDashboardItems(List<GeneralDashboardItem> closedDashboardItems) {
        this.closedDashboardItems = closedDashboardItems;
    }

    public int getOpenCount() {
        return openCount;
    }

    public void setOpenCount(int openCount) {
        this.openCount = openCount;
    }

    public int getScheduleCount() {
        return scheduleCount;
    }

    public void setScheduleCount(int scheduleCount) {
        this.scheduleCount = scheduleCount;
    }

    public int getClosedCount() {
        return closedCount;
    }

    public void setClosedCount(int closedCount) {
        this.closedCount = closedCount;
    }
}
