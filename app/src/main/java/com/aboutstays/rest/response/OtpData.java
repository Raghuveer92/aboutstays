package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by mrnee on 5/13/2017.
 */

public class OtpData implements Serializable {

    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("debugMode")
    @Expose
    private boolean debugMode;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public boolean getDebugMode() {
        return debugMode;
    }

    public void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
    }
}
