package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aman on 27/03/17.
 */

public class ParentCategoryItems {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("listChildCategory")
    @Expose
    private List<ChildCategoryItem> listChildCategory = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ChildCategoryItem> getListChildCategory() {
        return listChildCategory;
    }

    public void setListChildCategory(List<ChildCategoryItem> listChildCategory) {
        this.listChildCategory = listChildCategory;
    }
}
