package com.aboutstays.rest.response;

import com.aboutstays.model.hotels.GeneralInformation;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by aman on 03/03/17.
 */

public class GetAirportServiceResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private SupportedAirportsList data;

    public SupportedAirportsList getData() {
        return data;
    }

    public void setData(SupportedAirportsList data) {
        this.data = data;
    }
}
