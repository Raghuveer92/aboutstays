package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class DashboardOrderDetailsApiResponce extends BaseApiResponse{

@SerializedName("data")
@Expose
private DashBoardDetailData data;

public DashBoardDetailData getData() {
return data;
}

public void setData(DashBoardDetailData data) {
this.data = data;
}

}