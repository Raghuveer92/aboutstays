package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aman on 03/03/17.
 */

public class SupportedAirports {

    @SerializedName("airportName")
    @Expose
    private String airportName;
    @SerializedName("carDetailsAndPriceList")
    @Expose
    private List<CarDetailsAndPrice> carDetailsAndPriceList = null;

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public List<CarDetailsAndPrice> getCarDetailsAndPriceList() {
        return carDetailsAndPriceList;
    }

    public void setCarDetailsAndPriceList(List<CarDetailsAndPrice> carDetailsAndPriceList) {
        this.carDetailsAndPriceList = carDetailsAndPriceList;
    }
}
