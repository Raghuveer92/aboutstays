package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class GetHotelGeneralServicesResponse extends BaseApiResponse {

@SerializedName("data")
@Expose
private GeneralServiceList data;


public GeneralServiceList getData() {
return data;
}

public void setData(GeneralServiceList data) {
this.data = data;
}


}