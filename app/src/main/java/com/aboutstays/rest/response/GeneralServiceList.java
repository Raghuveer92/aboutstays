package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by neeraj on 1/3/17.
 */

public class GeneralServiceList {

        @SerializedName("listGS")
        @Expose
        private List<GeneralServiceData> listGS = null;

        public List<GeneralServiceData> getListGS() {
            return listGS;
        }

        public void setListGS(List<GeneralServiceData> listGS) {
            this.listGS = listGS;
        }

}
