package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by aman on 22/03/17.
 */

public class GetCategoryBaseExpenseResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private CategoryBasedExpenseData data;

    public CategoryBasedExpenseData getData() {
        return data;
    }

    public void setData(CategoryBasedExpenseData data) {
        this.data = data;
    }
}
