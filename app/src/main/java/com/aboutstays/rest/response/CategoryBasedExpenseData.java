package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aman on 22/03/17.
 */

public class CategoryBasedExpenseData {

    @SerializedName("listServiceExpense")
    @Expose
    private List<ExpenseItem> listExpenseItem = null;
    @SerializedName("grandTotal")
    @Expose
    private double grandTotal;

    public List<ExpenseItem> getListExpenseItem() {
        return listExpenseItem;
    }

    public void setListExpenseItem(List<ExpenseItem> listExpenseItem) {
        this.listExpenseItem = listExpenseItem;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }
}

