package com.aboutstays.rest.response;

import com.aboutstays.model.lateCheckout.LateCheckoutServiceData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by aman on 06/04/17.
 */

public class GetLateCheckoutServiceResponse extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private LateCheckoutServiceData data;

    public LateCheckoutServiceData getData() {
        return data;
    }

    public void setData(LateCheckoutServiceData data) {
        this.data = data;
    }
}
