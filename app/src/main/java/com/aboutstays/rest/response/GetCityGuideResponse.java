package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by aman on 26/03/17.
 */

public class GetCityGuideResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private CityGuideData data;

    public CityGuideData getData() {
        return data;
    }

    public void setData(CityGuideData data) {
        this.data = data;
    }
}
