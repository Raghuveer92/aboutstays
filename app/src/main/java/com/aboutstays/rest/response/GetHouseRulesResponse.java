package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by aman on 27/03/17.
 */

public class GetHouseRulesResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private HouseRulesData data;

    public HouseRulesData getData() {
        return data;
    }

    public void setData(HouseRulesData data) {
        this.data = data;
    }
}
