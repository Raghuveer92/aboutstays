package com.aboutstays.rest.response;

import com.aboutstays.model.lateCheckout.RetreiveLateCheckoutData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by aman on 21/04/17.
 */

public class RetreiveLateCheckoutResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private RetreiveLateCheckoutData data;

    public RetreiveLateCheckoutData getData() {
        return data;
    }

    public void setData(RetreiveLateCheckoutData data) {
        this.data = data;
    }
}
