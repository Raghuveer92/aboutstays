package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by aman on 28/03/17.
 */

public class GetRequestDashboardResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private RequestDashboardData data;

    public RequestDashboardData getData() {
        return data;
    }

    public void setData(RequestDashboardData data) {
        this.data = data;
    }
}
