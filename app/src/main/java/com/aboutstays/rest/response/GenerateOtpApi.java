
package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class GenerateOtpApi extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private OtpData data;

    public OtpData getData() {
        return data;
    }

    public void setData(OtpData data) {
        this.data = data;
    }


}