package com.aboutstays.rest.response;

import com.aboutstays.model.feedbackAndSuggestions.FeedbackAndSuggestionsData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by aman on 09/04/17.
 */

public class GetStaysFeedbackResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private FeedbackAndSuggestionsData data;

    public FeedbackAndSuggestionsData getData() {
        return data;
    }

    public void setData(FeedbackAndSuggestionsData data) {
        this.data = data;
    }
}
