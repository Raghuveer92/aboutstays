package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aman on 03/03/17.
 */

public class SupportedAirportsList {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("supportedAirportList")
    @Expose
    private List<SupportedAirports> supportedAirportList = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<SupportedAirports> getSupportedAirportList() {
        return supportedAirportList;
    }

    public void setSupportedAirportList(List<SupportedAirports> supportedAirportList) {
        this.supportedAirportList = supportedAirportList;
    }
}
