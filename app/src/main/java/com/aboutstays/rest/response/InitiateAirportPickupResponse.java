package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by aman on 04/03/17.
 */

public class InitiateAirportPickupResponse extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private AirportPickupId data;

    public AirportPickupId getData() {
        return data;
    }

    public void setData(AirportPickupId data) {
        this.data = data;
    }
}
