package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by aman on 01/03/17.
 */

public class GetUserOptedServicesResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private List<UserOptedService> data;

    public List<UserOptedService> getData() {
        return data;
    }

    public void setData(List<UserOptedService> data) {
        this.data = data;
    }
}

