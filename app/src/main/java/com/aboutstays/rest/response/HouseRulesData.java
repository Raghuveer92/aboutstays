package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aman on 27/03/17.
 */

public class HouseRulesData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("listPGItems")
    @Expose
    private List<ParentCategoryItems> listPGItems = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ParentCategoryItems> getListPGItems() {
        return listPGItems;
    }

    public void setListPGItems(List<ParentCategoryItems> listPGItems) {
        this.listPGItems = listPGItems;
    }
}
