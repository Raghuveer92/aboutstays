package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by aman on 22/03/17.
 */

public class DateWiseExpenseData {

    @SerializedName("dateVsExpenseItemList")
    @Expose
    private LinkedHashMap<String, DateBasedExpenseItem> dateVsExpenseItemList;
    @SerializedName("grandTotal")
    @Expose
    private Double grandTotal;

    public LinkedHashMap<String, DateBasedExpenseItem> getDateVsExpenseItemList() {
        return dateVsExpenseItemList;
    }

    public void setDateVsExpenseItemList(LinkedHashMap<String, DateBasedExpenseItem> dateVsExpenseItemList) {
        this.dateVsExpenseItemList = dateVsExpenseItemList;
    }

    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }
}
