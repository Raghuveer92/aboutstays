package com.aboutstays.rest.response;

import com.aboutstays.model.hotels.GeneralInformation;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aman on 14/03/17.
 */

public class GeneralInfoDataWithHotelType {

    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("hotelType")
    @Expose
    private Integer hotelType;
    @SerializedName("generalInformation")
    @Expose
    private GeneralInformation generalInformation;

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public Integer getHotelType() {
        return hotelType;
    }

    public void setHotelType(Integer hotelType) {
        this.hotelType = hotelType;
    }

    public GeneralInformation getGeneralInformation() {
        return generalInformation;
    }

    public void setGeneralInformation(GeneralInformation generalInformation) {
        this.generalInformation = generalInformation;
    }
}
