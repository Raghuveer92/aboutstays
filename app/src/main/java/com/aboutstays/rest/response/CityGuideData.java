package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aman on 26/03/17.
 */

public class CityGuideData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("generalInfo")
    @Expose
    private CityGuideGeneralInfo generalInfo;
    @SerializedName("itemByTypeList")
    @Expose
    private List<GeneralItemType> cityGuideItemTypeList = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CityGuideGeneralInfo getGeneralInfo() {
        return generalInfo;
    }

    public void setGeneralInfo(CityGuideGeneralInfo generalInfo) {
        this.generalInfo = generalInfo;
    }

    public List<GeneralItemType> getCityGuideItemTypeList() {
        return cityGuideItemTypeList;
    }

    public void setCityGuideItemTypeList(List<GeneralItemType> cityGuideItemTypeList) {
        this.cityGuideItemTypeList = cityGuideItemTypeList;
    }
}
