package com.aboutstays.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by neeraj on 29/3/17.
 */

public class DashBoardDetailData implements Serializable{

    @SerializedName("requestedTime")
    @Expose
    private long requestedTime;
    @SerializedName("lastUpdatedTime")
    @Expose
    private long lastUpdatedTime;
    @SerializedName("requestType")
    @Expose
    private String requestType;
    @SerializedName("requestStatus")
    @Expose
    private String requestStatus;
    @SerializedName("listGeneralItemDetails")
    @Expose
    private List<ListGeneralItemDetail> listGeneralItemDetails = null;
    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("serviceCharge")
    @Expose
    private double serviceCharge;
    @SerializedName("editable")
    @Expose
    private boolean editable;
    @SerializedName("scheduledTime")
    @Expose
    private long scheduledTime;
    @SerializedName("tableEntries")
    @Expose
    private TableEntries tableEntries;

    public TableEntries getTableEntries() {
        return tableEntries;
    }

    public void setTableEntries(TableEntries tableEntries) {
        this.tableEntries = tableEntries;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public long getRequestedTime() {
        return requestedTime;
    }

    public void setRequestedTime(long requestedTime) {
        this.requestedTime = requestedTime;
    }

    public long getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(long lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public List<ListGeneralItemDetail> getListGeneralItemDetails() {
        return listGeneralItemDetails;
    }

    public void setListGeneralItemDetails(List<ListGeneralItemDetail> listGeneralItemDetails) {
        this.listGeneralItemDetails = listGeneralItemDetails;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public double getServiceCharge() {
        return serviceCharge;
    }

    public void setServiceCharge(double serviceCharge) {
        this.serviceCharge = serviceCharge;
    }

    public long getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(long scheduledTime) {
        this.scheduledTime = scheduledTime;
    }
}
