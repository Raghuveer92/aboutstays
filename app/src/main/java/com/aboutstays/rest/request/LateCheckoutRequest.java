package com.aboutstays.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aman on 06/04/17.
 */

public class LateCheckoutRequest extends BaseServiceRequest {

    @SerializedName("gsId")
    @Expose
    private String gsId;
    @SerializedName("deliveryTime")
    @Expose
    private long deliveryTime;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("requestedNow")
    @Expose
    private Boolean requestedNow;
    @SerializedName("checkoutTime")
    @Expose
    private long checkoutTime;
    @SerializedName("checkoutDate")
    @Expose
    private long checkoutDate;
    @SerializedName("price")
    @Expose
    private double price;
    @SerializedName("id")
    @Expose
    private String id;

    public String getGsId() {
        return gsId;
    }

    public void setGsId(String gsId) {
        this.gsId = gsId;
    }

    public long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getRequestedNow() {
        return requestedNow;
    }

    public void setRequestedNow(Boolean requestedNow) {
        this.requestedNow = requestedNow;
    }

    public long getCheckoutTime() {
        return checkoutTime;
    }

    public void setCheckoutTime(long checkoutTime) {
        this.checkoutTime = checkoutTime;
    }

    public long getCheckoutDate() {
        return checkoutDate;
    }

    public void setCheckoutDate(long checkoutDate) {
        this.checkoutDate = checkoutDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
