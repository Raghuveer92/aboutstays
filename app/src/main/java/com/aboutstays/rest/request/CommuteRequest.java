package com.aboutstays.rest.request;

import com.aboutstays.rest.response.CarDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommuteRequest {

    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("staysId")
    @Expose
    private String staysId;
    @SerializedName("gsId")
    @Expose
    private String gsId;
    @SerializedName("deliveryTime")
    @Expose
    private long deliveryTime;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("requestedNow")
    @Expose
    private Boolean requestedNow;
    @SerializedName("packageName")
    @Expose
    private String packageName;
    @SerializedName("carDetails")
    @Expose
    private CarDetails carDetails;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("deliverTimeString")
    @Expose
    private String deliverTimeString;

    public String getDeliverTimeString() {
        return deliverTimeString;
    }

    public void setDeliverTimeString(String deliverTimeString) {
        this.deliverTimeString = deliverTimeString;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStaysId() {
        return staysId;
    }

    public void setStaysId(String staysId) {
        this.staysId = staysId;
    }

    public String getGsId() {
        return gsId;
    }

    public void setGsId(String gsId) {
        this.gsId = gsId;
    }

    public long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getRequestedNow() {
        return requestedNow;
    }

    public void setRequestedNow(Boolean requestedNow) {
        this.requestedNow = requestedNow;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public CarDetails getCarDetails() {
        return carDetails;
    }

    public void setCarDetails(CarDetails carDetails) {
        this.carDetails = carDetails;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}