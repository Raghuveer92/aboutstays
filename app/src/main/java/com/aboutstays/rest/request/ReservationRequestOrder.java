package com.aboutstays.rest.request;

import com.aboutstays.model.internet.InternetPackList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReservationRequestOrder extends BaseServiceRequest{

    @SerializedName("gsId")
    @Expose
    private String gsId;
    @SerializedName("deliveryTime")
    @Expose
    private long deliveryTime;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("requestedNow")
    @Expose
    private Boolean requestedNow;
    @SerializedName("rsItemId")
    @Expose
    private String rsItemId;
    @SerializedName("price")
    @Expose
    private double price;
    @SerializedName("duration")
    @Expose
    private long duration;
    @SerializedName("type")
    @Expose
    private int type;
    @SerializedName("pax")
    @Expose
    private int pax;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("deliverTimeString")
    @Expose
    private String deliverTimeString;

    public String getDeliverTimeString() {
        return deliverTimeString;
    }

    public void setDeliverTimeString(String deliverTimeString) {
        this.deliverTimeString = deliverTimeString;
    }

    public int getPax() {
        return pax;
    }

    public void setPax(int pax) {
        this.pax = pax;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getGsId() {
        return gsId;
    }

    public void setGsId(String gsId) {
        this.gsId = gsId;
    }

    public long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getRequestedNow() {
        return requestedNow;
    }

    public void setRequestedNow(Boolean requestedNow) {
        this.requestedNow = requestedNow;
    }

    public String getRsItemId() {
        return rsItemId;
    }

    public void setRsItemId(String rsItemId) {
        this.rsItemId = rsItemId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

}