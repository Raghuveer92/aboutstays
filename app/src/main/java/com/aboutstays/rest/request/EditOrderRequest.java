package com.aboutstays.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class EditOrderRequest {

    @SerializedName("uosId")
    @Expose
    private String uosId;
    private Map<String, Integer> mapItemIdVSQuantity;

    public Map<String, Integer> getMapItemIdVSQuantity() {
        return mapItemIdVSQuantity;
    }

    public void setMapItemIdVSQuantity(Map<String, Integer> mapItemIdVSQuantity) {
        this.mapItemIdVSQuantity = mapItemIdVSQuantity;
    }

    public String getUosId() {
        return uosId;
    }

    public void setUosId(String uosId) {
        this.uosId = uosId;
    }

}