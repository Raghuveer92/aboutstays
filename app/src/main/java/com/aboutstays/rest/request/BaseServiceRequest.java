package com.aboutstays.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aman on 28/03/17.
 */

public class BaseServiceRequest {

    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("staysId")
    @Expose
    private String staysId;
    @SerializedName("userId")
    @Expose
    private String userId;

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getStaysId() {
        return staysId;
    }

    public void setStaysId(String staysId) {
        this.staysId = staysId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
