package com.aboutstays.rest.request;

import com.aboutstays.model.internet.InternetPackList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aman on 24/04/17.
 */

public class InternetOrderRequest extends BaseServiceRequest{

    @SerializedName("gsId")
    @Expose
    private String gsId;
    @SerializedName("deliveryTime")
    @Expose
    private long deliveryTime;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("requestedNow")
    @Expose
    private Boolean requestedNow;
    @SerializedName("internetPack")
    @Expose
    private InternetPackList internetPack;


    public String getGsId() {
        return gsId;
    }

    public void setGsId(String gsId) {
        this.gsId = gsId;
    }

    public long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getRequestedNow() {
        return requestedNow;
    }

    public void setRequestedNow(Boolean requestedNow) {
        this.requestedNow = requestedNow;
    }

    public InternetPackList getInternetPack() {
        return internetPack;
    }

    public void setInternetPack(InternetPackList internetPack) {
        this.internetPack = internetPack;
    }
}
