package com.aboutstays.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubmittFeedbackRequest {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("uosId")
    @Expose
    private String uosId;
    @SerializedName("timeTaken")
    @Expose
    private Integer timeTaken;
    @SerializedName("quality")
    @Expose
    private Integer quality;
    @SerializedName("comments")
    @Expose
    private String comments;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUosId() {
        return uosId;
    }

    public void setUosId(String uosId) {
        this.uosId = uosId;
    }

    public Integer getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(Integer timeTaken) {
        this.timeTaken = timeTaken;
    }

    public Integer getQuality() {
        return quality;
    }

    public void setQuality(Integer quality) {
        this.quality = quality;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

}