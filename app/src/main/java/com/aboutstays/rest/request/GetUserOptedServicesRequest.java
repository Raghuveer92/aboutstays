package com.aboutstays.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aman on 01/03/17.
 */

public class GetUserOptedServicesRequest {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("stayId")
    @Expose
    private String stayId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStayId() {
        return stayId;
    }

    public void setStayId(String stayId) {
        this.stayId = stayId;
    }
}
