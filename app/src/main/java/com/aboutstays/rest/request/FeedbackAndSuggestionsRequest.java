package com.aboutstays.rest.request;

import com.aboutstays.model.feedbackAndSuggestions.GeneralTimeTakenAndQuality;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aman on 09/04/17.
 */

public class FeedbackAndSuggestionsRequest extends BaseServiceRequest {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("gsId")
    @Expose
    private String gsId;
    @SerializedName("listFeedback")
    @Expose
    private List<GeneralTimeTakenAndQuality> listFeedbacks;
    @SerializedName("specialStaff")
    @Expose
    private String specialStaff;
    @SerializedName("comment")
    @Expose
    private String comment;

    public String getGsId() {
        return gsId;
    }

    public void setGsId(String gsId) {
        this.gsId = gsId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<GeneralTimeTakenAndQuality> getListFeedbacks() {
        return listFeedbacks;
    }

    public void setListFeedbacks(List<GeneralTimeTakenAndQuality> listFeedbacks) {
        this.listFeedbacks = listFeedbacks;
    }

    public String getSpecialStaff() {
        return specialStaff;
    }

    public void setSpecialStaff(String specialStaff) {
        this.specialStaff = specialStaff;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
