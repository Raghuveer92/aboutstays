package com.aboutstays.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintenanceRequest extends BaseServiceRequest{

    @SerializedName("gsId")
    @Expose
    private String gsId;
    @SerializedName("deliveryTime")
    @Expose
    private long deliveryTime;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("requestedNow")
    @Expose
    private Boolean requestedNow;
    @SerializedName("type")
    @Expose
    private int type;
    @SerializedName("itemName")
    @Expose
    private String itemName;
    @SerializedName("problemType")
    @Expose
    private String problemType;
    @SerializedName("deliverTimeString")
    @Expose
    private String deliverTimeString;

    public String getDeliverTimeString() {
        return deliverTimeString;
    }

    public void setDeliverTimeString(String deliverTimeString) {
        this.deliverTimeString = deliverTimeString;
    }

    public String getProblemType() {
        return problemType;
    }

    public void setProblemType(String problemType) {
        this.problemType = problemType;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getGsId() {
        return gsId;
    }

    public void setGsId(String gsId) {
        this.gsId = gsId;
    }

    public long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getRequestedNow() {
        return requestedNow;
    }

    public void setRequestedNow(Boolean requestedNow) {
        this.requestedNow = requestedNow;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}