package com.aboutstays.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class GetAirportBookingData extends BaseApiResponse{

@SerializedName("data")
@Expose
private BookingData data;

public BookingData getData() {
return data;
}

public void setData(BookingData data) {
this.data = data;
}

}