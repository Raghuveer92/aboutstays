package com.aboutstays.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aman on 21/04/17.
 */

public class SuggestHotelRequest {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("hotel")
    @Expose
    private String hotel;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }
}
