package com.aboutstays.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aman on 04/03/17.
 */

public class InitiateAirportPickupRequest {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("stayId")
    @Expose
    private String stayId;
    @SerializedName("gsId")
    @Expose
    private String gsId;
    @SerializedName("airportName")
    @Expose
    private String airportName;
    @SerializedName("airlineName")
    @Expose
    private String airlineName;
    @SerializedName("flightNumber")
    @Expose
    private String flightNumber;
    @SerializedName("pickupDateAndTime")
    @Expose
    private long pickupDateAndTime;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("carType")
    @Expose
    private Integer carType;
    @SerializedName("pickupStatus")
    @Expose
    private Integer pickupStatus;
    @SerializedName("price")
    @Expose
    private double price;
    @SerializedName("drop")
    @Expose
    private boolean drop = false;
    @SerializedName("deliverTimeString")
    @Expose
    private String deliverTimeString;

    public String getDeliverTimeString() {
        return deliverTimeString;
    }

    public void setDeliverTimeString(String deliverTimeString) {
        this.deliverTimeString = deliverTimeString;
    }

    public boolean isDrop() {
        return drop;
    }

    public void setDrop(boolean drop) {
        this.drop = drop;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getPickupStatus() {
        return pickupStatus;
    }

    public void setPickupStatus(Integer pickupStatus) {
        this.pickupStatus = pickupStatus;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getStayId() {
        return stayId;
    }

    public void setStayId(String stayId) {
        this.stayId = stayId;
    }

    public String getGsId() {
        return gsId;
    }

    public void setGsId(String gsId) {
        this.gsId = gsId;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public long getPickupDateAndTime() {
        return pickupDateAndTime;
    }

    public void setPickupDateAndTime(long pickupDateAndTime) {
        this.pickupDateAndTime = pickupDateAndTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getCarType() {
        return carType;
    }

    public void setCarType(Integer carType) {
        this.carType = carType;
    }
}
