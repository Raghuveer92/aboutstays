package com.aboutstays.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FetchReservationRequest {

    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("rsId")
    @Expose
    private String rsId;
    @SerializedName("category")
    @Expose
    private String category;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getRsId() {
        return rsId;
    }

    public void setRsId(String rsId) {
        this.rsId = rsId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}