package com.aboutstays.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.aboutstays.model.common.FetchCommonData;
import com.aboutstays.model.common.FetchProfileTitles;
import com.aboutstays.utitlity.ApiRequestGenerator;

import org.json.JSONException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.Service;
import simplifii.framework.asyncmanager.ServiceFactory;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.response.response.PrefData;
import simplifii.framework.rest.response.response.PreferanceResponceApi;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;

/**
 *
 * Created by robin on 4/29/17.
 */

public class CommonDataServices extends IntentService {

    private interface ACTIONS{
        String FETCH_PROFILE_TITLES = "fetchProfileTitles";
        String FETCH_STAY_PREFERENCES = "fetchStayPreferences";
        String FETCH_ALL_USER_DATA = "fetchAllUserData";
    }

    public CommonDataServices(){
        super("CommonDataServices");
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
//        String action = intent.getAction();
//        switch (action) {
//            case ACTIONS.FETCH_ALL_USER_DATA :
//                fetchAllUserCommonData();
//                break;
//        }
        return super.onStartCommand(intent, flags, startId);



//        String action = intent.getAction();
//        switch (action){
//            case ACTIONS.FETCH_PROFILE_TITLES:{
//                fetchProfileTitles();
//                break;
//            }
//            case ACTIONS.FETCH_STAY_PREFERENCES:{
//                fetchPreferences();
//                break;
//            }
//        }
//        return super.onStartCommand(intent, flags, startId);

    }

    private void fetchPreferences(){
        HttpParamObject httpParamObject = ApiRequestGenerator.getAllStayPref();
        Service service = ServiceFactory.getInstance(this, AppConstants.TASK_CODES.GET_STAY_PREF);
        try{
            PreferanceResponceApi preferanceResponceApi = (PreferanceResponceApi) service.getData(httpParamObject);
            if(!preferanceResponceApi.getError()){
                //Preferences already saved in parse Json method

//                PrefData prefData = preferanceResponceApi.getData();
//                String preferncesJson = JsonUtil.toJson(prefData);
//                Preferences.saveData(Preferences.KEY_PREFERENCES, preferncesJson);
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (RestException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fetchProfileTitles() {
        HttpParamObject httpParamObject = ApiRequestGenerator.fetchProfileTitles();
        Service service = ServiceFactory.getInstance(this, AppConstants.TASK_CODES.FETCH_PROFILE_TITLES);
        try{
            FetchProfileTitles fetchProfileTitles = (FetchProfileTitles) service.getData(httpParamObject);
            if(!fetchProfileTitles.getError()){
                //Prefernces already saved in parseJson method
                //Preferences.saveData(Preferences.KEY_PROFILE_TITLES, new HashSet<String>(fetchProfileTitles.getProfileTitles()));
            } else {

            }
        }catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (RestException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void fetchCommonData(Context context){
        Intent intent = new Intent(context, UserServices.class);
        intent.setAction(ACTIONS.FETCH_PROFILE_TITLES);
        context.startService(intent);

        intent.setAction(ACTIONS.FETCH_STAY_PREFERENCES);
        context.startService(intent);
    }

    public static void fetchUserCommonData(Context context) {
        Intent intent = new Intent(context, CommonDataServices.class);
        intent.setAction(ACTIONS.FETCH_ALL_USER_DATA);
        context.startService(intent);
    }




    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
//        String action = intent.getAction();
//        switch (action){
//            case ACTIONS.FETCH_PROFILE_TITLES:{
//                fetchProfileTitles();
//                break;
//            }
//            case ACTIONS.FETCH_STAY_PREFERENCES:{
//                fetchPreferences();
//                break;
//            }
//        }
        String action = intent.getAction();
        switch (action) {
            case ACTIONS.FETCH_ALL_USER_DATA :
                fetchAllUserCommonData();
                break;
        }

    }

    private void fetchAllUserCommonData() {
        HttpParamObject httpParamObject = ApiRequestGenerator.fetchUserCommonData();
        Service service = ServiceFactory.getInstance(this, AppConstants.TASK_CODES.FETCH_USER_COMMON_DATA);
        try {
            FetchCommonData fetchCommonData = (FetchCommonData) service.getData(httpParamObject);
            if(!fetchCommonData.getError()) {

            } else {

            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (RestException e) {
            e.printStackTrace();
        }
    }
}
