package com.aboutstays.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import com.aboutstays.utitlity.ApiRequestGenerator;

import org.json.JSONException;

import java.io.IOException;
import java.sql.SQLException;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.asyncmanager.Service;
import simplifii.framework.asyncmanager.ServiceFactory;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.response.response.GetUserResponse;
import simplifii.framework.rest.response.response.UserProfileData;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;


public class UserServices extends IntentService {
    private static final String ACTION_REFRESH_USER = "action_refresh_user";
    private static final String ACTION_UPDATE_USER = "action_update_user";

    public UserServices() {
        super("UserServices");
    }

    public static void startUpdateUser(Context context){
        Intent intent = new Intent(context, UserServices.class);
        intent.setAction(ACTION_UPDATE_USER);
        context.startService(intent);
    }

    public static void refreshUser(Context context){
        Intent intent = new Intent(context, UserServices.class);
        intent.setAction(ACTION_REFRESH_USER);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
           switch (action){
               case ACTION_UPDATE_USER:
                   updateUser();
                   break;
               case ACTION_REFRESH_USER:
                   break;
           }
        }
    }

    private void updateUser() {
        UserSession session = UserSession.getSessionInstance();
        if(session != null){
            String userId = session.getUserId();
            HttpParamObject httpParamObject = ApiRequestGenerator.getUserByUserId(userId);
            Service service = ServiceFactory.getInstance(this, AppConstants.TASK_CODES.GET_USER);
            try {
                GetUserResponse response = (GetUserResponse) service.getData(httpParamObject);
                if(response != null){
                    UserProfileData userData = (UserProfileData) response.getData();
                    if(userData != null){
                        UserSession.saveUserData(userData);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (RestException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


}
