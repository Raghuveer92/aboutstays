package com.aboutstays.holder;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.enums.ServiceType;
import com.aboutstays.model.GeneralServiceModel;
import com.squareup.picasso.Picasso;

import simplifii.framework.utility.AppConstants;

/**
 * Created by Neeraj on 12/29/2016.
 */

public class CheckInServiceHolder extends BaseHolder {

    TextView tvName;
    ImageView logo, ivTick;
    TextView tvCount;
    private GeneralServiceModel generalServiceModel;
    private RelativeLayout rlMain;

    public CheckInServiceHolder(View itemView) {
        super(itemView);
        tvName = (TextView) itemView.findViewById(R.id.tv_name);
        logo = (ImageView) itemView.findViewById(R.id.iv_icon);
        rlMain = (RelativeLayout) itemView.findViewById(R.id.rl_main);
        ivTick = (ImageView) itemView.findViewById(R.id.iv_icon_select);
        tvCount = (TextView) itemView.findViewById(R.id.tv_head_count);
    }

    @Override
    public void onBind(final int position, Object obj) {
        super.onBind(position, obj);
        generalServiceModel = (GeneralServiceModel) obj;

        if (generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null){
            String imgUrl;
            tvName.setText(generalServiceModel.getGeneralServiceData().getName());
            imgUrl = generalServiceModel.getGeneralServiceData().getActivatedImageUrl();
            if(generalServiceModel.isShowTick()) {
                ivTick.setVisibility(View.VISIBLE);
                tvCount.setVisibility(View.GONE);
            } else if(generalServiceModel.isShowNumber()){
                ivTick.setVisibility(View.GONE);
                tvCount.setVisibility(View.VISIBLE);
                tvCount.setText(generalServiceModel.getHeadCount() + "");
            } else {
                ivTick.setVisibility(View.GONE);
                tvCount.setVisibility(View.GONE);
            }

            if(!TextUtils.isEmpty(imgUrl)){
                Picasso.with(context).load(imgUrl)
                        .placeholder(R.drawable.progress_animation)
                        .error(R.mipmap.taj_logo2)
                        .into(logo);
            }

//            if(generalServiceModel.isActive()){
//               imgUrl = generalServiceModel.getGeneralServiceData().getActivatedImageUrl();
//                if(generalServiceModel.isUserOpted()){
//                    if(generalServiceModel.isShowTickable()){
//                        ivTick.setVisibility(View.VISIBLE);
//                        tvCount.setVisibility(View.GONE);
//                    } else if(generalServiceModel.isShowNumerable()){
//                        ivTick.setVisibility(View.GONE);
//                        tvCount.setVisibility(View.VISIBLE);
//                        tvCount.setText(generalServiceModel.getHeadCount() + "");
//                    }
//                } else{
//                    ivTick.setVisibility(View.GONE);
//                    tvCount.setVisibility(View.GONE);
//                }
//            } else{
//               imgUrl = generalServiceModel.getGeneralServiceData().getUnactivatedImageUrl();
//                ivTick.setVisibility(View.GONE);
//            }
//            if(!TextUtils.isEmpty(imgUrl)){
//                Picasso.with(context).load(imgUrl)
//                        .error(R.mipmap.taj_logo2)
//                        .into(logo);
//            }
        }


//        rlMain.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               // onEventAction(AppConstants.ACTION_TYPE.NEXT_ACTIVITY, v.getTag());
//            }
//        });

        rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             clickListener.onItemClicked(position, generalServiceModel, AppConstants.ACTION_TYPE.NEXT_ACTIVITY);
            }
        });
    }


}
