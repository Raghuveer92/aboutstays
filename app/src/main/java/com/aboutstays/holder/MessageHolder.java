package com.aboutstays.holder;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.BaseAdapterModel;
import com.aboutstays.model.message.MessageList;

import java.util.List;

/**
 * Created by mrnee on 4/28/2017.
 */

public class MessageHolder extends BaseHolder {
    private TextView tvMsg;
    private RelativeLayout rlMsg;

    public MessageHolder(View itemView) {
        super(itemView);
        tvMsg = (TextView) itemView.findViewById(R.id.tv_msg_text);
        rlMsg = (RelativeLayout) itemView.findViewById(R.id.rl_message);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        MessageList messageList = (MessageList) obj;
        if (messageList != null){
            tvMsg.setText(messageList.getMessageText());
            if (messageList.isLastMessage()){
                if (messageList.getByUser()){
                    rlMsg.setBackgroundResource(R.drawable.background_outgoing_msg);
                } else {
                    rlMsg.setBackgroundResource(R.drawable.background_incomming_msg);
                }

            } else {
                if (messageList.getByUser()){
                    rlMsg.setBackgroundResource(R.drawable.background_outgoing_msg);
                } else {
                    rlMsg.setBackgroundResource(R.drawable.background_incomming_msg);
                }
            }
        }
    }
}
