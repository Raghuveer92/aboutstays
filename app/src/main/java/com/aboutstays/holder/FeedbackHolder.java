package com.aboutstays.holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.aboutstays.R;

/**
 * Created by neeraj on 6/4/17.
 */

public class FeedbackHolder implements SeekBar.OnSeekBarChangeListener, View.OnClickListener {
    private Context ctx;
    private SeekBar seekrangeBar;
    private FeedbackListener feedbackListener;
    private ImageView ivRating1, ivRating2, ivRating3, ivRating4, ivRating5;
    private TextView tvHeading;
    private int maxProgress = 5;
    private int minProgress = 1;
    private int stepProgress = 1;
    private boolean dontShowFaded;
    private boolean isEditable;
    private boolean submitted;
    private UpdateFeedBackListener updateFeedBackListener;

    public boolean isSubmitted() {
        return submitted;
    }

    public void setSubmitted(boolean submitted) {
        this.submitted = submitted;
        if (feedbackListener != null) {
            feedbackListener.onChangeCheck();
        }
    }



    public boolean isDontShowFaded() {
        return dontShowFaded;
    }

    public void setDontShowFaded(boolean dontShowFaded) {
        this.dontShowFaded = dontShowFaded;
    }


    public FeedbackListener getFeedbackListener() {
        return feedbackListener;
    }

    public void setFeedbackListener(FeedbackListener feedbackListener) {
        this.feedbackListener = feedbackListener;
    }

    public FeedbackHolder(Context context, ViewGroup group, boolean isEditable, UpdateFeedBackListener updateFeedBackListener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_feedback_rating, group, false);
        this.updateFeedBackListener = updateFeedBackListener;
        seekrangeBar = (SeekBar) view.findViewById(R.id.rangeSeekbar);
        seekrangeBar.setOnSeekBarChangeListener(this);
        seekrangeBar.setEnabled(isEditable);


        tvHeading = (TextView) view.findViewById(R.id.tv_heding);
        ivRating1 = (ImageView) view.findViewById(R.id.iv_rating1);
        ivRating2 = (ImageView) view.findViewById(R.id.iv_rating2);
        ivRating3 = (ImageView) view.findViewById(R.id.iv_rating3);
        ivRating4 = (ImageView) view.findViewById(R.id.iv_rating4);
        ivRating5 = (ImageView) view.findViewById(R.id.iv_rating5);

        setOnClickListener(view, R.id.iv_rating1, R.id.iv_rating2, R.id.iv_rating3, R.id.iv_rating4, R.id.iv_rating5);
        group.addView(view);
    }


    private void setOnClickListener(View view,  int... viewIds) {
        for (int i : viewIds) {
            view.findViewById(i).setOnClickListener(this);
        }
    }


    public void setData(int rating, String headingText) {

        seekrangeBar.setMax((maxProgress - minProgress) / stepProgress);
        if (rating < 1) {
            seekrangeBar.setProgress(0);
        } else {
            seekrangeBar.setProgress(rating - 1);
        }
        tvHeading.setText(headingText);
    }


    public void changeSmileys() {

        if(isSubmitted()) {
            ivRating1.setImageResource(R.mipmap.feedback_one);
            ivRating2.setImageResource(R.mipmap.feedback_two);
            ivRating3.setImageResource(R.mipmap.feedback_three);
            ivRating4.setImageResource(R.mipmap.feedback_four);
            ivRating5.setImageResource(R.mipmap.feedback_five);
        } else {
            ivRating1.setImageResource(R.mipmap.feedback_one_fade);
            ivRating2.setImageResource(R.mipmap.feedback_two_fade);
            ivRating3.setImageResource(R.mipmap.feedback_three_fade);
            ivRating4.setImageResource(R.mipmap.feedback_four_fade);
            ivRating5.setImageResource(R.mipmap.feedback_five_fade);
        }
    }

    public int getData() {
        return seekrangeBar.getProgress() + 1;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (feedbackListener != null) {
            feedbackListener.onProgressChanged();
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        submitted = true;
        updateFeedBackListener.updateFeedback();
        if (feedbackListener != null) {
            feedbackListener.onChangeCheck();
        }
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        setSubmitted(true);
    }

    public void setProgress(int value) {
        if (value < 1) {
            seekrangeBar.setProgress(0);
        } else {
            seekrangeBar.setProgress(value - 1);
        }
    }

    @Override
    public void onClick(View v) {
        submitted = true;
        updateFeedBackListener.updateFeedback();
        if (feedbackListener != null) {
            feedbackListener.onChangeCheck();
        }
        switch (v.getId()) {
            case R.id.iv_rating1 :
                seekrangeBar.setProgress(0);
                break;
            case R.id.iv_rating2 :
                seekrangeBar.setProgress(1);
                break;
            case R.id.iv_rating3 :
                seekrangeBar.setProgress(2);
                break;
            case R.id.iv_rating4:
                seekrangeBar.setProgress(3);
                break;
            case R.id.iv_rating5 :
                seekrangeBar.setProgress(4);
                break;
        }
    }

    public interface FeedbackListener {
        void onProgressChanged();
        void onChangeCheck();
    }

    public interface UpdateFeedBackListener{
        public void updateFeedback();
    }

}
