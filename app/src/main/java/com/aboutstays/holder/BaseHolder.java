package com.aboutstays.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.aboutstays.adapter.BaseRecycleAdapter;

/**
 * Created by Neeraj on 12/29/2016.
 */

public class BaseHolder extends RecyclerView.ViewHolder {

    protected Context context;
    protected BaseRecycleAdapter.RecyclerClickListener clickListener;

    public void setClickListener(BaseRecycleAdapter.RecyclerClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public BaseHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
    }

    public void onBind(int position, Object obj) {

    }
//
//    //public BaseRecycleAdapter.RecyclerClickInterface getClickListener() {
//        return clickListener;
//    }

//    public void setClickListener(BaseRecycleAdapter.RecyclerClickInterface clickListener) {
//        this.clickListener = clickListener;
//    }

//    protected void onEventAction(int actionType, Object object) {
//        if (clickListener != null) {
//            clickListener.onItemClick(itemView, getAdapterPosition(), object, actionType);
//        }
//    }
}
