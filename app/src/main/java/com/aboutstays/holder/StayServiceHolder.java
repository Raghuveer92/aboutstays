package com.aboutstays.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.CheckinServiceModel;
import com.aboutstays.model.StayServiceModel;

import simplifii.framework.utility.AppConstants;

/**
 * Created by Neeraj on 1/2/2017.
 */

public class StayServiceHolder extends BaseHolder {

    TextView tvName;
    ImageView logo, logoSelect;
    private RelativeLayout rlMain;
    private StayServiceModel stayServiceModel;

    public StayServiceHolder(View itemView) {
        super(itemView);
        tvName = (TextView) itemView.findViewById(R.id.tv_name);
        logo = (ImageView) itemView.findViewById(R.id.iv_icon);
        rlMain = (RelativeLayout) itemView.findViewById(R.id.rl_main);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        super.onBind(position, obj);
        stayServiceModel = (StayServiceModel) obj;
        if (stayServiceModel != null){
            tvName.setText(stayServiceModel.getTitle());
            logo.setImageResource(stayServiceModel.getLogo());
        }

//        rlMain.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onEventAction(AppConstants.ACTION_TYPE.NEXT_ACTIVITY, v.getTag());
//            }
//        });
    }
}
