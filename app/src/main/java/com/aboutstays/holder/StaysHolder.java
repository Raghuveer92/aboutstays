package com.aboutstays.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.stayline.HotelStaysList;

import simplifii.framework.widgets.CustomRatingBar;

public class StaysHolder extends BaseHolder{
        TextView tvStayStaus, tvTitle, tvCheckinDate, tvCheckOutDate, tvCheckinTime, tvCheckoutTime, tvAddress;
        ImageView ivRowUpcoming,ivPartnerShip;
        RelativeLayout rlMain;
        CustomRatingBar customRatingBar;

        public StaysHolder(View view) {
            super(view);
            tvStayStaus = (TextView) view.findViewById(R.id.tv_stay);
            tvTitle = (TextView) view.findViewById(R.id.tv_title_upcoming);
            tvCheckinDate = (TextView) view.findViewById(R.id.tv_checkin_date);
            tvCheckOutDate = (TextView) view.findViewById(R.id.tv_checkout_date);
            tvCheckinTime = (TextView) view.findViewById(R.id.tv_checkin_time);
            tvCheckoutTime = (TextView) view.findViewById(R.id.tv_checkout_time);
            tvAddress = (TextView) view.findViewById(R.id.tv_address);
            ivRowUpcoming = (ImageView) view.findViewById(R.id.iv_row_upcoming);
            rlMain = (RelativeLayout) view.findViewById(R.id.lay_top);
            customRatingBar = (CustomRatingBar) view.findViewById(R.id.custom_rating_bar);
            ivPartnerShip= (ImageView) view.findViewById(R.id.iv_prtnership);
        }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        HotelStaysList hotelStaysList= (HotelStaysList) obj;

    }
}
