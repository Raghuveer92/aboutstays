package com.aboutstays.holder;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.GeneralServiceModel;
import com.squareup.picasso.Picasso;

import simplifii.framework.utility.AppConstants;

/**
 * Created by Neeraj on 12/29/2016.
 */

public class CheckInInformationHolder extends BaseHolder {

    TextView tvName;
    ImageView logo, logoSelect;
    private GeneralServiceModel generalInformationModel;
    private RelativeLayout rlMain;

    public CheckInInformationHolder(View itemView) {
        super(itemView);
        tvName = (TextView) itemView.findViewById(R.id.tv_name);
        logo = (ImageView) itemView.findViewById(R.id.iv_icon);
        rlMain = (RelativeLayout) itemView.findViewById(R.id.rl_main);
    }

    @Override
    public void onBind(final int position, Object obj) {
        super.onBind(position, obj);
        generalInformationModel = (GeneralServiceModel) obj;
        if (generalInformationModel != null && generalInformationModel.getGeneralServiceData() != null){
            tvName.setText(generalInformationModel.getGeneralServiceData().getName());
            String imageUrl = generalInformationModel.getGeneralServiceData().getActivatedImageUrl();
            if (!TextUtils.isEmpty(imageUrl)){
                Picasso.with(context).load(imageUrl)
                        .error(R.mipmap.hotel_info)
                        .into(logo);
            }
//            logo.setImageResource(generalInformationModel.getLogo());
        }

        rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClicked(position, generalInformationModel, AppConstants.ACTION_TYPE.NEXT_ACTIVITY);
            }
        });
    }

}
