package com.aboutstays.activity;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.enums.HouseKeepingItemType;
import com.aboutstays.model.cart.HouseKeepingCartResponse;
import com.aboutstays.model.cart.HkItemOrderDetails;
import com.aboutstays.model.cart.HkItemOrderDetailsList;
import com.aboutstays.model.cart.HouseKeepingCart;
import com.aboutstays.model.cart.HouseKeepingCartData;
import com.aboutstays.model.cart.HouseKeepingCartResponseData;
import com.aboutstays.model.housekeeping.HouseKeepingItemData;
import com.aboutstays.model.housekeeping.HouseKeepingItemDataResponce;
import com.aboutstays.model.housekeeping.HouseKeepingItemList;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import simplifii.framework.ListAdapters.CustomExpandableListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

public class HouseKeepingItemActivity extends AppBaseActivity implements SearchView.OnQueryTextListener, CustomExpandableListAdapter.ExpandableListener {

    private List<HouseKeepingItemData> itemDataList = new ArrayList<>();
    private ExpandableListView expandableListView;
    private String userId;
    private TextView tvEmptyView;
    private String hotelServiceId;
    private String staysId;
    private String title;
    private Integer itemType;
    private MenuItem searchMenuItem;
    private SearchView searchView;
    private int optionMenuImageUrl;
    private HouseKeepingCart houseKeepingCart;
    private CustomExpandableListAdapter customExpandableListAdapter;
    private LinkedHashMap<String, List<HouseKeepingItemData>> mapExpandable = new LinkedHashMap<>();
    private LinkedHashMap<String, List<HouseKeepingItemData>> mapExpandableAll = new LinkedHashMap<>();
    private List<String> listHeader = new ArrayList<>();
    private String hotelId;
    private TextView tv;
    private String gsId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_housekeeping_item);
        registerCartReceiver();


        initToolBar("");

        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            userId = sessionInstance.getUserId();
        }

        Bundle b = getIntent().getExtras();
        if (b != null) {
            hotelServiceId = b.getString(AppConstants.BUNDLE_KEYS.HOTEL_SERVICE_ID);
            staysId = b.getString(AppConstants.BUNDLE_KEYS.STAYS_ID);
            gsId = b.getString(AppConstants.BUNDLE_KEYS.GS_ID);
            hotelId = b.getString(AppConstants.BUNDLE_KEYS.HOTEL_ID);
            HouseKeepingItemList houseKeepingItemList = (HouseKeepingItemList) b.getSerializable(AppConstants.BUNDLE_KEYS.HOUSEKEEPING_SERVICE_TITLE);
            if (houseKeepingItemList != null) {
                itemType = houseKeepingItemList.getType();
                title = houseKeepingItemList.getTypeName();
                HouseKeepingItemType byCode = HouseKeepingItemType.findByCode(itemType);
                optionMenuImageUrl = byCode.getImageUrl();
            }

        }

        if (!TextUtils.isEmpty(title)) {
            initToolBar(title);
        }
        tvEmptyView = (TextView) findViewById(R.id.tv_empty_view);
        expandableListView = (ExpandableListView) findViewById(R.id.list_expandable_house_keeping);
        customExpandableListAdapter = new CustomExpandableListAdapter(this, R.layout.row_header_expandable, R.layout.row_laundry_item, mapExpandable, this);
        expandableListView.setAdapter(customExpandableListAdapter);
        expandableListView.setTextFilterEnabled(false);
        expandableListView.setEmptyView(tvEmptyView);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAllHouseKeepingItem();
    }


    @Override
    public void onPreExecute(int taskCode) {
        showProgressDialog();
        if (taskCode == AppConstants.TASK_CODES.ADD_REMOVE_HOUSEKEEPING_TO_CART) {
            return;
        }
        if (taskCode == AppConstants.TASK_CODES.GET_HOUSEKEEPING_CART_DETAILS) {
            return;
        }
        super.onPreExecute(taskCode);
    }

//    @Override
//    public void onBackPressed() {
//        setResult(Activity.RESULT_OK);
//        finish();
//    }

    public void addItemToCart() {
        customExpandableListAdapter.notifyDataSetChanged();
        List<HouseKeepingCartData> cartDataList = new ArrayList<>();
        for (HouseKeepingItemData itemData : itemDataList) {

            houseKeepingCart = new HouseKeepingCart();
            houseKeepingCart.setUserId(userId);
            houseKeepingCart.setHotelId(itemData.getHotelId());
            houseKeepingCart.setStaysId(staysId);

            HouseKeepingCartData cartData = new HouseKeepingCartData();
            cartData.setCartType(2);
            cartData.setCartOperation(1);

            HkItemOrderDetails itemOrderDetails = new HkItemOrderDetails();
            itemOrderDetails.setHkItemId(itemData.getItemId());
            itemOrderDetails.setType(itemData.getType());
            itemOrderDetails.setPrice(itemData.getPrice());
            itemOrderDetails.setQuantity(itemData.getItemCount());

            cartData.setHkItemOrderDetails(itemOrderDetails);
            cartDataList.add(cartData);
            houseKeepingCart.setData(cartDataList);
        }

        if (houseKeepingCart != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.addOrRemoveHouseKeepingItem(houseKeepingCart);
            executeTask(AppConstants.TASK_CODES.ADD_REMOVE_HOUSEKEEPING_TO_CART, httpParamObject);
        } else {
            finish();
        }
    }

    private void getAllHouseKeepingItem() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getAllFoodItemmsbyHousekeeping(hotelServiceId, itemType);
        executeTask(AppConstants.TASK_CODES.GET_HOUSEKEEPING_ITEMS, httpParamObject);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_roomservice:
                OrderDetailActivity.startActivity(HouseKeepingItemActivity.this, gsId, staysId, hotelId, AppConstants.STAYS_TYPE.HOUSE_KEEPING, AppConstants.FROM_ORDERING.HOUSEKEEPING);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_service, menu);

        MenuItem item = menu.findItem(R.id.action_room_service);
        MenuItemCompat.setActionView(item, R.layout.action_menu_count_layout);
        RelativeLayout notifCount = (RelativeLayout) MenuItemCompat.getActionView(item);

        ImageView iv = (ImageView) notifCount.findViewById(R.id.iv_roomservice);
        iv.setImageResource(optionMenuImageUrl);
        iv.setColorFilter(Color.WHITE);
        iv.setOnClickListener(this);

        tv = (TextView) notifCount.findViewById(R.id.actionbar_notifcation_textview);
        tv.setText("0");
        tv.setVisibility(View.GONE);

        refreshCartData();

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchMenuItem.getActionView();
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showSearchFragment();
            }
        });
        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        filterListOnSearch(newText);
        return true;
    }

    private void filterListOnSearch(String newText) {
        mapExpandable.clear();
        for (Map.Entry<String, List<HouseKeepingItemData>> entry : mapExpandableAll.entrySet()) {
            List<HouseKeepingItemData> packLists = entry.getValue();
            List<HouseKeepingItemData> packListsSelected = new ArrayList<>();
            for (HouseKeepingItemData internetPackList : packLists) {
                if (internetPackList.getName().toLowerCase().contains(newText.toLowerCase())) {
                    packListsSelected.add(internetPackList);
                }
            }
            if (packListsSelected.size() > 0) {
                mapExpandable.put(entry.getKey(), packListsSelected);
            }
        }
        refreshParentData();
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_HOUSEKEEPING_ITEMS:
                HouseKeepingItemDataResponce dataResponce = (HouseKeepingItemDataResponce) response;
                if (dataResponce != null && dataResponce.getError() == false) {
                    List<HouseKeepingItemData> dataList = dataResponce.getData();
                    if (!CollectionUtils.isEmpty(dataList)) {
                        ckecIfAlreadyAddedInCart(dataList);
                        itemDataList.clear();
                        for (HouseKeepingItemData itemData : dataList){
                            if (itemData.getShowOnApp() == true){
                                itemDataList.add(itemData);
                            }
                        }
//                        itemDataList.addAll(dataList);
                        filterList(itemDataList);
                    }
                }
                break;
            case AppConstants.TASK_CODES.ADD_REMOVE_HOUSEKEEPING_TO_CART:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false) {
//                    showToast(getString(R.string.cart_added));
                    setResult(AppConstants.RESULT_CODE.RESULT_NULL);
                }
                break;
        }
    }

    private void refreshCartData() {
        HouseKeepingCartResponseData cartData = HouseKeepingCartResponse.getInstance();
        if (cartData.getHkItemOrderDetailsList().size() > 0) {
            tv.setText(cartData.getHkItemOrderDetailsList().size() + "");
            tv.setVisibility(View.VISIBLE);
        } else {
            tv.setText("0");
            tv.setVisibility(View.GONE);
        }
        customExpandableListAdapter.notifyDataSetChanged();
    }

    private void filterList(List<HouseKeepingItemData> itemDataList) {
        listHeader.clear();
        mapExpandable.clear();
        for (HouseKeepingItemData internetPackList : itemDataList) {
            String category = internetPackList.getCategory();
            if (!TextUtils.isEmpty(category)) {
                if (mapExpandable.containsKey(category)) {
                    List<HouseKeepingItemData> internetPackLists = mapExpandable.get(category);
                    internetPackLists.add(internetPackList);
                } else {
                    List<HouseKeepingItemData> laundryItemDatas = new ArrayList<>();
                    laundryItemDatas.add(internetPackList);
                    mapExpandable.put(category, laundryItemDatas);
                }
            }
        }
        refreshParentData();
        mapExpandableAll.putAll(mapExpandable);
    }

    private void ckecIfAlreadyAddedInCart(List<HouseKeepingItemData> housekeepingList) {
        List<HkItemOrderDetailsList> dataList = HouseKeepingCartResponse.getInstance().getHkItemOrderDetailsList();
        if (!CollectionUtils.isEmpty(dataList)) {
            for (HouseKeepingItemData dataByType : housekeepingList) {
                for (HkItemOrderDetailsList detailList : dataList) {
                    if (dataByType.getItemId().equals(detailList.getHkItemId())) {
                        dataByType.setItemCount(detailList.getQuantity());
                    }
                }
            }
        }
    }

    private void refreshParentData() {
        listHeader.clear();
        Iterator<String> iterator = mapExpandable.keySet().iterator();
        while (iterator.hasNext()) {
            listHeader.add(iterator.next());
        }
        customExpandableListAdapter = new CustomExpandableListAdapter(this, R.layout.row_header_expandable, R.layout.row_internet, mapExpandable, this);
        expandableListView.setAdapter(customExpandableListAdapter);
        for (int x = 0; x < listHeader.size(); x++) {
            expandableListView.expandGroup(x);
        }
    }

    @Override
    public int getChildSize(int parentPosition) {
        return mapExpandable.get(listHeader.get(parentPosition)).size();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        HolderParent holderParent;
        if (convertView == null) {
            convertView = inflater.inflate(resourceId, null);
            holderParent = new HolderParent(convertView);
            convertView.setTag(holderParent);
        } else {
            holderParent = (HolderParent) convertView.getTag();
        }
        String title = listHeader.get(groupPosition);
        holderParent.bindData(title, isExpanded);
        return convertView;
    }


    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_laundry_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (isLastChild) {
            holder.viewBottomLine.setVisibility(View.INVISIBLE);
        } else {
            holder.viewBottomLine.setVisibility(View.VISIBLE);
        }

        HouseKeepingItemData houseKeepingItemData = mapExpandable.get(listHeader.get(groupPosition)).get(childPosition);
        holder.onBindData(houseKeepingItemData);
        return convertView;
    }

    class ViewHolder {

        TextView tvTitle, tvTime, serviceDescription, tvAdd;
        TextView tvMinus, tvCount, tvPlus;
        ImageView imgVegNonveg, imgChilli, imgLike;
        RelativeLayout rlCount, rlTitle;
        View viewBottomLine;


        public ViewHolder(View view) {

            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            tvTime = (TextView) view.findViewById(R.id.tv_price);
            serviceDescription = (TextView) view.findViewById(R.id.tv_service_description);
            imgVegNonveg = (ImageView) view.findViewById(R.id.iv_veg_nonveg);
            imgChilli = (ImageView) view.findViewById(R.id.iv_chilli);
            imgLike = (ImageView) view.findViewById(R.id.iv_like);
            tvAdd = (TextView) view.findViewById(R.id.tv_add);
            tvMinus = (TextView) view.findViewById(R.id.tv_minus);
            tvCount = (TextView) view.findViewById(R.id.tv_count);
            tvPlus = (TextView) view.findViewById(R.id.tv_plus);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_add_count);
            rlTitle = (RelativeLayout) view.findViewById(R.id.rl_title);
            viewBottomLine = (View) view.findViewById(R.id.view_bottom_line);
        }

        public void onBindData(final HouseKeepingItemData ob) {

            tvCount.setText(ob.getItemCount() + "");
            tvTitle.setText(ob.getName());
            tvTime.setVisibility(View.GONE);

            tvPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int itemCount = ob.getItemCount();
                    ob.setItemCount(itemCount + 1);
                    HouseKeepingCartResponseData.refreshItem(ob, HouseKeepingItemActivity.this);
                    refreshCartData();
                    addItemToCart();
                }
            });

            tvMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int itemCount = ob.getItemCount();
                    if (itemCount >= 1) {
                        ob.setItemCount(itemCount - 1);
                    }
                    if (ob.getItemCount() == 0) {
                        HouseKeepingCartResponseData.refreshItem(ob, HouseKeepingItemActivity.this);
                        refreshCartData();
                    }
                    addItemToCart();
                }
            });

            tvAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ob.setItemCount(1);
                    HouseKeepingCartResponseData.refreshItem(ob, HouseKeepingItemActivity.this);
                    refreshCartData();
                    addItemToCart();
                }
            });

            if (ob.getItemCount() >= 1) {
                tvAdd.setVisibility(View.GONE);
                rlCount.setVisibility(View.VISIBLE);
            } else {
                tvAdd.setVisibility(View.VISIBLE);
                rlCount.setVisibility(View.GONE);
            }

            rlTitle.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    class HolderParent {
        TextView textView;
        ImageView ivIndicator;

        public HolderParent(View view) {
            textView = (TextView) view.findViewById(R.id.tv_men_women);
            ivIndicator = (ImageView) view.findViewById(R.id.iv_indicator);
        }

        public void bindData(String title, boolean isExpand) {
            textView.setText(title);
            if (isExpand) {
                ivIndicator.setImageResource(R.mipmap.up_arrow_white);
            } else {
                ivIndicator.setImageResource(R.mipmap.arrow_down_white);
            }
        }
    }

    @Override
    public void onCartUpdate() {
//        getAllHouseKeepingItem();
        refreshCartData();
    }

    @Override
    public void onCartClear() {
        getAllHouseKeepingItem();
    }
}

