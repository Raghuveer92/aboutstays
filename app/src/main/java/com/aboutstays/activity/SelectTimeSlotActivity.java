package com.aboutstays.activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aboutstays.R;
import com.aboutstays.fragment.dialog.BookingConfirmDialogFragment;
import com.aboutstays.model.SelectTimeSlotModel;
import com.yuncun.swipeableweekview.CircleView;
import com.yuncun.swipeableweekview.WeekViewAdapter;
import com.yuncun.swipeableweekview.WeekViewSwipeable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.Util;

public class SelectTimeSlotActivity extends AppBaseActivity implements CustomListAdapterInterface {
    private ListView lvTimeSlot;
    private CustomListAdapter listAdapter;
    private List<SelectTimeSlotModel> slotModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_time_slot);
        initToolBar(getString(R.string.select_time_slot));

        lvTimeSlot = (ListView) findViewById(R.id.lv_time_slot);
        listAdapter = new CustomListAdapter(this, R.layout.row_select_time_slot, slotModelList, this);
        lvTimeSlot.setAdapter(listAdapter);
        loadData();

        swipeWeekView();
    }

    private void loadData() {
        for (int i = 0; i < 10; i++) {
            SelectTimeSlotModel selectTimeSlotModel = new SelectTimeSlotModel();
            int j = i+1;
            selectTimeSlotModel.setTvTime(i + ":00 am - " + j + ":00 am");
            slotModelList.add(selectTimeSlotModel);
            listAdapter.notifyDataSetChanged();
        }
    }

    private void swipeWeekView() {
        List<Boolean> data = Arrays.asList(false, false, true, false, true, true, false, false, true, false, true, true); //Example set; use your own List<T> here
        WeekViewSwipeable wvs = (WeekViewSwipeable) findViewById(R.id.calendar_component);
        WeekViewAdapter<Boolean> adapter = new WeekViewAdapter(data) {
            @Override
            public int getStrokeColor(final int index) {
                if ((Boolean) get(index) == true) {
                    return ContextCompat.getColor(SelectTimeSlotActivity.this, R.color.teal);
                } else {
                    return ContextCompat.getColor(SelectTimeSlotActivity.this, R.color.color_discvr_red);
                }
            }

            @Override
            public int getFillColor(final int index) {
                if ((Boolean) get(index) == true) {
                    return ContextCompat.getColor(SelectTimeSlotActivity.this, R.color.teal);
                } else {
                    return ContextCompat.getColor(SelectTimeSlotActivity.this, R.color.color_discvr_red);
                }
            }

            @Override
            public TextView getTextView(TextView tv, int index) {
                if ((Boolean) get(index) == true) {
                    tv.setText("Hit");
                } else {
                    tv.setText("Miss");
                }
                return tv;
            }

            @Override
            public View getDayLayout(View dv, final int index) {
                dv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(view.getContext(), "Click! on index " + index, Toast.LENGTH_LONG).show();
                    }
                });
                return dv;
            }
        };
        wvs.setAdapter(adapter);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_select_time_slot, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final SelectTimeSlotModel selectTimeSlotModel = slotModelList.get(position);
        holder.tvTime.setText(selectTimeSlotModel.getTvTime());
        holder.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openConfirmDialog(selectTimeSlotModel);
            }
        });
        return convertView;
    }

    private void openConfirmDialog(SelectTimeSlotModel selectTimeSlotModel) {
        BookingConfirmDialogFragment.show(getSupportFragmentManager(),
                getString(R.string.slot_confirmed),
                "",
                "",
                "",
                selectTimeSlotModel.getTvTime(),
                "",
                new BookingConfirmDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                        finish();
                    }
                }
        );
    }

    class Holder {
        TextView tvTime, tvAdd;

        public Holder(View view) {
            tvTime = (TextView) view.findViewById(R.id.tv_time_slot);
            tvAdd = (TextView) view.findViewById(R.id.tv_add);
        }
    }
}
