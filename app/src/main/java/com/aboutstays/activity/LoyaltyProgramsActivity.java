package com.aboutstays.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.LoyaltyModel;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;

/**
 * Created by ajay on 13/12/16.
 */
public class LoyaltyProgramsActivity extends AppBaseActivity implements CustomListAdapterInterface {
    private List<LoyaltyModel> listLoyalty = new ArrayList<>();
    private CustomListAdapter listAdapter;
    private ListView listView;
    private Button btnAddRoyaltyProgram;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loyalty_programs);
        initToolBar("Loyalty Programs");
        btnAddRoyaltyProgram = (Button) findViewById(R.id.btn_add_royalty_program);
        listView = (ListView) findViewById(R.id.list_view_loyalty_program);
        listAdapter = new CustomListAdapter(this, R.layout.row_loyalty_programs, listLoyalty, this);
        listView.setAdapter(listAdapter);
        putDataIntoList();
        setOnClickListener(R.id.btn_add_royalty_program);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_royalty_program:
                startNextActivity(SelectLoyaltyProgramActivity.class);
                break;
        }
    }

    private void putDataIntoList() {
        for (int i = 0; i <3; i++) {
            LoyaltyModel ob = new LoyaltyModel();
            ob.setProgramNo(getString(R.string.loyalty_program) + i);
            ob.setPoints("20" + i+" "+"Points");
            ob.setImgUrl(R.mipmap.ic_star);
            ob.setProgramType("Silver");
            listLoyalty.add(ob);
        }
        listAdapter.notifyDataSetChanged();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        LoyaltyModel ob = listLoyalty.get(position);
        holder.tvProgramNo.setText(ob.getProgramNo());
        holder.ivProgramType.setImageResource(ob.getImgUrl());
        holder.tvPoints.setText(ob.getPoints());
        holder.tvProgramType.setText(ob.getProgramType());
        return convertView;
    }


    class Holder {
        TextView tvProgramNo, tvPoints, tvProgramType;
        ImageView ivProgramType;

        public Holder(View view) {
            tvProgramNo = (TextView) view.findViewById(R.id.tv_program_title);
            tvPoints = (TextView) view.findViewById(R.id.tv_points);
            tvProgramType = (TextView) view.findViewById(R.id.tv_grade_mark);
            ivProgramType = (ImageView) view.findViewById(R.id.iv_star);
        }
    }
}

