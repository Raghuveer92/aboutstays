package com.aboutstays.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.stayline.HotelBasicInfo;
import com.aboutstays.rest.response.GenerateOtpApi;
import com.aboutstays.rest.response.OtpData;
import com.aboutstays.utils.FBLoginUtil;
import com.aboutstays.utils.GoogleUtil;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.cunoraz.gifview.library.GifView;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.response.requests.SignupRequest;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.response.LoginResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class LoginActivity extends AppBaseActivity implements GoogleApiClient.OnConnectionFailedListener {
    private Button btnLogin;
    private EditText etMail, etPass;
    //    private TextInputLayout tilMail, tilPass;
    private TextView tvForgetPass, tvSignup, guestUser, tvMailError, tvPassError;
    private Integer type;
    private String value;
    private RelativeLayout rlFacebookLogin;
    private RelativeLayout rlGoogleLogin;
    private GoogleUtil googleUtil = GoogleUtil.getInstance(this);
    private FBLoginUtil fbLoginUtil;
    private HotelBasicInfo hotelBasicInfo;
    private boolean isChildActivity, isStayLineFragment;
    private TextView tvMsg;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initCroutonView();

        if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle b = getIntent().getExtras();
            hotelBasicInfo = (HotelBasicInfo) b.getSerializable(AppConstants.BUNDLE_KEYS.FROM_BOOKING);
            isChildActivity = b.getBoolean(AppConstants.BUNDLE_KEYS.IS_CHILD_ACTIVITY, false);
            isStayLineFragment = b.getBoolean(AppConstants.BUNDLE_KEYS.IS_STAY_LINE_FRAGMENT, false);
        }

        btnLogin = (Button) findViewById(R.id.btn_login);
        tvForgetPass = (TextView) findViewById(R.id.tv_forgot_password);
        tvSignup = (TextView) findViewById(R.id.text_signup);
        guestUser = (TextView) findViewById(R.id.text_guest_user);
//        tilMail = (TextInputLayout) findViewById(R.id.til_email);
//        tilPass = (TextInputLayout) findViewById(R.id.til_pass);
        etMail = (EditText) findViewById(R.id.et_email);
        etPass = (EditText) findViewById(R.id.et_pass);
        rlFacebookLogin = (RelativeLayout) findViewById(R.id.rl_facebook_login);
        rlGoogleLogin = (RelativeLayout) findViewById(R.id.rl_google_login);
        tvMailError = (TextView) findViewById(R.id.tv_email_error);
        tvPassError = (TextView) findViewById(R.id.tv_pass_error);

        setOnClickListener(R.id.btn_login, R.id.tv_forgot_password, R.id.text_signup, R.id.text_guest_user,
                R.id.rl_facebook_login, R.id.rl_google_login);
//        setTextChangeListener();
    }

    private void setTextChangeListener() {
        etMail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                changeButtonStatus();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                changeButtonStatus();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void changeButtonStatus() {
        String loginInput = etMail.getText().toString().trim();
        String loginPassword = etPass.getText().toString().trim();
        if (TextUtils.isEmpty(loginInput) || TextUtils.isEmpty(loginPassword)) {
            btnLogin.setBackgroundResource(R.drawable.shape_button_login_disable);
            btnLogin.setEnabled(false);
        } else {
            btnLogin.setBackgroundResource(R.drawable.shape_button_login);
            btnLogin.setEnabled(true);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_login:
                validateAndManualLogin();
                break;
            case R.id.tv_forgot_password:
                validateForgotPassword();
                //startNextActivity(ActivityResetPasswordOptions.class);
                break;
            case R.id.text_signup:
                startNextActivity(SignUpActivity.class);
                break;
            case R.id.text_guest_user:
                Bundle b = new Bundle();
                b.putBoolean(AppConstants.BUNDLE_KEYS.IF_GUEST, true);
                startNextActivity(b, DrawerActivity.class);
                finish();
                break;
            case R.id.rl_facebook_login:
                getFacebookLoginData();
                fbLoginUtil.initiateFbLogin();
                break;
            case R.id.rl_google_login:
                getGoogleLoginData();
                googleUtil.signInWithGoogle();
                break;
        }
    }

    private void getFacebookLoginData() {
        fbLoginUtil = new FBLoginUtil(this, new FBLoginUtil.FBLoginCallback() {

            @Override
            public void onSuccess(Bundle bundle) {
                SignupRequest request = new SignupRequest();
                String number = bundle.getString(AppConstants.FACEBOOK_BUNDLE_KEYS.NUMBER);
                String email = bundle.getString(AppConstants.FACEBOOK_BUNDLE_KEYS.EMAIL);
                String idFacebook = bundle.getString(AppConstants.FACEBOOK_BUNDLE_KEYS.ID_FACEBOOK);
                String firstName = bundle.getString(AppConstants.FACEBOOK_BUNDLE_KEYS.FIRST_NAME);
                String lastName = bundle.getString(AppConstants.FACEBOOK_BUNDLE_KEYS.LAST_NAME);

                if (!TextUtils.isEmpty(number)) {
                    request.setNumber(number);
                    if (!TextUtils.isEmpty(email)) {
                        request.setEmailId(email);
                        request.setPassword(idFacebook);
                    } else {
                        request.setEmailId(number);
                        request.setPassword(idFacebook);
                    }
                    type = AppConstants.TYPE.PHONE;
                    value = number;
                } else if (!TextUtils.isEmpty(email)) {
                    request.setEmailId(email);
                    request.setNumber(email);
                    request.setPassword(idFacebook);
                    type = AppConstants.TYPE.EMAIL;
                    value = email;
                } else if (!TextUtils.isEmpty(idFacebook)) {
                    request.setEmailId(idFacebook);
                    request.setNumber(idFacebook);
                    request.setPassword(idFacebook);
                    type = AppConstants.TYPE.PHONE;
                    value = idFacebook;
                }

                if (!TextUtils.isEmpty(firstName)) {
                    request.setFirstName(firstName);
                }
                if (!TextUtils.isEmpty(lastName)) {
                    request.setLastName(lastName);
                }
                request.setSignupType(AppConstants.SIGNUP_TYPE.FACEBOOK);
                socialMediaLogin(AppConstants.LOGIN_TYPE.FACEBOOK, request);
            }

            @Override
            public void onFailure() {
                showToast("Unable to login via facebook");
            }
        });
    }

    private void getGoogleLoginData() {
        googleUtil.setListener(new GoogleUtil.GoogleLoginCallBack() {
            @Override
            public void onSuccess(GoogleSignInAccount googleSignInAccount) {
                if (googleSignInAccount != null) {
                    SignupRequest request = new SignupRequest();
                    if (!TextUtils.isEmpty(googleSignInAccount.getEmail())) {
                        request.setEmailId(googleSignInAccount.getEmail());
                        request.setNumber(googleSignInAccount.getEmail());
                        type = AppConstants.TYPE.EMAIL;
                        value = googleSignInAccount.getEmail();
                    } else {
                        request.setEmailId(googleSignInAccount.getId());
                        request.setNumber(googleSignInAccount.getId());
                        type = AppConstants.TYPE.PHONE;
                        value = googleSignInAccount.getId();
                    }
                    request.setPassword(googleSignInAccount.getId());
                    request.setFirstName(Util.getFirstNameAndLastName(googleSignInAccount.getDisplayName(), true));
                    request.setLastName(Util.getFirstNameAndLastName(googleSignInAccount.getDisplayName(), false));
                    request.setSignupType(AppConstants.SIGNUP_TYPE.GMAIL);
                    socialMediaLogin(AppConstants.LOGIN_TYPE.GMAIL, request);
                }
            }

            @Override
            public void onFailed() {
                showToast("Unable to fetch data");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == AppConstants.REQUEST_CODE.GOOGLE_SIGN_IN) {
            googleUtil.onActivityResult(data);
            return;
        }
        fbLoginUtil.onActivityResult(requestCode, resultCode, data);
    }


    private void validateForgotPassword() {
        String forgotPassInput = etMail.getText().toString().trim();
        if (!validateEmailOrPhoneInput(forgotPassInput)) {
            return;
        }
        HttpParamObject httpParamObject = ApiRequestGenerator.forgotPassword(type, value);
        executeTask(AppConstants.TASK_CODES.FORGOT_PASSWORD, httpParamObject);
    }


    private void socialMediaLogin(Integer loginType, SignupRequest request) {
        HttpParamObject httpParamObject = ApiRequestGenerator.login(type, value, request.getPassword(), loginType, request);
        executeTask(AppConstants.TASK_CODES.LOGIN, httpParamObject);
    }

    private void validateAndManualLogin() {
        String loginInput = etMail.getText().toString().trim();
        String loginPassword = etPass.getText().toString().trim();
        SignupRequest signupRequest = null;
        if (!validateEmailOrPhoneInput(loginInput))
            return;

        if (TextUtils.isEmpty(loginPassword)) {
            tvPassError.setText(getString(R.string.enter_pass));
            tvPassError.setVisibility(View.VISIBLE);
            return;
        } else {
            tvPassError.setVisibility(View.INVISIBLE);
        }
        HttpParamObject httpParamObject = ApiRequestGenerator.login(type, value, loginPassword, AppConstants.LOGIN_TYPE.MANUAL, signupRequest);
        executeTask(AppConstants.TASK_CODES.LOGIN, httpParamObject);
    }


    @Override
    public void onPreExecute(int taskCode) {
        super.onPreExecute(taskCode);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }


    @Override
    public void
    onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.LOGIN:
                LoginResponse loginResponse = (LoginResponse) response;
                if (loginResponse != null) {
                    if (loginResponse.getError() == false) {
                        UserSession.saveUserSessionAndLogin(loginResponse.getData());
                        if (isStayLineFragment) {
                            setResult(RESULT_OK);
                            finish();
                        } else {
                            startNextActivity(DrawerActivity.class);
                        }
                        finish();
                    } else {
                        if (loginResponse.getCode().equals("INV_PASS")) {
                            tvPassError.setText(getString(R.string.wrong_pass));
                            tvPassError.setVisibility(View.VISIBLE);
                            tvMailError.setVisibility(View.INVISIBLE);
                        } else if (loginResponse.getCode().equals("INV_ACC")) {
                            tvMailError.setText(getString(R.string.no_account));
                            tvMailError.setVisibility(View.VISIBLE);
                            tvPassError.setVisibility(View.INVISIBLE);
                        } else {
                            tvPassError.setVisibility(View.INVISIBLE);
                            tvMailError.setVisibility(View.INVISIBLE);
                        }
                    }
                }
                break;

            case AppConstants.TASK_CODES.GENERATE_OTP:
                GenerateOtpApi otpApi = (GenerateOtpApi) response;
                progressBar.setVisibility(View.GONE);
                if (otpApi != null && otpApi.getError()== false){
                    OtpData data = otpApi.getData();
                    if (data != null){
                        Bundle bundle = new Bundle();
                        bundle.putString(AppConstants.BUNDLE_KEYS.NUMBER, value);
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, data);
                        bundle.putInt(AppConstants.BUNDLE_KEYS.BUTTON_TARGET, AppConstants.ACTIVITY_CONSTANTS.TARGET_CHANGE_PASSWORD);
                        startNextActivity(bundle, ValidateOtpActivity.class);
                    }
                }
                else {
                    showToast(otpApi.getMessage());
                    tvMsg.setText(otpApi.getMessage());
                }
                break;

            case AppConstants.TASK_CODES.FORGOT_PASSWORD:
                BaseApiResponse forgotPasswordResponse = (BaseApiResponse) response;
                if (forgotPasswordResponse != null) {
                    if (forgotPasswordResponse.getError() == false) {
                        resetPasswordOptionsDialog(value);
//                        Bundle bundle = new Bundle();
//                        bundle.putInt(AppConstants.BUNDLE_KEYS.TYPE, type);
//                        bundle.putString(AppConstants.BUNDLE_KEYS.VALUE, value);
//                        startNextActivity(bundle, ActivityResetPasswordOptions.class);
                    } else {
                        if (forgotPasswordResponse.getCode().equals("INV_ACC")) {
                            tvMailError.setText(getString(R.string.no_account));
                            tvMailError.setVisibility(View.VISIBLE);
                        } else {
                            tvMailError.setVisibility(View.INVISIBLE);
                        }
                    }
                }
                break;
        }
    }


    private boolean validateEmailOrPhoneInput(String loginInput) {
        if (TextUtils.isEmpty(loginInput)) {
            tvMailError.setText(getString(R.string.empty_email));
            tvMailError.setVisibility(View.VISIBLE);
            return false;
        } else {
            tvMailError.setVisibility(View.INVISIBLE);
        }
        try {
            Long.parseLong(loginInput);
            type = AppConstants.TYPE.PHONE;
        } catch (Exception e) {
            type = AppConstants.TYPE.EMAIL;
        }
        value = loginInput;

        switch (type) {
            case AppConstants.TYPE.EMAIL:
                if (!Util.isValidEmail(value)) {
                    tvMailError.setText(getString(R.string.invailid_email_phone));
                    tvMailError.setVisibility(View.VISIBLE);
                    return false;
                } else {
                    tvMailError.setVisibility(View.INVISIBLE);
                }
                break;
            case AppConstants.TYPE.PHONE:
                if (!Util.isValidPhoneNumber(value)) {
                    tvMailError.setText(getString(R.string.invailid_email_phone));
                    tvMailError.setVisibility(View.VISIBLE);
                    return false;
                } else {
                    tvMailError.setVisibility(View.INVISIBLE);
                }
                if (value.length()<10){
                    tvMailError.setText(getString(R.string.mobile_number_error));
                    tvMailError.setVisibility(View.VISIBLE);
                    return false;
                } else {
                    tvMailError.setVisibility(View.INVISIBLE);
                }
                break;
        }
        return true;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (googleUtil.mGoogleApiClient != null) {
            googleUtil.mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (googleUtil.mGoogleApiClient != null && googleUtil.mGoogleApiClient.isConnected()) {
            googleUtil.mGoogleApiClient.stopAutoManage(this);
            googleUtil.mGoogleApiClient.disconnect();
        }
    }

    private void resetPasswordOptionsDialog(final String value) {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_reset_password);

        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        TextView tvSetNewPass = (TextView) dialog.findViewById(R.id.tv_set_new_pass);
        progressBar = (ProgressBar) dialog.findViewById(R.id.progress_loader);
        tvMsg = (TextView) dialog.findViewById(R.id.tv_cnf_msg);
        final TextInputLayout tilEmailAndNumber = (TextInputLayout) dialog.findViewById(R.id.til_email);
        final RadioGroup rgOptions = (RadioGroup) dialog.findViewById(R.id.rg_reset_password);
        final RadioButton rbEmail = (RadioButton) dialog.findViewById(R.id.rb_email);
        final RadioButton rbMobile = (RadioButton) dialog.findViewById(R.id.rb_mobile);
        tilEmailAndNumber.getEditText().setText(value);

        if(value.matches("\\d+(?:\\.\\d+)?")){
            tilEmailAndNumber.setHint(getString(R.string.mobile_number));
            rbMobile.setChecked(true);
            tvMsg.setText(getString(R.string.an_otp_will_be_send_to_your_registered_number));
        }
        else{
            tilEmailAndNumber.setHint(getString(R.string.hint_email));
            rbEmail.setChecked(true);
            tvMsg.setText(getString(R.string.an_otp_will_be_send_to_your_registered_email));
        }



        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvSetNewPass.setOnClickListener(new View.OnClickListener() {
            public String reqType;

            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(tilEmailAndNumber.getEditText().getText().toString().trim())){
                    tilEmailAndNumber.setError(getString(R.string.empty_email));
                    return;
                } else {
                    tilEmailAndNumber.setError(null);
                    tilEmailAndNumber.setErrorEnabled(false);
                }
                int checkedRadioButtonId = rgOptions.getCheckedRadioButtonId();
                if (checkedRadioButtonId == R.id.rb_email){
                    reqType = "1";
                    if (!Util.isValidEmail(tilEmailAndNumber.getEditText().getText().toString().trim())) {
                        tilEmailAndNumber.setError(getString(R.string.error_invalid_email));
                        return ;
                    } else {
                        tilEmailAndNumber.setError(null);
                        tilEmailAndNumber.setErrorEnabled(false);
                    }
                } else {
                    reqType = "2";
                    if (!Util.isValidPhoneNumber(tilEmailAndNumber.getEditText().getText().toString().trim())) {
                        tilEmailAndNumber.setError(getString(R.string.error_invalid_mobile));
                        return;
                    } else {
                        tilEmailAndNumber.setError(null);
                        tilEmailAndNumber.setErrorEnabled(false);
                    }
                    if (tilEmailAndNumber.getEditText().getText().toString().trim().length()<10){
                        tilEmailAndNumber.setError(getString(R.string.mobile_number_error));
                        return;
                    } else {
                        tilEmailAndNumber.setError(null);
                        tilEmailAndNumber.setErrorEnabled(false);
                    }
                }
                changeThroughMobileNumber(reqType,tilEmailAndNumber.getEditText().getText().toString().trim());
            }
        });

        rgOptions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_email:
                        tilEmailAndNumber.setHint(getString(R.string.hint_email));
                        rbEmail.setChecked(true);
                        tvMsg.setText(getString(R.string.an_otp_will_be_send_to_your_registered_email));
                        tilEmailAndNumber.getEditText().setText("");
//                        dialog.dismiss();
//                        changeThroughEmail(value);
                        break;
                    case R.id.rb_mobile:
                        tilEmailAndNumber.setHint(getString(R.string.mobile_number));
                        rbMobile.setChecked(true);
                        tvMsg.setText(getString(R.string.an_otp_will_be_send_to_your_registered_number));
                        tilEmailAndNumber.getEditText().setText("");
//                        dialog.dismiss();
//                        changeThroughMobileNumber(value);
                        break;
                }
            }
        });
        showProgressDialog();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void changeThroughEmail(String value) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.EMAIL, value);
        startNextActivity(bundle, ActivityResetPasswordByEmailMsg.class);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void changeThroughMobileNumber(String reqType, String value) {
        this.value = value;
        HttpParamObject httpParamObject = ApiRequestGenerator.generateOtp(reqType, value);
        executeTask(AppConstants.TASK_CODES.GENERATE_OTP, httpParamObject);
        progressBar.setVisibility(View.VISIBLE);
    }
}
