package com.aboutstays.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.MenuItemCompat;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.enums.LaundryItemType;
import com.aboutstays.fragment.LaundryItemFragment;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.cart.LaundryCartResponse;
import com.aboutstays.model.cart.LaundryCartItemData;
import com.aboutstays.model.cart.ListLaundryItemOrderDetail;
import com.aboutstays.model.laundry.LaundryApiResponse;
import com.aboutstays.model.laundry.LaundryData;
import com.aboutstays.model.laundry.SupportedLaundryTypeList;
import com.aboutstays.model.laundry.SupportedLaundryUIModel;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.cunoraz.gifview.library.GifView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import simplifii.framework.ListAdapters.CustomExpandableListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

public class LaundryActivity extends AppBaseActivity implements SearchView.OnQueryTextListener, LaundryItemFragment.AddToCartListener, CustomExpandableListAdapter.ExpandableListener, ExpandableListView.OnChildClickListener {

    //    private CustomListAdapter listAdapter;
    private List<SupportedLaundryTypeList> laundryList = new ArrayList<>();
    private ExpandableListView laundryListView;
    private LinkedHashMap<String, List<SupportedLaundryUIModel>> mapExpandable = new LinkedHashMap<>();
    private List<String> listHeader = new ArrayList<>();
    private CustomExpandableListAdapter customExpandableListAdapter;
    private GeneralServiceModel generalServiceModel;
    private String serviceId;
    private MenuItem searchMenuItem;
    private SearchView searchView;
    private LaundryData laundryData;
    private FrameLayout fragContainer;
    private LaundryItemFragment laundryItemFragment;
    private Bundle bundle = new Bundle();
    private String staysId;
    private String userId;
    private LaundryCartItemData laundryCartData;
    private TextView tv;
    private String gsId;
    private FrameLayout frameLayout;
    private GifView gifView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laundry);
        registerCartReceiver();
        initToolBar("Laundry");
        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            userId = sessionInstance.getUserId();
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null) {
                serviceId = generalServiceModel.getGeneralServiceData().getHsId();
                staysId = generalServiceModel.getHotelStaysList().getStaysPojo().getStaysId();
                gsId = generalServiceModel.getGeneralServiceData().getId();
            }
        }

        frameLayout = (FrameLayout) findViewById(R.id.lay_frame_loader);
        gifView = (GifView) findViewById(R.id.gif);

        laundryListView = (ExpandableListView) findViewById(R.id.lv_laundry);
        customExpandableListAdapter = new CustomExpandableListAdapter(this, R.layout.row_header_expandable, R.layout.row_room_service, mapExpandable, this);
        laundryListView.setAdapter(customExpandableListAdapter);
//        listAdapter = new CustomListAdapter(this, R.layout.row_room_service, laundryList, this);
//        laundryListView.setAdapter(listAdapter);
        laundryListView.setEmptyView(findViewById(R.id.tv_empty_view));
        laundryListView.setOnChildClickListener(this);
        customExpandableListAdapter.notifyDataSetChanged();
        fragContainer = (FrameLayout) findViewById(R.id.frame_frag_container);
        laundryItemFragment = LaundryItemFragment.getInstance(this);
        getSupportFragmentManager().beginTransaction().add(R.id.frame_frag_container, laundryItemFragment).commit();
    }

    private void setFooter() {
        LayoutInflater inflater = getLayoutInflater();
        View inflate = inflater.inflate(R.layout.view_massege, laundryListView, false);
        TextView tvMsg = (TextView) inflate.findViewById(R.id.tv_msg);
        if (laundryData != null){
            if (laundryData.isSwiftDeliveryEnabled()){
                tvMsg.setText(laundryData.getSwiftDeliveryMessage());
            } else {
                inflate.setVisibility(View.GONE);
            }
        }
        inflate.setPadding(32,32,32,0);
        laundryListView.addFooterView(inflate);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAllCartItem();
    }

    @Override
    public void showProgressDialog() {
        super.showProgressDialog();
        frameLayout.setVisibility(View.VISIBLE);
        gifView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        super.hideProgressBar();
        frameLayout.setVisibility(View.GONE);
        gifView.setVisibility(View.GONE);
    }

    private void getAllCartItem() {
        if (generalServiceModel != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.getAllLaundryCartItem(userId, staysId, generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId(), "3");
            executeTask(AppConstants.TASK_CODES.GET_LAUNDRY_CART_DETAILS, httpParamObject);
        }
    }

    private void getAllLaundryServices() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getLaundryById(serviceId);
        executeTask(AppConstants.TASK_CODES.LAUNDRY_DATA, httpParamObject);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.LAUNDRY_DATA:
                LaundryApiResponse laundryApiResponse = (LaundryApiResponse) response;
                if (laundryApiResponse != null && laundryApiResponse.getError() == false) {
                    laundryData = laundryApiResponse.getData();
                    if (laundryData != null) {
                        setFooter();
                        List<SupportedLaundryTypeList> supportedLaundryTypeList = laundryData.getSupportedLaundryTypeList();
                        if (!CollectionUtils.isEmpty(supportedLaundryTypeList)) {
                            laundryList.clear();
                            laundryList.addAll(supportedLaundryTypeList);
                            filterList(laundryData);
                        }
                        customExpandableListAdapter.notifyDataSetChanged();
                    }
                }
                break;
            case AppConstants.TASK_CODES.GET_LAUNDRY_CART_DETAILS:
                refreshCartData();
                customExpandableListAdapter.notifyDataSetChanged();
                getAllLaundryServices();
                hideProgressBar();
                break;
        }
    }

    private void filterList(LaundryData laundryData) {
        listHeader.clear();
        mapExpandable.clear();
        List<SupportedLaundryTypeList> laundryList = laundryData.getSupportedLaundryTypeList();
        boolean swiftDeliveryEnabled = laundryData.isSwiftDeliveryEnabled();
        for (SupportedLaundryTypeList laundryTypeList : laundryList) {
            Boolean sameDayAvailable = laundryTypeList.getSameDayAvailable();
            String allday = getString(R.string.all);
            SupportedLaundryUIModel uiModel = new SupportedLaundryUIModel(laundryTypeList, true);
            setFilterList(allday, uiModel);
            if (swiftDeliveryEnabled) {
                String keySameDayAvailable = getString(R.string.same_day_delivery);
                uiModel = new SupportedLaundryUIModel(laundryTypeList, false);
                setFilterList(keySameDayAvailable, uiModel);
            }

        }
        refreshUiModelData();
        refreshParentData();
        customExpandableListAdapter.notifyDataSetChanged();
    }

    private void refreshParentData() {
        listHeader.clear();
        Iterator<String> iterator = mapExpandable.keySet().iterator();
        while (iterator.hasNext()) {
            listHeader.add(iterator.next());
        }
        customExpandableListAdapter = new CustomExpandableListAdapter(this, R.layout.row_header_expandable, R.layout.row_room_service, mapExpandable, this);
        laundryListView.setAdapter(customExpandableListAdapter);
        for (int x = 0; x < listHeader.size(); x++) {
            laundryListView.expandGroup(x);
        }
    }

    private void setFilterList(String keySameDayAvailable, SupportedLaundryUIModel uiChild) {
        boolean isContain = mapExpandable.containsKey(keySameDayAvailable);
        if (isContain) {
            List<SupportedLaundryUIModel> supportedLaundryTypeLists = mapExpandable.get(keySameDayAvailable);
            supportedLaundryTypeLists.add(uiChild);
        } else {
            List<SupportedLaundryUIModel> supportedLaundryTypeLists = new ArrayList<>();
            supportedLaundryTypeLists.add(uiChild);
            mapExpandable.put(keySameDayAvailable, supportedLaundryTypeLists);
        }
    }
    public void refreshUiModelData(){
        for(Map.Entry<String, List<SupportedLaundryUIModel>> listEntry:mapExpandable.entrySet()){
            List<SupportedLaundryUIModel> listEntryValue = listEntry.getValue();
            for(SupportedLaundryUIModel uiChild:listEntryValue){
                uiChild.setItemCount(0);
                if (laundryCartData != null) {
                    List<ListLaundryItemOrderDetail> dataList = laundryCartData.getListLaundryItemOrderDetails();
                    if (!CollectionUtils.isEmpty(dataList)) {
                        for (ListLaundryItemOrderDetail orderDetailList : dataList) {
                            if (uiChild.getType() == orderDetailList.getType() && uiChild.isNormalDay()!= orderDetailList.isSameDaySelected()) {
                                uiChild.setItemCount(uiChild.getItemCount() + 1);
                                uiChild.setShowCount(true);
                            }
                        }
                    }
                }
            }
        }
    }

    private void refreshCartData() {
        laundryCartData = LaundryCartResponse.getInstance();
        if (laundryCartData.getListLaundryItemOrderDetails().size() > 0) {
            tv.setText(laundryCartData.getListLaundryItemOrderDetails().size() + "");
            tv.setVisibility(View.VISIBLE);
        } else {
            tv.setText("0");
            tv.setVisibility(View.GONE);
        }
        refreshUiModelData();
        customExpandableListAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_service, menu);

        MenuItem item = menu.findItem(R.id.action_room_service);
        MenuItemCompat.setActionView(item, R.layout.action_menu_count_layout);
        RelativeLayout notifCount = (RelativeLayout) MenuItemCompat.getActionView(item);

        ImageView iv = (ImageView) notifCount.findViewById(R.id.iv_roomservice);
        iv.setColorFilter(Color.WHITE);
        iv.setImageResource(R.mipmap.laundry);
        iv.setOnClickListener(this);

        tv = (TextView) notifCount.findViewById(R.id.actionbar_notifcation_textview);
        tv.setVisibility(View.GONE);

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
//        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_roomservice:
                OrderDetailActivity.startActivity(LaundryActivity.this, gsId, staysId, generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId(), AppConstants.STAYS_TYPE.LAUNDRY, AppConstants.FROM_ORDERING.LAUNDRY);
                break;
        }
    }

    /*@Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_room_service, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        SupportedLaundryTypeList supportedLaundryTypeList = laundryList.get(position);
        supportedLaundryTypeList.setItemCount(0);
        if (laundryCartData != null) {
            List<ListLaundryItemOrderDetail> dataList = laundryCartData.getListLaundryItemOrderDetails();
            if (!CollectionUtils.isEmpty(dataList)) {
                for (ListLaundryItemOrderDetail orderDetailList : dataList) {
                    if (supportedLaundryTypeList.getType() == orderDetailList.getType()) {
                        supportedLaundryTypeList.setItemCount(supportedLaundryTypeList.getItemCount() + 1);
                    }
                }
            }
        }

        if (!TextUtils.isEmpty(supportedLaundryTypeList.getImageUrl()) && !TextUtils.isEmpty(supportedLaundryTypeList.getTypeName())) {
            Picasso.with(this).load(supportedLaundryTypeList.getImageUrl()).into(holder.titleImage);
            holder.laundryTitle.setText(supportedLaundryTypeList.getTypeName());
        } else {
            final Integer laundryType = supportedLaundryTypeList.getType();
            LaundryItemType itemType = LaundryItemType.findByCode(laundryType);
            if (itemType != null) {
                String displayName = itemType.getDisplayName();
                int imageUrl = itemType.getImageUrl();
                holder.laundryTitle.setText(displayName);
                holder.titleImage.setImageResource(imageUrl);
            }
        }

        if (supportedLaundryTypeList.getItemCount() >= 1) {
            holder.rlCount.setVisibility(View.VISIBLE);
//            holder.imageRightAngle.setVisibility(View.GONE);
            holder.tvItemCount.setText(supportedLaundryTypeList.getItemCount() + "");
        } else {
            holder.rlCount.setVisibility(View.GONE);
        }

        holder.tvTime.setVisibility(View.GONE);

        return convertView;
    }*/


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.length() >= 3) {
            fragContainer.setVisibility(View.VISIBLE);
            laundryItemFragment.sendQueryData(serviceId, newText, staysId, laundryCartData);
        } else {
            fragContainer.setVisibility(View.GONE);
            getAllCartItem();
        }
        if (TextUtils.isEmpty(newText)) {
            laundryItemFragment.callBack();
        }
        return true;
    }

    @Override
    public void searchCallback() {
        fragContainer.setVisibility(View.GONE);
        getAllCartItem();
    }

    @Override
    public void updateCart() {
        refreshCartData();
    }

/*    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final SupportedLaundryTypeList ob = laundryList.get(position);
        Intent intent = new Intent(LaundryActivity.this, LaundryItemActivity.class);
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.LAUNDRY_ITEMS, laundryData);
        bundle.putString(AppConstants.BUNDLE_KEYS.LAUNDRY_TYPE, ob.getType() + "");
        bundle.putString(AppConstants.BUNDLE_KEYS.STAYS_ID, staysId);
        bundle.putString(AppConstants.BUNDLE_KEYS.GS_ID, gsId);
        bundle.putString(AppConstants.BUNDLE_KEYS.HOTEL_ID, generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId());
        intent.putExtras(bundle);
        startActivityForResult(intent, AppConstants.REQUEST_CODE.LAUNDRY_CART_ITEM);
    }*/

    @Override
    public int getChildSize(int parentPosition) {
        return mapExpandable.get(listHeader.get(parentPosition)).size();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        HolderParent holderParent;
        if (convertView == null) {
            convertView = inflater.inflate(resourceId, null);
            holderParent = new HolderParent(convertView);
            convertView.setTag(holderParent);
        } else {
            holderParent = (HolderParent) convertView.getTag();
        }

        String title = listHeader.get(groupPosition);
        holderParent.bindData(title, isExpanded);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        HolderChild holderChild;
        if (convertView == null) {
            convertView = inflater.inflate(resourceId, parent, false);
            holderChild = new HolderChild(convertView);
            convertView.setTag(holderChild);
        } else {
            holderChild = (HolderChild) convertView.getTag();
        }

        View viewDescription = convertView.findViewById(R.id.view_line_description);
        if (isLastChild) {
            viewDescription.setVisibility(View.INVISIBLE);
        } else {
            viewDescription.setVisibility(View.VISIBLE);
        }
        SupportedLaundryUIModel uiModel = mapExpandable.get(listHeader.get(groupPosition)).get(childPosition);
        if (!TextUtils.isEmpty(uiModel.getImageUrl()) && !TextUtils.isEmpty(uiModel.getTypeName())) {
            Picasso.with(this).load(uiModel.getImageUrl()).placeholder(R.drawable.progress_animation).into(holderChild.titleImage);
            holderChild.laundryTitle.setText(uiModel.getTypeName());
        } else {
            final Integer laundryType = uiModel.getType();
            LaundryItemType itemType = LaundryItemType.findByCode(laundryType);
            if (itemType != null) {
                String displayName = itemType.getDisplayName();
                int imageUrl = itemType.getImageUrl();
                holderChild.laundryTitle.setText(displayName);
                holderChild.titleImage.setImageResource(imageUrl);
            }
        }

//        switch (groupPosition) {
//            case 0:
//                if (supportedLaundryTypeList.isSameDaySelected()) {
//                    holderChild.rlCount.setVisibility(View.GONE);
//                } else if (!supportedLaundryTypeList.isSameDaySelected() && supportedLaundryTypeList.getItemCount() > 1) {
//                    holderChild.rlCount.setVisibility(View.VISIBLE);
//                    holderChild.tvItemCount.setText(supportedLaundryTypeList.getItemCount() + "");
//                }
//                break;
//
//            case 1:
//                if (!supportedLaundryTypeList.isSameDaySelected()) {
//                    holderChild.rlCount.setVisibility(View.GONE);
//                } else if (supportedLaundryTypeList.isSameDaySelected() && supportedLaundryTypeList.getItemCount() > 1) {
//                    holderChild.rlCount.setVisibility(View.VISIBLE);
//                    holderChild.tvItemCount.setText(supportedLaundryTypeList.getItemCount() + "");
//                }
//                break;
//        }

        if (uiModel.isShowCount()&&uiModel.getItemCount()>0) {
            holderChild.rlCount.setVisibility(View.VISIBLE);
            holderChild.tvItemCount.setText(uiModel.getItemCount() + "");
        } else {
            holderChild.rlCount.setVisibility(View.GONE);
        }

        customExpandableListAdapter.notifyDataSetChanged();
        holderChild.tvTime.setVisibility(View.GONE);

        return convertView;
    }

    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int parentPosition, int childPosition, long l) {
        final SupportedLaundryTypeList ob = laundryList.get(childPosition);
        switch (parentPosition) {
            case 0:
                switchNextActitity(ob, false);
                break;
            case 1:
                switchNextActitity(ob, true);
                break;
        }
        return false;
    }

    private void switchNextActitity(SupportedLaundryTypeList ob, boolean fromSameDay) {
        Intent intent = new Intent(LaundryActivity.this, LaundryItemActivity.class);
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.LAUNDRY_ITEMS, laundryData);
        bundle.putString(AppConstants.BUNDLE_KEYS.LAUNDRY_TYPE, ob.getType() + "");
        bundle.putString(AppConstants.BUNDLE_KEYS.STAYS_ID, staysId);
        bundle.putString(AppConstants.BUNDLE_KEYS.GS_ID, gsId);
        bundle.putBoolean(AppConstants.BUNDLE_KEYS.FROM_SAME_DAY, fromSameDay);
        bundle.putString(AppConstants.BUNDLE_KEYS.HOTEL_ID, generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId());
        intent.putExtras(bundle);
        startActivityForResult(intent, AppConstants.REQUEST_CODE.LAUNDRY_CART_ITEM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else if (resultCode == AppConstants.RESULT_CODE.RESULT_NULL){
            getAllCartItem();
            hideProgressBar();
        }
    }

/*    class Holder {
        TextView laundryTitle, tvTime, tvItemCount;
        ImageView titleImage, arrowImage;
        RelativeLayout rlCount;

        public Holder(View view) {
            laundryTitle = (TextView) view.findViewById(R.id.tv_title);
            titleImage = (ImageView) view.findViewById(R.id.iv_title_image1);
            arrowImage = (ImageView) view.findViewById(R.id.iv_right_angle);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_count);
            tvItemCount = (TextView) view.findViewById(R.id.tv_item_count);
        }
    }*/

    class HolderChild {
        TextView laundryTitle, tvTime, tvItemCount;
        ImageView titleImage, arrowImage;
        RelativeLayout rlCount;

        public HolderChild(View view) {
            laundryTitle = (TextView) view.findViewById(R.id.tv_title);
            titleImage = (ImageView) view.findViewById(R.id.iv_title_image1);
            arrowImage = (ImageView) view.findViewById(R.id.iv_right_angle);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_count);
            tvItemCount = (TextView) view.findViewById(R.id.tv_item_count);
        }
    }

    class HolderParent {
        TextView textView;
        ImageView ivIndicator;

        public HolderParent(View view) {
            textView = (TextView) view.findViewById(R.id.tv_men_women);
            ivIndicator = (ImageView) view.findViewById(R.id.iv_indicator);
        }

        public void bindData(String title, boolean isExpand) {
            textView.setText(title);
            if (isExpand) {
                ivIndicator.setImageResource(R.mipmap.up_arrow_white);
            } else {
                ivIndicator.setImageResource(R.mipmap.arrow_down_white);
            }
        }
    }


    @Override
    public void onCartUpdate() {
//        getAllCartItem();
        refreshCartData();
    }

    @Override
    public void onCartClear() {
        getAllCartItem();
//        refreshCartData();
        if (searchView != null) {
            laundryItemFragment.sendQueryData(serviceId, searchView.getQuery().toString(), staysId, laundryCartData);
        }
    }
}
