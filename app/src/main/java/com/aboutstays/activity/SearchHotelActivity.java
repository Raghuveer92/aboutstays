package com.aboutstays.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.enums.Partnership;
import com.aboutstays.model.stayline.GeneralSearchInformation;
import com.aboutstays.model.stayline.HotelBasicInfo;
import com.aboutstays.model.stayline.SearchedData;
import com.aboutstays.model.stayline.SearchedHotelAddress;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class SearchHotelActivity extends AppBaseActivity implements CustomListAdapterInterface{
    private CustomListAdapter listAdapter;
    private List<HotelBasicInfo> hotelList = new ArrayList<>();
    private ListView searchListView;
    private SearchedData searchData;
    private String cityName, hotelName;
    private static final int REQUEST_SUGGEST_HOTEL = 1;
    private String nameHotel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_hotel);
        initToolBar(getString(R.string.retrieve_stay));
        searchListView = (ListView) findViewById(R.id.listview_search_hotels);
        listAdapter = new CustomListAdapter(this, R.layout.row_search_hotel, hotelList, this);
        searchListView.setAdapter(listAdapter);

        Bundle getSearchData = getIntent().getExtras();
        if (getSearchData != null) {
            searchData = (SearchedData) getSearchData.getSerializable(AppConstants.BUNDLE_KEYS.SEARCH_HOTEL_RESPONSE);
            if (searchData != null) {
                List<HotelBasicInfo> hotelBasicInfo = searchData.getHotelBasicInfo();
                if (hotelBasicInfo != null) {
                    hotelList.addAll(hotelBasicInfo);
                }
            }
            cityName = getSearchData.getString(AppConstants.BUNDLE_KEYS.CITY_NAME);
            hotelName = getSearchData.getString(AppConstants.BUNDLE_KEYS.HOTEL_NAME);
            addHeaderAndFooter(cityName, hotelName);
            listAdapter.notifyDataSetChanged();
        }

        setOnClickListener(R.id.text_not_in_list);
    }

    private void addHeaderAndFooter(String cityName, String hotelName) {
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.view_search_header, searchListView, false);
        TextInputLayout tilHotelName = (TextInputLayout) header.findViewById(R.id.til_hotel_name);
        String hotelNameCity = null;
        if (!TextUtils.isEmpty(cityName)) {
            hotelNameCity = cityName;
            nameHotel = hotelNameCity;
        }
        if (!TextUtils.isEmpty(hotelName)) {
            if (hotelNameCity != null) {
                nameHotel = hotelName +", " + hotelNameCity;
            } else {
                hotelNameCity = hotelName;
                nameHotel = hotelName;
            }
        }
        tilHotelName.getEditText().setText(nameHotel);
        searchListView.addHeaderView(header, null, false);

        LayoutInflater inflater1 = getLayoutInflater();
        ViewGroup footer = (ViewGroup) inflater1.inflate(R.layout.view_search_footer, searchListView, false);
        searchListView.addFooterView(footer, null, false);
    }

//    private void setDataInList(SearchedData searchData) {
//        for(int i=0; i < 4; i++) {
//            HotelModel ob = new HotelModel();
//            ob.setHotelName("The Taj hotel in");
//            ob.setHotelAddrass("Banglore,Country");
//            ob.setImgUrl(R.mipmap.hilton);
//            ob.setRightImgUrl(R.mipmap.handshake_color);
//            hotelList.add(ob);
//        }
//        listAdapter.notifyDataSetChanged();
//    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final HotelBasicInfo hotelBasicInfo = hotelList.get(position);
        Partnership partnerByType = Partnership.findPartnerByType(hotelBasicInfo.getPartnershipType());
        if (partnerByType != null) {
            switch (partnerByType) {
                case PARTNER:
                    holder.ivPartner.setVisibility(View.VISIBLE);
                    holder.ivPartner.setColorFilter(Color.RED);
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle b = new Bundle();
                            b.putSerializable(AppConstants.BUNDLE_KEYS.SEARCH_HOTEL_RESPONSE, hotelBasicInfo);
                            startNextActivity(b, RetrieveBookingActivity.class);
                        }
                    });
                    break;
                case NON_PARTNER:
                    holder.ivPartner.setVisibility(View.GONE);
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle b = new Bundle();
                            b.putSerializable(AppConstants.BUNDLE_KEYS.SEARCH_HOTEL_RESPONSE, hotelBasicInfo);
                            startNextActivity(b, AddToStaylineActivity.class);
                        }
                    });

            }
        } else {
            holder.ivPartner.setVisibility(View.GONE);
        }

        GeneralSearchInformation generalInformation = hotelBasicInfo.getGeneralInformation();
        if (generalInformation != null) {
            holder.tvHotelName.setText(generalInformation.getName());
            SearchedHotelAddress address = generalInformation.getAddress();
            if (address != null) {
                holder.tvHotelAddress.setText(address.getCompleteAddress());
            } else {
                holder.tvHotelAddress.setText("");
            }
            if (!TextUtils.isEmpty(generalInformation.getLogoUrl())) {
                Picasso.with(this).load(generalInformation.getLogoUrl())
                        .placeholder(R.drawable.progress_animation)
                        .into(holder.ivHotelRoom);
            } else {
                holder.ivHotelRoom.setImageResource(R.drawable.progress_animation);
            }
        } else {
            holder.tvHotelName.setText("");
            holder.tvHotelAddress.setText("");
            holder.ivHotelRoom.setImageResource(R.drawable.progress_animation);
        }


//                    holder.ivHotelRoom.setImageResource(ob.getImgUrl());
//                    holder.ivPartner.setImageResource(ob.getRightImgUrl());


//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Bundle b = new Bundle();
//                b.putSerializable(AppConstants.BUNDLE_KEYS.SEARCH_HOTEL_RESPONSE, searchHotelList);
//                startNextActivity(b, RetrieveBookingActivity.class);
//            }
//        });


        return convertView;
    }


    class Holder {
        ImageView ivHotelRoom, ivPartner;
        TextView tvHotelName, tvHotelAddress;

        public Holder(View view) {
            ivHotelRoom = (ImageView) view.findViewById(R.id.iv_hotel_icon);
            ivPartner = (ImageView) view.findViewById(R.id.iv_image_partner);
            tvHotelName = (TextView) view.findViewById(R.id.tv_hotel_name_retrieve);
            tvHotelAddress = (TextView) view.findViewById(R.id.tv_hotel_address_retrieve);
        }

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch(v.getId()) {
            case R.id.text_not_in_list:

                startNextActivityForResult(getIntent().getExtras(), SuggestHotelsActivity.class, REQUEST_SUGGEST_HOTEL);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_SUGGEST_HOTEL :
                    finish();
                    break;
            }
        }
    }
}

