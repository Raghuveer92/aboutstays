package com.aboutstays.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.enums.CheckInDetails;
import com.aboutstays.enums.DocType;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.model.earlycheckIn.EarlyCheckInData;
import com.aboutstays.model.initiate_checkIn.CheckInData;
import com.aboutstays.model.initiate_checkIn.CheckinModel;
import com.aboutstays.model.initiate_checkIn.Company;
import com.aboutstays.model.initiate_checkIn.EarlyCheckinRequest;
import com.aboutstays.model.initiate_checkIn.InitiateCheckInResponse;
import com.aboutstays.model.initiate_checkIn.SpecialRequest;
import com.aboutstays.model.stayline.HotelStaysList;
import com.aboutstays.services.UserServices;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.pojo.IdentityDoc;
import simplifii.framework.rest.response.requests.StayPref;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.response.UserProfileData;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

public class CheckInDetailsActivity extends AppBaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    private ListView listCheckinDetails;
    private CustomListAdapter listAdapter;
    private Button btnSubmit;
    private TextView tvEmptyView;
    private List<CheckinModel> checkinModelList = new ArrayList<>();
    private RadioGroup rgTripType;
    private RadioButton rbOfficial, rbPersonal;
    private int selectedPosition;
    private CheckInData updtedCheckInData = new CheckInData();
    private String roomId;
    private String hotelId, staysId, userId;
    private boolean officialTrip;
    private List<IdentityDoc> identityDoc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in_details);
        initToolBar(getString(R.string.checkIn_details));

        UserServices.startUpdateUser(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            HotelStaysList hotelStaysList = (HotelStaysList) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (hotelStaysList != null) {
                if (hotelStaysList.getStaysPojo().getRoomId() != null) {
                    roomId = hotelStaysList.getStaysPojo().getRoomId();
                    hotelId = hotelStaysList.getStaysPojo().getHotelId();
                    staysId = hotelStaysList.getStaysPojo().getStaysId();
                    userId = hotelStaysList.getStaysPojo().getUserId();
                }
            }
        }


        tvEmptyView = (TextView) findViewById(R.id.tv_empty_view);
        btnSubmit = (Button) findViewById(R.id.btn_initiate_check_in);
        rgTripType = (RadioGroup) findViewById(R.id.rg_trip_type);
        rbOfficial = (RadioButton) findViewById(R.id.rb_oficial);
        rbPersonal = (RadioButton) findViewById(R.id.rb_personal);
        rgTripType.check(R.id.rb_oficial);
        listCheckinDetails = (ListView) findViewById(R.id.list_checkin_details);
        listAdapter = new CustomListAdapter(this, R.layout.row_room_service, checkinModelList, this);
        listCheckinDetails.setAdapter(listAdapter);
        listCheckinDetails.setEmptyView(tvEmptyView);
        listAdapter.notifyDataSetChanged();
        setData();
        setUserSession();

        listCheckinDetails.setOnItemClickListener(this);
        setOnClickListener(R.id.btn_initiate_check_in);
    }

    private void setUserSession() {
        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null){
            if (!TextUtils.isEmpty(sessionInstance.getFirstName()) && !TextUtils.isEmpty(sessionInstance.getLastName()) && !TextUtils.isEmpty(sessionInstance.getEmailId())
                    && !TextUtils.isEmpty(sessionInstance.getNumber())){
                updtedCheckInData.setPersonalInformation(sessionInstance);
                CheckinModel checkInData1 = checkinModelList.get(0);
                checkInData1.setTitle(sessionInstance.getFirstName());
                listAdapter.notifyDataSetChanged();
            }

            if (CollectionUtils.isNotEmpty(sessionInstance.getListStayPreferences())){
                updtedCheckInData.setListStayPreferences(sessionInstance.getListStayPreferences());
                List<StayPref> listStayPreferences = updtedCheckInData.getListStayPreferences();
                this.updtedCheckInData.setListStayPreferences(listStayPreferences);
                StringBuilder stringBuilder = new StringBuilder();
                for (StayPref s : listStayPreferences) {
                    stringBuilder.append(s.getPreferenceName() + ", ");
                }
                CheckinModel checkInData1 = checkinModelList.get(2);
                checkInData1.setTitle(stringBuilder.toString());
                listAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getInitiateCheckIn();
    }

    private void getInitiateCheckIn() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getInitiateCheckin(hotelId, staysId, userId);
        executeTask(AppConstants.TASK_CODES.GET_INITIATE_CHECKIN, httpParamObject);
    }

    private void setData() {
        List<CheckinModel> checkInDetailList = CheckInDetails.getCheckInDetailList(this);
        checkinModelList.addAll(checkInDetailList);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        CheckinModel checkinModel = checkinModelList.get(position);

        if (!TextUtils.isEmpty(checkinModel.getTitle())) {
            holder.tvDesc.setText(checkinModel.getTitle());
            holder.rldesc.setVisibility(View.VISIBLE);
            holder.rlCount.setVisibility(View.GONE);
        } else {
            holder.rldesc.setVisibility(View.GONE);
            if (checkinModel.isMendatry()) {
                holder.tvCount.setText("!");
                holder.rlCount.setVisibility(View.VISIBLE);
            } else {
                holder.rlCount.setVisibility(View.GONE);
            }
        }

        holder.tvItem.setText(checkinModel.getName());
        int imageUrl = checkinModel.getImageUrl();
        holder.imageView.setImageResource(imageUrl);
        holder.tvTime.setVisibility(View.GONE);
//        holder.rlCount.setVisibility(View.VISIBLE);
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        selectedPosition = position;
        CheckInDetails checkInDetails = CheckInDetails.finCheckInDetailsbyType(position);
        if (checkInDetails != null) {
            switch (checkInDetails) {
                case PERSONAL_DETAIL:
                    Intent intent = new Intent(this, PersonalDetailsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(AppConstants.BUNDLE_KEYS.FROM_CHECKIN, true);
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, updtedCheckInData);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, AppConstants.REQUEST_CODE.PERSONAL_INFO_ACTIVITY);
                    break;
                case IDENTITY_DOC:
                    Intent idIntent = new Intent(this, IdentityDocuments.class);
                    Bundle b1 = new Bundle();
                    b1.putBoolean(AppConstants.BUNDLE_KEYS.FROM_CHECKIN, true);
                    b1.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, updtedCheckInData);
                    b1.putSerializable(AppConstants.BUNDLE_KEYS.IDENTITY_DOC, (Serializable) identityDoc);
                    idIntent.putExtras(b1);
                    startActivityForResult(idIntent, AppConstants.REQUEST_CODE.IDENTITY_ACTIVITY);
                    break;
                case PREFERANSE:
                    Intent prefIntent = new Intent(this, StayPreferencesActivity.class);
                    Bundle b2 = new Bundle();
                    b2.putBoolean(AppConstants.BUNDLE_KEYS.FROM_CHECKIN, true);
                    b2.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, updtedCheckInData);
                    prefIntent.putExtras(b2);
                    startActivityForResult(prefIntent, AppConstants.REQUEST_CODE.PREF_ACTIVITY);
                    break;
                case SPECIAL_REQUEST:
                    Intent specialIntent = new Intent(this, SpecialRequestActivity.class);
                    Bundle b3 = new Bundle();
                    b3.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, updtedCheckInData);
                    specialIntent.putExtras(b3);
                    startActivityForResult(specialIntent, AppConstants.REQUEST_CODE.SPECIAL_REQ_ACTIVITY);
                    break;
//                case LOYALTY_PROGRAM:
//                    Intent loyaltyIntent = new Intent(this, LoyaltyProgramActivity.class);
//                    Bundle b4 = new Bundle();
//                    b4.putBoolean(AppConstants.BUNDLE_KEYS.FROM_CHECKIN, true);
//                    b4.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, updtedCheckInData);
//                    loyaltyIntent.putExtras(b4);
//                    startActivityForResult(loyaltyIntent, AppConstants.REQUEST_CODE.LOYALTY_ACTIVITY);
//                    break;
//                case COMPANY:
//                    Intent companyIntent = new Intent(this, SelectCompanyActivity.class);
//                    Bundle b5 = new Bundle();
//                    b5.putBoolean(AppConstants.BUNDLE_KEYS.FROM_CHECKIN, true);
//                    b5.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, updtedCheckInData);
//                    companyIntent.putExtras(b5);
//                    startActivityForResult(companyIntent, AppConstants.REQUEST_CODE.COMPANY_ACTIVITY);
//                    break;
                case EARLY_CHECKIN:
                    Intent earlyCheckinIntent = new Intent(this, EarlyCheckInActivity.class);
                    Bundle b6 = new Bundle();
                    b6.putBoolean(AppConstants.BUNDLE_KEYS.FROM_CHECKIN, true);
                    b6.putString(AppConstants.BUNDLE_KEYS.ROOM_SERVICE_ID, roomId);
                    b6.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, updtedCheckInData);
                    earlyCheckinIntent.putExtras(b6);
                    startActivityForResult(earlyCheckinIntent, AppConstants.REQUEST_CODE.EARLY_CHECKIN_ACTIVITY);
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_initiate_check_in:
                initiateCheckIn();
                break;
        }
    }

    private void initiateCheckIn() {
        updtedCheckInData.setUserId(userId);
        updtedCheckInData.setHotelId(hotelId);
        updtedCheckInData.setStaysId(staysId);
        int checkedRadioButtonId = rgTripType.getCheckedRadioButtonId();
        if (checkedRadioButtonId == rbOfficial.getId()) {
            officialTrip = true;
        } else {
            officialTrip = false;
        }
        updtedCheckInData.setOfficialTrip(officialTrip);

        if (updtedCheckInData != null && updtedCheckInData.getPersonalInformation() != null && updtedCheckInData.getIdentityDocuments() != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.initiateCheckin(updtedCheckInData);
            executeTask(AppConstants.TASK_CODES.INIITIATE_CHECKIN, httpParamObject);
        } else {
            showToast("Please fill mandatory fields");
            return;
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.INIITIATE_CHECKIN:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false) {
                    showSuccessDialoge();
                } else {
                    showToast(apiResponse.getMessage());
                }
                break;
            case AppConstants.TASK_CODES.GET_INITIATE_CHECKIN:
                InitiateCheckInResponse initiateCheckInResponse = (InitiateCheckInResponse) response;
                if (initiateCheckInResponse != null && initiateCheckInResponse.getError() == false) {
                    CheckInData checkInData = initiateCheckInResponse.getData();
                    if (checkInData != null) {
                        String id = checkInData.getCheckinId();
                        if (id != null) {
                            updtedCheckInData.setCheckinId(id);
                        }
                        this.updtedCheckInData = checkInData;
                        setNewData(checkInData);
                        btnSubmit.setText(R.string.modify_checkin);
//                        btnSubmit.setVisibility(View.VISIBLE);
                    }
                }
                break;
        }
    }

    private void showSuccessDialoge() {
        SuccessDialogFragment.show(getSupportFragmentManager(),
                "CHECK IN",
                getString(R.string.request_initiated),
                getString(R.string.confirmation_msg),
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                        setResult(AppConstants.RESULT_CODE.RESULT_DONE);
                        finish();
                    }
                }
        );
    }

    private void setNewData(CheckInData checkInData) {
        Boolean officialTrip = checkInData.getOfficialTrip();
        if (officialTrip == true) {
            rgTripType.check(R.id.rb_oficial);
        } else {
            rgTripType.check(R.id.rb_personal);
        }
        for (int x = 0; x < checkinModelList.size(); x++) {
            CheckInDetails checkInDetails = CheckInDetails.finCheckInDetailsbyType(x);
            if (checkInDetails != null) {
                switch (checkInDetails) {
                    case PERSONAL_DETAIL:
                        if (checkInData.getPersonalInformation() != null) {
                            checkinModelList.get(x).setTitle(checkInData.getPersonalInformation().getFirstName());
                        }
                        break;
                    case IDENTITY_DOC:
                        List<IdentityDoc> identityDocuments = checkInData.getIdentityDocuments();
                        String docName = null;
                        if (CollectionUtils.isNotEmpty(identityDocuments)) {
                            StringBuilder stringBuilder = new StringBuilder();
                            for (IdentityDoc identityDoc : identityDocuments) {
                                docName = identityDoc.getDocType();
                                stringBuilder.append(docName);
//                                if (docType != null) {
//                                    DocType docByType = DocType.findDocByType(docType);
//                                    if (docByType != null) {
//                                        docName = docByType.getDocName();
//                                        stringBuilder.append(docName);
//                                    }
//                                }
                            }
                            checkinModelList.get(x).setTitle(stringBuilder.toString());
                        }
                        break;
                    case PREFERANSE:
                        List<StayPref> listStayPreferences = checkInData.getListStayPreferences();
                        if (CollectionUtils.isNotEmpty(listStayPreferences)) {
                            StringBuilder stringBuilder = new StringBuilder();
                            for (StayPref s : listStayPreferences) {
                                stringBuilder.append(s.getPreferenceName() + " ");
                            }
                            checkinModelList.get(x).setTitle(stringBuilder.toString());
                        }
                        break;
//                    case COMPANY:
//                        List<Company> companies = checkInData.getCompanies();
//                        if (CollectionUtils.isNotEmpty(companies)) {
//                            StringBuilder stringBuilder = new StringBuilder();
//                            for (Company s : companies) {
//                                stringBuilder.append(s.getName() + " ");
//                            }
//                            checkinModelList.get(x).setTitle(stringBuilder.toString());
//                        }
//                        break;
                    case EARLY_CHECKIN:
                        EarlyCheckinRequest earlyCheckinRequest = checkInData.getEarlyCheckinRequest();
                        if (earlyCheckinRequest != null) {
                            checkinModelList.get(x).setTitle(earlyCheckinRequest.getRequestedCheckinTime());
                        }
                        break;
                    case SPECIAL_REQUEST:
                        List<SpecialRequest> listSpecialRequest = checkInData.getSpecialRequests();
                        if (CollectionUtils.isNotEmpty(listSpecialRequest)) {
                            StringBuilder s = new StringBuilder();
                            for (SpecialRequest specialRequest : listSpecialRequest) {
                                if (specialRequest == null) {
                                    continue;
                                }
                                s.append(specialRequest.getRequest());
                            }
                            checkinModelList.get(x).setTitle(s.toString());
                        }
                        break;
                }
            }
        }
        listAdapter.notifyDataSetChanged();
    }

    class Holder {
        ImageView imageView;
        TextView tvItem, tvDesc, tvTime, tvCount;
        RelativeLayout rldesc, rlCount;

        public Holder(View view) {
            imageView = (ImageView) view.findViewById(R.id.iv_title_image1);
            tvItem = (TextView) view.findViewById(R.id.tv_title);
            tvDesc = (TextView) view.findViewById(R.id.tv_text);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_count);
            rldesc = (RelativeLayout) view.findViewById(R.id.rl_text);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            tvCount = (TextView) view.findViewById(R.id.tv_item_count);
            btnSubmit = (Button) findViewById(R.id.btn_initiate_check_in);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            switch (requestCode) {
                case AppConstants.REQUEST_CODE.PERSONAL_INFO_ACTIVITY:
                    if (data != null) {
                        Bundle extras = data.getExtras();
                        if (extras != null) {
                            CheckInData checkInData = (CheckInData) extras.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
                            if (checkInData != null && checkInData.getPersonalInformation() != null) {
                                addPersonalInfoData(checkInData);
                            }
                        }
                    }
                    break;
                case AppConstants.REQUEST_CODE.PREF_ACTIVITY:
                    if (data != null) {
                        Bundle extras = data.getExtras();
                        if (extras != null) {
                            CheckInData checkInData = (CheckInData) extras.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
                            if (checkInData != null && CollectionUtils.isNotEmpty(checkInData.getListStayPreferences())) {
                                addPrefData(checkInData);
                            }
                        }
                    }
                    break;
                case AppConstants.REQUEST_CODE.EARLY_CHECKIN_ACTIVITY:
                    if (data != null) {
                        Bundle extras = data.getExtras();
                        if (extras != null) {
                            CheckInData checkInData = (CheckInData) extras.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
                            if (checkInData.getEarlyCheckinRequest() != null) {
                                addEarlyCheckIn(checkInData);
                            }
                        }
                    }
                    break;
//                case AppConstants.REQUEST_CODE.COMPANY_ACTIVITY:
//                    if (data != null) {
//                        Bundle extras = data.getExtras();
//                        if (extras != null) {
//                            CheckInData checkInData = (CheckInData) extras.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
//                            if (checkInData != null && CollectionUtils.isNotEmpty(checkInData.getCompanies())) {
//                                addCompanyData(checkInData);
//                            }
//                        }
//                    }
//                    break;
                case AppConstants.REQUEST_CODE.IDENTITY_ACTIVITY:
                    if (data != null) {
                        Bundle extras = data.getExtras();
                        if (extras != null) {
                            identityDoc = (List<IdentityDoc>) extras.getSerializable(AppConstants.BUNDLE_KEYS.IDENTITY_DOC);
                            CheckInData checkInData = (CheckInData) extras.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
                            if (checkInData != null && CollectionUtils.isNotEmpty(checkInData.getIdentityDocuments())) {
                                addIdentityData(checkInData);
                            }
                        }
                    }
                    break;
                case AppConstants.REQUEST_CODE.SPECIAL_REQ_ACTIVITY:
                    if (data != null) {
                        Bundle extras = data.getExtras();
                        if (extras != null) {
                            String specialRequest = extras.getString(AppConstants.BUNDLE_KEYS.SPECIAL_REQUEST);
                            setSpecialRequestData(specialRequest);
                        }
                    }
                    break;
            }
        }
    }

    private void setSpecialRequestData(String specialRequest) {
        List<SpecialRequest> listSpecialRequest = new ArrayList<>();
        SpecialRequest request = new SpecialRequest();
        request.setRequest(specialRequest);
        listSpecialRequest.add(request);
        this.updtedCheckInData.setSpecialRequests(listSpecialRequest);
        CheckinModel checkInData1 = checkinModelList.get(selectedPosition);
        checkInData1.setTitle(specialRequest);
        listAdapter.notifyDataSetChanged();
    }

    private void addPersonalInfoData(CheckInData checkInData) {
        this.updtedCheckInData.setPersonalInformation(checkInData.getPersonalInformation());
        CheckinModel checkInData1 = checkinModelList.get(selectedPosition);
        checkInData1.setTitle(checkInData.getPersonalInformation().getFirstName());
        listAdapter.notifyDataSetChanged();
    }

    private void addPrefData(CheckInData checkInData) {
        List<StayPref> listStayPreferences = checkInData.getListStayPreferences();
        this.updtedCheckInData.setListStayPreferences(listStayPreferences);
        StringBuilder stringBuilder = new StringBuilder();
        for (StayPref s : listStayPreferences) {
            stringBuilder.append(s.getPreferenceName() + " ");
        }
        CheckinModel checkInData1 = checkinModelList.get(selectedPosition);
        checkInData1.setTitle(stringBuilder.toString());
        listAdapter.notifyDataSetChanged();
    }

    private void addEarlyCheckIn(CheckInData checkInData) {
        this.updtedCheckInData.setEarlyCheckinRequest(checkInData.getEarlyCheckinRequest());
        CheckinModel checkInData1 = checkinModelList.get(selectedPosition);
        checkInData1.setTitle(checkInData.getEarlyCheckinRequest().getRequestedCheckinTime());
        listAdapter.notifyDataSetChanged();
    }

    private void addCompanyData(CheckInData checkInData) {
        List<Company> companies = checkInData.getCompanies();
        this.updtedCheckInData.setCompanies(companies);
        StringBuilder stringBuilder = new StringBuilder();
        for (Company s : companies) {
            stringBuilder.append(s.getName() + " ");
        }
        CheckinModel checkInData1 = checkinModelList.get(selectedPosition);
        checkInData1.setTitle(stringBuilder.toString());
        listAdapter.notifyDataSetChanged();
    }

    private void addIdentityData(CheckInData checkInData) {
        List<IdentityDoc> identityDocuments = checkInData.getIdentityDocuments();
        this.updtedCheckInData.setIdentityDocuments(identityDocuments);
        StringBuilder stringBuilder = new StringBuilder();
        for (IdentityDoc s : identityDocuments) {
            stringBuilder.append(s.getDocType() + " ");
        }
        CheckinModel checkInData1 = checkinModelList.get(selectedPosition);
        checkInData1.setTitle(stringBuilder.toString());
        listAdapter.notifyDataSetChanged();


    }
}
