package com.aboutstays.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.adapter.MainChipviewAdapter;
import com.aboutstays.model.ActionTag;
import com.aboutstays.model.hotels.Amenity;
import com.aboutstays.model.hotels.OtherServiceData;
import com.aboutstays.model.rooms_category.RoomsCategoryData;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

public class RoomNameActivity extends AppBaseActivity {

    private TextView tvName, tvRoomTypeAndno, tvDescription, tvRoomSize, tvMaxOccupancy, tvChildCount;
    private ImageView ivHotel, ivAdult, ivChild;
    private RoomsCategoryData roomsCategoryData;
    private String roomName;
    private MainChipviewAdapter adapterRoomEmenities;
    private ChipView chipViewRoomEmenities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_name);

        findViews();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            roomsCategoryData = (RoomsCategoryData) extras.getSerializable(AppConstants.BUNDLE_KEYS.ROOMS_DATA);
            if (roomsCategoryData != null) {
                roomName = roomsCategoryData.getName();
                List<Chip> chipList = initCardsChipList();
                adapterRoomEmenities = new MainChipviewAdapter(this, chipList, 1);
                chipViewRoomEmenities = (ChipView) findViewById(R.id.chip_view_room_emenities);
                adapterRoomEmenities.setChipList(chipList);
                chipViewRoomEmenities.setAdapter(adapterRoomEmenities);
            }
            setData();
        }

        if (!TextUtils.isEmpty(roomName)) {
            initToolBar(roomName);
        } else {
            initToolBar(getString(R.string.room_name));
        }

        setMapview();
        setOnClickListener(R.id.more_room_emenities);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.more_room_emenities:
                OtherServicesActivity.startActivity(this, getString(R.string.amenities), new OtherServiceData(roomsCategoryData.getInRoomAmenities()));
                break;
        }
    }

    private List<Chip> initCardsChipList() {
        ArrayList<Chip> list = new ArrayList<>();
        List<Amenity> listCards = new ArrayList<>();
        List<Amenity> amenities = roomsCategoryData.getInRoomAmenities();
        if (CollectionUtils.isNotEmpty(amenities)) {
            if (amenities.size() > 4) {
                for (int x = 0; x < 4; x++) {
                    listCards.add(amenities.get(x));
                }
                showVisibility(R.id.more_room_emenities);
                setText("+" + (amenities.size() - 4), R.id.tv_more_room_emenities);
            } else {
                listCards.addAll(amenities);
            }

            for (Amenity nameImageGeneralInfo : listCards) {
                ActionTag tag = new ActionTag();
                tag.setTitle(nameImageGeneralInfo.getName());
                tag.setImageUrl(nameImageGeneralInfo.getIconUrl());
                list.add(tag);
            }

        } else {
            hideVisibility(R.id.tv_amenities);
        }
        return list;
    }

    private void setMapview() {

    }

    private void setData() {
        if (roomsCategoryData != null) {
            final String imageUrl = roomsCategoryData.getImageUrl();
            if (!TextUtils.isEmpty(imageUrl)) {
                Picasso.with(this).load(imageUrl).placeholder(R.drawable.progress_animation).into(ivHotel, new Callback() {
                    @Override
                    public void onSuccess() {
                        Picasso.with(RoomNameActivity.this).load(imageUrl).placeholder(R.drawable.progress_animation).into(ivHotel);
                    }

                    @Override
                    public void onError() {
                        Picasso.with(RoomNameActivity.this).load(R.mipmap.room).placeholder(R.drawable.progress_animation).into(ivHotel);
                    }
                });
            } else {
                Picasso.with(RoomNameActivity.this).load(R.mipmap.room).placeholder(R.drawable.progress_animation).into(ivHotel);
            }
            String roomType = roomsCategoryData.getName();
            Integer availableRooms = roomsCategoryData.getAdultCapacity();
            tvRoomTypeAndno.setText(roomType + " | " + availableRooms + " Rooms");
            tvName.setText(roomsCategoryData.getName());
            tvDescription.setText(roomsCategoryData.getDescription());
            tvRoomSize.setText(roomsCategoryData.getSize());
            ivAdult.setVisibility(View.VISIBLE);
            ivChild.setVisibility(View.VISIBLE);
            tvChildCount.setVisibility(View.VISIBLE);
            ivAdult.setColorFilter(R.color.text_gray);
            ivChild.setColorFilter(R.color.text_gray);
            tvMaxOccupancy.setText("" + roomsCategoryData.getSize() + " ");
            tvChildCount.setText("" + roomsCategoryData.getChildCapacity() + " ");
        }

    }

    private void findViews() {
        ivHotel = (ImageView) findViewById(R.id.iv_hotel_room);
        ivAdult = (ImageView) findViewById(R.id.iv_adult);
        ivChild = (ImageView) findViewById(R.id.iv_child);
        tvChildCount = (TextView) findViewById(R.id.tv_child_count);
        tvName = (TextView) findViewById(R.id.tv_room_title);
        tvRoomTypeAndno = (TextView) findViewById(R.id.tv_type_no);
        tvDescription = (TextView) findViewById(R.id.tv_hotel_description);
        tvRoomSize = (TextView) findViewById(R.id.tv_hotel_room_size);
        tvMaxOccupancy = (TextView) findViewById(R.id.tv_max_occupancy);
    }
}
