package com.aboutstays.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.ServiceModel;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;

public class SelectServiceActivity extends AppBaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    private ListView lvService;
    private CustomListAdapter listAdapter;
    private List<ServiceModel> serviceModelList = new ArrayList<>();
    private String[] name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_service);
        initToolBar(getString(R.string.select_service));

        name = new String[]{
                "Massages",
                "Facial",
                "Body Wraps",
                "Body Scrubs",
                "Manicure",
                "Pedicure"
        };

        lvService = (ListView) findViewById(R.id.lv_service);
        listAdapter = new CustomListAdapter(this, R.layout.row_edit_profile, serviceModelList, this);
        lvService.setAdapter(listAdapter);
        loadData();
        lvService.setOnItemClickListener(this);
    }

    private void loadData() {
        for (int i=0; i<5; i++){
            ServiceModel serviceModel = new ServiceModel();
            serviceModel.setTvTitle(name[i]);
            serviceModelList.add(serviceModel);
            listAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_edit_profile, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        ServiceModel serviceModel = serviceModelList.get(position);
        holder.tvTitle.setText(serviceModel.getTvTitle());

        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 0:
                startNextActivity(MassagesActivity.class);
                break;
        }
    }

    class Holder{
        TextView tvTitle;

        public Holder(View view){
            tvTitle = (TextView) view.findViewById(R.id.tv_row_edit_profile);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_option_spa, menu);
        return true;
    }
}
