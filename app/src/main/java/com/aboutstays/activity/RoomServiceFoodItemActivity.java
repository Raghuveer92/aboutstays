package com.aboutstays.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.enums.FoodLabels;
import com.aboutstays.enums.SpicyType;
import com.aboutstays.fragment.SearchFoodItemsFragment;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.cart.GetCartData;
import com.aboutstays.model.cart.RoomServiceCartResponse;
import com.aboutstays.model.cart.RoomServiceCartData;
import com.aboutstays.model.cart.FoodItemOrderDetailList;
import com.aboutstays.model.cart.FoodItemOrderDetails;
import com.aboutstays.model.cart.RoomServiceCart;
import com.aboutstays.model.food.FoodDataByType;
import com.aboutstays.model.food.GetFoodItemsbyRoomServiceApi;
import com.aboutstays.model.roomService.RoomServiceMenuItem;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import simplifii.framework.ListAdapters.CustomExpandableListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

public class RoomServiceFoodItemActivity extends AppBaseActivity implements SearchView.OnQueryTextListener, CustomExpandableListAdapter.ExpandableListener {
    private ArrayList<FoodDataByType> searchServiceList = new ArrayList<>();
    private ExpandableListView expandableListView;
    private String title;
    private String roomServiceId;
    private TextView tvEmptyView;
    private Integer menuItemType;
    private MenuItem searchMenuItem;
    private SearchView searchView;
    SearchFoodItemsFragment fragment = new SearchFoodItemsFragment();
    private String staysId,userId,optionMenuIcon;
    private RoomServiceCart roomServiceCart;
    private List<FoodItemOrderDetailList> dataList = new ArrayList<>();
    private GeneralServiceModel generalServiceModel;

    private CustomExpandableListAdapter customExpandableListAdapter;
    private LinkedHashMap<String, List<FoodDataByType>> mapExpandable = new LinkedHashMap<>();
    private LinkedHashMap<String, List<FoodDataByType>> mapExpandableAll = new LinkedHashMap<>();
    private List<String> listHeader = new ArrayList<>();
    private TextView tv;
    private String gsId;
    private RoomServiceMenuItem menuItem;
    private String hotelId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_services_food_item);
        registerCartReceiver();

        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            userId = sessionInstance.getUserId();
        }

        Bundle b = getIntent().getExtras();
        if (b != null) {
            generalServiceModel = (GeneralServiceModel) b.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            roomServiceId = b.getString(AppConstants.BUNDLE_KEYS.ROOM_SERVICE_ID);
            staysId = b.getString(AppConstants.BUNDLE_KEYS.STAYS_ID);
            gsId = b.getString(AppConstants.BUNDLE_KEYS.GS_ID);
            hotelId = b.getString(AppConstants.BUNDLE_KEYS.HOTEL_ID);

            menuItem = (RoomServiceMenuItem) b.getSerializable(AppConstants.BUNDLE_KEYS.ROOM_SERVICE_TITLE);
            if (menuItem != null) {
                menuItemType = menuItem.getType();
                title = menuItem.getTypeName();
                optionMenuIcon = menuItem.getImageUrl();

            }
        }

        if (!TextUtils.isEmpty(title)) {
            initToolBar(title);
        }

        tvEmptyView = (TextView) findViewById(R.id.tv_empty_view);

        expandableListView = (ExpandableListView) findViewById(R.id.list_expandable_room_service);
        customExpandableListAdapter = new CustomExpandableListAdapter(this, R.layout.row_header_expandable, R.layout.row_search_services, mapExpandable, this);
        expandableListView.setAdapter(customExpandableListAdapter);
        expandableListView.setEmptyView(tvEmptyView);

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                FoodDataByType foodDataByType = mapExpandable.get(listHeader.get(groupPosition)).get(childPosition);
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.FOOD_ITEM_DETAILS, foodDataByType);
                bundle.putString(AppConstants.BUNDLE_KEYS.STAYS_ID, staysId);
                bundle.putString(AppConstants.BUNDLE_KEYS.GS_ID, gsId);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.ROOM_SERVICE_TITLE, menuItem);
                Intent intent = new Intent(RoomServiceFoodItemActivity.this, FoodItemActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, AppConstants.REQUEST_CODE.SHOW_FOOD_DETAIL);
                return true;
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAllFoodItems();
    }


    private void getAllFoodItems() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getAllFoodItemmsbyRoomService(roomServiceId, menuItemType);
        executeTask(AppConstants.TASK_CODES.GET_FOOD_ITEMS, httpParamObject);
    }

    @Override
    public void onPreExecute(int taskCode) {
        if (taskCode == AppConstants.TASK_CODES.ADD_REMOVE_FOOD_TO_CART) {
            return;
        }
        if (taskCode == AppConstants.TASK_CODES.GET_CART_DETAILS) {
            return;
        }
        super.onPreExecute(taskCode);
    }

    public void addItemToCart() {
        customExpandableListAdapter.notifyDataSetChanged();
        List<RoomServiceCartData> roomServiceCartDataList = new ArrayList<>();
        for (FoodDataByType foodDataByType : searchServiceList) {
            int itemCount = foodDataByType.getItemCount();

            roomServiceCart = new RoomServiceCart();
            roomServiceCart.setUserId(userId);
            roomServiceCart.setStaysId(staysId);
            roomServiceCart.setHotelId(foodDataByType.getHotelId());

            RoomServiceCartData roomServiceCartData = new RoomServiceCartData();
            roomServiceCartData.setCartType(1);
            roomServiceCartData.setCartOperation(1);

            FoodItemOrderDetails data = new FoodItemOrderDetails();
            data.setQuantity(itemCount);
            data.setType(foodDataByType.getType());
            data.setFoodItemId(foodDataByType.getFooditemId());
            data.setPrice(foodDataByType.getPrice());

            roomServiceCartData.setFoodItemOrderDetails(data);
            roomServiceCartDataList.add(roomServiceCartData);
            roomServiceCart.setData(roomServiceCartDataList);
        }

        if (roomServiceCart != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.addOrRemoveFoodItem(roomServiceCart);
            executeTask(AppConstants.TASK_CODES.ADD_REMOVE_FOOD_TO_CART, httpParamObject);
        }
    }

    private void getAllCartItem() {
        if (generalServiceModel != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.getAllRoomServiceCartItem(userId, staysId, generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId(), "1");
            executeTask(AppConstants.TASK_CODES.GET_CART_DETAILS, httpParamObject);
        }
    }

//    @Override
//    public void onBackPressed() {
//        StayCartUpdateReceiver.callOnClear(this);
//        setResult(RESULT_OK);
//        finish();
//    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_FOOD_ITEMS:
                GetFoodItemsbyRoomServiceApi getFoodItemsApi = (GetFoodItemsbyRoomServiceApi) response;
                if (getFoodItemsApi != null && getFoodItemsApi.getError() == false) {
                    List<FoodDataByType> foodItems = getFoodItemsApi.getData();
                    if (foodItems != null && !foodItems.isEmpty()) {
                        ckecIfAlreadyAddedInCart(foodItems);
                        searchServiceList.clear();
                        for (FoodDataByType itemData : foodItems){
                            if (itemData.getShowOnApp() == true){
                                searchServiceList.add(itemData);
                            }
                        }
//                        searchServiceList.addAll(foodItems);
                    }
                    filterList();
                    String searchText = searchView.getQuery().toString().trim();
                    if (searchText.length() > 2) {
                        filterListOnSearch(searchText);
                    }
                }
                break;
            case AppConstants.TASK_CODES.ADD_REMOVE_FOOD_TO_CART:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false) {
//                    showToast(getString(R.string.cart_added));
                    setResult(AppConstants.RESULT_CODE.RESULT_DONE);
                }
                break;

        }
    }

    private void refreshCartData() {
        GetCartData cartData = RoomServiceCartResponse.getInstance();
        List<FoodItemOrderDetailList> foodItemOrderDetailList = cartData.getFoodItemOrderDetailList();
        if (foodItemOrderDetailList.size() > 0) {
            tv.setVisibility(View.VISIBLE);
            tv.setText(foodItemOrderDetailList.size() + "");
        } else {
            tv.setVisibility(View.GONE);
        }
        dataList.clear();
        dataList.addAll(foodItemOrderDetailList);
        customExpandableListAdapter.notifyDataSetChanged();
    }

    private void filterListOnSearch(String newText) {
        mapExpandable.clear();
        for (Map.Entry<String, List<FoodDataByType>> entry : mapExpandableAll.entrySet()) {
            List<FoodDataByType> packLists = entry.getValue();
            List<FoodDataByType> packListsSelected = new ArrayList<>();
            for (FoodDataByType internetPackList : packLists) {
                if (internetPackList.getName().toLowerCase().contains(newText.toLowerCase())) {
                    packListsSelected.add(internetPackList);
                }
            }
            if (packListsSelected.size() > 0) {
                mapExpandable.put(entry.getKey(), packListsSelected);
            }
        }
        refreshHeader(mapExpandable);
    }

    private void refreshHeader(LinkedHashMap<String, List<FoodDataByType>> mapExpandable) {
        listHeader.clear();
        Iterator<String> iterator = mapExpandable.keySet().iterator();
        while (iterator.hasNext()) {
            listHeader.add(iterator.next());
        }
        customExpandableListAdapter = new CustomExpandableListAdapter(this, R.layout.row_header_expandable, R.layout.row_search_services, this.mapExpandable, this);
        expandableListView.setAdapter(customExpandableListAdapter);

        for (int x = 0; x < listHeader.size(); x++) {
            expandableListView.expandGroup(x);
        }
    }

    private void filterList() {
        listHeader.clear();
        mapExpandableAll.clear();
        mapExpandable.clear();

        for (FoodDataByType internetPackList : searchServiceList) {
            String category = internetPackList.getCategory();
            if (!TextUtils.isEmpty(category)) {
                if (mapExpandable.containsKey(category)) {
                    List<FoodDataByType> internetPackLists = mapExpandable.get(category);
                    internetPackLists.add(internetPackList);
                } else {
                    List<FoodDataByType> laundryItemDatas = new ArrayList<>();
                    laundryItemDatas.add(internetPackList);
                    mapExpandable.put(category, laundryItemDatas);
                }
            }
        }
        refreshHeader(mapExpandable);
        mapExpandableAll.putAll(mapExpandable);
    }

    private void ckecIfAlreadyAddedInCart(List<FoodDataByType> foodItems) {
        if (!CollectionUtils.isEmpty(dataList)) {
            for (FoodDataByType dataByType : foodItems) {
                for (FoodItemOrderDetailList detailList : dataList) {
                    if (dataByType.getFooditemId().equals(detailList.getFoodItemId())) {
                        dataByType.setItemCount(detailList.getQuantity());
                    }
                }
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_service, menu);

        MenuItem item = menu.findItem(R.id.action_room_service);
        MenuItemCompat.setActionView(item, R.layout.action_menu_count_layout);
        RelativeLayout notifCount = (RelativeLayout) MenuItemCompat.getActionView(item);

        ImageView iv = (ImageView) notifCount.findViewById(R.id.iv_roomservice);
        Picasso.with(this).load(optionMenuIcon).into(iv);
        iv.setColorFilter(Color.WHITE);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderDetailActivity.startActivity(RoomServiceFoodItemActivity.this, gsId, staysId, hotelId, AppConstants.STAYS_TYPE.ROOM_SERVICE, AppConstants.FROM_ORDERING.ROOM_SERVICE);
            }
        });
        tv = (TextView) notifCount.findViewById(R.id.actionbar_notifcation_textview);
        tv.setVisibility(View.GONE);
        tv.setText("0");
        refreshCartData();

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);

        return true;
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
//        filterListOnSearch(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.length() > 2) {
            filterListOnSearch(newText);
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppConstants.REQUEST_CODE.ORDER_PLACED:
                    setResult(RESULT_OK);
                    finish();
                    break;
                case AppConstants.REQUEST_CODE.SHOW_FOOD_DETAIL:
                    if (data != null) {
                        Bundle extras = data.getExtras();
                        if (extras != null) {
                            FoodDataByType foodDataByType = (FoodDataByType) extras.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
                            for (FoodDataByType foodList : searchServiceList) {
                                if (foodList.getFooditemId().equalsIgnoreCase(foodDataByType.getFooditemId())) {
                                    foodList.setItemCount(foodDataByType.getItemCount());
                                    break;
                                }
                            }
                            customExpandableListAdapter.notifyDataSetChanged();
                        }
                    }
                    setResult(RESULT_OK);
                    finish();
                    break;
            }
        }
        refreshCartData();
        hideProgressBar();
    }

    @Override
    public int getChildSize(int parentPosition) {
        return mapExpandable.get(listHeader.get(parentPosition)).size();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        HolderParent holderParent;
        if (convertView == null) {
            convertView = inflater.inflate(resourceId, null);
            holderParent = new HolderParent(convertView);
            convertView.setTag(holderParent);
        } else {
            holderParent = (HolderParent) convertView.getTag();
        }
        String title = listHeader.get(groupPosition);
        holderParent.bindData(title, isExpanded);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_search_services, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        View viewDescription = (View) convertView.findViewById(R.id.view_line_description);
        if (isLastChild) {
            viewDescription.setVisibility(View.INVISIBLE);
        } else {
            viewDescription.setVisibility(View.VISIBLE);
        }
        FoodDataByType foodDataByType = mapExpandable.get(listHeader.get(groupPosition)).get(childPosition);
        holder.onBindData(foodDataByType);
        return convertView;

    }

    class HolderParent {
        TextView textView;
        ImageView ivIndicator;

        public HolderParent(View view) {
            textView = (TextView) view.findViewById(R.id.tv_men_women);
            ivIndicator = (ImageView) view.findViewById(R.id.iv_indicator);
        }

        public void bindData(String title, boolean isExpand) {
            textView.setText(title);
            if (isExpand) {
                ivIndicator.setImageResource(R.mipmap.up_arrow_white);
            } else {
                ivIndicator.setImageResource(R.mipmap.arrow_down_white);
            }
        }
    }

    class ViewHolder {
        TextView serviceTitle, serviceCost, serviceDescription, tvAdd;
        TextView tvMinus, tvCount, tvPlus;
        ImageView imgVegNonveg, imgChilli, imgLike;
        RelativeLayout rlCount;

        public ViewHolder(View view) {
            serviceTitle = (TextView) view.findViewById(R.id.tv_service_title);
            serviceCost = (TextView) view.findViewById(R.id.tv_service_cost);
            serviceDescription = (TextView) view.findViewById(R.id.tv_service_description);
            imgVegNonveg = (ImageView) view.findViewById(R.id.iv_veg_nonveg);
            imgChilli = (ImageView) view.findViewById(R.id.iv_chilli);
            imgLike = (ImageView) view.findViewById(R.id.iv_like);
            tvAdd = (TextView) view.findViewById(R.id.tv_add);
            tvMinus = (TextView) view.findViewById(R.id.tv_minus);
            tvCount = (TextView) view.findViewById(R.id.tv_count);
            tvPlus = (TextView) view.findViewById(R.id.tv_plus);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_add_count);
        }

        public void onBindData(final FoodDataByType foodDataByType) {
            Boolean recommended = foodDataByType.getRecommended();
            tvCount.setText(foodDataByType.getItemCount() + "");
            serviceTitle.setText(foodDataByType.getName());
            serviceDescription.setLineSpacing(16, 1);
            serviceDescription.setText(foodDataByType.getDescription());
            Double price = foodDataByType.getPrice();
            if (price != null) {
                serviceCost.setText("\u20B9 " + price);
            }

            Integer foodLabel = foodDataByType.getFoodLabel();
            if (foodLabel != 0 && foodLabel < 9) {
                FoodLabels statusByCode = FoodLabels.findStatusByCode(foodLabel);
                if (statusByCode != null) {
                    int vegUrl = statusByCode.getImageUrl();
                    imgVegNonveg.setImageResource(vegUrl);
                    imgVegNonveg.setVisibility(View.VISIBLE);
                }
            } else {
                imgVegNonveg.setVisibility(View.GONE);
            }

            tvPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int itemCount = foodDataByType.getItemCount();
                    foodDataByType.setItemCount(itemCount + 1);
                    GetCartData.refreshItem(foodDataByType, RoomServiceFoodItemActivity.this);
                    refreshCartData();
                    addItemToCart();
                }
            });

            tvMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int itemCount = foodDataByType.getItemCount();
                    if (itemCount >= 1) {
                        foodDataByType.setItemCount(itemCount - 1);
                    }
                    GetCartData.refreshItem(foodDataByType, RoomServiceFoodItemActivity.this);
                    refreshCartData();
                    addItemToCart();
                }
            });

            Integer spicyType = foodDataByType.getSpicyType();
            if (spicyType != 0 && spicyType < 9) {
                SpicyType statusByCode1 = SpicyType.findStatusByCode(spicyType);
                if (statusByCode1 != null) {
                    int spicyUrl = statusByCode1.getImageUrl();
                    imgChilli.setImageResource(spicyUrl);
                    imgChilli.setVisibility(View.VISIBLE);
                }
            } else {
                imgChilli.setVisibility(View.GONE);
            }

            if (recommended == true) {
                imgLike.setVisibility(View.VISIBLE);
            } else {
                imgLike.setVisibility(View.GONE);
            }

            tvAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    foodDataByType.setItemCount(1);
                    addItemToCart();
                    refreshCartData();
                    GetCartData.refreshItem(foodDataByType, RoomServiceFoodItemActivity.this);
                }
            });

            if (foodDataByType.getItemCount() > 0) {
                tvAdd.setVisibility(View.GONE);
                rlCount.setVisibility(View.VISIBLE);
            } else {
                tvAdd.setVisibility(View.VISIBLE);
                rlCount.setVisibility(View.GONE);
            }
        }

    }


    @Override
    public void onCartUpdate() {
        refreshCartData();
//        getAllFoodItems();
    }

    @Override
    public void onCartClear() {
        getAllFoodItems();
        refreshCartData();
    }
}
