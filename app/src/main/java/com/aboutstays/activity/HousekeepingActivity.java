package com.aboutstays.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.MenuItemCompat;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.enums.HouseKeepingItemType;
import com.aboutstays.fragment.SearchHousekeepingItemFragment;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.cart.HouseKeepingCartResponse;
import com.aboutstays.model.cart.HkItemOrderDetailsList;
import com.aboutstays.model.cart.HouseKeepingCartResponseData;
import com.aboutstays.model.housekeeping.HouseKeepingData;
import com.aboutstays.model.housekeeping.HouseKeepingItemList;
import com.aboutstays.model.housekeeping.HouseKeepingServiceItem;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.cunoraz.gifview.library.GifView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

import static android.view.View.GONE;

public class HousekeepingActivity extends AppBaseActivity implements CustomListAdapterInterface, SearchView.OnQueryTextListener, SearchHousekeepingItemFragment.AddToCartListener, AdapterView.OnItemClickListener {

    private CustomListAdapter listAdapter;
    private List<HouseKeepingItemList> housekeepingList = new ArrayList<>();
    private ListView housekeepingListView;
    private String userId;
    private GeneralServiceModel generalServiceModel;
    private String serviceId;
    private String staysId;
    private FrameLayout searchFrag;
    private MenuItem searchMenuItem;
    private SearchView searchView;
    private TextView tv, tvEmptyView;
    private SearchHousekeepingItemFragment housekeepingItemFragment;
    private HouseKeepingCartResponseData cartData = new HouseKeepingCartResponseData();
    private String gsId;
    private String hotelId;
    private FrameLayout frameLayout;
    private GifView gifView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_housekeeping);
        registerCartReceiver();
        initToolBar("Housekeeping");

        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            userId = sessionInstance.getUserId();
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null) {
                serviceId = generalServiceModel.getGeneralServiceData().getHsId();
                staysId = generalServiceModel.getHotelStaysList().getStaysPojo().getStaysId();
                gsId = generalServiceModel.getGeneralServiceData().getId();
                hotelId = generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId();
            }
        }

        frameLayout = (FrameLayout) findViewById(R.id.lay_frame_loader);
        gifView = (GifView) findViewById(R.id.gif);

        searchFrag = (FrameLayout) findViewById(R.id.frame_search_fragment);
        tvEmptyView = (TextView) findViewById(R.id.tv_empty_view);

        housekeepingItemFragment = SearchHousekeepingItemFragment.getInstance(this);
        getSupportFragmentManager().beginTransaction().add(R.id.frame_search_fragment, housekeepingItemFragment).commit();

        housekeepingListView = (ListView) findViewById(R.id.lv_housekeeping);
        listAdapter = new CustomListAdapter(this, R.layout.row_room_service, housekeepingList, this);
        housekeepingListView.setAdapter(listAdapter);
        housekeepingListView.setEmptyView(tvEmptyView);
        housekeepingListView.setOnItemClickListener(this);
        listAdapter.notifyDataSetChanged();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAllCartData();
    }

    @Override
    public void showProgressDialog() {
        super.showProgressDialog();
        frameLayout.setVisibility(View.VISIBLE);
        gifView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        super.hideProgressBar();
        frameLayout.setVisibility(View.GONE);
        gifView.setVisibility(View.GONE);
    }

    private void getAllCartData() {
        if (generalServiceModel != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.getAllHouseKeepingCartItem(userId, staysId, hotelId, "2");
            executeTask(AppConstants.TASK_CODES.GET_HOUSEKEEPING_CART_DETAILS, httpParamObject);
        }
    }

    private void getHouseKeepingData() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getHouseKeepingList(serviceId);
        executeTask(AppConstants.TASK_CODES.GET_HOUSEKEEPING_SERVICE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_HOUSEKEEPING_SERVICE:
                HouseKeepingServiceItem houseKeepingServiceItem = (HouseKeepingServiceItem) response;
                if (houseKeepingServiceItem != null) {
                    HouseKeepingData houseKeepingData = houseKeepingServiceItem.getData();
                    if (houseKeepingData != null) {
                        List<HouseKeepingItemList> listHKItems = houseKeepingData.getListHKItems();
                        housekeepingList.clear();
                        if (!CollectionUtils.isEmpty(listHKItems)) {
                            housekeepingList.addAll(listHKItems);
                        }
                        listAdapter.notifyDataSetChanged();
                    }
                }
                break;
            case AppConstants.TASK_CODES.GET_HOUSEKEEPING_CART_DETAILS:
                refreshCartData();
                getHouseKeepingData();
                hideProgressBar();
                break;
        }
    }

    private void refreshCartData() {
        cartData = HouseKeepingCartResponse.getInstance();
        if (cartData.getHkItemOrderDetailsList().size() > 0) {
            tv.setText(cartData.getHkItemOrderDetailsList().size() + "");
            tv.setVisibility(View.VISIBLE);
        } else {
            tv.setText("0");
            tv.setVisibility(GONE);
        }
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_room_service, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        HouseKeepingItemList ob = housekeepingList.get(position);
        ob.setItemCount(0);
        if (cartData != null) {
            List<HkItemOrderDetailsList> dataList = cartData.getHkItemOrderDetailsList();
            if (!CollectionUtils.isEmpty(dataList)) {
                for (HkItemOrderDetailsList orderDetailList : dataList) {
                    if (ob.getType() == orderDetailList.getType()) {
                        ob.setItemCount(ob.getItemCount() + 1);
                    }
                }
            }
        }

        if (!TextUtils.isEmpty(ob.getImageUrl())) {
            Picasso.with(this).load(ob.getImageUrl()).placeholder(R.drawable.progress_animation).into(holder.titleImage);
        } else {
            Integer type = ob.getType();
            HouseKeepingItemType byCode = HouseKeepingItemType.findByCode(type);
            if (byCode != null) {
                int imageUrl = byCode.getImageUrl();
                holder.titleImage.setImageResource(imageUrl);
            }
        }

        if (ob.getItemCount() >= 1) {
            holder.rlCount.setVisibility(View.VISIBLE);
            holder.tvItemCount.setText(ob.getItemCount() + "");
        } else {
            holder.rlCount.setVisibility(GONE);
        }

        holder.housekeepingTitle.setText(ob.getTypeName());
        holder.tvTime.setVisibility(GONE);

        return convertView;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_service, menu);

        MenuItem item = menu.findItem(R.id.action_room_service);
        MenuItemCompat.setActionView(item, R.layout.action_menu_count_layout);
        RelativeLayout relativeLayout = (RelativeLayout) MenuItemCompat.getActionView(item);

        ImageView iv = (ImageView) relativeLayout.findViewById(R.id.iv_roomservice);
        iv.setImageResource(R.mipmap.housekeeping);
        iv.setColorFilter(Color.WHITE);
        iv.setOnClickListener(this);

        tv = (TextView) relativeLayout.findViewById(R.id.actionbar_notifcation_textview);

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_roomservice:
                OrderDetailActivity.startActivity(HousekeepingActivity.this, gsId, staysId, hotelId, AppConstants.STAYS_TYPE.HOUSE_KEEPING, AppConstants.FROM_ORDERING.HOUSEKEEPING);
                break;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.length() >= 3) {
            searchFrag.setVisibility(View.VISIBLE);
            housekeepingItemFragment.textUpdate(newText, serviceId, staysId, cartData);
        } else {
            searchFrag.setVisibility(GONE);
//            getAllCartData();
        }
        if (TextUtils.isEmpty(newText)) {
            housekeepingItemFragment.callBack();
        }
        return true;
    }

    @Override
    public void searchCallback() {
        getAllCartData();
        searchFrag.setVisibility(GONE);
    }

    @Override
    public void updateCart() {
        refreshCartData();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        HouseKeepingItemList ob = housekeepingList.get(position);

        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.GS_ID, gsId);
        bundle.putString(AppConstants.BUNDLE_KEYS.STAYS_ID, staysId);
        bundle.putString(AppConstants.BUNDLE_KEYS.HOTEL_ID, hotelId);
        bundle.putString(AppConstants.BUNDLE_KEYS.HOTEL_SERVICE_ID, serviceId);
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.HOUSEKEEPING_SERVICE_TITLE, ob);
        Intent intent = new Intent(HousekeepingActivity.this, HouseKeepingItemActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, AppConstants.REQUEST_CODE.ORDER_PLACED);
    }


    class Holder {
        TextView housekeepingTitle, tvTime, tvItemCount;
        ImageView titleImage, arrowImage;
        RelativeLayout rlCount;

        public Holder(View view) {
            housekeepingTitle = (TextView) view.findViewById(R.id.tv_title);
            titleImage = (ImageView) view.findViewById(R.id.iv_title_image1);
            arrowImage = (ImageView) view.findViewById(R.id.iv_right_angle);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_count);
            tvItemCount = (TextView) view.findViewById(R.id.tv_item_count);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        } else if (resultCode == AppConstants.RESULT_CODE.RESULT_NULL){
            getAllCartData();
            hideProgressBar();
        }
    }

    @Override
    public void onCartUpdate() {
        refreshCartData();
    }

    @Override
    public void onCartClear() {
        refreshCartData();
        listAdapter.notifyDataSetChanged();
        if(searchView!=null){
            housekeepingItemFragment.textUpdate(searchView.getQuery().toString(), serviceId, staysId, cartData);
        }
    }
}
