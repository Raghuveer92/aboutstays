package com.aboutstays.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.hotels.GeneralInformation;
import com.aboutstays.model.hotels.HotelAddress;
import com.aboutstays.model.hotels.Hotel;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

/**
 * Created by ajay on 26/12/16.
 */

public class LocationAndContactActivity extends AppBaseActivity {

    private GoogleMap googleMap;
    private TextView tvPhone1, tvPhone2, tvEmil, tvFax, tvAddress;
    private MapView mapView;
    private ImageView ivMap;
    private TextView getDirection;
//    private ListHotel listHotel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_contact);
        initToolBar(getString(R.string.location_contact));

        findViews();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Hotel listHotel = (Hotel) extras.getSerializable(AppConstants.BUNDLE_KEYS.GENERAL_INFO);
            setData(listHotel);
        }
    }


    private void findViews() {

        tvPhone1 = (TextView) findViewById(R.id.tv_phone_1);
        tvPhone2 = (TextView) findViewById(R.id.tv_phone_2);
        tvEmil = (TextView) findViewById(R.id.tv_email);
        tvFax = (TextView) findViewById(R.id.tv_fax);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        ivMap = (ImageView) findViewById(R.id.map_view);
        getDirection = (TextView) findViewById(R.id.btn_get_directions);
    }

    private void setData(Hotel listHotel) {
        if (listHotel != null) {
            GeneralInformation generalInformation = listHotel.getGeneralInformation();
            if (generalInformation != null) {
                HotelAddress address = generalInformation.getAddress();
                tvPhone1.setText(generalInformation.getPhone1());
                tvPhone2.setText(generalInformation.getPhone2());
                tvEmil.setText(generalInformation.getEmail());
                tvFax.setText(generalInformation.getFax());

                if (address != null) {
                    tvAddress.setText(address.getCompleteAddress());
                    final String latitude = address.getLatitude();
                    final String longitude = address.getLongitude();
                    getMapImage(latitude, longitude);
                    getDirection.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String strUri = "http://maps.google.com/maps?q=loc:" + latitude + "," + longitude;
                            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));
                            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                            startActivity(intent);
                        }
                    });
                }
            }
        }
    }

    private void getMapImage(String latitude, String longitude) {
        final String path = "http://maps.google.com/maps/api/staticmap?zoom=10&markers=" + latitude + "," + longitude + "&size=600x400";
        Picasso.with(this).load(path).placeholder(R.drawable.progress_animation).into(ivMap);
    }
}



