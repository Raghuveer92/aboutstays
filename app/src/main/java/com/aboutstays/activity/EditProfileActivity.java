package com.aboutstays.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.services.UserServices;
import com.aboutstays.utils.FileUtil;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.FileParamObject;
import simplifii.framework.fragments.MediaFragment;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

import static com.aboutstays.fragment.ProfileFragment.MEMBER_SINCE_DATE_FORMAT;

public class EditProfileActivity extends AppBaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    private List<String> list = new ArrayList<>();
    private ListView listView;
    private CustomListAdapter listAdapter;
    private MediaFragment imagePicker;
    private ImageView propic, ivInfo;
    private File file;
    private File bitmapFile;
    private String path;
    private UserSession userSession;
    private ViewGroup header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initToolBar(getString(R.string.edit_profile));

        UserServices.startUpdateUser(this);
        userSession = UserSession.getSessionInstance();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            boolean fromProfile = bundle.getBoolean(AppConstants.BUNDLE_KEYS.FROM_PROFILE);
            if (fromProfile) {
                askPermissions();
            }
        }

        listView = (ListView) findViewById(R.id.listview_edit_profile);
        listAdapter = new CustomListAdapter(this, R.layout.row_edit_profile, list, this);
        listView.setAdapter(listAdapter);
        addDataIntoList();
        listAdapter.notifyDataSetChanged();
        listView.setOnItemClickListener(this);

        LayoutInflater inflater = getLayoutInflater();
        header = (ViewGroup) inflater.inflate(R.layout.view_header_profile, listView, false);

        setData(userSession);

        listView.addHeaderView(header, null, false);

        imagePicker = new MediaFragment();
        getSupportFragmentManager().beginTransaction().add(imagePicker, "Profile image").commit();
    }

    private void setData(UserSession userSession) {

        propic = (ImageView) header.findViewById(R.id.iv_propic);
        ivInfo = (ImageView) header.findViewById(R.id.iv_info);
        ivInfo.setImageResource(R.mipmap.edit_icon);
        ivInfo.setPadding(8, 8, 8, 8);
        RelativeLayout rlEdit = (RelativeLayout) header.findViewById(R.id.rl_edit);
        rlEdit.setVisibility(View.VISIBLE);
        TextView tvMember = (TextView) header.findViewById(R.id.tv_member_since);
        String signedUpTime = Util.getDate(this.userSession.getCreated(), MEMBER_SINCE_DATE_FORMAT);
        TextView tvName = (TextView) header.findViewById(R.id.tv_profile_title);
        tvName.setText(userSession.getFirstName() + " " + userSession.getLastName());
        if (userSession.getImageUrl() != null) {
            Picasso.with(this).load(this.userSession.getImageUrl()).placeholder(R.drawable.progress_animation).into(propic);
        }
        propic.setOnClickListener(this);
        tvMember.setText("Member Since " + signedUpTime);
        rlEdit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iv_propic:
//                askPermissions();
                break;
            case R.id.rl_edit:
                askPermissions();
                break;
        }
    }

    private void askPermissions() {
        new TedPermission(this)
                .setPermissions(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        selectImageViaPicker();
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                    }
                }).check();
    }

    private void selectImageViaPicker() {
        imagePicker.getPicture(new MediaFragment.MediaListener() {
            @Override
            public void setUri(Uri uri, String MediaType) {
                if (uri != null) {
                    propic.setImageURI(uri);
                    String imagePathFromURI = getImagePathFromURI(uri);
                    getFile(imagePathFromURI);
                }
            }

            @Override
            public void setBitmap(Bitmap bitmap) {
                if (bitmap != null) {
//                    iv.setImageBitmap(bitmap);
                    propic.setImageBitmap(bitmap);
                    bitmap = Util.getResizeBitmap(bitmap, 1024);
                    try {
                        bitmapFile = Util.getFile(bitmap, "AboutStays");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    uploadPic();
                }
            }

            @Override
            public void onGetPdfUri(Uri uri, int MediaType, String path) {

            }
        });

    }

    private void uploadPic() {
        if (bitmapFile != null) {
            this.file = bitmapFile;
        } else if (!TextUtils.isEmpty(path)) {
            file = new File(path);
        } else {
            showToast(getString(R.string.select_file_first));
        }

        if (file != null) {
            if (!file.exists()) {
                showToast(getString(R.string.select_file_first));
            }
            if (file.exists()) {
                FileParamObject fileParamObject = new FileParamObject(file, file.getName(), "displayPicture");
                fileParamObject.setUrl(AppConstants.PAGE_URL.UPDATE_PROFILE_PIC);
                fileParamObject.setPostMethod();
                fileParamObject.addParameter("userId", userSession.getUserId());
                fileParamObject.setContentType("");
                fileParamObject.setClassType(BaseApiResponse.class);
                executeTask(AppConstants.TASK_CODES.UPLOAD_FILE, fileParamObject);
            }
        }

    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        BaseApiResponse baseApiResponse = (BaseApiResponse) response;
        if (baseApiResponse != null && baseApiResponse.getError() == false) {
//            showToast(baseApiResponse.getMessage());
            UserServices.startUpdateUser(this);
            userSession = UserSession.getSessionInstance();
            setData(userSession);
        } else {
//            showToast(baseApiResponse.getMessage());
        }
    }

    private void getFile(String path) {
        this.path = path;
        uploadPic();
    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.left_arrow;
    }

    private void addDataIntoList() {

        list.add(getString(R.string.personal_details));
        list.add(getString(R.string.contact_details));
        list.add(getString(R.string.stay_preferences));
        list.add(getString(R.string.identity_docs));
//        list.add(getString(R.string.loyalty_programs));
//        list.add(getString(R.string.work));
//        list.add(getString(R.string.payment_method));

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {

        Holder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_edit_profile, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.textView.setText(list.get(position));
        holder.textView.setTextSize(18);
        holder.textView.setTextColor(getResourceColor(R.color.button_login_color));
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        switch (position) {
            case 1:
                Intent intent = new Intent(this, PersonalDetailsActivity.class);
                startActivityForResult(intent, AppConstants.REQUEST_CODE.FROM_EDIT_PROFILE);
//                startNextActivity(PersonalDetailsActivity.class);
                break;
            case 2:
                Intent contactIntent = new Intent(this, ContactDetailsActivity.class);
                startActivityForResult(contactIntent, AppConstants.REQUEST_CODE.FROM_EDIT_PROFILE);
                break;
            case 3:
                startNextActivity(StayPreferencesActivity.class);
                break;
            case 4:
                startNextActivity(IdentityDocuments.class);
                break;
//            case 5:
//                startNextActivity(LoyaltyProgramsActivity.class);
//                break;
//            case 6:
//                startNextActivity(WorkActivity.class);
//                break;
//            case 7:
//                startNextActivity(SavedCardsActivity.class);
//                break;
        }
    }


    class Holder {
        TextView textView;

        public Holder(View view) {
            textView = (TextView) view.findViewById(R.id.tv_row_edit_profile);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            UserServices.startUpdateUser(this);
            userSession = UserSession.getSessionInstance();
            setData(userSession);
        }
    }

    public String getImagePathFromURI(Uri contentUri) {
        if (true) {
            return FileUtil.getPath(this, contentUri);
        }
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = this.managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }
}
