package com.aboutstays.activity;

/**
 * Created by ajay on 13/12/16.
 */
public class SelectLoyaltyModel {
    String title;
    String subTitle;

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
