package com.aboutstays.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.AddCardModel;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

/**
 * Created by ajay on 14/12/16.
 */

public class SavedCardsActivity extends AppBaseActivity implements CustomListAdapterInterface {

    private List<AddCardModel> addCardModelList = new ArrayList<>();
    private CustomListAdapter listAdapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_cards);

        initToolBar("Saved Cards");
        listView = (ListView) findViewById(R.id.list_view_saved_Cards);
        listAdapter = new CustomListAdapter(this, R.layout.row_saved_cards_added, addCardModelList, this);
        listView.setAdapter(listAdapter);
        setData();
    }

    private void setData() {
        for (int i = 0; i < 1; i++) {
            AddCardModel ob = new AddCardModel();
                        /*ob.setBankName("hdfe");
            ob.setCardNumber("1234 4444 4444 3333");
            ob.setValidDate("12/11");*/
            // ob.setAddNewCard("add card");
            addCardModelList.add(ob);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {

        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_saved_cards_added, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        AddCardModel ob = addCardModelList.get(position);
        if (position == listAdapter.getCount() - 1) {
//            showToast("position = " + position + " counet=" + listAdapter.getCount());
            holder.rlViewAddNewCard.setVisibility(View.VISIBLE);
            holder.rlViewCardAdded.setVisibility(View.GONE);
            holder.rlViewAddNewCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SavedCardsActivity.this, AddCardActivity.class);
                    startActivityForResult(intent, 1);
                }
            });
        } else {
            showToast("position e = " + position);
            holder.rlViewAddNewCard.setVisibility(View.GONE);
            holder.rlViewCardAdded.setVisibility(View.VISIBLE);
            holder.tvBankName.setText(ob.getBankName());
            holder.tvAddedCardNo.setText(ob.getCardNumber());
            holder.tvAddedValidDate.setText(ob.getValidDate());
        }

        return convertView;
    }

    class Holder {
        TextView tvBankName, tvAddedCardNo, tvAddedValidDate, tvAddNewCard;
        ImageView ivBankLogo;
        RelativeLayout rlViewAddNewCard, rlViewCardAdded;

        public Holder(View view) {

            rlViewAddNewCard = (RelativeLayout) view.findViewById(R.id.lay_view_add_new_card);
            tvAddNewCard = (TextView) view.findViewById(R.id.tv_add_card);
            rlViewCardAdded = (RelativeLayout) view.findViewById(R.id.lay_view_added_card);
            tvBankName = (TextView) view.findViewById(R.id.tv_bank_name);
            tvAddedCardNo = (TextView) view.findViewById(R.id.tv_added_card_number);
            tvAddedValidDate = (TextView) view.findViewById(R.id.tv_added_valid_date);
            ivBankLogo = (ImageView) view.findViewById(R.id.iv_bank_logo);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case 1:
                AddCardModel addCardModel = (AddCardModel) data.getSerializableExtra(AppConstants.BUNDLE_KEYS.ADD_CARD_MODEL);
                addCardModelList.add(0, addCardModel);
                addCardModel.setShowAddNewCardLayout(false);
                listAdapter.notifyDataSetChanged();
        }
    }
}