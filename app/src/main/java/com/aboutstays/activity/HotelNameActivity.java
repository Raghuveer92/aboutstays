package com.aboutstays.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.HotelNameModel;
import com.aboutstays.model.hotels.GeneralInformation;
import com.aboutstays.model.hotels.GetHotelById;
import com.aboutstays.model.hotels.HotelAddress;
import com.aboutstays.model.hotels.Hotel;
import com.aboutstays.model.hotels.OtherServiceData;
import com.aboutstays.model.reservation.ReservationServicePojo;
import com.aboutstays.model.reservation.TypeInfoList;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.widgets.CustomRatingBar;

/**
 * Created by ajay on 9/12/16.
 */

public class HotelNameActivity extends AppBaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    List itemList = new ArrayList<>();
    private ListView listView;
    private Hotel hotel;
    private CustomListAdapter listAdapter;
    private String hotelId;
    private String hotelName;
    private List<TypeInfoList> typeInfoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_names);

        if (!TextUtils.isEmpty(hotelName)) {
            initToolBar(hotelName);
        } else {
            initToolBar(getString(R.string.hotel_name));
        }
        listView = (ListView) findViewById(R.id.listview_hotel_name);
        listAdapter = new CustomListAdapter(this, R.layout.row_hotel_names, itemList, this);
        listView.setAdapter(listAdapter);
        putDataIntoList();
        listAdapter.notifyDataSetChanged();
        setData(hotel);
        listView.setOnItemClickListener(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getHotelById();
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        hotel = (Hotel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.HOTEL_DATA);
        hotelId = bundle.getString(AppConstants.BUNDLE_KEYS.HOTEL_ID);
        if (hotel != null) {
            hotelName = hotel.getGeneralInformation().getName();
            hotelId = hotel.getHotelId();
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.HOTEL_BY_ID:
                GetHotelById getHotelById = (GetHotelById) response;
                if (getHotelById != null && getHotelById.getError() == false) {
                    hotel = getHotelById.getData();
                    setData(hotel);
                    if (hotel != null) {
                        ReservationServicePojo reservationServicePojo = hotel.getReservationServicePojo();
                        if (reservationServicePojo != null) {
                            typeInfoList = reservationServicePojo.getTypeInfoList();
                            if (CollectionUtils.isNotEmpty(typeInfoList)) {
                                itemList.addAll(typeInfoList);
                                listAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
                break;
        }
    }

    private void getHotelById() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getHotelsById(hotelId);
        executeTask(AppConstants.TASK_CODES.HOTEL_BY_ID, httpParamObject);
    }


    private void setData(Hotel hotel) {
        if (hotel != null) {
            GeneralInformation generalInformation = hotel.getGeneralInformation();
            if (generalInformation != null) {
                setText(generalInformation.getName(), R.id.tv_hotel_name);
                setImage(generalInformation.getLogoUrl(), R.id.iv_hotel_logo, R.drawable.progress_animation);
                HotelAddress address = generalInformation.getAddress();
                if (address != null) {
                    //setText(address.getCityCountry(), R.id.tv_hotel_address);
                    setText(address.getCity(), R.id.tv_hotel_address);
                }
            }
            CustomRatingBar customRatingBar = (CustomRatingBar) findViewById(R.id.custom_rating_bar);
            customRatingBar.setRating(hotel.getHotelType());
        }
    }

    Integer[] icon = {
            R.mipmap.information_icon,
            R.mipmap.location,
            R.mipmap.review_rate,
            R.mipmap.rooms,
            R.mipmap.essential,
            R.mipmap.restaurants,
            R.mipmap.health_beauty,
            R.mipmap.sports,
            R.mipmap.business,
            R.mipmap.shopes
    };

    private void putDataIntoList() {
        String[] temp = getResources().getStringArray(R.array.item_list);
        for (int i = 0; i < 5; i++) {
            HotelNameModel hotelNameModel = new HotelNameModel();
            hotelNameModel.setName(temp[i]);
            hotelNameModel.setImgUrl(icon[i]);
            itemList.add(hotelNameModel);
        }
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {

        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_hotel_names, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        Object o = itemList.get(position);
        if (o instanceof HotelNameModel) {
            HotelNameModel hotelNameModel = (HotelNameModel) o;
            holder.tvItem.setText(hotelNameModel.getName());
            holder.imageView.setImageResource(hotelNameModel.getImgUrl());
        } else if (o instanceof TypeInfoList) {
            TypeInfoList typeInfoList = (TypeInfoList) o;
            holder.tvItem.setText(typeInfoList.getTypeName());
            Picasso.with(this).load(typeInfoList.getImageUrl()).placeholder(R.drawable.progress_animation).into(holder.imageView);
        }


        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.GENERAL_INFO, hotel);

        switch (position) {
            case 0:
                startNextActivity(bundle, GenralHotelInfoActivity.class);
                break;
            case 1:
                startNextActivity(bundle, LocationAndContactActivity.class);
                break;
            case 2:
                startNextActivity(ReviewsAndRatings.class);
                break;
            case 3:
                startNextActivity(bundle, RoomsActivity.class);
                break;
            case 4:
                OtherServicesActivity.startActivity(this, getString(R.string.essentials), new OtherServiceData(hotel.getEssentials()));
                break;
            case 5:
                bundle.putBoolean(AppConstants.BUNDLE_KEYS.FROM_HOTEL, true);
                bundle.putInt(AppConstants.BUNDLE_KEYS.RESERVATION_TYPE, 1);
                FragmentContainerActivity.startActivity(this, AppConstants.FRAGMENT_TYPE.RESERVATION_FRAGMENT, bundle);
                break;
            case 6:
                bundle.putBoolean(AppConstants.BUNDLE_KEYS.FROM_HOTEL, true);
                bundle.putInt(AppConstants.BUNDLE_KEYS.RESERVATION_TYPE, 2);
                FragmentContainerActivity.startActivity(this, AppConstants.FRAGMENT_TYPE.RESERVATION_FRAGMENT, bundle);
                break;
            case 7:
                bundle.putBoolean(AppConstants.BUNDLE_KEYS.FROM_HOTEL, true);
                bundle.putInt(AppConstants.BUNDLE_KEYS.RESERVATION_TYPE, 3);
                FragmentContainerActivity.startActivity(this, AppConstants.FRAGMENT_TYPE.RESERVATION_FRAGMENT, bundle);
                break;
            case 8:
                bundle.putBoolean(AppConstants.BUNDLE_KEYS.FROM_HOTEL, true);
                bundle.putInt(AppConstants.BUNDLE_KEYS.RESERVATION_TYPE, 4);
                FragmentContainerActivity.startActivity(this, AppConstants.FRAGMENT_TYPE.RESERVATION_FRAGMENT, bundle);
                break;
        }
    }

    class Holder {
        ImageView imageView;
        TextView tvItem;

        public Holder(View view) {

            imageView = (ImageView) view.findViewById(R.id.iv_row_icon);
            tvItem = (TextView) view.findViewById(R.id.tv_item);

        }
    }

}
