package com.aboutstays.activity.checkout;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.aboutstays.R;
import com.aboutstays.activity.AppBaseActivity;
import com.aboutstays.model.insiate_checkout.IniciateCheckoutData;
import com.aboutstays.model.insiate_checkout.IniciateCheckoutResponse;
import com.aboutstays.utitlity.ApiRequestGenerator;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.pojo.Address;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;

public class InitiateCheckoutActivity extends AppBaseActivity {

    private String hotelId;
    private String staysId;

    public static void startActivity(Context context, String hotelId, String stayId) {
        Intent intent = new Intent(context, InitiateCheckoutActivity.class);
        intent.putExtra(AppConstants.BUNDLE_KEYS.HOTEL_ID, hotelId);
        intent.putExtra(AppConstants.BUNDLE_KEYS.STAYS_ID, stayId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initiate_checkout);
        initToolBar(getString(R.string.initiate_checkout_small));
        Intent intent = getIntent();
        hotelId = intent.getStringExtra(AppConstants.BUNDLE_KEYS.HOTEL_ID);
        staysId = intent.getStringExtra(AppConstants.BUNDLE_KEYS.STAYS_ID);
        setOnClickListener(R.id.btn_checkout);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getIniticateData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_checkout:
                submitData();
                break;
        }
    }


    private void submitData() {
        boolean isOnline = getRadioStatus(R.id.radio_online);
        int methodeType;
        if (isOnline) {
            methodeType = AppConstants.PAYMENT_METHODE.ONLINE;
        } else {
            methodeType = AppConstants.PAYMENT_METHODE.FRONT_DESK;
        }
        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.postIniciateCheckout(sessionInstance.getUserId(), staysId, hotelId, methodeType);
            executeTask(AppConstants.TASK_CODES.POST_INITIATE_CHECKIN_DATA, httpParamObject);
        }
    }

    private void getIniticateData() {
        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.getInitiateChoutData(sessionInstance.getUserId(), staysId, hotelId);
            executeTask(AppConstants.TASK_CODES.GET_INITIATE_CHECKIN_DATA, httpParamObject);
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_INITIATE_CHECKIN_DATA:
                IniciateCheckoutResponse iniciateCheckoutResponse = (IniciateCheckoutResponse) response;
                if (iniciateCheckoutResponse != null) {
                    IniciateCheckoutData data = iniciateCheckoutResponse.getData();
                    if (data != null) {
                        setData(data);
                    }
                }
                break;
            case AppConstants.TASK_CODES.POST_INITIATE_CHECKIN_DATA:
                BaseApiResponse baseApiResponse= (BaseApiResponse) response;
                if(baseApiResponse!=null){
                    if(baseApiResponse.getError()){
                        showToast(baseApiResponse.getMessage());
                    }else {
                        showToast(baseApiResponse.getMessage());
                        finish();
                    }
                }
                break;
        }
    }

    private void setData(IniciateCheckoutData data) {
        if(!TextUtils.isEmpty(data.getId())) {
            setText("Modify Check Out", R.id.btn_checkout);
        }
        setText(data.getRoomNo() + "", R.id.tv_room_no);
        Boolean officialTrip = data.getOfficialTrip();
        if (officialTrip != null && officialTrip) {
            setText(getString(R.string.official), R.id.tv_trip_type);
        } else {
            setText(getString(R.string.personal), R.id.tv_trip_type);
        }
        setText(data.getFirstName(), R.id.tv_first_name);
        setText(data.getLastName(), R.id.tv_last_name);
        Address address = data.getAddress();
        if (address != null) {
           // String fullAddtess = address.getFullAddtess();
            String fullAddress = address.getDescription();
            setText(fullAddress, R.id.tv_address);
        }
        setText(data.getEmailId(), R.id.tv_email);
        setText(data.getMobileNo(), R.id.tv_mobile);
        setText(data.getTotalAmount() + "", R.id.tv_total_amount);
        setText(data.getGrandTotal() + "", R.id.tv_grand_total);
        Integer paymentMethod = data.getPaymentMethod();
        if (paymentMethod != null && paymentMethod == AppConstants.PAYMENT_METHODE.ONLINE) {
            setRadioStatus(true, R.id.radio_online);
        } else if (paymentMethod != null && paymentMethod == AppConstants.PAYMENT_METHODE.ONLINE) {
            setRadioStatus(true, R.id.radio_frunt_dest);
        }
    }

}
