package com.aboutstays.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.aboutstays.R;
import com.aboutstays.model.NotificationsModel;
import com.aboutstays.model.notification.NotificationsList;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class NotificationDetailsActivity extends AppBaseActivity {

    ImageView imageView;
    public static final String REQUEST_DASHBOARD_DELIVERY_TIME_UI_FORMAT = "hh:mm a, dd MMM";
    private View viewLine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_details);
                initToolBar(getString(R.string.title_notification_details));
        Bundle extras = getIntent().getExtras();
        if(extras != null)
        {
            imageView = (ImageView) findViewById(R.id.iv_dummyImage_Row_RequestRelated);
            viewLine = findViewById(R.id.view_line);
            viewLine.setVisibility(View.GONE);
            NotificationsList notificationsModel= (NotificationsList) extras.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            setText(notificationsModel.getTitle(),R.id.tv_title);
            setText(notificationsModel.getDescription(),R.id.tv_dummyText_Row_RequestRelated);
            long created = notificationsModel.getCreated();
            String dateString = new SimpleDateFormat(REQUEST_DASHBOARD_DELIVERY_TIME_UI_FORMAT).format(new Date(created));
            setText(dateString.toUpperCase(),R.id.tv_date);
            String iconUrl = notificationsModel.getIconUrl();
            if (!TextUtils.isEmpty(iconUrl)){
                Picasso.with(this).load(iconUrl).placeholder(R.drawable.progress_animation).into(imageView);
            }

        }

    }
}
