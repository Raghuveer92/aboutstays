package com.aboutstays.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.adapter.MainChipviewAdapter;
import com.aboutstays.model.ActionTag;
import com.aboutstays.model.hotels.Amenity;
import com.aboutstays.model.hotels.AwardsData;
import com.aboutstays.model.hotels.GeneralInformation;
import com.aboutstays.model.hotels.HotelAddress;
import com.aboutstays.model.hotels.Hotel;
import com.aboutstays.model.hotels.NameImageGeneralInfo;
import com.aboutstays.model.hotels.OtherServiceData;
import com.aurelhubert.simpleratingbar.SimpleRatingBar;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;
import simplifii.framework.widgets.CustomRatingBar;

/**
 * Created by ajay on 24/12/16.
 */

public class GenralHotelInfoActivity extends AppBaseActivity {

    private List<Chip> chipListCards, chipListAminities, chipListAwards;
    private ChipView chipViewCard, chipViewAmenities, chipViewAwards;
    private TextView tvTitle, tvAddress, tvDesc, tvCheckIn, tvCheckOut;

    private Hotel hotel;

    private MainChipviewAdapter adapterCards, adapterAminities, adapterAwards;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_genral_info);

        findViews();
        initToolBar("Hotel Info");

        Bundle getGeneralInfo = getIntent().getExtras();
        if (getGeneralInfo != null) {
            hotel = (Hotel) getGeneralInfo.getSerializable(AppConstants.BUNDLE_KEYS.GENERAL_INFO);
            setdata();
        }

        chipListAminities = initAminitiesChipList();
        chipListCards = initCardsChipList();
        chipListAwards = initAwardChipList();

        chipViewCard = (ChipView) findViewById(R.id.chip_view_cards);
        chipViewAmenities = (ChipView) findViewById(R.id.chip_view_amenities);
        chipViewAwards = (ChipView) findViewById(R.id.chip_view_awards);
        // chipViewAwards.setHasBackground(true);
        adapterCards = new MainChipviewAdapter(this, chipListCards, 0);
        adapterAminities = new MainChipviewAdapter(this, chipListAminities, 1);
        adapterAwards = new MainChipviewAdapter(this, chipListAwards, 2);

        adapterCards.setChipList(chipListCards);
        adapterAminities.setChipList(chipListAminities);
        adapterAwards.setChipList(chipListAwards);

        chipViewCard.setAdapter(adapterCards);
        chipViewAmenities.setAdapter(adapterAminities);
        chipViewAwards.setAdapter(adapterAwards);
        setOnClickListener(R.id.more_amenities, R.id.more_awards, R.id.more_cards);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.more_amenities:
                OtherServicesActivity.startActivity(this, getString(R.string.amenities), new OtherServiceData(hotel.getAmenities()));
                break;
            case R.id.more_awards:
                OtherServicesActivity.startActivity(this, getString(R.string.awards), new OtherServiceData().setList(hotel.getListAwards()));
                break;
            case R.id.more_cards:
                OtherServicesActivity.startActivity(this, getString(R.string.cards_accepted), new OtherServiceData().setAwardsList(hotel.getListCardAccepted()));
                break;
        }
    }

    private void findViews() {
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        tvDesc = (TextView) findViewById(R.id.tv_description);
        tvCheckIn = (TextView) findViewById(R.id.tv_check_in);
        tvCheckOut = (TextView) findViewById(R.id.tv_check_out);
    }

    private void setdata() {
        if (hotel != null) {
            GeneralInformation generalInformation = hotel.getGeneralInformation();
            if (generalInformation != null) {
                tvTitle.setText(generalInformation.getName());
                tvDesc.setText(generalInformation.getDescription());

                String checkinTime = Util.convertDateFormat(generalInformation.getCheckInTime(), Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
                tvCheckIn.setText(checkinTime);
                String checkoutTime = Util.convertDateFormat(generalInformation.getCheckOutTime(), Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
                tvCheckOut.setText(checkoutTime);

                HotelAddress address = generalInformation.getAddress();
                String logoUrl = generalInformation.getLogoUrl();
                setImage(logoUrl, R.id.iv_logo_hotel_info, R.drawable.progress_animation);
                if (address != null) {
                    tvAddress.setText(address.getCityCountry());
                }
            }
            CustomRatingBar customRatingBar = (CustomRatingBar) findViewById(R.id.custom_rating_bar);
            customRatingBar.setRating(hotel.getHotelType());
        }
    }


    private List<Chip> initAminitiesChipList() {
        ArrayList<Chip> list = new ArrayList<>();
        if (hotel != null) {
                List<Amenity> amenities = hotel.getAmenities();
                if (CollectionUtils.isNotEmpty(amenities)) {
                    List<Amenity> listAmenity = new ArrayList<>();
                    if (amenities.size() > 4) {
                        for (int x = 0; x < 4; x++) {
                            listAmenity.add(amenities.get(x));
                        }
                        showVisibility(R.id.more_amenities);
                        setText("+" + (amenities.size() - 4), R.id.tv_more_amenities);
                    } else {
                        listAmenity.addAll(amenities);
                    }
                    if (CollectionUtils.isNotEmpty(amenities)) {
                        for (Amenity amenity : listAmenity) {
                            ActionTag tag = new ActionTag();
                            tag.setTitle(amenity.getName());
                            tag.setImageUrl(amenity.getIconUrl());
                            list.add(tag);
                        }
                    }
                } else {
                    hideVisibility(R.id.tv_amenities, R.id.line_amenities);
                }
        }
        return list;
    }

    private List<Chip> initAwardChipList() {
        ArrayList<Chip> list = new ArrayList<>();
        if (hotel != null) {
                List<AwardsData> listAwards = new ArrayList<>();
                List<AwardsData> listAwardsList = hotel.getListAwards();
                if (CollectionUtils.isNotEmpty(listAwardsList)) {
                    if (listAwardsList.size() > 4) {
                        for (int x = 0; x < 4; x++) {
                            listAwards.add(listAwardsList.get(x));
                        }
                        showVisibility(R.id.more_awards);
                        setText("+" + (listAwardsList.size() - 4), R.id.tv_more_awards);

                    } else {
                        listAwards.addAll(listAwardsList);
                    }
                    for (AwardsData nameImageGeneralInfo : listAwards) {
                        ActionTag tag = new ActionTag();
                        NameImageGeneralInfo award = nameImageGeneralInfo.getAward();
                        if(adapterAwards!=null){
                            tag.setTitle(award.getName());
                            tag.setImageUrl(award.getImageUrl());
                        }
                        list.add(tag);
                    }
                }
            }
            else {
                hideVisibility(R.id.tv_award);
            }
        return list;
    }

    private List<Chip> initCardsChipList() {
        ArrayList<Chip> list = new ArrayList<>();
        if (hotel != null) {
                List<NameImageGeneralInfo> listCards = new ArrayList<>();
                List<NameImageGeneralInfo> listCardAccepted = hotel.getListCardAccepted();
                if (CollectionUtils.isNotEmpty(listCardAccepted)) {
                    if (listCardAccepted.size() > 7) {
                        for (int x = 0; x < 6; x++) {
                            listCards.add(listCardAccepted.get(x));
                        }
                        showVisibility(R.id.more_cards);
                        setText("+" + (listCardAccepted.size() - 6), R.id.tv_more_cards);

                    } else {
                        listCards.addAll(listCardAccepted);
                    }

                    for (NameImageGeneralInfo nameImageGeneralInfo : listCards) {
                        ActionTag tag = new ActionTag();
                        tag.setTitle(nameImageGeneralInfo.getName());
                        tag.setImageUrl(nameImageGeneralInfo.getImageUrl());
                        list.add(tag);
                    }
                }
            }
            else {
                hideVisibility(R.id.tv_card_accepted, R.id.line_card_accepted);
            }
        return list;
    }


}

