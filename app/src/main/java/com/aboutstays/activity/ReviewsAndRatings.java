package com.aboutstays.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.enums.RatingEnum;
import com.aboutstays.model.ReviewModel;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.widgets.CustomRatingBar;

/**
 * Created by ajay on 26/12/16.
 */

public class ReviewsAndRatings extends AppBaseActivity implements CustomListAdapterInterface {

    private int ratingList[] = {600, 400, 350, 250, 120};
    private int colorId[] = {R.color.star_5, R.color.star_4, R.color.star_3, R.color.star_2, R.color.star_1};
    private List<ReviewModel> list;
    private ListView listview;
    private CustomListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews_raings);

        initToolBar("Reviews & Ratings");

        list = new ArrayList<>();
        listview = (ListView) findViewById(R.id.list_view);
        adapter = new CustomListAdapter(this, R.layout.row_review_rating, list, this);
        listview.setAdapter(adapter);

        // addHeaderToList();

        initData();
        adapter.notifyDataSetChanged();

        setRating();
    }

    private void setRating() {

        int index = 0;
        int index2 = 0;
        View ratingView = getLayoutInflater().inflate(R.layout.row_header_review, null);
        LinearLayout llRatingContainer = (LinearLayout) ratingView.findViewById(R.id.ll_rating_container);
        // View v = getLayoutInflater().inflate(R.layout.view_row_rating_header, null);
        for (RatingEnum ratingEnum : RatingEnum.values()) {
            View v = getLayoutInflater().inflate(R.layout.view_row_rating_header, null);

            View ratingLineView = v.findViewById(R.id.view_rating_line);
            TextView tvRatingCount = (TextView) v.findViewById(R.id.tv_rating_no);
            int ratingCount = ratingList[index++];
            tvRatingCount.setText(String.valueOf(ratingCount));
            int maxWidth = 600;//v.getWidth()-tvRatingCount.getWidth();
            int ratingLineWidth = RatingEnum.getWidthAccordingToValue(maxWidth, ratingCount, 600);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ratingLineWidth, ratingLineView.getHeight());
            ratingLineView.getLayoutParams().width = ratingLineWidth;
            ratingLineView.setBackgroundColor(getResources().getColor(ratingEnum.getColorId()));
            llRatingContainer.addView(v);
        }

        listview.addHeaderView(ratingView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_rating_options, menu);
        return true;
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.by_rating:
                break;
            case R.id.by_date:
                break;

        }

        return super.onOptionsItemSelected(item);
    }*/

    private void addHeaderToList() {
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup mHeader = (ViewGroup) inflater.inflate(R.layout.row_header_review, listview, false);
        listview.addHeaderView(mHeader, null, false);
    }

    private void initData() {

        for (int i = 0; i < 10; i++) {
            ReviewModel ob = new ReviewModel();

            ob.setTitle("Mr. Perera" + i);
            ob.setDate("12/12/201" + i);
            //ob.setReview("");
            list.add(ob);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {

        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_review_rating, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        ReviewModel ob = list.get(position);
        holder.tvTitle.setText(ob.getTitle());
        holder.tvDate.setText(ob.getDate());

        return convertView;
    }

    class Holder {
        TextView tvTitle, tvDate, tvReview;
        CustomRatingBar ratingBar;

        public Holder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.tv_title_review);
            tvDate = (TextView) view.findViewById(R.id.tv_date_review);
            tvReview = (TextView) view.findViewById(R.id.tv_review);
             ratingBar = (CustomRatingBar) view.findViewById(R.id.ratingbar);
        }

    }
}
