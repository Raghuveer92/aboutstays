package com.aboutstays.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.aboutstays.R;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by aman on 22/01/17.
 */

public class ActivityResetPasswordOptions extends AppBaseActivity{

    private RadioGroup rgOptions;
    private EditText etEmail, etMobile;
    private Button btnOk;
    private int CHECKED_OPTION;
    private Integer userType;
    private String userValue;
    private RadioButton rbMobile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivty_reset_password_options);

        initToolBar("CHOOSE OPTION");
        initCroutonView();

        userType = getIntent().getExtras().getInt(AppConstants.BUNDLE_KEYS.TYPE);
        userValue = getIntent().getExtras().getString(AppConstants.BUNDLE_KEYS.VALUE);
        rgOptions = (RadioGroup) findViewById(R.id.rg_reset_password);
        etEmail = (EditText) findViewById(R.id.et_option_email);
        etMobile = (EditText) findViewById(R.id.et_option_phone);
        rbMobile = (RadioButton) findViewById(R.id.rb_mobile);
        btnOk = (Button) findViewById(R.id.btn_ok);

        setOnClickListener(R.id.btn_ok);
        showCheckedOptions();

        rgOptions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb_email:
                        etEmail.setVisibility(View.VISIBLE);
                        etMobile.setVisibility(View.GONE);

                        break;
                    case R.id.rb_mobile:
                        etMobile.setVisibility(View.VISIBLE);
                        etEmail.setVisibility(View.GONE);
                        break;
                }
                etEmail.setText(userValue);
                CHECKED_OPTION = userType;
            }
        });
    }

    private void showCheckedOptions(){
        if(userType == AppConstants.TYPE.PHONE){
            CHECKED_OPTION = AppConstants.TYPE.PHONE;
            etMobile.setVisibility(View.VISIBLE);
            etEmail.setVisibility(View.GONE);
            etMobile.setText(userValue);
            rbMobile.setChecked(true);
            etMobile.setSelection(userValue.length());
        } else if(userType == AppConstants.TYPE.EMAIL){
            etEmail.setText(userValue);
            etEmail.setSelection(userValue.length());
            CHECKED_OPTION = AppConstants.TYPE.EMAIL;
        }
    }

    private void validateAndSubmitMobile() {
        String mobile = etMobile.getText().toString().trim();
        if(TextUtils.isEmpty(mobile)){
            showCrouton("Fields can not be empty");
            return;
        } else if(mobile.length() != 10){
            showCrouton("Mobile number should be of 10 digits");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.NUMBER, mobile);
        bundle.putInt(AppConstants.BUNDLE_KEYS.BUTTON_TARGET, AppConstants.ACTIVITY_CONSTANTS.TARGET_CHANGE_PASSWORD);
        startNextActivity(bundle, ValidateOtpActivity.class);
    }

    private void validateAndSubmitEmail() {
        String email = etEmail.getText().toString().trim();
        if(TextUtils.isEmpty(email)){
            showCrouton("Fields can not be empty");
            return;
        }else if(!Util.isValidEmail(email)){
            showCrouton("Please enter a valid email");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.EMAIL, email);
        startNextActivity(bundle, ActivityResetPasswordByEmailMsg.class);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_ok:
                if(CHECKED_OPTION == AppConstants.TYPE.EMAIL){
                    validateAndSubmitEmail();
                } else if(CHECKED_OPTION == AppConstants.TYPE.PHONE){
                    validateAndSubmitMobile();
                }
                break;
        }
    }
}
