package com.aboutstays.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.MenuItemCompat;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.LaundryItemFragment;
import com.aboutstays.model.cart.LaundryCartItemData;
import com.aboutstays.model.cart.LaundryCartResponse;
import com.aboutstays.model.laundry.LaundryData;
import com.aboutstays.model.laundry.LaundryItemData;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;

public class LaundryItemActivity extends AppBaseActivity implements LaundryItemFragment.AddToCartListener, SearchView.OnQueryTextListener {

    private LaundryData laundryData;
    private String serviceId;
    LaundryItemFragment laundryItemFragment;
    private String staysId;
    private LaundryCartItemData cartData = new LaundryCartItemData();
    private String laundryType;
    private TextView tv;
    private String gsId;
    private String hotelId;
    private boolean fromSameDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laundry_item);
        registerCartReceiver();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            staysId = bundle.getString(AppConstants.BUNDLE_KEYS.STAYS_ID);
            gsId = bundle.getString(AppConstants.BUNDLE_KEYS.GS_ID);
            hotelId = bundle.getString(AppConstants.BUNDLE_KEYS.HOTEL_ID);
            laundryType = bundle.getString(AppConstants.BUNDLE_KEYS.LAUNDRY_TYPE);
            laundryData = (LaundryData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.LAUNDRY_ITEMS);
            cartData = (LaundryCartItemData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.CART_DATA);
            fromSameDay = bundle.getBoolean(AppConstants.BUNDLE_KEYS.FROM_SAME_DAY);
            if (laundryData != null) {
                serviceId = laundryData.getServiceId();
            }
        }
        laundryItemFragment = LaundryItemFragment.getInstance(this);
        laundryItemFragment.sendData(serviceId, staysId, cartData, laundryType, fromSameDay);
        getSupportFragmentManager().beginTransaction().add(R.id.frame_frag_container, laundryItemFragment).commit();
        initToolBar("Laundry Item");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_service, menu);

        MenuItem item = menu.findItem(R.id.action_room_service);
        MenuItemCompat.setActionView(item, R.layout.action_menu_count_layout);
        RelativeLayout notifCount = (RelativeLayout) MenuItemCompat.getActionView(item);

        ImageView iv = (ImageView) notifCount.findViewById(R.id.iv_roomservice);
        iv.setColorFilter(Color.WHITE);
        iv.setImageResource(R.mipmap.laundry);
        iv.setOnClickListener(this);

        tv = (TextView) notifCount.findViewById(R.id.actionbar_notifcation_textview);

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
//        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);
        refreshCartData();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_roomservice:
                OrderDetailActivity.startActivity(LaundryItemActivity.this, gsId, staysId, hotelId, AppConstants.STAYS_TYPE.LAUNDRY, AppConstants.FROM_ORDERING.LAUNDRY );
                break;
        }
    }

//    @Override
//    public void onBackPressed() {
//        setResult(RESULT_OK);
//        finish();
//    }

    @Override
    public void searchCallback() {
        setResult(RESULT_OK);
        finish();
    }

    private void refreshCartData() {
        LaundryCartItemData laundryCartData = LaundryCartResponse.getInstance();
        if (laundryCartData.getListLaundryItemOrderDetails().size() > 0) {
            tv.setText(laundryCartData.getListLaundryItemOrderDetails().size() + "");
            tv.setVisibility(View.VISIBLE);
        } else {
            tv.setText("0");
            tv.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void updateCart() {
        refreshCartData();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        laundryItemFragment.searchQuery(query);
//        laundryItemFragment.getSearchedData();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        laundryItemFragment.searchQuery(newText);
//        laundryItemFragment.getSearchedData();
        return true;
    }

    @Override
    public void onCartClear() {
        laundryItemFragment.refreshData();
//        laundryItemFragment.getLaundryItemData();
    }

    @Override
    public void onCartUpdate() {
        refreshCartData();
//        laundryItemFragment.refreshData();
//        laundryItemFragment.getLaundryItemData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(AppConstants.RESULT_CODE.RESULT_NULL);
    }
}
