package com.aboutstays.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.initiate_checkIn.CheckInData;
import com.aboutstays.model.initiate_checkIn.Company;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

public class SelectCompanyActivity extends AppBaseActivity implements CustomListAdapterInterface {

    private TextView btnAddDocuments;
    private ListView listDoc;
    private CustomListAdapter listAdapter;
    private List<Company> selectCompanyModels = new ArrayList<>();
    private boolean fromCheckIn;
    private CheckInData checkInData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_company);

        initToolBar("Select Company");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fromCheckIn = bundle.getBoolean(AppConstants.BUNDLE_KEYS.FROM_CHECKIN);
            checkInData = (CheckInData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        }

        listDoc = (ListView) findViewById(R.id.list_doc);
        listAdapter = new CustomListAdapter(this, R.layout.row_identity_doc, selectCompanyModels, this);
        listDoc.setAdapter(listAdapter);

        LayoutInflater inflater = getLayoutInflater();
        View inflate = inflater.inflate(R.layout.doc_list_footer, listDoc, false);
        TextView tvAdd = (TextView) inflate.findViewById(R.id.tv_add_document);
        tvAdd.setText("ADD COMPANY");
        tvAdd.setOnClickListener(this);
        listDoc.addFooterView(inflate);

        if (fromCheckIn && CollectionUtils.isNotEmpty(checkInData.getCompanies())){
            List<Company> companies = checkInData.getCompanies();
            selectCompanyModels.addAll(companies);
            listAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_add_document:
                openDialog();
                break;
        }
    }

    private void openDialog() {

        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.add_company_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.et_cmp_name);

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Company selectCompanyModel = new Company();
                                selectCompanyModel.setName(userInput.getText().toString().trim());
                                selectCompanyModels.add(selectCompanyModel);
                                listAdapter.notifyDataSetChanged();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_done, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.ic_action_done:
                CheckInData checkInData = new CheckInData();
                if (CollectionUtils.isNotEmpty(selectCompanyModels)){
                    checkInData.setCompanies(selectCompanyModels);
                }
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, checkInData);
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_identity_doc, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        Company selectCompanyModel = selectCompanyModels.get(position);
        holder.tvComName.setText(selectCompanyModel.getName());
        holder.tvDocNum.setVisibility(View.GONE);

        return convertView;
    }

    class Holder {
        TextView tvComName, tvDocNum;

        public Holder(View view) {
            tvComName = (TextView) view.findViewById(R.id.tv_doc_name);
            tvDocNum = (TextView) view.findViewById(R.id.tv_doc_num);
        }
    }
}
