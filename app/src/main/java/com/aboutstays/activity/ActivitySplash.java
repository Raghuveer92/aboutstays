package com.aboutstays.activity;

import android.os.Bundle;
import android.os.Handler;

import com.aboutstays.R;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by aman on 24/01/17.
 */

public class ActivitySplash extends AppBaseActivity{

    private final int SPLASH_DISPLAY_TIME = 3000;
    private boolean isLoggedIn;
    private Boolean ifFirst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Preferences.initSharedPreferences(this);
        isLoggedIn = Preferences.getData(Preferences.LOGIN_KEY, false);
        ifFirst = Preferences.getData(Preferences.IF_FIRST, false);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(isLoggedIn){
                    startNextActivity(DrawerActivity.class);
                } else if (!ifFirst){
                    startNextActivity(IntroActivity.class);
                } else {
                    startNextActivity(LoginActivity.class);
                }
                finish();
            }
        }, SPLASH_DISPLAY_TIME);
    }
}
