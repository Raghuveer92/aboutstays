package com.aboutstays.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.hotels.Amenity;
import com.aboutstays.model.hotels.OtherServiceData;

import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.utility.AppConstants;

public class OtherServicesActivity extends AppBaseActivity implements CustomListAdapterInterface {
    private String title="";
    private TextView tvEmptyView;
    private OtherServiceData otherServiceData;
    private List<Amenity> iconData;

    public static void startActivity(Context context, String title, OtherServiceData otherServiceData) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.TITLE_NAME,title);
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,otherServiceData);
        Intent intent=new Intent(context,OtherServicesActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_services);
        initToolBar(title);
        tvEmptyView = (TextView) findViewById(R.id.tv_empty_view);
        GridView gridView= (GridView) findViewById(R.id.grid_other_data);
        if(otherServiceData!=null){
            iconData = otherServiceData.getIconData();
            if(iconData!=null){
                CustomListAdapter customListAdapter=new CustomListAdapter(this,R.layout.row_other_service_icon,iconData,this);
                gridView.setAdapter(customListAdapter);
            }
        }
        gridView.setEmptyView(tvEmptyView);
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        super.loadBundle(bundle);
        title=bundle.getString(AppConstants.BUNDLE_KEYS.TITLE_NAME);
        otherServiceData= (OtherServiceData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        if(convertView==null){
            convertView=inflater.inflate(resourceID,null);
        }
        Amenity iconData = this.iconData.get(position);
        setImage(iconData.getIconUrl(),R.id.iv_service_icon,R.drawable.progress_animation,convertView);
        setText(iconData.getName(),R.id.tv_service,convertView);
        return convertView;
    }
}
