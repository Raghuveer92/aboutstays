package com.aboutstays.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.booking.BookingData;
import com.aboutstays.model.booking.ListBooking;
import com.aboutstays.model.hotels.GeneralInformation;
import com.aboutstays.model.stayline.AddtoStaylineApi;
import com.aboutstays.model.stayline.GeneralSearchInformation;
import com.aboutstays.model.stayline.HotelBasicInfo;
import com.aboutstays.model.stayline.SearchedHotelAddress;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.aboutstays.utitlity.DatePickerUtil;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class AddToStaylineActivity extends AppBaseActivity {
    private TextView tvCheckInDate, tvCheckOutdate;
    private TextView tvHotelName, tvHotelAddress, tvGuestName, tvCheckindate, tvCheckoutDate, tvRoomType, tvpax;
    private Button btnAdd;
    private String dateFormate = "dd-MM-yyyy";
    private Calendar calendarInstance;
    private BookingData bookingData;
    private HotelBasicInfo listBooking;
    private String userId;
    private String bookingId;
    private String hotelId;
    private String roomId;
    private SwitchCompat switchBtn;
    private ImageView ivHotelIcon;
    private String checkOutTime, checkInTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_stayline);
        initToolBar(getString(R.string.retrieve_stay));
        calendarInstance = Calendar.getInstance();
        tvCheckInDate = (TextView) findViewById(R.id.text_checkin_date);
        tvCheckOutdate = (TextView) findViewById(R.id.text_checkout_date);
        TextView tvPartner = (TextView) findViewById(R.id.tv_partner_msg);
        btnAdd = (Button) findViewById(R.id.btn_add_stayline);
        switchBtn = (SwitchCompat) findViewById(R.id.switch_official);
        tvHotelName = (TextView) findViewById(R.id.tv_hotel_name_retrieve);
        tvHotelAddress = (TextView) findViewById(R.id.tv_hotel_address_retrieve);
        ivHotelIcon = (ImageView) findViewById(R.id.iv_hotel_icon);

        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            userId = sessionInstance.getUserId();
        }

        Bundle b = getIntent().getExtras();
        if (b != null) {
            HotelBasicInfo searchHotelList = (HotelBasicInfo) b.getSerializable(AppConstants.BUNDLE_KEYS.SEARCH_HOTEL_RESPONSE);
            if (searchHotelList != null) {
                hotelId = searchHotelList.getHotelId();
                if (searchHotelList.getGeneralInformation() != null) {
                    checkInTime = searchHotelList.getGeneralInformation().getCheckInTime();
                    checkOutTime = searchHotelList.getGeneralInformation().getCheckOutTime();
                }
                setDataIntoList(searchHotelList);
            }
        }

        setOnClickListener(R.id.rl_checkIn, R.id.rl_checkout, R.id.btn_add_stayline);
    }

    private void setDataIntoList(HotelBasicInfo searchHotelList) {
        GeneralSearchInformation generalInformation = searchHotelList.getGeneralInformation();
        if (generalInformation != null) {
            tvHotelName.setText(generalInformation.getName());
            SearchedHotelAddress address = generalInformation.getAddress();
            if (address != null) {
                tvHotelAddress.setText(address.getCompleteAddress());
            }
            if (!TextUtils.isEmpty(generalInformation.getLogoUrl())) {
                Picasso.with(this).load(generalInformation.getLogoUrl())
                        .placeholder(R.drawable.progress_animation)
                        .into(ivHotelIcon);
            }
        }
    }

    private void addtoStayLine(String userId, String bookingId, String hotelId, String roomId, String checkinDate, String checkoutDate, String checkinTime, String checkoutTime) {
        HttpParamObject httpParamObject = ApiRequestGenerator.addToStayline(hotelId, roomId, userId, bookingId, checkinDate, checkoutDate, checkinTime, checkoutTime, switchBtn.isChecked());
        executeTask(AppConstants.TASK_CODES.ADD_STAYLINE, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.ADD_STAYLINE:
                AddtoStaylineApi addtoStaylineApi = (AddtoStaylineApi) response;
                if (addtoStaylineApi != null && !addtoStaylineApi.getError()) {
                    openFragment();
                } else {
                    showToast(addtoStaylineApi.getMessage());
                }
                break;
        }
    }

    private void openFragment() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppConstants.BUNDLE_KEYS.FROM_ADD_STAY, true);
        Intent intent = new Intent(this, DrawerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.left_arrow;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.rl_checkIn:
                openCalender(tvCheckInDate);
                break;
            case R.id.rl_checkout:
                openCalender(tvCheckOutdate);
                break;
            case R.id.btn_add_stayline:
//                startNextActivity(SelectLoyaltyProgramActivity.class);
//                startNextActivity(StayLineFragment.class);
                validateData();
                break;
        }
    }

    private void validateData() {
        if (TextUtils.isEmpty(userId) || TextUtils.isEmpty(hotelId)) {
            showToast("UserId or Room Id is empty");
            return;
        }
        String checkinDate = tvCheckInDate.getText().toString().trim();
        String checkoutDate = tvCheckOutdate.getText().toString().trim();

        long checkinMilies = Util.convertStringToLong(dateFormate, checkinDate);
        long checkOutMilies = Util.convertStringToLong(dateFormate, checkoutDate);

        if (TextUtils.isEmpty(checkinDate) || TextUtils.isEmpty(checkoutDate)) {
            showToast("Please select Check-in and Check-out date");
            return;
        }

        if (checkinMilies != 0 && checkOutMilies != 0 && checkinMilies > checkOutMilies){
            showToast("CheckIn date must be smaller than CheckOut date");
            return;
        }

        addtoStayLine(userId, bookingId, hotelId, roomId, checkinDate, checkoutDate, checkInTime, checkOutTime);
    }

    private void openCalender(final TextView tvDate) {

        DatePickerUtil datePickerUtil = new DatePickerUtil(dateFormate, calendarInstance, getFragmentManager(), getString(R.string.select_date), false, false, new DatePickerUtil.OnDateSelected() {
            @Override
            public void onDateSelected(Calendar calendar, String date) {
                tvDate.setText(date);
            }

            @Override
            public void onCanceled() {
//                tvDate.setText("");
            }
        });
        datePickerUtil.show();
    }
}
