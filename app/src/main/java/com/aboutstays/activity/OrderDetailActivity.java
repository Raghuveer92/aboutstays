package com.aboutstays.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.model.cart.FoodItemOrderDetails;
import com.aboutstays.model.cart.GetCartData;
import com.aboutstays.model.cart.HkItemOrderDetails;
import com.aboutstays.model.cart.HouseKeepingCart;
import com.aboutstays.model.cart.HouseKeepingCartData;
import com.aboutstays.model.cart.ItemOrderDetails;
import com.aboutstays.model.cart.LaundryCart;
import com.aboutstays.model.cart.LaundryServiceCartData;
import com.aboutstays.model.cart.RoomServiceCart;
import com.aboutstays.model.cart.RoomServiceCartData;
import com.aboutstays.model.cart.RoomServiceCartResponse;
import com.aboutstays.model.cart.HouseKeepingCartResponse;
import com.aboutstays.model.cart.LaundryCartResponse;
import com.aboutstays.model.cart.HouseKeepingCartResponseData;
import com.aboutstays.model.cart.LaundryCartItemData;
import com.aboutstays.model.food.FoodDataByType;
import com.aboutstays.model.housekeeping.HouseKeepingItemData;
import com.aboutstays.model.laundry.LaundryItemData;
import com.aboutstays.model.oder_detail.CartData;
import com.aboutstays.model.oder_detail.OrderDetail;
import com.aboutstays.receiver.StayCartUpdateReceiver;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.aboutstays.utitlity.DatePickerUtil;
import com.aboutstays.utitlity.TimePickerUtil;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import simplifii.framework.ListAdapters.CustomExpandableListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.receivers.CartUpdateReceiver;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class OrderDetailActivity extends AppBaseActivity {

    private String staysId;
    private String hotelId;
    private String type;
    private CartData cartData;
    private TextView tvTotalCount;
    private TextView tvTotalPrice;
    private Calendar calendarInstance;
    private boolean fromSameDay;
    private int fromOrder;
    private String title;
    private String deliveryDate;
    private RoomServiceCart roomServiceCart;
    private String userId;
    private CustomExpandableListAdapter customExpandableListAdapter;
    private HouseKeepingCart houseKeepingCart;
    private LaundryCart laundryCart;
    private long checkOutDate;

    public static void startActivity(Activity context, String gsId, String staysId, String hotelId, String type, int fromOrder) {
        Intent intent = new Intent(context, OrderDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.GS_ID, gsId);
        bundle.putString(AppConstants.BUNDLE_KEYS.STAYS_ID, staysId);
        bundle.putString(AppConstants.BUNDLE_KEYS.HOTEL_ID, hotelId);
        bundle.putString(AppConstants.BUNDLE_KEYS.TYPE, type);
        bundle.putInt(AppConstants.BUNDLE_KEYS.FROM_ORDER, fromOrder);

        intent.putExtras(bundle);
        context.startActivityForResult(intent, AppConstants.REQUEST_CODE.ORDER_PLACED);
    }

    private ExpandableListView expandableListView;
    private String gsId;
    private int totalPrice;
    private int totalQuantity;
    private TextView tvRequestNow;
    private EditText etComment;
    private TextView tvDeliveryDate;
    private boolean reqNow;
    private long pickupTime;
    private boolean isPreference;
    private int laundryPrefs = 1;
    public static final String ORDER_DETAILS_PICKUP_DATE = "MMM dd";
    public static final String ORDER_DETAILS_DATE_YEAR_FORMAT = "dd MM yyyy";
    public static final String ORDER_DETAILS_PICKUP_DATE_AND_TIME = "MMM dd, hh:mm a";
    public String datePickerDate;
    private static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    private String format;
    private Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        showVisibility(R.id.btn_place_order);
        initToolBar("Order Details");
        calendarInstance = Calendar.getInstance();

        checkOutDate = Preferences.getData(AppConstants.PREF_KEYS.CHECKOUT_DATE_TIME, 0l);

        UserSession userSession = UserSession.getSessionInstance();
        if (userSession != null) {
            userId = userSession.getUserId();
        }

        calendar = Calendar.getInstance();
        expandableListView = (ExpandableListView) findViewById(R.id.lv_order_details);
        setOnClickListener(R.id.btn_place_order);
        expandableListView.setEmptyView(findViewById(R.id.tv_empty_view));
        setHeader();
        setFooter();
    }

    private void setHeader() {
        LayoutInflater inflater = getLayoutInflater();
        View inflate = inflater.inflate(R.layout.view_order_details_header, expandableListView, false);
        TextView tvRate = (TextView) inflate.findViewById(R.id.tv_rate);
        if (fromOrder == AppConstants.FROM_ORDERING.HOUSEKEEPING) {
            tvRate.setVisibility(View.INVISIBLE);
        } else {
            tvRate.setVisibility(View.VISIBLE);
        }
        expandableListView.addHeaderView(inflate);
    }

    private void setFooter() {
        LayoutInflater inflater = getLayoutInflater();
        View inflate = inflater.inflate(R.layout.view_order_details_footer, expandableListView, false);
        tvTotalPrice = (TextView) inflate.findViewById(R.id.tv_total_price);
        tvTotalCount = (TextView) inflate.findViewById(R.id.tv_total_count);
        tvRequestNow = (TextView) inflate.findViewById(R.id.tv_add);
//        RelativeLayout layOr = (RelativeLayout) inflate.findViewById(R.id.lay_or);
        TextView tvTitle = (TextView) inflate.findViewById(R.id.tv_title);
        etComment = (EditText) inflate.findViewById(R.id.et_comment);
        tvDeliveryDate = (TextView) inflate.findViewById(R.id.tv_date_time);
        LinearLayout rlDatePick = (LinearLayout) inflate.findViewById(R.id.rl_row_data);
        TextView tvMsg = (TextView) inflate.findViewById(R.id.tv_msg);
        if (fromOrder == AppConstants.FROM_ORDERING.HOUSEKEEPING) {
            tvTitle.setText(getString(R.string.delivery_date_time));
        } else {
            tvTitle.setText(getString(R.string.preferred_dateTime));
        }
        tvMsg.setText(R.string.request_automatic_initiated);
//        layOr.setVisibility(View.GONE);
        if (fromOrder != AppConstants.FROM_ORDERING.HOUSEKEEPING) {
            tvTotalPrice.setText("\u20B9 " + totalPrice);
        } else {
            tvTotalPrice.setText("");
        }
        tvTotalCount.setText(totalQuantity + "");
        rlDatePick.setOnClickListener(this);
        tvRequestNow.setOnClickListener(this);
        expandableListView.addFooterView(inflate);
        if (isPreference) {
            showVisibility(inflate, R.id.rl_preference);
        }
        RadioGroup rg = (RadioGroup) inflate.findViewById(R.id.rg_preference);
        rg.check(R.id.rb_folded);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.rb_folded:
                        laundryPrefs = 1;
                        break;
                    case R.id.rb_on_hanger:
                        laundryPrefs = 2;
                        break;
                }
            }
        });
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        gsId = bundle.getString(AppConstants.BUNDLE_KEYS.GS_ID);
        staysId = bundle.getString(AppConstants.BUNDLE_KEYS.STAYS_ID);
        hotelId = bundle.getString(AppConstants.BUNDLE_KEYS.HOTEL_ID);
        type = bundle.getString(AppConstants.BUNDLE_KEYS.TYPE);
        fromOrder = bundle.getInt(AppConstants.BUNDLE_KEYS.FROM_ORDER);
        if (type.equals(AppConstants.STAYS_TYPE.LAUNDRY)) {
            isPreference = true;
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAllCartItem();
    }

    private void getAllCartItem() {
        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.getAllCartItem(sessionInstance.getUserId(), staysId, hotelId, type);
            executeTask(AppConstants.TASK_CODES.ALL_CART_ITEMS, httpParamObject);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_row_data:
                setReqNowFalse();
                setDateandTime();
                break;
            case R.id.tv_add:
                if (reqNow == false) {
                    reqNow = true;
                    tvRequestNow.setBackgroundResource(R.drawable.shape_button_selected);
                    tvRequestNow.setTextColor(Color.WHITE);
                    tvDeliveryDate.setText("");
//                    tvRequestNow.setText(R.string.requested);
                } else {
                    setReqNowFalse();
                }
                break;
            case R.id.btn_place_order:
                boolean ifBeforeCheckout = Util.ifGreaterThnCheckOut(pickupTime, checkOutDate);
                if (ifBeforeCheckout) {
                    placeOrder();
                } else {
                    errorDialog(getString(R.string.select_date_error));
                    return;
                }
                break;
        }
    }

    private void setReqNowFalse() {
        reqNow = false;
        tvRequestNow.setBackgroundResource(R.drawable.shape_button_email);
        tvRequestNow.setTextColor(getResourceColor(R.color.color_red_bg));
        tvRequestNow.setText(R.string.request_now);
        tvDeliveryDate.setText("");
        pickupTime = 0;
    }

    private void selectCurrentDateandTime() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time =&gt; " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat(ORDER_DETAILS_PICKUP_DATE_AND_TIME);
        String formattedDate = df.format(c.getTime());
        tvDeliveryDate.setText(formattedDate);
        pickupTime = c.getTimeInMillis() - 1000;
    }

    private void placeOrder() {
        if (cartData != null && (pickupTime != 0 || reqNow)) {
            String comment = etComment.getText().toString().trim();
            if (!isPreference) {
                laundryPrefs = 0;
            }

            if (reqNow == false && TextUtils.isEmpty(deliveryDate)) {
                showToast(getString(R.string.please_select_time_first));
                return;
            }


            HttpParamObject httpParamObject = ApiRequestGenerator.placeOrder(gsId, cartData.getStaysId(), cartData.getHotelId(),
                    cartData.getUserId(), cartData.getCartType(), pickupTime, reqNow, comment, laundryPrefs, deliveryDate);
            executeTask(AppConstants.TASK_CODES.PLACE_ORDER, httpParamObject);
        } else {
            showToast(getString(R.string.place_order_error));
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.PLACE_ORDER:
                CartUpdateReceiver.sendBroadcast(this, CartUpdateReceiver.ACTION_UPDATE_REQUEST);
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false) {
                    successDialog();
                    RoomServiceCartResponse.clear(this);
                    HouseKeepingCartResponse.clear(this);
                    LaundryCartResponse.clear(this);
                } else {
//                    showToast(apiResponse.getMessage());
                    errorDialog(apiResponse.getMessage());
                }
                break;

            case AppConstants.TASK_CODES.ADD_REMOVE_FOOD_TO_CART:
                BaseApiResponse rsResponse = (BaseApiResponse) response;
                if (rsResponse != null && rsResponse.getError() == false) {
                    getAllCartItem();
                    if (customExpandableListAdapter != null) {
                        customExpandableListAdapter.notifyDataSetChanged();
                    }
                } else {
                    showToast(rsResponse.getMessage());
                }
                StayCartUpdateReceiver.callOnClear(this);
                break;

            case AppConstants.TASK_CODES.ADD_REMOVE_HOUSEKEEPING_TO_CART:
                BaseApiResponse hkResponse = (BaseApiResponse) response;
                if (hkResponse != null && hkResponse.getError() == false) {
                    getAllCartItem();
                    if (customExpandableListAdapter != null) {
                        customExpandableListAdapter.notifyDataSetChanged();
                    }
                } else {
                    showToast(hkResponse.getMessage());
                }
                StayCartUpdateReceiver.callOnClear(this);
                break;

            case AppConstants.TASK_CODES.ADD_REMOVE_LAUNDRY_TO_CART:
                BaseApiResponse lResponse = (BaseApiResponse) response;
                if (lResponse != null && lResponse.getError() == false) {
                    getAllCartItem();
                    if (customExpandableListAdapter != null) {
                        customExpandableListAdapter.notifyDataSetChanged();
                    }
                } else {
                    showToast(lResponse.getMessage());
                }
                StayCartUpdateReceiver.callOnClear(this);
                break;
            case AppConstants.TASK_CODES.ALL_CART_ITEMS:
                switch (type) {
                    case AppConstants.STAYS_TYPE.HOUSE_KEEPING:
                        HouseKeepingCartResponse houseKeepingCartResponse = (HouseKeepingCartResponse) response;
                        HouseKeepingCartResponseData cartResponseData = houseKeepingCartResponse.getData();
                        if (cartResponseData != null) {
                            cartData = CartData.getCartData(cartResponseData);
                        }
                        break;
                    case AppConstants.STAYS_TYPE.LAUNDRY:
                        LaundryCartResponse laundryCartResponse = (LaundryCartResponse) response;
                        LaundryCartItemData laundryCartItemData = laundryCartResponse.getData();
                        if (laundryCartItemData != null) {
                            cartData = CartData.getCartData(laundryCartItemData);
                        }
                        break;
                    case AppConstants.STAYS_TYPE.ROOM_SERVICE:
                        RoomServiceCartResponse roomServiceCartResponse = (RoomServiceCartResponse) response;
                        GetCartData getCartData = roomServiceCartResponse.getData();
                        if (getCartData != null) {
                            cartData = CartData.getCartData(getCartData);
                        }
                        break;
                }
                if (cartData != null) {
                    setData(cartData);
                }
                break;
        }
    }

    private void errorDialog(String message) {
        if (fromOrder == AppConstants.FROM_ORDERING.ROOM_SERVICE) {
            title = "ROOM SERVICE";
        } else if (fromOrder == AppConstants.FROM_ORDERING.HOUSEKEEPING) {
            title = "HOUSEKEEPING";
        } else if (fromOrder == AppConstants.FROM_ORDERING.LAUNDRY) {
            title = "LAUNDRY";
        } else {
            title = "";
        }
        SuccessDialogFragment.show(getSupportFragmentManager(),
                title,
                getString(R.string.order_error),
                message,
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                    }
                }
        );
    }

    private void successDialog() {

        if (fromOrder == AppConstants.FROM_ORDERING.ROOM_SERVICE) {
            title = "ROOM SERVICE";
        } else if (fromOrder == AppConstants.FROM_ORDERING.HOUSEKEEPING) {
            title = "HOUSEKEEPING";
        } else if (fromOrder == AppConstants.FROM_ORDERING.LAUNDRY) {
            title = "LAUNDRY";
        } else {
            title = "";
        }
        SuccessDialogFragment.show(getSupportFragmentManager(),
                title,
                getString(R.string.order_placed),
                getString(R.string.order_placed_success),
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                        setResult(RESULT_OK);
                        finish();
                    }
                }
        );
    }

    private void setDateandTime() {

        DatePickerUtil datePickerUtil = new DatePickerUtil(ORDER_DETAILS_DATE_YEAR_FORMAT, calendarInstance, getFragmentManager(), getString(R.string.select_date), true, false, new DatePickerUtil.OnDateSelected() {
            @Override
            public void onDateSelected(Calendar calendar, String date) {
                TimePickerUtil timePickerUtil = new TimePickerUtil(ORDER_DETAILS_PICKUP_DATE_AND_TIME, calendar, getFragmentManager(), getString(R.string.select_time), new TimePickerUtil.OnTimeSelected() {
                    @Override
                    public void onTimeSelected(Calendar calendar, String date) {
                        if (validation(calendar)) {
                            calendarInstance = calendar;
                            pickupTime = calendar.getTimeInMillis() - 1000;
                            tvDeliveryDate.setText(date);
                            Date time = calendar.getTime();
                            deliveryDate = Util.format(time, Util.DELIVERY_TIME_DATE_PATTERN);
                        }
                    }
                });
//                Calendar instance = Calendar.getInstance();
//                Timepoint timepoint = new Timepoint(instance.get(Calendar.HOUR_OF_DAY), instance.get(Calendar.MINUTE), instance.get(Calendar.SECOND));
//                timePickerUtil.show(timepoint);
                timePickerUtil.show();
            }

            @Override
            public void onCanceled() {
//                tvDeliveryDate.setText("");
            }
        });
        datePickerUtil.show();
    }

    private boolean validation(Calendar calendar) {
        if (calendar.getTimeInMillis() < System.currentTimeMillis() - 10000) {
            showToast("Please choose a valid time");
            tvDeliveryDate.setText("");
            return false;
        }
        return true;
    }

    private void setData(CartData cartData) {
        List<OrderDetail> detailList = cartData.getDetailList();
        filterList(detailList);
        totalQuantity = 0;
        totalPrice = 0;
        for (OrderDetail orderDetails : detailList) {
            totalPrice += orderDetails.getPrice() * orderDetails.getQuantity();
            totalQuantity += orderDetails.getQuantity();
        }
        if (fromOrder != AppConstants.FROM_ORDERING.HOUSEKEEPING) {
            tvTotalPrice.setText("\u20B9 " + totalPrice);
        }
        tvTotalCount.setText(totalQuantity + "");
    }

    private void filterList(List<OrderDetail> detailList) {
        if (CollectionUtils.isEmpty(detailList)) {
            hideVisibility(R.id.btn_place_order);
        }

        LinkedHashMap<String, List<OrderDetail>> listMap = new LinkedHashMap<>();
        for (OrderDetail foodItemOrderDetailList : detailList) {
            String type = foodItemOrderDetailList.getTypeName();
            if (listMap.containsKey(type)) {
                List<OrderDetail> list = listMap.get(type);
                list.add(foodItemOrderDetailList);
            } else {
                List<OrderDetail> list = new ArrayList<>();
                list.add(foodItemOrderDetailList);
                listMap.put(type, list);
            }
        }
        setExpandableList(listMap);
    }


    private void setExpandableList(final LinkedHashMap<String, List<OrderDetail>> listMap) {
        Iterator<String> iterator = listMap.keySet().iterator();
        final List<String> keys = new ArrayList<>();
        while (iterator.hasNext()) {
            keys.add(iterator.next());
        }
        customExpandableListAdapter = new CustomExpandableListAdapter(this, R.layout.row_header_expandable, R.layout.row_order_detail, listMap, new CustomExpandableListAdapter.ExpandableListener() {
            @Override
            public int getChildSize(int parentPosition) {
                return listMap.get(keys.get(parentPosition)).size();
            }

            @Override
            public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
                convertView = inflater.inflate(resourceId, parent, false);
                setText(keys.get(groupPosition), R.id.tv_men_women, convertView);
                ImageView imageView = (ImageView) convertView.findViewById(R.id.iv_indicator);
                if (isExpanded) {
                    imageView.setImageResource(R.mipmap.up_arrow_white);
                } else {
                    imageView.setImageResource(R.mipmap.arrow_down_white);
                }
                imageView.setColorFilter(Color.WHITE);
                return convertView;
            }

            @Override
            public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
                Holder holder = null;
                if (convertView == null) {
                    convertView = inflater.inflate(resourceId, parent, false);
                    holder = new Holder(convertView);
                    convertView.setTag(holder);
                } else {
                    holder = (Holder) convertView.getTag();
                }

                final OrderDetail ob = listMap.get(keys.get(groupPosition)).get(childPosition);
                holder.tvItemName.setText(ob.getItemName());
                holder.tvItemCount.setText(ob.getQuantity() + "");
                double price = ob.getPrice() * ob.getQuantity();
                String decimal = Util.formatDecimal(price, "0.00#");
                if (fromOrder != AppConstants.FROM_ORDERING.HOUSEKEEPING) {
                    holder.tvItemPrice.setText("\u20B9 " + decimal);
                } else {
                    holder.tvItemPrice.setText("");
                }
                holder.tvMinus.setVisibility(View.VISIBLE);
                holder.tvPlus.setVisibility(View.VISIBLE);

                holder.tvMinus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int quantity = ob.getQuantity();
                        if (quantity > 0) {
                            quantity = quantity - 1;
                        }

                        if (fromOrder == AppConstants.FROM_ORDERING.ROOM_SERVICE) {
                            fillFoodData(ob, quantity);

                        } else if (fromOrder == AppConstants.FROM_ORDERING.HOUSEKEEPING) {
                            fillHousekeepingData(ob, quantity);

                        } else if (fromOrder == AppConstants.FROM_ORDERING.LAUNDRY) {
                            fillLaundryData(ob, quantity);
                        }
                    }
                });

                holder.tvPlus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int quantity = ob.getQuantity();
                        quantity = quantity + 1;

                        if (fromOrder == AppConstants.FROM_ORDERING.ROOM_SERVICE) {
                            fillFoodData(ob, quantity);

                        } else if (fromOrder == AppConstants.FROM_ORDERING.HOUSEKEEPING) {
                            fillHousekeepingData(ob, quantity);

                        } else if (fromOrder == AppConstants.FROM_ORDERING.LAUNDRY) {
                            fillLaundryData(ob, quantity);
                        }
                    }
                });


//                convertView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Bundle bundle = new Bundle();
//                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.CART_DATA, cartData);
//                        bundle.putString(AppConstants.BUNDLE_KEYS.STAYS_ID, staysId);
//                        bundle.putString(AppConstants.BUNDLE_KEYS.GS_ID, gsId);
//                        bundle.putString(AppConstants.BUNDLE_KEYS.FOOD_ITEM_ID, );
//                    }
//                });

                return convertView;
            }
        });
        expandableListView.setAdapter(customExpandableListAdapter);
        for (int x = 0; x < keys.size(); x++) {
            expandableListView.expandGroup(x);
        }
    }

    private void fillLaundryData(OrderDetail ob, int quantity) {
        boolean sameDay = ob.isSameDay();
        LaundryItemData laundryItemData = new LaundryItemData();
        laundryItemData.setQuqntity(quantity);
        laundryItemData.setName(ob.getItemName());
        laundryItemData.setType(ob.getType());
        laundryItemData.setItemId(ob.getItemId());
        laundryItemData.setPrice(ob.getPrice());
        LaundryCartItemData.refreshItem(laundryItemData, sameDay, OrderDetailActivity.this);
        addLaundryItemToCart(laundryItemData, sameDay);
    }

    private void addLaundryItemToCart(LaundryItemData itemData, boolean sameDay) {
        List<LaundryServiceCartData> serviceCartDataList = new ArrayList<>();
        laundryCart = new LaundryCart();
        laundryCart.setUserId(userId);
        laundryCart.setHotelId(hotelId);
        laundryCart.setStaysId(staysId);

        LaundryServiceCartData cartData = new LaundryServiceCartData();
        cartData.setCartType(3);
        cartData.setCartOperation(1);

        ItemOrderDetails itemOrderDetails = new ItemOrderDetails();
        itemOrderDetails.setLaundryItemId(itemData.getItemId());
        itemOrderDetails.setType(itemData.getType());
//            if (fromSameDay && itemData.getSameDayPrice() > 0) {
//                itemOrderDetails.setPrice(itemData.getSameDayPrice());
//                itemOrderDetails.setSameDaySelected(true);
//            } else {
        itemOrderDetails.setPrice(itemData.getPrice());
        itemOrderDetails.setSameDaySelected(sameDay);
//            }
        itemOrderDetails.setQuantity(itemData.getQuqntity());

        cartData.setItemOrderDetails(itemOrderDetails);
        serviceCartDataList.add(cartData);
        laundryCart.setData(serviceCartDataList);
        if (laundryCart != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.addOrRemoveLaundryItem(laundryCart);
            executeTask(AppConstants.TASK_CODES.ADD_REMOVE_LAUNDRY_TO_CART, httpParamObject);
        }
    }

    private void fillHousekeepingData(OrderDetail ob, int quantity) {
        HouseKeepingItemData houseKeepingItemData = new HouseKeepingItemData();
        houseKeepingItemData.setItemCount(quantity);
        houseKeepingItemData.setName(ob.getItemName());
        houseKeepingItemData.setType(ob.getType());
        houseKeepingItemData.setItemId(ob.getItemId());
        houseKeepingItemData.setPrice(ob.getPrice());
        HouseKeepingCartResponseData.refreshItem(houseKeepingItemData, OrderDetailActivity.this);
        addHousekeepingItemToCart(houseKeepingItemData);
    }

    private void addHousekeepingItemToCart(HouseKeepingItemData itemData) {
        List<HouseKeepingCartData> cartDataList = new ArrayList<>();
        houseKeepingCart = new HouseKeepingCart();
        houseKeepingCart.setUserId(userId);
        houseKeepingCart.setHotelId(hotelId);
        houseKeepingCart.setStaysId(staysId);

        HouseKeepingCartData cartData = new HouseKeepingCartData();
        cartData.setCartType(2);
        cartData.setCartOperation(1);

        HkItemOrderDetails itemOrderDetails = new HkItemOrderDetails();
        itemOrderDetails.setHkItemId(itemData.getItemId());
        itemOrderDetails.setType(itemData.getType());
        itemOrderDetails.setPrice(itemData.getPrice());
        itemOrderDetails.setQuantity(itemData.getItemCount());

        cartData.setHkItemOrderDetails(itemOrderDetails);
        cartDataList.add(cartData);
        houseKeepingCart.setData(cartDataList);

        if (houseKeepingCart != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.addOrRemoveHouseKeepingItem(houseKeepingCart);
            executeTask(AppConstants.TASK_CODES.ADD_REMOVE_HOUSEKEEPING_TO_CART, httpParamObject);
        }
    }

    private void fillFoodData(OrderDetail ob, int quantity) {
        FoodDataByType foodDataByType = new FoodDataByType();
        foodDataByType.setType(ob.getType());
        foodDataByType.setFooditemId(ob.getItemId());
        foodDataByType.setName(ob.getItemName());
        foodDataByType.setPrice(ob.getPrice());
        foodDataByType.setItemCount(quantity);
        GetCartData.refreshItem(foodDataByType, OrderDetailActivity.this);
        addFoodItemToCart(foodDataByType);
    }

    private void addFoodItemToCart(FoodDataByType foodDataByType) {
        List<RoomServiceCartData> roomServiceCartDataList = new ArrayList<>();
        int itemCount = foodDataByType.getItemCount();

        roomServiceCart = new RoomServiceCart();
        roomServiceCart.setUserId(userId);
        roomServiceCart.setStaysId(staysId);
        roomServiceCart.setHotelId(hotelId);

        RoomServiceCartData roomServiceCartData = new RoomServiceCartData();
        roomServiceCartData.setCartType(1);
        roomServiceCartData.setCartOperation(1);

        FoodItemOrderDetails data = new FoodItemOrderDetails();
        data.setQuantity(itemCount);
        data.setType(foodDataByType.getType());
        data.setFoodItemId(foodDataByType.getFooditemId());
        data.setPrice(foodDataByType.getPrice());

        roomServiceCartData.setFoodItemOrderDetails(data);
        roomServiceCartDataList.add(roomServiceCartData);
        roomServiceCart.setData(roomServiceCartDataList);

        if (roomServiceCart != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.addOrRemoveFoodItem(roomServiceCart);
            executeTask(AppConstants.TASK_CODES.ADD_REMOVE_FOOD_TO_CART, httpParamObject);
        }
    }

    class Holder {
        TextView tvItemName, tvItemCount, tvItemPrice, tvMinus, tvPlus;

        public Holder(View view) {
            tvItemName = (TextView) view.findViewById(R.id.tv_item);
            tvItemCount = (TextView) view.findViewById(R.id.tv_item_count);
            tvMinus = (TextView) view.findViewById(R.id.tv_minus);
            tvPlus = (TextView) view.findViewById(R.id.tv_plus);
            tvItemPrice = (TextView) view.findViewById(R.id.tv_item_price);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_OK);
        finish();
    }
}
