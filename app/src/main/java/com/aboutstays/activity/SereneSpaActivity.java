package com.aboutstays.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.ImageContainerFragment;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.reservation.ReservationByTypeData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

public class SereneSpaActivity extends BaseActivity implements CustomPagerAdapter.PagerAdapterInterface, ViewPager.OnPageChangeListener {
    private String title;
    private int type;
    private String hsId;
    private ImageView ivMainImage;
    private List<String> additionalImageUrls;
    private String id;
    private TextView tvTitle, tvopnHour, tvSubTitle, tvRestaurantType, tvDescription, tvEntryCriteriafix, tvEntryCriteria,
            tvTimeText, tvTime, tvOpeningTimeFix, tvMostPopular, tvAminityType, tvAddDirection, tvOffer;
    private View view1, view2;
    private LinearLayout layMostPopular, layOffer;
    private GeneralServiceModel generalServiceModel;
    private Button btnReserve;
    private LinearLayout llSubImages, llGetDirection;
    private Boolean leaf;
    private String name;
    private ReservationByTypeData reservationData;
    private boolean fromHotel;
    private ViewPager viewPager;
    private CustomPagerAdapter pagerAdapter;
    private List<ImageContainerFragment> listImageContainerFragment = new ArrayList<>();
    private ImageView[] dots;
    private LinearLayout pager_indicator;
    private int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_name);

        findViews();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fromHotel = bundle.getBoolean(AppConstants.BUNDLE_KEYS.FROM_HOTEL);
            reservationData = (ReservationByTypeData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.RESERVATION_SUB_TYPES);
            if (reservationData != null) {
                type = reservationData.getReservationType();
                title = reservationData.getReservationCategoryName();
                name = reservationData.getName();
//                leaf = reservationData.getLeaf();
            }
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null) {
                hsId = generalServiceModel.getGeneralServiceData().getHsId();
            }
        }

        if (!TextUtils.isEmpty(name)) {
            initToolBar(name);
        } else {
            initToolBar("");
        }

        if (fromHotel) {
            hideVisibility(R.id.rl_button, R.id.tv_add_direction);
        } else {
            showVisibility(R.id.rl_button, R.id.tv_add_direction);
        }
        setOnClickListener(R.id.btn_reserve, R.id.tv_add_direction);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setData(reservationData);
    }

    private void findViews() {
        //ivMainImage = (ImageView) findViewById(R.id.iv_hotel_room);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvSubTitle = (TextView) findViewById(R.id.tv_subTitle);
        tvopnHour = (TextView) findViewById(R.id.tv_opn_hour);
        tvAminityType = (TextView) findViewById(R.id.tv_aminity_type);
        tvRestaurantType = (TextView) findViewById(R.id.tv_restaurant_type);
        tvDescription = (TextView) findViewById(R.id.tv_description);
        tvEntryCriteria = (TextView) findViewById(R.id.tv_entry_criteria);
        tvEntryCriteriafix = (TextView) findViewById(R.id.tv_entry_criteria_fix);
        tvAddDirection = (TextView) findViewById(R.id.tv_add_direction);
        tvTimeText = (TextView) findViewById(R.id.tv_time_text);
        tvTime = (TextView) findViewById(R.id.tv_time);
        layMostPopular = (LinearLayout) findViewById(R.id.lay_most_popular);
        layOffer = (LinearLayout) findViewById(R.id.lay_offering);
        btnReserve = (Button) findViewById(R.id.btn_reserve);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);
        //llSubImages = (LinearLayout) findViewById(R.id.ll_subImages);
        //llGetDirection = (LinearLayout) findViewById(R.id.ll_get_direction);
        tvOpeningTimeFix = (TextView) findViewById(R.id.tv_opening_time);
        tvMostPopular = (TextView) findViewById(R.id.tv_most_popular);
        view1 = findViewById(R.id.view1);
        view2 = findViewById(R.id.view2);
        viewPager = (ViewPager) findViewById(R.id.vp_image);

    }

    private void setData(ReservationByTypeData reservationData) {
        List<String> headerImageList = new ArrayList<>();
        tvTitle.setText(reservationData.getName());
        String imageUrl = reservationData.getImageUrl();

        if (!TextUtils.isEmpty(imageUrl)) {
            headerImageList.add(imageUrl);
        }
        if (CollectionUtils.isNotEmpty(reservationData.getAdditionalImageUrls())) {
            headerImageList.addAll(reservationData.getAdditionalImageUrls());
        }

        if (CollectionUtils.isNotEmpty(headerImageList)) {
            for (String amenityImage : headerImageList) {
                ImageContainerFragment imageContainerFragment = ImageContainerFragment.getInstance(amenityImage);
                listImageContainerFragment.add(imageContainerFragment);
            }
            pagerAdapter = new CustomPagerAdapter(getSupportFragmentManager(), listImageContainerFragment, SereneSpaActivity.this);
            viewPager.setAdapter(pagerAdapter);
            viewPager.setCurrentItem(0);
            viewPager.setOnPageChangeListener(this);
            setUiPageViewController();
        }

//        if (!TextUtils.isEmpty(imageUrl)) {
//            Picasso.with(this).load(imageUrl).into(ivMainImage);
//        }

        String openingTime = reservationData.getStartTime();
        String closingTime = reservationData.getEndTime();
        String startTime = Util.convertDateFormat(openingTime,
                Util.CHECK_IN_TIME_UI_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
        String endTime = Util.convertDateFormat(closingTime,
                Util.CHECK_IN_TIME_UI_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);


        tvopnHour.setText(startTime + " - " + endTime);
        if (reservationData.getPrimaryCuisine() != null)
        tvSubTitle.setText(reservationData.getPrimaryCuisine());
        tvDescription.setText(reservationData.getDescription());
        tvEntryCriteria.setText(reservationData.getEntryCriteria());

        if (TextUtils.isEmpty(reservationData.getAmenityType())) {
            hideVisibility(R.id.tv_aminity_type, R.id.tv_restaurant_type);
        } else {
            tvRestaurantType.setText(reservationData.getAmenityType());
        }
        if (TextUtils.isEmpty(reservationData.getEntryCriteria())) {
            hideVisibility(R.id.tv_entry_criteria, R.id.tv_entry_criteria_fix);
        } else {
            tvEntryCriteria.setText(reservationData.getEntryCriteria());
        }

        List<String> mostPopularList = reservationData.getMostPopular();
        if (!CollectionUtils.isEmpty(mostPopularList)) {
            Typeface tf = Typeface.createFromAsset(SereneSpaActivity.this.getAssets(), "fonts/" + getString(R.string.font_regular));
            for (int i = 0; i < mostPopularList.size(); i++) {
                LayoutInflater inflater = LayoutInflater.from(this);
                View view = inflater.inflate(R.layout.row_aminites, layMostPopular, false);
                TextView tvAminity = (TextView) view.findViewById(R.id.tv_aminity);
                tvAminity.setText(mostPopularList.get(i));
                tvAminity.setTypeface(tf);
                layMostPopular.addView(view);
            }
        } else {
            hideVisibility(R.id.tv_most_popular, R.id.view2);
        }

//        List<String> offerings = reservationData.getOfferings();
//        if (CollectionUtils.isNotEmpty(offerings)) {
//            Typeface tf = Typeface.createFromAsset(SereneSpaActivity.this.getAssets(), "fonts/" + getString(R.string.font_regular));
//            showVisibility(R.id.tv_offering, R.id.view3);
//            for (int i = 0; i < offerings.size(); i++) {
//                LayoutInflater inflater = LayoutInflater.from(this);
//                View view = inflater.inflate(R.layout.row_aminites, layOffer, false);
//                TextView tvAminity = (TextView) view.findViewById(R.id.tv_aminity);
//                tvAminity.setText(offerings.get(i));
//                tvAminity.setTypeface(tf);
//                layOffer.addView(view);
//            }
//        } else {
//            hideVisibility(R.id.tv_offering, R.id.view3);
//        }


//        additionalImageUrls = reservationData.getAdditionalImageUrls();
//        if (!CollectionUtils.isEmpty(additionalImageUrls)) {
//            int size = additionalImageUrls.size();
//            for (int i = 0; i < size; i++) {
//                addImagesToLayout(additionalImageUrls.get(i), false, 0);
//            }
//
////            if (size > 4) {
////                for (int i = 0; i < 3; i++) {
////                    addImagesToLayout(additionalImageUrls.get(i), false, 0);
////                }
////                addImagesToLayout(additionalImageUrls.get(3), true, size - 4);
////            } else {
////                for (int i = 0; i < size; i++) {
////                    addImagesToLayout(additionalImageUrls.get(i), false, 0);
////                }
////            }
//        } else {
//
//        }
    }

    private void setUiPageViewController() {
        count = pagerAdapter.getCount();
        if (count > 1) {
            dots = new ImageView[count];

            for (int i = 0; i < count; i++) {
                dots[i] = new ImageView(this);
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot_not));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                params.setMargins(4, 0, 4, 0);

                pager_indicator.addView(dots[i], params);
            }

            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
        }
    }


    private void addImagesToLayout(String s, boolean isMore, int moreSize) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.view_serene_spa_images, llSubImages, false);
        ImageView iv = (ImageView) view.findViewById(R.id.iv_hotel_room);
        RelativeLayout rlMoreImage = (RelativeLayout) view.findViewById(R.id.rl_image_shade);
        TextView tvMore = (TextView) view.findViewById(R.id.tv_more_number);
        if (isMore == true) {
            rlMoreImage.setVisibility(View.VISIBLE);
            tvMore.setText("+" + moreSize + " More");
        }
        Picasso.with(this).load(s).placeholder(R.drawable.progress_animation).into(iv);
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Bundle bundle = new Bundle();
//                bundle.putSerializable(AppConstants.BUNDLE_KEYS.SUB_IMAGES_LIST, (Serializable) additionalImageUrls);
//                FragmentContainerActivity.startActivity(SereneSpaActivity.this, AppConstants.FRAGMENT_TYPE.SUB_IMAGE_FRAGMENT, bundle);
//            }
//        });
        llSubImages.addView(view);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_reserve:
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.RESERVATION_TYPES, reservationData);
//                if (leaf == true) {
//                    FragmentContainerActivity.startActivityForResult(this, AppConstants.FRAGMENT_TYPE.RESERVATION_ORDER_FRAGMENT, bundle, AppConstants.REQUEST_CODE.ORDER_PLACED);
//                } else {
                    FragmentContainerActivity.startActivityForResult(this, AppConstants.FRAGMENT_TYPE.FRAGMENT_RESERVATION_PACKAGE, bundle, AppConstants.REQUEST_CODE.ORDER_PLACED);
//                }
                break;
            case R.id.tv_add_direction:
                FragmentContainerActivity.startActivityForResult(this, AppConstants.FRAGMENT_TYPE.FRAGMENT_GET_DIRECTION, null, AppConstants.REQUEST_CODE.GET_DIRECTION);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppConstants.RESULT_CODE.RESULT_DONE) {
            setResult(AppConstants.RESULT_CODE.RESULT_DONE);
            finish();
        }
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        return listImageContainerFragment.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return null;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < count; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot_not));
        }
        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_option_spa, menu);
//        return true;
//    }
}
