package com.aboutstays.activity;

import android.content.Intent;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.DrawerFragment;
import com.aboutstays.fragment.HotelsFragment;
import com.aboutstays.services.CommonDataServices;
import com.aboutstays.services.UserServices;

import java.util.ArrayList;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class DrawerActivity extends AppBaseActivity implements DrawerLayout.DrawerListener, DrawerFragment.ToolbarTitleListner {

    private FrameLayout container;
    public TextView tvTitle;
    private DrawerLayout drawerLayout;
    private boolean isDrawerOpen;
    private FragmentManager fragmentManager;
    private boolean fromStay = false;
    private boolean fromGuest, addStay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        initToolBar("HOTELS");
        UserServices.startUpdateUser(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fromStay = bundle.getBoolean(AppConstants.BUNDLE_KEYS.FROM_ADD_STAY);
            fromGuest = bundle.getBoolean(AppConstants.BUNDLE_KEYS.IF_GUEST);
            addStay = bundle.getBoolean(AppConstants.BUNDLE_KEYS.ADD_STAY);
        }

        if (fromStay) {
            initToolBar("STAYLINE");
        }

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_home);
        drawerLayout.addDrawerListener(this);
        fragmentManager = getSupportFragmentManager();

        HotelsFragment hotelsFragment = HotelsFragment.getInstance();
        addDrawerFragment(hotelsFragment);
        addFragment(hotelsFragment, false);

        //CommonDataServices.fetchCommonData(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        new Thread(new Runnable() {
            @Override
            public void run() {
                CommonDataServices.fetchUserCommonData(DrawerActivity.this);
            }
        }).start();

    }

    public void addFragment(Fragment fragment, boolean b) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        getSupportFragmentManager().popBackStack();
        if (b) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.replace(R.id.lay_fragment_container, fragment).commit();
        drawerLayout.closeDrawer(Gravity.LEFT);
    }


    private void addDrawerFragment(HotelsFragment hotelsFragment) {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        String[] val = getResources().getStringArray(R.array.drawer_list_item);
        ArrayList<String> arrayList = new ArrayList<>();
        for (String s : val) {
            arrayList.add(s);
        }

        DrawerFragment drawerFragment = DrawerFragment.getInstance(arrayList, this, drawerLayout, this, fromStay, fromGuest, addStay);
        drawerFragment.setGetHotelDataListener(hotelsFragment);
        fragmentManager.beginTransaction().replace(R.id.lay_drawer, drawerFragment).commit();
    }

    @Override
    public void onBackPressed() {
        if (isDrawerOpen) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            isDrawerOpen = false;
            return;
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
            return;
        }
        initToolBar("HOTELS");
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_option,menu);
        return true;
    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.ic_drawer_menu;
    }

    @Override
    protected void onHomePressed() {
        drawerLayout.openDrawer(Gravity.LEFT);
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {
        isDrawerOpen = true;
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        isDrawerOpen = false;
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    @Override
    public void sendTitle(String s) {
        initToolBar(s);
    }
}
