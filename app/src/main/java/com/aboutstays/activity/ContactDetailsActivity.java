package com.aboutstays.activity;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.aboutstays.R;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.model.contactUpdate.AddressHome;
import com.aboutstays.model.contactUpdate.AddressOffice;
import com.aboutstays.model.contactUpdate.ContactUpdate;
import com.aboutstays.model.contactUpdate.HomeAddress;
import com.aboutstays.model.contactUpdate.OfficeAddress;
import com.aboutstays.services.UserServices;
import com.aboutstays.utitlity.ApiRequestGenerator;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.pojo.Address;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;

/**
 * Created by ajay on 13/12/16.
 */
public class ContactDetailsActivity extends AppBaseActivity {
    private TextInputLayout til_email, til_mobile, tilHome_address, tilOffice_address;
    private String userId;
    private Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);
        initToolBar("Contact Details");

        til_email = (TextInputLayout) findViewById(R.id.til_email);
        til_mobile = (TextInputLayout) findViewById(R.id.til_mobile_no);
        tilHome_address = (TextInputLayout) findViewById(R.id.til_home_address);
        tilOffice_address = (TextInputLayout) findViewById(R.id.til_office_address);
        btnSave = (Button) findViewById(R.id.btn_save_changes);

        til_email.getEditText().setEnabled(false);
        til_mobile.getEditText().setEnabled(false);

        setSessionData();

        btnSave.setOnClickListener(this);

    }

    private void setSessionData() {
        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            userId = sessionInstance.getUserId();
            til_email.getEditText().setText(sessionInstance.getEmailId());
            til_mobile.getEditText().setText(sessionInstance.getNumber());
            if(sessionInstance.getOfficeAddress() != null && !TextUtils.isEmpty(sessionInstance.getOfficeAddress().getDescription())){
                tilOffice_address.getEditText().setText(sessionInstance.getOfficeAddress().getDescription());
            }
            if(sessionInstance.getHomeAddress() != null && !TextUtils.isEmpty(sessionInstance.getHomeAddress().getDescription())){
                tilHome_address.getEditText().setText(sessionInstance.getHomeAddress().getDescription());
            }
        }
    }

    private void updateContactData() {
        String email = til_email.getEditText().getText().toString().trim();
        String mobileNumber = til_mobile.getEditText().getText().toString().trim();
        String homeAdd = tilHome_address.getEditText().getText().toString().trim();
        String officeAdd = tilOffice_address.getEditText().getText().toString().trim();

        ContactUpdate contactUpdate = new ContactUpdate();
        contactUpdate.setHomeAddress(new Address());
        contactUpdate.getHomeAddress().setDescription(homeAdd);
        contactUpdate.setOfficeAddress(new Address());
        contactUpdate.getOfficeAddress().setDescription(officeAdd);
        contactUpdate.setEmail(email);
        contactUpdate.setMobileNumber(mobileNumber);
        contactUpdate.setUserId(userId);

        HttpParamObject httpParamObject = ApiRequestGenerator.updateContact(contactUpdate);
        executeTask(AppConstants.TASK_CODES.UPDATE_CONTACTS, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null){
            showToast(getString(R.string.serverr_error));
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.UPDATE_CONTACTS:
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                if (baseApiResponse != null && baseApiResponse.getError() == false){
                    UserServices.startUpdateUser(this);
                    successDialog();
//                    showToast("Contact details updated.");
//                    finish();
                } else {
                    errorDialog(baseApiResponse.getMessage());
//                    showToast(baseApiResponse.getMessage());
                }
                break;
        }
    }

    private void errorDialog(String message) {
        SuccessDialogFragment.show(getSupportFragmentManager(),
                "CONTACT DETAILS",
                "UPDATE ERROR",
                message,
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                    }
                }
        );
    }

    private void successDialog() {
        SuccessDialogFragment.show(getSupportFragmentManager(),
                "CONTACT DETAILS",
                "UPDATED",
                "Contact details update.",
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
//                        setResult(RESULT_OK);
                        finish();
                    }
                }
        );
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_save_changes:
                updateContactData();
        }
    }
}
