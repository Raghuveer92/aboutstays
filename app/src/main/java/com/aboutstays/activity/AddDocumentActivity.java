package com.aboutstays.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.enums.DocType;
import com.aboutstays.model.DeleteIdPojo;
import com.aboutstays.model.common.FetchIdentityDocsType;
import com.aboutstays.rest.response.ImageUploadResponse;
import com.aboutstays.services.UserServices;
import com.aboutstays.utils.FileUtil;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import simplifii.framework.asyncmanager.FileParamObject;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.MediaFragment;
import simplifii.framework.rest.response.pojo.IdentityDoc;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;


public class AddDocumentActivity extends AppBaseActivity implements AdapterView.OnItemSelectedListener {

    private EditText etDocType, etDocName, etDocNumber;
    private Button btnAddDocument;
    private TextView tvUploadButton;
    private MediaFragment imagePicker, audioPicker, videoPicker, pdfPicker;
    private ImageView iv;
    private Spinner spinnerDocType;
    private String path;
    private String userId;
    private File bitmapFile;
    private File file;
    private List<String> docsType = new ArrayList<>();
    private int documentType;
    private IdentityDoc identity;
    private List<String> docTitles = new ArrayList<>();
    private boolean permission;
    private boolean fromCheckin;
    private String docType, docNumber, docName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_document);

        initToolBar("Add Document");
        askPermissions();

        iv = (ImageView) findViewById(R.id.iv);
        btnAddDocument = (Button) findViewById(R.id.btn_add_document);
        tvUploadButton = (TextView) findViewById(R.id.tv_btn_upload);
//        etDocType = (EditText) findViewById(R.id.et_doc_type);
        spinnerDocType = (Spinner) findViewById(R.id.spineer_doc_type);
        etDocName = (EditText) findViewById(R.id.et_doc_name);
        etDocNumber = (EditText) findViewById(R.id.et_doc_num);

        //setSpinner();
        //getBundleData();

        UserSession userSession = UserSession.getSessionInstance();
        if (userSession!=null){
            userId = userSession.getUserId();
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            fromCheckin = bundle.getBoolean(AppConstants.BUNDLE_KEYS.FROM_CHECKIN);
        }

        imagePicker = new MediaFragment();
        getSupportFragmentManager().beginTransaction().add(imagePicker, "Profile image").commit();
        setOnClickListener(R.id.btn_add_document, R.id.tv_btn_upload);

//        pdfPicker = new MediaFragment();
//        getSupportFragmentManager().beginTransaction().add(pdfPicker, "Pdf Picker").commit();
    }

    private void getBundleData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            identity = (IdentityDoc) bundle.getSerializable(AppConstants.BUNDLE_KEYS.IDENTITY_DOC);
            if (identity!= null){
                btnAddDocument.setText("DELETE DOCUMENT");
                String docTypeName = identity.getDocType();
                if(docsType.contains(docTypeName)) {
                    int i = docsType.indexOf(docTypeName);
                    spinnerDocType.setSelection(i);
                    spinnerDocType.setEnabled(false);
                }
                initToolBar(docTypeName);
                tvUploadButton.setVisibility(View.GONE);
                etDocName.setText(identity.getNameOnDoc());
                etDocNumber.setText(identity.getDocNumber());
                etDocName.setFocusable(false);
                etDocNumber.setFocusable(false);
                String docUrl = identity.getDocUrl();
                if (docUrl != null){
                    new getBitmapFromImageUrl().execute(docUrl);
                    Picasso.with(this).load(docUrl).placeholder(R.drawable.progress_animation).into(iv);
                }
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getIdentityDocumentsType();
    }

    private void getIdentityDocumentsType() {
        Set<String> identityDocs = Preferences.getData(Preferences.KEY_IDENTITY_DOCS);
        if(CollectionUtils.isNotEmpty(identityDocs)) {
            docTitles.clear();
            docTitles.addAll(identityDocs);
            setSpinner();
        } else {
            HttpParamObject httpParamObject = ApiRequestGenerator.fetchIdentityDocTypes();
            executeTask(AppConstants.TASK_CODES.GET_IDENTITY_DOC_TYPES, httpParamObject);
        }
    }





    //    public static Bitmap getBitmapFromURL(String src) {
//        try {
//            URL url = new URL(src);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            InputStream input = connection.getInputStream();
//            Bitmap myBitmap = BitmapFactory.decodeStream(input);
//            return myBitmap;
//        } catch (IOException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }

    private class getBitmapFromImageUrl extends AsyncTask<String, Void, Bitmap>{

        @Override
        protected Bitmap doInBackground(String... params) {
            String imageUrl = params[0];
            try {
                URL url = new URL(imageUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap != null){
                getFilePathfromBitmap(bitmap);
            }
        }
    }


    private void setSpinner() {
        docsType.clear();
        docsType.add("--Select--");
        docsType.addAll(docTitles);
//        List<Integer> typeList = DocType.getTypeList();
//        int size = typeList.size();
//        for (int i=1 ; i <= size; i++) {
//            DocType docByType = DocType.findDocByType(i);
//            String docName = docByType.getDocName();
//            docsType.add(docName);
//        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.row_spinner_pesonal_detail, docsType);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDocType.setAdapter(dataAdapter);
        spinnerDocType.setOnItemSelectedListener(this);

        getBundleData();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_add_document:
                if (identity != null){
                    deleteDoc();
                } else {
                    uploadData();
                }
                break;
            case R.id.tv_btn_upload:
                if (permission) {
                    selectImageViaPicker();
                } else {
                    askPermissions();
                }
                break;
        }
    }

    private void deleteDoc() {
        DeleteIdPojo deleteIdPojo = new DeleteIdPojo();
        deleteIdPojo.setUserId(userId);
//        IdentityDoc identityDoc = new IdentityDoc();
//        identityDoc.setDocType(identity.getDocType());
//        identityDoc.setDocNumber(identity.getDocNumber());
//        identityDoc.setNameOnDoc(identity.getNameOnDoc());
//        identityDoc.setDocUrl(identity.getDocUrl());
        deleteIdPojo.setIdentityDoc(identity);
        HttpParamObject httpParamObject = ApiRequestGenerator.deleteDoc(deleteIdPojo);
        executeTask(AppConstants.TASK_CODES.DELETE_DOC, httpParamObject);
    }

    private void askPermissions() {
        new TedPermission(this)
                .setPermissions(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        permission = true;
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                        permission = false;
                    }
                }).check();
    }

    private void selectImageViaPicker() {
        imagePicker.getDoc(new MediaFragment.MediaListener() {

            @Override
            public void setUri(Uri uri, String MediaType) {
                if (uri != null) {
                    iv.setImageURI(uri);
                    String imagePathFromURI = getImagePathFromURI(uri);
                    getFile(imagePathFromURI);
                }
            }

            @Override
            public void setBitmap(Bitmap bitmap) {
                getFilePathfromBitmap(bitmap);
            }

            @Override
            public void onGetPdfUri(Uri uri, int MediaType, String path) {
                if (path != null) {
                    iv.setImageResource(R.mipmap.pdf);
                    AddDocumentActivity.this.path = path;
                }
            }
        });


    }

    private void getFilePathfromBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            bitmap = Util.getResizeBitmap(bitmap,1024);
            iv.setImageBitmap(bitmap);
            try {
                File aboutStays = Util.getFile(bitmap, "AboutStays");
                this.bitmapFile = aboutStays;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getFile(String path) {
        this.path = path;
    }

    private void uploadData() {
        if(spinnerDocType.getSelectedItemPosition()==0){
            showToast("Please select document type");
            return;
        }
        docType = spinnerDocType.getSelectedItem().toString();
        docName = etDocName.getText().toString().trim();
        docNumber = etDocNumber.getText().toString().trim();
        if (TextUtils.isEmpty(docName) && TextUtils.isEmpty(docType) && TextUtils.isEmpty(docNumber)) {
            showToast("Please fill all fields");
            return;
        }


        if (bitmapFile != null){
            this.file = bitmapFile;
        } else if (!TextUtils.isEmpty(path)) {
            file = new File(path);
        } else {
            showToast(getString(R.string.select_file_first));
        }
        uploadtoServer(docType, docName, docNumber);
//        showToast("success");
//        finish();
    }

    private void uploadtoServer(String docType, String docName, String docNumber) {
        if (file != null) {
            if (!file.exists()) {
                showToast(getString(R.string.select_file_first));
            }
            if (file.exists()) {
                String fileKey;
                if (!fromCheckin){
                    fileKey = "scannedCopy";
                } else {
                    fileKey = "file";
                }
                FileParamObject fileParamObject = new FileParamObject(file, file.getName(), fileKey);
                fileParamObject.setPostMethod();

                if (!fromCheckin) {
                    fileParamObject.setUrl(AppConstants.PAGE_URL.UPDATE_IDENTITY);
                    fileParamObject.addParameter("nameOnDoc", docName);
                    fileParamObject.addParameter("docNumber", docNumber);
                    fileParamObject.addParameter("docType", docType);
                    if (identity != null) {
                        fileParamObject.addParameter("overwrite", "true");
                    } else {
                        fileParamObject.addParameter("overwrite", "false");
                    }
                    fileParamObject.addParameter("userId", userId);
                    fileParamObject.setClassType(ImageUploadResponse.class);
//                    fileParamObject.setContentType("");
//                    executeTask(AppConstants.TASK_CODES.UPLOAD_FILE, fileParamObject);
                } else {
                    fileParamObject.setUrl(AppConstants.PAGE_URL.UPDATE_IDENTITY_IMAGE);
                    fileParamObject.setClassType(ImageUploadResponse.class);
//                    fileParamObject.setContentType("");
//                    executeTask(AppConstants.TASK_CODES.UPLOAD_IMAGE, fileParamObject);
                }
                fileParamObject.setContentType("");
                executeTask(AppConstants.TASK_CODES.UPLOAD_FILE, fileParamObject);
            }
        }
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_IDENTITY_DOC_TYPES :
                FetchIdentityDocsType fetchIdentityDocsType = (FetchIdentityDocsType) response;
                 if(fetchIdentityDocsType != null && !fetchIdentityDocsType.getError()) {
                     if(CollectionUtils.isNotEmpty(fetchIdentityDocsType.getIdentityDocTypes())) {
                         docTitles.clear();
                         docTitles.addAll(fetchIdentityDocsType.getIdentityDocTypes());
                         setSpinner();
                     }
                 }
                 break;
            case AppConstants.TASK_CODES.UPLOAD_FILE:
                ImageUploadResponse baseApiResponse = (ImageUploadResponse) response;
                if (baseApiResponse != null && baseApiResponse.getError() == false){
                    UserServices.startUpdateUser(this);
                    if (baseApiResponse.getData()!= null){
                        String data = baseApiResponse.getData();
                        if (!TextUtils.isEmpty(data)){
                            IdentityDoc identityDoc = new IdentityDoc();
                            identityDoc.setDocUrl(data);
                            identityDoc.setDocNumber(docNumber);
                            identityDoc.setDocType(docType);
                            identityDoc.setNameOnDoc(docName);

                            Intent intent = new Intent();
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, identityDoc);
                            intent.putExtras(bundle);
                            setResult(RESULT_OK, intent);
                        } else {
                            setResult(RESULT_OK);
                        }
                    }
                    finish();
                } else {
                    showToast(baseApiResponse.getMessage());
                }
                break;

            case AppConstants.TASK_CODES.DELETE_DOC:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false){
                    UserServices.startUpdateUser(this);
                    setResult(RESULT_OK);
                    finish();
                } else {
                    showToast(apiResponse.getMessage());
                }
                break;
         }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getPDFPathFromURI(Uri contentUri) {
        String filePath;
        if (contentUri != null && "content".equals(contentUri.getScheme())) {
            Cursor cursor = this.getContentResolver().query(contentUri, new String[]{MediaStore.Images.ImageColumns.DATA}, null, null, null);
            cursor.moveToFirst();
            filePath = cursor.getString(0);
            cursor.close();
        } else {
            filePath = contentUri.getPath();
        }
        return filePath;
    }

    public String getImagePathFromURI(Uri contentUri) {
        if (true) {
            return FileUtil.getPath(this, contentUri);
        }
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = this.managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        documentType = parent.getSelectedItemPosition() + 1;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
