package com.aboutstays.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.MultipleBookingModel;
import com.aboutstays.model.booking.BookingData;
import com.aboutstays.model.booking.ListBooking;
import com.aboutstays.model.stayline.HotelBasicInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class MultipleBookingSelectionActivity extends AppBaseActivity implements CustomListAdapterInterface {
    List<ListBooking> bookingList = new ArrayList<>();
    ListView listViewBoking;
    private CustomListAdapter customListAdapter;
    private BookingData bookingData;
    private HotelBasicInfo hotelBasicInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_booking_selection);
        initToolBar(getString(R.string.multiple_booking_selection));
        listViewBoking = (ListView) findViewById(R.id.lv_multiple_boooking);
        customListAdapter = new CustomListAdapter(this, R.layout.row_multiple_booking, bookingList, this);
        listViewBoking.setAdapter(customListAdapter);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            hotelBasicInfo = (HotelBasicInfo) bundle.getSerializable(AppConstants.BUNDLE_KEYS.HOTEL_DATA);
            bookingData = (BookingData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.BOOKING_DATA);
            if (bookingData != null) {
                List<ListBooking> listBookings = bookingData.getListBookings();
                bookingList.addAll(listBookings);
            }
            customListAdapter.notifyDataSetChanged();
        }

    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.left_arrow;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final ListBooking listBooking = bookingList.get(position);
        if (listBooking != null) {
            boolean addedToStayline = listBooking.isAddedToStayline();
            if (addedToStayline) {
                holder.rlMain.setBackgroundResource(R.drawable.shape_background_multiple_selected);
                convertView.setEnabled(false);
            } else {
                holder.rlMain.setBackgroundResource(R.drawable.shape_background_multiple);
                convertView.setEnabled(true);
            }
            holder.checkInDate.setText(listBooking.getCheckInDate());
            holder.checkOutDate.setText(listBooking.getCheckOutDate());
            if (listBooking.getBookedForUser() != null) {
                holder.guestName.setText(listBooking.getBookedForUser().getName());
                holder.pax.setText(listBooking.getBookedForUser().getPax());
            }
            if (listBooking.getRoomPojo() != null) {
                holder.roomType.setText(listBooking.getRoomPojo().getName());
            }
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putSerializable(AppConstants.BUNDLE_KEYS.HOTEL_DATA,hotelBasicInfo);
                b.putSerializable(AppConstants.BUNDLE_KEYS.BOOKING_LIST, listBooking);
                startNextActivity(b, AddStayActivity.class);
            }
        });
        return convertView;

    }


    class Holder {
        TextView guestName, checkInDate, checkOutDate, roomType, pax;
        private RelativeLayout rlMain;

        public Holder(View view) {
            guestName = (TextView) view.findViewById(R.id.text_guest_name_multiple);
            checkInDate = (TextView) view.findViewById(R.id.tv_date_checkin);
            checkOutDate = (TextView) view.findViewById(R.id.tv_date_checkout);
            roomType = (TextView) view.findViewById(R.id.tv_room_type);
            pax = (TextView) view.findViewById(R.id.tv_pax);
            rlMain = (RelativeLayout) view.findViewById(R.id.rl_main);
        }
    }


}
