package com.aboutstays.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.rest.response.ExpenseItem;
import com.aboutstays.rest.response.TimeBasedExpenseItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class ExpenseDetailsActivity extends AppBaseActivity implements CustomListAdapterInterface {


    private CustomListAdapter listAdapter;
    private List<ExpenseItem> listSubExpenseItem = new ArrayList<>();
    private ListView expenceDetailListView;
    private TimeBasedExpenseItem timeBasedExpenseItem;
    private TextView tvRequestDate;
    private TextView tvExpenseTypeName;
    private ImageView ivExpenseTypeIcon;
    private TextView tvEmptyView;
    private String totalAmount;

    private static final String EXPENSE_DETAIL_TIME_FORMAT = "hh:mm a, dd MMM yyyy";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense_details);
        initToolBar("Expense Details");

        tvRequestDate = (TextView) findViewById(R.id.tv_requested_date);
        ivExpenseTypeIcon = (ImageView) findViewById(R.id.iv_expence_image);
        tvExpenseTypeName = (TextView) findViewById(R.id.tv_expence_name);
        tvEmptyView = (TextView) findViewById(R.id.tv_empty_view);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            timeBasedExpenseItem = (TimeBasedExpenseItem) bundle.getSerializable(AppConstants.BUNDLE_KEYS.TIME_BASED_EXPENSE_ITEM);
            if(timeBasedExpenseItem != null){
                totalAmount = Util.formatDecimal(timeBasedExpenseItem.getTotal(), "0.00#");
//                totalAmount = timeBasedExpenseItem.getTotal();
                tvExpenseTypeName.setText(timeBasedExpenseItem.getName());
                if(!TextUtils.isEmpty(timeBasedExpenseItem.getIconUrl())){
                    Picasso.with(this).load(timeBasedExpenseItem.getIconUrl())
                            .into(ivExpenseTypeIcon);
                }
                listSubExpenseItem.addAll(timeBasedExpenseItem.getSubExpenseItemList());
                Long time = timeBasedExpenseItem.getCreated();
                if(time != 0){
                  tvRequestDate.setText(Util.getDate(time, EXPENSE_DETAIL_TIME_FORMAT));
                }
            }
        }

        expenceDetailListView = (ListView) findViewById(R.id.ll_expense_detail);
        listAdapter = new CustomListAdapter(this, R.layout.row_expencse_detail, listSubExpenseItem, this);
        expenceDetailListView.setAdapter(listAdapter);
        expenceDetailListView.setEmptyView(tvEmptyView);
        setExpenseDetailListViewFooter();
        listAdapter.notifyDataSetChanged();
    }

    private void setExpenseDetailListViewFooter() {
        LayoutInflater inflater = getLayoutInflater();
        View inflate = inflater.inflate(R.layout.row_expencse_detail, expenceDetailListView, false);

        TextView tvTotal = (TextView) inflate.findViewById(R.id.tv_title);
        TextView tvTotalAmount = (TextView) inflate.findViewById(R.id.tv_amount);
        tvTotal.setText("Total");
        tvTotalAmount.setText("\u20B9 " +totalAmount);

        expenceDetailListView.addFooterView(inflate);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ExpenseDetailHolder expenseDetailHolder = null;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.row_expencse_detail,parent,false);
            expenseDetailHolder = new ExpenseDetailHolder(convertView);
            convertView.setTag(expenseDetailHolder);
        } else{
            expenseDetailHolder = (ExpenseDetailHolder) convertView.getTag();
        }

        ExpenseItem expenseItem = listSubExpenseItem.get(position);

        expenseDetailHolder.tvExpenseName.setText(expenseItem.getName());
        String amount = Util.formatDecimal(expenseItem.getTotal(), "0.00#");
        expenseDetailHolder.tvAmount.setText("\u20B9 " + amount);
        return convertView;
    }

    private class ExpenseDetailHolder{
        TextView tvExpenseName,tvAmount;

        public ExpenseDetailHolder(View view){
            tvExpenseName = (TextView) view.findViewById(R.id.tv_title);
            tvAmount= (TextView) view.findViewById(R.id.tv_amount);
        }
    }

}
