package com.aboutstays.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.utitlity.ApiRequestGenerator;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class SignUpActivity extends AppBaseActivity {
    private TextInputLayout tilEmail, tilFirstName, tilLastName, tilNumber, tilPassword, tilConfirmPassword;
    private Button btnGenerateOtp;
    private TextView tvPasswordStrength, tvError;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initCroutonView();
        findViews();
        initToolBar("Sign Up");
        setOnClickListener(R.id.btn_generate_otp);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_generate_otp:
                validateAndSubmit();
                break;
        }
    }

    private void findViews() {
        tvPasswordStrength = (TextView) findViewById(R.id.tv_password_strength);
        tvError = (TextView) findViewById(R.id.tv_error);
        tilEmail = (TextInputLayout) findViewById(R.id.til_email);
        tilFirstName = (TextInputLayout) findViewById(R.id.til_fname);
        tilLastName = (TextInputLayout) findViewById(R.id.til_lname);
        tilNumber = (TextInputLayout) findViewById(R.id.til_mobile);
        tilPassword = (TextInputLayout) findViewById(R.id.til_pass);
        tilConfirmPassword = (TextInputLayout) findViewById(R.id.til_confirm_pass);
//        tilRefferCode = (TextInputLayout) findViewById(R.id.til_ref_code);
        btnGenerateOtp = (Button) findViewById(R.id.btn_generate_otp);
//        setTextChangeListener(tilFirstName,tilLastName,tilEmail,tilNumber,tilPassword,tilConfirmPassword);
    }

    private void setTextChangeListener(TextInputLayout... tils) {
        for(TextInputLayout textInputLayout:tils){
            textInputLayout.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    changeButtonStatus();
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }

    private void changeButtonStatus() {
        String email = tilEmail.getEditText().getText().toString().trim();
        String firstName = tilFirstName.getEditText().getText().toString().trim();
        String lastName = tilLastName.getEditText().getText().toString().trim();
        String mNumber = tilNumber.getEditText().getText().toString().trim();
        String pass = tilPassword.getEditText().getText().toString().trim();
        String cPass = tilConfirmPassword.getEditText().getText().toString().trim();
        if(TextUtils.isEmpty(email)||TextUtils.isEmpty(firstName)||TextUtils.isEmpty(lastName)||TextUtils.isEmpty(mNumber)||TextUtils.isEmpty(pass)||TextUtils.isEmpty(cPass)){
            btnGenerateOtp.setEnabled(false);
            btnGenerateOtp.setBackgroundResource(R.drawable.shape_button_login_disable);
        }else {
            btnGenerateOtp.setEnabled(true);
            btnGenerateOtp.setBackgroundResource(R.drawable.shape_button_login);
        }
    }

    private void validateAndSubmit() {
        String email = tilEmail.getEditText().getText().toString().trim();
        String firstName = tilFirstName.getEditText().getText().toString().trim();
        String lastName = tilLastName.getEditText().getText().toString().trim();
        String mNumber = tilNumber.getEditText().getText().toString().trim();
        String pass = tilPassword.getEditText().getText().toString().trim();
        String cPass = tilConfirmPassword.getEditText().getText().toString().trim();
//        String refferCode = tilRefferCode.getEditText().getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            tvError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.email_not_empty));
            return;
        }
        else if (!Util.isValidEmail(email)) {
            tvError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.valid_email_msg));
            return;
        }
        else {
            tvError.setVisibility(View.GONE);
//            tilEmail.setError(null);
//            tilEmail.setErrorEnabled(false);
        }

        if (TextUtils.isEmpty(firstName)){
            tvError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.error_no_first_name));
            return;
        } else {
            tvError.setVisibility(View.GONE);
//            tilFirstName.setError(null);
//            tilFirstName.setErrorEnabled(false);
        }

        if (TextUtils.isEmpty(lastName)){
            tvError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.error_no_last_name));
            return;
        } else {
            tvError.setVisibility(View.GONE);
//            tilLastName.setError(null);
//            tilLastName.setErrorEnabled(false);
        }

        if (TextUtils.isEmpty(mNumber)){
            tvError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.mobile_number_error));
            return;
        } else if(!Util.isValidPhoneNumber(mNumber)){
            tvError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.mobile_number_error));
        } else if (mNumber.length() < 10){
            tvError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.mobile_number_error));
            return;
        } else {
            tvError.setVisibility(View.GONE);
//            tilNumber.setError(null);
//            tilNumber.setErrorEnabled(false);
        }

        if (TextUtils.isEmpty(pass)){
            tvError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.enter_pass));
            tvPasswordStrength.setVisibility(View.INVISIBLE);
            return;
        } else {
            tvError.setVisibility(View.GONE);
            tvPasswordStrength.setVisibility(View.VISIBLE);
//            tilPassword.setError(null);
//            tilPassword.setErrorEnabled(false);
        }

        if(tilPassword.getEditText().getText().toString().length() >= 6 && tilPassword.getEditText().getText().toString().length() <= 8){
            tvPasswordStrength.setText("Weak");
            tvPasswordStrength.setTextColor(getResourceColor(R.color.color_border_grey));
            tvError.setVisibility(View.GONE);
//            btnGenerateOtp.setBackgroundResource(R.drawable.shape_button_login_disable);
//                    btnChangePassword.setBackgroundColor(getResourceColor(R.color.button_login_color));
        } else if (tilPassword.getEditText().getText().toString().length() > 8){
            tvPasswordStrength.setText("Strong");
            tvPasswordStrength.setTextColor(getResourceColor(R.color.strong_pass_clr));
            tvError.setVisibility(View.GONE);
//            btnGenerateOtp.setBackgroundResource(R.drawable.shape_button_login);
//                    btnChangePassword.setBackgroundColor(getResourceColor(R.color.clr_btn_disabled));
        } else {
            tvPasswordStrength.setText("Poor");
            tvPasswordStrength.setTextColor(getResourceColor(R.color.button_login_color));
//            btnGenerateOtp.setBackgroundResource(R.drawable.shape_button_login_disable);
            tvError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.six_character_pass));
            return;
        }

        if (!pass.equals(cPass)){
            tvError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.password_not_match));
            return;
        } else {
            tvError.setVisibility(View.GONE);
//            tilConfirmPassword.setError(null);
//            tilConfirmPassword.setErrorEnabled(false);
        }

        HttpParamObject httpParamObject = ApiRequestGenerator.validateSignup(email, firstName, lastName, mNumber, pass, "", AppConstants.SIGNUP_TYPE.MANUAL);
        executeTask(AppConstants.TASK_CODES.VALIDATE_SIGNUP, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode){
            case AppConstants.TASK_CODES.SIGNUP:
//                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
//                if(baseApiResponse != null){
//                    if(baseApiResponse.getError() == false){
//                        Bundle bundle = new Bundle();
//                        bundle.putString(AppConstants.BUNDLE_KEYS.NUMBER, number);
//                        bundle.putInt(AppConstants.BUNDLE_KEYS.BUTTON_TARGET, AppConstants.ACTIVITY_CONSTANTS.TARGET_SIGNUP_SUCCESS);
//                        startNextActivity(bundle, ValidateOtpActivity.class);
//                    }
//                    else{
//                        if(baseApiResponse.getCode().equals("DUP_EMAIL")){
//                            openAlertDialog("Email Id already exists. Please login back.");
//                        } else if(baseApiResponse.getCode().equals("DUP_CONTACT")){
//                            openAlertDialog("Mobile number already exists. Please login back");
//                        } else {
//                            showCrouton(baseApiResponse.getMessage());
//                        }
//                    }
//                }
                break;
            case AppConstants.TASK_CODES.VALIDATE_SIGNUP:
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                if(baseApiResponse != null){
                    if(baseApiResponse.getError() == true){
                        if(baseApiResponse.getCode().equals("DUP_EMAIL")){
                           openAlertDialog("Email Id already exists. Please login back.");
                        } else if(baseApiResponse.getCode().equals("DUP_CONTACT")){
                            openAlertDialog("Mobile number already exists. Please login back");
                        } else {
                            showCrouton(baseApiResponse.getMessage());
                        }
                    } else{
                        Bundle bundle = new Bundle();
                        bundle.putString(AppConstants.BUNDLE_KEYS.EMAIL, tilEmail.getEditText().getText().toString().trim());
                        bundle.putString(AppConstants.BUNDLE_KEYS.FIRST_NAME, tilFirstName.getEditText().getText().toString().trim());
                        bundle.putString(AppConstants.BUNDLE_KEYS.LAST_NAME, tilLastName.getEditText().getText().toString().trim());
                        bundle.putString(AppConstants.BUNDLE_KEYS.NUMBER, tilNumber.getEditText().getText().toString().trim());
                        bundle.putString(AppConstants.BUNDLE_KEYS.PASSWORD, tilPassword.getEditText().getText().toString().trim());
//                        bundle.putString(AppConstants.BUNDLE_KEYS.REFERRAL_CODE, tilRefferCode.getEditText().getText().toString().trim());
                        bundle.putInt(AppConstants.BUNDLE_KEYS.SIGNUP_TYPE, AppConstants.SIGNUP_TYPE.MANUAL);
                        bundle.putInt(AppConstants.BUNDLE_KEYS.BUTTON_TARGET, AppConstants.ACTIVITY_CONSTANTS.TARGET_SIGNUP_SUCCESS);
                        Intent intent = new Intent(this, ValidateOtpActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
//                        startNextActivity(bundle, ValidateOtpActivity.class);
                    }
                }
                break;
        }
    }

    private void openAlertDialog(String msg) {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_success);

        TextView tvTitle = (TextView) dialog.findViewById(R.id.dialog_title);
        TextView tvMsg = (TextView) dialog.findViewById(R.id.dialog_msg);
        Button btnOk = (Button) dialog.findViewById(R.id.btn_ok);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);

        tvTitle.setText(R.string.account_exist);
        tvMsg.setText(msg);
        tvCancel.setVisibility(View.VISIBLE);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNextActivity(LoginActivity.class);
            }
        });

        dialog.show();
    }


}
