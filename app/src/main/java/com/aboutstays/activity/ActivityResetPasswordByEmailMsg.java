package com.aboutstays.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.aboutstays.R;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

/**
 * Created by Neeraj Yadav on 12/20/2016.
 */

public class ActivityResetPasswordByEmailMsg extends AppBaseActivity {
    private Button btnOk;
    private String userEmail;
    private TextView tvTitleMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_via_email_msg);
        initToolBar("Reset Password");

        userEmail = getIntent().getExtras().getString(AppConstants.BUNDLE_KEYS.EMAIL);
        btnOk = (Button) findViewById(R.id.btn_ok);
        tvTitleMsg = (TextView) findViewById(R.id.tv_title_msg);
        tvTitleMsg.setText(getTitleText());
        setOnClickListener(R.id.btn_ok);
    }

    private String getTitleText() {
        StringBuilder builder = new StringBuilder(getString(R.string.link_sent));
        builder.append(" ");
        builder.append(userEmail);
        return builder.toString();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_ok:
                finish();
                break;
        }
    }
}
