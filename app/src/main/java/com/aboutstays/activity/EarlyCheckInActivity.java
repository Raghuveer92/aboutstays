package com.aboutstays.activity;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.aboutstays.R;
import com.aboutstays.model.earlycheckIn.EarlyCheckInData;
import com.aboutstays.model.earlycheckIn.EarlyCheckInResponse;
import com.aboutstays.model.earlycheckIn.ListDeviationInfo;
import com.aboutstays.model.initiate_checkIn.CheckInData;
import com.aboutstays.model.initiate_checkIn.EarlyCheckinRequest;
import com.aboutstays.model.lateCheckout.DeviationAndPrice;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.aboutstays.utitlity.TimePickerUtil;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

import static simplifii.framework.utility.Util.convertStringToLong;

public class EarlyCheckInActivity extends AppBaseActivity {

    private String roomId;
    private List<DeviationAndPrice> listDeviationAndPrice;
    public static final int MILISECONDS_IN_HOUR = 60 * 60 * 1000;
    public long pickupTime;
    private TextView tvTime, tvCharge, tvScheduleTime, tvMsg;
    private EarlyCheckInData earlyCheckInData;
    private String standardTime;
    private long requestCheckinTime;
    private double requestPrice;
    private CheckInData checkInData;
    private String checkInTime;
    private Button btnCheckin;
    private Calendar calendarInstance;
    private String earlyRequestTime;
    private String standardTimeString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_early_check_in);
        initToolBar("Early Check-In");
        calendarInstance = Calendar.getInstance();

        findViews();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            roomId = bundle.getString(AppConstants.BUNDLE_KEYS.ROOM_SERVICE_ID);
            checkInData = (CheckInData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        }

        if (!TextUtils.isEmpty(roomId)) {
            getEarlyCheckInData();
        }

        btnCheckin.setVisibility(View.INVISIBLE);
        setData();
        setOnClickListener(R.id.lay_select_time, R.id.btn_check_in);
        tvMsg.setText("Early CheckIn is subject to availability. Plase wait for the confermation");
    }

    private void setData() {
        if (checkInData != null) {
            EarlyCheckinRequest earlyCheckinRequest = checkInData.getEarlyCheckinRequest();
            if (earlyCheckinRequest != null) {
                btnCheckin.setText(getString(R.string.modify_early_check_in));
                tvScheduleTime.setText("Schedule Check-In Time: " + earlyCheckinRequest.getScheduledCheckinTime());
                if (earlyCheckinRequest.getRequestedCheckinTime() != null) {
                    String requestedCheckinTime = earlyCheckinRequest.getRequestedCheckinTime();
                    tvTime.setText(earlyCheckinRequest.getRequestedCheckinTime() + "");
                    DateFormat df = new SimpleDateFormat("hh:mm a");
                    try {
                        calendarInstance.setTime(df.parse(requestedCheckinTime));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
//                    try {
//                        Date date = formatter.parse(requestedCheckinTime);
//                        long time = date.getTime();
//                        calendarInstance.setTimeInMillis(time);
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//                    long stringToLong = Util.convertStringToLong("HH:mm a", requestedCheckinTime);
//                    calendarInstance.setTimeInMillis(stringToLong);
                } else {
                    tvTime.setText(earlyCheckinRequest.getScheduledCheckinTime() + "");
                }
                if (earlyCheckinRequest.getCharges() > 0) {
                    tvCharge.setText("\u20B9 " + earlyCheckinRequest.getCharges());
                }
                requestCheckinTime = convertStringToLong(Util.CHECK_IN_TIME_API_FORMAT, tvTime.getText().toString());
                requestPrice = earlyCheckinRequest.getCharges();
            }
        }
    }

    private void findViews() {
        tvTime = (TextView) findViewById(R.id.tv_time);
        tvCharge = (TextView) findViewById(R.id.tv_charge);
        tvScheduleTime = (TextView) findViewById(R.id.tv_schedule_text);
        tvMsg = (TextView) findViewById(R.id.tv_msg);
        btnCheckin = (Button) findViewById(R.id.btn_check_in);
    }

    private void getEarlyCheckInData() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getRoomEarlyCheckInList(roomId);
        executeTask(AppConstants.TASK_CODES.GET_EARLY_CHECKIN_SERVICE_RESPONSE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_EARLY_CHECKIN_SERVICE_RESPONSE:
                EarlyCheckInResponse checkInResponse = (EarlyCheckInResponse) response;
                if (checkInResponse != null && checkInResponse.getError() == false) {
                    earlyCheckInData = checkInResponse.getData();
                    if (earlyCheckInData != null) {
                        setUpDeviationAndPriceList(earlyCheckInData);
                        if (earlyCheckInData.getCheckInInfo() != null) {
                            if (!TextUtils.isEmpty(earlyCheckInData.getCheckInInfo().getStandardCheckinTime())) {
                                standardTimeString = earlyCheckInData.getCheckInInfo().getStandardCheckinTime();
                                standardTime = Util.convertDateFormat(standardTimeString, Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
                            }
                            checkInTime = Util.convertDateFormat(standardTimeString, Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
                            tvScheduleTime.setText("Schedule Check-In Time: " + checkInTime);
                            if (checkInData.getEarlyCheckinRequest() != null) {
                                if (checkInData.getEarlyCheckinRequest().getRequestedCheckinTime() != null) {
                                    tvTime.setText(checkInData.getEarlyCheckinRequest().getRequestedCheckinTime());
                                }
                            } else {
                                tvTime.setText(checkInTime);
                            }
                            DateFormat df = new SimpleDateFormat("hh:mm a");
                            try {
                                calendarInstance.setTime(df.parse(checkInTime));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_select_time:
                setTimePicker();
                break;
            case R.id.btn_check_in:
                submitData();
                break;
        }
    }

    private void submitData() {
        if (requestCheckinTime <= 0) {
            showToast("Please select Check-In time first ");
            return;
        } else {
            CheckInData checkInData = new CheckInData();
            EarlyCheckinRequest earlyCheckinRequest = new EarlyCheckinRequest();
            earlyCheckinRequest.setCharges(requestPrice);
            earlyCheckinRequest.setEarlyCheckinRequested(true);
            String earlyTime = tvTime.getText().toString();
//            earlyRequestTime = Util.convertDateFormat(earlyTime, Util.CHECK_IN_TIME_UI_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
            earlyCheckinRequest.setRequestedCheckinTime(earlyTime);
            earlyCheckinRequest.setScheduledCheckinTime(checkInTime.toLowerCase());
            checkInData.setEarlyCheckinRequest(earlyCheckinRequest);
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, checkInData);
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void setTimePicker() {
/*        if(!TextUtils.isEmpty(checkInTime)){
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat(Util.CHECK_IN_TIME_UI_FORMAT);
            try {
                Date date = simpleDateFormat.parse(checkInTime);
                Calendar c=Calendar.getInstance();
                c.setTime(date);
                calendar.set(Calendar.HOUR,c.HOUR);
                calendar.set(Calendar.MINUTE,c.MINUTE);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }*/
        TimePickerUtil timePickerUtil = new TimePickerUtil(Util.CHECK_IN_TIME_UI_FORMAT, calendarInstance, getFragmentManager(), "Select Time", new TimePickerUtil.OnTimeSelected() {
            @Override
            public void onTimeSelected(Calendar calendar, String time) {
                calendarInstance = calendar;
                tvTime.setText(time);
                pickupTime = calendar.getTimeInMillis() - 1000;
                verifyValidCheckoutTime();
            }
        });
        Timepoint timepointMax = new Timepoint(calendarInstance.HOUR, calendarInstance.MINUTE);
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(calendarInstance.getTimeInMillis() - (6 * MILISECONDS_IN_HOUR));
        Timepoint timepointMin = new Timepoint(c.HOUR, c.MINUTE, c.SECOND);
        timePickerUtil.show();
    }

    private void verifyValidCheckoutTime() {

        String selectedTime = tvTime.getText().toString();
        long selectedTimeinLong = convertStringToLong(Util.CHECK_IN_TIME_UI_FORMAT, selectedTime);
        long standardTimeInLong = convertStringToLong(Util.CHECK_IN_TIME_API_FORMAT, standardTime);
        if (selectedTimeinLong >= standardTimeInLong) {
            showToast("Please select a time earlier than standard check-in time");
            tvTime.setText(standardTime);
            tvCharge.setText("");
            requestPrice = 0;
        } else if (selectedTimeinLong < (standardTimeInLong - (6 * MILISECONDS_IN_HOUR))) {
            showToast("Deviation can only be maximum of 6 hours");
            tvTime.setText(standardTime);
            requestCheckinTime = standardTimeInLong;
            tvCharge.setText("");
            requestPrice = 0;
        }
//        else if (selectedTimeinLong == standardTimeInLong) {
//            tvTime.setText(standardTime);
//            tvCharge.setText("");
//            requestPrice = 0;
//            requestCheckinTime = standardTimeInLong;
//        }
        else {
            long selectedDeviation = selectedTimeinLong - standardTimeInLong;
            calculateApplicableCharges(selectedDeviation, selectedTimeinLong);
        }

        if (requestCheckinTime <= 0) {
            btnCheckin.setVisibility(View.INVISIBLE);
        } else {
            btnCheckin.setVisibility(View.VISIBLE);
        }

    }

    private void calculateApplicableCharges(long selectedDeviation, long selectedTimeInlong) {
        if (CollectionUtils.isNotEmpty(listDeviationAndPrice)) {

            for (DeviationAndPrice deviationAndPrice : listDeviationAndPrice) {
                if (deviationAndPrice == null || deviationAndPrice.getDevaition() < selectedDeviation) {
                    continue;
                } else {
                    requestCheckinTime = selectedTimeInlong;
                    requestPrice = deviationAndPrice.getPrice();
                    tvCharge.setText("\u20B9 " + deviationAndPrice.getPrice());
                    break;
                }
            }
        }
    }

    private void setUpDeviationAndPriceList(EarlyCheckInData earlyCheckInData) {
        if (earlyCheckInData.getCheckInInfo() != null) {
            List<ListDeviationInfo> listDeviationInfo = earlyCheckInData.getCheckInInfo().getListDeviationInfo();
            if (CollectionUtils.isNotEmpty(listDeviationInfo)) {
                listDeviationAndPrice = new ArrayList<>();
                for (ListDeviationInfo info : listDeviationInfo) {
                    if (info == null || info.getDeviationInHours() == null) {
                        continue;
                    }
                    DeviationAndPrice deviationAdnAndPrice = new DeviationAndPrice();
                    deviationAdnAndPrice.setPrice(info.getPrice());
                    deviationAdnAndPrice.setDevaition((long) (info.getDeviationInHours() * (double) MILISECONDS_IN_HOUR));
                    listDeviationAndPrice.add(deviationAdnAndPrice);
                }

                Collections.sort(listDeviationAndPrice, new Comparator<DeviationAndPrice>() {
                    @Override
                    public int compare(DeviationAndPrice o1, DeviationAndPrice o2) {
                        return o1.getDevaition().compareTo(o2.getDevaition());
                    }
                });
            }
        }
    }
}
