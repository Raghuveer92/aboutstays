package com.aboutstays.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.model.StayPreferenceModel;
import com.aboutstays.model.common.GeneralNameImageInfo;
import com.aboutstays.model.initiate_checkIn.CheckInData;
import com.aboutstays.model.initiate_checkIn.ListStayPreference;
import com.aboutstays.services.UserServices;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.response.requests.StayPref;
import simplifii.framework.rest.response.requests.UpdateStayPreferences;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.response.PrefData;
import simplifii.framework.rest.response.response.PreferanceResponceApi;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;

/**
 * Created by ajay on 13/12/16.
 */
public class StayPreferencesActivity extends AppBaseActivity implements CustomListAdapterInterface {

    private List<StayPref> listPreference = new ArrayList<>();
    private List<StayPref> listPreferenceSelected = new ArrayList<>();
    private ListView listView;
    private CustomListAdapter listAdapter;
    private String userId;
    private List<StayPref> listStayPrefs = new ArrayList<>();
    private boolean fromCheckIn;
    private Button btnSave;
    private String[] pref;
    private CheckInData checkInData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stay_preferences);
        initToolBar("Stay Preferences");
        btnSave = (Button) findViewById(R.id.btn_save_changes);
        listView = (ListView) findViewById(R.id.list_View_stay_preferences);
        listAdapter = new CustomListAdapter(this, R.layout.row_stay_preferences, listPreference, this);
        listView.setAdapter(listAdapter);

        UserSession userSession = UserSession.getSessionInstance();
        if (userSession != null) {
            userId = userSession.getUserId();
        }

//        LayoutInflater inflater = getLayoutInflater();
//        View inflate = inflater.inflate(R.layout.view_list_button, listView, false);
//        Button btnSave = (Button) inflate.findViewById(R.id.btn_save_changes);
//        btnSave.setOnClickListener(this);
//        listView.addFooterView(inflate);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fromCheckIn = bundle.getBoolean(AppConstants.BUNDLE_KEYS.FROM_CHECKIN);
            checkInData = (CheckInData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
/*            if (checkInData != null) {
                List<StayPref> listStayPreferences = checkInData.getListStayPreferences();
                if (CollectionUtils.isNotEmpty(listStayPreferences)) {
                    setSelectedList(listStayPreferences);
                }
            }*/
        }
        btnSave.setOnClickListener(this);
    }

    private void setSelectedData() {
        if (fromCheckIn && CollectionUtils.isNotEmpty(checkInData.getListStayPreferences())) {
            setSelectedList(checkInData.getListStayPreferences());
        } else {
            UserSession userSession = UserSession.getSessionInstance();
            if (userSession != null) {
                List<StayPref> listStayPrefs = userSession.getListStayPreferences();
                setSelectedList(listStayPrefs);
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAllStayPref();
    }

    private void getAllStayPref() {
        String preferencesJson = Preferences.getData(Preferences.KEY_PREFERENCES, "");
        PrefData prefData = (PrefData) JsonUtil.parseJson(preferencesJson, PrefData.class);
        if (prefData != null && CollectionUtils.isNotEmpty(prefData.getListPreferneces())) {
            listPreference.clear();
            listPreference.addAll(prefData.getListPreferneces());
            listAdapter.notifyDataSetChanged();
            setSelectedData();
        } else {
        HttpParamObject httpParamObject = ApiRequestGenerator.getAllStayPref();
        executeTask(AppConstants.TASK_CODES.GET_STAY_PREF, httpParamObject);
        }
    }

    private void setSelectedList(List<StayPref> listStayPrefs) {
        listPreferenceSelected.clear();
        if (listStayPrefs != null) {
            for (StayPref stayPref : listPreference) {
                for (StayPref pref : listStayPrefs) {
                    String preferenceName = pref.getPreferenceName();

                    if (!TextUtils.isEmpty(preferenceName) && preferenceName.equals(stayPref.getPreferenceName())) {
                        listPreferenceSelected.add(stayPref);
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_save_changes:
                submitData();
                break;
        }
    }

    private void submitData() {
//        listStayPrefs.clear();
//        for (StayPref stayPreferenceModel : listPreferenceSelected) {
//            int index = listPreference.indexOf(stayPreferenceModel) + 1;
//            StayPref listStayPref = new StayPref();
//            listStayPref.setPreference(index);
//            if (fromCheckIn) {
//                listStayPref.setPreferenceName(pref[index - 1]);
//            }
//            listStayPrefs.add(listStayPref);
//        }
        if (fromCheckIn) {
            CheckInData checkInData = new CheckInData();
            checkInData.setListStayPreferences(listPreferenceSelected);
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, checkInData);
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            UpdateStayPreferences updateStayPreferences = new UpdateStayPreferences();
            updateStayPreferences.setUserId(userId);
            updateStayPreferences.setListStayPrefs(listPreferenceSelected);
            HttpParamObject httpParamObject = ApiRequestGenerator.updateStayPreference(updateStayPreferences);
            executeTask(AppConstants.TASK_CODES.UPDATE_PREFERCENCE, httpParamObject);
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

/*    Integer[] prefIcon = {
            R.mipmap.quiet_red,
            R.mipmap.closer_to_elevator_red,
            R.mipmap.non_smoke_red,
            R.mipmap.smoke_red,
            R.mipmap.balcony_red,
            R.mipmap.bath_red,
            R.mipmap.higher_fllors_red,
            R.mipmap.lower_floors_red,
            R.mipmap.twin_bedsred,
            R.mipmap.external_view_red
    };*/

/*    private void putDataIntoList() {
        pref = getResources().getStringArray(R.array.stayPreferences);
        for (int i = 0; i < pref.length; i++) {
            StayPref ob = new StayPref();
            ob.setPreference(i + 1);
            ob.setPreferenceName(pref[i]);
            ob.setImgUrl(prefIcon[i]);
            listPreference.add(ob);
        }
    }*/

    @Override
    public View getView(int position, View convertView, final ViewGroup parent, int resourceID, LayoutInflater inflater) {

        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_stay_preferences, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final StayPref stayPref = listPreference.get(position);

        holder.tvPreference.setText(stayPref.getPreferenceName());
        String imgUrl = stayPref.getImgUrl();
        if (!TextUtils.isEmpty(imgUrl)) {
            Picasso.with(this).load(imgUrl).placeholder(R.drawable.progress_animation).into(holder.ivIconPreference);
        } else {
            holder.ivIconPreference.setImageResource(0);
        }


        if (listPreferenceSelected.contains(stayPref)) {
            holder.ivSelect.setVisibility(View.VISIBLE);
        } else {
            holder.ivSelect.setVisibility(View.GONE);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!listPreferenceSelected.contains(stayPref)) {
                    listPreferenceSelected.add(stayPref);
                } else if (listPreferenceSelected.contains(stayPref)) {
                    listPreferenceSelected.remove(stayPref);
                }
                listAdapter.notifyDataSetChanged();
            }
        });

        return convertView;

    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.UPDATE_PREFERCENCE:
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                if (baseApiResponse != null && baseApiResponse.getError() == false) {
//                    showToast("Stay Prefercence updated");
                    UserServices.startUpdateUser(this);
                    successDialog();
//                    finish();
                } else {
                    errorDialog(baseApiResponse.getMessage());
//                    showToast(baseApiResponse.getMessage());
                }
                break;
            case AppConstants.TASK_CODES.GET_STAY_PREF:
                PreferanceResponceApi responceApi = (PreferanceResponceApi) response;
                if (responceApi != null && responceApi.getError() == false) {
                    PrefData data = responceApi.getData();
                    if (data != null) {
                        List<StayPref> listPreferneces = data.getListPreferneces();
                        if (CollectionUtils.isNotEmpty(listPreferneces)) {
                            listPreference.addAll(listPreferneces);
                            listAdapter.notifyDataSetChanged();
                        }
                    }
                }
                setSelectedData();
                break;
        }
    }

    private void errorDialog(String message) {
        SuccessDialogFragment.show(getSupportFragmentManager(),
                "STAY PREFERENCE",
                "UPDATE ERROR",
                message,
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                    }
                }
        );
    }

    private void successDialog() {
        SuccessDialogFragment.show(getSupportFragmentManager(),
                "STAY PREFERENCE",
                "UPDATED",
                "Stay Preference update.",
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
//                        setResult(RESULT_OK);
                        finish();
                    }
                }
        );
    }

    class Holder {
        TextView tvPreference;
        ImageView ivIconPreference, ivSelect;

        public Holder(View view) {
            tvPreference = (TextView) view.findViewById(R.id.tv_preference);
            ivIconPreference = (ImageView) view.findViewById(R.id.iv_preferences);
            ivSelect = (ImageView) view.findViewById(R.id.iv_select);
        }
    }

}
