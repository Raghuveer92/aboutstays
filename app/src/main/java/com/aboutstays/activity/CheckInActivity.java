package com.aboutstays.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.CheckinInformationFragment;
import com.aboutstays.fragment.CheckinServicesFragment;
import com.aboutstays.fragment.dialog.ConfirmDialogFragment;
import com.aboutstays.model.currentStays.GetAllServicesData;
import com.aboutstays.model.currentStays.StaysService;
import com.aboutstays.model.initiate_checkIn.CheckInData;
import com.aboutstays.model.initiate_checkIn.InitiateCheckInResponse;
import com.aboutstays.model.rooms_category.RoomsCategoryData;
import com.aboutstays.model.stayline.GeneralInfo;
import com.aboutstays.model.stayline.HotelStaysList;
import com.aboutstays.model.stayline.StaysAddress;
import com.aboutstays.model.stayline.StaysPojo;
import com.aboutstays.rest.request.BaseServiceRequest;
import com.aboutstays.rest.response.GetAllServicesResponse;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.daimajia.swipe.SwipeLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by neeraj on 23/12/16.
 */

public class CheckInActivity extends AppBaseActivity implements CustomPagerAdapter.PagerAdapterInterface {

    private List<String> list;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private CustomPagerAdapter adapter;
    private ImageView ivCheckIn, ivStay, ivCheckOut, ivOfficial;
    private String selectId = "checkIn";
    private CheckinServicesFragment checkinServicesFragment;
    private CheckinInformationFragment checkinInformationFragment;
    private TextView tvWelcomeUser, tvHotelName, tvAddress, tvRoomName, tvRoomNum, tvCheckindate, tvCheckoutDate, tvCheckinTime, tvCheckoutTime;
    private ImageView ivPartnership;
    private ImageView ivHotelIcon;
    private RelativeLayout laayTop, rl_delete, rl_refresh;
    private LinearLayout layTop;
    private BaseServiceRequest baseServiceRequest;

    private String fullName;
    private HotelStaysList checkInData;
    private String hotelId;
    private String stayId;
    private String userId;
    private boolean isCheckin;
    private long oneDay = 86400000;

    private boolean addServiceTab;
    private boolean addInformationTab;
    private int currentItem;
    private AppBarLayout appBarLayout;
    private SwipeLayout swipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);

        findViews();

        if (appBarLayout.getLayoutParams() != null) {
            AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) layTop.getLayoutParams();
            params.setScrollFlags(0);
        }

        UserSession userSession = UserSession.getSessionInstance();
        if (userSession != null) {
            userId = userSession.getUserId();
            String firstName = userSession.getFirstName();
            String lastName = userSession.getLastName();
            fullName = firstName + " " + lastName;
        }
        initToolBar("Welcome " + fullName);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            checkInData = (HotelStaysList) bundle.getSerializable(AppConstants.BUNDLE_KEYS.HOTEL_CHECK_IN);
            if (checkInData != null && checkInData.getStaysPojo() != null) {
                hotelId = checkInData.getStaysPojo().getHotelId();
                stayId = checkInData.getStaysPojo().getStaysId();
                String checkOutDate = checkInData.getStaysPojo().getCheckOutDate();
                String checkOutTime = checkInData.getStaysPojo().getCheckOutTime();
                String checkOutDateTime = checkOutDate + " " + checkOutTime;
                long checkOut_Date = Util.convertStringToLong(Util.API_DATE_FORMAT, checkOutDate);
                long checkOut_Time = Util.convertStringToLong(Util.API_DATE_FORMAT, checkOutTime);
                long checkout_date_time = Util.convertStringToLong(Util.CHECKOUT_DATE_TIME, checkOutDateTime);
                Preferences.saveData(AppConstants.PREF_KEYS.CHECKOUT_DATE_TIME, checkout_date_time);
                Preferences.saveData(AppConstants.PREF_KEYS.CHECKOUT_DATE, checkOut_Date + oneDay);
                Preferences.saveData(AppConstants.PREF_KEYS.CHECKOUT_TIME, checkOut_Time);
            }
            setData();

        }

        setOnClickListener(R.id.rl_delete, R.id.rl_refresh);
        //setUpTabandViewPager();

       // setOnClickListener(R.id.iv_check_in, R.id.iv_stay, R.id.iv_check_out);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //getInitiateCheckIn();
        getAllServices();

    }

    private void getAllServices() {
        baseServiceRequest = new BaseServiceRequest();
        baseServiceRequest.setHotelId(hotelId);
        baseServiceRequest.setStaysId(stayId);
        baseServiceRequest.setUserId(userId);
        HttpParamObject httpParamObject = ApiRequestGenerator.getAllServices(baseServiceRequest);
        executeTask(AppConstants.TASK_CODES.GET_ALL_SERVICES, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

    private void setUpTabandViewPager() {
        adapter = new CustomPagerAdapter(getSupportFragmentManager(), list, this);
        viewPager.setAdapter(adapter);
        if (currentItem >= 0){
            viewPager.setCurrentItem(currentItem);
        }
        tabLayout.setupWithViewPager(viewPager);
    }


    private void setData() {
        if (checkInData != null) {
            laayTop.setBackgroundColor(getResourceColor(checkInData.getColorId()));
            //tvWelcomeUser.setText("Welcome " + fullName);
            setCustomRating(R.id.custom_rating_bar, checkInData.getHotelType());
            if (checkInData.getPartnershipType() == 1) {
                showVisibility(R.id.iv_partnership);
            } else {
                ivPartnership.setVisibility(View.INVISIBLE);
            }

            if (checkInData.getStaysPojo() != null){
                boolean official = checkInData.getStaysPojo().isOfficial();
                if (official){
                    ivOfficial.setVisibility(View.VISIBLE);
                } else {
                    ivOfficial.setVisibility(View.GONE);
                }
            }

            StaysPojo staysPojo = checkInData.getStaysPojo();
            if (staysPojo != null) {
                tvCheckindate.setText(staysPojo.getCheckInDate());
                tvCheckoutDate.setText(staysPojo.getCheckOutDate());

                String checkInTime = Util.convertDateFormat(staysPojo.getCheckInTime(),
                        Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
                String checkOutTime = Util.convertDateFormat(staysPojo.getCheckOutTime(),
                        Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);


                if(!TextUtils.isEmpty(checkInData.getActualCheckedIn())) {
                    String checkin = Util.convertDateFormat(checkInData.getActualCheckedIn(),
                            Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
                    tvCheckinTime.setText(checkin);
                } else {
                    String hotelCheckinTime = Util.convertDateFormat(staysPojo.getCheckInTime(),
                            Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
                    if (TextUtils.isEmpty(checkInTime)) {
                        tvCheckinTime.setText(staysPojo.getCheckInTime());
                    } else {
                        tvCheckinTime.setText(hotelCheckinTime);
                    }
                }

                if(!TextUtils.isEmpty(checkInData.getActualCheckedOut())) {
                    String checkout = Util.convertDateFormat(checkInData.getActualCheckedOut(),
                            Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
                    tvCheckoutTime.setText(checkout);
                } else {
                    String hotelCheckoutTime = Util.convertDateFormat(staysPojo.getCheckOutTime(),
                            Util.CHECK_IN_TIME_API_FORMAT, Util.CHECK_IN_TIME_UI_FORMAT);
                    if (TextUtils.isEmpty(checkOutTime)) {
                        tvCheckoutTime.setText(staysPojo.getCheckOutTime());
                    } else {
                        tvCheckoutTime.setText(hotelCheckoutTime);
                    }
                }

//                if (TextUtils.isEmpty(checkInTime)) {
//                    tvCheckinTime.setText(staysPojo.getCheckInTime());
//                } else {
//                    tvCheckinTime.setText(checkInTime);
//                }
//
//                if (TextUtils.isEmpty(checkOutTime)) {
//                    tvCheckoutTime.setText(staysPojo.getCheckOutTime());
//                } else {
//                    tvCheckoutTime.setText(checkOutTime);
//                }
            }

            GeneralInfo generalInfo = checkInData.getGeneralInfo();
            if (generalInfo != null) {
                tvHotelName.setText(generalInfo.getName());
                StaysAddress address = generalInfo.getAddress();
                if (address != null) {
                    tvAddress.setText(address.getCity());
                }
                if (!TextUtils.isEmpty(generalInfo.getLogoUrl())) {
                    Picasso.with(this).load(generalInfo.getLogoUrl())
                            .error(R.mipmap.place_holder_circular)
                            .into(ivHotelIcon);
                }
            }

            RoomsCategoryData roomInfo = checkInData.getRoomPojo();
            if (roomInfo != null) {
                tvRoomName.setText(roomInfo.getName() + " - ");
            }
        }
    }


    private void findViews() {
//        ivCheckIn = (ImageView) findViewById(R.id.iv_check_in);
//        ivStay = (ImageView) findViewById(R.id.iv_stay);
//        ivCheckOut = (ImageView) findViewById(R.id.iv_check_out);
        swipeLayout = (SwipeLayout) findViewById(R.id.swipe_layout);
        rl_delete = (RelativeLayout) findViewById(R.id.rl_delete);
        rl_refresh = (RelativeLayout) findViewById(R.id.rl_refresh);

        ivPartnership = (ImageView) findViewById(R.id.iv_partnership);
        ivOfficial = (ImageView) findViewById(R.id.iv_official);
        ivHotelIcon = (ImageView) findViewById(R.id.iv_row_upcoming);
       // tvWelcomeUser = (TextView) findViewById(R.id.tv_welcome_name);
        tvHotelName = (TextView) findViewById(R.id.tv_title_upcoming);
        tvAddress = (TextView) findViewById(R.id.tv_hotel_address);
        tvRoomName = (TextView) findViewById(R.id.tv_room_name);
        tvRoomNum = (TextView) findViewById(R.id.tv_room_num);
        tvCheckindate = (TextView) findViewById(R.id.tv_checkin_date);
        tvCheckoutDate = (TextView) findViewById(R.id.tv_checkout_date);
        tvCheckinTime = (TextView) findViewById(R.id.tv_checkin_time);
        tvCheckoutTime = (TextView) findViewById(R.id.tv_checkout_time);
        laayTop = (RelativeLayout) findViewById(R.id.lay_top);
        appBarLayout = (AppBarLayout) findViewById(R.id.appBar);
        tabLayout = (TabLayout) findViewById(R.id.tablayout_checkin);
        viewPager = (ViewPager) findViewById(R.id.viewpager_checkin);
        layTop = (LinearLayout) findViewById(R.id.lay_view_top);
    }


    private void setUpTabsList() {
        list = new ArrayList<>();
        if(addServiceTab) {
            checkinServicesFragment = CheckinServicesFragment.getInstance(selectId);
            checkinServicesFragment.setArguments(getIntent().getExtras());
            list.add(getString(R.string.service_tab));
        }
        if(addInformationTab) {
            checkinInformationFragment = CheckinInformationFragment.getInstance(selectId);
            checkinInformationFragment.setArguments(getIntent().getExtras());
            list.add(getString(R.string.information_tab));
        }
        setUpTabandViewPager();
    }

    private void initTabs() {
        checkinServicesFragment = CheckinServicesFragment.getInstance(selectId);
        checkinServicesFragment.setArguments(getIntent().getExtras());
        checkinInformationFragment = CheckinInformationFragment.getInstance(selectId);
        checkinInformationFragment.setArguments(getIntent().getExtras());
        list = new ArrayList<>();
        list.add(getString(R.string.service_tab));
        list.add(getString(R.string.information_tab));
    }

    private void getInitiateCheckIn() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getInitiateCheckin(hotelId, stayId, userId);
        executeTask(AppConstants.TASK_CODES.GET_INITIATE_CHECKIN, httpParamObject);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.rl_delete:
//                showToast("Operation Pending");
                openConfirmDialog();
                swipeLayout.close();
                break;
            case R.id.rl_refresh:
                getAllServices();
                swipeLayout.close();
                break;
        }
    }


    private void openConfirmDialog() {

        ConfirmDialogFragment.show(getSupportFragmentManager(), "DELETE?", "Do you really want to delete this stay?", "", "", new ConfirmDialogFragment.OnConfirmListener() {
            @Override
            public void confirm() {
                deleteStay();
            }
        });

//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//                this);
//        alertDialogBuilder.setTitle("Delete Stay");
//        alertDialogBuilder
//                .setMessage("Are you sure you want to delete this stay")
//                .setCancelable(false)
//                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int i) {
//                        dialog.dismiss();
//                    }
//                })
//                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        deleteStay();
//                    }
//                });
//        AlertDialog alertDialog = alertDialogBuilder.create();
//        alertDialog.show();
    }

    private void deleteStay() {
        if (!TextUtils.isEmpty(stayId)) {
            HttpParamObject httpParamObject = ApiRequestGenerator.DeleteStay(stayId);
            executeTask(AppConstants.TASK_CODES.DELETE_STAY, httpParamObject);
        } else {
            showToast("Stay id is empty");
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {

            case AppConstants.TASK_CODES.GET_INITIATE_CHECKIN:
                InitiateCheckInResponse initiateCheckInResponse = (InitiateCheckInResponse) response;
                if (initiateCheckInResponse != null && initiateCheckInResponse.getError() == false) {
                    CheckInData checkInData = initiateCheckInResponse.getData();
                    checkinServicesFragment.setCheckinData(checkInData);
                    checkinInformationFragment.setCheckinData(checkInData);
                    isCheckin=true;
                }
                break;

            case AppConstants.TASK_CODES.DELETE_STAY:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && !apiResponse.getError()){
                    setResult(RESULT_OK);
                    finish();
                } else {
                    showToast(apiResponse.getMessage());
                }
                break;

            case AppConstants.TASK_CODES.GET_ALL_SERVICES:
                GetAllServicesResponse serviceResponse = (GetAllServicesResponse) response;
                if(serviceResponse != null && !serviceResponse.getError()) {
                    GetAllServicesData serviceData = serviceResponse.getData();
                    if(serviceData != null) {
                        List<StaysService> tempList = serviceData.getServicesList();
                        if(CollectionUtils.isNotEmpty(tempList)) {

                            for(StaysService staysService : tempList) {
                                if(staysService == null) {
                                    continue;
                                }
                                if(staysService.isGeneralInfo()) {
                                    addInformationTab = true;
                                } else {
                                    addServiceTab = true;
                                }

                                if(addInformationTab && addServiceTab) {
                                    break;
                                }
                            }
                            setUpTabsList();
                            if(addServiceTab) {
                                checkinServicesFragment.onPostExecute(response,taskCode,params);
                            }
                            if(addInformationTab) {
                                checkinInformationFragment.onPostExecute(response,taskCode,params);
                            }
                        }
                    }
                }

                break;
        }
    }

    private void setotherGray() {
        ivCheckIn.setBackgroundResource(R.drawable.shpae_circular_gray);
        ivStay.setBackgroundResource(R.drawable.shpae_circular_gray);
        ivCheckOut.setBackgroundResource(R.drawable.shpae_circular_gray);
    }


    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        String fragmentName = list.get(position);
        if(fragmentName.equalsIgnoreCase(getString(R.string.service_tab))) {
            return checkinServicesFragment;
        } else if(fragmentName.equalsIgnoreCase(getString(R.string.information_tab))) {
            return checkinInformationFragment;
        }
        return null;

//        switch (position) {
//            case 0:
//                return checkinServicesFragment;
//            case 1:
//                return checkinInformationFragment;
//            default:
//                return null;
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppConstants.RESULT_CODE.RESULT_DONE){
            currentItem = viewPager.getCurrentItem();
            getAllServices();
            hideProgressBar();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        currentItem = viewPager.getCurrentItem();
        getAllServices();
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return (CharSequence) listItem;
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK) {
//         //   getInitiateCheckIn();
//        }
//    }
}
