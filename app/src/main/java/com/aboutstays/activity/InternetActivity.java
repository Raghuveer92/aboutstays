package com.aboutstays.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.adapter.InternetListAdapter;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.internet.InternetPackList;
import com.aboutstays.model.internet.InternetServiceApi;
import com.aboutstays.model.internet.InternetServiceData;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import simplifii.framework.ListAdapters.CustomExpandableListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;

public class InternetActivity extends AppBaseActivity implements SearchView.OnQueryTextListener, CustomExpandableListAdapter.ExpandableListener {
    private List<InternetPackList> internetList = new ArrayList<>();
    private ExpandableListView expandableListView;
    private GeneralServiceModel generalServiceModel;
    private String serviceId;
    private String staysId;
    private String userId;
    private FrameLayout searchFrag;
    private TextView tvEmptyView;
    private MenuItem searchMenuItem;
    private SearchView searchView;
    private TextView tv;
    private List<String> listHeader = new ArrayList<>();
    private LinkedHashMap<String, List<InternetPackList>> mapExpandable = new LinkedHashMap<>();
    private LinkedHashMap<String, List<InternetPackList>> mapExpandableAll = new LinkedHashMap<>();
    private CustomExpandableListAdapter customExpandableListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internet);
        initToolBar("Internet");

        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            userId = sessionInstance.getUserId();
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null) {
                serviceId = generalServiceModel.getGeneralServiceData().getHsId();
                staysId = generalServiceModel.getHotelStaysList().getStaysPojo().getStaysId();
            }
        }

        searchFrag = (FrameLayout) findViewById(R.id.frame_search_fragment);
        tvEmptyView = (TextView) findViewById(R.id.tv_empty_view);

        expandableListView = (ExpandableListView) findViewById(R.id.list_expandable_internet);
        customExpandableListAdapter = new CustomExpandableListAdapter(this, R.layout.row_header_expandable, R.layout.row_internet, mapExpandable, this);
        expandableListView.setAdapter(customExpandableListAdapter);

        expandableListView.setEmptyView(tvEmptyView);
        expandableListView.setTextFilterEnabled(false);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAllInternetService();
    }

    private void getAllInternetService() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getInternetService(serviceId);
        executeTask(AppConstants.TASK_CODES.INTERNET_DATA, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        hideProgressBar();
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.INTERNET_DATA:
                InternetServiceApi serviceApiResponse = (InternetServiceApi) response;
                if (serviceApiResponse != null && serviceApiResponse.getError() == false) {
                    InternetServiceData internetData = serviceApiResponse.getData();
                    if (internetData != null) {
                        List<InternetPackList> listPacks = internetData.getListPacks();
                        if (listPacks != null && !listPacks.isEmpty()) {
                            internetList.addAll(listPacks);
                            filterList(internetList);
                        }
                    }
                }
                break;
        }
    }

    private void filterList(List<InternetPackList> internetList) {
        listHeader.clear();
        mapExpandable.clear();
        for (InternetPackList internetPackList : internetList) {
            String category = internetPackList.getCategory();
            if (!TextUtils.isEmpty(category)) {
                if (mapExpandable.containsKey(category)) {
                    List<InternetPackList> internetPackLists = mapExpandable.get(category);
                    internetPackLists.add(internetPackList);
                } else {
                    List<InternetPackList> laundryItemDatas = new ArrayList<>();
                    laundryItemDatas.add(internetPackList);
                    mapExpandable.put(category, laundryItemDatas);
                }
            }
        }
        refreshParentData();
        mapExpandableAll.putAll(mapExpandable);
    }

    private void refreshParentData() {
        listHeader.clear();
        Iterator<String> iterator = mapExpandable.keySet().iterator();
        while (iterator.hasNext()) {
            listHeader.add(iterator.next());
        }
        customExpandableListAdapter = new CustomExpandableListAdapter(this, R.layout.row_header_expandable, R.layout.row_internet, mapExpandable, this);
        expandableListView.setAdapter(customExpandableListAdapter);
        for (int x = 0; x < listHeader.size(); x++) {
            expandableListView.expandGroup(x);
        }
    }

    private void filterListOnSearch(String newText) {
        mapExpandable.clear();
        for(Map.Entry<String, List<InternetPackList>> entry:mapExpandableAll.entrySet()){
            List<InternetPackList> packLists = entry.getValue();
            List<InternetPackList> packListsSelected = new ArrayList<>();
            for(InternetPackList internetPackList:packLists){
                if(internetPackList.getTitle().toLowerCase().contains(newText.toLowerCase())){
                    packListsSelected.add(internetPackList);
                }
            }
            if(packListsSelected.size()>0){
                mapExpandable.put(entry.getKey(),packListsSelected);
            }
        }
        refreshParentData();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        filterListOnSearch(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        filterListOnSearch(newText);
        return true;
    }


    @Override
    public int getChildSize(int parentPosition) {
        return mapExpandable.get(listHeader.get(parentPosition)).size();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        HolderParent holderParent;
        if(convertView==null){
            convertView=inflater.inflate(resourceId,null);
            holderParent=new HolderParent(convertView);
            convertView.setTag(holderParent);
        }else {
            holderParent= (HolderParent) convertView.getTag();
        }
        String title = listHeader.get(groupPosition);
        holderParent.bindData(title,isExpanded);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent, int resourceId, LayoutInflater inflater) {
        HolderChild holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_internet, parent, false);
            holder = new HolderChild(convertView);
            convertView.setTag(holder);
        } else {
            holder = (HolderChild) convertView.getTag();
        }

        if(isLastChild) {
            holder.viewBottomLine.setVisibility(View.INVISIBLE);
        } else {
            holder.viewBottomLine.setVisibility(View.VISIBLE);
        }

        holder.onBindData(mapExpandable.get(listHeader.get(groupPosition)).get(childPosition));
        return convertView;
    }
    class HolderChild {
        TextView textTitle, textOffer, textCost, textButton;
        View viewBottomLine;

        public HolderChild(View view){
            textTitle = (TextView) view.findViewById(R.id.tv_title_internet);
            textOffer = (TextView) view.findViewById(R.id.tv_internet_speed);
            textCost = (TextView) view.findViewById(R.id.tv_internet_cost);
            textButton = (TextView) view.findViewById(R.id.tv_add);
            viewBottomLine = (View) view.findViewById(R.id.view_line_description);
        }
        public void onBindData(final InternetPackList ob){
            if (ob != null) {


                textTitle.setText(ob.getTitle());
                textOffer.setText(ob.getDescription());

                Boolean complementry = ob.getComplementry();
                if (complementry) {
                    textCost.setText("Free");
                    textButton.setText("GET");
                } else {
                    textCost.setText("\u20B9 " + ob.getPrice());
                    textButton.setText("BUY");
                }
            }

            textButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.INTERNET_PACK_LIST, ob);
                    FragmentContainerActivity.startActivityForResult(InternetActivity.this, AppConstants.FRAGMENT_TYPE.INTERNET_ORDER_DETAILS, bundle, AppConstants.REQUEST_CODE.INTERNET_ORDER);
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }

    class HolderParent{
        TextView textView;
        ImageView ivIndicator;
        public HolderParent(View view) {
            textView= (TextView) view.findViewById(R.id.tv_men_women);
            ivIndicator= (ImageView) view.findViewById(R.id.iv_indicator);
        }
        public void bindData(String title, boolean isExpand){
            textView.setText(title);
            if (isExpand) {
                ivIndicator.setImageResource(R.mipmap.up_arrow_white);
            } else {
                ivIndicator.setImageResource(R.mipmap.arrow_down_white);
            }
        }
    }
}
