package com.aboutstays.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.NotificationFragment;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.notification.NotificationData;
import com.aboutstays.model.notification.NotificationResponseApi;
import com.aboutstays.model.notification.NotificationsList;
import com.aboutstays.model.stayline.GetStayResponse;
import com.aboutstays.rest.request.BaseServiceRequest;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Preferences;

public class NotificationsActivity extends AppBaseActivity implements CustomPagerAdapter.PagerAdapterInterface {
    private List<NotificationFragment> notificationFragments = new ArrayList<>();
    private List<String> tabs = new ArrayList<>();
    private List<NotificationsList> notificationsLists = new ArrayList<>();
    private CustomPagerAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private NotificationFragment notificationFragment = new NotificationFragment();
    private String hsId;
    private String hotelId;
    private String userId;
    private String staysId;
    private TextView tvEmptyView;
    private View viewElevation;
    private List<NotificationsList> notificationsList;
    private Boolean notificationOn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        initToolBar("Notifications");
//        initTabs();

        notificationOn = Preferences.getData(AppConstants.PREF_KEYS.NOTIFICATION_ON, false);

        UserSession session = UserSession.getSessionInstance();
        if(session != null){
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            GeneralServiceModel generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null) {
                if (generalServiceModel.getGeneralServiceData() != null) {
                    hsId = generalServiceModel.getGeneralServiceData().getHsId();
                    GetStayResponse staysPojo = generalServiceModel.getHotelStaysList().getStaysPojo();
                    if (staysPojo != null){
                        hotelId = staysPojo.getHotelId();
                        userId = staysPojo.getUserId();
                        staysId = staysPojo.getStaysId();
                    }
                }
            }
        }

        tabLayout = (TabLayout) findViewById(R.id.tablayout_Notifications);
        viewPager = (ViewPager) findViewById(R.id.viewpager_Notifications);
        tvEmptyView = (TextView) findViewById(R.id.tv_empty_view);
        viewElevation = findViewById(R.id.view_elevation);
        adapter = new CustomPagerAdapter(getSupportFragmentManager(), tabs, this);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (notificationOn != null && notificationOn){
            getNotifications();
        }
    }

    private void getNotifications() {
        BaseServiceRequest baseServiceRequest = new BaseServiceRequest();
        baseServiceRequest.setHotelId(hotelId);
        baseServiceRequest.setStaysId(staysId);
        baseServiceRequest.setUserId(userId);
        HttpParamObject httpParamObject = ApiRequestGenerator.getNotifications(baseServiceRequest);
        executeTask(AppConstants.TASK_CODES.GET_NOTIFICATION, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode){
            case AppConstants.TASK_CODES.GET_NOTIFICATION:
                NotificationResponseApi notificationResponseApi = (NotificationResponseApi) response;
                if (notificationResponseApi != null && !notificationResponseApi.getError()){
                    viewElevation.setVisibility(View.VISIBLE);
                    NotificationData notificationData = notificationResponseApi.getData();
                   if (notificationData != null && CollectionUtils.isNotEmpty(notificationData.getNotificationsList())){
                       notificationsLists.clear();
                       notificationsLists.addAll(notificationData.getNotificationsList());
                       notificationsList = notificationData.getNotificationsList();
                       HashMap<String, List<NotificationsList>> map = new HashMap<>();
                       for (NotificationsList list : notificationsList){
                           if (list != null){
                               String type = list.getType();
                               if (map.containsKey(type)){
                                   List<NotificationsList> notificationsLists = map.get(type);
                                   notificationsLists.add(list);
                               } else {
                                   List<NotificationsList> notificationsLists = new ArrayList<>();
                                   notificationsLists.add(list);
                                   map.put(type, notificationsLists);
                               }
                           }
                       }

                       tabs.clear();
                       notificationFragments.clear();
                       Set<String> strings = map.keySet();
                       List<String> stringList = new ArrayList<>();
                       for (String s : strings){
                           stringList.add(s);
                       }
                       Collections.sort(stringList);
                       tabs.add("All");
                       notificationFragments.add(NotificationFragment.getInstance(notificationsList));
                       for (String s : stringList){
                           List<NotificationsList> notificationsLists = map.get(s);
                           tabs.add(s);
                           notificationFragments.add(NotificationFragment.getInstance(notificationsLists));
                           adapter.notifyDataSetChanged();
                       }
                   } else {
                   }
                } else {
                    viewElevation.setVisibility(View.GONE);
                    tvEmptyView.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

//    private void initTabs() {
//        tabs.add(getString(R.string.requestRelated));
//        tabs.add(getString(R.string.genral));
//        tabs.add(getString(R.string.promotions));
//    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        /*switch (position) {
            case 0:
                requestRelatedFragment = RequestRelatedFragment.getInstance();
                return requestRelatedFragment;
            case 1:
                generalFragment = GeneralFragment.getInstance();
                return generalFragment;
            case 2:
                notificationFragment = NotificationFragment.getInstance(notificationsLists);
                return notificationFragment;
        }*/
        return notificationFragments.get(position);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_switch_notification, menu);
        MenuItem item = menu.findItem(R.id.switchId);
        MenuItemCompat.setActionView(item, R.layout.layout_switch_toolabr);
        View actionView = MenuItemCompat.getActionView(item);
        SwitchCompat switchCompat = (SwitchCompat) actionView.findViewById(R.id.switchAB);
        if (notificationOn != null && notificationOn){
            switchCompat.setChecked(true);
        } else {
            switchCompat.setChecked(false);
        }
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked){
                    Preferences.saveData(AppConstants.PREF_KEYS.NOTIFICATION_ON, true);
                    getNotifications();
//                    showToast("Checked");
                } else {
                    Preferences.saveData(AppConstants.PREF_KEYS.NOTIFICATION_ON, false);
//                    showToast("Not_checked");
                }
            }
        });

        return true;
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return tabs.get(position);
    }
}
