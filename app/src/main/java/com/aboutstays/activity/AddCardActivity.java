package com.aboutstays.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.aboutstays.R;
import com.aboutstays.model.AddCardModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

/**
 * Created by ajay on 9/12/16.
 */

public class AddCardActivity extends AppBaseActivity {

    private Button btnSaveCard;
    private TextInputLayout tilBankName, tilCardNumber, tilValidDate;
    private EditText etValidDate;
    private int mYear, mMonth, mDay;
    private List<AddCardModel> cardModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);

        initToolBar("Add Card");
        btnSaveCard = (Button) findViewById(R.id.btn_save_card);
        tilBankName = (TextInputLayout) findViewById(R.id.til_bank_name);
        tilCardNumber = (TextInputLayout) findViewById(R.id.til_card_number);
        tilValidDate = (TextInputLayout) findViewById(R.id.til_expiry_date);

        etValidDate = (EditText) findViewById(R.id.et_expiry_date);

        setOnClickListener(R.id.btn_save_card, R.id.et_expiry_date);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save_card:
                saveData();
                break;
            case R.id.et_expiry_date:
                openDatePicker();
                break;
        }
    }

    private void saveData() {
        String bName = tilBankName.getEditText().getText().toString().trim();
        String cNumber = tilCardNumber.getEditText().getText().toString().trim();
        String date = tilValidDate.getEditText().getText().toString().trim();

        if (TextUtils.isEmpty(bName)) {
            showToast(getString(R.string.bank_name));
            return;
        }
        if (TextUtils.isEmpty(cNumber)) {
            showToast(getString(R.string.card_num));
            return;
        }
        if (TextUtils.isEmpty(date)) {
            showToast(getString(R.string.valid_date));
            return;
        }

        AddCardModel addCardModel = new AddCardModel();
        addCardModel.setBankName(bName);
        addCardModel.setCardNumber(cNumber);
        addCardModel.setValidDate(date);
        Intent intent = new Intent();
        intent.putExtra(AppConstants.BUNDLE_KEYS.ADD_CARD_MODEL, addCardModel);
        setResult(RESULT_OK, intent);
        //  cardModelList.add(addCardModel);
        openDialog();

    }

    private void openDatePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
//        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-yyyy");
                        String fromDatePick = simpleDateFormat.format(c.getTime());
                        etValidDate.setText(fromDatePick);
//                        long fromDate = getTimeFromCalendar(dayOfMonth, monthOfYear, year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
        datePickerDialog.show();
    }

    private long getTimeFromCalendar(int day, int month, int year) {
        final Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        return c.getTimeInMillis();
    }

    private void openDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.view_dialog_card_added);
        Button okButton = (Button) dialog.findViewById(R.id.btn_dialog_ok);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.show();
    }
}
