package com.aboutstays.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.model.booking.BookingData;
import com.aboutstays.model.booking.GetBookingApi;
import com.aboutstays.model.stayline.GeneralSearchInformation;
import com.aboutstays.model.stayline.HotelBasicInfo;
import com.aboutstays.model.stayline.SearchedHotelAddress;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.aboutstays.utitlity.DatePickerUtil;
import com.aboutstays.utitlity.TimePickerUtil;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class RetrieveBookingActivity extends AppBaseActivity {
    private Button btnRetrveBooking;
    private ImageView ivHotelIcon;
    private TextView tvHotelName, tvHotelAddress, tvRevError, tvGuestNameError;
    private HotelBasicInfo searchHotelList, commingFromLogin;
    private EditText etRevNum, etName, etCheckindate;
    private String revNum;
    private Boolean isLoggedIn;
    private String hotelId;
    private String userId;
    public static final String RETRIEVE_BOOKING_DATE = "dd-MM-yyyy";
    private Calendar calendarInstance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrieve_booking);
        initToolBar(getString(R.string.retrieve_stay));
        calendarInstance = Calendar.getInstance();

        UserSession userSession = UserSession.getSessionInstance();
        if (userSession != null) {
            userId = userSession.getUserId();
        }

        Preferences.initSharedPreferences(this);
        isLoggedIn = Preferences.getData(Preferences.LOGIN_KEY, false);

        tvHotelName = (TextView) findViewById(R.id.tv_hotel_name_retrieve);
        tvHotelAddress = (TextView) findViewById(R.id.tv_hotel_address_retrieve);
        etRevNum = (EditText) findViewById(R.id.et_reservation_num);
        etName = (EditText) findViewById(R.id.et_name);
        etCheckindate = (EditText) findViewById(R.id.et_checkin_date);
        ivHotelIcon = (ImageView) findViewById(R.id.iv_hotel_icon);
        btnRetrveBooking = (Button) findViewById(R.id.btn_retrieve_booking);
        tvRevError = (TextView) findViewById(R.id.tv_rev_num_error);
        tvGuestNameError = (TextView) findViewById(R.id.tv_guest_name_error);
        setOnClickListener(R.id.btn_retrieve_booking, R.id.et_checkin_date);

        Bundle getSearchData = getIntent().getExtras();
        if (getSearchData != null) {
            searchHotelList = (HotelBasicInfo) getSearchData.getSerializable(AppConstants.BUNDLE_KEYS.SEARCH_HOTEL_RESPONSE);
            if (searchHotelList != null) {
                hotelId = searchHotelList.getHotelId();
                setDataIntoList(searchHotelList);
            }
        }

    }

    private void setDataIntoList(HotelBasicInfo searchHotelList) {
        GeneralSearchInformation generalInformation = searchHotelList.getGeneralInformation();
        if (generalInformation != null) {
            tvHotelName.setText(generalInformation.getName());
            SearchedHotelAddress address = generalInformation.getAddress();
            if (address != null) {
                tvHotelAddress.setText(address.getCompleteAddress());
            }
            if (!TextUtils.isEmpty(generalInformation.getLogoUrl())) {
                Picasso.with(this).load(generalInformation.getLogoUrl())
                        .placeholder(R.drawable.progress_animation)
                        .into(ivHotelIcon);
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_retrieve_booking:
                if (isLoggedIn) {
                    validateData();
                } else {
                    showToast(getString(R.string.login_to_continue));
                    Intent intent = new Intent(RetrieveBookingActivity.this, LoginActivity.class);
                    Bundle b = new Bundle();
                    b.putBoolean(AppConstants.BUNDLE_KEYS.IS_CHILD_ACTIVITY, true);
                    intent.putExtras(b);
                    startActivityForResult(intent, AppConstants.REQUEST_CODE.LOGIN);
                }

//                startNextActivity(AddStayActivity.class);
                break;

            case R.id.et_checkin_date:
                //datePicker();
                setDateandTime();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)
            return;
        switch (requestCode) {
            case AppConstants.REQUEST_CODE.LOGIN:
                isLoggedIn = true;
                validateData();
                break;
        }
    }

    private void datePicker() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        String fromDatePick = simpleDateFormat.format(c.getTime());
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        String dateinFormat = dateFormat.format(c.getTime());
                        etCheckindate.setText(dateinFormat);
// long fromDate = getTimeFromCalendar(dayOfMonth, monthOfYear, year);

                    }
                }, mYear, mMonth, mDay);
//        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.show();
    }

    private void setDateandTime() {
        DatePickerUtil datePickerUtil = new DatePickerUtil(RETRIEVE_BOOKING_DATE, calendarInstance, getFragmentManager(), getString(R.string.select_date), false, false, new DatePickerUtil.OnDateSelected() {
            @Override
            public void onDateSelected(Calendar calendar, String date) {
                etCheckindate.setText(date);
            }

            @Override
            public void onCanceled() {
//                etCheckindate.setText("");
            }
        });
        datePickerUtil.show();
    }

    private void validateData() {
        revNum = etRevNum.getText().toString().trim();
        String name = etName.getText().toString().trim();
        String checkinDate = etCheckindate.getText().toString().trim();

        if (TextUtils.isEmpty(revNum)) {
            tvRevError.setVisibility(View.VISIBLE);
            return;
        } else {
            tvRevError.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(hotelId)) {
            showToast("Can not search with current information");
            return;
        }
        if (TextUtils.isEmpty(name) && TextUtils.isDigitsOnly(checkinDate)) {
            tvGuestNameError.setVisibility(View.VISIBLE);
            return;
        } else {
            tvGuestNameError.setVisibility(View.GONE);
        }
        getBookingData(revNum, checkinDate, name);

    }

    private void getBookingData(String revNum, String checkinDate, String name) {
        HttpParamObject httpParamObject = ApiRequestGenerator.getBookingData(revNum, checkinDate, hotelId, name, userId);
        executeTask(AppConstants.TASK_CODES.GET_BOOKING_DATA, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast("Server Error");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_BOOKING_DATA:
                GetBookingApi getBookingApi = (GetBookingApi) response;
                if (getBookingApi != null && getBookingApi.getError() == false) {
                    BookingData bookingData = getBookingApi.getData();
                    if (bookingData.getListBookings() != null && bookingData.getListBookings().size() == 1) {
                        Bundle b = new Bundle();
                        b.putSerializable(AppConstants.BUNDLE_KEYS.HOTEL_DATA,searchHotelList);
                        b.putSerializable(AppConstants.BUNDLE_KEYS.BOOKING_LIST, bookingData.getListBookings().get(0));
                        startNextActivity(b, AddStayActivity.class);
                    } else {
                        Bundle b = new Bundle();
                        b.putSerializable(AppConstants.BUNDLE_KEYS.HOTEL_DATA,searchHotelList);
                        b.putSerializable(AppConstants.BUNDLE_KEYS.BOOKING_DATA, bookingData);
                        startNextActivity(b, MultipleBookingSelectionActivity.class);
                    }
                } else {
                    openAlertDialoge();
//                    showToast(getBookingApi.getMessage());
                }
                break;
        }
    }

    private void openAlertDialoge() {

        SuccessDialogFragment.show(getSupportFragmentManager(),
                getString(R.string.retrieve_stay),
                "",
                "No matching bookings found. Kindly check the details and retry",
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
//                        setResult(RESULT_OK);
//                        finish();
                    }
                }
        );

//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//                this);
//        alertDialogBuilder.setTitle("");
//        alertDialogBuilder
//                .setMessage("No matching bookings found. Kindly check the details and retry")
//                .setCancelable(false)
//                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.dismiss();
//                    }
//                });
//        AlertDialog alertDialog = alertDialogBuilder.create();
//        alertDialog.show();
    }



    @Override
    protected int getHomeIcon() {
        return R.mipmap.left_arrow;
    }
}
