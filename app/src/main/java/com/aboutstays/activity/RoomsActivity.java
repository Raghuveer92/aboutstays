package com.aboutstays.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.hotels.Hotel;
import com.aboutstays.model.rooms_category.GetAllRoomCategories;
import com.aboutstays.model.rooms_category.RoomsCategoryData;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class RoomsActivity extends AppBaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    private ListView lvRooms;
    private TextView tvEmptyView;
    private CustomListAdapter listAdapter;
    private List<RoomsCategoryData> roomsModelList = new ArrayList<>();
    private Hotel listHotel;
    private String hotelId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);
        initToolBar("Rooms");

        tvEmptyView = (TextView) findViewById(R.id.tv_empty_view);

        Bundle getGeneralInfo = getIntent().getExtras();
        if (getGeneralInfo != null) {
            listHotel = (Hotel) getGeneralInfo.getSerializable(AppConstants.BUNDLE_KEYS.GENERAL_INFO);
            hotelId = listHotel.getHotelId();
        }

        lvRooms = (ListView) findViewById(R.id.lv_rooms);
        listAdapter = new CustomListAdapter(this, R.layout.row_3_star, roomsModelList, this);
        lvRooms.setAdapter(listAdapter);
        lvRooms.setEmptyView(tvEmptyView);
        lvRooms.setOnItemClickListener(this);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        loadRoomsData();
    }

    private void loadRoomsData() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getRoomsbyId(hotelId);
        executeTask(AppConstants.TASK_CODES.GET_ROOM_BY_ID, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_ROOM_BY_ID:
                GetAllRoomCategories getAllRoomCategories = (GetAllRoomCategories) response;
                if (getAllRoomCategories != null) {
                    List<RoomsCategoryData> roomsList = ((GetAllRoomCategories) response).getData();
                    if (roomsList != null) {
                        roomsModelList.addAll(roomsList);
                        listAdapter.notifyDataSetChanged();
                    }
                }
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        RoomsCategoryData roomModel = roomsModelList.get(position);
        holder.tvTitle.setText(roomModel.getName());
//        holder.tvPerson.setText(roomModel.getOccupancy()+ " persons");
        holder.llCount.setVisibility(View.VISIBLE);
        holder.tvAdult_count.setText(roomModel.getSize() + "");
        holder.tvChildCount.setText(roomModel.getChildCapacity() + "");
        holder.tvType.setText(roomModel.getAdultCapacity() + " Rooms");

        final String imageUrl = roomModel.getImageUrl();
        if (imageUrl != null) {
            final Holder finalHolder = holder;
            Picasso.with(RoomsActivity.this).load(imageUrl).placeholder(R.drawable.progress_animation).into(holder.ivHotelRoom, new Callback() {
                @Override
                public void onSuccess() {
                    Picasso.with(RoomsActivity.this).load(imageUrl).placeholder(R.drawable.progress_animation).into(finalHolder.ivHotelRoom);
                }

                @Override
                public void onError() {
                    Picasso.with(RoomsActivity.this).load(R.mipmap.aboutstays_logo).placeholder(R.drawable.progress_animation).into(finalHolder.ivHotelRoom);
                }
            });
        } else {
            Picasso.with(RoomsActivity.this).load(R.mipmap.aboutstays_logo).placeholder(R.drawable.progress_animation).into(holder.ivHotelRoom);
        }
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        RoomsCategoryData roomModel = roomsModelList.get(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.ROOMS_DATA, roomModel);
        startNextActivity(bundle, RoomNameActivity.class);
    }

    class Holder {
        TextView tvTitle, tvPerson, tvType, tvPrice, tvAdult_count, tvChildCount;
        ImageView ivHotelRoom;
        LinearLayout llCount;

        public Holder(View view) {
            tvPrice = (TextView) view.findViewById(R.id.tv_price);
            tvTitle = (TextView) view.findViewById(R.id.tv_hotel_name);
            tvPerson = (TextView) view.findViewById(R.id.tv_person);
            tvType = (TextView) view.findViewById(R.id.tv_hotel_address);
            ivHotelRoom = (ImageView) view.findViewById(R.id.iv_hotel_room);
            llCount = (LinearLayout) view.findViewById(R.id.ll_count);
            tvAdult_count = (TextView) view.findViewById(R.id.tv_adult_count);
            tvChildCount = (TextView) view.findViewById(R.id.tv_child_count);

        }
    }
}
