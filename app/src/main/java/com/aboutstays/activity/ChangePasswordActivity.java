package com.aboutstays.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.utitlity.ApiRequestGenerator;


import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class ChangePasswordActivity extends AppBaseActivity{

    private TextInputLayout tilconfirmPassword, tilPassword;
    private TextView tvPasswordStrength;
    private Button btnChangePassword;
    private String value;
    private Integer type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initToolBar(getString(R.string.change_pass));
        initCroutonView();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            value = bundle.getString(AppConstants.BUNDLE_KEYS.NUMBER);
            String typeName = bundle.getString(AppConstants.BUNDLE_KEYS.TYPE);
            try {
                type = Integer.parseInt(typeName);
            } catch(NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }

        }

//        type = getIntent().getExtras().getInt(AppConstants.BUNDLE_KEYS.TYPE);
//        if(type == AppConstants.TYPE.PHONE){
//            value = getIntent().getExtras().getString(AppConstants.BUNDLE_KEYS.NUMBER);
//        } else if(type == AppConstants.TYPE.EMAIL){
//            value = getIntent().getExtras().getString(AppConstants.BUNDLE_KEYS.EMAIL);
//        } else{
//            value = "";
//        }

        tilPassword = (TextInputLayout) findViewById(R.id.til_pass);
        tilconfirmPassword = (TextInputLayout) findViewById(R.id.til_confirm_pass);
        tvPasswordStrength = (TextView) findViewById(R.id.tv_password_strength);
        btnChangePassword = (Button) findViewById(R.id.btn_change_password);

        setOnClickListener(R.id.btn_change_password);

        tilPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeButtonStatus();
                if(!TextUtils.isEmpty(tilPassword.getEditText().getText().toString().trim())){
                    tvPasswordStrength.setVisibility(View.VISIBLE);
                }

                if(tilPassword.getEditText().getText().toString().length() >= 6 && tilPassword.getEditText().getText().toString().length() <= 8){
                    tvPasswordStrength.setText("Weak");
                    tvPasswordStrength.setTextColor(getResourceColor(R.color.color_border_grey));
                    btnChangePassword.setClickable(true);
                    btnChangePassword.setBackgroundResource(R.drawable.shape_button_login_disable);
//                    btnChangePassword.setBackgroundColor(getResourceColor(R.color.button_login_color));
                } else if (tilPassword.getEditText().getText().toString().length() > 8){
                    tvPasswordStrength.setText("Strong");
                    tvPasswordStrength.setTextColor(getResourceColor(R.color.strong_pass_clr));
                    btnChangePassword.setClickable(true);
                    btnChangePassword.setBackgroundResource(R.drawable.shape_button_login);
//                    btnChangePassword.setBackgroundColor(getResourceColor(R.color.clr_btn_disabled));
                } else {
                    tvPasswordStrength.setText("Poor");
                    tvPasswordStrength.setTextColor(getResourceColor(R.color.button_login_color));
                    btnChangePassword.setClickable(false);
                    btnChangePassword.setBackgroundResource(R.drawable.shape_button_login_disable);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tilconfirmPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                changeButtonStatus();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void changeButtonStatus(){
        String password = tilPassword.getEditText().getText().toString().trim();
        String confPassword = tilconfirmPassword.getEditText().getText().toString().trim();
        if(TextUtils.isEmpty(password)||TextUtils.isEmpty(confPassword)){
            btnChangePassword.setBackgroundResource(R.drawable.shape_button_login_disable);
            btnChangePassword.setEnabled(false);
        }else {
            btnChangePassword.setBackgroundResource(R.drawable.shape_button_login);
            btnChangePassword.setEnabled(true);
        }
    }

    private void validateAndSubmit() {
        String password = tilPassword.getEditText().getText().toString().trim();
        String confPassword = tilconfirmPassword.getEditText().getText().toString().trim();

        if(!password.equals(confPassword)){
            tilconfirmPassword.setError(getString(R.string.tow_pass_not_match));
            return;
        } else {
            tilconfirmPassword.setErrorEnabled(false);
            tilconfirmPassword.setError("");
        }

        HttpParamObject httpParamObject = ApiRequestGenerator.updatePassword(type, value, password);
        executeTask(AppConstants.TASK_CODES.CHANGE_PASS, httpParamObject);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_change_password:
                validateAndSubmit();
                break;
        }
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode){
            case AppConstants.TASK_CODES.CHANGE_PASS:
                BaseApiResponse baseApiResponse = (BaseApiResponse) response;
                if(baseApiResponse != null){
                    if(baseApiResponse.getError() == false){
                        showToast("Password updated successfully");
                        finish();
                        startNextActivity(LoginActivity.class);
                    } else{
                        showToast(baseApiResponse.getMessage());
                    }
                }
                break;
        }
    }
}
