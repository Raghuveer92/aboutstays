package com.aboutstays.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.aboutstays.R;
import com.aboutstays.model.initiate_checkIn.CheckInData;
import com.aboutstays.model.initiate_checkIn.SpecialRequest;

import java.util.List;

import simplifii.framework.rest.response.requests.StayPref;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

public class SpecialRequestActivity extends AppBaseActivity {

    private EditText etSpecialRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_request);
        initToolBar("Special Request");

        etSpecialRequest = (EditText) findViewById(R.id.et_special_request);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            CheckInData checkInData = (CheckInData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (checkInData != null){
                List<SpecialRequest> listSpecialRequests = checkInData.getSpecialRequests();
                if (CollectionUtils.isNotEmpty(listSpecialRequests)){
                    for(SpecialRequest specialRequest : listSpecialRequests) {
                        if(specialRequest == null) {
                            continue;
                        }
                        etSpecialRequest.setText(specialRequest.getRequest());
                    }
                }
            }
        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_done, menu);
        MenuItem item = menu.findItem(R.id.ic_action_done);
        item.setVisible(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_action_done:
                String specialRequest = etSpecialRequest.getText().toString();
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.BUNDLE_KEYS.SPECIAL_REQUEST, specialRequest);
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
