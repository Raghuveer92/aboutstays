package com.aboutstays.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.transition.Visibility;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.enums.DocType;
import com.aboutstays.model.initiate_checkIn.CheckInData;
import com.aboutstays.services.UserServices;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.rest.response.pojo.IdentityDoc;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

/**
 * Created by ajay on 13/12/16.
 */
public class IdentityDocuments extends AppBaseActivity implements CustomListAdapterInterface {

    private TextView btnAddDocuments;
    private ListView listDoc;
    private CustomListAdapter listAdapter;
    private List<IdentityDoc> identityDocumentList = new ArrayList<>();
    private List<IdentityDoc> selectedList = new ArrayList<>();
    private boolean fromCheckIn;
    private CheckInData checkInData;
    private AddDocumentListner addDocumentListner;
    private List<IdentityDoc> listIdentityDocs;
    private List<IdentityDoc> identityDoc;

    public static IdentityDocuments getInstance(AddDocumentListner addDocumentListner){
        IdentityDocuments identityDocuments = new IdentityDocuments();
        identityDocuments.addDocumentListner = addDocumentListner;
        return identityDocuments;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_identity_documents);
        initToolBar("Identity Documents");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fromCheckIn = bundle.getBoolean(AppConstants.BUNDLE_KEYS.FROM_CHECKIN);
            identityDoc = (List<IdentityDoc>) bundle.getSerializable(AppConstants.BUNDLE_KEYS.IDENTITY_DOC);
            checkInData = (CheckInData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        }

        listDoc = (ListView) findViewById(R.id.list_doc);
        listAdapter = new CustomListAdapter(this, R.layout.row_identity_doc, identityDocumentList, this);
        listDoc.setAdapter(listAdapter);

        LayoutInflater inflater = getLayoutInflater();
        View inflate = inflater.inflate(R.layout.doc_list_footer, listDoc, false);
        TextView tvAdd = (TextView) inflate.findViewById(R.id.tv_add_document);
        tvAdd.setOnClickListener(this);
        listDoc.addFooterView(inflate);

        setDataintoList();
    }


    private void setDataintoList() {
        UserServices.startUpdateUser(this);
        if (checkInData!=null && CollectionUtils.isNotEmpty(checkInData.getIdentityDocuments())) {
            listIdentityDocs = checkInData.getIdentityDocuments();
        } else {
            UserSession userSession = UserSession.getSessionInstance();
            if (userSession != null) {
                listIdentityDocs = userSession.getListIdentityDocs();
            }
        }

            identityDocumentList.clear();
            if (CollectionUtils.isNotEmpty(listIdentityDocs)) {
                for (IdentityDoc identityDoc : listIdentityDocs) {
                    IdentityDoc ob = new IdentityDoc();
//                    Integer docType = identityDoc.getDocType();
//                    DocType docByType = DocType.findDocByType(docType);
//                    String docName = docByType.getDocName();
                    ob.setNameOnDoc(identityDoc.getNameOnDoc());
                    ob.setDocNumber(identityDoc.getDocNumber());
                    ob.setDocName(identityDoc.getDocName());
                    ob.setDocType(identityDoc.getDocType());
                    ob.setDocUrl(identityDoc.getDocUrl());
                    identityDocumentList.add(ob);
                }
            }
        listAdapter.notifyDataSetChanged();
    }

    private void setSelectedList(List<IdentityDoc> docList) {
        selectedList.clear();
        if (listIdentityDocs != null) {
            for (IdentityDoc docs : listIdentityDocs) {
                for (IdentityDoc listDoc : docList) {
                    String nameOnDoc = listDoc.getNameOnDoc();

                    if (!TextUtils.isEmpty(nameOnDoc) && nameOnDoc.equals(docs.getNameOnDoc())) {
                        selectedList.add(docs);
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_add_document:
                Bundle bundle = new Bundle();
                bundle.putBoolean(AppConstants.BUNDLE_KEYS.FROM_CHECKIN, fromCheckIn);
                Intent intent = new Intent(this, AddDocumentActivity.class);
                if (fromCheckIn){
                    intent.putExtras(bundle);
                }
                startActivityForResult(intent, AppConstants.REQUEST_CODE.ADD_DOC_ACTIVITY);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppConstants.REQUEST_CODE.ADD_DOC_ACTIVITY:
                    if (data != null){
                        Bundle extras = data.getExtras();
                        if (extras != null){
                            IdentityDoc identityDoc = (IdentityDoc) extras.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
                            if (identityDoc != null){
                                identityDocumentList.add(identityDoc);
                            }
                        }
                    }
                    if (!fromCheckIn){
                        finish();
                    } else {
                        listAdapter.notifyDataSetChanged();
                    }
                    break;
            }
        }
    }

//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_done, menu);
//        MenuItem item = menu.findItem(R.id.ic_action_done);
//        if (fromCheckIn) {
//            item.setVisible(true);
//        } else {
//            item.setVisible(false);
//        }
//        return super.onCreateOptionsMenu(menu);
//    }

/*    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_action_done:
                CheckInData checkInData = new CheckInData();
                if (CollectionUtils.isNotEmpty(identityDocumentList)) {
                    checkInData.setIdentityDocuments(identityDocumentList);
                }
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, checkInData);
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onBackPressed() {
        CheckInData checkInData = new CheckInData();
        if (CollectionUtils.isNotEmpty(selectedList)) {
            checkInData.setIdentityDocuments(selectedList);
        }
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, checkInData);
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.IDENTITY_DOC, (Serializable) identityDocumentList);
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_identity_doc, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final IdentityDoc id = identityDocumentList.get(position);

        if (id != null) {
            holder.tvDocName.setText(id.getDocType());
            holder.tvDocNum.setText(id.getDocNumber());
        }
        holder.ivSelect.setVisibility(View.INVISIBLE);
        final Holder finalHolder = holder;
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fromCheckIn){
                    if (selectedList.contains(id)) {
                        selectedList.remove(id);
                        finalHolder.ivSelect.setVisibility(View.INVISIBLE);
                    } else {
                        selectedList.add(id);
                        finalHolder.ivSelect.setVisibility(View.VISIBLE);
                    }
                } else {
                    Bundle b = new Bundle();
                    b.putSerializable(AppConstants.BUNDLE_KEYS.IDENTITY_DOC, id);
                    Intent intent = new Intent(IdentityDocuments.this, AddDocumentActivity.class);
                    intent.putExtras(b);
                    startActivityForResult(intent, AppConstants.REQUEST_CODE.ADD_DOC_ACTIVITY);
                }
            }
        });
        return convertView;
    }

    class Holder {
        ImageView ivSelect;
        TextView tvDocName, tvDocNum;

        public Holder(View view) {
            tvDocName = (TextView) view.findViewById(R.id.tv_doc_name);
            tvDocNum = (TextView) view.findViewById(R.id.tv_doc_num);
            ivSelect = (ImageView) view.findViewById(R.id.iv_select);
        }
    }

    public interface AddDocumentListner {
        public void uploadDocs();
        public void selectDocs();
    }

}
