package com.aboutstays.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.aboutstays.R;
import com.aboutstays.rest.request.SuggestHotelRequest;
import com.aboutstays.utitlity.ApiRequestGenerator;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;

public class SuggestHotelsActivity extends AppBaseActivity {

    private String cityName, hotelName;
    private EditText etHotelName;
    private EditText etCityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_hotels);
        initToolBar(getString(R.string.suggest_a_hotel));

        etHotelName = (EditText) findViewById(R.id.tv_enter_hotel_name);
        etCityName = (EditText) findViewById(R.id.text_enter_hotel_location);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            cityName = bundle.getString(AppConstants.BUNDLE_KEYS.CITY_NAME);
            hotelName = bundle.getString(AppConstants.BUNDLE_KEYS.HOTEL_NAME);
            setText(hotelName, R.id.tv_enter_hotel_name);
            setText(cityName, R.id.text_enter_hotel_location);
        }

        setOnClickListener(R.id.btn_retrieve_booking);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_retrieve_booking :
                suggestHotel();
                break;
        }
    }

    private void suggestHotel() {
        if(TextUtils.isEmpty(etHotelName.getText().toString())) {
            showToast("Please enter a hotel name");
            return;
        }

        SuggestHotelRequest request = new SuggestHotelRequest();

        request.setCity(etCityName.getText().toString().trim());
        request.setHotel(etHotelName.getText().toString().trim());

        UserSession session = UserSession.getSessionInstance();
        if(session != null) {
            request.setUserId(session.getUserId());
        }

        HttpParamObject httpParamObject = ApiRequestGenerator.suggestHotel(request);
        executeTask(AppConstants.TASK_CODES.SUGGEST_HOTLE, httpParamObject);
    }


    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        showToast(e.getMessage());
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch(taskCode) {
            case AppConstants.TASK_CODES.SUGGEST_HOTLE:
                BaseApiResponse baseResponse = (BaseApiResponse) response;
                if(baseResponse != null && !baseResponse.getError()) {
                    showToast("Thank you for your suggestion");
                    setResult(RESULT_OK);
                    finish();
                }
                break;
        }
    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.left_arrow;
    }
}
