package com.aboutstays.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aboutstays.R;
import com.aboutstays.enums.CarType;
import com.aboutstays.enums.ServiceType;
import com.aboutstays.fragment.dialog.ConfirmDialogFragment;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.stayline.GeneralInfo;
import com.aboutstays.rest.request.BookingData;
import com.aboutstays.rest.request.GetAirportBookingData;
import com.aboutstays.rest.request.InitiateAirportPickupRequest;
import com.aboutstays.rest.response.AirportPickupId;
import com.aboutstays.rest.response.Car;
import com.aboutstays.rest.response.CarDetailsAndPrice;
import com.aboutstays.rest.response.Driver;
import com.aboutstays.rest.response.GetAirportServiceResponse;
import com.aboutstays.rest.response.InitiateAirportPickupResponse;
import com.aboutstays.rest.response.SupportedAirports;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.aboutstays.utitlity.DatePickerUtil;
import com.aboutstays.utitlity.TimePickerUtil;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.receivers.CartUpdateReceiver;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;


/**
 * Created by Neeraj on 22/12/16.
 */

public class AirportPickupActivity extends AppBaseActivity implements CustomListAdapterInterface,
        AdapterView.OnItemSelectedListener{

    private CustomListAdapter adapter;
    private Spinner spinnerAirports;
    private List<CarDetailsAndPrice> listCar = new ArrayList<>();
    private TextInputLayout tilAirport, tilCarType, tilAirline, tilFlightNumber, tilPickupDateAndTime, tilComment;
    private Button btnRequestPickup, btnCancel, btnModify;
    private EditText etPickupDateAndTime;
    private TextView tvSelectCarType, tvMsg, tvSelectRent, driverName, driverCarType, driverNumber, tvDateTitle, tvDropTime, tvAdd, tvContact;
    private RelativeLayout rlCarType, rlDate, rlHeader, rlDateTime, rlDropDateTime, rlCancelModify;
    private ImageView driverIcon;
    private LinearLayout llMsg;
    private View line3, line4, line5;
    private static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    private List<SupportedAirports> listSupportedAirports = new ArrayList<>();
    private Map<String, List<CarDetailsAndPrice>> mapAirportVsCarPriceDetails = new HashMap<>();
    private List<String> spinnerList = new ArrayList<>();
    private ArrayAdapter<String> spinnerAdapter;
    private long pickupTime;
    private Integer pickupCarType = 0;
    private GeneralServiceModel generalServiceModel;
    private Integer pickupStatus;
    private Double carprice;
    private double bookCarPrice;
    private boolean reqNow = false, initiate = false;
    private boolean isFromDrop = false;
    private String carDetail, driverMobileNumber, refId, airportPickupId, titleName, serviceId;
    public static final String AIPROT_PICKUP_DATE = "dd MMM ";
    public static final String AIPROT_PICKUP_DATE_FORMAT = "dd MMM hh:mm a";
    public static final String AIRPORT_DROP_DATE = "dd MMM yyyy";
    public static final String AIRPORT_DROP_DATE_AND_TIME = "dd MMM yyyy, hh:mm a";
    public static final String PICKUP_DATE_YEAR_FORMAT = "dd-MM-yyyy";
    private Calendar calendarInstance;
    public static final int MILISECONDS_IN_HOUR = 60 * 60 * 1000;
    private boolean isModifiable;
    private String hotelNumber;
    private int call_reciepent;
    private String deliveryDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airport_pickup);
        findViews();
        calendarInstance = Calendar.getInstance();
        calendarInstance.setTimeInMillis(System.currentTimeMillis()+(2*MILISECONDS_IN_HOUR));

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //refId = bundle.getString(AppConstants.BUNDLE_KEYS.REF_ID);
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null) {
                serviceId = generalServiceModel.getGeneralServiceData().getHsId();
                titleName = generalServiceModel.getGeneralServiceData().getName();
                Integer serviceType = generalServiceModel.getGeneralServiceData().getServiceType();

                if (serviceType == ServiceType.AIRPORT_DROP.getCode()) {
                    isFromDrop = true;
                }
            }
            if(generalServiceModel != null) {
                refId = generalServiceModel.getReferenceId();

                if(generalServiceModel.getHotelStaysList() != null) {
                    GeneralInfo generalInfo = generalServiceModel.getHotelStaysList().getGeneralInfo();
                    if(generalInfo != null) {
                        if(!TextUtils.isEmpty(generalInfo.getPhone1())) {
                            hotelNumber = generalInfo.getPhone1();
                        } else {
                            hotelNumber = generalInfo.getPhone2();
                        }
                    }
                }
            }


        }

        if (isFromDrop) {
            rlCancelModify.setVisibility(View.GONE);
            btnRequestPickup.setVisibility(View.VISIBLE);
            llMsg.setVisibility(View.GONE);
            setActivity();
        } else {
            rlCancelModify.setVisibility(View.VISIBLE);
            btnRequestPickup.setVisibility(View.GONE);
            llMsg.setVisibility(View.VISIBLE);
            tvMsg.setText(R.string.slect_pickup_atleast_2_from_now);
        }
        if (!TextUtils.isEmpty(titleName)) {
            initToolBar(titleName);
        }
        adapter = new CustomListAdapter(this, R.layout.row_dialog_row, listCar, this);
        initiateSpinner();
        setOnClickListener(R.id.tv_select_car_type, R.id.rl_date_time, R.id.lay_car_type, R.id.btn_request_pick_up, R.id.btn_modify, R.id.btn_cancel);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getSupportedAirports();
       // getBookingData();
    }

    private void setActivity() {
        tilAirline.setVisibility(View.GONE);
        tilFlightNumber.setVisibility(View.GONE);
        rlDateTime.setVisibility(View.GONE);
        line3.setVisibility(View.GONE);
        line4.setVisibility(View.GONE);
        line5.setVisibility(View.GONE);
        rlDropDateTime.setVisibility(View.VISIBLE);
        rlDropDateTime.setOnClickListener(this);
        tvAdd.setOnClickListener(this);
        tvDateTitle.setText("Date and Time");
    }

    private void getBookingData() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getAirportBookingData(refId);
        executeTask(AppConstants.TASK_CODES.GET_AIRPORT_BOOKING, httpParamObject);
    }

    private void findViews() {
        tvMsg = (TextView) findViewById(R.id.tv_msg);
        llMsg = (LinearLayout) findViewById(R.id.ll_msg);
        driverIcon = (ImageView) findViewById(R.id.iv_userPic);
        driverName = (TextView) findViewById(R.id.tv_user_name);
        driverCarType = (TextView) findViewById(R.id.tv_user_car_type);
        driverNumber = (TextView) findViewById(R.id.tv_user_number);
        rlHeader = (RelativeLayout) findViewById(R.id.rl_airport_pickup_header);
        tvContact = (TextView) findViewById(R.id.tv_contact);
        rlCancelModify = (RelativeLayout) findViewById(R.id.rl_edit_cancle_button);
        btnCancel = (Button) findViewById(R.id.btn_cancel);
        btnModify = (Button) findViewById(R.id.btn_modify);
        btnRequestPickup = (Button) findViewById(R.id.btn_request_pick_up);
        tvSelectCarType = (TextView) findViewById(R.id.tv_select_car_type);
        tvSelectRent = (TextView) findViewById(R.id.tv_car_rent);
        spinnerAirports = (Spinner) findViewById(R.id.spineer_airports);
        tilAirline = (TextInputLayout) findViewById(R.id.til_select_airline);
        tilFlightNumber = (TextInputLayout) findViewById(R.id.til_flight_number);
        tilComment = (TextInputLayout) findViewById(R.id.til_comment);
        etPickupDateAndTime = (EditText) findViewById(R.id.et_pick_up_time);
        rlCarType = (RelativeLayout) findViewById(R.id.lay_car_type);
        rlDate = (RelativeLayout) findViewById(R.id.rl_date_time);
        rlDateTime = (RelativeLayout) findViewById(R.id.rl_date_and_time);
        rlDropDateTime = (RelativeLayout) findViewById(R.id.rl_row_data);
        tvDateTitle = (TextView) findViewById(R.id.tv_title);
        tvDropTime = (TextView) findViewById(R.id.tv_date_time);
        tvAdd = (TextView) findViewById(R.id.tv_add);

        line3 = findViewById(R.id.line3);
        line4 = findViewById(R.id.line4);
        line5 = findViewById(R.id.line5);

    }

    private void initiateSpinner() {
        spinnerList.clear();
        spinnerList.add(getString(R.string.select_airport));
        spinnerAdapter = new ArrayAdapter<String>(this, R.layout.row_spinner, spinnerList);
        spinnerAdapter.setDropDownViewResource(R.layout.row_spinner_drop_down);
        spinnerAirports.setAdapter(spinnerAdapter);
        spinnerAirports.setOnItemSelectedListener(this);
    }

    private void getSupportedAirports() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getSupportedAirportList(serviceId);
        executeTask(AppConstants.TASK_CODES.GET_AIRPORT_SERVICE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        hideProgressBar();
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_AIRPORT_SERVICE:
                GetAirportServiceResponse serviceResponse = (GetAirportServiceResponse) response;
                if (serviceResponse != null && serviceResponse.getError() == false) {
                    listSupportedAirports = serviceResponse.getData().getSupportedAirportList();
                    if (CollectionUtils.isNotEmpty(listSupportedAirports)) {
                        for (SupportedAirports supportedAirports : listSupportedAirports) {
                            mapAirportVsCarPriceDetails.put(supportedAirports.getAirportName(), supportedAirports.getCarDetailsAndPriceList());
                            spinnerList.add(supportedAirports.getAirportName());
                        }
                        spinnerAdapter.notifyDataSetChanged();
                        getBookingData();
                    }
                } else {
                    showToast("No supported airports found!");
                }
                break;
            case AppConstants.TASK_CODES.INITIATE_AIRPORT_PICKUP:
                InitiateAirportPickupResponse pickupResponse = (InitiateAirportPickupResponse) response;
                if (pickupResponse != null && pickupResponse.getError() == false) {
                    AirportPickupId pickupId = pickupResponse.getData();
                    // save to preference;
                    initiate = true;
                    successDialog(initiate);
                }
                break;

            case AppConstants.TASK_CODES.UPDATE_AIRPORT_PICKUP:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false) {
                    // save to preference;
                    initiate = false;
                    successDialog(initiate);
                }
                break;

            case AppConstants.TASK_CODES.GET_AIRPORT_BOOKING:
                GetAirportBookingData getAirportBookingData = (GetAirportBookingData) response;
                if (getAirportBookingData != null && getAirportBookingData.getError() == false) {
                    setBookingData(getAirportBookingData);
                    rlCancelModify.setVisibility(View.VISIBLE);
                    btnRequestPickup.setVisibility(View.GONE);
                } else {
                    rlCancelModify.setVisibility(View.GONE);
                    btnRequestPickup.setVisibility(View.VISIBLE);
                }
                break;

            case AppConstants.TASK_CODES.CANCEL_AIRPORT_PICKUP:
                BaseApiResponse responseApi = (BaseApiResponse) response;
                if (responseApi != null && responseApi.getError() == false) {
                    setResult(RESULT_OK);
                    finish();
                }
                break;
        }
    }

    private void setBookingData(GetAirportBookingData getAirportBookingData) {
        if (getAirportBookingData != null) {
            BookingData getbookeddata = getAirportBookingData.getData();
            if (getbookeddata != null) {
                Integer pickupStatus = getbookeddata.getPickupStatus();
                bookCarPrice = getbookeddata.getPrice();
                tvSelectRent.setText("Rs. " + bookCarPrice + "");
                tilAirline.getEditText().setText(getbookeddata.getAirlineName());
                tilFlightNumber.getEditText().setText(getbookeddata.getFlightNumber());
                tilComment.getEditText().setText(getbookeddata.getComment());
                long pickupDateAndTime = getbookeddata.getPickupDateAndTime();
                pickupTime = pickupDateAndTime;
                Date datePick = new Date(pickupDateAndTime);
                deliveryDate = Util.format(datePick, Util.DELIVERY_TIME_DATE_PATTERN);
                String date = Util.getDate(pickupDateAndTime, "d MMM h:mm a");
                calendarInstance.setTimeInMillis(pickupDateAndTime);
                if (!TextUtils.isEmpty(date)) {
                    if (isFromDrop) {
                        String dropDate = Util.getDate(pickupDateAndTime, AIRPORT_DROP_DATE_AND_TIME);
                        tvDropTime.setText(dropDate);
                    } else {
                        String pickupDate = Util.getDate(pickupDateAndTime, AIPROT_PICKUP_DATE_FORMAT);
                        etPickupDateAndTime.setText(pickupDate);
                    }
                }
            }

            String airportName = getbookeddata.getAirportName();
            if (spinnerList.contains(airportName)) {
                int i = spinnerList.indexOf(airportName);
                spinnerAirports.setSelection(i, true);
            }

            pickupCarType = getbookeddata.getCarType();
            CarType carbyType = CarType.findCarbyType(pickupCarType);
            if (carbyType != null) {
                String carName = carbyType.getCarName();
                tvSelectCarType.setText(carName);
            }

            pickupCarType = getbookeddata.getCarType();
            airportPickupId = getbookeddata.getId();
            pickupStatus = getbookeddata.getPickupStatus();

            Car car = getbookeddata.getCar();
            if (car != null) {
                String brand = car.getBrand();
                String name = car.getName();
                String numberPlate = car.getNumberPlate();
                carDetail = numberPlate + " -" + brand;
            }

            Driver driver = getbookeddata.getDriver();
            if (driver != null) {
                if (!TextUtils.isEmpty(carDetail)) {
                    driverCarType.setText(carDetail);
                }
                rlHeader.setVisibility(View.VISIBLE);
                driverName.setText(driver.getName());
                driverMobileNumber = driver.getMobileNumber();
                driverNumber.setText(driverMobileNumber + "");
                String imageUrl = driver.getImageUrl();
                if (!TextUtils.isEmpty(imageUrl)) {
                    Picasso.with(this).load(imageUrl).into(driverIcon);
                }
                tvContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        call_reciepent = AppConstants.CALL.DRIVER;
                        checkPermission();
                    }
                });
            } else {
                rlHeader.setVisibility(View.GONE);
            }
            setEditableFalse(false);
        }
    }

    private void setEditableFalse(boolean editable) {
        tilAirline.getEditText().setEnabled(editable);
        tilFlightNumber.getEditText().setEnabled(editable);
        tilComment.getEditText().setEnabled(editable);
        rlDate.setEnabled(editable);
        rlCarType.setEnabled(editable);
        spinnerAirports.setEnabled(editable);
        rlDropDateTime.setEnabled(editable);
        tvAdd.setEnabled(editable);

    }

    private void checkPermission() {
        ActivityCompat.requestPermissions(AirportPickupActivity.this,
                new String[]{Manifest.permission.CALL_PHONE},
                simplifii.framework.utility.AppConstants.TASK_CODES.MY_PERMISSIONS_REQUEST_CALL);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case simplifii.framework.utility.AppConstants.TASK_CODES.MY_PERMISSIONS_REQUEST_CALL: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Util.callUs(this, driverMobileNumber);
                    callPermissionGranted();
                } else {
                    Toast.makeText(AirportPickupActivity.this, getString(R.string.permission_not_grant), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private void callPermissionGranted() {
        switch(call_reciepent) {
            case AppConstants.CALL.DRIVER :
                Util.callUs(this, driverMobileNumber);
                break;
            case AppConstants.CALL.HOTEL :
                Util.callUs(this, hotelNumber);
                break;
        }
    }

    private void setDropDateAndtime() {

        DatePickerUtil datePickerUtil = new DatePickerUtil(PICKUP_DATE_YEAR_FORMAT, calendarInstance, getFragmentManager(), getString(R.string.select_date), true, false, new DatePickerUtil.OnDateSelected() {
            @Override
            public void onDateSelected(Calendar calendar, String date) {
                TimePickerUtil timePickerUtil = new TimePickerUtil(AIRPORT_DROP_DATE_AND_TIME, calendar, getFragmentManager(), getString(R.string.select_time), new TimePickerUtil.OnTimeSelected() {
                    @Override
                    public void onTimeSelected(Calendar calendar, String date) {
                        calendarInstance= calendar;
                        pickupTime = calendar.getTimeInMillis() - 1000;
                        tvDropTime.setText(date);
                        Date time = calendar.getTime();
                        deliveryDate = Util.format(time, Util.DELIVERY_TIME_DATE_PATTERN);
                    }
                });

                long currentTime = System.currentTimeMillis();
                Calendar dateCalender = Calendar.getInstance();
                dateCalender.setTimeInMillis(currentTime);
                SimpleDateFormat sdf = new SimpleDateFormat(PICKUP_DATE_YEAR_FORMAT);
                String selectedDate = sdf.format(calendarInstance.getTime());
                String currentDate = sdf.format(dateCalender.getTime());
                if(selectedDate.equalsIgnoreCase(currentDate)) {
                    long finalTime = currentTime + (2 * MILISECONDS_IN_HOUR);
                    Calendar c = Calendar.getInstance();
                    c.setTimeInMillis(finalTime);
                    Timepoint timepoint = new Timepoint(c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), c.get(Calendar.SECOND));
                    timePickerUtil.show(timepoint);
                } else {
                    timePickerUtil.show();
                }


                //timePickerUtil.show();
            }

            @Override
            public void onCanceled() {
//                tvDropTime.setText("");
                pickupTime=0;
            }
        });
        datePickerUtil.show(Calendar.getInstance());
    }



    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.lay_car_type:
                if(spinnerAirports.getSelectedItemPosition()==0){
                    showToast("Please select an airport type");
                    return;
                }
                openCarTypeDialog();
                break;
            case R.id.rl_date_time:
                setDateAndTime();
                break;
            case R.id.rl_row_data:
                setReqNowFalse();
                setDropDateAndtime();
                break;
            case R.id.btn_request_pick_up:
//                if(validateRequestPickup() == false){
//                    showToast("Select a pickup time at least 2 hours from now");
//                    return;
//                }
                validateCheck();
                break;
            case R.id.btn_modify:
                if(validate2HourModifyCheck() == false){
                    errorDialog("MODIFY");
                    return;
                }

                if(isModifiable == true) {
                    validateCheck();
                }
                isModifiable = true;
                setEditableFalse(true);
                changeBottomButton();
                break;
            case R.id.btn_cancel:
                if(validate2HourModifyCheck() == false){
                    errorDialog("CANCEL");
                    return;
                } else {
                    ConfirmDialogFragment.show(getSupportFragmentManager(), "CANCEL?", "Do you really want to cancel this request?", "", "", new ConfirmDialogFragment.OnConfirmListener() {
                        @Override
                        public void confirm() {
                            String uosId = generalServiceModel.getUosId();
                            if (!TextUtils.isEmpty(uosId)) {
                                HttpParamObject httpParamObject = ApiRequestGenerator.cancelAirportPickup(uosId);
                                executeTask(AppConstants.TASK_CODES.CANCEL_AIRPORT_PICKUP, httpParamObject);
                            }
                        }
                    });
                }
                break;
            case R.id.tv_add:
                if (reqNow == false) {
                    reqNow = true;
                    tvAdd.setBackgroundResource(R.drawable.shape_button_selected);
                    tvAdd.setTextColor(Color.WHITE);
                    tvDropTime.setText("");
                    pickupTime = System.currentTimeMillis() + (2 * (MILISECONDS_IN_HOUR));
                    Date time = new Date(pickupTime);
                    deliveryDate = Util.format(time, Util.DELIVERY_TIME_DATE_PATTERN);
                    //tvAdd.setText(R.string.requested);
                } else {
                    setReqNowFalse();
                }
                break;
        }
    }

    private boolean validate2HourModifyCheck() {
        long twoHours = 2 * 60 * 60 * 1000;
        long effectiveTime = pickupTime - twoHours;

        if(System.currentTimeMillis() > effectiveTime) {
            return false;
        }
        return true;
    }

    private boolean validateRequestPickup() {
        long effectivieTime = System.currentTimeMillis() + ( 2 * MILISECONDS_IN_HOUR);
        if(pickupTime < effectivieTime) {
            return false;
        }
        return true;
    }

    private void changeBottomButton() {
        btnCancel.setVisibility(View.GONE);
        btnModify.setVisibility(View.GONE);
        btnRequestPickup.setVisibility(View.VISIBLE);
        btnRequestPickup.setText("Save Changes");
    }

    private void setReqNowFalse() {
        reqNow = false;
        tvAdd.setBackgroundResource(R.drawable.shape_button_email);
        tvAdd.setTextColor(getResourceColor(R.color.color_red_bg));
        tvAdd.setText(R.string.request_now);
        pickupTime = 0;
        tvDropTime.setText("");
        deliveryDate = "";
    }

    private void selectCurrentDateandTime() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time =&gt; " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat(AIRPORT_DROP_DATE_AND_TIME);
        String formattedDate = df.format(c.getTime());
        tvDropTime.setText(formattedDate);
        pickupTime = c.getTimeInMillis() - 1000;
        Date time = c.getTime();
        deliveryDate = Util.format(time, Util.DELIVERY_TIME_DATE_PATTERN);
    }

    private void validateCheck() {

        String airportName = (String) spinnerAirports.getSelectedItem();
        String airline = tilAirline.getEditText().getText().toString().trim();
        String flighNumber = tilFlightNumber.getEditText().getText().toString().trim();
        String comment = tilComment.getEditText().getText().toString().trim();

        if(spinnerAirports.getSelectedItemPosition() == 0) {
            showToast("Please select an airport name");
            return;
        }

        if (TextUtils.isEmpty(airportName)) {
            showToast("Please select an airport");
            return;
        }
        if (pickupCarType == null || pickupCarType == 0) {
            showToast("Please select a car type");
            return;
        }


        if (pickupTime == 0&&reqNow==false) {
            showToast("Please select a pickup time");
            return;
        }

        final InitiateAirportPickupRequest request = new InitiateAirportPickupRequest();
        if (generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null) {
            request.setGsId(generalServiceModel.getGeneralServiceData().getId());
        }
        if (generalServiceModel != null && generalServiceModel.getHotelStaysList() != null && generalServiceModel.getHotelStaysList().getStaysPojo() != null) {
            request.setHotelId(generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId());
            request.setStayId(generalServiceModel.getHotelStaysList().getStaysPojo().getStaysId());
        }
        UserSession session = UserSession.getSessionInstance();
        if (session != null) {
            request.setUserId(session.getUserId());
        }
        request.setAirlineName(airline);
        request.setAirportName(airportName);
        request.setPickupDateAndTime(pickupTime);
        request.setCarType(pickupCarType);
        request.setFlightNumber(flighNumber);
        request.setComment(comment);
        if (isFromDrop) {
            request.setDrop(true);
        }
        if (carprice != null) {
            request.setPrice(carprice);
        } else {
            request.setPrice(bookCarPrice);
        }

        if (reqNow == false && TextUtils.isEmpty(deliveryDate)){
            showToast(getString(R.string.please_select_time_first));
            return;
        } else {
            request.setDeliverTimeString(deliveryDate);
        }

        if (!TextUtils.isEmpty(refId)) {
            updateAirportPickup(request);
        } else {
            HttpParamObject httpParamObject = ApiRequestGenerator.requestAirportPickup(request);
            executeTask(AppConstants.TASK_CODES.INITIATE_AIRPORT_PICKUP, httpParamObject);
        }
    }

    private void updateAirportPickup(InitiateAirportPickupRequest request) {
        HttpParamObject httpParamObject = ApiRequestGenerator.updateAirportPickup(airportPickupId, request, pickupStatus);
        executeTask(AppConstants.TASK_CODES.UPDATE_AIRPORT_PICKUP, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

    private void successDialog(boolean initiate) {
        String title,message;
        if (initiate == true) {
            title=getString(R.string.request_initiated);
            message=getString(R.string.req_initiated);
        } else {
            title=getString(R.string.modify);
            message=getString(R.string.req_modify);
        }

        String updateTitle;
        if(isFromDrop) {
            updateTitle = getString(R.string.airport_drop);
        } else {
            updateTitle = getString(R.string.airport_pickup);
        }

        SuccessDialogFragment.show(getSupportFragmentManager(),
                updateTitle, title, message, "OK", new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                        if (isFromDrop) {
                            Intent intent = new Intent();
                            intent.putExtra(AppConstants.BUNDLE_KEYS.FROM_CHECKOUT, 3);
                            setResult(RESULT_OK, intent);
                        } else {
                            setResult(RESULT_OK);
                        }
                        finish();
                        if(isFromDrop){
                            CartUpdateReceiver.sendBroadcast(AirportPickupActivity.this,CartUpdateReceiver.ACTION_UPDATE_CHECKOUT);
                        }else {
                            CartUpdateReceiver.sendBroadcast(AirportPickupActivity.this,CartUpdateReceiver.ACTION_UPDATE_CHECKIN);
                        }
                    }
                }
        );
    }

    private void errorDialog(String message) {
        ConfirmDialogFragment.show(getSupportFragmentManager(),
                "UNABLE TO "+  message,
                getString(R.string.pickup_error_msg),
                getString(R.string.call),
                getString(R.string.close),
                new ConfirmDialogFragment.OnConfirmListener() {
                    @Override
                    public void confirm() {
                        call_reciepent = AppConstants.CALL.HOTEL;
                        checkPermission();
                    }
                });


    }

    private void setDateAndTime() {

        DatePickerUtil datePickerUtil = new DatePickerUtil(PICKUP_DATE_YEAR_FORMAT, calendarInstance, getFragmentManager(), getString(R.string.select_date), true, false, new DatePickerUtil.OnDateSelected() {
            @Override
            public void onDateSelected(Calendar calendar, final String date) {
                TimePickerUtil timePickerUtil = new TimePickerUtil(AIPROT_PICKUP_DATE_FORMAT, calendar, getFragmentManager(), getString(R.string.select_time), new TimePickerUtil.OnTimeSelected() {
                    @Override
                    public void onTimeSelected(Calendar calendar, String time) {
                        calendarInstance = calendar;
                        pickupTime = calendar.getTimeInMillis() - 1000;
                        etPickupDateAndTime.setText(time);
                        Date dateTime = calendar.getTime();
                        deliveryDate = Util.format(dateTime, Util.DELIVERY_TIME_DATE_PATTERN);
                    }
                });

                long currentTime = System.currentTimeMillis();
                Calendar dateCalender = Calendar.getInstance();
                dateCalender.setTimeInMillis(currentTime);
                SimpleDateFormat sdf = new SimpleDateFormat(PICKUP_DATE_YEAR_FORMAT);
                String selectedDate = sdf.format(calendarInstance.getTime());
                String currentDate = sdf.format(dateCalender.getTime());
                if(selectedDate.equalsIgnoreCase(currentDate)) {
                    long finalTime = currentTime + (2 * MILISECONDS_IN_HOUR);
                    Calendar c = Calendar.getInstance();
                    c.setTimeInMillis(finalTime);
                    Timepoint timepoint = new Timepoint(c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), c.get(Calendar.SECOND));
                    timePickerUtil.show(timepoint);
                } else {
                    timePickerUtil.show();
                }
            }

            @Override
            public void onCanceled() {
                etPickupDateAndTime.setText("");
            }
        });
        datePickerUtil.show(Calendar.getInstance());

    }

    private void verifyPickupTime(Calendar cal) {
       // String selectedDateAndTime = etPickupDateAndTime.getText().toString().trim();
       // long longSelectedTme = Util.convertStringToLong(PICKUP_DATE_YEAR_FORMAT, date);
        long longSelectedTme = cal.getTimeInMillis();
        long twoHoursTimeFromCurrentTime = Calendar.getInstance().getTimeInMillis();// + (2 * MILISECONDS_IN_HOUR);
        long twoHoursTime = 2 * MILISECONDS_IN_HOUR;
        long finalTime = twoHoursTimeFromCurrentTime + twoHoursTime;

        if(longSelectedTme <= finalTime) {
            showToast("Select a pickup time at least 2 hours from now");
            etPickupDateAndTime.setText("");
        }
    }

    private void openCarTypeDialog() {
        ListView listView;
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        /// dialog.setTitle("SELECT CAR TYPE");
        dialog.setContentView(R.layout.dialog_view_select_car);
        listView = (ListView) dialog.findViewById(R.id.list_view_dialog);
        listView.setAdapter(adapter);
        dialog.show();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CarDetailsAndPrice carDetails = listCar.get(position);

                if (carDetails != null && carDetails.getCarDetails() != null) {
                    tvSelectCarType.setText(carDetails.getCarDetails().getName());
                    carprice = carDetails.getPrice();
                    tvSelectRent.setText(carprice + "");
                    pickupCarType = carDetails.getCarDetails().getType();
                }
                dialog.dismiss();
            }
        });

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_dialog_row, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        CarDetailsAndPrice car = listCar.get(position);
        if (pickupCarType != 0) {
            if (pickupCarType == car.getCarDetails().getType()) {
                holder.ivSelect.setVisibility(View.VISIBLE);
            } else {
                holder.ivSelect.setVisibility(View.INVISIBLE);
            }
        }
        holder.tvCarType.setText(car.getCarDetails().getName());
        holder.tvMaxPeople.setText("upto " + car.getCarDetails().getPax() + " people");
        holder.tvRent.setText(getString(R.string.rupeesSymbol) + car.getPrice());
        if (car.getCarDetails() != null && car.getCarDetails().getImageUrl() != null) {
            Picasso.with(AirportPickupActivity.this).load(car.getCarDetails().getImageUrl()).placeholder(R.drawable.progress_animation).into(holder.ivCarIcon);
        }

        if(position == listCar.size() - 1) {
            holder.bottomLine.setVisibility(View.GONE);
        } else {
            holder.bottomLine.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(position == 0) {
            return;
        }
        String airportName = listSupportedAirports.get(position-1).getAirportName();
        listCar.clear();
        listCar.addAll(mapAirportVsCarPriceDetails.get(airportName));
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    class Holder {
        ImageView ivCarIcon, ivSelect;
        TextView tvCarType, tvMaxPeople, tvRent;
        View bottomLine;

        Holder(View view) {
            ivCarIcon = (ImageView) view.findViewById(R.id.iv_car);
            ivSelect = (ImageView) view.findViewById(R.id.iv_select_icon);
            tvCarType = (TextView) view.findViewById(R.id.tv_car_type);
            tvMaxPeople = (TextView) view.findViewById(R.id.tv_max_people);
            tvRent = (TextView) view.findViewById(R.id.tv_rent);
            bottomLine = (View) view.findViewById(R.id.view_pickup_line);
        }
    }
}
