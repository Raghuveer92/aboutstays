package com.aboutstays.activity;

import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.receiver.StayCartUpdateReceiver;
import com.cunoraz.gifview.library.GifView;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.Util;

/**
 * Created by aman on 17/01/17.
 */

public class AppBaseActivity extends BaseActivity implements StayCartUpdateReceiver.OnCartUpdateListener {

    private ViewGroup croutonView;
    private StayCartUpdateReceiver stayCartUpdateReceiver;
    private GifView gifLoader;
    private View loaderContainer;
    private TextView tvEmptyView;

    protected void initCroutonView(){
        setCroutonView(R.id.frame_crouton);
    }

    public void setCroutonView(int viewId){
        ViewGroup view = (ViewGroup) findViewById(viewId);
        this.croutonView = view;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPreExecute(int taskCode) {
        super.onPreExecute(taskCode);
        if (tvEmptyView != null) {
//            tvEmptyView.setVisibility(View.INVISIBLE);
            tvEmptyView.setText("");
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (tvEmptyView != null) {
//            tvEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.no_data_found));
        }
    }

    public void showCrouton(String msg){
        Configuration croutonConfiguration = new Configuration.Builder()
                .setDuration(4500).build();
        // Define custom styles for crouton
        Style style = new Style.Builder()
                .setBackgroundColorValue(Color.parseColor("#80000000"))
                .setGravity(Gravity.CENTER_HORIZONTAL)
                .setConfiguration(croutonConfiguration)
                .setHeight(150)
                .setTextColorValue(Color.parseColor("#ffffff")).build();
        // Display notice with custom style and configuration
        if(croutonView!=null){
            Crouton crouton = Crouton.makeText(AppBaseActivity.this, msg, style,croutonView);
            crouton.show();
        }else {
            Crouton.showText(AppBaseActivity.this, msg, style);
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        hideProgressBar();
        if(re!=null){
            switch (re.getHttpStatusCode()){
                case 401:
                    if(!"Token invalid".equalsIgnoreCase(re.getMessage())){
                        showCrouton(re.getMessage());
                        break;
                    }//Else logout as token is invalid
                case 500:
                    showCrouton("Session expired! Please login again to continue.");
                    break;
                case 403:
                case 400:
                    showCrouton(re.getMessage());
                    break;
                default:
                    showCrouton("Internal Server Error!");
                    break;
            }
        }
    }

    protected void registerCartReceiver(){
        stayCartUpdateReceiver=new StayCartUpdateReceiver(this);
        IntentFilter intentFilter=new IntentFilter();
        intentFilter.addAction(StayCartUpdateReceiver.ACTION_UPDATE_CART);
        intentFilter.addAction(StayCartUpdateReceiver.ACTION_UPDATE_CLEAR);
        registerReceiver(stayCartUpdateReceiver,intentFilter);
    }

    @Override
    public void onCartUpdate() {

    }

    @Override
    public void onCartClear() {

    }

    @Override
    public void onDestroy() {
        if(stayCartUpdateReceiver!=null){
            unregisterReceiver(stayCartUpdateReceiver);
        }
        super.onDestroy();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        LayoutInflater layoutInflater = getLayoutInflater();
        View rootView = layoutInflater.inflate(R.layout.root_layout, null);
        View childView = layoutInflater.inflate(layoutResID, null);
        FrameLayout frameLayout= (FrameLayout) rootView.findViewById(R.id.lay_container);
        loaderContainer= rootView.findViewById(R.id.lay_frame_loader);
        frameLayout.addView(childView);
        setContentView(rootView);
        tvEmptyView = (TextView) findViewById(R.id.tv_empty_view);
    }

    @Override
    public void showProgressDialog() {
        gifLoader = (GifView)findViewById(R.id.gif);
        gifLoader.setGifResource(R.drawable.ripple);
        loaderContainer.setVisibility(View.VISIBLE);
//        loaderContainer.setOnClickListener(this);
        gifLoader.play();
    }

    @Override
    public void hideProgressBar() {
        if(gifLoader!=null){
            loaderContainer.setVisibility(View.GONE);
        }
    }
}
