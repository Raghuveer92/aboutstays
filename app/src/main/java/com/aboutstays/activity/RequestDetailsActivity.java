package com.aboutstays.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.model.feedback.FeedbackData;
import com.aboutstays.model.feedback.GetServiceFeedbackResponse;
import com.aboutstays.fragment.dialog.ConfirmDialogFragment;
import com.aboutstays.rest.response.DashBoardDetailData;
import com.aboutstays.rest.response.DashboardOrderDetailsApiResponce;
import com.aboutstays.rest.response.GeneralDashboardItem;
import com.aboutstays.rest.response.ListGeneralItemDetail;
import com.aboutstays.rest.response.RowDashboard;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.receivers.CartUpdateReceiver;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

public class RequestDetailsActivity extends AppBaseActivity {
    private Button btnSubmit, btnCancel, bbtnEdit;
    private LinearLayout layContainer;
    private TextView tvQuantity;
    //    private GeneralDashboardItem generalDashboardItem;
    private TextView tvReqDate, tvLastUpdate, tvReqType, tvReqStatus, tvServiceCharge, tvTotalCost;
    //    private GeneralDashboardItem generalDashboardItem;
    private TextView tvScheduledTime, tvItemOrder;
    private TextView tvScheduledTimeValue;
    private View viewBelowSchedule, viewServiceChargeDoted, viewReqDetail;
    private LinearLayout llEditCancle, llReqItemOrder;
    private String uosId;
    private int reqFrom;
    private DashBoardDetailData orderData;
    private List<ListGeneralItemDetail> listGeneralItemDetails;
    private boolean isEditable;
    private boolean isUpdated;
    private View viewReqStatus;
    private TextView tvTitle;
    private TextView tvRate;
    private List<RowDashboard> rows;
    private LinearLayout llTotalCost;
    private GeneralDashboardItem generalDashboardItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_details);
        initToolBar("Request Details");
        findViews();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            reqFrom = bundle.getInt(AppConstants.BUNDLE_KEYS.REQUEST_FROM);
            generalDashboardItem = (GeneralDashboardItem) bundle.getSerializable(AppConstants.BUNDLE_KEYS.GENERAL_DASHBOARD_ITEM);
            if (generalDashboardItem != null) {
                uosId = generalDashboardItem.getUosId();
            }
        }

        btnCancel = (Button) findViewById(R.id.btn_cancel_order);
        bbtnEdit = (Button) findViewById(R.id.btn_edit_order);
        llEditCancle = (LinearLayout) findViewById(R.id.rl_edit_cancle_button);
        btnSubmit = (Button) findViewById(R.id.btn_submit_feedback);

        tvScheduledTime = (TextView) findViewById(R.id.tv_schduled_time);
        tvScheduledTimeValue = (TextView) findViewById(R.id.tv_schduled_time_value);
//        viewBelowSchedule = (View) findViewById(R.id.view_scheduled_time);
        viewServiceChargeDoted = (View) findViewById(R.id.view_service_charge);
        viewReqDetail = (View) findViewById(R.id.view_requesDetails2);
        setButton(reqFrom);

        setOnClickListener(R.id.btn_submit_feedback, R.id.btn_cancel_order, R.id.btn_edit_order);
        getOrderRequestDetailData();

    }

    private void setButton(int reqFrom) {
        if (reqFrom == 1) {
            btnSubmit.setVisibility(View.GONE);
            llReqItemOrder.setVisibility(View.VISIBLE);
            tvItemOrder.setVisibility(View.GONE);
            llEditCancle.setVisibility(View.VISIBLE);
            tvScheduledTimeValue.setVisibility(View.VISIBLE);
            tvScheduledTime.setVisibility(View.VISIBLE);
//            viewBelowSchedule.setVisibility(View.INVISIBLE);
            viewServiceChargeDoted.setVisibility(View.VISIBLE);
            viewReqDetail.setVisibility(View.GONE);
        } else if (reqFrom == 2) {
            llEditCancle.setVisibility(View.VISIBLE);
            btnSubmit.setVisibility(View.GONE);
            llReqItemOrder.setVisibility(View.VISIBLE);
            tvItemOrder.setVisibility(View.GONE);
            tvScheduledTimeValue.setVisibility(View.VISIBLE);
            tvScheduledTime.setVisibility(View.VISIBLE);
//            viewBelowSchedule.setVisibility(View.INVISIBLE);
            viewServiceChargeDoted.setVisibility(View.VISIBLE);
            viewReqDetail.setVisibility(View.GONE);
        } else {
            llEditCancle.setVisibility(View.GONE);
            if (generalDashboardItem.getStatus().equals("COMPLETED")) {
                btnSubmit.setVisibility(View.VISIBLE);
            } else {
                btnSubmit.setVisibility(View.GONE);
            }
            llReqItemOrder.setVisibility(View.VISIBLE);
            viewReqStatus.setVisibility(View.INVISIBLE);
            tvItemOrder.setVisibility(View.GONE);
            tvScheduledTimeValue.setVisibility(View.GONE);
            tvScheduledTime.setVisibility(View.GONE);
//            viewBelowSchedule.setVisibility(View.GONE);
            viewServiceChargeDoted.setVisibility(View.VISIBLE);
            viewReqDetail.setVisibility(View.GONE);
        }
    }

    private void getOrderRequestDetailData() {
        if (!TextUtils.isEmpty(uosId)) {
            HttpParamObject httpParamObject = ApiRequestGenerator.getOrderRequestData(uosId);
            executeTask(AppConstants.TASK_CODES.DASHBOARD_ORDER_DETAILS, httpParamObject);
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

    private void findViews() {
        llTotalCost = (LinearLayout) findViewById(R.id.ll_total_costs);
        layContainer = (LinearLayout) findViewById(R.id.ll_container);
        tvReqDate = (TextView) findViewById(R.id.tv_requested_date_data);
        tvLastUpdate = (TextView) findViewById(R.id.tv_requested_lastUpdated_data);
        tvReqType = (TextView) findViewById(R.id.tv_requested_type_data);
        tvReqStatus = (TextView) findViewById(R.id.tv_requested_status_data);
        tvServiceCharge = (TextView) findViewById(R.id.tv_service_charge);
        tvTotalCost = (TextView) findViewById(R.id.tv_total_amount);
        tvTitle = (TextView) findViewById(R.id.tv_item_title);
        tvQuantity = (TextView) findViewById(R.id.tv_quantity);
        tvRate = (TextView) findViewById(R.id.tv_rate);
        llReqItemOrder = (LinearLayout) findViewById(R.id.ll_requested_items_order);
        tvItemOrder = (TextView) findViewById(R.id.tv_item_order);
        viewReqStatus = findViewById(R.id.view_request_status);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.DASHBOARD_ORDER_DETAILS:
                DashboardOrderDetailsApiResponce apiResponce = (DashboardOrderDetailsApiResponce) response;
                if (apiResponce != null && apiResponce.getError() == false) {
                    orderData = apiResponce.getData();
                    if (orderData != null) {
                        setData(orderData);
                    }
                }
                break;
            case AppConstants.TASK_CODES.CANCEL_ORDER:
                CartUpdateReceiver.sendBroadcast(this, CartUpdateReceiver.ACTION_UPDATE_REQUEST);
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false) {
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            case AppConstants.TASK_CODES.GET_FEEDBACK:
                GetServiceFeedbackResponse feedbackResponse = (GetServiceFeedbackResponse) response;
                if (feedbackResponse != null && feedbackResponse.getError() == false) {
                    FeedbackData feedbackData = feedbackResponse.getData();
                    if (feedbackData != null) {
                        btnSubmit.setText("View Feedback");
                    }
                }
                break;
        }
    }

    private void setData(DashBoardDetailData data) {
        isEditable = data.isEditable();
        long requestedTime = data.getRequestedTime();
        long lastUpdatedTime = data.getLastUpdatedTime();
        long scheduledTime = data.getScheduledTime();

        String reqDate = Util.getDate(requestedTime, "hh:mm a, dd MMM");
        String updateDate = Util.getDate(lastUpdatedTime, "hh:mm a, dd MMM");

        if (scheduledTime >= 0) {
            String scheduledDate = Util.getDate(scheduledTime, "hh:mm a, dd MMM");
            tvScheduledTimeValue.setText(scheduledDate);
        }

        if (!TextUtils.isEmpty(reqDate)) {
            tvReqDate.setText(reqDate);
        }
        if (!TextUtils.isEmpty(updateDate)) {
            tvLastUpdate.setText(updateDate);
        }
        tvReqType.setText(data.getRequestType());

        if (data.getTableEntries() != null) {
            if (data.getTableEntries().getTitles() != null) {
                String value1 = data.getTableEntries().getTitles().getValue1();
                String value2 = data.getTableEntries().getTitles().getValue2();
                String value3 = data.getTableEntries().getTitles().getValue3();

                if (TextUtils.isEmpty(value1)) {
                    tvTitle.setText("");
                } else {
                    tvTitle.setText(value1);
                }
                if (TextUtils.isEmpty(value2)) {
                    tvQuantity.setText("");
                } else {
                    tvQuantity.setText(value2);
                }
                if (TextUtils.isEmpty(value3)) {
                    tvRate.setText("");
                } else {
                    tvRate.setText(value3);
                }
            }
        }

//        if(data.getRequestType().equalsIgnoreCase("Reservations")) {
//            tvQuantity.setText("Pax");
//        }

        tvReqStatus.setText(data.getRequestStatus());
        tvServiceCharge.setText("\u20B9 " + data.getServiceCharge());
        String requestStatus = data.getRequestStatus();
        if (requestStatus.equalsIgnoreCase("CANCELLED") && ((reqFrom == 1) || (reqFrom == 2))) {
            setButton(3);
        }
        tvReqStatus.setText(requestStatus);
        tvServiceCharge.setText("\u20B9 " + data.getServiceCharge());

        listGeneralItemDetails = data.getListGeneralItemDetails();

        if (data.getTableEntries() != null && data.getTableEntries().getRows() != null) {
            rows = data.getTableEntries().getRows();
        }
        layContainer.removeAllViews();
        if (CollectionUtils.isNotEmpty(rows)) {
            for (int i = 0; i < rows.size(); i++) {
                LayoutInflater inflater = getLayoutInflater();
                View view = inflater.inflate(R.layout.row_item, null);
                TextView tvItemName = (TextView) view.findViewById(R.id.tv_item);
                TextView tvItemQty = (TextView) view.findViewById(R.id.tv_item_count);
                TextView tvItemPrice = (TextView) view.findViewById(R.id.tv_item_price);
                RowDashboard listGeneralItemDetail = rows.get(i);
                String value1 = listGeneralItemDetail.getValue1();
                String value2 = listGeneralItemDetail.getValue2();
                String value3 = listGeneralItemDetail.getValue3();

                tvItemName.setText(value1);
                tvItemQty.setText(value2);
                tvItemPrice.setText(value3);
//                if (reqFrom == 1 || reqFrom == 2){
//                    tvItemQty.setVisibility(View.INVISIBLE);
//                } else {
//                    tvItemQty.setVisibility(View.VISIBLE);
//                }
//                double price = listGeneralItemDetail.getPrice() * listGeneralItemDetail.getQuantity();
//                if (price > 0) {
//                    String decimal = Util.formatDecimal(price, "0.0#");
//                    tvItemPrice.setText("\u20B9 " + decimal);
//                } else {
//                    tvItemPrice.setText("");
//                }

                layContainer.addView(view);
            }
        }
        Double total = data.getTotal();
        if (total != null && total > 0) {
            String decimal = Util.formatDecimal(total, "0.0#");
            tvTotalCost.setText("\u20B9 " + decimal);
        } else {
            tvTotalCost.setText("");
            llTotalCost.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAllFeedback();
    }


    private void getAllFeedback() {
        if (!TextUtils.isEmpty(uosId)) {
            HttpParamObject httpParamObject = ApiRequestGenerator.getServiceFeedback(uosId);
            executeTask(AppConstants.TASK_CODES.GET_FEEDBACK, httpParamObject);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit_feedback:
                Bundle b = new Bundle();
                b.putString(AppConstants.BUNDLE_KEYS.UOSID, uosId);
                startNextActivity(b, FeedbackActivity.class);
                break;
            case R.id.btn_edit_order:
                if (isEditable == false) {
                    errorDialoge("MODIFY");
                    return;
                }
                if (validateOrderTime() == true) {
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.BUNDLE_KEYS.UOSID, uosId);
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, orderData);
                    FragmentContainerActivity.startActivityForResult(this, AppConstants.FRAGMENT_TYPE.EDIT_ORDER_FRAGMENT, bundle, AppConstants.REQUEST_CODE.EDIT_ORDER);
                } else {
                    errorDialoge("MODIFY");
                    return;
                }
                break;
            case R.id.btn_cancel_order:
                if (validateOrderTime() == true) {
                    ConfirmDialogFragment.show(getSupportFragmentManager(), "CANCEL?", "Do you really want to cancel this order?", "", "", new ConfirmDialogFragment.OnConfirmListener() {
                        @Override
                        public void confirm() {
                            HttpParamObject httpParamObject = ApiRequestGenerator.cancelOrder(uosId);
                            executeTask(AppConstants.TASK_CODES.CANCEL_ORDER, httpParamObject);
                        }
                    });
                } else {
                    errorDialoge("CANCEL");
                    return;
                }
                break;
        }
    }

    private boolean validateOrderTime() {
        long orderedTime = generalDashboardItem.getOrderedTime();
        long fiveMinutes = 5 * 60 * 1000;
        long effectiveTime = orderedTime + fiveMinutes;

        if(System.currentTimeMillis() > effectiveTime) {
            return false;
        }
        return true;
    }

    private void errorDialoge(String message) {
        SuccessDialogFragment.show(getSupportFragmentManager(),
                "UNABLE TO " + message,
                "",
                "You cannot "+message+" this order.",
                getString(R.string.close),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {

                    }
                }
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppConstants.REQUEST_CODE.EDIT_ORDER:
                    isUpdated = true;
                    getOrderRequestDetailData();
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isUpdated) {
            CartUpdateReceiver.sendBroadcast(this, CartUpdateReceiver.ACTION_UPDATE_REQUEST);
        }
    }
}
