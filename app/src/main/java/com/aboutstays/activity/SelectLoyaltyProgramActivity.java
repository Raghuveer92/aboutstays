package com.aboutstays.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.MultipleBookingModel;
import com.aboutstays.model.ProgramModel;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;

public class SelectLoyaltyProgramActivity extends AppBaseActivity implements CustomListAdapterInterface {
    private ListView listViewProgram;
    List<ProgramModel> programList = new ArrayList<>();
    private CustomListAdapter customListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_localty_program);
        initToolBar(getString(R.string.select_loyalty_program));
        listViewProgram = (ListView) findViewById(R.id.list_view_program);
        customListAdapter = new CustomListAdapter(this, R.layout.row_loyalty_program, programList, this);
        listViewProgram.setAdapter(customListAdapter);
        setDatInList();
    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.left_arrow;
    }

    private void setDatInList() {
        for (int x = 0; x < 6; x++) {
            ProgramModel programModel = new ProgramModel();
            programModel.setTitle(getString(R.string.loyalty_program) + x);
            programModel.setSubtitle(getString(R.string.single_line_description));
            programList.add(programModel);
        }
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final ProgramModel program = programList.get(position);
        holder.title.setText(program.getTitle());
        holder.subTitle.setText(program.getSubtitle());
        return convertView;
    }

    class Holder {
        TextView title,subTitle;
        public Holder(View view) {
            title = (TextView) view.findViewById(R.id.loyalty_program_title);
            subTitle = (TextView) view.findViewById(R.id.loyalty_program_subtitle);
        }
    }
}
