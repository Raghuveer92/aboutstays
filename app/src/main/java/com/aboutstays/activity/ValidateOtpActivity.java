package com.aboutstays.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.rest.response.GenerateOtpApi;
import com.aboutstays.rest.response.OtpData;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.mukesh.OtpView;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;

public class ValidateOtpActivity extends AppBaseActivity {
    private OtpView otpView;
    private Button btnValidateOtp;
    private TextView tvResendOtp, tv_error, tvTitleOtp;
    private String value;
    private int validateBtnTarget;
    private static final int OTP_MAX_LENGTH = 4;
    private static final int MASKED_NUMBER_DIGIT_LENGTH = 4;
    private Bundle signupBundle;
    private String titleOtp;
    private String typeReq;
    private boolean debugMode;
    private String generatedOtp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_otp);
        initCroutonView();
        initToolBar("Validate OTP");

        findViews();

        signupBundle = getIntent().getExtras();
        if (signupBundle != null){
            value = signupBundle.getString(AppConstants.BUNDLE_KEYS.NUMBER);
            OtpData otpData = (OtpData) signupBundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (otpData != null){
                debugMode = otpData.getDebugMode();
                generatedOtp = otpData.getOtp();
                if (debugMode){
                    showToast(generatedOtp);
                }
            }
        }
        if(value.matches("\\d+(?:\\.\\d+)?")){
            typeReq = "2";
            titleOtp = getString(R.string.otp_number_text)+" " + value;
        }
        else{
            typeReq = "1";
            titleOtp = getString(R.string.otp_email_text)+" " + value;
        }

        tvTitleOtp.setText(titleOtp);


        validateBtnTarget = signupBundle.getInt(AppConstants.BUNDLE_KEYS.BUTTON_TARGET);
//        value = getIntent().getExtras().getString(AppConstants.BUNDLE_KEYS.NUMBER);
//        validateBtnTarget = getIntent().getExtras().getInt(AppConstants.BUNDLE_KEYS.BUTTON_TARGET);

        setOnClickListener(R.id.btn_validate_otp, R.id.tv_resend_otp);
    }

    private void findViews() {
        tvTitleOtp = (TextView) findViewById(R.id.tv_title_otp);
        otpView = (OtpView) findViewById(R.id.otp_view);
        btnValidateOtp = (Button) findViewById(R.id.btn_validate_otp);
        tvResendOtp = (TextView) findViewById(R.id.tv_resend_otp);
        tv_error = (TextView) findViewById(R.id.tv_errorMsg);
        tv_error.setVisibility(View.GONE);
    }


    private String getOtpNumber(){
        if(value.length() > MASKED_NUMBER_DIGIT_LENGTH){
            String titleNumber = "X";
            int numLength = value.length() - MASKED_NUMBER_DIGIT_LENGTH;
            for(int i = 0; i < numLength - 1; i++){
                titleNumber += "X";
            }
            titleNumber += value.substring(numLength);
            return titleNumber;
        }
        return value;
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
//        fetchOtp();
    }

    private void fetchOtp() {
        HttpParamObject httpParamObject = ApiRequestGenerator.generateOtp(typeReq, value);
        executeTask(AppConstants.TASK_CODES.GENERATE_OTP, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_validate_otp:
                checkOtpValidation();
                break;
            case R.id.tv_resend_otp:
                fetchOtp();
                break;
        }
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch(taskCode){
            case AppConstants.TASK_CODES.GENERATE_OTP:
                GenerateOtpApi otpApi = (GenerateOtpApi) response;
                if (otpApi != null && ! otpApi.getError()){
                    OtpData data = otpApi.getData();
                    if (data != null){
                        generatedOtp = data.getOtp();
                        openDialog("RESEND OTP", titleOtp, "OK", AppConstants.ACTIVITY_CONSTANTS.RESEND_DIALOG);
                    } else {
                        showToast(otpApi.getMessage());
                    }
                }
            break;
            case AppConstants.TASK_CODES.VALIDATE_OTP:
                BaseApiResponse validateResponse = (BaseApiResponse) response;
                if(validateResponse != null){
                    if(validateResponse.getError() == false){
                        openDialog("SIGNUP SUCCESS", "Your account has been created successfully", "OK", AppConstants.ACTIVITY_CONSTANTS.SUCCESS_DIALOG);
                    } else {
                        showCrouton(validateResponse.getMessage());
                    }
                }
            break;
            case AppConstants.TASK_CODES.SIGNUP:
                BaseApiResponse signupResponse = (BaseApiResponse) response;
                if(signupResponse != null){
                    if(signupResponse.getError() == false){
                        openDialog("SIGNUP SUCCESS", "Your account has been created successfully", "OK", AppConstants.ACTIVITY_CONSTANTS.SUCCESS_DIALOG);
                    } else{
                        showCrouton(signupResponse.getMessage());
                    }
                }
            break;
        }
    }

    private void checkOtpValidation() {
        int length = otpView.getOTP().toString().length();
        String enteredOtp = otpView.getOTP();
        if (length < OTP_MAX_LENGTH){
            tv_error.setVisibility(View.VISIBLE);
            tv_error.setText("OTP should be of 4 characters");
        } else if(enteredOtp.equals(generatedOtp)){
            tv_error.setVisibility(View.GONE);
            processValidateOtp();
        } else{
            tv_error.setVisibility(View.VISIBLE);
            tv_error.setText("OTP does not match, Please enter a valid otp");
        }
    }

    private void processValidateOtp() {
        switch(validateBtnTarget){
            case AppConstants.ACTIVITY_CONSTANTS.TARGET_SIGNUP_SUCCESS:
                //updateOtpValidated();
                signupUser();
            break;
            case AppConstants.ACTIVITY_CONSTANTS.TARGET_CHANGE_PASSWORD:
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.BUNDLE_KEYS.NUMBER, value);
                bundle.putString(AppConstants.BUNDLE_KEYS.TYPE, typeReq);
                startNextActivity(bundle, ChangePasswordActivity.class);
            break;
        }
    }
    
    private void signupUser(){
        HttpParamObject httpParamObject = ApiRequestGenerator.signup(signupBundle, true);
        executeTask(AppConstants.TASK_CODES.SIGNUP, httpParamObject);
    }

    private void openDialog(String titleText, String msg, String btnText, final int which) {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_success);

        TextView tvTitle = (TextView) dialog.findViewById(R.id.dialog_title);
        TextView tvMsg = (TextView) dialog.findViewById(R.id.dialog_msg);
        Button btnOk = (Button) dialog.findViewById(R.id.btn_ok);
        TextView tvSubTitle = (TextView) dialog.findViewById(R.id.dialog_sub_title);

        tvTitle.setText(titleText);
        tvSubTitle.setText("");
        tvMsg.setText(msg);
        btnOk.setText(btnText);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(which){
                    case AppConstants.ACTIVITY_CONSTANTS.SUCCESS_DIALOG:
                        Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        //startNextActivity(DrawerActivity.class);
                    break;
                    case AppConstants.ACTIVITY_CONSTANTS.RESEND_DIALOG:
                        dialog.dismiss();
                }
            }
        });
        dialog.show();
    }
}
