package com.aboutstays.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.Program;
import com.aboutstays.model.ProgramModel;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;

public class LoyaltyProgramActivity extends AppBaseActivity implements CustomListAdapterInterface {
    ListView listLoyalty;
    List<Program> programList = new ArrayList<>();
    private CustomListAdapter customListAdapter;
    private Button btnAddProgram;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loyalty_program);
        initToolBar(getString(R.string.loyalty_program));
        btnAddProgram = (Button) findViewById(R.id.btn_add_loyalty_program);
        listLoyalty = (ListView) findViewById(R.id.lv_loyalty_program);
        customListAdapter = new CustomListAdapter(this, R.layout.row_loyalty_programs, programList, this);
        listLoyalty.setAdapter(customListAdapter);
        setDataInList();
        setOnClickListener(R.id.btn_add_loyalty_program);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_add_loyalty_program:
//                startNextActivity(SelectLoyaltyProgramActivity.class);
                break;
        }
    }

    private void setDataInList() {
        for (int x = 0; x < 3; x++) {
            Program programs = new Program();
            programs.setTitle(getString(R.string.loyalty_program) + x);
            programs.setHotelType(getString(R.string.silver));
            programs.setPoints(getString(R.string._112_points));
            programList.add(programs);
        }
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final Program program = programList.get(position);
        holder.title.setText(program.getTitle());
        holder.type.setText(program.getHotelType());
        holder.points.setText(program.getPoints());
        return convertView;
    }

    class Holder {
        TextView title, type,points;

        public Holder(View view) {
            title = (TextView) view.findViewById(R.id.tv_program_title);
            type = (TextView) view.findViewById(R.id.tv_grade_mark);
            points = (TextView) view.findViewById(R.id.tv_points);

        }
    }
}