package com.aboutstays.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.aboutstays.R;
import com.aboutstays.fragment.BellBoyFragment;
import com.aboutstays.fragment.CityGuideFragment;
import com.aboutstays.fragment.CityGuideItemDetailFragment;
import com.aboutstays.fragment.CityGuideItemsFragment;
import com.aboutstays.fragment.CommuteFragment;
import com.aboutstays.fragment.ComplementaryServicesFragment;
import com.aboutstays.fragment.FeedbackAndSuggestionsFragment;
import com.aboutstays.fragment.FeedbackAndSuggestionsRowFragment;
import com.aboutstays.fragment.FindHotelRetrieveFragment;
import com.aboutstays.fragment.FragmentAllChannels;
import com.aboutstays.fragment.FragmentEditOrder;
import com.aboutstays.fragment.FragmentEntertainmentDetails;
import com.aboutstays.fragment.FragmentMessages;
import com.aboutstays.fragment.FragmentTvEntertainment;
import com.aboutstays.fragment.GetDirectionFragment;
import com.aboutstays.fragment.InternetOrderFragment;
import com.aboutstays.fragment.LateCheckoutFragment;
import com.aboutstays.fragment.ReservationOrderFragment;
import com.aboutstays.fragment.FragmentImageView;
import com.aboutstays.fragment.FragmentPackageDetail;
import com.aboutstays.fragment.FragmentReservationPackages;
import com.aboutstays.fragment.FragmentRestaurantsList;
import com.aboutstays.fragment.FragmentSubImages;
import com.aboutstays.fragment.HouseRulesDescriptionFragment;
import com.aboutstays.fragment.HouseRulesFragment;
import com.aboutstays.fragment.MaintenanceDetailFragment;
import com.aboutstays.fragment.MaintenanceFragment;
import com.aboutstays.fragment.MaintenanceItemFragment;
import com.aboutstays.fragment.ReservationFragment;
import com.aboutstays.fragment.ReservatonSubItemFragment;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

/**
 * Created by aman on 26/03/17.
 */

public class FragmentContainerActivity extends AppBaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);
        if(getIntent() != null){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame, getFragment(getIntent().getIntExtra(AppConstants.BUNDLE_KEYS.FRAGMENT_TYPE,0)))
                    .commit();

        }

    }

    private Fragment getFragment(int fragmentType){
        Fragment fragment = null;
        switch (fragmentType){
            case AppConstants.FRAGMENT_TYPE.CITY_GUIDE_FRAGMENT:
                fragment = new CityGuideFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.CITY_GUIDE_ITEM_FRAGMENT:
                fragment = new CityGuideItemsFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.CITY_GUIDE_ITEM_DETAIL_FRAGMENT:
                fragment = new CityGuideItemDetailFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.HOUSE_RULES_DESCRIPTION:
                fragment = new HouseRulesDescriptionFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.HOUSE_RULES:
                fragment = new HouseRulesFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.SUB_IMAGE_FRAGMENT:
                fragment = new FragmentSubImages();
                break;
            case AppConstants.FRAGMENT_TYPE.IMAGE_FRAGMENT:
                fragment = new FragmentImageView();
                break;
            case AppConstants.FRAGMENT_TYPE.EDIT_ORDER_FRAGMENT:
                fragment = new FragmentEditOrder();
                break;
            case AppConstants.FRAGMENT_TYPE.COMMUTE_FRAGMENT:
                fragment = new CommuteFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.BELL_BOY_FRAGMENT:
                fragment = new BellBoyFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.MAINTENANCE_FRAGMENT:
                fragment = new MaintenanceFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.MAINTENANCE_ITEM_FRAGMENT:
                fragment = new MaintenanceItemFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.MAINTENANCE_DETAIL_FRAGMENT:
                fragment = new MaintenanceDetailFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.RESERVATION_FRAGMENT:
                fragment = new ReservationFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.RESERVATION_SUBITEM_FRAGMENT:
                fragment = new ReservatonSubItemFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.FRAGMENT_RESERVATION_PACKAGE:
                fragment = new FragmentReservationPackages();
                break;
            case AppConstants.FRAGMENT_TYPE.FRAGMENT_RESERVATION_PACKAGE_DETAIL:
                fragment = new FragmentPackageDetail();
                break;
            case AppConstants.FRAGMENT_TYPE.RESTAURANTS_LIST_FRAGMENT:
                fragment = new FragmentRestaurantsList();
                break;
            case AppConstants.FRAGMENT_TYPE.RESERVATION_ORDER_FRAGMENT:
                fragment = new ReservationOrderFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.LATE_CHECKOUT:
                fragment = new LateCheckoutFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.TV_FRAGMENT:
                fragment = new FragmentTvEntertainment();
                break;
            case AppConstants.FRAGMENT_TYPE.FRAGMENT_ALL_CHANNEL:
                fragment = new FragmentAllChannels();
                break;
            case AppConstants.FRAGMENT_TYPE.FRAGMENT_ENTERTAINMENT_DETAILS:
                fragment = new FragmentEntertainmentDetails();
                break;
            case AppConstants.FRAGMENT_TYPE.FEEDBACK_AND_SUGGESTIONS:
                fragment = new FeedbackAndSuggestionsFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.COMPLEMENTARY_SERVICE:
                fragment = new ComplementaryServicesFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.INTERNET_ORDER_DETAILS:
                fragment = new InternetOrderFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.MESSAGE:
                fragment = new FragmentMessages();
                break;
            case AppConstants.FRAGMENT_TYPE.RETRIEVE_FRAGMENT:
                fragment = new FindHotelRetrieveFragment();
                break;
            case AppConstants.FRAGMENT_TYPE.FRAGMENT_GET_DIRECTION:
                fragment = new GetDirectionFragment();
                break;
        }
        if(fragment != null){
            fragment.setArguments(getIntent().getBundleExtra(AppConstants.BUNDLE_KEYS.EXTRA_BUNDLE));
        }
        return fragment;
    }

    public static void startActivity(Context context, int fragmentType, Bundle extraBundle){
        Intent intent = new Intent(context, FragmentContainerActivity.class);
        if(extraBundle != null){
            intent.putExtra(AppConstants.BUNDLE_KEYS.EXTRA_BUNDLE, extraBundle);
        }
        intent.putExtra(AppConstants.BUNDLE_KEYS.FRAGMENT_TYPE, fragmentType);
        context.startActivity(intent);
    }

    public static void startActivityForResult(Context context, int fragmentType, Bundle extraBundle, int requestCode, Fragment fragment){
        Intent intent = new Intent(context, FragmentContainerActivity.class);
        if(extraBundle != null){
            intent.putExtra(AppConstants.BUNDLE_KEYS.EXTRA_BUNDLE, extraBundle);
        }
        intent.putExtra(AppConstants.BUNDLE_KEYS.FRAGMENT_TYPE, fragmentType);
        fragment.startActivityForResult(intent, requestCode);
    }

    public static void startActivityForResult(Activity context, int fragmentType, Bundle extraBundle, int requestCode){
        Intent intent = new Intent(context, FragmentContainerActivity.class);
        if(extraBundle != null){
            intent.putExtra(AppConstants.BUNDLE_KEYS.EXTRA_BUNDLE, extraBundle);
        }
        intent.putExtra(AppConstants.BUNDLE_KEYS.FRAGMENT_TYPE, fragmentType);
        context.startActivityForResult(intent, requestCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppConstants.RESULT_CODE.RESULT_DONE){
            setResult(AppConstants.RESULT_CODE.RESULT_DONE);
            finish();
        }
    }

}
