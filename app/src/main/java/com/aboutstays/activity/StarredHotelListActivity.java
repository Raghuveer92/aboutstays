package com.aboutstays.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.ProgramModel;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;

public class StarredHotelListActivity extends AppBaseActivity implements CustomListAdapterInterface{
    List<ProgramModel> hotelList = new ArrayList<>();
    private ListView listHotels;
    private CustomListAdapter customListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starred_hotel_list);
        listHotels = (ListView) findViewById(R.id.lv_hotels_details);
        customListAdapter = new CustomListAdapter(this,R.layout.row_hotel_list,hotelList,this);
        listHotels.setAdapter(customListAdapter);
        setDataInList();
    }

    private void setDataInList() {
        for (int x=0; x<2;x++){
            ProgramModel program = new ProgramModel();
            program.setTitle(getString(R.string.hotel_name));
            program.setSubtitle(getString(R.string._12_14_dec_2016));
            hotelList.add(program);
        }
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final ProgramModel program = hotelList.get(position);
        holder.title.setText(program.getTitle());
        holder.subTitle.setText(program.getSubtitle());
        return convertView;
    }

    class Holder {
        TextView title,subTitle;
        public Holder(View view) {
            title = (TextView) view.findViewById(R.id.tv_hotel_title);
            subTitle = (TextView) view.findViewById(R.id.tv_hotel_subtitle);
        }
    }

}
