package com.aboutstays.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.MenuItemCompat;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.enums.MenuItemType;
import com.aboutstays.fragment.SearchFoodItemsFragment;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.cart.FoodItemOrderDetailList;
import com.aboutstays.model.cart.GetCartData;
import com.aboutstays.model.cart.RoomServiceCartResponse;
import com.aboutstays.model.food.FoodDataByType;
import com.aboutstays.model.roomService.RoomServiceData;
import com.aboutstays.model.roomService.RoomServiceMenuItem;
import com.aboutstays.model.roomService.RoomServiceResponse;
import com.aboutstays.model.search.SearchResponseApi;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.cunoraz.gifview.library.GifView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

public class RoomServiceActivity extends AppBaseActivity implements CustomListAdapterInterface, SearchView.OnQueryTextListener, SearchFoodItemsFragment.AddToCartListener, AdapterView.OnItemClickListener {

    private CustomListAdapter listAdapter;
    private List<RoomServiceMenuItem> roomServiceList = new ArrayList<>();
    private GeneralServiceModel generalServiceModel;
    private String serviceId;
    private TextView tvEmptyView;
    private MenuItem searchMenuItem;
    private SearchView searchView;
    private FrameLayout searchFrag;
    private SearchFoodItemsFragment foodItemsFragment;
    private String staysId;
    private String userId, hotelId;
    private GetCartData cartData;
    private TextView tv;
    private String gsId;
    private GifView gifView;
    private FrameLayout frameLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_service);
        registerCartReceiver();
        initToolBar("Room Service");
        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            userId = sessionInstance.getUserId();
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null || generalServiceModel.getHotelStaysList() != null || generalServiceModel.getHotelStaysList().getStaysPojo() != null ||
                    generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId() != null) {
                hotelId = generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId();
            } else {
                showToast("Internet Error");
            }
            if (generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null) {
                serviceId = generalServiceModel.getGeneralServiceData().getHsId();
                gsId = generalServiceModel.getGeneralServiceData().getId();
                staysId = generalServiceModel.getHotelStaysList().getStaysPojo().getStaysId();
            }
        }

        frameLayout = (FrameLayout) findViewById(R.id.lay_frame_loader);
        gifView = (GifView) findViewById(R.id.gif);

        searchFrag = (FrameLayout) findViewById(R.id.frame_search_fragment);
        tvEmptyView = (TextView) findViewById(R.id.tv_empty_view);
        searchFrag.setVisibility(View.GONE);

        foodItemsFragment = SearchFoodItemsFragment.getInstance(this, serviceId, gsId, generalServiceModel);
        getSupportFragmentManager().beginTransaction().add(R.id.frame_search_fragment, foodItemsFragment).commit();

        ListView listView = (ListView) findViewById(R.id.ll_room_service);
        listAdapter = new CustomListAdapter(this, R.layout.row_room_service, roomServiceList, this);
        listView.setAdapter(listAdapter);
        listView.setEmptyView(tvEmptyView);
        listView.setOnItemClickListener(this);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAllCartItem();
    }

    @Override
    public void showProgressDialog() {
        super.showProgressDialog();
        frameLayout.setVisibility(View.VISIBLE);
        gifView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        super.hideProgressBar();
        frameLayout.setVisibility(View.GONE);
        gifView.setVisibility(View.GONE);
    }

    private void getAllCartItem() {
        if (generalServiceModel != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.getAllRoomServiceCartItem(userId, staysId, hotelId, "1");
            executeTask(AppConstants.TASK_CODES.GET_CART_DETAILS, httpParamObject);
        }
    }

    private void getAllRoomService() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getSupportedRoomServiceList(serviceId);
        executeTask(AppConstants.TASK_CODES.GET_ROOM_SERVICE, httpParamObject);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_ROOM_SERVICE:
                RoomServiceResponse roomServiceResponse = (RoomServiceResponse) response;
                if (roomServiceResponse != null && roomServiceResponse.getError() == false) {
                    RoomServiceData serviceData = roomServiceResponse.getData();
                    if (serviceData != null) {
                        List<RoomServiceMenuItem> listMenuItem = serviceData.getListMenuItem();
                        if (!CollectionUtils.isEmpty(listMenuItem)) {
                            roomServiceList.clear();
                            roomServiceList.addAll(listMenuItem);
                            foodItemsFragment.sendRoomServiceMenu(roomServiceList);
                        }
                        listAdapter.notifyDataSetChanged();
                    }
                } else {
                    showToast(roomServiceResponse.getMessage());
                }
                break;

            case AppConstants.TASK_CODES.SEARCH_FOOD_ITEMS:
                SearchResponseApi searchResponseApi = (SearchResponseApi) response;
                if (searchResponseApi != null && searchResponseApi.getError() == false) {
//                    showToast(searchResponseApi.getMessage());
                }
                break;
            case AppConstants.TASK_CODES.GET_CART_DETAILS:
                updateCart();
                getAllRoomService();
                break;
        }
    }

    private void updateCart() {
        cartData = RoomServiceCartResponse.getInstance();
        int size = cartData.getFoodItemOrderDetailList().size();
        if (size > 0) {
            tv.setVisibility(View.VISIBLE);
            tv.setText(size + "");
        } else {
            tv.setVisibility(View.GONE);
        }
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, final LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        RoomServiceMenuItem menuItem = roomServiceList.get(position);
        menuItem.setItemCount(0);
        if (cartData != null) {
            List<FoodItemOrderDetailList> dataList = cartData.getFoodItemOrderDetailList();
            if (!CollectionUtils.isEmpty(dataList)) {
                for (FoodItemOrderDetailList orderDetailList : dataList) {
                    if (menuItem.getType() == orderDetailList.getType()) {
                        menuItem.setItemCount(menuItem.getItemCount() + 1);
                    }
                }
            } else {
                holder.imageRightAngle.setVisibility(View.VISIBLE);
            }
        }

        if (menuItem.getItemCount() >= 1) {
            holder.rlCount.setVisibility(View.VISIBLE);
            holder.tvItemCount.setText(menuItem.getItemCount() + "");
        } else {
            holder.rlCount.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(menuItem.getImageUrl()) && !TextUtils.isEmpty(menuItem.getTypeName())) {
            Picasso.with(this).load(menuItem.getImageUrl()).placeholder(R.drawable.progress_animation).into(holder.imageService);
            holder.roomServiceName.setText(menuItem.getTypeName());
        } else {
            Integer type = menuItem.getType();
            MenuItemType byCode = MenuItemType.getByCode(type);
            if (byCode != null) {
                String displayName = byCode.getDisplayName();
                int imageUrl = byCode.getImageUrl();
                holder.roomServiceName.setText(displayName);
                holder.imageService.setImageResource(imageUrl);
            }
        }

        //holder.Time.setText(menuItem.getTiming());
        if(menuItem.isAllDay()) {
            holder.Time.setText("24 Hrs");
        } else {
            holder.Time.setText(menuItem.getStartTime() + " - " + menuItem.getEndTime());
        }

        return convertView;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_service, menu);

        MenuItem item = menu.findItem(R.id.action_room_service);
        MenuItemCompat.setActionView(item, R.layout.action_menu_count_layout);
        RelativeLayout notifCount = (RelativeLayout) MenuItemCompat.getActionView(item);

        ImageView iv = (ImageView) notifCount.findViewById(R.id.iv_roomservice);
        iv.setColorFilter(Color.WHITE);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderDetailActivity.startActivity(RoomServiceActivity.this, gsId, staysId, hotelId, AppConstants.STAYS_TYPE.ROOM_SERVICE, AppConstants.FROM_ORDERING.ROOM_SERVICE);
            }
        });

        tv = (TextView) notifCount.findViewById(R.id.actionbar_notifcation_textview);

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.length() >= 3) {
            searchFrag.setVisibility(View.VISIBLE);
            foodItemsFragment.textUpdate(newText, serviceId, staysId);
        } else {
            searchFrag.setVisibility(View.GONE);
            getAllCartItem();
        }
        if (TextUtils.isEmpty(newText)) {
            foodItemsFragment.callBack();
        }
        return true;
    }

    @Override
    public void searchCallback() {
        searchFrag.setVisibility(View.GONE);
    }

    @Override
    public void updateCartItem(FoodDataByType foodDataByType) {
        if (cartData != null) {
            cartData.refreshItem(foodDataByType,this);
        } else {
            cartData = new GetCartData();
            cartData.refreshItem(foodDataByType,this);
        }
        updateCart();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        RoomServiceMenuItem menuItem = roomServiceList.get(position);
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.ROOM_SERVICE_ID, serviceId);
        bundle.putString(AppConstants.BUNDLE_KEYS.STAYS_ID, staysId);
        bundle.putString(AppConstants.BUNDLE_KEYS.HOTEL_ID, hotelId);
        bundle.putString(AppConstants.BUNDLE_KEYS.GS_ID, gsId);
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.ROOM_SERVICE_TITLE, menuItem);
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
        Intent intent = new Intent(RoomServiceActivity.this, RoomServiceFoodItemActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, AppConstants.REQUEST_CODE.CART_IEMS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
        else if (resultCode == AppConstants.RESULT_CODE.RESULT_DONE){
            getAllCartItem();
            hideProgressBar();
        }
    }

    class Holder {
        TextView roomServiceName, Time, tvItemCount;
        ImageView imageService, imageRightAngle;
        RelativeLayout rlCount;

        public Holder(View view) {
            roomServiceName = (TextView) view.findViewById(R.id.tv_title);
            Time = (TextView) view.findViewById(R.id.tv_time);
            imageService = (ImageView) view.findViewById(R.id.iv_title_image1);
            imageRightAngle = (ImageView) view.findViewById(R.id.iv_right_angle);
            rlCount = (RelativeLayout) view.findViewById(R.id.rl_count);
            tvItemCount = (TextView) view.findViewById(R.id.tv_item_count);
        }
    }

    @Override
    public void onCartUpdate() {
        updateCart();
//        getAllCartItem();
    }

    @Override
    public void onCartClear() {
        getAllRoomService();
        if(searchView!=null){
            if(searchFrag.getVisibility()==View.VISIBLE){
                foodItemsFragment.textUpdate(searchView.getQuery().toString(), serviceId, staysId);
            }
        }
    }
}
