package com.aboutstays.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.HotelInfoModel;
import com.aboutstays.model.hotels.GetHotelById;
import com.aboutstays.model.hotels.Hotel;
import com.aboutstays.model.stayline.HotelStaysList;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class HotelInfoActivity extends BaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    private ListView lvHotelList;
    private CustomListAdapter listAdapter;
    private List<HotelInfoModel> hotelInfoModelList = new ArrayList<>();
    private Integer[] logo;
    List<String> itemList = new ArrayList<>();
    private GeneralServiceModel generalServiceModel;
    private HotelStaysList hotelStaysList = new HotelStaysList();
    private String hotelId;
    private Hotel hoteldata = new Hotel();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_info);
        initToolBar("Hotel Info");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null && generalServiceModel.getGeneralServiceData() != null) {
                hotelStaysList = generalServiceModel.getHotelStaysList();
                hotelId = generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId();
                String roomId = generalServiceModel.getHotelStaysList().getStaysPojo().getRoomId();
            }
        }


        logo = new Integer[]{
                R.mipmap.information_icon,
                R.mipmap.location,
                R.mipmap.review_rate,
                R.mipmap.rooms,
                R.mipmap.essential,
                R.mipmap.restaurants,
                R.mipmap.health_beauty,
                R.mipmap.sports,
                R.mipmap.business,
                R.mipmap.shopes,
        };

        lvHotelList = (ListView) findViewById(R.id.list_hotel_info);
        listAdapter = new CustomListAdapter(this, R.layout.row_stay_preferences, hotelInfoModelList, this);
        lvHotelList.setAdapter(listAdapter);
        loadData();

        lvHotelList.setOnItemClickListener(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getHotelById();
    }

    private void getHotelById() {
        if (!TextUtils.isEmpty(hotelId)){
            HttpParamObject httpParamObject = ApiRequestGenerator.getHotelsById(hotelId);
            executeTask(AppConstants.TASK_CODES.HOTEL_BY_ID, httpParamObject);
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null){
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.HOTEL_BY_ID:
                GetHotelById getHotelById = (GetHotelById) response;
                if (getHotelById != null && getHotelById.getError() == false){
                    hoteldata = getHotelById.getData();
                }
                break;
        }
    }

    private void loadData() {
        String[] temp = getResources().getStringArray(R.array.item_list);
        for (int i = 0; i < 4; i++) {
            HotelInfoModel hotelInfoModel = new HotelInfoModel();
            hotelInfoModel.setName(temp[i]);
            hotelInfoModel.setLogo(logo[i]);
            hotelInfoModelList.add(hotelInfoModel);
            listAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_stay_preferences, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        HotelInfoModel hotelInfoModel = hotelInfoModelList.get(position);
        holder.name.setText(hotelInfoModel.getName());
        holder.logo.setImageResource(hotelInfoModel.getLogo());
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle b = new Bundle();
        b.putSerializable(AppConstants.BUNDLE_KEYS.GENERAL_INFO, hoteldata);

        switch (position){
            case 0:
                startNextActivity(b, GenralHotelInfoActivity.class);
                break;
            case 1:
                startNextActivity(b, LocationAndContactActivity.class);
                break;
            case 2:
                startNextActivity(ReviewsAndRatings.class);
                break;
            case 3:
                startNextActivity(b, RoomsActivity.class);
                break;
            case 4:

                break;
        }
    }

    class Holder {
        TextView name;
        ImageView logo, ivSelect;

        public Holder(View view) {
            name = (TextView) view.findViewById(R.id.tv_preference);
            logo = (ImageView) view.findViewById(R.id.iv_preferences);
            ivSelect = (ImageView) view.findViewById(R.id.iv_select);
            ivSelect.setVisibility(View.GONE);
        }
    }
}
