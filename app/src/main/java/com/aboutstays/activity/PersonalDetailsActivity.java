package com.aboutstays.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.aboutstays.R;
import com.aboutstays.fragment.dialog.SuccessDialogFragment;
import com.aboutstays.model.common.FetchProfileTitles;
import com.aboutstays.model.initiate_checkIn.CheckInData;
import com.aboutstays.services.UserServices;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.aboutstays.utitlity.DatePickerUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.pojo.Address;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.response.UserProfileData;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by neeraj on 13/12/16.
 */
public class PersonalDetailsActivity extends AppBaseActivity implements AdapterView.OnItemSelectedListener {


    private TextInputLayout tilFristName, tilLastName, tilDob, tilAnniversery, tilEmail, tilNumber, tilHomeAddress, tilOfficeAddress;
    private Button btnSaveChanges;
    private Spinner spinnerMarital, spinnerTitle;
    private String userId;
    private LinearLayout layAnnerservary, layExtraDetails;
    private UserSession session;
    private String dateFormate = "dd-MM-yyyy";
    private Integer maritalStatus;
    private Integer getMaritalStatus;
    private Calendar calendarInstance;
    private boolean fromCheckIn;
    private CheckInData checkInData;
    private String titleName;
    List<String> titleList = new ArrayList<String>();
    private ArrayList<String> statusList =new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_details);

        initToolBar("Personal Details");
        calendarInstance = Calendar.getInstance();

        findViews();

        UserServices.startUpdateUser(this);
        initStatusList();
        initTitleList();


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fromCheckIn = bundle.getBoolean(AppConstants.BUNDLE_KEYS.FROM_CHECKIN);
            checkInData = (CheckInData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);

            if (fromCheckIn && checkInData != null) {
                UserProfileData personalInformation = checkInData.getPersonalInformation();
                if (personalInformation != null) {
                    tilFristName.getEditText().setText(personalInformation.getFirstName());
                    tilLastName.getEditText().setText(personalInformation.getLastName());
                    if (!TextUtils.isEmpty(personalInformation.getDob())) {
                        tilDob.getEditText().setText(personalInformation.getDob());
                    }
                    if (personalInformation.getMaritalStatus() != null) {
                        getMaritalStatus = personalInformation.getMaritalStatus();
                    }
                    if (!TextUtils.isEmpty(personalInformation.getTitle())) {
                        titleName = personalInformation.getTitle();
                    }
                    if (personalInformation.getAnniversary() != null) {
                        tilAnniversery.getEditText().setText(personalInformation.getAnniversary());
                    }
                } else {
                    setPersonalSessionData();
                }
            }

            if (fromCheckIn) {
                layExtraDetails.setVisibility(View.VISIBLE);
                if (checkInData.getPersonalInformation() != null) {
                    UserProfileData personalInformation = checkInData.getPersonalInformation();
                    userId = personalInformation.getUserId();
                    tilEmail.getEditText().setText(personalInformation.getEmailId());
                    tilNumber.getEditText().setText(personalInformation.getNumber());
                    if (personalInformation.getOfficeAddress() != null && !TextUtils.isEmpty(personalInformation.getOfficeAddress().getDescription())) {
                        tilOfficeAddress.getEditText().setText(personalInformation.getOfficeAddress().getDescription());
                    }
                    if (personalInformation.getHomeAddress() != null && !TextUtils.isEmpty(personalInformation.getHomeAddress().getDescription())) {
                        tilHomeAddress.getEditText().setText(personalInformation.getHomeAddress().getDescription());
                    }
                } else {
                    setSessionData();
                }
            }
        } else {
            layExtraDetails.setVisibility(View.GONE);
        }

        if (!fromCheckIn) {
            setPersonalSessionData();
        }
        refreshTitleList();
        refreshMarialList();
        setOnClickListener(R.id.et_dob, R.id.et_anniversary, R.id.btn_save_changes);
    }

    private void refreshMarialList() {
        ArrayAdapter spinnerAdapter = new ArrayAdapter<String>(this, R.layout.row_spinner_pesonal_detail, statusList);
        spinnerAdapter.setDropDownViewResource(R.layout.row_spinner_drop_down_personal_detail);
        spinnerMarital.setAdapter(spinnerAdapter);
        spinnerMarital.setAdapter(spinnerAdapter);
        spinnerMarital.setOnItemSelectedListener(this);
        if (getMaritalStatus != null&&getMaritalStatus<=statusList.size()) {
            spinnerMarital.setSelection(getMaritalStatus - 1);
        }
    }

    private void refreshTitleList() {
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, R.layout.row_spinner_pesonal_detail, titleList);
        spinnerAdapter.setDropDownViewResource(R.layout.row_spinner_drop_down_personal_detail);
        spinnerTitle.setAdapter(spinnerAdapter);
        if(!TextUtils.isEmpty(titleName)){
            int i = titleList.indexOf(titleName);
            if (i != -1) {
                spinnerTitle.setSelection(i);
            }
        }
    }

    private void initTitleList() {
        Set<String> titlesSet = Preferences.getData(Preferences.KEY_PROFILE_TITLES);
        if (CollectionUtils.isNotEmpty(titlesSet)) {
            titleList = new ArrayList<>(titlesSet);
        } else {
            HttpParamObject httpParamObject = ApiRequestGenerator.fetchProfileTitles();
            executeTask(AppConstants.TASK_CODES.FETCH_PROFILE_TITLES, httpParamObject);
        }
    }

    private void initStatusList() {
        statusList.clear();
        statusList.add("SINGLE");
        statusList.add("MARRIED");
        statusList.add("ENGAGED");
    }

    private void setPersonalSessionData() {
        session = UserSession.getSessionInstance();
        if (session != null) {
            userId = session.getUserId();
            tilFristName.getEditText().setText(session.getFirstName());
            tilLastName.getEditText().setText(session.getLastName());
            if (!TextUtils.isEmpty(session.getDob())) {
                tilDob.getEditText().setText(session.getDob());
            }
            if (session.getMaritalStatus() != null) {
                getMaritalStatus = session.getMaritalStatus();
            }
            if (session.getAnniversary() != null) {
                tilAnniversery.getEditText().setText(session.getAnniversary());
            }
            if (!TextUtils.isEmpty(session.getTitle())) {
                titleName = session.getTitle();
            }
        }
    }

    private void findViews() {
        layAnnerservary = (LinearLayout) findViewById(R.id.lay_anniversary);
        layAnnerservary.setVisibility(View.INVISIBLE);
        tilFristName = (TextInputLayout) findViewById(R.id.til_first_name);
        tilLastName = (TextInputLayout) findViewById(R.id.til_last_name);
        tilDob = (TextInputLayout) findViewById(R.id.til_dob);
        spinnerMarital = (Spinner) findViewById(R.id.spineer_marital_status);
        spinnerTitle = (Spinner) findViewById(R.id.spineer_title);
        tilAnniversery = (TextInputLayout) findViewById(R.id.til_anniversary);
        btnSaveChanges = (Button) findViewById(R.id.btn_save_changes);
        layExtraDetails = (LinearLayout) findViewById(R.id.lay_extra_details);
        tilEmail = (TextInputLayout) findViewById(R.id.til_email);
        tilNumber = (TextInputLayout) findViewById(R.id.til_number);
        tilHomeAddress = (TextInputLayout) findViewById(R.id.til_home_address);
        tilOfficeAddress = (TextInputLayout) findViewById(R.id.til_office_address);
        Typeface typeface=Typeface.createFromAsset(this.getAssets(),"fonts/Gotham Narrow Book.otf");
        tilFristName.setTypeface(typeface);
        tilLastName.setTypeface(typeface);
        tilDob.setTypeface(typeface);
        tilAnniversery.setTypeface(typeface);
        tilEmail.setTypeface(typeface);
        tilNumber.setTypeface(typeface);
        tilHomeAddress.setTypeface(typeface);
        tilOfficeAddress.setTypeface(typeface);
        tilFristName.getEditText().setTypeface(typeface);
        tilLastName.getEditText().setTypeface(typeface);
        tilDob.getEditText().setTypeface(typeface);
        tilAnniversery.getEditText().setTypeface(typeface);
        tilEmail.getEditText().setTypeface(typeface);
        tilNumber.getEditText().setTypeface(typeface);
        tilHomeAddress.getEditText().setTypeface(typeface);
        tilOfficeAddress.getEditText().setTypeface(typeface);
    }

    private void setSessionData() {
        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            userId = sessionInstance.getUserId();
            tilEmail.getEditText().setText(sessionInstance.getEmailId());
            tilNumber.getEditText().setText(sessionInstance.getNumber());
            if (sessionInstance.getOfficeAddress() != null && !TextUtils.isEmpty(sessionInstance.getOfficeAddress().getDescription())) {
                tilOfficeAddress.getEditText().setText(sessionInstance.getOfficeAddress().getDescription());
            }
            if (sessionInstance.getHomeAddress() != null && !TextUtils.isEmpty(sessionInstance.getHomeAddress().getDescription())) {
                tilHomeAddress.getEditText().setText(sessionInstance.getHomeAddress().getDescription());
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_dob:
                getDate(tilDob);
                break;
            case R.id.et_anniversary:
                getDate(tilAnniversery);
                break;
            case R.id.btn_save_changes:
                validateAndSaveChanges();
                break;
        }
    }


    private void getDate(final TextInputLayout til) {

        DatePickerUtil datePickerUtil = new DatePickerUtil(dateFormate, calendarInstance, getFragmentManager(), getString(R.string.select_date), false, true, new DatePickerUtil.OnDateSelected() {
            @Override
            public void onDateSelected(Calendar calendar, String date) {

                if (til == tilDob) {
                    til.getEditText().setText(date);
                } else {
                    til.getEditText().setText(date);
                }
            }

            @Override
            public void onCanceled() {
//                til.getEditText().setText("");
            }
        });
        datePickerUtil.show();
    }

    private void validateAndSaveChanges() {
        String firstName = tilFristName.getEditText().getText().toString().trim();
        String lastName = tilLastName.getEditText().getText().toString().trim();
        String userDob = tilDob.getEditText().getText().toString().trim();
        String anniversaryDate = tilAnniversery.getEditText().getText().toString().trim();
        String email = tilEmail.getEditText().getText().toString().trim();
        String number = tilNumber.getEditText().getText().toString().trim();
        String hAddress = tilHomeAddress.getEditText().getText().toString().trim();
        String oAddress = tilOfficeAddress.getEditText().getText().toString().trim();

        if (TextUtils.isEmpty(firstName)) {
            showToast("First name can not be empty");
            return;
        } else {
            tilFristName.setError(null);
            tilFristName.setErrorEnabled(false);
        }
        if (TextUtils.isEmpty(lastName)) {
            showToast("Last name can not be empty");
            return;
        } else {
            tilLastName.setError(null);
            tilLastName.setErrorEnabled(false);
        }

        if (fromCheckIn) {
            if (TextUtils.isEmpty(email)) {
                showToast("Email can not be empty");
                return;
            } else if (!Util.isValidEmail(email)) {
                tilEmail.setError(getString(R.string.invalid_email));
                return;
            } else {
                tilEmail.setError(null);
                tilEmail.setErrorEnabled(false);
            }
            if (TextUtils.isEmpty(number) || number.length() < 10) {
                showToast("Please enter 10 digit vaid mobile number");
                return;
            } else {
                tilNumber.setError(null);
                tilNumber.setErrorEnabled(false);
            }
/*            if (TextUtils.isEmpty(hAddress)) {
                showToast("Home Address can not be empty");
                return;
            } else {
                tilHomeAddress.setError(null);
                tilHomeAddress.setErrorEnabled(false);
            }
            if (TextUtils.isEmpty(oAddress)) {
                showToast("Office Address can not be empty");
                return;
            } else {
                tilOfficeAddress.setError(null);
                tilOfficeAddress.setErrorEnabled(false);
            }*/
            CheckInData checkInData = new CheckInData();
            UserProfileData userProfileData = new UserProfileData();
            userProfileData.setFirstName(firstName);
            userProfileData.setLastName(lastName);
            userProfileData.setDob(userDob);
            userProfileData.setEmailId(email);
            userProfileData.setNumber(number);
            userProfileData.setTitle(titleList.get(spinnerTitle.getSelectedItemPosition()));

            Address homeAddress = new Address();
            homeAddress.setDescription(hAddress);
            userProfileData.setHomeAddress(homeAddress);

            Address officeAddress = new Address();
            officeAddress.setDescription(oAddress);
            userProfileData.setOfficeAddress(officeAddress);

            checkInData.setPersonalInformation(userProfileData);
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, checkInData);
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            String selectedTitle = titleList.get(spinnerTitle.getSelectedItemPosition());
            HttpParamObject httpParamObject = ApiRequestGenerator.updatePersonalDetails(selectedTitle, firstName, lastName, userDob, maritalStatus, anniversaryDate, userId);
            executeTask(AppConstants.TASK_CODES.UPDATE_USER_PERSONAL_DETAILS, httpParamObject);
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.UPDATE_USER_PERSONAL_DETAILS:
                BaseApiResponse updateResponse = (BaseApiResponse) response;
                if (updateResponse != null) {
                    if (updateResponse.getError() == false) {
                        UserServices.startUpdateUser(this);
                        successDialog();
//                        showToast("Personal details update.");
//                        setResult(RESULT_OK);
//                        finish();
                    } else {
                        errorDialog(updateResponse.getMessage());
//                        showToast(updateResponse.getMessage());
                    }
                }
                break;
            case AppConstants.TASK_CODES.FETCH_PROFILE_TITLES:
                FetchProfileTitles profileTitles = (FetchProfileTitles) response;
                if (profileTitles != null && !profileTitles.getError()) {
                    List<String> profileTitles1 = profileTitles.getProfileTitles();
                    if (CollectionUtils.isNotEmpty(profileTitles1)) {
                        titleList.clear();
                        titleList.addAll(profileTitles1);
                        refreshTitleList();
                    }
                }
                break;
        }
    }

    private void errorDialog(String message) {
        SuccessDialogFragment.show(getSupportFragmentManager(),
                "PERSONAL DETAILS",
                "UPDATE ERROR",
                message,
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                    }
                }
        );
    }

    private void successDialog() {
        SuccessDialogFragment.show(getSupportFragmentManager(),
                "PERSONAL DETAILS",
                "UPDATED",
                "Personal details update.",
                getString(R.string.ok),
                new SuccessDialogFragment.OnSuccessListener() {
                    @Override
                    public void done() {
                        setResult(RESULT_OK);
                        finish();
                    }
                }
        );
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        maritalStatus = parent.getSelectedItemPosition() + 1;
        if (maritalStatus == 2) {
            layAnnerservary.setVisibility(View.VISIBLE);
        } else {
            layAnnerservary.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
