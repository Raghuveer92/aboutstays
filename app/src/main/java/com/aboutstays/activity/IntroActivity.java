package com.aboutstays.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.FloatRange;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.aboutstays.R;

import agency.tango.materialintroscreen.MaterialIntroActivity;
import agency.tango.materialintroscreen.MessageButtonBehaviour;
import agency.tango.materialintroscreen.SlideFragmentBuilder;
import agency.tango.materialintroscreen.animations.IViewTranslation;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class IntroActivity extends MaterialIntroActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams attrs = this.getWindow().getAttributes();
        attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
        getWindow().setAttributes(attrs);
        super.onCreate(savedInstanceState);

        enableLastSlideAlphaExitTransition(true);

        setSkipButtonVisible();

        skipButton.setVisibility(View.VISIBLE);
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.saveData(Preferences.IF_FIRST, true);
                Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

//        getBackButtonTranslationWrapper()
//                .setEnterTranslation(new IViewTranslation() {
//                    @Override
//                    public void translate(View view, @FloatRange(from = 0, to = 1.0) float percentage) {
//                        view.setAlpha(percentage);
//                    }
//                });

        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.white)
                .buttonsColor(R.color.button_login_color)
                .image(R.mipmap.anytime_access)
                .build());

        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.white)
                .buttonsColor(R.color.button_login_color)
                .image(R.mipmap.quick_checkin_checkout)
                .build());

        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.white)
                .buttonsColor(R.color.button_login_color)
                .image(R.mipmap.service_access)
                .build());

        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.white)
                .buttonsColor(R.color.button_login_color)
                .image(R.mipmap.deals_offers)
                .build());


        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.white)
                .buttonsColor(R.color.button_login_color)
                .image(R.mipmap.stayline)
                .build());

//        addSlide(new SlideFragmentBuilder()
//                        .backgroundColor(R.color.color_primary)
//                        .buttonsColor(R.color.color_time_orange)
//                        .image(R.mipmap.quick_checkin_checkout)
//                        .titleList("Organize your time with us")
//                        .description("Would you try?")
//                        .build(),

//        addSlide(new CustomSlide());
//
//        addSlide(new SlideFragmentBuilder()
//                        .backgroundColor(R.color.third_slide_background)
//                        .buttonsColor(R.color.third_slide_buttons)
//                        .possiblePermissions(new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_SMS})
//                        .neededPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
//                        .image(R.drawable.img_equipment)
//                        .titleList("We provide best tools")
//                        .description("ever")
//                        .build(),
//                new MessageButtonBehaviour(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        showMessage("Try us!");
//                    }
//                }, "Tools"));
    }

    @Override
    public void onFinish() {
        super.onFinish();
        Preferences.saveData(Preferences.IF_FIRST, true);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}