package com.aboutstays.activity;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.DrawerFragment;
import com.aboutstays.fragment.StayLineFragment;
import com.aboutstays.model.booking.BookingData;
import com.aboutstays.model.booking.ListBooking;
import com.aboutstays.model.hotels.GeneralInformation;
import com.aboutstays.model.stayline.AddtoStaylineApi;
import com.aboutstays.model.stayline.GeneralSearchInformation;
import com.aboutstays.model.stayline.HotelBasicInfo;
import com.aboutstays.rest.response.GeneralInfoDataWithHotelType;
import com.aboutstays.rest.response.GetHotelGeneralInfoResponse;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;

public class AddStayActivity extends AppBaseActivity {
    private Button addStay;
    private ImageView ivHotelIcon;
    private TextView tvHotelName, tvHotelAddress, tvGuestName, tvCheckindate, tvCheckoutDate, tvRoomType, tvpax;
    private EditText etRevNum;
    private String userId;
    private String bookingId;
    private String hotelId;
    private String roomId;
    private String checkInDate;
    private String checkOutDate;
    private String revNum;
    private String checkInTime;
    private String checkOutTime;
    private FrameLayout fragContainer;
    private DrawerActivity drawerActivity;
    private SwitchCompat switchBtn;
    private boolean official = false;


    public static AddStayActivity getInstance(DrawerActivity drawerActivity) {
        AddStayActivity addStayActivity = new AddStayActivity();
        addStayActivity.drawerActivity = drawerActivity;
        return addStayActivity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_stay);
        initToolBar(getString(R.string.retrieve_stay));

        findViews();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            HotelBasicInfo hotelBasicInfo = (HotelBasicInfo) bundle.getSerializable(AppConstants.BUNDLE_KEYS.HOTEL_DATA);
            if (hotelBasicInfo != null){
                GeneralSearchInformation generalInformation = hotelBasicInfo.getGeneralInformation();
                if (generalInformation != null){
                    setDatatoView(generalInformation);
                }
            }
            revNum = bundle.getString(AppConstants.BUNDLE_KEYS.REV_NUMBER);
            BookingData bookingData = (BookingData) bundle.getSerializable(AppConstants.BUNDLE_KEYS.BOOKING_DATA);
            if (bookingData != null){
                List<ListBooking> listBookings = bookingData.getListBookings();
                for (ListBooking listBooking : listBookings) {
                    setData(listBooking);
                }
            }
            ListBooking listBooking = (ListBooking) bundle.getSerializable(AppConstants.BUNDLE_KEYS.BOOKING_LIST);
            if (listBooking != null){
                setData(listBooking);
            }
        }

        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            userId = sessionInstance.getUserId();
        }
        setOnClickListener(R.id.btn_add_stay);

        switchBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
    }

    private void setDatatoView(GeneralSearchInformation generalInformation) {
        checkInTime = generalInformation.getCheckInTime();
        checkOutTime = generalInformation.getCheckOutTime();
        tvHotelName.setText(generalInformation.getName());
        if(generalInformation.getAddress() != null){
            tvHotelAddress.setText(generalInformation.getAddress().getCity() + ",  " + generalInformation.getAddress().getCountry());
        }
        if(!TextUtils.isEmpty(generalInformation.getLogoUrl())){
            Picasso.with(this).load(generalInformation.getLogoUrl())
                    .placeholder(R.drawable.progress_animation)
                    .into(ivHotelIcon);
        }
    }

    private void findViews() {
        fragContainer = (FrameLayout) findViewById(R.id.fl_frag_container);
        addStay = (Button) findViewById(R.id.btn_add_stay);
        etRevNum = (EditText) findViewById(R.id.text_enter_reservation_num);
        tvHotelAddress = (TextView) findViewById(R.id.tv_hotel_address_retrieve);
        tvGuestName = (TextView) findViewById(R.id.tv_guest_name);
        tvHotelName = (TextView) findViewById(R.id.tv_hotel_name_retrieve);
        tvCheckindate = (TextView) findViewById(R.id.tv_date_checkin);
        tvCheckoutDate = (TextView) findViewById(R.id.tv_date_checkout);
        tvpax = (TextView) findViewById(R.id.tv_pax);
        tvRoomType = (TextView) findViewById(R.id.tv_room_type);
        ivHotelIcon = (ImageView) findViewById(R.id.iv_hotel_icon);
        switchBtn = (SwitchCompat) findViewById(R.id.switch_official);
    }

    private void setData(ListBooking listBooking) {
        if (listBooking != null) {
            bookingId = listBooking.getBookingId();
            hotelId = listBooking.getHotelId();
            roomId = listBooking.getRoomId();
            checkInDate = listBooking.getCheckInDate();
            checkOutDate = listBooking.getCheckOutDate();

            if(listBooking.getBookedForUser() != null){
                tvGuestName.setText(listBooking.getBookedForUser().getName());
                tvpax.setText(listBooking.getBookedForUser().getPax());
            }
            if(listBooking.getRoomPojo() !=  null){
                tvRoomType.setText(listBooking.getRoomPojo().getName());
            }
            tvCheckindate.setText(checkInDate);
            tvCheckoutDate.setText(checkOutDate);
            if (!TextUtils.isEmpty(revNum)) {
                etRevNum.setText(revNum);
            } else {
                etRevNum.setText(listBooking.getReservationNumber());
            }
        }
//        getHotelGeneralInfo();
    }

/*    private void getHotelGeneralInfo() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getHotelGeneralInfoById(hotelId);
        executeTask(AppConstants.TASK_CODES.HOTEL_GENERAL_INFO, httpParamObject);
    }*/


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_add_stay:
                validatedata();
                break;
        }
    }

    private void validatedata() {
        if (TextUtils.isEmpty(userId) || TextUtils.isEmpty(bookingId) || TextUtils.isEmpty(roomId) || TextUtils.isEmpty(hotelId)) {
            showToast("Error");
            return;
        }
        if (TextUtils.isEmpty(checkInDate) || TextUtils.isEmpty(checkOutDate)) {
            showToast("Error");
            return;
        }
//        if(TextUtils.isEmpty(checkInTime)){
//            checkInTime = "12:00";
//        }
//        if(TextUtils.isEmpty(checkOutTime)){
//            checkOutTime = "11:00";
//        }
        addToStayline();
    }

    private void addToStayline() {
        HttpParamObject httpParamObject = ApiRequestGenerator.addToStayline(hotelId,roomId,userId,bookingId,checkInDate,checkOutDate,checkInTime,checkOutTime, switchBtn.isChecked());
        executeTask(AppConstants.TASK_CODES.ADD_STAYLINE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast("Server Error");
            return;
        }
        switch (taskCode){
            case AppConstants.TASK_CODES.ADD_STAYLINE:
                AddtoStaylineApi addtoStaylineApi = (AddtoStaylineApi) response;
                if (addtoStaylineApi != null && addtoStaylineApi.getError() == false){
                    //showToast(addtoStaylineApi.getMessage());
                    openFragment();
                } else {
                    showToast(addtoStaylineApi.getMessage());
                }
                break;
/*            case AppConstants.TASK_CODES.HOTEL_GENERAL_INFO:
                GetHotelGeneralInfoResponse hotelResponse = (GetHotelGeneralInfoResponse) response;
                if(hotelResponse != null && hotelResponse.getError() == false){
                    GeneralInfoDataWithHotelType hotelGeneralInfoWithType = hotelResponse.getData();
                    if(hotelGeneralInfoWithType != null){
                        GeneralInformation generalInformation = hotelGeneralInfoWithType.getGeneralInformation();
                        if(generalInformation != null){

                        }
                    }
                }
                break;*/
        }
    }

    private void openFragment() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppConstants.BUNDLE_KEYS.FROM_ADD_STAY, true);
        Intent intent = new Intent(this, DrawerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.left_arrow;
    }

}
