package com.aboutstays.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.enums.FoodLabels;
import com.aboutstays.enums.MenuItemType;
import com.aboutstays.enums.SpicyType;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.model.cart.RoomServiceCartResponse;
import com.aboutstays.model.cart.RoomServiceCartData;
import com.aboutstays.model.cart.FoodItemOrderDetailList;
import com.aboutstays.model.cart.FoodItemOrderDetails;
import com.aboutstays.model.cart.GetCartData;
import com.aboutstays.model.cart.RoomServiceCart;
import com.aboutstays.model.food.FoodDataByType;
import com.aboutstays.model.food.GetFoodItemsApi;
import com.aboutstays.model.food.GoesWellItemData;
import com.aboutstays.model.roomService.RoomServiceMenuItem;
import com.aboutstays.receiver.StayCartUpdateReceiver;
import com.aboutstays.utitlity.ApiRequestGenerator;
import com.aurelhubert.simpleratingbar.SimpleRatingBar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

public class FoodItemActivity extends AppBaseActivity {

    private TextView tvTitle, tvPrice, tvServe, tvFullDesc, tvDesc, tvAdd, tvSpicy, tvVeg, tvRecommended, tvGoes;
    private TextView tvMinus, tvCount, tvPlus;
    private ImageView ivChilly, ivVeg, ivRecomanded, ivMain;
    private LinearLayout llItem1, llItem2, llItem3, llGoesWellWith;
    private TextView tvDescription;
    private RelativeLayout rlAddCount, rlBottomButton;
    private String displayName;
    private Button btnAddtoList;
    private String fooditemId;
    private FoodDataByType foodItem;
    private String userId;
    private String staysId, hotelId;
    private RoomServiceCart roomServiceCart;
    private GeneralServiceModel generalServiceModel;
    private List<FoodItemOrderDetailList> foodItemOrderDetailList = new ArrayList<>();
    private RoomServiceMenuItem menuItem;
    private TextView tv;
    private String gsId;
    private boolean orderPlaced;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_item);
        registerCartReceiver();
        findViews();
        initToolBar("Food Item");

        UserSession sessionInstance = UserSession.getSessionInstance();
        if (sessionInstance != null) {
            userId = sessionInstance.getUserId();
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null && generalServiceModel.getHotelStaysList() != null && generalServiceModel.getHotelStaysList().getStaysPojo() != null &&
                    generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId() != null) {
                hotelId = generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId();
            } else {
                showToast("Internet Error");
            }
            fooditemId = bundle.getString(AppConstants.BUNDLE_KEYS.FOOD_ITEM_ID);
            staysId = bundle.getString(AppConstants.BUNDLE_KEYS.STAYS_ID);
            foodItem = (FoodDataByType) bundle.getSerializable(AppConstants.BUNDLE_KEYS.FOOD_ITEM_DETAILS);
            if (foodItem != null) {
                fooditemId = foodItem.getFooditemId();
                Integer menuItemType = foodItem.getType();
                if (menuItemType != null && menuItemType != 0 && menuItemType < 10) {
                    MenuItemType byCode = MenuItemType.getByCode(menuItemType);
                    if (byCode != null) {
                        displayName = byCode.getDisplayName();
                    }
                }

                if (!TextUtils.isEmpty(displayName)) {
                    initToolBar(displayName + " item");
                } else {
                    initToolBar("Food Item");
                }
                setData();
            }
            menuItem = (RoomServiceMenuItem) bundle.getSerializable(AppConstants.BUNDLE_KEYS.ROOM_SERVICE_TITLE);
            gsId = bundle.getString(AppConstants.BUNDLE_KEYS.GS_ID);
        }
        btnAddtoList.setOnClickListener(this);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getFoodItemsById();
    }

    @Override
    public void onPreExecute(int taskCode) {
        if (taskCode == AppConstants.TASK_CODES.ADD_REMOVE_FOOD_TO_CART) {
            return;
        }
        super.onPreExecute(taskCode);
    }

    private void getFoodItemsById() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getFoodItemmsById(fooditemId);
        executeTask(AppConstants.TASK_CODES.GET_FOOD_BY_ID, httpParamObject);
    }


    private void addItemToCart() {
        List<RoomServiceCartData> roomServiceCartDataList = new ArrayList<>();
        int itemCount = foodItem.getItemCount();

        roomServiceCart = new RoomServiceCart();
        roomServiceCart.setUserId(userId);
        roomServiceCart.setStaysId(staysId);
        roomServiceCart.setHotelId(foodItem.getHotelId());

        RoomServiceCartData roomServiceCartData = new RoomServiceCartData();
        roomServiceCartData.setCartType(1);
        roomServiceCartData.setCartOperation(1);

        FoodItemOrderDetails data = new FoodItemOrderDetails();
        data.setQuantity(itemCount);
        data.setType(foodItem.getType());
        data.setFoodItemId(foodItem.getFooditemId());
        data.setPrice(foodItem.getPrice());

        roomServiceCartData.setFoodItemOrderDetails(data);
        roomServiceCartDataList.add(roomServiceCartData);
        roomServiceCart.setData(roomServiceCartDataList);

        if (roomServiceCart != null) {
            HttpParamObject httpParamObject = ApiRequestGenerator.addOrRemoveFoodItem(roomServiceCart);
            executeTask(AppConstants.TASK_CODES.ADD_REMOVE_FOOD_TO_CART, httpParamObject);
        }

    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        hideProgressBar();
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_FOOD_BY_ID:
                GetFoodItemsApi getFoodItemsApi = (GetFoodItemsApi) response;
                if (getFoodItemsApi != null && getFoodItemsApi.getError() == false) {
                    foodItem = getFoodItemsApi.getData();
                    if (foodItem!=null){
                        Integer menuItemType = foodItem.getType();
                        if (menuItemType != null && menuItemType != 0 && menuItemType < 10) {
                            MenuItemType byCode = MenuItemType.getByCode(menuItemType);
                            if (byCode != null) {
                                displayName = byCode.getDisplayName();
                            }
                        }

                        if (!TextUtils.isEmpty(displayName)) {
                            initToolBar(displayName + " item");
                        } else {
                            initToolBar("Food Item");
                        }
                        checkIfAlreadyInCart();
                        setData();
                    }
                }
                break;

            case AppConstants.TASK_CODES.ADD_REMOVE_FOOD_TO_CART:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, foodItem);
                    Intent intent = new Intent();
                    intent.putExtras(bundle);
                    setResult(RESULT_OK, intent);
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_service, menu);
        MenuItem item = menu.findItem(R.id.action_room_service);
        menu.findItem(R.id.action_search).setVisible(false);
        MenuItemCompat.setActionView(item, R.layout.action_menu_count_layout);
        RelativeLayout notifCount = (RelativeLayout) MenuItemCompat.getActionView(item);

        ImageView iv = (ImageView) notifCount.findViewById(R.id.iv_roomservice);
        if(menuItem!=null){
            String imageUrl = menuItem.getImageUrl();
            if (!TextUtils.isEmpty(imageUrl)) {
                Picasso.with(this).load(imageUrl).into(iv);
            }
        }
        iv.setColorFilter(Color.WHITE);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderDetailActivity.startActivity(FoodItemActivity.this,gsId,staysId,hotelId,AppConstants.STAYS_TYPE.ROOM_SERVICE, AppConstants.FROM_ORDERING.ROOM_SERVICE);
            }
        });
        tv = (TextView) notifCount.findViewById(R.id.actionbar_notifcation_textview);
        tv.setVisibility(View.GONE);
        tv.setText("0");
        refreshCartData();
        return super.onCreateOptionsMenu(menu);
    }

    private void refreshCartData() {
        GetCartData cartData = RoomServiceCartResponse.getInstance();
        List<FoodItemOrderDetailList> foodItemOrderDetailList = cartData.getFoodItemOrderDetailList();
        if (foodItemOrderDetailList.size() > 0) {
            tv.setVisibility(View.VISIBLE);
            tv.setText(foodItemOrderDetailList.size() + "");
        } else {
            tv.setVisibility(View.GONE);
        }
        this.foodItemOrderDetailList.clear();
        this.foodItemOrderDetailList.addAll(foodItemOrderDetailList);
        checkIfAlreadyInCart();
        setData();
    }

    private void checkIfAlreadyInCart() {
        if(foodItem==null){
            return;
        }
        if (CollectionUtils.isNotEmpty(foodItemOrderDetailList)) {
            for (FoodItemOrderDetailList detailList : foodItemOrderDetailList) {
                String foodItemId = detailList.getFoodItemId();
                if (foodItemId.equals(foodItem.getFooditemId())) {
                    foodItem.setItemCount(detailList.getQuantity());
                }
            }
        }
    }

    private void setData() {
        if(foodItem==null){
            return;
        }
        SimpleRatingBar customRatingBar= (SimpleRatingBar) findViewById(R.id.ratingbar_quality);
        customRatingBar.setRating(foodItem.getType());
        tvCount.setText(foodItem.getItemCount() + "");
        tvTitle.setText(foodItem.getName());
        tvFullDesc.setText(foodItem.getDescription());
        //tvFullDesc.setLineSpacing(10, 1);
        String cuisineType = foodItem.getCuisineType();
        Integer serves = foodItem.getServes();
        tvServe.setText(cuisineType + " | " + "Serves " + serves);
        Double price = foodItem.getPrice();
        if (price != 0.0) {
            tvPrice.setText("\u20B9 " + price);
        } else {
            tvPrice.setText("\u20B9 " + "");
        }

        String imageUrl = foodItem.getImageUrl();
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(this).load(imageUrl).placeholder(R.drawable.progress_animation).into(ivMain);
        }

        Integer foodLabel = foodItem.getFoodLabel();
        if (foodLabel != 0 && foodLabel < 9) {
            FoodLabels statusByCode = FoodLabels.findStatusByCode(foodLabel);
            if (statusByCode != null) {
                int imgUrl = statusByCode.getImageUrl();
                String type = statusByCode.getType();
                ivVeg.setImageResource(imgUrl);
                tvVeg.setText(type);
                llItem2.setVisibility(View.VISIBLE);
            }
        } else {
            llItem2.setVisibility(View.GONE);
        }

        Integer spicyType = foodItem.getSpicyType();
        if (spicyType != 0 && spicyType < 9) {
            SpicyType statusByCode1 = SpicyType.findStatusByCode(spicyType);
            if (statusByCode1 != null) {
                int spicyUrl = statusByCode1.getImageUrl();
                String spicyType1 = statusByCode1.getSpicyType();
                tvSpicy.setText(spicyType1);
                ivChilly.setImageResource(spicyUrl);
                llItem1.setVisibility(View.VISIBLE);
            }
        } else {
            llItem1.setVisibility(View.GONE);
        }

        tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int itemCount = foodItem.getItemCount();
                foodItem.setItemCount(itemCount + 1);
                tvCount.setText(foodItem.getItemCount() + "");
                addItemToCart();
                GetCartData.refreshItem(foodItem,FoodItemActivity.this);
                refreshCartData();
            }
        });

        tvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int itemCount = foodItem.getItemCount();
                if (itemCount > 0) {
                    foodItem.setItemCount(itemCount - 1);
                    tvCount.setText(foodItem.getItemCount() + "");
                }else {
                    tvMinus.setVisibility(View.GONE);
                    tvAdd.setVisibility(View.VISIBLE);
                }
                addItemToCart();
                GetCartData.refreshItem(foodItem,FoodItemActivity.this);
                refreshCartData();
            }
        });

        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                foodItem.setItemCount(1);
                tvAdd.setVisibility(View.GONE);
                rlAddCount.setVisibility(View.VISIBLE);
//                rlBottomButton.setVisibility(View.VISIBLE);
//                btnAddtoList.setText("ADD TO LIST");
                tvCount.setText(foodItem.getItemCount() + "");
                addItemToCart();
                GetCartData.refreshItem(foodItem,FoodItemActivity.this);
                refreshCartData();
            }
        });

        if (foodItem.getItemCount() > 0) {
            tvAdd.setVisibility(View.GONE);
            rlAddCount.setVisibility(View.VISIBLE);
        } else {
            tvAdd.setVisibility(View.VISIBLE);
            rlAddCount.setVisibility(View.GONE);
        }

        Boolean recommended = foodItem.getRecommended();
        if (recommended == true) {
            llItem3.setVisibility(View.VISIBLE);
        } else {
            llItem3.setVisibility(View.GONE);
        }
        llGoesWellWith.removeAllViews();
        List<GoesWellItemData> listGoesWellItems = foodItem.getListGoesWellItems();
        if (listGoesWellItems != null && !listGoesWellItems.isEmpty()) {
            tvGoes.setVisibility(View.VISIBLE);
            for (final GoesWellItemData goesWellItemData : listGoesWellItems) {
                View view = getLayoutInflater().inflate(R.layout.row_goes_well_with, llGoesWellWith, false);
                ImageView ivFood = (ImageView) view.findViewById(R.id.iv_food);
                TextView tvFoodName = (TextView) view.findViewById(R.id.tv_food_name);

                Picasso.with(this).load(goesWellItemData.getImageUrl()).placeholder(R.drawable.progress_animation).into(ivFood);
                tvFoodName.setText(goesWellItemData.getName());

                llGoesWellWith.addView(view);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.FOOD_ITEM_DETAILS, foodItem);
                        bundle.putString(AppConstants.BUNDLE_KEYS.STAYS_ID, staysId);
                        bundle.putString(AppConstants.BUNDLE_KEYS.GS_ID, gsId);
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, generalServiceModel);
                        bundle.putSerializable(AppConstants.BUNDLE_KEYS.ROOM_SERVICE_TITLE,menuItem);
                        startNextActivity(bundle, FoodItemActivity.class);
                    }
                });

            }
        } else {
            tvGoes.setVisibility(View.GONE);
        }

    }

    private void findViews() {
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvPrice = (TextView) findViewById(R.id.tv_amount);
        tvServe = (TextView) findViewById(R.id.tv_location);
        tvFullDesc = (TextView) findViewById(R.id.tv_fulldescription);
        tvAdd = (TextView) findViewById(R.id.tv_add);
        tvSpicy = (TextView) findViewById(R.id.tv_spicy);
        tvVeg = (TextView) findViewById(R.id.tv_veg);
        tvRecommended = (TextView) findViewById(R.id.tv_recommended);
        tvGoes = (TextView) findViewById(R.id.tv_goes);
        tvMinus = (TextView) findViewById(R.id.tv_minus);
        tvCount = (TextView) findViewById(R.id.tv_count);
        tvPlus = (TextView) findViewById(R.id.tv_plus);
        rlAddCount = (RelativeLayout) findViewById(R.id.rl_add_count);
        rlBottomButton = (RelativeLayout) findViewById(R.id.rl_bottom_button);
        btnAddtoList = (Button) findViewById(R.id.btn_save_changes);
        ivChilly = (ImageView) findViewById(R.id.iv_chilli);
        ivVeg = (ImageView) findViewById(R.id.iv_veg_nonveg);
        ivRecomanded = (ImageView) findViewById(R.id.iv_like);
        ivMain = (ImageView) findViewById(R.id.iv_hotel_room);
        llItem1 = (LinearLayout) findViewById(R.id.ll_item1);
        llItem2 = (LinearLayout) findViewById(R.id.ll_item2);
        llItem3 = (LinearLayout) findViewById(R.id.ll_item3);
        llGoesWellWith = (LinearLayout) findViewById(R.id.ll_goes_well_with);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        StayCartUpdateReceiver.callOnClear(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onCartUpdate() {
        refreshCartData();
        getFoodItemsById();
    }

    @Override
    public void onCartClear() {
        getFoodItemsById();
    }
}