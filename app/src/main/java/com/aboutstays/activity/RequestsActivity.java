package com.aboutstays.activity;

import android.graphics.Typeface;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.RequestsClosedFragment;
import com.aboutstays.fragment.RequestsOpenFragment;
import com.aboutstays.fragment.RequestsScheduledFragment;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.rest.request.GenerateExpenseDashboardRequest;
import com.aboutstays.rest.request.RequestDashboardRequest;
import com.aboutstays.rest.response.GeneralDashboardItem;
import com.aboutstays.rest.response.GeneralServiceList;
import com.aboutstays.rest.response.GetRequestDashboardResponse;
import com.aboutstays.rest.response.RequestDashboardData;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.receivers.CartUpdateReceiver;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;

public class RequestsActivity extends AppBaseActivity implements CustomPagerAdapter.PagerAdapterInterface {

    List<String> tabs = new ArrayList<>();
    private CustomPagerAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private RequestDashboardRequest request;
    private RequestsOpenFragment openFragment = new RequestsOpenFragment();
    private RequestsScheduledFragment scheduledFragment = new RequestsScheduledFragment();
    private RequestsClosedFragment closedFragment = new RequestsClosedFragment();
    private RequestDashboardData dashboardData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests);
        registerReceiver(CartUpdateReceiver.ACTION_UPDATE_REQUEST);
        initToolBar("Requests");
        initTabs();

        request = new RequestDashboardRequest();
        UserSession session = UserSession.getSessionInstance();
        if(session != null){
            request.setUserId(session.getUserId());
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            GeneralServiceModel generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null && generalServiceModel.getHotelStaysList() != null
                    && generalServiceModel.getHotelStaysList().getStaysPojo() != null) {
                request.setStaysId(generalServiceModel.getHotelStaysList().getStaysPojo().getStaysId());
                request.setHotelId(generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId());
            }
        }

        tabLayout = (TabLayout) findViewById(R.id.tablayout_requests);
        viewPager = (ViewPager) findViewById(R.id.viewpager_requests);
        viewPager.setOffscreenPageLimit(3);
        adapter = new CustomPagerAdapter(getSupportFragmentManager(), tabs, this);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onReceive(Intent intent) {
        String action = intent.getAction();
        if(action.equals(CartUpdateReceiver.ACTION_UPDATE_REQUEST)){
            getRequestDashboard();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getRequestDashboard();
    }

    private void getRequestDashboard() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getRequestDashboard(request);
        executeTask(AppConstants.TASK_CODES.GET_REQUEST_DASHBOARD, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode){
            case AppConstants.TASK_CODES.GET_REQUEST_DASHBOARD:
                GetRequestDashboardResponse dashboardResponse = (GetRequestDashboardResponse) response;
                if(dashboardResponse != null && !dashboardResponse.getError()){
                    dashboardData = dashboardResponse.getData();
                    if(dashboardData != null){
                        List<GeneralDashboardItem> listDashboardItem = new ArrayList<>();
                        if(null!=dashboardData.getClosedDashboardItems()){
                            listDashboardItem.clear();
                            listDashboardItem.addAll(dashboardData.getClosedDashboardItems());
                            closedFragment.setRequestsList(listDashboardItem);
//                            closedFragment.refreshData();

                                String closedText = getString(R.string.tab_closed);
                                closedText = closedText + " (" + dashboardData.getClosedDashboardItems().size() + ")";
                                tabs.set(2, closedText);
                                adapter.notifyDataSetChanged();

                        }
                        if(null!=dashboardData.getOpenDashboardItems()){
                            listDashboardItem.clear();
                            listDashboardItem.addAll(dashboardData.getOpenDashboardItems());
                            openFragment.setOpenList(listDashboardItem);
//                            openFragment.refreshData();

                                String openText = getString(R.string.tab_open);
                                openText = openText + " (" + dashboardData.getOpenDashboardItems().size() + ")";
                                tabs.set(0, openText);
                                adapter.notifyDataSetChanged();

                        } else {
                            adapter.notifyDataSetChanged();
                        }
                        if(null!=dashboardData.getScheduledDashboardItems()){
                            listDashboardItem.clear();
                            listDashboardItem.addAll(dashboardData.getScheduledDashboardItems());
                            scheduledFragment.setListScheduledItems(listDashboardItem);
//                            scheduledFragment.refreshData();

                                String scheduledText = getString(R.string.tab_scheduled);
                                scheduledText = scheduledText + " (" + dashboardData.getScheduledDashboardItems().size() + ")";
                                tabs.set(1, scheduledText);
                                adapter.notifyDataSetChanged();
                        }
                    }
                }
                break;
        }
    }

    private void initTabs() {
        tabs.add(getString(R.string.tab_open));
        tabs.add(getString(R.string.tab_scheduled));
        tabs.add(getString(R.string.tab_closed));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            switch (requestCode){
                case AppConstants.REQUEST_CODE.ORDER_UPDATE:
                    getRequestDashboard();
                    break;
            }
        }
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        switch (position) {
            case 0:
                openFragment = RequestsOpenFragment.getInstance();
                return openFragment;
            case 1:
                scheduledFragment = RequestsScheduledFragment.getInstance();
                return scheduledFragment;
            case 2:
                closedFragment = RequestsClosedFragment.getInstance();
                return closedFragment;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return (CharSequence) listItem;
    }
}
