package com.aboutstays.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.aboutstays.R;

import simplifii.framework.activity.BaseActivity;

public class FaqDetailsActivity extends AppBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq_details);

        initToolBar(getString(R.string.faq_details));
    }
}
