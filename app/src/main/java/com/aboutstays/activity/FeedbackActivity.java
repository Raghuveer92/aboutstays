package com.aboutstays.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.feedback.FeedbackData;
import com.aboutstays.model.feedback.GetServiceFeedbackResponse;
import com.aboutstays.rest.request.SubmittFeedbackRequest;
import com.aboutstays.utitlity.ApiRequestGenerator;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.receivers.CartUpdateReceiver;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;

public class FeedbackActivity extends AppBaseActivity {

    private SeekBar qualityRating, timeTakenRating;
    private ImageView ivQuality1, ivQuality2, ivQuality3, ivQuality4, ivQuality5;
    private ImageView ivTimeTaken1, ivTimeTaken2, ivTimeTaken3, ivTimeTaken4, ivTimeTaken5;
    private TextView tvComment;
    private EditText etComment;
    int step = 1;
    int max = 5;
    int min = 1;
    private String uosId;
    private Button btnSubmit;
    private int quality = 1, timeTaken = 1;
    private String feedbackId;
    private boolean alreadySubmitted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        initToolBar("Feedback");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            uosId = bundle.getString(AppConstants.BUNDLE_KEYS.UOSID);
        }

        etComment = (EditText) findViewById(R.id.et_comments);
        tvComment = (TextView) findViewById(R.id.tv_comments);
        qualityRating = (SeekBar) findViewById(R.id.rangeSeekbar1);
        timeTakenRating = (SeekBar) findViewById(R.id.rangeSeekbar2);
        btnSubmit = (Button) findViewById(R.id.btn_submit_feedback);
        ivQuality1 = (ImageView) findViewById(R.id.iv_quality1);
        ivQuality2 = (ImageView) findViewById(R.id.iv_quality2);
        ivQuality3 = (ImageView) findViewById(R.id.iv_quality3);
        ivQuality4 = (ImageView) findViewById(R.id.iv_quality4);
        ivQuality5 = (ImageView) findViewById(R.id.iv_quality5);
        ivTimeTaken1 = (ImageView) findViewById(R.id.iv_time_taken1);
        ivTimeTaken2 = (ImageView) findViewById(R.id.iv_time_taken2);
        ivTimeTaken3 = (ImageView) findViewById(R.id.iv_time_taken3);
        ivTimeTaken4 = (ImageView) findViewById(R.id.iv_time_taken4);
        ivTimeTaken5 = (ImageView) findViewById(R.id.iv_time_taken5);

        qualityRating.setMax((max - min) / step);
        timeTakenRating.setMax((max - min) / step);




        qualityRating.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                quality = progress+1;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        timeTakenRating.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    timeTaken = progress+1;
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        if (!TextUtils.isEmpty(feedbackId)) {
            btnSubmit.setText(getString(R.string.update_feedback));
        } else {
            btnSubmit.setText(getString(R.string.submit_feedback));
        }
        btnSubmit.setOnClickListener(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAllFeedback();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit_feedback:
                submitFeedback();
                break;
        }
    }

    private void submitFeedback() {
        SubmittFeedbackRequest submittFeedbackRequest = new SubmittFeedbackRequest();
        submittFeedbackRequest.setUosId(uosId);
        if (quality == 0){
            showToast("");
        }
        submittFeedbackRequest.setQuality(quality);
        submittFeedbackRequest.setTimeTaken(timeTaken);
        String comment = etComment.getText().toString().trim();
        if (!TextUtils.isEmpty(comment)) {
            submittFeedbackRequest.setComments(comment);
        }

        if (!TextUtils.isEmpty(feedbackId)) {
            submittFeedbackRequest.setId(feedbackId);
            HttpParamObject httpParamObject = ApiRequestGenerator.submitFeedback(submittFeedbackRequest, feedbackId);
            executeTask(AppConstants.TASK_CODES.SUBMIT_FEEDBACK, httpParamObject);
        } else {
            HttpParamObject httpParamObject = ApiRequestGenerator.submitFeedback(submittFeedbackRequest, "");
            executeTask(AppConstants.TASK_CODES.SUBMIT_FEEDBACK, httpParamObject);
        }
    }

    private void getAllFeedback() {
        if (!TextUtils.isEmpty(uosId)) {
            HttpParamObject httpParamObject = ApiRequestGenerator.getServiceFeedback(uosId);
            executeTask(AppConstants.TASK_CODES.GET_FEEDBACK, httpParamObject);
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.server_error));
            return;
        }
        switch (taskCode) {
            case AppConstants.TASK_CODES.GET_FEEDBACK:
                GetServiceFeedbackResponse feedbackResponse = (GetServiceFeedbackResponse) response;
                if (feedbackResponse != null && feedbackResponse.getError() == false) {
                    FeedbackData feedbackData = feedbackResponse.getData();
                    if (feedbackData != null) {
                        feedbackId = feedbackData.getId();
                        Integer quality = feedbackData.getQuality();
                        Integer timeTaken = feedbackData.getTimeTaken();
                        qualityRating.setProgress(quality-1);
                        timeTakenRating.setProgress(timeTaken-1);
                        etComment.setText(feedbackData.getComments());
                        alreadySubmitted = true;
                    } else {
                        showToast("no data");
                    }
                }
                break;
            case AppConstants.TASK_CODES.SUBMIT_FEEDBACK:
                BaseApiResponse apiResponse = (BaseApiResponse) response;
                if (apiResponse != null && apiResponse.getError() == false) {
                    showToast("Thanks for Feedback");
                    CartUpdateReceiver.sendBroadcast(this,CartUpdateReceiver.ACTION_UPDATE_CHECKOUT);
                    finish();
                }
                break;
        }
        setSmileys();
    }

    private void setSmileys() {
        if(!alreadySubmitted) {
            setAllSmileysFaded();
            qualityRating.setProgress(2);
            timeTakenRating.setProgress(2);
        } else {
            btnSubmit.setText(getString(R.string.update_feedback));
        }
    }

    private void setAllSmileysFaded() {
        ivQuality1.setImageResource(R.mipmap.feedback_one_fade);
        ivQuality2.setImageResource(R.mipmap.feedback_two_fade);
        ivQuality3.setImageResource(R.mipmap.feedback_three_fade);
        ivQuality4.setImageResource(R.mipmap.feedback_four_fade);
        ivQuality5.setImageResource(R.mipmap.feedback_five_fade);
        ivTimeTaken1.setImageResource(R.mipmap.feedback_one_fade);
        ivTimeTaken2.setImageResource(R.mipmap.feedback_two_fade);
        ivTimeTaken3.setImageResource(R.mipmap.feedback_three_fade);
        ivTimeTaken4.setImageResource(R.mipmap.feedback_four_fade);
        ivTimeTaken5.setImageResource(R.mipmap.feedback_five_fade);
    }
}