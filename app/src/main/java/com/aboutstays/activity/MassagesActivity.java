package com.aboutstays.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.model.MassageModel;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;

public class MassagesActivity extends AppBaseActivity implements CustomListAdapterInterface {

    private ListView lvMassage;
    private CustomListAdapter listAdapter;
    private List<MassageModel> massageModelList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_massages);
        initToolBar(getString(R.string.massage));

        lvMassage = (ListView) findViewById(R.id.lv_masseges);
        listAdapter = new CustomListAdapter(this, R.layout.row_3_star, massageModelList, this);
        lvMassage.setAdapter(listAdapter);
        loadData();
    }

    private void loadData() {
        for (int i=0; i<5; i++){
            MassageModel massageModel = new MassageModel();
            massageModel.setTitle("Massage");
            massageModelList.add(massageModel);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
       Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_3_star, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        MassageModel massageModel = massageModelList.get(position);
        holder.tvTitle.setText(massageModel.getTitle());
        holder.tvPrice.setVisibility(View.VISIBLE);
        holder.tvDuration.setVisibility(View.VISIBLE);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startNextActivity(MasageNameActivity.class);
            }
        });
        return convertView;
    }

    class Holder{
        TextView tvTitle, tvTime, tvPrice, tvDuration;

        public Holder(View view){
            tvTitle = (TextView) view.findViewById(R.id.tv_hotel_name);
            tvTime = (TextView) view.findViewById(R.id.tv_hotel_address);
            tvPrice = (TextView) view.findViewById(R.id.tv_price);
            tvDuration = (TextView) view.findViewById(R.id.tv_person);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_option_spa, menu);
        return true;
    }
}
