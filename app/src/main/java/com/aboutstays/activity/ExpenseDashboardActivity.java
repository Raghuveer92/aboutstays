package com.aboutstays.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.widget.TextView;

import com.aboutstays.R;
import com.aboutstays.fragment.ByCategoryFragment;
import com.aboutstays.fragment.DateWiseFragment;
import com.aboutstays.model.GeneralServiceModel;
import com.aboutstays.rest.request.GenerateExpenseDashboardRequest;
import com.aboutstays.rest.response.CategoryBasedExpenseData;
import com.aboutstays.rest.response.DateBasedExpenseItem;
import com.aboutstays.rest.response.DateWiseExpenseData;
import com.aboutstays.rest.response.ExpenseItem;
import com.aboutstays.rest.response.GetCategoryBaseExpenseResponse;
import com.aboutstays.rest.response.GetDateWiseExpenseResponse;
import com.aboutstays.utitlity.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.rest.response.session.UserSession;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

public class ExpenseDashboardActivity extends AppBaseActivity implements CustomPagerAdapter.PagerAdapterInterface {


    List<String> tabs = new ArrayList<>();
    private CustomPagerAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private GeneralServiceModel generalServiceModel;
    private GenerateExpenseDashboardRequest expenseRequest;
    private TextView tvGrandTotal;
    private List<ExpenseItem> listExpenseItem;

    private ByCategoryFragment byCategoryFragment;
    private DateWiseFragment dateWiseFragment;
    private LinkedHashMap<String, DateBasedExpenseItem> dateVsExpenseItemMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expences);
        initToolBar("Expense Dashboard");
        initTabs();

        expenseRequest = new GenerateExpenseDashboardRequest();
        UserSession session = UserSession.getSessionInstance();
        if(session != null){
            expenseRequest.setUserId(session.getUserId());
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            generalServiceModel = (GeneralServiceModel) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
            if (generalServiceModel != null && generalServiceModel.getHotelStaysList() != null
                    && generalServiceModel.getHotelStaysList().getStaysPojo() != null) {
                expenseRequest.setStaysId(generalServiceModel.getHotelStaysList().getStaysPojo().getStaysId());
                expenseRequest.setHotelId(generalServiceModel.getHotelStaysList().getStaysPojo().getHotelId());
            }
        }

        tabLayout = (TabLayout) findViewById(R.id.tablayout_expence);
        viewPager = (ViewPager) findViewById(R.id.viewpager_expence);
        tvGrandTotal = (TextView) findViewById(R.id.tv_grand_total);
        adapter = new CustomPagerAdapter(getSupportFragmentManager(), tabs, this);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getCategoryBasedExpenses();
        getDateWiseExpenses();
    }

    private void getDateWiseExpenses() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getDateWiseExpense(expenseRequest);
        executeTask(AppConstants.TASK_CODES.GENERATE_DATEWISE_EXPENSE, httpParamObject);
    }

    private void getCategoryBasedExpenses() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getCategoryBasedExpenses(expenseRequest);
        executeTask(AppConstants.TASK_CODES.GENERATE_CATEGORY_BASED_EXPENSE, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

    private void initTabs() {
        tabs.add(getString(R.string.by_category));
        tabs.add(getString(R.string.date_wise));
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode){
            case AppConstants.TASK_CODES.GENERATE_CATEGORY_BASED_EXPENSE:
                GetCategoryBaseExpenseResponse categoryResponse = (GetCategoryBaseExpenseResponse)response;
                if(categoryResponse != null && categoryResponse.getError() == false){
                    CategoryBasedExpenseData expenseData = categoryResponse.getData();
                    if(expenseData != null){
                        String total = Util.formatDecimal(expenseData.getGrandTotal(), "0.00#");
                        tvGrandTotal.setText("\u20B9" + total);
                        if(CollectionUtils.isNotEmpty(expenseData.getListExpenseItem())){
                            listExpenseItem = new ArrayList<>();
                            listExpenseItem.addAll(expenseData.getListExpenseItem());
                            byCategoryFragment.setListExpenseItem(listExpenseItem);
                            byCategoryFragment.refreshData();
                        }
                    }
                }
                break;
            case AppConstants.TASK_CODES.GENERATE_DATEWISE_EXPENSE:
                GetDateWiseExpenseResponse dateWiseResponse = (GetDateWiseExpenseResponse) response;
                if(dateWiseResponse != null && dateWiseResponse.getError() == false){
                    DateWiseExpenseData expenseData = dateWiseResponse.getData();
                    if(expenseData != null){
                        String total = Util.formatDecimal(expenseData.getGrandTotal(), "0.00#");
                        tvGrandTotal.setText("\u20B9" + total);
                        if(expenseData.getDateVsExpenseItemList() != null && !expenseData.getDateVsExpenseItemList().isEmpty()){
                            dateVsExpenseItemMap = new LinkedHashMap<>();
                            dateVsExpenseItemMap.putAll(expenseData.getDateVsExpenseItemList());
                            dateWiseFragment.setDateVsExpenseItemMap(dateVsExpenseItemMap);
                            dateWiseFragment.refreshData();
                        }
                    }
                }
                break;
        }
    }

    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        switch (position) {
            case 0:
                byCategoryFragment = ByCategoryFragment.getInstance();
                return byCategoryFragment;
            case 1:
                dateWiseFragment = DateWiseFragment.getInstance();
                return dateWiseFragment;


        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return (CharSequence) listItem;
    }
}
