package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Rahul Aarya on 29-12-2016.
 */

public class ExpencesModelCategory implements Serializable {

    public int iv_title_image;

    public int getIv_title_image() {
        return iv_title_image;
    }

    public void setIv_title_image(int iv_title_image) {
        this.iv_title_image = iv_title_image;
    }

    public String getTv_title() {
        return tv_title;
    }

    public void setTv_title(String tv_title) {
        this.tv_title = tv_title;
    }

    public String getTv_amount() {
        return tv_amount;
    }

    public void setTv_amount(String tv_amount) {
        this.tv_amount = tv_amount;
    }

    public String tv_title, tv_amount;
}
