package com.aboutstays.model;

/**
 * Created by ajay on 8/12/16.
 */

public class HotelModel {
    String hotelName;
    String hotelAddrass;
    int  imgUrl;
    int rightImgUrl;

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelAddrass(String hotelAddrass) {
        this.hotelAddrass = hotelAddrass;
    }

    public String getHotelAddrass() {
        return hotelAddrass;
    }

    public void setImgUrl(int imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getImgUrl() {
        return imgUrl;
    }

    public void setRightImgUrl(int rightImgUrl) {
        this.rightImgUrl = rightImgUrl;
    }

    public int getRightImgUrl() {
        return rightImgUrl;
    }
}
