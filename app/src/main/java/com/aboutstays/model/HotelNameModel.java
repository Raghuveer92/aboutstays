package com.aboutstays.model;

/**
 * Created by neeraj on 24/2/17.
 */

public class HotelNameModel {
    int imgUrl;
    String name;

    public int getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(int imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
