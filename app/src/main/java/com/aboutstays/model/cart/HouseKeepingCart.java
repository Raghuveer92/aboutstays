package com.aboutstays.model.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by neeraj on 22/3/17.
 */

public class HouseKeepingCart extends BaseAddToCart {

    @SerializedName("data")
    @Expose
    private List<HouseKeepingCartData> data = null;

    public List<HouseKeepingCartData> getData() {
        return data;
    }

    public void setData(List<HouseKeepingCartData> data) {
        this.data = data;
    }

}
