package com.aboutstays.model.cart;

import android.content.Context;
import android.text.TextUtils;

import com.aboutstays.receiver.StayCartUpdateReceiver;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class LaundryCartResponse extends BaseApiResponse{

    private static LaundryCartResponse roomServiceCartResponse;
    @SerializedName("data")
    @Expose
    private LaundryCartItemData data;

    public LaundryCartItemData getData() {
        if(data==null){
            return new LaundryCartItemData();
        }
        return data;
    }

    public void setData(LaundryCartItemData data) {
        this.data = data;
    }
    public static LaundryCartResponse parseJson(String json) {
        Gson gson=new Gson();
        LaundryCartResponse roomServiceCartResponse = gson.fromJson(json, LaundryCartResponse.class);
        roomServiceCartResponse.save(roomServiceCartResponse.getData());
        return roomServiceCartResponse;
    }
    public static void save(LaundryCartItemData cartData){
        if(roomServiceCartResponse==null){
            roomServiceCartResponse=new LaundryCartResponse();
        }
        roomServiceCartResponse.setData(cartData);
        Gson gson=new Gson();
        Preferences.saveData(AppConstants.PREF_KEYS.ROOM_SERVICE_CART_DATA,gson.toJson(roomServiceCartResponse));
    }
    public static LaundryCartItemData getInstance(){
        if(roomServiceCartResponse!=null){
            return roomServiceCartResponse.getData();
        }
        Gson gson=new Gson();
        String json = Preferences.getData(AppConstants.PREF_KEYS.ROOM_SERVICE_CART_DATA, "");
        if(!TextUtils.isEmpty(json)){
            roomServiceCartResponse = gson.fromJson(json, LaundryCartResponse.class);
            return roomServiceCartResponse.getData();
        }
        return new LaundryCartItemData();
    }

    public static void clear(Context context) {
        LaundryCartItemData cartData = getInstance();
        cartData.getListLaundryItemOrderDetails().clear();
        save(cartData);
        StayCartUpdateReceiver.callOnUpdateCart(context);
        StayCartUpdateReceiver.callOnClear(context);
    }
}