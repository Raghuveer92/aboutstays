package com.aboutstays.model.cart;

import com.aboutstays.model.GeneralItemTypeInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HkItemOrderDetailsList extends GeneralItemTypeInfo{

    @SerializedName("hkItemId")
    @Expose
    private String hkItemId;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("price")
    @Expose
    private double price;
    @SerializedName("name")
    @Expose
    private String name;

    private boolean isFirst = false;

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public String getHkItemId() {
        return hkItemId;
    }

    public void setHkItemId(String hkItemId) {
        this.hkItemId = hkItemId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}