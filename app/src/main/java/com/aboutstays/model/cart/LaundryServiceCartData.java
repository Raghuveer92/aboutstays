package com.aboutstays.model.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by neeraj on 21/3/17.
 */

public class LaundryServiceCartData {

    @SerializedName("cartOperation")
    @Expose
    private Integer cartOperation;
    @SerializedName("cartType")
    @Expose
    private Integer cartType;
    @SerializedName("itemOrderDetails")
    @Expose
    private ItemOrderDetails itemOrderDetails;

    public Integer getCartOperation() {
        return cartOperation;
    }

    public void setCartOperation(Integer cartOperation) {
        this.cartOperation = cartOperation;
    }

    public Integer getCartType() {
        return cartType;
    }

    public void setCartType(Integer cartType) {
        this.cartType = cartType;
    }

    public ItemOrderDetails getItemOrderDetails() {
        return itemOrderDetails;
    }

    public void setItemOrderDetails(ItemOrderDetails itemOrderDetails) {
        this.itemOrderDetails = itemOrderDetails;
    }
}
