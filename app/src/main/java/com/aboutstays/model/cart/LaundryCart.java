package com.aboutstays.model.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by neeraj on 21/3/17.
 */

public class LaundryCart extends BaseAddToCart {

    @SerializedName("data")
    @Expose
    private List<LaundryServiceCartData> data = null;

    public List<LaundryServiceCartData> getData() {
        return data;
    }

    public void setData(List<LaundryServiceCartData> data) {
        this.data = data;
    }

}
