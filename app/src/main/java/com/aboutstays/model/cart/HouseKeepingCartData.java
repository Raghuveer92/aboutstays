package com.aboutstays.model.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by neeraj on 22/3/17.
 */

public class HouseKeepingCartData implements Serializable{

    @SerializedName("cartOperation")
    @Expose
    private Integer cartOperation;
    @SerializedName("cartType")
    @Expose
    private Integer cartType;
    @SerializedName("hkItemOrderDetails")
    @Expose
    private HkItemOrderDetails hkItemOrderDetails;

    public Integer getCartOperation() {
        return cartOperation;
    }

    public void setCartOperation(Integer cartOperation) {
        this.cartOperation = cartOperation;
    }

    public Integer getCartType() {
        return cartType;
    }

    public void setCartType(Integer cartType) {
        this.cartType = cartType;
    }

    public HkItemOrderDetails getHkItemOrderDetails() {
        return hkItemOrderDetails;
    }

    public void setHkItemOrderDetails(HkItemOrderDetails hkItemOrderDetails) {
        this.hkItemOrderDetails = hkItemOrderDetails;
    }

}
