package com.aboutstays.model.cart;

import com.aboutstays.model.GeneralItemTypeInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ListLaundryItemOrderDetail extends GeneralItemTypeInfo{

    @SerializedName("laundryItemId")
    @Expose
    private String laundryItemId;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("itemName")
    @Expose
    private String itemName;
    @SerializedName("sameDaySelected")
    @Expose
    private boolean sameDaySelected;

    public boolean isSameDaySelected() {
        return sameDaySelected;
    }

    public void setSameDaySelected(boolean sameDaySelected) {
        this.sameDaySelected = sameDaySelected;
    }

    private boolean isFirst = false;

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public String getLaundryItemId() {
        return laundryItemId;
    }

    public void setLaundryItemId(String laundryItemId) {
        this.laundryItemId = laundryItemId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

}