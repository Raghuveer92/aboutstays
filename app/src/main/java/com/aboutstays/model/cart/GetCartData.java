package com.aboutstays.model.cart;

import android.content.Context;

import com.aboutstays.model.food.FoodDataByType;
import com.aboutstays.receiver.StayCartUpdateReceiver;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by neeraj on 17/3/17.
 */

public class GetCartData implements Serializable{

    @SerializedName("cartType")
    @Expose
    private Integer cartType;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("staysId")
    @Expose
    private String staysId;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("foodItemOrderDetailList")
    @Expose
    private List<FoodItemOrderDetailList> foodItemOrderDetailList = new ArrayList<>();

    public Integer getCartType() {
        return cartType;
    }

    public void setCartType(Integer cartType) {
        this.cartType = cartType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStaysId() {
        return staysId;
    }

    public void setStaysId(String staysId) {
        this.staysId = staysId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public List<FoodItemOrderDetailList> getFoodItemOrderDetailList() {
        return foodItemOrderDetailList;
    }

    public void setFoodItemOrderDetailList(List<FoodItemOrderDetailList> foodItemOrderDetailList) {
        this.foodItemOrderDetailList = foodItemOrderDetailList;
    }
public void addItemToCart(FoodDataByType foodDataByType){
    FoodItemOrderDetailList data = new FoodItemOrderDetailList();
    data.setQuantity(foodDataByType.getItemCount());
    data.setType(foodDataByType.getType());
    data.setFoodItemId(foodDataByType.getFooditemId());
    data.setPrice((int)foodDataByType.getPrice());
    data.setTypeName(foodDataByType.getTypeName());
    data.setType(foodDataByType.getType());
    data.setItemName(foodDataByType.getName());
    foodItemOrderDetailList.add(data);
}
public void removeFromCart(FoodDataByType foodDataByType){
    FoodItemOrderDetailList selected = null;
    for(FoodItemOrderDetailList foodItemOrderDetailList:getFoodItemOrderDetailList()){
        if(foodItemOrderDetailList.getFoodItemId().equalsIgnoreCase(foodDataByType.getFooditemId())){
            selected=foodItemOrderDetailList;
        }
    }
    if(selected!=null){
        foodItemOrderDetailList.remove(selected);
    }
}
public static void refreshItem(FoodDataByType foodDataByType, Context context){
    GetCartData cartData = RoomServiceCartResponse.getInstance();
    FoodItemOrderDetailList selected = null;
    boolean isContain = false;
    for(FoodItemOrderDetailList foodItemOrderDetailList:cartData.getFoodItemOrderDetailList()){
        if(foodItemOrderDetailList.getFoodItemId().equalsIgnoreCase(foodDataByType.getFooditemId())){
            isContain=true;
            selected=foodItemOrderDetailList;
        }
    }
    if(isContain){
        if(foodDataByType.getItemCount()>0){
            selected.setQuantity(foodDataByType.getItemCount());
        }else {
            cartData.removeFromCart(foodDataByType);
        }
    }else {
        if(foodDataByType.getItemCount()>0){
            cartData.addItemToCart(foodDataByType);
        }
    }
    RoomServiceCartResponse.save(cartData);
    StayCartUpdateReceiver.callOnUpdateCart(context);
}
}
