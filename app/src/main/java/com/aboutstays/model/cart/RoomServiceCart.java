package com.aboutstays.model.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by neeraj on 21/3/17.
 */

public class RoomServiceCart extends BaseAddToCart{

    @SerializedName("data")
    @Expose
    private List<RoomServiceCartData> data = null;

    public List<RoomServiceCartData> getData() {
        return data;
    }

    public void setData(List<RoomServiceCartData> data) {
        this.data = data;
    }

}
