package com.aboutstays.model.cart;

import android.content.Context;
import android.text.TextUtils;

import com.aboutstays.receiver.StayCartUpdateReceiver;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class RoomServiceCartResponse extends BaseApiResponse {

    private static RoomServiceCartResponse roomServiceCartResponse;
    @SerializedName("data")
    @Expose
    private GetCartData data;

    public GetCartData getData() {
        if(data==null){
            return new GetCartData();
        }
        return data;
    }

    public void setData(GetCartData data) {
        this.data = data;
    }

    public static RoomServiceCartResponse parseJson(String json) {
        Gson gson=new Gson();
        RoomServiceCartResponse roomServiceCartResponse = gson.fromJson(json, RoomServiceCartResponse.class);
        roomServiceCartResponse.save(roomServiceCartResponse.getData());
        return roomServiceCartResponse;
    }
    public static void save(GetCartData cartData){
        if(roomServiceCartResponse==null){
            roomServiceCartResponse=new RoomServiceCartResponse();
        }
        roomServiceCartResponse.setData(cartData);
        Gson gson=new Gson();
        Preferences.saveData(AppConstants.PREF_KEYS.ROOM_SERVICE_CART_DATA,gson.toJson(roomServiceCartResponse));
    }
    public static GetCartData getInstance(){
        if(roomServiceCartResponse!=null){
            return roomServiceCartResponse.getData();
        }
        Gson gson=new Gson();
        String json = Preferences.getData(AppConstants.PREF_KEYS.ROOM_SERVICE_CART_DATA, "");
        if(!TextUtils.isEmpty(json)){
            roomServiceCartResponse = gson.fromJson(json, RoomServiceCartResponse.class);
            return roomServiceCartResponse.getData();
        }
        return new GetCartData();
    }

    public static void clear(Context context) {
        GetCartData cartData = getInstance();
        cartData.getFoodItemOrderDetailList().clear();
        save(cartData);
        StayCartUpdateReceiver.callOnUpdateCart(context);
        StayCartUpdateReceiver.callOnClear(context);
    }
}