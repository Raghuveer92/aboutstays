package com.aboutstays.model.cart;

import com.aboutstays.model.GeneralItemTypeInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by neeraj on 21/3/17.
 */

public class ItemOrderDetails extends GeneralItemTypeInfo{

    @SerializedName("laundryItemId")
    @Expose
    private String laundryItemId;
    @SerializedName("price")
    @Expose
    private double price;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("sameDaySelected")
    @Expose
    private boolean sameDaySelected;

    public boolean isSameDaySelected() {
        return sameDaySelected;
    }

    public void setSameDaySelected(boolean sameDaySelected) {
        this.sameDaySelected = sameDaySelected;
    }

    public String getLaundryItemId() {
        return laundryItemId;
    }

    public void setLaundryItemId(String laundryItemId) {
        this.laundryItemId = laundryItemId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
