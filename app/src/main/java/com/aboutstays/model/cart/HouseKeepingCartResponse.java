package com.aboutstays.model.cart;

import android.content.Context;
import android.text.TextUtils;

import com.aboutstays.receiver.StayCartUpdateReceiver;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class HouseKeepingCartResponse extends BaseApiResponse{

    private static HouseKeepingCartResponse roomServiceCartResponse;
    @SerializedName("data")
    @Expose
    private HouseKeepingCartResponseData data;

    public HouseKeepingCartResponseData getData() {
        if(data==null){
            return new HouseKeepingCartResponseData();
        }
        return data;
    }

    public void setData(HouseKeepingCartResponseData data) {
        this.data = data;
    }

    public static HouseKeepingCartResponse parseJson(String json) {
        Gson gson=new Gson();
        HouseKeepingCartResponse roomServiceCartResponse = gson.fromJson(json, HouseKeepingCartResponse.class);
        roomServiceCartResponse.save(roomServiceCartResponse.getData());
        return roomServiceCartResponse;
    }
    public static void save(HouseKeepingCartResponseData cartData){
        if(roomServiceCartResponse==null){
            roomServiceCartResponse=new HouseKeepingCartResponse();
        }
        roomServiceCartResponse.setData(cartData);
        Gson gson=new Gson();
        Preferences.saveData(AppConstants.PREF_KEYS.ROOM_SERVICE_CART_DATA,gson.toJson(roomServiceCartResponse));
    }
    public static HouseKeepingCartResponseData getInstance(){
        if(roomServiceCartResponse!=null){
            return roomServiceCartResponse.getData();
        }
        Gson gson=new Gson();
        String json = Preferences.getData(AppConstants.PREF_KEYS.ROOM_SERVICE_CART_DATA, "");
        if(!TextUtils.isEmpty(json)){
            roomServiceCartResponse = gson.fromJson(json, HouseKeepingCartResponse.class);
            return roomServiceCartResponse.getData();
        }
        return new HouseKeepingCartResponseData();
    }

    public static void clear(Context context) {
        HouseKeepingCartResponseData cartData = getInstance();
        cartData.getHkItemOrderDetailsList().clear();
        save(cartData);
        StayCartUpdateReceiver.callOnUpdateCart(context);
        StayCartUpdateReceiver.callOnClear(context);
    }
}