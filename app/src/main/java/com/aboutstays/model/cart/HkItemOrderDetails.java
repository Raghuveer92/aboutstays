package com.aboutstays.model.cart;

import com.aboutstays.model.GeneralItemTypeInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HkItemOrderDetails extends GeneralItemTypeInfo{

    @SerializedName("hkItemId")
    @Expose
    private String hkItemId;
    @SerializedName("price")
    @Expose
    private double price;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;

    public String getHkItemId() {
        return hkItemId;
    }

    public void setHkItemId(String hkItemId) {
        this.hkItemId = hkItemId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}