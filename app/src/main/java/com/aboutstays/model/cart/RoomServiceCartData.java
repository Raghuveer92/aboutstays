package com.aboutstays.model.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by neeraj on 18/3/17.
 */

public class RoomServiceCartData {

    @SerializedName("cartType")
    @Expose
    private Integer cartType;
    @SerializedName("cartOperation")
    @Expose
    private Integer cartOperation;
    @SerializedName("foodItemOrderDetails")
    @Expose
    private FoodItemOrderDetails foodItemOrderDetails;

    public Integer getCartType() {
        return cartType;
    }

    public void setCartType(Integer cartType) {
        this.cartType = cartType;
    }

    public Integer getCartOperation() {
        return cartOperation;
    }

    public void setCartOperation(Integer cartOperation) {
        this.cartOperation = cartOperation;
    }

    public FoodItemOrderDetails getFoodItemOrderDetails() {
        return foodItemOrderDetails;
    }

    public void setFoodItemOrderDetails(FoodItemOrderDetails foodItemOrderDetails) {
        this.foodItemOrderDetails = foodItemOrderDetails;
    }

}
