package com.aboutstays.model.cart;

import android.content.Context;

import com.aboutstays.model.food.FoodDataByType;
import com.aboutstays.model.laundry.LaundryItemData;
import com.aboutstays.receiver.StayCartUpdateReceiver;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by neeraj on 21/3/17.
 */

public class LaundryCartItemData implements Serializable {

    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("staysId")
    @Expose
    private String staysId;
    @SerializedName("gsId")
    @Expose
    private String gsId;
    @SerializedName("cartType")
    @Expose
    private Integer cartType;
    @SerializedName("listLaundryItemOrderDetails")
    @Expose
    private List<ListLaundryItemOrderDetail> listLaundryItemOrderDetails = new ArrayList<>();

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStaysId() {
        return staysId;
    }

    public void setStaysId(String staysId) {
        this.staysId = staysId;
    }

    public String getGsId() {
        return gsId;
    }

    public void setGsId(String gsId) {
        this.gsId = gsId;
    }

    public Integer getCartType() {
        return cartType;
    }

    public void setCartType(Integer cartType) {
        this.cartType = cartType;
    }

    public List<ListLaundryItemOrderDetail> getListLaundryItemOrderDetails() {
        return listLaundryItemOrderDetails;
    }

    public void setListLaundryItemOrderDetails(List<ListLaundryItemOrderDetail> listLaundryItemOrderDetails) {
        this.listLaundryItemOrderDetails = listLaundryItemOrderDetails;
    }

    public void addItemToCart(LaundryItemData foodDataByType, boolean fromSameDay) {
        ListLaundryItemOrderDetail data = new ListLaundryItemOrderDetail();
        data.setQuantity(foodDataByType.getQuqntity());
        data.setType(foodDataByType.getType());
        data.setLaundryItemId(foodDataByType.getItemId());
        data.setPrice(foodDataByType.getPrice());
        data.setTypeName(foodDataByType.getTypeName());
        data.setSameDaySelected(fromSameDay);
        listLaundryItemOrderDetails.add(data);
    }

    public void removeFromCart(LaundryItemData foodDataByType, boolean fromSameDay) {
        ListLaundryItemOrderDetail selected = null;
        for (ListLaundryItemOrderDetail foodItemOrderDetailList : getListLaundryItemOrderDetails()) {
            if (foodItemOrderDetailList.getLaundryItemId().equalsIgnoreCase(foodDataByType.getItemId())&&foodItemOrderDetailList.isSameDaySelected()==fromSameDay) {
                selected = foodItemOrderDetailList;
            }
        }
        if (selected != null) {
            listLaundryItemOrderDetails.remove(selected);
        }
    }

    public static void refreshItem(LaundryItemData foodDataByType, boolean fromSameDay, Context context) {
        LaundryCartItemData instance = LaundryCartResponse.getInstance();
        ListLaundryItemOrderDetail selected = null;
        boolean isContain = false;
        for (ListLaundryItemOrderDetail foodItemOrderDetailList : instance.getListLaundryItemOrderDetails()) {
            if (foodItemOrderDetailList.getLaundryItemId().equalsIgnoreCase(foodDataByType.getItemId())&&foodItemOrderDetailList.isSameDaySelected()==fromSameDay) {
                isContain = true;
                selected = foodItemOrderDetailList;
            }
        }
        if (isContain) {
            if (foodDataByType.getQuqntity() > 0) {
                selected.setQuantity(foodDataByType.getQuqntity());
            } else {
                instance.removeFromCart(foodDataByType,fromSameDay);
            }
        } else {
            if (foodDataByType.getQuqntity() > 0) {
                instance.addItemToCart(foodDataByType,fromSameDay);
            }
        }
        StayCartUpdateReceiver.callOnUpdateCart(context);
    }

}
