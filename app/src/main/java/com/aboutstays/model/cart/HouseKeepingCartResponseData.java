package com.aboutstays.model.cart;

import android.content.Context;

import com.aboutstays.model.food.FoodDataByType;
import com.aboutstays.model.housekeeping.HouseKeepingItemData;
import com.aboutstays.receiver.StayCartUpdateReceiver;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by neeraj on 22/3/17.
 */

public class HouseKeepingCartResponseData implements Serializable{

    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("staysId")
    @Expose
    private String staysId;
    @SerializedName("gsId")
    @Expose
    private Object gsId;
    @SerializedName("cartType")
    @Expose
    private Integer cartType;
    @SerializedName("hkItemOrderDetailsList")
    @Expose
    private List<HkItemOrderDetailsList> hkItemOrderDetailsList = new ArrayList<>();

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStaysId() {
        return staysId;
    }

    public void setStaysId(String staysId) {
        this.staysId = staysId;
    }

    public Object getGsId() {
        return gsId;
    }

    public void setGsId(Object gsId) {
        this.gsId = gsId;
    }

    public Integer getCartType() {
        return cartType;
    }

    public void setCartType(Integer cartType) {
        this.cartType = cartType;
    }

    public List<HkItemOrderDetailsList> getHkItemOrderDetailsList() {
        return hkItemOrderDetailsList;
    }

    public void setHkItemOrderDetailsList(List<HkItemOrderDetailsList> hkItemOrderDetailsList) {
        this.hkItemOrderDetailsList = hkItemOrderDetailsList;
    }

    public void addItemToCart(HouseKeepingItemData itemData){
        HkItemOrderDetailsList itemOrderDetails = new HkItemOrderDetailsList();
        itemOrderDetails.setHkItemId(itemData.getItemId());
        itemOrderDetails.setType(itemData.getType());
        itemOrderDetails.setPrice(itemData.getPrice());
        itemOrderDetails.setQuantity(itemData.getItemCount());
        hkItemOrderDetailsList.add(itemOrderDetails);
    }
    public void removeFromCart(HouseKeepingItemData itemData){
        HkItemOrderDetailsList selectedItem = null;
        for(HkItemOrderDetailsList hkItemOrderDetailsList:getHkItemOrderDetailsList()){
            if(hkItemOrderDetailsList.getHkItemId().equalsIgnoreCase(itemData.getItemId())){
                selectedItem=hkItemOrderDetailsList;
                break;
            }
        }
        if(selectedItem!=null){
            hkItemOrderDetailsList.remove(selectedItem);
        }
    }
    public static void refreshItem(HouseKeepingItemData houseKeepingItemData, Context context){
        HouseKeepingCartResponseData cartData = HouseKeepingCartResponse.getInstance();
        HkItemOrderDetailsList selected = null;
        boolean isContain = false;
        for(HkItemOrderDetailsList foodItemOrderDetailList:cartData.getHkItemOrderDetailsList()){
            if(foodItemOrderDetailList.getHkItemId().equalsIgnoreCase(houseKeepingItemData.getItemId())){
                isContain=true;
                selected=foodItemOrderDetailList;
            }
        }
        if(isContain){
            if(houseKeepingItemData.getItemCount()>0){
                selected.setQuantity(houseKeepingItemData.getItemCount());
            }else {
                cartData.removeFromCart(houseKeepingItemData);
            }
        }else {
            if(houseKeepingItemData.getItemCount()>0){
                cartData.addItemToCart(houseKeepingItemData);
            }
        }
        HouseKeepingCartResponse.save(cartData);
        StayCartUpdateReceiver.callOnUpdateCart(context);
    }

}
