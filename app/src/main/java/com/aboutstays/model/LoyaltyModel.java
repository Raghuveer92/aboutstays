package com.aboutstays.model;

/**
 * Created by ajay on 13/12/16.
 */
public class LoyaltyModel {

    String programNo;
    String points;
    String programType;
    int imgUrl;

    public void setProgramNo(String programNo) {
        this.programNo = programNo;
    }

    public String getProgramNo() {
        return programNo;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getPoints() {
        return points;
    }

    public void setProgramType(String programType) {
        this.programType = programType;
    }

    public String getProgramType() {
        return programType;
    }

    public void setImgUrl(int imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getImgUrl() {
        return imgUrl;
    }
}
