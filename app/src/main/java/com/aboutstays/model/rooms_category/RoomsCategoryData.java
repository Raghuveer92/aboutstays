package com.aboutstays.model.rooms_category;

import com.aboutstays.model.earlycheckIn.EarlyCheckInData;
import com.aboutstays.model.hotels.Amenity;
import com.aboutstays.model.lateCheckout.CheckoutInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by neeraj on 7/2/17.
 */

public class RoomsCategoryData implements Serializable {
    @SerializedName("roomCategoryId")
    @Expose
    private String roomCategoryId;

    @SerializedName("hotelId")
    @Expose
    private String hotelId;

    @SerializedName("categoryName")
    @Expose
    private String name;

    @SerializedName("categoryCode")
    @Expose
    private String code;

    @SerializedName("count")
    @Expose
    private String count;

    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("size")
    @Expose
    private String size;

    @SerializedName("sizeMetric")
    @Expose
    private String sizeMetric;

    @SerializedName("adultCapacity")
    @Expose
    private Integer adultCapacity;

    @SerializedName("childCapacity")
    @Expose
    private Integer childCapacity;

    @SerializedName("additionalImageUrls")
    @Expose
    private AdditionalImageUrls additionalImageUrls;

    @SerializedName("earlyCheckinInfo")
    @Expose
    private EarlyCheckInData earlyCheckInInfo;

    @SerializedName("lateCheckoutInfo")
    @Expose
    private CheckoutInfo lateCheckOutInfo;

    @SerializedName("inRoomAmenities")
    @Expose
    private List<Amenity> inRoomAmenities;

    @SerializedName("preferenceBasedAmenities")
    @Expose
    private List<Amenity> preferenceBasedAmenities;

    public String getRoomCategoryId() {
        return roomCategoryId;
    }

    public void setRoomCategoryId(String roomCategoryId) {
        this.roomCategoryId = roomCategoryId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSizeMetric() {
        return sizeMetric;
    }

    public void setSizeMetric(String sizeMetric) {
        this.sizeMetric = sizeMetric;
    }

    public Integer getAdultCapacity() {
        return adultCapacity;
    }

    public void setAdultCapacity(Integer adultCapacity) {
        this.adultCapacity = adultCapacity;
    }

    public Integer getChildCapacity() {
        return childCapacity;
    }

    public void setChildCapacity(Integer childCapacity) {
        this.childCapacity = childCapacity;
    }

    public AdditionalImageUrls getAdditionalImageUrls() {
        return additionalImageUrls;
    }

    public void setAdditionalImageUrls(AdditionalImageUrls additionalImageUrls) {
        this.additionalImageUrls = additionalImageUrls;
    }

    public EarlyCheckInData getEarlyCheckInInfo() {
        return earlyCheckInInfo;
    }

    public void setEarlyCheckInInfo(EarlyCheckInData earlyCheckInInfo) {
        this.earlyCheckInInfo = earlyCheckInInfo;
    }

    public CheckoutInfo getLateCheckOutInfo() {
        return lateCheckOutInfo;
    }

    public void setLateCheckOutInfo(CheckoutInfo lateCheckOutInfo) {
        this.lateCheckOutInfo = lateCheckOutInfo;
    }

    public List<Amenity> getInRoomAmenities() {
        return inRoomAmenities;
    }

    public void setInRoomAmenities(List<Amenity> inRoomAmenities) {
        this.inRoomAmenities = inRoomAmenities;
    }

    public List<Amenity> getPreferenceBasedAmenities() {
        return preferenceBasedAmenities;
    }

    public void setPreferenceBasedAmenities(List<Amenity> preferenceBasedAmenities) {
        this.preferenceBasedAmenities = preferenceBasedAmenities;
    }
}
