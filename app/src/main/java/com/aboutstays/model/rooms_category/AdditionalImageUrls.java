package com.aboutstays.model.rooms_category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AdditionalImageUrls  implements Serializable{

@SerializedName("imageUrls")
@Expose
private List<String> imageUrls = null;

public List<String> getImageUrls() {
return imageUrls;
}

public void setImageUrls(List<String> imageUrls) {
this.imageUrls = imageUrls;
}

}