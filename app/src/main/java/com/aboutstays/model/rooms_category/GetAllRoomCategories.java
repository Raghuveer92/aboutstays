package com.aboutstays.model.rooms_category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAllRoomCategories {

    @SerializedName("data")
    @Expose
    private List<RoomsCategoryData> data;

    public List<RoomsCategoryData> getData() {
        return data;
    }

    public void setData(List<RoomsCategoryData> data) {
        this.data = data;
    }
}