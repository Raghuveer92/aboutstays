package com.aboutstays.model.maintenance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class MaintenanceItemResponse extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private List<MaintenanceItemData> data = null;

    public List<MaintenanceItemData> getData() {
        return data;
    }

    public void setData(List<MaintenanceItemData> data) {
        this.data = data;
    }

}