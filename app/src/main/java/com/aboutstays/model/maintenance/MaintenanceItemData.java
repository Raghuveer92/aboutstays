package com.aboutstays.model.maintenance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by neeraj on 3/4/17.
 */

public class MaintenanceItemData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("maintenanceServiceId")
    @Expose
    private String maintenanceServiceId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("itemType")
    @Expose
    private Integer itemType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getMaintenanceServiceId() {
        return maintenanceServiceId;
    }

    public void setMaintenanceServiceId(String maintenanceServiceId) {
        this.maintenanceServiceId = maintenanceServiceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getItemType() {
        return itemType;
    }

    public void setItemType(Integer itemType) {
        this.itemType = itemType;
    }

}
