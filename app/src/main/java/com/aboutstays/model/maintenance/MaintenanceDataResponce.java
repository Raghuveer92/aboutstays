package com.aboutstays.model.maintenance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class MaintenanceDataResponce extends BaseApiResponse{

@SerializedName("data")
@Expose
private MaintenanceData data;

public MaintenanceData getData() {
return data;
}

public void setData(MaintenanceData data) {
this.data = data;
}

}