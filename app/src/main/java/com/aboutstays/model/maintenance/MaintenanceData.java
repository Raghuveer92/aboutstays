package com.aboutstays.model.maintenance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by neeraj on 1/4/17.
 */

public class MaintenanceData implements Serializable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("listMaintenanceItemType")
    @Expose
    private List<ListMaintenanceItemType> listMaintenanceItemType = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ListMaintenanceItemType> getListMaintenanceItemType() {
        return listMaintenanceItemType;
    }

    public void setListMaintenanceItemType(List<ListMaintenanceItemType> listMaintenanceItemType) {
        this.listMaintenanceItemType = listMaintenanceItemType;
    }

}
