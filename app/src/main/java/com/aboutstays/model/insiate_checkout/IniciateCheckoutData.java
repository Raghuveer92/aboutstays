package com.aboutstays.model.insiate_checkout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.pojo.Address;

/**
 * Created by admin on 4/12/17.
 */

public class IniciateCheckoutData {

    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("staysId")
    @Expose
    private String staysId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("officialTrip")
    @Expose
    private Boolean officialTrip;
    @SerializedName("mobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("roomNo")
    @Expose
    private Integer roomNo;
    @SerializedName("totalAmount")
    @Expose
    private Integer totalAmount;
    @SerializedName("grandTotal")
    @Expose
    private Integer grandTotal;
    @SerializedName("paymentMethod")
    @Expose
    private Integer paymentMethod;

    public String getHotelId() {
        return hotelId;
    }

    public String getUserId() {
        return userId;
    }

    public String getStaysId() {
        return staysId;
    }

    public String getId() {
        return id;
    }

    public Boolean getOfficialTrip() {
        return officialTrip;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public Address getAddress() {
        return address;
    }

    public Object getRoomNo() {
        return roomNo;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public Integer getGrandTotal() {
        return grandTotal;
    }

    public Integer getPaymentMethod() {
        return paymentMethod;
    }
}
