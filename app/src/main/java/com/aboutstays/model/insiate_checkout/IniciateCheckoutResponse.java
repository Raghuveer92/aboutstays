package com.aboutstays.model.insiate_checkout;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by admin on 4/12/17.
 */

public class IniciateCheckoutResponse extends BaseApiResponse {
    private IniciateCheckoutData data;

    public IniciateCheckoutData getData() {
        return data;
    }
}
