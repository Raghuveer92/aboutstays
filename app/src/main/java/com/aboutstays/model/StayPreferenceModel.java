package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by ajay on 13/12/16.
 */
public class StayPreferenceModel implements Serializable{

    int imgUrl;
    String preference;
    boolean isSelect;

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public void setImgUrl(int imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getImgUrl() {
        return imgUrl;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

    public String getPreference() {
        return preference;
    }
}
