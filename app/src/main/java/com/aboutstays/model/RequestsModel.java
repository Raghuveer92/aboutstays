package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Rahul Aarya on 26-12-2016.
 */

public class RequestsModel implements Serializable {

    public int getRequestsTitleImage() {
        return requestsTitleImage;
    }

    public void setRequestsTitleImage(int requestsTitleImage) {
        this.requestsTitleImage = requestsTitleImage;
    }

    public String getRequestsTitle() {
        return requestsTitle;
    }

    public void setRequestsTitle(String requestsTitle) {
        this.requestsTitle = requestsTitle;
    }

    public String getRequestsStatus() {
        return requestsStatus;
    }

    public void setRequestsStatus(String requestsStatus) {
        this.requestsStatus = requestsStatus;
    }

    public String getRequestsItems() {
        return requestsItems;
    }

    public void setRequestsItems(String requestsItems) {
        this.requestsItems = requestsItems;
    }

    public String getRequestsDate() {
        return requestsDate;
    }

    public void setRequestsDate(String requestsDate) {
        this.requestsDate = requestsDate;
    }

    public int requestsTitleImage;
    public  String requestsTitle, requestsStatus, requestsItems, requestsDate;


}
