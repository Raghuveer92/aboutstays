package com.aboutstays.model;

/**
 * Created by ajay on 9/12/16.
 */
public class StayModel {
    String status;
    String hotelName;
    String hotelAddress;
    int imgUrlLogo;
    int imgUrlRight;
    String fromDate;
    String toDate;
    String premium;
    int noOfPeople;


    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public void setImgUrlLogo(int imgUrlLogo) {
        this.imgUrlLogo = imgUrlLogo;
    }

    public int getImgUrlLogo() {
        return imgUrlLogo;
    }

    public void setImgUrlRight(int imgUrlRight) {
        this.imgUrlRight = imgUrlRight;
    }

    public int getImgUrlRight() {
        return imgUrlRight;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public String getPremium() {
        return premium;
    }

    public void setNoOfPeople(int noOfPeople) {
        this.noOfPeople = noOfPeople;
    }

    public int getNoOfPeople() {
        return noOfPeople;
    }
}