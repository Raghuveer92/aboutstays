package com.aboutstays.model.feedback;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class GetServiceFeedbackResponse extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private FeedbackData data;


    public FeedbackData getData() {
        return data;
    }

    public void setData(FeedbackData data) {
        this.data = data;
    }

}