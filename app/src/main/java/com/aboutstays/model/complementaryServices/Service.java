package com.aboutstays.model.complementaryServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aman on 22/04/17.
 */

public class Service {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("descriptionList")
    @Expose
    private List<String> descriptionList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getDescriptionList() {
        return descriptionList;
    }

    public void setDescriptionList(List<String> descriptionList) {
        this.descriptionList = descriptionList;
    }
}
