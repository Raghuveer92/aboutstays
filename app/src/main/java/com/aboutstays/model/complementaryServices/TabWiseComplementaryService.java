package com.aboutstays.model.complementaryServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aman on 22/04/17.
 */

public class TabWiseComplementaryService {

    @SerializedName("tabName")
    @Expose
    private String tabName;
    @SerializedName("services")
    @Expose
    private List<Service> services = null;

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }
}
