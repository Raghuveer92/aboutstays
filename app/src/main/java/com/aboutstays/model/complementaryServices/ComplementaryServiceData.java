package com.aboutstays.model.complementaryServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aman on 22/04/17.
 */

public class ComplementaryServiceData {

    @SerializedName("tabWiseComplementaryServices")
    @Expose
    private List<TabWiseComplementaryService> tabWiseComplementaryServices = null;
    @SerializedName("servicesId")
    @Expose
    private String servicesId;

    public List<TabWiseComplementaryService> getTabWiseComplementaryServices() {
        return tabWiseComplementaryServices;
    }

    public void setTabWiseComplementaryServices(List<TabWiseComplementaryService> tabWiseComplementaryServices) {
        this.tabWiseComplementaryServices = tabWiseComplementaryServices;
    }

    public String getServicesId() {
        return servicesId;
    }

    public void setServicesId(String servicesId) {
        this.servicesId = servicesId;
    }
}
