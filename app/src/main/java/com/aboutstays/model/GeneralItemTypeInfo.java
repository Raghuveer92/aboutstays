package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by neeraj on 23/3/17.
 */

public class GeneralItemTypeInfo implements Serializable{
    private boolean sameDayAvailable;
    private int type;
    private String typeName;
    private String imageUrl;

    public GeneralItemTypeInfo(){}

    public GeneralItemTypeInfo(boolean sameDayAvailable, int type, String typeName, String imageUrl) {
        this.sameDayAvailable = sameDayAvailable;
        this.type = type;
        this.typeName = typeName;
        this.imageUrl = imageUrl;
    }

    public boolean getSameDayAvailable() {
        return sameDayAvailable;
    }

    public void setSameDayAvailable(Boolean sameDayAvailable) {
        this.sameDayAvailable = sameDayAvailable;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
