package com.aboutstays.model.lateCheckout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aman on 21/04/17.
 */

public class RetreiveLateCheckoutData {

    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("staysId")
    @Expose
    private String staysId;
    @SerializedName("gsId")
    @Expose
    private String gsId;
    @SerializedName("deliveryTime")
    @Expose
    private Long deliveryTime;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("requestedNow")
    @Expose
    private boolean requestedNow;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("checkoutTime")
    @Expose
    private long checkoutTime;
    @SerializedName("checkoutDate")
    @Expose
    private long checkoutDate;
    @SerializedName("price")
    @Expose
    private double price;
    @SerializedName("status")
    @Expose
    private int status;

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStaysId() {
        return staysId;
    }

    public void setStaysId(String staysId) {
        this.staysId = staysId;
    }

    public String getGsId() {
        return gsId;
    }

    public void setGsId(String gsId) {
        this.gsId = gsId;
    }

    public Long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isRequestedNow() {
        return requestedNow;
    }

    public void setRequestedNow(boolean requestedNow) {
        this.requestedNow = requestedNow;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getCheckoutTime() {
        return checkoutTime;
    }

    public void setCheckoutTime(long checkoutTime) {
        this.checkoutTime = checkoutTime;
    }

    public long getCheckoutDate() {
        return checkoutDate;
    }

    public void setCheckoutDate(long checkoutDate) {
        this.checkoutDate = checkoutDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
