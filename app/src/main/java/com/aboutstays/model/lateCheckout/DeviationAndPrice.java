package com.aboutstays.model.lateCheckout;

/**
 * Created by aman on 06/04/17.
 */

public class DeviationAndPrice {

    private Long devaition;
    private double price;

    public Long getDevaition() {
        return devaition;
    }

    public void setDevaition(Long devaition) {
        this.devaition = devaition;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}

