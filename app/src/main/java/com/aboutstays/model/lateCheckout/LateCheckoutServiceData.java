package com.aboutstays.model.lateCheckout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aman on 06/04/17.
 */

public class LateCheckoutServiceData {

    @SerializedName("roomCategoryId")
    @Expose
    private String roomCategoryId;
    @SerializedName("checkOutInfo")
    @Expose
    private CheckoutInfo checkOutInfo;

    public String getRoomCategoryId() {
        return roomCategoryId;
    }

    public void setRoomCategoryId(String roomCategoryId) {
        this.roomCategoryId = roomCategoryId;
    }

    public CheckoutInfo getCheckOutInfo() {
        return checkOutInfo;
    }

    public void setCheckOutInfo(CheckoutInfo checkOutInfo) {
        this.checkOutInfo = checkOutInfo;
    }
}
