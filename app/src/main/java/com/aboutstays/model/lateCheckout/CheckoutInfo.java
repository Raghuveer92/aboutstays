package com.aboutstays.model.lateCheckout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by aman on 06/04/17.
 */

public class CheckoutInfo implements Serializable{

    @SerializedName("standardCheckoutTime")
    @Expose
    private String standardCheckoutTime;
    @SerializedName("listDeviationInfo")
    @Expose
    private List<DeviationInformation> listDeviationInfo = null;

    public String getStandardCheckoutTime() {
        return standardCheckoutTime;
    }

    public void setStandardCheckoutTime(String standardCheckoutTime) {
        this.standardCheckoutTime = standardCheckoutTime;
    }

    public List<DeviationInformation> getListDeviationInfo() {
        return listDeviationInfo;
    }

    public void setListDeviationInfo(List<DeviationInformation> listDeviationInfo) {
        this.listDeviationInfo = listDeviationInfo;
    }
}
