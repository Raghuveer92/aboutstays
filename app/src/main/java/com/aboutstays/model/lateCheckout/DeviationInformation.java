package com.aboutstays.model.lateCheckout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by aman on 06/04/17.
 */

public class DeviationInformation implements Serializable{

    @SerializedName("deviationInHours")
    @Expose
    private Double deviationInHours;
    @SerializedName("price")
    @Expose
    private Integer price;

    public Double getDeviationInHours() {
        return deviationInHours;
    }

    public void setDeviationInHours(Double deviationInHours) {
        this.deviationInHours = deviationInHours;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
