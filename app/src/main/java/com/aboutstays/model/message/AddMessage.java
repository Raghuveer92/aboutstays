package com.aboutstays.model.message;

import com.aboutstays.rest.request.BaseServiceRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mrnee on 4/29/2017.
 */

public class AddMessage extends BaseServiceRequest {
    @SerializedName("messageText")
    @Expose
    private String messageText;

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }
}
