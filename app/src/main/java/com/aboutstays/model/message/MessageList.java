package com.aboutstays.model.message;

import com.aboutstays.model.BaseAdapterModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import simplifii.framework.utility.AppConstants;

/**
 * Created by mrnee on 4/28/2017.
 */

public class MessageList extends BaseAdapterModel implements Serializable{
    @SerializedName("messageText")
    @Expose
    private String messageText;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("staysId")
    @Expose
    private String staysId;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("messageId")
    @Expose
    private String messageId;
    @SerializedName("byUser")
    @Expose
    private Boolean byUser;
    @SerializedName("created")
    @Expose
    private long created;

    private boolean isLastMessage = true;

    public boolean isLastMessage() {
        return isLastMessage;
    }

    public void setLastMessage(boolean lastMessage) {
        isLastMessage = lastMessage;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStaysId() {
        return staysId;
    }

    public void setStaysId(String staysId) {
        this.staysId = staysId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Boolean getByUser() {
        return byUser;
    }

    public void setByUser(Boolean byUser) {
        this.byUser = byUser;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    @Override
    public int getViewType() {
        if (byUser == true){
            return AppConstants.VIEW_TYPE.CHAT_OUTGOING_MSG;
        } else {
            return AppConstants.VIEW_TYPE.CHAT_INCOMMING_MSG;
        }
    }
}
