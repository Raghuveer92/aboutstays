package com.aboutstays.model.message;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class MessageApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private MessageData data;

    public MessageData getData() {
        return data;
    }

    public void setData(MessageData data) {
        this.data = data;
    }

}