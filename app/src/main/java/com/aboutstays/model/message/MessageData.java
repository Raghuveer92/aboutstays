package com.aboutstays.model.message;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mrnee on 4/28/2017.
 */

public class MessageData {
    @SerializedName("messages")
    @Expose
    private List<MessageList> messages = null;

    public List<MessageList> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageList> messages) {
        this.messages = messages;
    }
}
