package com.aboutstays.model;

/**
 * Created by Neeraj on 1/2/2017.
 */

public class ReservationModel {
    int icon;
    String tvTitle;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTvTitle() {
        return tvTitle;
    }

    public void setTvTitle(String tvTitle) {
        this.tvTitle = tvTitle;
    }
}
