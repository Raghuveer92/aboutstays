package com.aboutstays.model.currentStays;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aman on 22/04/17.
 */

public class GetAllServicesData {

    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("staysId")
    @Expose
    private String staysId;
    @SerializedName("servicesList")
    @Expose
    private List<StaysService> servicesList = null;
    @SerializedName("showCheckInButton")
    @Expose
    private boolean showCheckInButton;
    @SerializedName("showCheckoutButton")
    @Expose
    private boolean showCheckoutButton;
    @SerializedName("checkinButtonText")
    @Expose
    private String checkinButtonText;

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStaysId() {
        return staysId;
    }

    public void setStaysId(String staysId) {
        this.staysId = staysId;
    }

    public List<StaysService> getServicesList() {
        return servicesList;
    }

    public void setServicesList(List<StaysService> servicesList) {
        this.servicesList = servicesList;
    }

    public Boolean getShowCheckInButton() {
        return showCheckInButton;
    }

    public boolean isShowCheckInButton() {
        return showCheckInButton;
    }

    public void setShowCheckInButton(boolean showCheckInButton) {
        this.showCheckInButton = showCheckInButton;
    }

    public boolean isShowCheckoutButton() {
        return showCheckoutButton;
    }

    public void setShowCheckoutButton(boolean showCheckoutButton) {
        this.showCheckoutButton = showCheckoutButton;
    }

    public String getCheckinButtonText() {
        return checkinButtonText;
    }

    public void setCheckinButtonText(String checkinButtonText) {
        this.checkinButtonText = checkinButtonText;
    }
}
