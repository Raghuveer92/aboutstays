package com.aboutstays.model.currentStays;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aman on 22/04/17.
 */

public class StaysService {

    @SerializedName("gsId")
    @Expose
    private String gsId;
    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("serviceImageUrl")
    @Expose
    private String serviceImageUrl;
    @SerializedName("serviceType")
    @Expose
    private int serviceType;
    @SerializedName("clickable")
    @Expose
    private boolean clickable;
    @SerializedName("showTick")
    @Expose
    private boolean showTick;
    @SerializedName("showNumber")
    @Expose
    private boolean showNumber;
    @SerializedName("count")
    @Expose
    private int count;
    @SerializedName("generalInfo")
    @Expose
    private boolean generalInfo;
    @SerializedName("uiOrder")
    @Expose
    private int uiOrder;
    @SerializedName("staysStatus")
    @Expose
    private int staysStatus;
    @SerializedName("serviceId")
    @Expose
    private String serviceId;
    @SerializedName("relevantUosId")
    @Expose
    private String relevantUosId;
    @SerializedName("referenceId")
    @Expose
    private String referenceId;

    public String getGsId() {
        return gsId;
    }

    public void setGsId(String gsId) {
        this.gsId = gsId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceImageUrl() {
        return serviceImageUrl;
    }

    public void setServiceImageUrl(String serviceImageUrl) {
        this.serviceImageUrl = serviceImageUrl;
    }

    public int getServiceType() {
        return serviceType;
    }

    public void setServiceType(int serviceType) {
        this.serviceType = serviceType;
    }

    public boolean isClickable() {
        return clickable;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }

    public boolean isShowTick() {
        return showTick;
    }

    public void setShowTick(boolean showTick) {
        this.showTick = showTick;
    }

    public boolean isShowNumber() {
        return showNumber;
    }

    public void setShowNumber(boolean showNumber) {
        this.showNumber = showNumber;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isGeneralInfo() {
        return generalInfo;
    }

    public void setGeneralInfo(boolean generalInfo) {
        this.generalInfo = generalInfo;
    }

    public int getUiOrder() {
        return uiOrder;
    }

    public void setUiOrder(int uiOrder) {
        this.uiOrder = uiOrder;
    }

    public int getStaysStatus() {
        return staysStatus;
    }

    public void setStaysStatus(int staysStatus) {
        this.staysStatus = staysStatus;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getRelevantUosId() {
        return relevantUosId;
    }

    public void setRelevantUosId(String relevantUosId) {
        this.relevantUosId = relevantUosId;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }
}
