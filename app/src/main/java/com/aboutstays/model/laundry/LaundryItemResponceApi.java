package com.aboutstays.model.laundry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class LaundryItemResponceApi extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private List<LaundryItemData> data = null;

    public List<LaundryItemData> getData() {
        return data;
    }

    public void setData(List<LaundryItemData> data) {
        this.data = data;
    }

}