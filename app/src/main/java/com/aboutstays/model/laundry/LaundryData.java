package com.aboutstays.model.laundry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by neeraj on 10/3/17.
 */

public class LaundryData implements Serializable{

    @SerializedName("serviceId")
    @Expose
    private String serviceId;
    @SerializedName("supportedLaundryTypeList")
    @Expose
    private List<SupportedLaundryTypeList> supportedLaundryTypeList = null;
    @SerializedName("slaApplicable")
    @Expose
    private Boolean slaApplicable;
    @SerializedName("slaTime")
    @Expose
    private Integer slaTime;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("endTime")
    @Expose
    private String endTime;
    @SerializedName("swiftDeliveryEnabled")
    @Expose
    private boolean swiftDeliveryEnabled;
    @SerializedName("pickupByTime")
    @Expose
    private String pickupByTime;
    @SerializedName("swiftDeliveryMessage")
    @Expose
    private String swiftDeliveryMessage;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public List<SupportedLaundryTypeList> getSupportedLaundryTypeList() {
        return supportedLaundryTypeList;
    }

    public void setSupportedLaundryTypeList(List<SupportedLaundryTypeList> supportedLaundryTypeList) {
        this.supportedLaundryTypeList = supportedLaundryTypeList;
    }

    public Boolean getSlaApplicable() {
        return slaApplicable;
    }

    public void setSlaApplicable(Boolean slaApplicable) {
        this.slaApplicable = slaApplicable;
    }

    public Integer getSlaTime() {
        return slaTime;
    }

    public void setSlaTime(Integer slaTime) {
        this.slaTime = slaTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public boolean isSwiftDeliveryEnabled() {
        return swiftDeliveryEnabled;
    }

    public void setSwiftDeliveryEnabled(boolean swiftDeliveryEnabled) {
        this.swiftDeliveryEnabled = swiftDeliveryEnabled;
    }

    public String getPickupByTime() {
        return pickupByTime;
    }

    public void setPickupByTime(String pickupByTime) {
        this.pickupByTime = pickupByTime;
    }

    public String getSwiftDeliveryMessage() {
        return swiftDeliveryMessage;
    }

    public void setSwiftDeliveryMessage(String swiftDeliveryMessage) {
        this.swiftDeliveryMessage = swiftDeliveryMessage;
    }

}
