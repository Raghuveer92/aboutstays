package com.aboutstays.model.laundry;

import com.aboutstays.enums.LaundryFor;
import com.aboutstays.model.GeneralItemTypeInfo;
import com.aboutstays.model.cart.FoodItemOrderDetailList;
import com.aboutstays.model.food.FoodDataByType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by neeraj on 11/3/17.
 */

public class LaundryItemData extends GeneralItemTypeInfo{

    @SerializedName("itemId")
    @Expose
    private String itemId;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("laundryServicesId")
    @Expose
    private String laundryServicesId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("laundryFor")
    @Expose
    private Integer laundryFor;

    private String category;

    @SerializedName("mrp")
    @Expose
    private Double mrp;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("sameDayPrice")
    @Expose
    private Double sameDayPrice;
    @SerializedName("showOnApp")
    @Expose
    private Boolean showOnApp;
    @SerializedName("complementary")
    @Expose
    private Boolean complementary;

    private boolean isFirst = false;
    private boolean ifAdd = false;
    private int quqntity = 0;

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getSameDayPrice() {
        if (sameDayPrice == null){
            return 0d;
        }
        return sameDayPrice;
    }

    public void setSameDayPrice(Double sameDayPrice) {
        this.sameDayPrice = sameDayPrice;
    }

    public int getQuqntity() {
        return quqntity;
    }

    public void setQuqntity(int quqntity) {
        this.quqntity = quqntity;
    }

    public boolean isIfAdd() {
        return ifAdd;
    }

    public void setIfAdd(boolean ifAdd) {
        this.ifAdd = ifAdd;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getLaundryServicesId() {
        return laundryServicesId;
    }

    public void setLaundryServicesId(String laundryServicesId) {
        this.laundryServicesId = laundryServicesId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLaundryFor() {
        return laundryFor;
    }

    public void setLaundryFor(Integer laundryFor) {
        this.laundryFor = laundryFor;
    }

    public Double getMrp() {
        return mrp;
    }

    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getComplementary() {
        return complementary;
    }

    public void setComplementary(Boolean complementary) {
        this.complementary = complementary;
    }

    public String getCategory() {
        return category;
    }

    public Boolean getShowOnApp() {
        return showOnApp;
    }

    public void setShowOnApp(Boolean showOnApp) {
        this.showOnApp = showOnApp;
    }

    public String getTitle() {
        String displayName = null;
        if (laundryFor != 0 && laundryFor <4) {
            LaundryFor byCode = LaundryFor.getByCode(laundryFor);
            if (byCode != null) {
                displayName = byCode.getDisplayName();
            }
        }

        return displayName;
    }


    public boolean isSameDayAvailable() {
        if (getSameDayPrice() == null || getSameDayPrice() < 11){
            return false;
        }
        return true;
    }
}
