package com.aboutstays.model.laundry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by neeraj on 10/3/17.
 */

public class LaundryApiResponse extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private LaundryData data;

    public LaundryData getData() {
        return data;
    }

    public void setData(LaundryData data) {
        this.data = data;
    }


}
