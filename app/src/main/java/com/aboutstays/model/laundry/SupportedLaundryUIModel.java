package com.aboutstays.model.laundry;

/**
 * Created by admin on 5/12/17.
 */

public class SupportedLaundryUIModel extends SupportedLaundryTypeList{
    private boolean isNormalDay;
    private boolean showCount;

    public SupportedLaundryUIModel(SupportedLaundryTypeList laundryTypeList, boolean isNormalDay) {
        super(laundryTypeList);
        this.isNormalDay = isNormalDay;
    }

    public boolean isNormalDay() {
        return isNormalDay;
    }

    public void setNormalDay(boolean normalDay) {
        isNormalDay = normalDay;
    }

    public boolean isShowCount() {
        return showCount;
    }

    public void setShowCount(boolean showCount) {
        this.showCount = showCount;
    }
}
