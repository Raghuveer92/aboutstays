package com.aboutstays.model.laundry;

import com.aboutstays.model.GeneralItemTypeInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SupportedLaundryTypeList extends GeneralItemTypeInfo{

    private int itemCount = 0;

    @SerializedName("sameDaySelected")
    @Expose
    private boolean sameDaySelected;


    public SupportedLaundryTypeList(){}

    public SupportedLaundryTypeList(SupportedLaundryTypeList supportedLaundryTypeList){
        super(supportedLaundryTypeList.getSameDayAvailable(),supportedLaundryTypeList.getType(),supportedLaundryTypeList.getTypeName(),supportedLaundryTypeList.getImageUrl());
        this.itemCount = supportedLaundryTypeList.itemCount;
        this.sameDaySelected = supportedLaundryTypeList.isSameDaySelected();
    }

    public boolean isSameDaySelected() {
        return sameDaySelected;
    }

    public void setSameDaySelected(boolean sameDaySelected) {
        this.sameDaySelected = sameDaySelected;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

}