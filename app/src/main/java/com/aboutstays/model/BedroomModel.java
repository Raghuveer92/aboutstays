package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Rahul Aarya on 03-01-2017.
 */

public class BedroomModel implements Serializable {
    public String item,itemAdd;

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getItemAdd() {
        return itemAdd;
    }

    public void setItemAdd(String itemAdd) {
        this.itemAdd = itemAdd;
    }
}
