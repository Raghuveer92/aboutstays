package com.aboutstays.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import simplifii.framework.rest.response.pojo.IdentityDoc;

public class DeleteIdPojo implements Serializable{

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("identityDoc")
    @Expose
    private IdentityDoc identityDoc;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public IdentityDoc getIdentityDoc() {
        return identityDoc;
    }

    public void setIdentityDoc(IdentityDoc identityDoc) {
        this.identityDoc = identityDoc;
    }

}