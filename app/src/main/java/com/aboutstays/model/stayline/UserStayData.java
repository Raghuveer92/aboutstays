package com.aboutstays.model.stayline;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by neeraj on 16/2/17.
 */

public class UserStayData implements Serializable{
    @SerializedName("hotelStaysList")
    @Expose
    private List<HotelStaysList> hotelStaysList = null;
    @SerializedName("nights")
    @Expose
    private int nights;
    @SerializedName("hotels")
    @Expose
    private int hotels;

    public List<HotelStaysList> getHotelStaysList() {
        return hotelStaysList;
    }

    public void setHotelStaysList(List<HotelStaysList> hotelStaysList) {
        this.hotelStaysList = hotelStaysList;
    }

    public int getNights() {
        return nights;
    }

    public void setNights(int nights) {
        this.nights = nights;
    }

    public int getHotels() {
        return hotels;
    }

    public void setHotels(int hotels) {
        this.hotels = hotels;
    }
}
