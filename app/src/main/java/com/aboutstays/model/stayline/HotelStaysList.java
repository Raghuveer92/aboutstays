package com.aboutstays.model.stayline;

import com.aboutstays.model.BaseAdapterModel;
import com.aboutstays.model.rooms_category.RoomsCategoryData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class HotelStaysList extends BaseAdapterModel implements Serializable {

    @SerializedName("staysPojo")
    @Expose
    private GetStayResponse staysPojo;
    @SerializedName("generalInfo")
    @Expose
    private GeneralInfo generalInfo;
    @SerializedName("roomPojo")
    @Expose
    private RoomsCategoryData roomPojo;
    @SerializedName("actualCheckedIn")
    @Expose
    private String actualCheckedIn;
    @SerializedName("actualCheckedOut")
    @Expose
    private String actualCheckedOut;

    private Integer hotelType;
    private Integer partnershipType;


    private boolean ifFutureStay = false;
    private boolean isFirst = false;
    private int colorId;
    private boolean isPast = false;

    public GetStayResponse getStaysPojo() {
        return staysPojo;
    }

    public void setStaysPojo(GetStayResponse staysPojo) {
        this.staysPojo = staysPojo;
    }



    public GeneralInfo getGeneralInfo() {
        return generalInfo;
    }

    public void setGeneralInfo(GeneralInfo generalInfo) {
        this.generalInfo = generalInfo;
    }

    public boolean isIfFutureStay() {
        return ifFutureStay;
    }

    public void setIfFutureStay(boolean ifFutureStay) {
        this.ifFutureStay = ifFutureStay;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }


        public RoomsCategoryData getRoomPojo() {
            return roomPojo;
        }

    public void setRoomPojo(RoomsCategoryData roomPojo) {
        this.roomPojo = roomPojo;
    }

    public boolean isCurrentStay() {
        Date date = null;
        try {
            date = Util.convertStringToDate(getStaysPojo().getCheckOutDate(), "dd-MM-yyyy");
            Date todayDate = new Date(System.currentTimeMillis());
            boolean before = todayDate.before(date);
            if (before) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isFutureStay() {
        Date date = null;
        try {
            date = Util.convertStringToDate(getStaysPojo().getCheckInDate(), "dd-MM-yyyy");
            Date afterDaysAgo = new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 2);
            boolean before = afterDaysAgo.before(date);
            if (before) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public String getStayTitle() {
        if(staysPojo != null) {
            if (staysPojo.getStaysOrdering() == AppConstants.STAYS_ORDERING.FUTURE) {
                return "Future Stay";
            } else if (staysPojo.getStaysOrdering() == AppConstants.STAYS_ORDERING.CURRENT) {
                return "Current Stay";
            } else {
                return "Past Stay";
            }
        }

//        if (isFutureStay()) {
//            return "Future Stay";
//        } else if (isCurrentStay()){
//            return "Current Stay";
//        } else {
//            return "Past Stay";
//        }
          return "";
    }

    public Integer getHotelType() {
        if(hotelType==null){
            return 0;
        }
        return hotelType;
    }

    public Integer getPartnershipType() {
        if(partnershipType ==null){
            return 0;
        }
        return partnershipType;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public int getColorId() {
        return colorId;
    }

    public boolean isPast() {
        return isPast;
    }

    public void setPast(boolean past) {
        isPast = past;
    }

    public String getActualCheckedIn() {
        return actualCheckedIn;
    }

    public void setActualCheckedIn(String actualCheckedIn) {
        this.actualCheckedIn = actualCheckedIn;
    }

    public String getActualCheckedOut() {
        return actualCheckedOut;
    }

    public void setActualCheckedOut(String actualCheckedOut) {
        this.actualCheckedOut = actualCheckedOut;
    }

    public void setHotelType(Integer hotelType) {
        this.hotelType = hotelType;
    }

    public void setPartnershipType(Integer partnershipType) {
        this.partnershipType = partnershipType;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.STAYS;
    }


    public boolean isEmptyStayPojo() {
        Date date = new Date();
        if (staysPojo != null){
            return false;
        }
        return true;
    }
}