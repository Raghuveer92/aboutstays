package com.aboutstays.model.stayline;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aman on 24/04/17.
 */

public class GetStayResponse extends StaysPojo {

    @SerializedName("staysOrdering")
    @Expose
    private int staysOrdering;
    @SerializedName("official")
    @Expose
    private boolean official;

    public boolean isOfficial() {
        return official;
    }

    public void setOfficial(boolean official) {
        this.official = official;
    }

    public int getStaysOrdering() {
        return staysOrdering;
    }

    public void setStaysOrdering(int staysOrdering) {
        this.staysOrdering = staysOrdering;
    }
}
