package com.aboutstays.model.stayline;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StaysPojo implements Serializable{

@SerializedName("staysId")
@Expose
private String staysId;
@SerializedName("bookingId")
@Expose
private String bookingId;
@SerializedName("hotelId")
@Expose
private String hotelId;
@SerializedName("roomId")
@Expose
private String roomId;
@SerializedName("userId")
@Expose
private String userId;
@SerializedName("checkInDate")
@Expose
private String checkInDate;
@SerializedName("checkInTime")
@Expose
private String checkInTime;
@SerializedName("checkOutTime")
@Expose
private String checkOutTime;
@SerializedName("checkOutDate")
@Expose
private String checkOutDate;


public String getStaysId() {
return staysId;
}

public void setStaysId(String staysId) {
this.staysId = staysId;
}

public String getBookingId() {
return bookingId;
}

public void setBookingId(String bookingId) {
this.bookingId = bookingId;
}

public String getHotelId() {
return hotelId;
}

public void setHotelId(String hotelId) {
this.hotelId = hotelId;
}

public String getRoomId() {
return roomId;
}

public void setRoomId(String roomId) {
this.roomId = roomId;
}

public String getUserId() {
return userId;
}

public void setUserId(String userId) {
this.userId = userId;
}

public String getCheckInDate() {
return checkInDate;
}

public void setCheckInDate(String checkInDate) {
this.checkInDate = checkInDate;
}

public String getCheckInTime() {
return checkInTime;
}

public void setCheckInTime(String checkInTime) {
this.checkInTime = checkInTime;
}

public String getCheckOutTime() {
return checkOutTime;
}

public void setCheckOutTime(String checkOutTime) {
this.checkOutTime = checkOutTime;
}

public String getCheckOutDate() {
return checkOutDate;
}

public void setCheckOutDate(String checkOutDate) {
this.checkOutDate = checkOutDate;
}


}