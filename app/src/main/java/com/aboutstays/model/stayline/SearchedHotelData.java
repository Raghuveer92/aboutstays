package com.aboutstays.model.stayline;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class SearchedHotelData extends BaseApiResponse implements Serializable{

@SerializedName("data")
@Expose
private SearchedData data;


public SearchedData getData() {
return data;
}

public void setData(SearchedData data) {
this.data = data;
}

}