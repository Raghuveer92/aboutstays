package com.aboutstays.model.stayline;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by neeraj on 15/2/17.
 */

public class SearchAmenity implements Serializable{


    @SerializedName("amenityId")
    @Expose
    private String amenityId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("iconUrl")
    @Expose
    private String iconUrl;
    @SerializedName("type")
    @Expose
    private String type;

    public String getAmenityId() {
        return amenityId;
    }

    public void setAmenityId(String amenityId) {
        this.amenityId = amenityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
