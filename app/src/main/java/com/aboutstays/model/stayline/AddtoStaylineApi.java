package com.aboutstays.model.stayline;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class AddtoStaylineApi extends BaseApiResponse implements Serializable {

@SerializedName("data")
@Expose
private StayData data;

public StayData getData() {
return data;
}

public void setData(StayData data) {
this.data = data;
}


}