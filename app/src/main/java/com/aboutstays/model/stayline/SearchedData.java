package com.aboutstays.model.stayline;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by neeraj on 15/2/17.
 */

public class SearchedData implements Serializable{

    @SerializedName("hotelBasicInfoList")
    @Expose
    private List<HotelBasicInfo> hotelBasicInfo = null;

    public List<HotelBasicInfo> getHotelBasicInfo() {
        return hotelBasicInfo;
    }

    public void setHotelBasicInfo(List<HotelBasicInfo> hotelBasicInfo) {
        this.hotelBasicInfo = hotelBasicInfo;
    }
}
