package com.aboutstays.model.stayline;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

public class GetStayApi extends BaseApiResponse {
    List<HotelStaysList> upcomingStayList = new ArrayList<>();
    List<HotelStaysList> pastStayList = new ArrayList<>();
    @SerializedName("data")
    @Expose
    private UserStayData data;


    public UserStayData getData() {
        return data;
    }

    public void setData(UserStayData data) {
        this.data = data;
    }

    public List<HotelStaysList> getUpcomingStayList() {
        return upcomingStayList;
    }

    public List<HotelStaysList> getPastStayList() {
        return pastStayList;
    }

    public static GetStayApi parseJson(String json) {
        Gson gson=new Gson();
        GetStayApi getStayApi = gson.fromJson(json, GetStayApi.class);
        List<HotelStaysList> upcomingStayList = getStayApi.getUpcomingStayList();
        List<HotelStaysList> pastStayList = getStayApi.getPastStayList();
        UserStayData data = getStayApi.getData();
        if(data!=null){

        }
        List<HotelStaysList> hotelStaysList = data.getHotelStaysList();
        if (hotelStaysList != null){
            for (HotelStaysList staysList : hotelStaysList){
                GetStayResponse staysPojo = staysList.getStaysPojo();
                if (staysPojo != null) {
                    int ordering = staysPojo.getStaysOrdering();
                    if(ordering == AppConstants.STAYS_ORDERING.PAST) {
                        pastStayList.add(staysList);
                        staysList.setPast(true);
                    } else {
                        upcomingStayList.add(staysList);
                    }

//                                    try {
//                                        Date date = Util.convertStringToDate(checkOutDate, BASE_DATE_FORMAT);
//
//                                        Date date1 = new Date();
//                                        boolean before = date1.before(date);
//                                        if (before) {
//                                            upcomingStayList.add(staysList);
//                                        } else {
//                                            pastStayList.add(staysList);
//                                            staysList.setPast(true);
//                                        }
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
                }
            }
        }
        getStayApi.sortPastStaysList();
        return getStayApi;
    }
    private void sortPastStaysList() {
        if (CollectionUtils.isNotEmpty(pastStayList)) {
            Collections.sort(pastStayList, new Comparator<HotelStaysList>() {
                @Override
                public int compare(HotelStaysList o1, HotelStaysList o2) {
                    if (o1.getStaysPojo() == null && o2.getStaysPojo() == null) {
                        return 0;
                    }
                    Long checkInTimeO1 = getLongFromHotelStaysPojo(o1);
                    Long checkInTimeO2 = getLongFromHotelStaysPojo(o2);
                    return checkInTimeO2.compareTo(checkInTimeO1);
                }
            });
        }
    }
    private Long getLongFromHotelStaysPojo(HotelStaysList hotelStays) {
        String checkInDate = hotelStays.getStaysPojo().getCheckInDate();
        String checkInTime = hotelStays.getStaysPojo().getCheckInTime();
        String combinedTime = checkInDate + " " + checkInTime;
        String DATE_AND_TIME_FORMAT = Util.API_DATE_FORMAT + " " + Util.CHECK_IN_TIME_API_FORMAT;
        return Util.convertStringToLong(DATE_AND_TIME_FORMAT, combinedTime);
    }


}
