package com.aboutstays.model.stayline;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by neeraj on 15/2/17.
 */

public class SearchHotelList implements Serializable{

    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("partnershipType")
    @Expose
    private int partnershipType;
    @SerializedName("generalInformation")
    @Expose
    private GeneralSearchInformation generalInformation;

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public int getPartnershipType() {
        return partnershipType;
    }

    public void setPartnershipType(int partnershipType) {
        this.partnershipType = partnershipType;
    }

    public GeneralSearchInformation getGeneralInformation() {
        return generalInformation;
    }

    public void setGeneralInformation(GeneralSearchInformation generalInformation) {
        this.generalInformation = generalInformation;
    }

}
