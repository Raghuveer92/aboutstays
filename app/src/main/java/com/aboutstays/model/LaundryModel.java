package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Rahul Aarya on 02-01-2017.
 */

public class LaundryModel implements Serializable {

    public int imageTitle, imageArrow;
    public String textTitle;

    public int getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(int imageTitle) {
        this.imageTitle = imageTitle;
    }

    public int getImageArrow() {
        return imageArrow;
    }

    public void setImageArrow(int imageArrow) {
        this.imageArrow = imageArrow;
    }

    public String getTextTitle() {
        return textTitle;
    }

    public void setTextTitle(String textTitle) {
        this.textTitle = textTitle;
    }
}

