package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Rahul Aarya on 05-01-2017.
 */

public class OpenModel implements Serializable {

    public int imageTitle,imageStatus;
    public String textTitle, textDateTime, textItems, textStatus;

    public int getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(int imageTitle) {
        this.imageTitle = imageTitle;
    }

    public int getImageStatus() {
        return imageStatus;
    }

    public void setImageStatus(int imageStatus) {
        this.imageStatus = imageStatus;
    }

    public String getTextTitle() {
        return textTitle;
    }

    public void setTextTitle(String textTitle) {
        this.textTitle = textTitle;
    }

    public String getTextDateTime() {
        return textDateTime;
    }

    public void setTextDateTime(String textDateTime) {
        this.textDateTime = textDateTime;
    }

    public String getTextItems() {
        return textItems;
    }

    public void setTextItems(String textItems) {
        this.textItems = textItems;
    }

    public String getTextStatus() {
        return textStatus;
    }

    public void setTextStatus(String textStatus) {
        this.textStatus = textStatus;
    }
}
