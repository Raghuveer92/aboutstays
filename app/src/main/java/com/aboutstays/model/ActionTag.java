package com.aboutstays.model;

import com.plumillonforge.android.chipview.Chip;

/**
 * Created by ajay on 24/12/16.
 */

public class ActionTag implements Chip {

    String imageUrl;
    String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String getText() {
        return title;
    }
}

