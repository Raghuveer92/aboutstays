package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Rahul Aarya on 05-01-2017.
 */

public class InternetModel implements Serializable {
    public String textTitle, textOffer, textCost, textButton;

    public String getTextTitle() {
        return textTitle;
    }

    public void setTextTitle(String textTitle) {
        this.textTitle = textTitle;
    }

    public String getTextOffer() {
        return textOffer;
    }

    public void setTextOffer(String textOffer) {
        this.textOffer = textOffer;
    }

    public String getTextCost() {
        return textCost;
    }

    public void setTextCost(String textCost) {
        this.textCost = textCost;
    }

    public String getTextButton() {
        return textButton;
    }

    public void setTextButton(String textButton) {
        this.textButton = textButton;
    }
}
