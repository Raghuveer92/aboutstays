package com.aboutstays.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.aboutstays.model.stayline.HotelStaysList;
import com.aboutstays.rest.response.GeneralServiceData;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import simplifii.framework.utility.AppConstants;

/**
 * Created by aman on 01/03/17.
 */

public class GeneralServiceModel extends BaseAdapterModel implements Serializable{

    private GeneralServiceData generalServiceData;
    private boolean showTickable;
    private boolean showNumerable;
    private int numerableValue;
    private boolean isUserOpted;
    private HotelStaysList hotelStaysList;
    private boolean active;
    private String uosId;
    private int headCount;
    private boolean isPast;

    private boolean showTick;
    private boolean showNumber;
    private boolean clickable;
    private String referenceId;


    public String getUosId() {
        return uosId;
    }

    public void setUosId(String uosId) {
        this.uosId = uosId;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.CHECKIN_SERVICE_DATA;
    }

    public GeneralServiceData getGeneralServiceData() {
        return generalServiceData;
    }

    public void setGeneralServiceData(GeneralServiceData generalServiceData) {
        this.generalServiceData = generalServiceData;
    }

    public boolean isShowTickable() {
        return showTickable;
    }

    public void setShowTickable(boolean showTickable) {
        this.showTickable = showTickable;
    }

    public boolean isShowNumerable() {
        return showNumerable;
    }

    public void setShowNumerable(boolean showNumerable) {
        this.showNumerable = showNumerable;
    }

    public int getNumerableValue() {
        return numerableValue;
    }

    public void setNumerableValue(int numerableValue) {
        this.numerableValue = numerableValue;
    }

    public boolean isUserOpted() {
        return isUserOpted;
    }

    public void setUserOpted(boolean userOpted) {
        isUserOpted = userOpted;
    }

    public HotelStaysList getHotelStaysList() {
        return hotelStaysList;
    }

    public void setHotelStaysList(HotelStaysList hotelStaysList) {
        this.hotelStaysList = hotelStaysList;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getHeadCount() {
        return headCount;
    }

    public void setHeadCount(int headCount) {
        this.headCount = headCount;
    }

    public boolean isPast() {
        return isPast;
    }

    public void setPast(boolean past) {
        isPast = past;
    }

    public boolean isShowTick() {
        return showTick;
    }

    public void setShowTick(boolean showTick) {
        this.showTick = showTick;
    }

    public boolean isShowNumber() {
        return showNumber;
    }

    public void setShowNumber(boolean showNumber) {
        this.showNumber = showNumber;
    }

    public boolean isClickable() {
        return clickable;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }
}
