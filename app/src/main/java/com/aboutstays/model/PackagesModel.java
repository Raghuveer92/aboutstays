package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Neeraj on 1/2/2017.
 */

public class PackagesModel implements Serializable {

    int ivOption1, ivOption2, ivBackground;
    String title, time, price, duration;

    public int getIvOption1() {
        return ivOption1;
    }

    public void setIvOption1(int ivOption1) {
        this.ivOption1 = ivOption1;
    }

    public int getIvOption2() {
        return ivOption2;
    }

    public void setIvOption2(int ivOption2) {
        this.ivOption2 = ivOption2;
    }

    public int getIvBackground() {
        return ivBackground;
    }

    public void setIvBackground(int ivBackground) {
        this.ivBackground = ivBackground;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
