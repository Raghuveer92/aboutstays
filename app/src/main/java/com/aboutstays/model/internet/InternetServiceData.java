package com.aboutstays.model.internet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by neeraj on 18/3/17.
 */

public class InternetServiceData {

    @SerializedName("listPacks")
    @Expose
    private List<InternetPackList> listPacks = null;
    @SerializedName("id")
    @Expose
    private String id;

    public List<InternetPackList> getListPacks() {
        return listPacks;
    }

    public void setListPacks(List<InternetPackList> listPacks) {
        this.listPacks = listPacks;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
