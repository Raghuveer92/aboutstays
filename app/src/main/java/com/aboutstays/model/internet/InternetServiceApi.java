package com.aboutstays.model.internet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class InternetServiceApi extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private InternetServiceData data;

    public InternetServiceData getData() {
        return data;
    }

    public void setData(InternetServiceData data) {
        this.data = data;
    }

}