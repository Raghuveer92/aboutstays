package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Rahul Aarya on 23-12-2016.
 */

public class NotificationsModel implements Serializable{

    String NotificationTitle;

    public String getNotificationTitle() {
        return NotificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        NotificationTitle = notificationTitle;
    }

    public String getNotifivationDateTime() {
        return NotifivationDateTime;
    }

    public void setNotifivationDateTime(String notifivationDateTime) {
        NotifivationDateTime = notifivationDateTime;
    }

    public String getNotificationsDummyText() {
        return notificationsDummyText;
    }

    public void setNotificationsDummyText(String notificationsDummyText) {
        this.notificationsDummyText = notificationsDummyText;
    }

    public int getRightImgUrl() {
        return rightImgUrl;
    }

    public void setRightImgUrl(int rightImgUrl) {
        this.rightImgUrl = rightImgUrl;
    }

    String NotifivationDateTime;
    String  notificationsDummyText;
    int rightImgUrl;
}
