package com.aboutstays.model;

/**
 * Created by Neeraj on 1/2/2017.
 */

public class ServiceModel {
    String tvTitle;

    public String getTvTitle() {
        return tvTitle;
    }

    public void setTvTitle(String tvTitle) {
        this.tvTitle = tvTitle;
    }
}
