package com.aboutstays.model.oder_detail;

import com.aboutstays.model.cart.FoodItemOrderDetailList;
import com.aboutstays.model.cart.GetCartData;
import com.aboutstays.model.cart.HkItemOrderDetailsList;
import com.aboutstays.model.cart.HouseKeepingCartResponseData;
import com.aboutstays.model.cart.LaundryCartItemData;
import com.aboutstays.model.cart.ListLaundryItemOrderDetail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 4/5/17.
 */

public class CartData implements Serializable{
    private Integer cartType=new Integer(0);
    private String userId;
    private String staysId;
    private String hotelId;
    private List<OrderDetail> detailList=new ArrayList<>();

    public CartData(Integer cartType, String userId, String staysId, String hotelId) {
        this.cartType = cartType;
        this.userId = userId;
        this.staysId = staysId;
        this.hotelId = hotelId;
    }

    public int getCartType() {
        if(cartType==null){
            return 0;
        }
        return cartType;
    }

    public String getUserId() {
        return userId;
    }

    public String getStaysId() {
        return staysId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public List<OrderDetail> getDetailList() {
        return detailList;
    }

    private void setFoodItemList(List<FoodItemOrderDetailList> foodItemOrderDetailLists){
        if(foodItemOrderDetailLists==null){
            return;
        }
        for (FoodItemOrderDetailList orderDetailList:foodItemOrderDetailLists){
            OrderDetail orderDetail = new OrderDetail(
                    orderDetailList.getType(),
                    orderDetailList.getTypeName(),
                    orderDetailList.getImageUrl(),
                    orderDetailList.getFoodItemId(),
                    orderDetailList.getItemName(),
                    orderDetailList.getQuantity(),
                    orderDetailList.getPrice()
            );
            this.detailList.add(orderDetail);
        }
    }
    private void setHouskeepingList(List<HkItemOrderDetailsList> hkItemOrderDetailsList) {
        if(hkItemOrderDetailsList==null){
            return;
        }
        for (HkItemOrderDetailsList detailsList:hkItemOrderDetailsList){
            OrderDetail orderDetail = new OrderDetail(
                    detailsList.getType(),
                    detailsList.getTypeName(),
                    detailsList.getImageUrl(),
                    detailsList.getHkItemId(),
                    detailsList.getName(),
                    detailsList.getQuantity(),
                    detailsList.getPrice()
            );
            detailList.add(orderDetail);
        }
    }
    private void setLaundyItemList(List<ListLaundryItemOrderDetail> listLaundryItemOrderDetails) {
        if(listLaundryItemOrderDetails==null){
            return;
        }
        for (ListLaundryItemOrderDetail detailsList:listLaundryItemOrderDetails){
            OrderDetail orderDetail = new OrderDetail(
                    detailsList.getType(),
                    detailsList.getTypeName(),
                    detailsList.getImageUrl(),
                    detailsList.getLaundryItemId(),
                    detailsList.getItemName(),
                    detailsList.getQuantity(),
                    detailsList.getPrice(),
                    detailsList.isSameDaySelected()
            );
            detailList.add(orderDetail);
        }
    }



    public static CartData getCartData(GetCartData getCartData){
        CartData cartData=new CartData(
                getCartData.getCartType(),
                getCartData.getUserId(),
                getCartData.getStaysId(),
                getCartData.getHotelId()
        );
        cartData.setFoodItemList(getCartData.getFoodItemOrderDetailList());
        return cartData;
    }

    public static CartData getCartData(HouseKeepingCartResponseData keepingCartResponseData) {
        CartData cartData=new CartData(
                keepingCartResponseData.getCartType(),
                keepingCartResponseData.getUserId(),
                keepingCartResponseData.getStaysId(),
                keepingCartResponseData.getHotelId()
        );
        cartData.setHouskeepingList(keepingCartResponseData.getHkItemOrderDetailsList());
        return cartData;
    }


    public static CartData getCartData(LaundryCartItemData laundryCartData) {
        CartData cartData=new CartData(
                laundryCartData.getCartType(),
                laundryCartData.getUserId(),
                laundryCartData.getStaysId(),
                laundryCartData.getHotelId()
        );
        cartData.setLaundyItemList(laundryCartData.getListLaundryItemOrderDetails());
        return cartData;
    }

}
