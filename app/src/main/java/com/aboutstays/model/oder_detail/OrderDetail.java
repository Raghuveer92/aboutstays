package com.aboutstays.model.oder_detail;

import java.io.Serializable;

/**
 * Created by admin on 4/5/17.
 */

public class OrderDetail implements Serializable{
    private int type;
    private String typeName;
    private String imageUrl;

    private String itemId;
    private String itemName;
    private int quantity;
    private double price;
    private boolean sameDay;

    public OrderDetail() {
    }

    public OrderDetail(int type, String typeName, String imageUrl, String itemId, String itemName, int quantity, double price, boolean sameDay) {
        this.type = type;
        this.typeName = typeName;
        this.imageUrl = imageUrl;
        this.itemId = itemId;
        this.itemName = itemName;
        this.quantity = quantity;
        this.price = price;
        this.sameDay = sameDay;
    }

    public OrderDetail(int type, String typeName, String imageUrl, String itemId, String itemName, int quantity, double price) {
        this.type = type;
        this.typeName = typeName;
        this.imageUrl = imageUrl;
        this.itemId = itemId;
        this.itemName = itemName;
        this.quantity = quantity;
        this.price = price;
    }

    public boolean isSameDay() {
        return sameDay;
    }

    public void setSameDay(boolean sameDay) {
        this.sameDay = sameDay;
    }

    public String getItemId() {
        return itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public int getType() {
        return type;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
