package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by neeraj on 22/2/17.
 */

public class IdentityDocument implements Serializable{

    String name, docNumber, docUrl, nameOnDoc;
    private Integer docType;

    public String getNameOnDoc() {
        return nameOnDoc;
    }

    public void setNameOnDoc(String nameOnDoc) {
        this.nameOnDoc = nameOnDoc;
    }

    public Integer getDocType() {
        return docType;
    }

    public void setDocType(Integer docType) {
        this.docType = docType;
    }

    public String getDocUrl() {
        return docUrl;
    }

    public void setDocUrl(String docUrl) {
        this.docUrl = docUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return docNumber;
    }

    public void setNumber(String number) {
        this.docNumber = number;
    }
}
