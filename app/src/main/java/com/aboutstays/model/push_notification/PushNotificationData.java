package com.aboutstays.model.push_notification;

import android.text.TextUtils;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import simplifii.framework.utility.Preferences;

/**
 * Created by admin on 2/28/17.
 */

public class PushNotificationData implements Serializable{
    private String type;
    private String title;
    private String description;
    private String metadata;
    private static final long serialVersionUID =2581769576570732954L;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public void save() {
        Gson gson=new Gson();
        String json = gson.toJson(this);
        JSONArray jsonArray = new JSONArray();
        String data = Preferences.getData(type, "");
        if(!TextUtils.isEmpty(data)){
            try {
                jsonArray=new JSONArray(data);
                jsonArray.put(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            jsonArray.put(json);
        }
        Preferences.saveData(type,jsonArray.toString());
    }
    public static List<PushNotificationData> getSavedNotification(String type){
        Gson gson=new Gson();
        List<PushNotificationData> pushNotificationDatas=new ArrayList<>();
        String data = Preferences.getData(type, "");
        try {
            JSONArray jsonArray=new JSONArray(data);
            for(int x=0;x<jsonArray.length();x++){
                String json = jsonArray.getString(x);
                PushNotificationData pushNotificationData = gson.fromJson(json, PushNotificationData.class);
                pushNotificationDatas.add(pushNotificationData);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return pushNotificationDatas;
    }
}
