package com.aboutstays.model.earlycheckIn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class EarlyCheckInResponse extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private EarlyCheckInData data;

    public EarlyCheckInData getData() {
        return data;
    }

    public void setData(EarlyCheckInData data) {
        this.data = data;
    }

}