package com.aboutstays.model.earlycheckIn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ListDeviationInfo implements Serializable{

    @SerializedName("deviationInHours")
    @Expose
    private Double deviationInHours;
    @SerializedName("price")
    @Expose
    private Integer price;

    public Double getDeviationInHours() {
        return deviationInHours;
    }

    public void setDeviationInHours(Double deviationInHours) {
        this.deviationInHours = deviationInHours;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

}