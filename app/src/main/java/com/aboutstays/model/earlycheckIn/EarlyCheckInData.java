package com.aboutstays.model.earlycheckIn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by neeraj on 12/4/17.
 */

public class EarlyCheckInData implements Serializable {

    @SerializedName("roomCategoryId")
    @Expose
    private String roomCategoryId;
    @SerializedName("checkInInfo")
    @Expose
    private CheckInInfo checkInInfo;

    public String getRoomCategoryId() {
        return roomCategoryId;
    }

    public void setRoomCategoryId(String roomCategoryId) {
        this.roomCategoryId = roomCategoryId;
    }

    public CheckInInfo getCheckInInfo() {
        return checkInInfo;
    }

    public void setCheckInInfo(CheckInInfo checkInInfo) {
        this.checkInInfo = checkInInfo;
    }

}
