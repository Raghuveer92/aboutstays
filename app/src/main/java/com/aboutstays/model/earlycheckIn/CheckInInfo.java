package com.aboutstays.model.earlycheckIn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CheckInInfo implements Serializable{

    @SerializedName("standardCheckinTime")
    @Expose
    private String standardCheckinTime;
    @SerializedName("listDeviationInfo")
    @Expose
    private List<ListDeviationInfo> listDeviationInfo = null;

    public String getStandardCheckinTime() {
        return standardCheckinTime;
    }

    public void setStandardCheckinTime(String standardCheckinTime) {
        this.standardCheckinTime = standardCheckinTime;
    }

    public List<ListDeviationInfo> getListDeviationInfo() {
        return listDeviationInfo;
    }

    public void setListDeviationInfo(List<ListDeviationInfo> listDeviationInfo) {
        this.listDeviationInfo = listDeviationInfo;
    }

}