package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Rahul Aarya on 02-01-2017.
 */

public class HousekeepingeModel implements Serializable {

    public int imgTitle, imgArrow;
    public String textTitle;

    public int getImgTitle() {
        return imgTitle;
    }

    public void setImgTitle(int imgTitle) {
        this.imgTitle = imgTitle;
    }

    public int getImgArrow() {
        return imgArrow;
    }

    public void setImgArrow(int imgArrow) {
        this.imgArrow = imgArrow;
    }

    public String getTextTitle() {
        return textTitle;
    }

    public void setTextTitle(String textTitle) {
        this.textTitle = textTitle;
    }
}
