package com.aboutstays.model;

import java.io.Serializable;

import simplifii.framework.utility.AppConstants;

/**
 * Created by Neeraj on 1/2/2017.
 */

public class StayServiceModel extends BaseAdapterModel implements Serializable {

    String title;
    int logo, logoSelect;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public int getLogoSelect() {
        return logoSelect;
    }

    public void setLogoSelect(int logoSelect) {
        this.logoSelect = logoSelect;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_TYPE.STAY_SERVICE_DATA;
    }
}
