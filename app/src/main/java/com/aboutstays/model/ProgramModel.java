package com.aboutstays.model;

/**
 * Created by my on 11-12-2016.
 */
public class ProgramModel {
    String title;
    String subtitle;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
}
