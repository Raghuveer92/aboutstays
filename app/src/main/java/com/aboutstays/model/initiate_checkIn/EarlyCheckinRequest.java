package com.aboutstays.model.initiate_checkIn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EarlyCheckinRequest implements Serializable{

    @SerializedName("scheduledCheckinTime")
    @Expose
    private String scheduledCheckinTime;
    @SerializedName("requestedCheckinTime")
    @Expose
    private String requestedCheckinTime;
    @SerializedName("charges")
    @Expose
    private double charges;
    @SerializedName("earlyCheckinRequested")
    @Expose
    private Boolean earlyCheckinRequested;

    public String getScheduledCheckinTime() {
        return scheduledCheckinTime;
    }

    public void setScheduledCheckinTime(String scheduledCheckinTime) {
        this.scheduledCheckinTime = scheduledCheckinTime;
    }

    public String getRequestedCheckinTime() {
        return requestedCheckinTime;
    }

    public void setRequestedCheckinTime(String requestedCheckinTime) {
        this.requestedCheckinTime = requestedCheckinTime;
    }

    public double getCharges() {
        return charges;
    }

    public void setCharges(double charges) {
        this.charges = charges;
    }

    public Boolean getEarlyCheckinRequested() {
        return earlyCheckinRequested;
    }

    public void setEarlyCheckinRequested(Boolean earlyCheckinRequested) {
        this.earlyCheckinRequested = earlyCheckinRequested;
    }

}