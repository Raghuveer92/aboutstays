package com.aboutstays.model.initiate_checkIn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListStayPreference {

    @SerializedName("preference")
    @Expose
    private Integer preference;

    public Integer getPreference() {
        return preference;
    }

    public void setPreference(Integer preference) {
        this.preference = preference;
    }
}