package com.aboutstays.model.initiate_checkIn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class InitiateCheckInResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private CheckInData data;

    public CheckInData getData() {
        return data;
    }

    public void setData(CheckInData data) {
        this.data = data;
    }

}