package com.aboutstays.model.initiate_checkIn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import simplifii.framework.rest.response.pojo.IdentityDoc;
import simplifii.framework.rest.response.requests.StayPref;
import simplifii.framework.rest.response.response.UserProfileData;

/**
 * Created by neeraj on 12/4/17.
 */

public class CheckInData implements Serializable{
    @SerializedName("checkinId")
    @Expose
    private String checkinId;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("staysId")
    @Expose
    private String staysId;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("personalInformation")
    @Expose
    private UserProfileData personalInformation;
    @SerializedName("identityDocuments")
    @Expose
    private List<IdentityDoc> identityDocuments = null;
    @SerializedName("listStayPreferences")
    @Expose
    private List<StayPref> listStayPreferences = null;
    @SerializedName("specialRequests")
    @Expose
    private List<SpecialRequest> specialRequests = null;
    @SerializedName("officialTrip")
    @Expose
    private Boolean officialTrip;
    @SerializedName("companies")
    @Expose
    private List<Company> companies = null;
    @SerializedName("earlyCheckinRequest")
    @Expose
    private EarlyCheckinRequest earlyCheckinRequest;
    @SerializedName("created")
    @Expose
    private Long created;
    @SerializedName("updated")
    @Expose
    private Long updated;

    public String getCheckinId() {
        return checkinId;
    }

    public void setCheckinId(String checkinId) {
        this.checkinId = checkinId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStaysId() {
        return staysId;
    }

    public void setStaysId(String staysId) {
        this.staysId = staysId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public UserProfileData getPersonalInformation() {
        return personalInformation;
    }

    public void setPersonalInformation(UserProfileData personalInformation) {
        this.personalInformation = personalInformation;
    }

    public List<IdentityDoc> getIdentityDocuments() {
        return identityDocuments;
    }

    public void setIdentityDocuments(List<IdentityDoc> identityDocuments) {
        this.identityDocuments = identityDocuments;
    }

    public List<StayPref> getListStayPreferences() {
        return listStayPreferences;
    }

    public void setListStayPreferences(List<StayPref> listStayPreferences) {
        this.listStayPreferences = listStayPreferences;
    }

    public List<SpecialRequest> getSpecialRequests() {
        return specialRequests;
    }

    public void setSpecialRequests(List<SpecialRequest> specialRequests) {
        this.specialRequests = specialRequests;
    }

    public Boolean getOfficialTrip() {
        return officialTrip;
    }

    public void setOfficialTrip(Boolean officialTrip) {
        this.officialTrip = officialTrip;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public EarlyCheckinRequest getEarlyCheckinRequest() {
        return earlyCheckinRequest;
    }

    public void setEarlyCheckinRequest(EarlyCheckinRequest earlyCheckinRequest) {
        this.earlyCheckinRequest = earlyCheckinRequest;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }
}
