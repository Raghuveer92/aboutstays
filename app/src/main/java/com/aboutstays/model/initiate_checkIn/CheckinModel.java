package com.aboutstays.model.initiate_checkIn;

/**
 * Created by neeraj on 11/4/17.
 */

public class  CheckinModel {
    String title, name;
    int imageUrl;
    boolean isMendatry;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isMendatry() {
        return isMendatry;
    }

    public void setMendatry(boolean mendatry) {
        isMendatry = mendatry;
    }
}
