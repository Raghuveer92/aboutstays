package com.aboutstays.model.reservation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mrnee on 6/26/2017.
 */

public class ReservationTypeData implements Serializable{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("endTime")
    @Expose
    private String endTime;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("additionalImageUrls")
    @Expose
    private List<String> additionalImageUrls = null;
    @SerializedName("entryCriteria")
    @Expose
    private String entryCriteria;
    @SerializedName("descripton")
    @Expose
    private String descripton;
    @SerializedName("price")
    @Expose
    private long price;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("mostPopularList")
    @Expose
    private List<String> mostPopularList = null;
    @SerializedName("offerings")
    @Expose
    private List<String> offerings = null;
    @SerializedName("amountApplicable")
    @Expose
    private Boolean amountApplicable;
    @SerializedName("durationBased")
    @Expose
    private Boolean durationBased;
    @SerializedName("subCategory")
    @Expose
    private String subCategory;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<String> getAdditionalImageUrls() {
        return additionalImageUrls;
    }

    public void setAdditionalImageUrls(List<String> additionalImageUrls) {
        this.additionalImageUrls = additionalImageUrls;
    }

    public String getEntryCriteria() {
        return entryCriteria;
    }

    public void setEntryCriteria(String entryCriteria) {
        this.entryCriteria = entryCriteria;
    }

    public String getDescripton() {
        return descripton;
    }

    public void setDescripton(String descripton) {
        this.descripton = descripton;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public List<String> getMostPopularList() {
        return mostPopularList;
    }

    public void setMostPopularList(List<String> mostPopularList) {
        this.mostPopularList = mostPopularList;
    }

    public List<String> getOfferings() {
        return offerings;
    }

    public void setOfferings(List<String> offerings) {
        this.offerings = offerings;
    }

    public Boolean getAmountApplicable() {
        return amountApplicable;
    }

    public void setAmountApplicable(Boolean amountApplicable) {
        this.amountApplicable = amountApplicable;
    }

    public Boolean getDurationBased() {
        return durationBased;
    }

    public void setDurationBased(Boolean durationBased) {
        this.durationBased = durationBased;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

}
