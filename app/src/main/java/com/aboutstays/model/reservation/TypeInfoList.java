package com.aboutstays.model.reservation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TypeInfoList implements Serializable{

    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("typeName")
    @Expose
    private String typeName;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("subTypes")
    @Expose
    private List<String> subTypes = null;
    @SerializedName("imageListing")
    @Expose
    private Boolean imageListing;
    @SerializedName("genereId")
    @Expose
    private String genereId;

    public String getGenereId() {
        return genereId;
    }

    public void setGenereId(String genereId) {
        this.genereId = genereId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<String> getSubTypes() {
        return subTypes;
    }

    public void setSubTypes(List<String> subTypes) {
        this.subTypes = subTypes;
    }

    public Boolean getImageListing() {
        return imageListing;
    }

    public void setImageListing(Boolean imageListing) {
        this.imageListing = imageListing;
    }

}