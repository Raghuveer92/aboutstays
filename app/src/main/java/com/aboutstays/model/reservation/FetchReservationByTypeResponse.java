package com.aboutstays.model.reservation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class FetchReservationByTypeResponse extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private List<ReservationByTypeData> data = null;

    public List<ReservationByTypeData> getData() {
        return data;
    }

    public void setData(List<ReservationByTypeData> data) {
        this.data = data;
    }

}