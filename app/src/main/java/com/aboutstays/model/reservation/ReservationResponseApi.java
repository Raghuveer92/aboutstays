package com.aboutstays.model.reservation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class ReservationResponseApi extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private ReservationDataResponse data;

    public ReservationDataResponse getData() {
        return data;
    }

    public void setData(ReservationDataResponse data) {
        this.data = data;
    }
}