package com.aboutstays.model.reservation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by mrnee on 6/26/2017.
 */

public class ReservationTypeResponseApi extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private List<ReservationTypeData> data = null;

    public List<ReservationTypeData> getData() {
        return data;
    }

    public void setData(List<ReservationTypeData> data) {
        this.data = data;
    }
}
