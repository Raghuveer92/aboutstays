package com.aboutstays.model.reservation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by neeraj on 4/4/17.
 */

public class ReservationByTypeData implements Serializable{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("endTime")
    @Expose
    private String endTime;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("wingId")
    @Expose
    private String wingId;
    @SerializedName("floorId")
    @Expose
    private String floorId;
    @SerializedName("entryCriteria")
    @Expose
    private String entryCriteria;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("mostPopular")
    @Expose
    private List<String> mostPopular = null;
    @SerializedName("mostPopularList")
    @Expose
    private List<String> mostPopularList = null;
    @SerializedName("hasDeals")
    @Expose
    private Boolean hasDeals;
    @SerializedName("provideReservations")
    @Expose
    private Boolean provideReservations;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("additionalImageUrls")
    @Expose
    private List<String> additionalImageUrls = null;
    @SerializedName("reservationType")
    @Expose
    private Integer reservationType;
    @SerializedName("primaryCuisine")
    @Expose
    private String primaryCuisine;
    @SerializedName("guestSenseCategory")
    @Expose
    private String guestSenseCategory;
    @SerializedName("amenityType")
    @Expose
    private String amenityType;
    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    @SerializedName("reservationCategoryName")
    @Expose
    private String reservationCategoryName;
    @SerializedName("directionList")
    @Expose
    private Object directionList;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("offerings")
    @Expose
    private List<String> offerings = null;
    @SerializedName("amountApplicable")
    @Expose
    private Boolean amountApplicable;
    @SerializedName("durationBased")
    @Expose
    private Boolean durationBased;
    @SerializedName("subCategory")
    @Expose
    private String subCategory;
    @SerializedName("id")
    @Expose
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getWingId() {
        return wingId;
    }

    public void setWingId(String wingId) {
        this.wingId = wingId;
    }

    public String getFloorId() {
        return floorId;
    }

    public void setFloorId(String floorId) {
        this.floorId = floorId;
    }

    public String getEntryCriteria() {
        return entryCriteria;
    }

    public void setEntryCriteria(String entryCriteria) {
        this.entryCriteria = entryCriteria;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getMostPopular() {
        return mostPopular;
    }

    public void setMostPopular(List<String> mostPopular) {
        this.mostPopular = mostPopular;
    }

    public Boolean getHasDeals() {
        return hasDeals;
    }

    public void setHasDeals(Boolean hasDeals) {
        this.hasDeals = hasDeals;
    }

    public Boolean getProvideReservations() {
        return provideReservations;
    }

    public void setProvideReservations(Boolean provideReservations) {
        this.provideReservations = provideReservations;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<String> getAdditionalImageUrls() {
        return additionalImageUrls;
    }

    public void setAdditionalImageUrls(List<String> additionalImageUrls) {
        this.additionalImageUrls = additionalImageUrls;
    }

    public Integer getReservationType() {
        return reservationType;
    }

    public void setReservationType(Integer reservationType) {
        this.reservationType = reservationType;
    }

    public String getPrimaryCuisine() {
        return primaryCuisine;
    }

    public void setPrimaryCuisine(String primaryCuisine) {
        this.primaryCuisine = primaryCuisine;
    }

    public String getGuestSenseCategory() {
        return guestSenseCategory;
    }

    public void setGuestSenseCategory(String guestSenseCategory) {
        this.guestSenseCategory = guestSenseCategory;
    }

    public String getAmenityType() {
        return amenityType;
    }

    public void setAmenityType(String amenityType) {
        this.amenityType = amenityType;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getReservationCategoryName() {
        return reservationCategoryName;
    }

    public void setReservationCategoryName(String reservationCategoryName) {
        this.reservationCategoryName = reservationCategoryName;
    }

    public Object getDirectionList() {
        return directionList;
    }

    public void setDirectionList(Object directionList) {
        this.directionList = directionList;
    }

    public List<String> getMostPopularList() {
        return mostPopularList;
    }

    public void setMostPopularList(List<String> mostPopularList) {
        this.mostPopularList = mostPopularList;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public List<String> getOfferings() {
        return offerings;
    }

    public void setOfferings(List<String> offerings) {
        this.offerings = offerings;
    }

    public Boolean getAmountApplicable() {
        return amountApplicable;
    }

    public void setAmountApplicable(Boolean amountApplicable) {
        this.amountApplicable = amountApplicable;
    }

    public Boolean getDurationBased() {
        return durationBased;
    }

    public void setDurationBased(Boolean durationBased) {
        this.durationBased = durationBased;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
