package com.aboutstays.model.reservation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ReservationServicePojo implements Serializable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("typeInfoList")
    @Expose
    private List<TypeInfoList> typeInfoList = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<TypeInfoList> getTypeInfoList() {
        return typeInfoList;
    }

    public void setTypeInfoList(List<TypeInfoList> typeInfoList) {
        this.typeInfoList = typeInfoList;
    }

}