package com.aboutstays.model.reservation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mrnee on 6/26/2017.
 */

public class ReservationDataResponse implements Serializable {

    @SerializedName("categories")
    @Expose
    private List<ReservationByTypeData> categories = null;

    public List<ReservationByTypeData> getCategories() {
        return categories;
    }

    public void setCategories(List<ReservationByTypeData> categories) {
        this.categories = categories;
    }


}
