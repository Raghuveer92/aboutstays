package com.aboutstays.model.reservation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by neeraj on 3/4/17.
 */

public class ReservationData implements Serializable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("rsId")
    @Expose
    private String rsId;
    @SerializedName("reservationType")
    @Expose
    private Integer reservationType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("openingTime")
    @Expose
    private String openingTime;
    @SerializedName("closingTime")
    @Expose
    private String closingTime;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("additionalImageUrls")
    @Expose
    private List<String> additionalImageUrls = null;
    @SerializedName("entryCriteria")
    @Expose
    private String entryCriteria;
    @SerializedName("shortDescription")
    @Expose
    private String shortDescription;
    @SerializedName("longDescripiton")
    @Expose
    private String longDescripiton;
    @SerializedName("leaf")
    @Expose
    private Boolean leaf;
    @SerializedName("parentId")
    @Expose
    private String parentId;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("mostPopularList")
    @Expose
    private List<String> mostPopularList = null;
    @SerializedName("type")
    @Expose
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getRsId() {
        return rsId;
    }

    public void setRsId(String rsId) {
        this.rsId = rsId;
    }

    public Integer getReservationType() {
        return reservationType;
    }

    public void setReservationType(Integer reservationType) {
        this.reservationType = reservationType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<String> getAdditionalImageUrls() {
        return additionalImageUrls;
    }

    public void setAdditionalImageUrls(List<String> additionalImageUrls) {
        this.additionalImageUrls = additionalImageUrls;
    }

    public String getEntryCriteria() {
        return entryCriteria;
    }

    public void setEntryCriteria(String entryCriteria) {
        this.entryCriteria = entryCriteria;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescripiton() {
        return longDescripiton;
    }

    public void setLongDescripiton(String longDescripiton) {
        this.longDescripiton = longDescripiton;
    }

    public Boolean getLeaf() {
        return leaf;
    }

    public void setLeaf(Boolean leaf) {
        this.leaf = leaf;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public List<String> getMostPopularList() {
        return mostPopularList;
    }

    public void setMostPopularList(List<String> mostPopularList) {
        this.mostPopularList = mostPopularList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
