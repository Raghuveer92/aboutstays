package com.aboutstays.model.hotels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 4/10/17.
 */

public class OtherServiceData implements Serializable {

    public OtherServiceData() {
    }

    public OtherServiceData(List<Amenity> itemsList) {
        this.itemsList = itemsList;
    }

    private List<Amenity> itemsList=new ArrayList<>();

    public List<Amenity> getIconData() {
        return itemsList;
    }

    public OtherServiceData setAwardsList(List<NameImageGeneralInfo> awardsList){
        for(NameImageGeneralInfo nameImageGeneralInfo:awardsList){
            Amenity amenity=new Amenity();
            amenity.setName(nameImageGeneralInfo.getName());
            amenity.setIconUrl(nameImageGeneralInfo.getImageUrl());
            itemsList.add(amenity);
        }
        return this;
    }

    public OtherServiceData setList(List<AwardsData> listCardAccepted) {
        for(AwardsData nameImageGeneralInfo:listCardAccepted){
            NameImageGeneralInfo award = nameImageGeneralInfo.getAward();
            if(award!=null){
                Amenity amenity=new Amenity();
                amenity.setName(award.getName());
                amenity.setIconUrl(award.getImageUrl());
                itemsList.add(amenity);
            }
        }
        return this;
    }
}
