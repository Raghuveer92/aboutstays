package com.aboutstays.model.hotels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by aman on 10/04/17.
 */

public class HotelOthersInfo implements Serializable{

    @SerializedName("listCardAccepted")
    @Expose
    private List<NameImageGeneralInfo> listCardAccepted = null;
    @SerializedName("listAwards")
    @Expose
    private List<AwardsData> listAwards = null;

    public List<NameImageGeneralInfo> getListCardAccepted() {
        return listCardAccepted;
    }

    public void setListCardAccepted(List<NameImageGeneralInfo> listCardAccepted) {
        this.listCardAccepted = listCardAccepted;
    }

    public List<AwardsData> getListAwards() {
        return listAwards;
    }

    public void setListAwards(List<AwardsData> listAwards) {
        this.listAwards = listAwards;
    }
}
