package com.aboutstays.model.hotels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HotelData {

    @SerializedName("listHotels")
    @Expose
    private List<Hotel> listHotels = null;

    public List<Hotel> getListHotels() {
        return listHotels;
    }

    public void setListHotels(List<Hotel> listHotels) {
        this.listHotels = listHotels;
    }

}