package com.aboutstays.model.hotels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by neeraj on 24/3/17.
 */

public class GetHotelById extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private Hotel data;

    public Hotel getData() {
        return data;
    }

    public void setData(Hotel data) {
        this.data = data;
    }

}
