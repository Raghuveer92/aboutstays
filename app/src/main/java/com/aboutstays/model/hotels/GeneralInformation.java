package com.aboutstays.model.hotels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GeneralInformation implements Serializable{

@SerializedName("address")
@Expose
private HotelAddress address;
@SerializedName("phone1")
@Expose
private String phone1;
@SerializedName("phone2")
@Expose
private String phone2;
@SerializedName("email")
@Expose
private String email;
@SerializedName("fax")
@Expose
private String fax;
@SerializedName("logoUrl")
@Expose
private String logoUrl;
@SerializedName("name")
@Expose
private String name;
@SerializedName("description")
@Expose
private String description;
@SerializedName("checkInTime")
@Expose
private String checkInTime;
@SerializedName("checkOutTime")
@Expose
private String checkOutTime;
@SerializedName("imageUrl")
@Expose
private String imageUrl;

public HotelAddress getAddress() {
return address;
}

public void setAddress(HotelAddress address) {
this.address = address;
}

public String getPhone1() {
return phone1;
}

public void setPhone1(String phone1) {
this.phone1 = phone1;
}

public String getPhone2() {
return phone2;
}

public void setPhone2(String phone2) {
this.phone2 = phone2;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public String getFax() {
return fax;
}

public void setFax(String fax) {
this.fax = fax;
}

public String getLogoUrl() {
return logoUrl;
}

public void setLogoUrl(String logoUrl) {
this.logoUrl = logoUrl;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public String getCheckInTime() {
return checkInTime;
}

public void setCheckInTime(String checkInTime) {
this.checkInTime = checkInTime;
}

public String getCheckOutTime() {
return checkOutTime;
}

public void setCheckOutTime(String checkOutTime) {
this.checkOutTime = checkOutTime;
}

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}