package com.aboutstays.model.hotels;

import java.io.Serializable;

/**
 * Created by admin on 6/9/17.
 */

public class AwardsData implements Serializable{
    private String year;
    private  NameImageGeneralInfo award;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public NameImageGeneralInfo getAward() {
        return award;
    }

    public void setAward(NameImageGeneralInfo award) {
        this.award = award;
    }
}
