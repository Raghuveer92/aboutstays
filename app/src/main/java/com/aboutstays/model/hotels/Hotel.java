package com.aboutstays.model.hotels;

import com.aboutstays.model.reservation.ReservationServicePojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Hotel implements Serializable {

    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("hotelType")
    @Expose
    private Integer hotelType=new Integer(0);
    @SerializedName("generalInformation")
    @Expose
    private GeneralInformation generalInformation;
    @SerializedName("roomsIds")
    @Expose
    private List<Object> roomsIds = null;
    @SerializedName("listResturantAndCuisines")
    @Expose
    private Object listResturantAndCuisines;
    @SerializedName("reviewsAndRatings")
    @Expose
    private Object reviewsAndRatings;
    @SerializedName("healthAndBeauty")
    @Expose
    private Object healthAndBeauty;
    @SerializedName("sportsAndEntertaiment")
    @Expose
    private Object sportsAndEntertaiment;
    @SerializedName("businessAndParties")
    @Expose
    private Object businessAndParties;
    @SerializedName("shopsAndStores")
    @Expose
    private Object shopsAndStores;
    @SerializedName("essentials")
    @Expose
    private List<Amenity> essentials=new ArrayList<>();

    @SerializedName("partnershipType")
    @Expose
    private int partnershipType;
    @SerializedName("amenities")
    @Expose
    private List<Amenity> amenities ;
    @SerializedName("listAwards")
    @Expose
    private List<AwardsData> listAwards = new ArrayList<>();
    @SerializedName("listCardAccepted")
    @Expose
    private List<NameImageGeneralInfo> listCardAccepted = new ArrayList<>();

    @SerializedName("reservationServicePojo")
    @Expose
    private ReservationServicePojo reservationServicePojo;

    public ReservationServicePojo getReservationServicePojo() {
        return reservationServicePojo;
    }

    public void setReservationServicePojo(ReservationServicePojo reservationServicePojo) {
        this.reservationServicePojo = reservationServicePojo;
    }

    public int getPartnershipType() {
        return partnershipType;
    }

    public void setPartnershipType(int partnershipType) {
        this.partnershipType = partnershipType;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public int getHotelType() {
        return hotelType;
    }

    public void setHotelType(Integer hotelType) {
        this.hotelType = hotelType;
    }

    public GeneralInformation getGeneralInformation() {
        return generalInformation;
    }

    public void setGeneralInformation(GeneralInformation generalInformation) {
        this.generalInformation = generalInformation;
    }

    public List<Object> getRoomsIds() {
        return roomsIds;
    }

    public void setRoomsIds(List<Object> roomsIds) {
        this.roomsIds = roomsIds;
    }

    public Object getListResturantAndCuisines() {
        return listResturantAndCuisines;
    }

    public void setListResturantAndCuisines(Object listResturantAndCuisines) {
        this.listResturantAndCuisines = listResturantAndCuisines;
    }

    public Object getReviewsAndRatings() {
        return reviewsAndRatings;
    }

    public void setReviewsAndRatings(Object reviewsAndRatings) {
        this.reviewsAndRatings = reviewsAndRatings;
    }

    public Object getHealthAndBeauty() {
        return healthAndBeauty;
    }

    public void setHealthAndBeauty(Object healthAndBeauty) {
        this.healthAndBeauty = healthAndBeauty;
    }

    public Object getSportsAndEntertaiment() {
        return sportsAndEntertaiment;
    }

    public void setSportsAndEntertaiment(Object sportsAndEntertaiment) {
        this.sportsAndEntertaiment = sportsAndEntertaiment;
    }

    public Object getBusinessAndParties() {
        return businessAndParties;
    }

    public void setBusinessAndParties(Object businessAndParties) {
        this.businessAndParties = businessAndParties;
    }

    public Object getShopsAndStores() {
        return shopsAndStores;
    }

    public void setShopsAndStores(Object shopsAndStores) {
        this.shopsAndStores = shopsAndStores;
    }

    public List<Amenity> getEssentials() {
        return essentials;
    }

    public void setEssentials(List<Amenity> essentials) {
        this.essentials = essentials;
    }

    public List<Amenity> getAmenities() {
        if(amenities==null){
            amenities=new ArrayList<>();
        }
        return amenities;
    }

    public List<AwardsData> getListAwards() {
        return listAwards;
    }

    public void setListAwards(List<AwardsData> listAwards) {
        this.listAwards = listAwards;
    }

    public List<NameImageGeneralInfo> getListCardAccepted() {
        return listCardAccepted;
    }

    public void setListCardAccepted(List<NameImageGeneralInfo> listCardAccepted) {
        this.listCardAccepted = listCardAccepted;
    }

    public void setAmenities(List<Amenity> amenities) {
        this.amenities = amenities;
    }
}