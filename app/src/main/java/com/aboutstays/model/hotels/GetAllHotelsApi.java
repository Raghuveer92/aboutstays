package com.aboutstays.model.hotels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class GetAllHotelsApi extends BaseApiResponse {


    @SerializedName("data")
    @Expose
    private HotelData data;

    public HotelData getData() {
        return data;
    }

    public void setData(HotelData data) {
        this.data = data;
    }


}