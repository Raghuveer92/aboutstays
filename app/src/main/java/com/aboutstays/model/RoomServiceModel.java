package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Rahul Aarya on 30-12-2016.
 */

public class RoomServiceModel implements Serializable {

    public int imageResourceId, imageRightAngle;
    public String title, time;

    public int getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    public int getImageRightAngle() {
        return imageRightAngle;
    }

    public void setImageRightAngle(int imageRightAngle) {
        this.imageRightAngle = imageRightAngle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
