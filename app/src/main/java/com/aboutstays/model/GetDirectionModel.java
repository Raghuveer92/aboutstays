package com.aboutstays.model;

/**
 * Created by mrnee on 5/26/2017.
 */

public class GetDirectionModel {
    private String directionName;
    private String imageUrl;

    public String getDirectionName() {
        return directionName;
    }

    public void setDirectionName(String directionName) {
        this.directionName = directionName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
