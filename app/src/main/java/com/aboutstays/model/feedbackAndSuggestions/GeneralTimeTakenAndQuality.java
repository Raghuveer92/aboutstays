package com.aboutstays.model.feedbackAndSuggestions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aman on 07/04/17.
 */

public class GeneralTimeTakenAndQuality {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("quality")
    @Expose
    private int quality;
    @SerializedName("timeTaken")
    @Expose
    private int timeTaken;
    @SerializedName("qualitySubmitted")
    @Expose
    private boolean qualitySubmitted;
    @SerializedName("timeTakenSubmitted")
    @Expose
    private boolean timeTakenSubmitted;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public int getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(int timeTaken) {
        this.timeTaken = timeTaken;
    }

    public boolean isQualitySubmitted() {
        return qualitySubmitted;
    }

    public void setQualitySubmitted(boolean qualitySubmitted) {
        this.qualitySubmitted = qualitySubmitted;
    }

    public boolean isTimeTakenSubmitted() {
        return timeTakenSubmitted;
    }

    public void setTimeTakenSubmitted(boolean timeTakenSubmitted) {
        this.timeTakenSubmitted = timeTakenSubmitted;
    }
}
