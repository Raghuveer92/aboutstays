package com.aboutstays.model.feedbackAndSuggestions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aman on 07/04/17.
 */

public class FeedbackAndSuggestionsData {

    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("staysId")
    @Expose
    private String staysId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("listFeedback")
    @Expose
    private List<GeneralTimeTakenAndQuality> listFeedbacks;
    @SerializedName("specialStaff")
    @Expose
    private String specialStaff;
    @SerializedName("comment")
    @Expose
    private String comment;


    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStaysId() {
        return staysId;
    }

    public void setStaysId(String staysId) {
        this.staysId = staysId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<GeneralTimeTakenAndQuality> getListFeedbacks() {
        return listFeedbacks;
    }

    public void setListFeedbacks(List<GeneralTimeTakenAndQuality> listFeedbacks) {
        this.listFeedbacks = listFeedbacks;
    }

    public String getSpecialStaff() {
        return specialStaff;
    }

    public void setSpecialStaff(String specialStaff) {
        this.specialStaff = specialStaff;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
