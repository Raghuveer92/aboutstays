package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Rahul Aarya on 02-01-2017.
 */

public class SearchServiceModel implements Serializable {

    public String serviceTitle, serviceCost, ServiceDescription;
    public int imgVegNonveg, imgChilli, imgLike;

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public String getServiceCost() {
        return serviceCost;
    }

    public void setServiceCost(String serviceCost) {
        this.serviceCost = serviceCost;
    }

    public String getServiceDescription() {
        return ServiceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        ServiceDescription = serviceDescription;
    }

    public int getImgVegNonveg() {
        return imgVegNonveg;
    }

    public void setImgVegNonveg(int imgVegNonveg) {
        this.imgVegNonveg = imgVegNonveg;
    }

    public int getImgChilli() {
        return imgChilli;
    }

    public void setImgChilli(int imgChilli) {
        this.imgChilli = imgChilli;
    }

    public int getImgLike() {
        return imgLike;
    }

    public void setImgLike(int imgLike) {
        this.imgLike = imgLike;
    }
}
