package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Rahul Aarya on 29-12-2016.
 */

public class ExpencesModelDateWise implements Serializable {
    public int imageResourceId;
    public String title, amount, time;

    public int getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
