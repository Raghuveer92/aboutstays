package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Rahul Aarya on 30-12-2016.
 */

public class ExpencesDetailModel implements Serializable {

   public String expenceName,Amount;

    public String getExpenceName() {
        return expenceName;
    }

    public void setExpenceName(String expenceName) {
        this.expenceName = expenceName;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }
}
