package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Neeraj on 1/3/2017.
 */

public class MaintenanceModel implements Serializable {
    String title;
    int logo, ivNext;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public int getIvNext() {
        return ivNext;
    }

    public void setIvNext(int ivNext) {
        this.ivNext = ivNext;
    }
}
