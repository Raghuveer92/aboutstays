package com.aboutstays.model.roomService;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by neeraj on 7/3/17.
 */

public class RoomServiceData {

    @SerializedName("listMenuItem")
    @Expose
    private List<RoomServiceMenuItem> listMenuItem = null;

    public List<RoomServiceMenuItem> getListMenuItem() {
        return listMenuItem;
    }

    public void setListMenuItem(List<RoomServiceMenuItem> listMenuItem) {
        this.listMenuItem = listMenuItem;
    }

}
