package com.aboutstays.model.roomService;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class RoomServiceResponse extends BaseApiResponse {

@SerializedName("data")
@Expose
private RoomServiceData data;

public RoomServiceData getData() {
return data;
}

public void setData(RoomServiceData data) {
this.data = data;
}


}