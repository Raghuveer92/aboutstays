package com.aboutstays.model.contactUpdate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OfficeAddress implements Serializable{

@SerializedName("address")
@Expose
private AddressOffice address;

public AddressOffice getAddress() {
return address;
}

public void setAddress(AddressOffice address) {
this.address = address;
}

}