package com.aboutstays.model.contactUpdate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import simplifii.framework.rest.response.pojo.Address;

public class ContactUpdate implements Serializable{

@SerializedName("userId")
@Expose
private String userId;
@SerializedName("email")
@Expose
private String email;
@SerializedName("mobileNumber")
@Expose
private String mobileNumber;
@SerializedName("homeAddress")
@Expose
private Address homeAddress;
@SerializedName("officeAddress")
@Expose
private Address officeAddress;

public String getUserId() {
return userId;
}

public void setUserId(String userId) {
this.userId = userId;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public String getMobileNumber() {
return mobileNumber;
}

public void setMobileNumber(String mobileNumber) {
this.mobileNumber = mobileNumber;
}

public Address getHomeAddress() {
return homeAddress;
}

public void setHomeAddress(Address homeAddress) {
this.homeAddress = homeAddress;
}

public Address getOfficeAddress() {
return officeAddress;
}

public void setOfficeAddress(Address officeAddress) {
this.officeAddress = officeAddress;
}

}