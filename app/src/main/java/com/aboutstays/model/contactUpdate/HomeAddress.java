package com.aboutstays.model.contactUpdate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HomeAddress implements Serializable{

@SerializedName("address")
@Expose
private AddressHome address;

public AddressHome getAddress() {
return address;
}

public void setAddress(AddressHome address) {
this.address = address;
}

}