package com.aboutstays.model.food;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class GetFoodItemsApi extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private FoodDataByType data;


    public FoodDataByType getData() {
        return data;
    }

    public void setData(FoodDataByType data) {
        this.data = data;
    }

}