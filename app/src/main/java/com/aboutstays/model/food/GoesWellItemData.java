package com.aboutstays.model.food;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by neeraj on 9/3/17.
 */

public class GoesWellItemData implements Serializable{

    @SerializedName("fooditemId")
    @Expose
    private String fooditemId;
    @SerializedName("roomServicesId")
    @Expose
    private String roomServicesId;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("serves")
    @Expose
    private Integer serves;
    @SerializedName("cuisineType")
    @Expose
    private String cuisineType;
    @SerializedName("mrp")
    @Expose
    private double mrp;
    @SerializedName("price")
    @Expose
    private double price;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("foodLabel")
    @Expose
    private Integer foodLabel;
    @SerializedName("spicyType")
    @Expose
    private Integer spicyType;
    @SerializedName("recommended")
    @Expose
    private Boolean recommended;
    @SerializedName("menuItemType")
    @Expose
    private Integer menuItemType;

    public String getFooditemId() {
        return fooditemId;
    }

    public void setFooditemId(String fooditemId) {
        this.fooditemId = fooditemId;
    }

    public String getRoomServicesId() {
        return roomServicesId;
    }

    public void setRoomServicesId(String roomServicesId) {
        this.roomServicesId = roomServicesId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getServes() {
        return serves;
    }

    public void setServes(Integer serves) {
        this.serves = serves;
    }

    public String getCuisineType() {
        return cuisineType;
    }

    public void setCuisineType(String cuisineType) {
        this.cuisineType = cuisineType;
    }

    public double getMrp() {
        return mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getFoodLabel() {
        return foodLabel;
    }

    public void setFoodLabel(Integer foodLabel) {
        this.foodLabel = foodLabel;
    }

    public Integer getSpicyType() {
        return spicyType;
    }

    public void setSpicyType(Integer spicyType) {
        this.spicyType = spicyType;
    }

    public Boolean getRecommended() {
        return recommended;
    }

    public void setRecommended(Boolean recommended) {
        this.recommended = recommended;
    }

    public Integer getMenuItemType() {
        return menuItemType;
    }

    public void setMenuItemType(Integer menuItemType) {
        this.menuItemType = menuItemType;
    }

}
