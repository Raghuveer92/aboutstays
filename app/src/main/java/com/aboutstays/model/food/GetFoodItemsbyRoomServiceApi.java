package com.aboutstays.model.food;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class GetFoodItemsbyRoomServiceApi extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private List<FoodDataByType> data = null;

    public List<FoodDataByType> getData() {
        return data;
    }

    public void setData(List<FoodDataByType> data) {
        this.data = data;
    }


}