package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Neeraj on 12/29/2016.
 */

public class BaseAdapterModel implements Serializable {
    protected int viewType;

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }
}
