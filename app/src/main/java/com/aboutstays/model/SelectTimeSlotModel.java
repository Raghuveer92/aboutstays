package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Neeraj on 1/3/2017.
 */

public class SelectTimeSlotModel implements Serializable {
    String tvTime;

    public String getTvTime() {
        return tvTime;
    }

    public void setTvTime(String tvTime) {
        this.tvTime = tvTime;
    }
}
