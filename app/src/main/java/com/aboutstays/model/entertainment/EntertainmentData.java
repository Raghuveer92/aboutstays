package com.aboutstays.model.entertainment;

import com.aboutstays.model.reservation.TypeInfoList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by neeraj on 6/4/17.
 */

public class EntertainmentData implements Serializable{

    @SerializedName("serviceId")
    @Expose
    private String serviceId;
    @SerializedName("typeInfoList")
    @Expose
    private List<TypeInfoList> typeInfoList = null;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public List<TypeInfoList> getTypeInfoList() {
        return typeInfoList;
    }

    public void setTypeInfoList(List<TypeInfoList> typeInfoList) {
        this.typeInfoList = typeInfoList;
    }

}
