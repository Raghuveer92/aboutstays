package com.aboutstays.model.entertainment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by neeraj on 6/4/17.
 */

public class AllEntertainmentData implements Serializable{

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("serviceId")
    @Expose
    private String serviceId;
    @SerializedName("itemId")
    @Expose
    private String itemId;
    @SerializedName("itemDetails")
    @Expose
    private EntertainmentItemDetails itemDetails;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public EntertainmentItemDetails getItemDetails() {
        return itemDetails;
    }

    public void setItemDetails(EntertainmentItemDetails itemDetails) {
        this.itemDetails = itemDetails;
    }


}
