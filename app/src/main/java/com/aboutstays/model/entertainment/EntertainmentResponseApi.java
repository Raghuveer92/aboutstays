package com.aboutstays.model.entertainment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by neeraj on 6/4/17.
 */

public class EntertainmentResponseApi extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private EntertainmentData data;

    public EntertainmentData getData() {
        return data;
    }

    public void setData(EntertainmentData data) {
        this.data = data;
    }

}
