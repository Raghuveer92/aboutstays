package com.aboutstays.model.entertainment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;

/**
 * Created by neeraj on 6/4/17.
 */

public class AllEntertainmentResponseApi  extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private List<AllEntertainmentData> data = null;

    public List<AllEntertainmentData> getData() {
        return data;
    }

    public void setData(List<AllEntertainmentData> data) {
        this.data = data;
    }

}
