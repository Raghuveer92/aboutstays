package com.aboutstays.model.entertainment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GenereDetails implements Serializable{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("masterGenereId")
    @Expose
    private String masterGenereId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getMasterGenereId() {
        return masterGenereId;
    }

    public void setMasterGenereId(String masterGenereId) {
        this.masterGenereId = masterGenereId;
    }

}