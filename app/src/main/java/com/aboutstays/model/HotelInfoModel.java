package com.aboutstays.model;

/**
 * Created by Neeraj on 12/29/2016.
 */

public class HotelInfoModel {
    String name;
    int logo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }
}
