package com.aboutstays.model.commute;

import com.aboutstays.rest.response.CarDetailsAndPrice;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListCommutePackage {

    @SerializedName("packageName")
    @Expose
    private String packageName;
    @SerializedName("carDetailsAndPriceList")
    @Expose
    private List<CarDetailsAndPrice> carDetailsAndPriceList = null;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public List<CarDetailsAndPrice> getCarDetailsAndPriceList() {
        return carDetailsAndPriceList;
    }

    public void setCarDetailsAndPriceList(List<CarDetailsAndPrice> carDetailsAndPriceList) {
        this.carDetailsAndPriceList = carDetailsAndPriceList;
    }

}