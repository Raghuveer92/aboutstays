package com.aboutstays.model.commute;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by neeraj on 31/3/17.
 */

public class CommuteData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("listCommutePackage")
    @Expose
    private List<ListCommutePackage> listCommutePackage = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ListCommutePackage> getListCommutePackage() {
        return listCommutePackage;
    }

    public void setListCommutePackage(List<ListCommutePackage> listCommutePackage) {
        this.listCommutePackage = listCommutePackage;
    }


}
