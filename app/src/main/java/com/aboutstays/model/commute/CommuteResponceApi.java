package com.aboutstays.model.commute;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class CommuteResponceApi extends BaseApiResponse{

@SerializedName("data")
@Expose
private CommuteData data;

public CommuteData getData() {
return data;
}

public void setData(CommuteData data) {
this.data = data;
}

}