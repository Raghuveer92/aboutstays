package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by ajay on 14/12/16.
 */

public class AddCardModel implements Serializable{
    String bankName, cardNumber, validDate ,addNewCard;
    boolean showAddNewCardLayout;

    public boolean isShowAddNewCardLayout() {
        return showAddNewCardLayout;
    }

    public void setShowAddNewCardLayout(boolean showAddNewCardLayout) {
        this.showAddNewCardLayout = showAddNewCardLayout;
    }

    public String getAddNewCard() {
        return addNewCard;
    }

    public void setAddNewCard(String addNewCard) {
        this.addNewCard = addNewCard;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getValidDate() {
        return validDate;
    }

    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }
}
