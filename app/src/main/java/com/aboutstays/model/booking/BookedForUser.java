package com.aboutstays.model.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BookedForUser implements Serializable{

@SerializedName("name")
@Expose
private String name;
@SerializedName("date")
@Expose
private String date;
@SerializedName("pax")
@Expose
private String pax;

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getDate() {
return date;
}

public void setDate(String date) {
this.date = date;
}

public String getPax() {
return pax;
}

public void setPax(String pax) {
this.pax = pax;
}

}