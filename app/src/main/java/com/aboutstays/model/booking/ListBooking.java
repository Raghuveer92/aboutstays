package com.aboutstays.model.booking;

import com.aboutstays.model.rooms_category.RoomsCategoryData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ListBooking implements Serializable {

    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("roomId")
    @Expose
    private String roomId;
    @SerializedName("checkInDate")
    @Expose
    private String checkInDate;
    @SerializedName("checkOutDate")
    @Expose
    private String checkOutDate;
    @SerializedName("bookedByUser")
    @Expose
    private BookedByUser bookedByUser;
    @SerializedName("bookedForUser")
    @Expose
    private BookedForUser bookedForUser;
    @SerializedName("bookingId")
    @Expose
    private String bookingId;
    @SerializedName("reservationNumber")
    @Expose
    private String reservationNumber;
    @SerializedName("roomPojo")
    @Expose
    private RoomsCategoryData roomPojo;
    @SerializedName("addedToStayline")
    @Expose
    private boolean addedToStayline;

    public boolean isAddedToStayline() {
        return addedToStayline;
    }

    public void setAddedToStayline(boolean addedToStayline) {
        this.addedToStayline = addedToStayline;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    public String getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public BookedByUser getBookedByUser() {
        return bookedByUser;
    }

    public void setBookedByUser(BookedByUser bookedByUser) {
        this.bookedByUser = bookedByUser;
    }

    public BookedForUser getBookedForUser() {
        return bookedForUser;
    }

    public void setBookedForUser(BookedForUser bookedForUser) {
        this.bookedForUser = bookedForUser;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getReservationNumber() {
        return reservationNumber;
    }

    public void setReservationNumber(String reservationNumber) {
        this.reservationNumber = reservationNumber;
    }

    public RoomsCategoryData getRoomPojo() {
        return roomPojo;
    }

    public void setRoomPojo(RoomsCategoryData roomPojo) {
        this.roomPojo = roomPojo;
    }
}