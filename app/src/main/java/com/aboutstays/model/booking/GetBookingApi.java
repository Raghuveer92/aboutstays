package com.aboutstays.model.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class GetBookingApi extends BaseApiResponse implements Serializable{


@SerializedName("data")
@Expose
private BookingData data;


public BookingData getData() {
return data;
}

public void setData(BookingData data) {
this.data = data;
}

}