package com.aboutstays.model.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by neeraj on 15/2/17.
 */

public class BookingData implements Serializable{

    @SerializedName("listBookings")
    @Expose
    private List<ListBooking> listBookings = null;

    public List<ListBooking> getListBookings() {
        return listBookings;
    }

    public void setListBookings(List<ListBooking> listBookings) {
        this.listBookings = listBookings;
    }

}
