package com.aboutstays.model.housekeeping;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by neeraj on 17/3/17.
 */

public class SearchHousekeepingData {

    @SerializedName("itemId")
    @Expose
    private String itemId;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("housekeepingServicesId")
    @Expose
    private String housekeepingServicesId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mrp")
    @Expose
    private Double mrp;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("housekeepingItemType")
    @Expose
    private Integer housekeepingItemType;
    @SerializedName("complementory")
    @Expose
    private Boolean complementory;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getHousekeepingServicesId() {
        return housekeepingServicesId;
    }

    public void setHousekeepingServicesId(String housekeepingServicesId) {
        this.housekeepingServicesId = housekeepingServicesId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMrp() {
        return mrp;
    }

    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getHousekeepingItemType() {
        return housekeepingItemType;
    }

    public void setHousekeepingItemType(Integer housekeepingItemType) {
        this.housekeepingItemType = housekeepingItemType;
    }

    public Boolean getComplementory() {
        return complementory;
    }

    public void setComplementory(Boolean complementory) {
        this.complementory = complementory;
    }

}
