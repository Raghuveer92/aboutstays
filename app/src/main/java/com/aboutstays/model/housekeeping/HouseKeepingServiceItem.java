package com.aboutstays.model.housekeeping;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class HouseKeepingServiceItem extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private HouseKeepingData data;


    public HouseKeepingData getData() {
        return data;
    }

    public void setData(HouseKeepingData data) {
        this.data = data;
    }


}