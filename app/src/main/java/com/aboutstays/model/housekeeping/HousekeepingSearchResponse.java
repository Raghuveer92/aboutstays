package com.aboutstays.model.housekeeping;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class HousekeepingSearchResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private List<SearchHousekeepingData> data = null;

    public List<SearchHousekeepingData> getData() {
        return data;
    }

    public void setData(List<SearchHousekeepingData> data) {
        this.data = data;
    }
}