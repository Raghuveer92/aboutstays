package com.aboutstays.model.housekeeping;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class HouseKeepingItemDataResponce extends BaseApiResponse{

    @SerializedName("data")
    @Expose
    private List<HouseKeepingItemData> data = null;


    public List<HouseKeepingItemData> getData() {
        return data;
    }

    public void setData(List<HouseKeepingItemData> data) {
        this.data = data;
    }


}