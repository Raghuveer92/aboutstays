package com.aboutstays.model.housekeeping;

import com.aboutstays.model.GeneralItemTypeInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by neeraj on 17/3/17.
 */

public class HouseKeepingItemList extends GeneralItemTypeInfo{

    private int itemCount = 0;

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

}
