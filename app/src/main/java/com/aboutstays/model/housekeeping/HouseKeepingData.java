package com.aboutstays.model.housekeeping;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by neeraj on 17/3/17.
 */

public class HouseKeepingData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("listHKItems")
    @Expose
    private List<HouseKeepingItemList> listHKItems = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<HouseKeepingItemList> getListHKItems() {
        return listHKItems;
    }

    public void setListHKItems(List<HouseKeepingItemList> listHKItems) {
        this.listHKItems = listHKItems;
    }

}
