package com.aboutstays.model.housekeeping;

import com.aboutstays.model.GeneralItemTypeInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by neeraj on 17/3/17.
 */

public class HouseKeepingItemData extends GeneralItemTypeInfo{

    @SerializedName("itemId")
    @Expose
    private String itemId;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("housekeepingServicesId")
    @Expose
    private String housekeepingServicesId;
    private String category;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mrp")
    @Expose
    private Double mrp;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("showOnApp")
    @Expose
    private Boolean showOnApp;
    @SerializedName("complementory")
    @Expose
    private Boolean complementory;

    private boolean ifAdd = false;
    private int itemCount = 0;

    public boolean isIfAdd() {
        return ifAdd;
    }

    public void setIfAdd(boolean ifAdd) {
        this.ifAdd = ifAdd;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getHousekeepingServicesId() {
        return housekeepingServicesId;
    }

    public void setHousekeepingServicesId(String housekeepingServicesId) {
        this.housekeepingServicesId = housekeepingServicesId;
    }

    public Boolean getShowOnApp() {
        return showOnApp;
    }

    public void setShowOnApp(Boolean showOnApp) {
        this.showOnApp = showOnApp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMrp() {
        return mrp;
    }

    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getComplementory() {
        return complementory;
    }

    public void setComplementory(Boolean complementory) {
        this.complementory = complementory;
    }

    public String getCategory() {
        return category;
    }
}
