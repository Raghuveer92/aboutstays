package com.aboutstays.model.common;

import android.preference.Preference;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import simplifii.framework.rest.response.requests.StayPref;
import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.rest.response.response.PrefData;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;

/**
 * Created by aman on 01/05/17.
 */

public class FetchCommonData extends BaseApiResponse{

    @SerializedName("listPreferneces")
    @Expose
    private List<StayPref> listPreferneces = null;
    @SerializedName("listTitles")
    @Expose
    private List<String> listTitles = null;
    @SerializedName("listIdentityDocs")
    @Expose
    private List<String> listIdentityDocs = null;


    public List<String> getListTitles() {
        return listTitles;
    }

    public void setListTitles(List<String> listTitles) {
        this.listTitles = listTitles;
    }

    public List<String> getListIdentityDocs() {
        return listIdentityDocs;
    }

    public void setListIdentityDocs(List<String> listIdentityDocs) {
        this.listIdentityDocs = listIdentityDocs;
    }

    public List<StayPref> getListPreferneces() {
        return listPreferneces;
    }

    public void setListPreferneces(List<StayPref> listPreferneces) {
        this.listPreferneces = listPreferneces;
    }

    public static FetchCommonData parseJson(String json) {
        FetchCommonData fetchCommonData = (FetchCommonData) JsonUtil.parseJson(json, FetchCommonData.class);
        if(fetchCommonData != null && !fetchCommonData.getError()) {
            List<StayPref> listStayPref = new ArrayList<>();
            if(CollectionUtils.isNotEmpty(fetchCommonData.getListPreferneces())) {
                listStayPref.addAll(fetchCommonData.getListPreferneces());
                PrefData prefData = new PrefData();
                prefData.setListPreferneces(listStayPref);
                String prefJson = JsonUtil.toJson(prefData);
                Preferences.saveData(Preferences.KEY_PREFERENCES, prefJson);
            }

            Preferences.saveData(Preferences.KEY_PROFILE_TITLES, new HashSet<String>(fetchCommonData.getListTitles()));
            Preferences.saveData(Preferences.KEY_IDENTITY_DOCS, new HashSet<String>(fetchCommonData.getListIdentityDocs()));
        }
        return fetchCommonData;
    }
}
