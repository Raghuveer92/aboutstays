package com.aboutstays.model.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashSet;
import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;

/**
 * Created by aman on 02/05/17.
 */

public class FetchIdentityDocsType extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private List<String> identityDocTypes;

    public List<String> getIdentityDocTypes() {
        return identityDocTypes;
    }

    public void setIdentityDocTypes(List<String> identityDocTypes) {
        this.identityDocTypes = identityDocTypes;
    }

    public static FetchIdentityDocsType parseJson(String json) {
        FetchIdentityDocsType fetchIdentityDocsType = (FetchIdentityDocsType) JsonUtil.parseJson(json, FetchIdentityDocsType.class);
        if(fetchIdentityDocsType != null && !fetchIdentityDocsType.getError()) {
            Preferences.saveData(Preferences.KEY_IDENTITY_DOCS, new HashSet<String>(fetchIdentityDocsType.getIdentityDocTypes()));
        }
        return fetchIdentityDocsType;
    }
}
