package com.aboutstays.model.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashSet;
import java.util.List;

import simplifii.framework.rest.response.response.BaseApiResponse;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;

/**
 * Created by robin on 4/29/17.
 */

public class FetchProfileTitles extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private List<String> profileTitles;

    public List<String> getProfileTitles() {
        return profileTitles;
    }

    public void setProfileTitles(List<String> profileTitles) {
        this.profileTitles = profileTitles;
    }

    public static FetchProfileTitles parseJson(String json){
        FetchProfileTitles fetchProfileTitles = (FetchProfileTitles) JsonUtil.parseJson(json, FetchProfileTitles.class);
        if(fetchProfileTitles!=null&&!fetchProfileTitles.getError()){
            Preferences.saveData(Preferences.KEY_PROFILE_TITLES, new HashSet<String>(fetchProfileTitles.getProfileTitles()));
        }
        return fetchProfileTitles;
    }
}
