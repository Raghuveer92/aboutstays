package com.aboutstays.model.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.rest.response.response.BaseApiResponse;

public class NotificationResponseApi extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private NotificationData data;

    public NotificationData getData() {
        return data;
    }

    public void setData(NotificationData data) {
        this.data = data;
    }

}