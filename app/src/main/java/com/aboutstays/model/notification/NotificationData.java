package com.aboutstays.model.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mrnee on 4/28/2017.
 */

public class NotificationData {

    @SerializedName("notificationsList")
    @Expose
    private List<NotificationsList> notificationsList = null;

    public List<NotificationsList> getNotificationsList() {
        return notificationsList;
    }

    public void setNotificationsList(List<NotificationsList> notificationsList) {
        this.notificationsList = notificationsList;
    }

}
