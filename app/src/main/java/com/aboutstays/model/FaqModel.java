package com.aboutstays.model;

import java.io.Serializable;

/**
 * Created by Neeraj on 12/29/2016.
 */

public class FaqModel implements Serializable {
    String faqText;

    public String getFaqText() {
        return faqText;
    }

    public void setFaqText(String faqText) {
        this.faqText = faqText;
    }
}
