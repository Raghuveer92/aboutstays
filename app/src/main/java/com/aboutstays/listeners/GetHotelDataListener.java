package com.aboutstays.listeners;

import com.aboutstays.model.hotels.Hotel;

import java.util.List;

/**
 * Created by admin on 4/7/17.
 */

public interface GetHotelDataListener {
    List<Hotel> getHotelList();
}
